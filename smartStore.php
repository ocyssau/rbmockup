<?php

//Setup
require_once './conf/ranchbe_setup.php';
require_once './class/common/container.php'; //Class to manage the container
require_once './GUI/smartStore.php';

//This form must be talk by a father to get the document name to store and the
//base_dir of the file. Get this values here
$Manager =& container::_factory($_REQUEST['space'] , $_REQUEST['SelectedContainer']); //Create new manager
$space =& $Manager->space;
$area_id = $Manager->AREA_ID;

if(!is_object($space)){
  global $error_stack;
  $error_stack->push(ERROR, 'Error', array(), 'You must activated a container');
  $error_stack->checkErrors(true);
  die;
}

//init wildspace
$wilspacePath = $space->getWildspace();

$Manager->RecordSelectedContainer(); //Record the selected container in the session

if ( isset ( $_SESSION['SelectedProjectNum']) )
  $selected_project    = $_SESSION['SelectedProjectNum'];
if ( isset ( $_SESSION['SelectedProject']) )
  $selected_project_id    = $_SESSION['SelectedProject'];

//Assign name to particular fields
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number');
$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description');
$smarty->assign('CONTAINER_STATE' , $Manager->SPACE_NAME . '_state');
$smarty->assign('CONTAINER_ID' , $Manager->SPACE_NAME . '_id');

$smarty->assign( 'container_number' , $Manager->GetName() );

// Active the tab
$smarty->assign('documentManageScript', "$Manager->SPACE_NAME".'DocManage.php'); //Assign variable to smarty
$smarty->assign('documentManage', 'active'); //Assign variable to smarty

check_ticket('container_document_get',$area_id);

//Construct the form with QuickForm lib
require_once('GUI/GUI.php');
require_once('GUI/form/form_collection.php');
require_once('GUI/form/subform.php');
$form_collection = new form_collection('form', 'post');

//Set hidden field
$form_collection->addElement('hidden', 'space', $_REQUEST['space']);
$form_collection->addElement('hidden', 'SelectedContainer', $_REQUEST['SelectedContainer']);
$form_collection->addElement('hidden', 'SelectedContainerNum', $_REQUEST['SelectedContainerNum']);
//Add submit button
$form_collection->addElement('submit', 'action', 'ValidateStore');

//Create a subForm to set the headers
$form_header = new subform('form_header', 'post');
$form_header->addElement('header', '', tra('Store files in').' '.$_REQUEST['SelectedContainerNum'] );
$form_header->addElement('header', '', tra('Selection review') );

$form_collection->add( $form_header );

//---------------------------------------------------------------------
switch ( $_REQUEST['action'] ) {
  case ('Store'):   // Step 2 test and set actions
  case ('StoreMulti'):

    if ( count($_REQUEST['checked']) == 0 ){
      $Manager->error_stack->push(ERROR, 'Error', array(), 'You must select at least one file');
      $Manager->error_stack->checkErrors( array('close_button'=>true) );
      break;
    }

    if ( count($_REQUEST['checked']) > 30 ){
      $chunk_select =  array_chunk  ( $_REQUEST['checked']  , 30  , true );
      $_REQUEST['checked'] = $chunk_select[0];
      unset($chunk_select);
      $Manager->error_stack->push(ERROR, 'Warning', array(), 'Sorry but your selection is too large and has been trunk to 30 elements');
    }

    $tmp_list = array(); //init the var for foreach loop
    $clean_list = array(); //init the var for foreach loop
    $i = 0;

    foreach($_REQUEST['checked'] as $file_name){
      $fileInfos = array(); //init var
      $docinfo = array(); //init var
      $doc_properties = array(); //init var
      $i = $i + 1;

      $odocument = $Manager->initDoc(); //init the document object from the current container
      $odocfile = $odocument->initDocfile(); //init the docfile object from current document

      //---------- Check if file exist in wildspace
      require_once './class/common/fsdata.php';
      $odata = fsdata::_dataFactory($wilspacePath.'/'.$file_name);
      if(!$odata){
        $doc_properties['errors'][] = $file_name.' is not in your Wildspace.';
        $doc_properties['actions']['ignore'] = 'Ignore';
        $doc_properties['properties']['file'] = $file_name;
        $smartStore = new smartStore($odocument, $doc_properties);
        $form_collection->add( $smartStore->SetCreateDocForm($i) );
        $doc_properties = array();
        continue;
      }
      $file = $odata->getProperty('file');

      if($file_id = $odocfile->GetFileId( $file_name ) ){ //- The file is recorded in database
        $odocfile->init($file_id);
        $document_id = $odocfile->GetDocumentId(); //Get info about the father document
        $odocument->init($document_id);
        $doc_properties['properties'] = $odocument->GetDocumentInfos(); //Get the document of this file
        $container_id = $doc_properties['properties']['container_id'];
        $container =& $odocument->initContainer($container_id);
        $container->GetName();
        $doc_properties['errors'][] = 'file '.$file_name . ' exist in this database. ID=' . $file_id;
        $doc_properties['errors'][] = 'file '.$file_name . ' is linked to document '. $doc_properties['properties']['document_number'].' ID='.$document_id.
                                      ' in container '.$container->GetName();

        //Compare md5
        if( $odocfile->GetProperty('file_md5') == $odata->getProperty('file_md5') ) $equal_md5 = true;

        if($odocument->CheckAccess() != 0){
          $doc_properties['errors'][] = 'The document '.$doc_properties['document_number'] . ' is not free ';
        }else if( $odocfile->CheckFileAccess() != 0 ){
          $doc_properties['errors'][] = 'file '.$file_name . ' is not free ';
        }else{
          if( $equal_md5 ){
            $doc_properties['errors'][] = 'files '.$file_name.' from wildspace and from vault are strictly identicals';
            $doc_properties['actions']['update_doc'] = 'Update the document '.$doc_properties['document_number'];
          }else{
            $doc_properties['actions']['update_doc_file'] = 'Update the document '.$doc_properties['document_number'].' and files';
            $doc_properties['actions']['update_file'] = 'Update the file '.$file_name.' only';
            $doc_properties['actions']['upgrade_doc'] = 'Create a new indice of '.$doc_properties['document_number'];
          }
        }
        $doc_properties['properties']['file_id'] = $file_id;
        $doc_properties['properties']['file'] = $file_name;
        $doc_properties['properties']['document_id'] = $document_id;

      }else{//-- The file is not recorded in database
        //echo 'Generate the number from the file name<br>';
        $doc_properties['properties']['file'] = $file_name;
        $doc_properties['properties']['document_number'] = $odata->getProperty('doc_name'); //Generate the document number
        $odocument->SetDocProperty( 'document_number', $doc_properties['properties']['document_number'] );
        //Check double documents
        if(in_array($doc_properties['properties']['document_number'] , $tmp_list)){
          $doc_properties['errors'][] = $doc_properties['properties']['document_number'] . ' is not unique in this package';
          //$doc_properties['actions']['add_file'] = 'Add the file to the document '.$doc_properties['properties']['document_number'];
          $doc_properties['actions']['add_file'] = '';
        }else{
          $tmp_list[] = $doc_properties['properties']['document_number'];
          //Check if document exist in database
          if( $document_id = $odocument->DocumentExist( $doc_properties['properties']['document_number'] ) ){
            $doc_properties['errors'][] = 'The document '.$doc_properties['properties']['document_number'] . ' exist in this database. ID=' . $document_id;
            $odocument->init($document_id);
            if($odocument->CheckAccess() !== 0){
              $doc_properties['errors'][] = 'The document '.$doc_properties['properties']['document_number'] . ' is not free ';
            }else{
              //$doc_properties['actions']['add_file'] = 'Add the file to the document '.$doc_properties['properties']['document_number'];
              $doc_properties['actions']['add_file'] = '';
              $doc_properties['actions']['upgrade_doc'] = 'Create a new indice of '.$doc_properties['properties']['document_number'];
            }
            $doc_properties['properties']['document_id'] = $document_id;
          }else{
            $doc_properties['actions']['create_doc'] = 'Create a new document';
          }
        }
      }

      //Check the doctype for the new document
      if(isset($doc_properties['actions']['create_doc'])){
        $fsdata = $odocfile->initFsdata($file);
        $doc_properties['doctype'] = $odocument->SetDocType($fsdata->getProperty('file_extension'), $fsdata->getProperty('file_type'));
        if(!$doc_properties['doctype']){
          $doc_properties['errors'][] = 'This document has a bad doctype';
          unset($doc_properties['actions']); //Reinit all actions
        }else{
          $doc_properties['feedbacks'][] = 'Reconize doctype : '.$doc_properties['doctype']['doctype_number'];
        }
      }

      //Add default actions
      $doc_properties['actions']['ignore'] = 'Ignore';
      //$doc_properties['actions_list'] =& $doc_properties['actions'];
      $smartStore = new smartStore($odocument, $doc_properties);
      $form_collection->add( $smartStore->SetCreateDocForm($i) );
    }//End of foreach

    $form_collection->addElement('hidden', 'loop', $i);

  break;

//---------------------------------------------------------------------
  case ('ValidateStore'):   // Step 3 apply actions
  if ( count($_REQUEST['file']) == 0 ){
    $Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select one file');
    break;
  }

  check_ticket( 'container_document_manage' , $area_id );

  //Loop action and get datas
  for ($i = 0; $i <= $_REQUEST['loop']; $i++){
    $docaction = $_REQUEST['docaction'][$i];
    if( empty($docaction) ) continue;

    //Get properties of document from form
    foreach($_REQUEST['fields'][$i] as $field){
      $doc_properties['properties'][$field] = $_REQUEST[$field][$i]; //document_number, designation, category_id, document_indice_id, document_id, file_id, file + meta-datas
    }
    if( !empty($doc_properties['properties']['document_id']) ){ //the document exist
      require_once('class/common/document.php');
      $odocument = new document($Manager->space, $doc_properties['properties']['document_id'] );
    }else{ //the document must be created
      $odocument =& $Manager->initDoc();
    }
    $doc_properties['actions'] = $_REQUEST['actions'][$i];
    $doc_properties['errors'] = $_REQUEST['errors'][$i];
    $doc_properties['isFrozen'] = $_REQUEST['isFrozen'][$i];
    if($doc_properties['isFrozen']) //redisplay previous feedback only if form is freezed
      $doc_properties['feedbacks'] = $_REQUEST['feedbacks'][$i]; 
    $smartStore = new smartStore($odocument, $doc_properties);
    $smartStore->SetCreateDocForm($i, $docaction, true);
    $form_collection->add( $smartStore->form );
  }//End of for

  $form_collection->addElement('hidden', 'loop', $i);
} //End of switch

$smarty->assign('createForm', $form_collection->toHtml());
$smarty->display('StoreMulti.tpl');
$Manager->error_stack->checkErrors();

?>
