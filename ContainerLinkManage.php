<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once './conf/ranchbe_setup.php';//ranchBE configuration
require_once './class/common/container.php'; //Class to manage the container
require_once './class/common/metadata.php';

$Manager = container::_factory($_REQUEST['space'], $_REQUEST['container_id']); //Create new manager
$space =& $Manager->space;
$metadata = new docmetadata($space);

include './inc/linkManage.php';

?>
