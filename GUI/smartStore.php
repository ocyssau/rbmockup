<?php

//Redefine the function array_intersect_key for php<5.1.0
if (!function_exists('array_intersect_key'))
{
  function array_intersect_key($isec, $keys)
  {
    $argc = func_num_args();
    if ($argc > 2)
    {
      for ($i = 1; !empty($isec) && $i < $argc; $i++)
      {
        $arr = func_get_arg($i);
        foreach (array_keys($isec) as $key)
        {
          if (!isset($arr[$key]))
          {
            unset($isec[$key]);
          }
        }
      }
      return $isec;
    }
    else
    {
      $res = array();
      foreach (array_keys($isec) as $key)
      {
        if (isset($keys[$key]))
        {
          $res[$key] = $isec[$key];
        }
      }
      return $res;
    }
  }
}

class smartStore{

private $doc_properties = array();
public $feedbacks = array();
public $errors = array();
private $document; //object document
private $container; //object container
private $space; //object space
private $wilspacePath; //string path to wildspace
private $area_id; //int
public $actions = array();
private $action_definition = array(
      'create_doc' => 'Create a new document',
      'update_doc_file' => 'Update the document and files',
      'update_doc' => 'Update the document',
      'update_file' => 'Update the file only',
      'add_file' => 'Add the file to current document',
      'ignore' => 'Ignore',
      'upgrade_doc' => 'Create a new indice',
);
public $subformTemplate = "<table><tr>\n{hidden}\n{content}\n</tr></table>";
public $subheaderTemplate = "<h1>{header}</h1>";
public $subelementTemplate = "<td NOWRAP><i>{label}<!-- BEGIN required --><font color=red>*</font><!-- END required --></i>
    <!-- BEGIN error --><font color=red><i><b><br />{error}</b></i></font><!-- END error --><br />
    {element}
    </td>";
public $feedbackTemplate = "<font color=green>{feedback}</font><br />";
public $errorTemplate = "<font color=red><b>{error}</b></font><br />";

//----------------------------------------------------------
/*
*
*/
function __construct(document &$document, &$doc_properties){
  $this->doc_properties = &$doc_properties;
  $this->document_id =& $this->doc_properties['properties']['document_id'];
  if(is_array($this->doc_properties['feedbacks']))
    $this->feedbacks = $this->doc_properties['feedbacks'];
  if(is_array($this->doc_properties['errors']))
    $this->errors = $this->doc_properties['errors'];
  $this->document =& $document;
  $this->container =& $this->document->GetContainer();
  $this->space =& $this->container->space;
  $this->wilspacePath = $this->space->getWildspace();
  $this->area_id = $this->container->AREA_ID;
  $this->isFrozen = $doc_properties['isFrozen'];

  $this->document_indice_id =& $this->doc_properties['properties']['document_indice_id'];
  $this->file_name =& $this->doc_properties['properties']['file']; //file name from form
  $this->file_id =& $this->doc_properties['properties']['file_id']; //file_id from form
  $this->file = $this->wilspacePath.'/'.$this->file_name; //file path from form

  //traduct actions
  /*
  foreach($this->action_definition as $key=>$val){
    $this->action_definition[$key] = tra($val);
  }
  */
  if( is_array(tra('action_definition')) )
    $this->action_definition = tra('action_definition'); //Get array from traduction file if traduction exists. Be careful to order of actions in this array. The first element fund is used as default value.

  
  if(is_array($this->doc_properties['actions']))
    $this->actions = array_intersect_key($this->action_definition , $this->doc_properties['actions']);

//var_dump($this->actions);
//var_dump($this->doc_properties['actions']);

  unset($this->doc_properties['file']); // this property is normaly not a document property
  unset($this->doc_properties['file_id']); // this property is normaly not a document property
  unset($this->doc_properties['document_id']); // this property is normaly not a document property
  unset($this->doc_properties['actions']); // this property is normaly not a document property
  unset($this->doc_properties['errors']); // this property is normaly not a document property
  unset($this->doc_properties['feedbacks']); // this property is normaly not a document property
  unset($this->doc_properties['isFrozen']); // this property is normaly not a document property

  $this->doc_properties['properties'] =& serializeProperties( $this->doc_properties['properties'] );

}//End of method

//----------------------------------------------------------
/* \param $doc_properties(array)
*/
function update_doc(){
  unset($this->doc_properties['properties']['document_indice_id']); // this property is normaly not change by this way
  if( $this->document->UpdateDocInfos($this->doc_properties['properties']) ){ //Update the document record metadatas
    $this->feedbacks[] = 'the document ='.$this->doc_properties['properties']['document_number'].' has been updated.';
  }else{
    $this->errors[] = 'Can not update the document = '.$this->doc_properties['properties']['document_number'];
    return false;
  }

  return true;
} //End of function

/* \param $doc_properties(array)
*/
function update_doc_file(){
  if( $this->update_doc() )
    if( $this->update_file() )
      return true;
} //End of function

/* \param $doc_properties(array)
*/
function update_file(){
  $this->feedbacks[] = 'update_file : '. $this->file_name;
  if( $odocfile =& $this->document->initDocfile($this->file_id) ){
    //---------- Check if file exist in wildspace
    require_once './class/common/fsdata.php';
    $odata =& fsdata::_dataFactory($this->file);
    if($odata){ //-----update the file
      $this->document->CheckOut(false); //Checkout without files checkout
      if($odocfile->CheckOutFile(false)){
        //if($odocfile->CheckInFile(false , false , true)){
        if( $this->document->CheckIn(false, false) ){ //replace document and checkout files
          $this->feedbacks[] = $this->file_name.' has been updated.';
        }else{
          $this->errors[] = 'Can not checkin the file ='. $this->file_name;
          return false;
        }
      }else{
        $this->errors[] = 'Can not checkout the file ='. $this->file_name;
        return false;
      }
    }else{
      $this->errors[] = 'The file '.$this->file_name.' is not in your Wildspace ';
      return false;
    }
  }
  return true;
} //End of function

/* \param $doc_properties(array)
*  Add the file to the document
*/
function add_file(){
  unset($this->doc_properties['properties']['document_indice_id']); // this property is normaly not change by this way
  $this->create_doc();
} //End of function


/* \param $doc_properties(array)
*  \brief! Create a new document
*/
function create_doc(){
  require_once './class/common/fsdata.php';
  $odata =& fsdata::_dataFactory($this->file);
  if($odata){ //Add file to doc
    if( $this->document_id = $this->document->DocumentExist($this->doc_properties['properties']['document_number']) ){ //Dont believe the id give by step 1. The document_id might have changed after an indice upgrade,
      $this->feedbacks[] = 'add_file : '. $this->file_name .' to '.$this->doc_properties['properties']['document_number'];
      $this->document->init($this->document_id);
      if( $this->document->AssociateFile($this->file , true) ){
        $this->feedbacks[] = 'the file '.$this->file_name.' has been associated to document '.$this->doc_properties['properties']['document_number'];
      }else{
        $this->errors[] = 'The file '.$this->file_name.' can not be associated to the document';
        return false;
      }
    }else{ //Create the doc
      $this->feedbacks[] = 'create_doc : '.$this->doc_properties['properties']['document_number'];
      if(!isset($this->doc_properties['properties']['container_id'])){
        $this->doc_properties['properties']['container_id'] = $this->container->GetId();
      }
      $this->doc_properties['properties']['file'] = $this->file_name;
      if($this->document->Store($this->doc_properties['properties']))
        $this->feedbacks[] = 'The doc '.$this->doc_properties['properties']['document_number'].' has been created';
      else{
        $this->errors[] = 'Can not create the doc '.$this->doc_properties['properties']['document_number'];
        return false;
      }
    } //End of create doc
  }else{
    $this->errors[] = 'The file '.$this->file_name.' is not in your Wildspace';
    return false;
  }

  return true;

} //End of function

/* \param $doc_properties(array)
*  //Create a new indice and update the files if specifed
*/
function upgrade_doc(){
  if(check_ticket( 'container_document_change_indice' , $this->area_id , false) === false){
    $this->feedbacks[] = 'You have not permission to upgrade the document '.$this->doc_properties['properties']['document_number'];
    break;
  }
  $this->feedbacks[] = 'Try to upgrade document '.$this->doc_properties['properties']['document_number'];
  //Check access
  $accessCode = $this->document->CheckAccess();
  if($accessCode == 0){
    //Lock the current indice
    if( !$this->document->LockDocument(11) ){
      $this->errors[] = 'Can not lock the document '.$this->doc_properties['properties']['document_number'];
      break;
    }
    //Lock access to files too
    $docfiles =& $this->document->GetDocfiles();
    if(is_array($docfiles))
      foreach($docfiles as $docfile){
        $docfile->LockFile(11);
      }
    $this->feedbacks[] = 'the document '.$this->doc_properties['properties']['document_number'].' has been locked.';
  }else{
    if($accessCode == 1){
      $this->errors[] = 'This document is checkout';
      break;
    }
  }
  
  //Create the new indice
  if($this->doc_properties['properties']['document_indice_id'] > $this->document->GetDocProperty('document_indice_id') )
    $new_indice_id = $this->doc_properties['properties']['document_indice_id'];
  else
    $new_indice_id = NULL; // Let UpgradeIndice method choose new indice
  global $Manager;
  if( $new_doc_id = $this->document->UpgradeIndice($new_indice_id, $Manager->GetId() ) ){
    $this->feedbacks[] = 'the document ID='.$new_doc_id.' has been created.';
    $onewDocument = new document($this->space, $new_doc_id);
    //Update the new indice with metadata to import
    if( !$onewDocument->UpdateDocInfos($this->doc_properties['properties']) ){
      $this->errors[] = 'Can not update document data='.$accessCode;
    }else{
      $this->feedbacks[] = 'the document ID='.$new_doc_id.' has been updated.';
    }

    //----------------- Update the files
    //Get docfiles
    $odocfile =& $onewDocument->initDocfile();
    if($file_id = $odocfile->GetFileId( $this->file_name , $new_doc_id ) ){ //- Get the id of file from new document
      $odocfile->init($file_id);
      require_once './class/common/fsdata.php';
      $odata = fsdata::_dataFactory($this->file);
      if($odata){ //-----update the file
        $onewDocument->CheckOut(false); //Checkout document without files
        if($odocfile->CheckOutFile(false)){ //Checkout file but without copy of file in wildspace
          if( $onewDocument->CheckIn(false, false) ){ //replace document and checkout files
            $this->feedbacks[] = $this->file_name.' has been updated.';
          }else{
            $this->errors[] = 'Can not checkin the file ='. $this->file_name;
          }
        }else{
          $this->errors[] = 'Can not checkout the file ='. $this->file_name;
        }
      }else{
        $this->errors[] = 'The file '.$this->file_name.' is not in your Wildspace ';
      }
    }else $this->errors[] = 'Can not update the new indice';
  }else $this->errors[] = 'Can not create the new indice';
  return true;
} //End of function

//-----------------------------------------------------------------
//Genere une ligne de formulaire dans le cas du store multiple
//Le principe est de renommer chaque champ du formulaire (y compris les champs issues des metadonnees crees par les utilisateurs)
//et d'y ajouter '[$i]' ou $i est un numero incrementale issue de la boucle de parcours de $_REQUEST['checked'].
//Le formulaire retourne alors des variables tableaux. La designation[1] correspondra au fichier[1] etc.
//function SetCreateDocForm(HTML_QuickForm &$form_collection, $loop_id, $docaction=NULL){
function &SetCreateDocForm($loop_id, $docaction=NULL, $validate=false){
  global $smarty;
  $this->form = new subform('form_'.$loop_id, 'post');
  $defaultRender =& $this->form->defaultRenderer();
  $defaultRender->setFormTemplate($this->subformTemplate);
  $defaultRender->setHeaderTemplate($this->subheaderTemplate);
  $defaultRender->setElementTemplate($this->subelementTemplate);

  $this->form->_requiredNote = '';
  $this->loop_id = $loop_id;
  
  if( count($this->actions) == 1 ){
    if ( isset($this->actions['ignore']) ){
      $disabled = 'DISABLED';
      $readonly = 'READONLY';
      $this->form->addElement('text', 'file['.$loop_id.']', 'File' ,array('readonly', 'value'=>$this->file_name, 'size'=>32));
      $this->form->addElement('text', 'document_number['.$loop_id.']', 'Document_number' , array('readonly', 'size'=>32));
      $this->form->setDefaults(array(
        'document_number['.$loop_id.']' => $this->doc_properties['properties']['document_number'],
        'designation['.$loop_id.']' => $this->doc_properties['properties']['designation'],
      ));
      $this->errorsToElement();
      return $this->form;
    }
  }
  
  $fields = array('document_number', 'designation', 'category_id', 'document_indice_id', 'file_id', 'document_id', 'file');
  
  $this->form->addElement('text', 'file['.$loop_id.']', tra('File') ,array('readonly', 'value'=>$this->file_name, 'size'=>32));
  $mask = DEFAULT_DOCUMENT_MASK;
  $this->form->addRule('file['.$loop_id.']', 'This file name is not valid', 'regex', "/$mask/" , 'server');
  $this->form->addElement('text', 'document_number['.$loop_id.']', tra('document_number') , array('readonly', 'size'=>32));

  if(empty($this->doc_properties['properties']['designation'])) $this->doc_properties['properties']['designation'] = 'undefined';

  $this->form->addElement('hidden', 'loop_id['.$loop_id.']', $loop_id);
  $this->form->addElement('hidden', 'document_id['.$loop_id.']', $this->document_id);
  $this->form->addElement('hidden', 'file_id['.$loop_id.']', $this->file_id);

  $this->form->setDefaults(array(
    'document_number['.$loop_id.']' => $this->doc_properties['properties']['document_number'],
    'designation['.$loop_id.']' => $this->doc_properties['properties']['designation'],
    'category_id['.$loop_id.']' => $this->doc_properties['properties']['category_id'],
  ));

  //Add select document indice
  $params = array(
            'field_multiple' => false,
            'field_name' => 'document_indice_id['.$loop_id.']',
            'field_description' => tra('document_indice_id').' <br /><a href="#" onClick="javascript:setValue(\'document_indice_id['.$loop_id.']\');return false;">'.tra('same for all').'</a>',
            'field_size' => '1',
            'field_required' => true,
            'default_value' => $this->doc_properties['properties']['document_indice_id'],
            'disabled' => $disabled,
  );
  construct_select_document_indice($params , $this->form , $this->container, 'server');
  /*
  if( array_key_exists( 'upgrade_doc', $this->actions ) || array_key_exists( 'create_doc', $this->actions ) ){
    construct_select_document_indice($params , $this->form , $this->container);
  }else{
    $this->form->addElement('select', $params['field_name'], $params['field_description'], array(), array($disabled) );
  }
  */

  $label = tra('Designation').' <br /><a href="#" onClick="javascript:setValue(\'designation['.$loop_id.']\');return false;">'.tra('same for all').'</a>';
  $this->form->addElement('textarea', 'designation['.$loop_id.']' , $label ,array('rows'=>2,'cols'=>32,$disabled,$readonly,'id'=>'designation['.$loop_id.']'));
  $this->form->addRule('designation['.$loop_id.']', tra('is required'), 'required');
  $this->form->addRule('document_number['.$loop_id.']', 'This document name is not valid', 'regex', "/$mask/" , 'server');

  //Add select category
  $params = array(
            'field_multiple' => false,
            'field_name' => 'category_id['.$loop_id.']',
            'field_description' => tra('Category').' <br /><a href="#" onClick="javascript:setValue(\'category_id['.$loop_id.']\');return false;">'.tra('same for all').'</a>',
            'field_size' => '1',
            'return_name' => false,
            'default_value' => $this->doc_properties['properties']['category_id'],
            'disabled' => $disabled,
  );
  construct_select_category($params , $this->form , $this->container, 'server');

//var_dump($this->actions);
//var_dump($docaction);

  //Get fields for custom metadata
  require_once('./class/common/metadata.php');
  $metadata = new docmetadata($this->space);
  $optionalFields = $metadata->GetMetadataLinked( NULL, $this->container->GetId() );
  $smarty->assign( 'optionalFields' , $optionalFields);
  foreach($optionalFields as $field){
    $field['default_value'] = $this->doc_properties['properties'][$field['field_name']];
    $fields[] = $field['field_name'];
    $field['field_name'] = $field['field_name'] . '['.$loop_id.']';
    $field['disabled'] = $disabled;
    if($field['field_type'] != 'date' && $field['field_type'] != 'liveSearch') //the js script to copy properties dont operate with date and livesearch fields
      $field['field_description'] = $field['field_description'].' <br /><a href="#" onClick="javascript:setValue(\''.$field['field_name'].'\');return false;">'.tra('same for all').'</a>';
    construct_element($field , $this->form, 'server'); //function declared in GUI.php
  }
  //$form->addElement('hidden', 'fields[]', $field['field_name']);
  //$smarty->assign('fields', $fields);

  //Create hidden field to retrieve initial action list
  foreach($this->actions as $action=>$description){
    $this->form->addElement('hidden', 'actions['.$loop_id.']['.$action.']', $description);
  }
    //if( !empty($docaction) )  $this->actions[$docaction] = $docaction; //to redisplay the action name when freeze form

  if( empty($docaction) ){ //To set the default value of select
    $tmp = array_keys($this->actions);
    $docaction = $tmp[0];
    unset($tmp);
  }

//var_dump($docaction);
//var_dump($this->actions);

  //Add select action to perform
  $params = array(
            'field_multiple' => false,
            'field_name' => 'docaction['.$loop_id.']',
            'field_description' => tra('Action').' <br /><a href="#" onClick="javascript:setValue(\'docaction['.$loop_id.']\');return false;">'.tra('same for all').'</a>',
  			'field_size' => '1',
            'return_name' => false,
            'default_value' => $docaction, //Default value is the first key of the action array
            'disabled' => $disabled,
  );
  construct_select($this->actions , $params , $this->form);

  //Apply new filters to the element values
  $this->form->applyFilter('__ALL__', 'trim');

  foreach($fields as $field_name){
    $this->form->addElement('hidden', 'fields['.$loop_id.'][]', $field_name);
  }

  if($validate){
    $this->ValidateForm($docaction);
  }

  $this->errorsToElement();
  $this->feedbackToElement();

  return $this->form;

} //End of function

//-----------------------------------------------------------------
// generate html for errors
function errorsToElement(){
  foreach($this->errors as $error){
    $this->form->addElement('hidden', 'errors['.$this->loop_id.'][]', $error);
    $error_text .= str_replace('{error}', $error, $this->errorTemplate);
  }
  $this->form->addElement('static', '', $error_text);
} //End of function

//-----------------------------------------------------------------
// generate html for feedback
function feedbackToElement(){
  foreach($this->feedbacks as $feedback){
    $this->form->addElement('hidden', 'feedbacks['.$this->loop_id.'][]', $feedback);
    $feedback_text .= str_replace('{feedback}', $feedback, $this->feedbackTemplate);
  }
  $this->form->addElement('static', '', $feedback_text);
} //End of function

//-----------------------------------------------------------------
// validate the form
function ValidateForm($docaction){
  global $smarty;

  if ( $this->isFrozen ){
    $this->form->addElement('hidden', 'isFrozen['.$this->loop_id.']', '1');
    $this->form->freeze();
    return true;
  }

  if ( $this->form->validate() ){
    switch($docaction){
      case "ignore": //Ignore
        return true;
        break;
      case "update_doc": //Update the document only
        if ( $this->update_doc() ){  //call function
          $this->form->addElement('hidden', 'isFrozen['.$this->loop_id.']', '1');
          $this->form->freeze(); //and freeze it
          return true;
        }
        break;
      case "update_doc_file": //Update the document and the file if specified
        if ( $this->update_doc_file() ){  //call function
          $this->form->addElement('hidden', 'isFrozen['.$this->loop_id.']', '1');
          $this->form->freeze(); //and freeze it
          return true;
        }
        break;
      case "update_file": //Update the associated file only
        if ( $this->update_file() ){  //call function
          $this->form->addElement('hidden', 'isFrozen['.$this->loop_id.']', '1');
          $this->form->freeze(); //and freeze it
          return true;
        }
        break;
      case "add_file": //Add the file to the document
        if ( $this->add_file() ){  //call function
          $this->form->addElement('hidden', 'isFrozen['.$this->loop_id.']', '1');
          $this->form->freeze(); //and freeze it
          return true;
        }
        break;
      case "create_doc": //Create a new document
        if ( $this->create_doc() ){  //call function
        //if ( true ){
          $this->form->addElement('hidden', 'isFrozen['.$this->loop_id.']', '1');
          $this->form->freeze(); //and freeze it
          return true;
        }
        break;
      case "upgrade_doc": //Create a new indice and update the files if specifed
        if ( $this->upgrade_doc() ){  //call function
          $this->form->addElement('hidden', 'isFrozen['.$this->loop_id.']', '1');
          $this->form->freeze(); //and freeze it
          return true;
        }
        break;
    } //End of switch
  }

  return false;

} //End of function

} //End of class

//---------------------------
function serializeProperties($properties, $form=''){
//FIX ME : add $form object in parameter
  foreach($properties as $key=>$val){
    $properties[$key] = $val;
  }

  foreach($properties as $key=>$val){
    if(is_array($val)){
      if( is_a($form,'HTML_QuickForm') ){
        if($form->GetElementType($key) == 'date')
          $properties[$key] = mktime($val['h'], $val['m'], $val['s'], $val['M'], $val['d'], $val['Y']);
      }else $properties[$key] = implode('#' , $val);
    }
  }

  return $properties;

} //End of function

?>
