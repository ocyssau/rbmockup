<?php
require_once "HTML/QuickForm.php"; //Librairy to easily create forms

class Ranchbe_HTML_QuickForm extends HTML_QuickForm{

function __construct($formName='', $method='post', $action='', $target='', $attributes=null, $trackSubmit = false){
  parent::HTML_QuickForm($formName, $method, $action, $target, $attributes, $trackSubmit);
  $defaultRender =& $this->defaultRenderer();
  $defaultRender->setElementTemplate(
  '<tr>
    <td class="formcolor">
      <!-- BEGIN required --><span style="color: #ff0000">*</span><!-- END required -->
      {label}
    </td>
    <td class="formcolor">
      <!-- BEGIN error --><span style="color: #ff0000">{error}</span><br /><!-- END error -->
      {element}
    </td>
  </tr>'
  );
  
  $defaultRender->setHeaderTemplate(
  '<tr>
    <td colspan=2 style="align=center">
      <h1>{header}</h1>
    </td>
  </tr>'
  );
  
  global $smarty;
  $ranchbe_html_header = $smarty->fetch('header.tpl');
  $defaultRender->setFormTemplate(
  $ranchbe_html_header.
  '
  <div id="tiki-center">
  <form{attributes}>
    <table class="normal">
      {content}
    </table>
  </form>
  </div>
  </body>
  ');
} //End of method

} //End of class

?>
