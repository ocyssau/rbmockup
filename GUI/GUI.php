<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once "HTML/QuickForm.php"; //Librairy to easily create forms
require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm
require_once './lib/Date/date.php';

function construct_select($list , $params , $form, $validation = 'client'){
//$list array
//$params array
  /*
  $params = array(
            'field_where',
            'field_name',
            'field_description'
            'default_value',
            'field_multiple',
            'return_name',
            'field_required',
            'adv_select',
            'display_both',
            'disabled',
            'field_id',
  );
  */

  //$params['display_both'] = true;
  //$params['adv_select'] = true;

  if(empty($params['field_id']))
    $params['field_id'] = $params['field_name'];

  if(!empty($params['default_value']))
    $form->setDefaults(array(
      $params['field_name'] => $params['default_value'],
    ));

/*  else 
    $form->setDefaults(array(
      $params['field_name'] => NULL,
    ));*/

  if (!isset($params['field_description']))
    $params['field_description'] = $params['field_name'];
  
  //Construct array for selection set
  if($params['adv_select'] == 0)
    $SelectSet[NULL] = ''; //Leave a blank option for default none selected. Not useful for advanced select

  if(is_array($list)){
  foreach ( $list as $key=>$values )
    if($params['display_both'])
      $SelectSet[$key] = $key.'-'.$values;
    else
      $SelectSet[$key] = $values;
  }else $SelectSet = array();

  if($params['adv_select'] == 1){
    $form->removeAttribute('name');        // XHTML compliance
    //Construct object for advanced select
    require_once 'HTML/QuickForm/advmultiselect.php';
    $select =& $form->addElement('advmultiselect', $params['field_name'], tra($params['field_description']), $SelectSet,
                               array('size' => 5,
                                     'class' => 'pool', 'style' => 'width:300px;'
                                    )
    );
    $select->setLabel(array(tra($params['field_description']), 'Available', 'Selected'));

    $select->setButtonAttributes('add',    array('value' => 'Add >>',
                                               'class' => 'inputCommand'
    ));
    $select->setButtonAttributes('remove', array('value' => '<< Remove',
                                               'class' => 'inputCommand'
    ));
    
    /*if (isset($_POST[$params['field_name']])) {
        $form->setDefaults(array($params['field_name'] => $_POST[$params['field_name']]));
    }*/
  }else{
    //Construct object for normal select
    $select =& $form->addElement('select', $params['field_name'] , tra($params['field_description']), $SelectSet, array($params['disabled'],'id'=>$params['field_id']) );

    //Set the size
    if(isset($params['field_size']))
      $select->setSize($params['field_size']);
    else $select->setSize(5);

    //Enable multi selection
    if($params['field_multiple'])
      $select->setMultiple(true);
    else $select->setMultiple(false);
  }

  //Add required rule
  if($params['field_required'])
    $form->addRule($params['field_name'], tra('is required'), 'required', $validation);

} // End of method

//------------------------------------------------------------------------------
function construct_select_partner($params , $form, $validation = 'client'){
//$list array
//$params array
/*
  $params = array(
            'field_where',
            'field_name',
            'field_description'
            'default_value',
            'field_multiple',
            'return_name',
            'field_required',
            'field_size',
            'sort_field' => '',
            'sort_order' => '',
*/  

  $params['select'] = array('partner_id' , 'partner_number');
  if(!empty($params['field_where']))
    $params['where'] = array($params['field_where']);
  $params['sort_order']='ASC';
  $params['sort_field']='partner_number';

  require_once './class/partner.php'; //Class to manage the partners
  $partnersManager = new partner(); //Create new manager
  $partnerList = $partnersManager->GetAllPartners($params);

  //Construct array for set partner select options
  if(is_array($partnerList)){
    if(!$params['return_name'])
      foreach ( $partnerList as $key=>$values )
        {$partnerSelectSet[$values['partner_id']] = $values['partner_number'];}
    else
      foreach ( $partnerList as $key=>$values )
        {$partnerSelectSet[$values['partner_number']] = $values['partner_number'];}
  }else $partnerSelectSet=array();

  construct_select($partnerSelectSet , $params , $form, $validation);

} // End of method

//------------------------------------------------------------------------------
function construct_select_doctype($params , $form, $validation = 'client'){
//$list array
//$params array
/*
  $params = array(
            'field_where',
            'field_name',
            'field_description'
            'default_value',
            'field_multiple',
            'return_name',
            'field_required',
            'field_size',
            'sort_field' => '',
            'sort_order' => '',
*/  

  if(!empty($params['field_where']))
  $p['where'][] = $params['field_where'];
  $p['select'] = array('doctype_id' , 'doctype_number');
  $p['sort_order']='ASC';
  $p['sort_field']='doctype_number';

  require_once './class/doctype.php'; //Class to manage the partners
  $doctypesManager = new doctype(); //Create new manager
  $doctypeList = $doctypesManager->GetAll($p);

  if(!isset($params['return_name'])) $params['return_name'] = true;

  //Construct array for set partner select options
  if(!$params['return_name']){
    foreach ( $doctypeList as $key=>$values )
      {$doctypeSelectSet[$values['doctype_id']] = $values['doctype_number'];}
  }else{
    foreach ( $doctypeList as $key=>$values )
      {$doctypeSelectSet[$values['doctype_number']] = $values['doctype_number'];}
  }

/*
echo $params['default_value'];

  if($params['field_multiple'] && isset($params['default_value'])){
      $params['default_value'] = explode('#' , $params['default_value']);
  }
*/

  construct_select($doctypeSelectSet , $params , $form, $validation);

} // End of method

//------------------------------------------------------------------------------
function construct_select_document_indice($params , $form , container $Manager, $validation = 'client'){
//$list array
//$params array
//$manager is the object from to get the method GetIndices
/*
  $params = array(
            'field_where',
            'field_name',
            'field_description'
            'default_value',
            'field_multiple',
            'return_name',
            'field_required',
            'field_size',
            'sort_field' => '',
            'sort_order' => '',
*/  

  //Get list of indice
  //$document =& $Manager->initDoc();
  require_once('class/common/document.php');
  $document = new document($Manager->space);
  if(empty($params['default_value'])) $params['default_value'] = 1;
  $indice_list = $document->GetIndices(0);
  construct_select($indice_list , $params , $form, $validation );

} // End of method

//------------------------------------------------------------------------------
function construct_select_user($params , $form, $validation = 'client'){
//$list array
//$params array
//$manager is the object from to get the method GetIndices
/*
  $params = array(
            'field_where',
            'field_name',
            'field_description'
            'default_value',
            'field_multiple',
            'return_name',
            'field_required',
            'field_size',
            'sort_field' => '',
            'sort_order' => '',
*/  

  global $LUA;
  $LUparams = array ( 'container' => 'auth',
                    'orders' => array('handle' => 'ASC'),
                    'limit' => 9999,
                    'offset' => 0,
                    'fields' => array('handle','auth_user_id')
                    );
  $list = $LUA->getUsers($LUparams);

  //Construct array for set partner select options
  if(!$params['return_name']){
    foreach ( $list as $key=>$values )
      {$SelectSet[$values['auth_user_id']] = $values['handle'];}
  }else{
    foreach ( $list as $key=>$values )
      {$SelectSet[$values['handle']] = $values['handle'];}
  }

  construct_select($SelectSet , $params , $form, $validation);

} // End of method

//------------------------------------------------------------------------------

function construct_select_process($params , $form, $validation = 'client'){
//$list array
//$params array
//$manager is the object from to get the method GetIndices
/*
  $params = array(
            'field_where',
            'field_name',
            'field_description'
            'default_value',
            'field_multiple',
            'return_name',
            'field_required',
            'field_size',
            'sort_field' => '',
            'sort_order' => '',
*/  

  require_once ('./lib/Galaxia/ProcessMonitor.php');
  global $processMonitor;
  if(isset($processMonitor))
    $processList = $processMonitor->monitor_list_processes(0, 9999 , 'lastModif_desc', '' , $params['field_where']);

  if(!isset($params['return_name'])) $params['return_name'] = false;

  if(!$params['return_name'])
    if(is_array($processList['data']))
      foreach($processList['data'] as $pid=>$values)
        {$processSelectSet[$pid] = $values['name'].'_'.$values['version'];}
  else
    if(is_array($processList['data']))
      foreach($processList['data'] as $pid=>$values)
        {$processSelectSet[$values['name'].'_'.$values['version']] = $values['name'].'_'.$values['version'];}
  construct_select($processSelectSet , $params , $form , $validation);

} // End of method

//------------------------------------------------------------------------------

function construct_select_category($params , &$form , container &$Manager, $validation = 'client'){
//$list array
//$params array
//If $all=true return all categories else return categories linked to container only
/*
  $params = array(
            'field_where',
            'field_name',
            'field_description'
            'default_value',
            'field_multiple',
            'return_name',
            'field_required',
            'field_size',
            'sort_field' => '',
            'sort_order' => '',
*/

  if(!isset($params['return_name'])) $params['return_name'] = false;
  
  //Get list of categories
  $ocategory =& $Manager->initCategory();
  //$categories = $ocategory->GetCategories($params);
  //$categories = $ocategory->GetLinks($params);
  $categories = $ocategory->GetCategories($params);

  if(!$params['return_name'])
    foreach ($categories as $value ) //Rewrite result array for quickform convenance
      {$selectedCat[$value['category_id']] = $value['category_number'];}
  else
    foreach ($categories as $value ) //Rewrite result array for quickform convenance
      {$selectedCat[$value['category_number']] = $value['category_number'];}

  construct_select($selectedCat , $params , $form , $validation);

} // End of method

//------------------------------------------------------------------------------

function construct_select_container($params , &$form , container &$Manager, $validation = 'client'){
//$list array
//$params array
//$manager is the object from to get the container list
/*
  $params = array(
            'field_where',
            'field_name',
            'field_description'
            'default_value',
            'field_multiple',
            'return_name',
            'field_required',
            'field_size',
            'sort_field' => '',
            'sort_order' => '',
*/

  if(!isset($params['return_name'])) $params['return_name'] = false;
  //Get list of containers
  $p=array(
        'sort_field' => $Manager->SPACE_NAME . '_number',
        'sort_order' => 'ASC',
        'select' => array($Manager->SPACE_NAME . '_number', $Manager->SPACE_NAME . '_id')
        );
  $containers = $Manager->GetAll($p);

  if(!$params['return_name'])
    foreach ($containers as $value ) //Rewrite result array for quickform convenance
      {$selectedCont[$value[$Manager->SPACE_NAME . '_id']] = $value[$Manager->SPACE_NAME . '_number'];}
  else
    foreach ($categories as $value ) //Rewrite result array for quickform convenance
      {$selectedCont[$value[$Manager->SPACE_NAME . '_number']] = $value[$Manager->SPACE_NAME . '_number'];}

  construct_select($selectedCont , $params , $form , $validation);

} // End of method

//------------------------------------------------------------------------------
function construct_select_propset($params , &$form , propset &$propset, $validation = 'client'){
//$list array
//$params array
//$manager is the object from to get the method GetIndices
/*
  $params = array(
            'field_where',
            'field_name',
            'field_description'
            'default_value',
            'field_multiple',
            'return_name',
            'field_required',
            'field_size',
            'sort_field' => '',
            'sort_order' => '',
*/

  if(!isset($params['return_name'])) $params['return_name'] = false;
  //Get list of categories
  $list = $propset->GetPropset();

  if(!$params['return_name'])
    foreach ($list as $value ) //Rewrite result array for quickform convenance
      {$select[$value['propset_id']] = $value['propset_name'];}
  else
    foreach ($list as $value ) //Rewrite result array for quickform convenance
      {$select[$value['propset_name']] = $value['propset_name'];}

  construct_select($select , $params , $form , $validation);

} // End of method

//------------------------------------------------------------------------------
function construct_select_property($params , &$form , metadata &$property, $validation = 'client'){
//$list array
//$params array
//$manager is the object from to get the method GetIndices
/*
  $params = array(
            'field_where',
            'field_name',
            'field_description'
            'default_value',
            'field_multiple',
            'return_name',
            'field_required',
            'field_size',
            'sort_field' => '',
            'sort_order' => '',
*/

  if(!isset($params['return_name'])) $params['return_name'] = false;
  //Get list of categories
  $p = array('select'=>array('property_id','field_description') );
  $list = $property->GetMetadata($p);

  if(!$params['return_name'])
    foreach ($list as $value ) //Rewrite result array for quickform convenance
      {$select[$value['property_id']] = $value['field_description'];}
  else
    foreach ($list as $value ) //Rewrite result array for quickform convenance
      {$select[$value['field_description']] = $value['field_description'];}

  construct_select($select , $params , $form , $validation);

} // End of method

//------------------------------------------------------------------------------

function construct_select_date($params , $form, $validation = 'client'){
//$list array
//$params array
//$manager is the object from to get the method GetIndices
/*
  $params = array(
    ["field_name"]=>string
    ["field_description"]=>string
    ["field_required"]=>string
    ["default_value"]=>string
    ["date_format"]=>string / default 'dMY'
    ["date_language"]=>string / default 'fr'
*/

  if(USE_JSCALENDAR === true){ //Use or not the jscalendar script for select date.
    
    require_once 'HTML/QuickForm/jscalendar.php'; //Lib to create date selection
    
    if(isset($params['date_format'])){
      $optionsjsc['daFormat'] = $params['date_format']; //Displayed date format. '%Y/%m/%d' by default
    }
    if(isset($params['daFormat']))
      $optionsjsc['daFormat'] = $params['daFormat']; //Displayed date
    if(isset($params['ifFormat']))
      $optionsjsc['ifFormat'] = $params['ifFormat']; //Format of return date. Timestamp by default
    if(isset($params['displayArea']))
      $optionsjsc['displayArea'] = $params['displayArea'];
    if(isset($params['button']))
      $optionsjsc['button'] = $params['button'];
    if(isset($params['showsTime']))
      $optionsjsc['showsTime'] = $params['showsTime'];

    if(isset($params['default_value'])){
      $optionsjsc['date'] = $params['default_value'];
      $attributes['value'] = $params['default_value'];
    }
    
    //If ifFormat=%s (to return timestamp), jscalendar return always to day date and ignore previous selected date when select a date
    //So we must set a format for ifFormat and create an hidden field to return timestamp.
    $optionsjsc['timestamp'] = true; //to return timestamp in an hidden field
    $optionsjsc['ifFormat'] = '%Y/%m/%d %H:%M:%S'; //Format of return date. Timestamp by default
    
    if(!isset($params['field_description']))
      $params['field_description'] = $params['field_name'];
    
    $attributes['size'] = $params['size'];
    
    $calendar =& $form->addElement('jscalendar', $params['field_name'], tra($params['field_description']), $optionsjsc , $attributes);
    
    if($params['field_required'])
      $form->addRule($params['field_name'], tra('is required'), 'required', null, $validation);
    
    if(isset($params['showsTime']))
      $calendar->_config['daFormat'] = LONG_DATE_FORMAT;
    
  }else{
    
    //Option to manage display of a date input
    if(!isset($params['language']))
      $params['language'] = LANG;
    if(!isset($params['date_format']))
      $params['format'] = 'dMY'; else $params['format'] = $params['date_format'];
    if(!isset($params['default_value']))
      $params['default_value'] = time();
    if(!isset($params['field_description']))
      $params['field_description'] = $params['field_name'];
    
    //Option to manage display of a date input
    $options = array(
        'language'  => $params['language'],
        'format'    => $params['format'],
        'minYear'   => date('Y'),
        'maxYear'   => date('Y' , time() + 20*365*24*3600),
    );
    
    $form->setConstants(array( //Set value for date
     $params['field_name'] => $params['default_value'],
    ));
    
    $form->addElement('date', $params['field_name'], tra($params['field_description']), $options);
    
  //Add required rule
    if($params['field_required'])
      $form->addRule($params['field_name'], tra('is required'), 'required', null, $validation);
  }

} // End of method

//------------------------------------------------------------------------------

function construct_element($field , $form, $validation = 'client'){
//$field array
/*
  $field = array(
    ["field_name"]=>string
    ["field_description"]=>string
    ["field_type"]=>string
    ["field_regex"]=>string
    ["field_required"]=>string
    ["field_multiple"]=>string
    ["field_size"]=>string
    ["return_name"]=>string
    ["field_list"]=>string
    ["field_where"]=>string
    ["field_regex_message"]=>string
    ["default_value"]=>string
    ["sort_order"]=>string
    ["sort_field"]=>string
    ["table_name"]=>string
    ["field_for_value"]=>string
    ["field_for_display"]=>string
    ["date_format"]=>string
    ["date_language"]=>string / default 'fr'
    ["display_both"]=>bool / default false
    ["disabled"]=>string
*/

//var_dump($field);

  global $Manager;

  if(!isset($field['field_name']))
    return false;
  if(!isset($field['field_description']))
    $field['field_description'] = $field['field_name'];
  if(!isset($field['field_type']))
    return false;
  if(empty($field['field_id']))
    $field['field_id'] = $field['field_name'];

  switch ( $field['field_type'] ) {

  case 'text':
    if(!isset($field['field_size']) || is_null($field['field_size']))
      $field['field_size'] = 20;
    $form->addElement('text', $field['field_name'] , $field['field_description'] ,array('value'=>$field['default_value'], 'size'=>$field['field_size'] , 'maxlength'=>$field['field_size'], $field['disabled'], 'id'=>$field['field_id'] ) );
    $form->addRule($field['field_name'] , tra('should be less than or equal to') .' '. $field['field_size'] . ' characters', 'maxlength', $field['field_size'] , $validation);
    if(!empty($field['field_regex'])){ //Add rule with regex
      if(empty($field['field_regex_message'])) $field['field_regex_message'] = tra('invalid format');
      $form->addRule($field['field_name'], $field['field_regex_message'], 'regex', '/'.$field['field_regex'].'/', $validation);
    }
    if($field['field_required']){ //Add rule with require option
      $form->addRule($field['field_name'], tra('is required'), 'required', null, $validation);
    }
    break;

  case 'long_text':
    if(!isset($field['field_size']))
      $field['field_size'] = 20;
    $form->addElement('textarea', $field['field_name'] , $field['field_description'] ,array('value'=>$field['default_value'], 'cols'=>$field['field_size'], $field['disabled'], 'id'=>$field['field_id'] ) );
    if(!empty($field['field_regex'])) //Add rule with regex
      if(empty($field['field_regex_message'])) $field['field_regex_message'] = tra('invalid format');
      $form->addRule($field['field_name'], $field['field_regex_message'], 'regex', '/'.$field['field_regex'].'/m', $validation);
    if($field['field_required']) //Add rule with require option
      $form->addRule($field['field_name'], tra('is required'), 'required', null, $validation);
    break;

  case 'html_area':
    //http://www.nabble.com/HTML-QuickForm-elments-for-HTMLArea-(xinha),-FCKeditor-and-jscalendar-td4523180.html
    if(!isset($field['field_size'])) //set default value
      $field['field_size'] = 20; //for number of lines, col is fixed to 80

    require_once 'HTML/QuickForm/htmlarea.php';
    $options = array('url' => 'lib/htmlarea/', 'lang' => LANG, 'cssURL' => 'style/default/htmlarea.css', $field['disabled']);
    $attributes = array('rows' => $field['field_size'], 'cols' => 80);
    $config = array('width' => '500px', 'height' => '400px', 'imgURL'=>'lib/htmlarea/images/', 'popupURL'=>'lib/htmlarea/popups/');

    $form->addElement('htmlarea', $field['field_name'], $field['field_description'], $options, $attributes);
    $form->setDefaults(array($field['field_name'] => $field['default_value']));

    $htmlarea = &$form->getElement($field['field_name']);
    $htmlarea->setURL('lib/htmlarea/');
    $htmlarea->setLang(LANG);
    $htmlarea->setConfig($config);
    $htmlarea->setConfig('statusBar', true);
    $htmlarea->hideButton('popupeditor');

    //$htmlarea->registerPlugin('CSS', $css_options);
    //$htmlarea->registerPlugin('TableOperations');

    break;

  case 'partner':
    construct_select_partner($field , $form);
    break;

  case 'doctype':
    construct_select_doctype($field , $form);
    break;

  case 'document_indice':
    global $Manager;
    construct_select_document_indice($field , $form , $Manager);
    break;

  case 'container_indice':
    break;

  case 'user':
    construct_select_user($field , $form);
    break;

  case 'process':
    construct_select_process($field , $form);
    break;

  case 'category':
    global $Manager;
    construct_select_category($field , $form, $Manager);
    break;

  case 'date':
    construct_select_date($field , $form);
    break;

  case 'integer':
    if(!isset($field['field_size']))
      $field['field_size'] = 20;
    $form->addElement('text', $field['field_name'] , $field['field_description'] ,array('value'=>$field['default_value'], 'size'=>$field['field_size'], 'id'=>$field['field_id'] ) );
    if($field['field_required']) //Add rule with require option
      $form->addRule($field['field_name'], tra('is required'), 'required', null, $validation);
    $form->addRule($field['field_name'],tra('should be numeric'), 'numeric', null, $validation);
  break;

  case 'decimal':
    if(!isset($field['field_size']))
      $field['field_size'] = 20;
    $form->addElement('text', $field['field_name'] , $field['field_description'] ,array('value'=>$field['default_value'], 'size'=>$field['field_size'], $field['disabled'], 'id'=>$field['field_id'] ) );
    if($field['field_required']) //Add rule with require option
      $form->addRule($field['field_name'], tra('is required'), 'required', null, $validation);
    $form->addRule($field['field_name'], tra('should be numeric'), 'numeric', null, $validation);
  break;

  case 'select':
     $list = explode('#' , $field['field_list']);
     $list = array_combine($list , $list);
     construct_select($list , $field , $form );
   break;

  case 'selectFromDB':
    $pget['sort_field'] = $field['field_for_display'];
    $pget['sort_order'] = 'ASC';
    $pget['maxRecords'] = 10000;
    $pget['select'] = array($field['field_for_value'] , $field['field_for_display']);
    $list = $Manager->Get($field['table_name'] , $pget , 'all' , false);
    
    //Construct array for set select options
    if(!$field['return_name']){
      foreach ( $list as $key=>$values )
        {$SelectSet[$values[$field['field_for_value']]] = $values[$field['field_for_display']];}
    }else{
      foreach ( $list as $key=>$values )
        {$SelectSet[$values[$field['field_for_display']]] = $values[$field['field_for_display']];}
    }

    construct_select($SelectSet , $field , $form);
    break;

  case 'liveSearch':
    require_once 'HTML/QuickForm/livesearch_select.php';
    $form->addElement('livesearch_select', $field['field_name'], tra($field['field_description']),
                     array(
                          'elementId' => 'qflssearch', //element id, name must be same that method in .class.php
//                          'callback' => array('Test', 'getTestName'),//callback function to retrieve value from ID selection
//                          'dbh' => $dbranchbe,//optional handler for callback function
                          'style' => '',//optional class for style not set or '' ==> default
                          'divstyle' => '',//optional class for style not set or '' ==> default
                          'ulstyle' => '',//optional class for style not set or '' ==> default
                          'listyle' => '',//optional class for style not set or '' ==> default
                          'searchZeroLength' => 1, //enable the search request with 0 length keyword(Default 0)
                          'buffer' => 350, //set the interval single buffer send time (ms)
                          'printStyle' => 1, //anything != 0 will render css inline(Default 1), 0 => the default style will not be rendered, you should put it in your style.css(XHTML fix)
                          'autoComplete' => 1, //if 0 the autocomplete attribute will not be set. Default not set;
                          'autoserverPath' => '', //path to auto_server.php file with trailing slash NOTE: check path for all files included in autoserver.php
                          'query' => 'SELECT '.$field['field_for_display'].', '.$field['field_for_value'].' FROM '.$field['table_name'].' WHERE '.$field['field_for_display']." LIKE '%search_term%%'".' ORDER BY '.$field['field_for_display'].' ASC',
                          ),
                          array('size' => $field['field_size'], $field['disabled'] )
                      );
   break;

  default:
  } //End of switch

//Set the default value of the field from field['default_value']
// -- if its a multiple field, explode the string value into array
if($field['field_multiple'] && isset($field['default_value'])){
    $field['default_value'] = explode('#' , $field['default_value']);
}
if (isset($field['default_value']))
  $form->setDefaults(array(
    $field['field_name'] => $field['default_value'],
  ));
else
  $form->setDefaults(array(
    $field['field_name'] => '',
  ));

} // End of method


?>
