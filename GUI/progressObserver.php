<?php
require_once('./class/common/observer.php');
class progressObserver extends rb_observer{

  function __construct(HTML_Progress2 &$progressBar){
    $this->_progressBar =& $progressBar;
    $this->_incrementIsSet = false;
  }

  function notify($event, &$obj){
    $this->_eventName = $event;
    $this->_object =& $obj;

    switch($this->_eventName){
      case('onExtractBegin'):
        $this->_progressBar->setIndeterminate(true);
        $this->_progressBar->addLabel(HTML_PROGRESS2_LABEL_TEXT, $this->_eventName );
        $this->_progressBar->setLabelAttributes('LABEL1',array('value' => 'Begin extraction'));
        //$this->_progressBar->moveStep(100);
        $this->_progressBar->sleep();
        echo '<b>Begin the extraction...</b><br />';
        break;
      case('onExtractEnd'):
        $this->_progressBar->setIndeterminate(true);
        $this->_progressBar->addLabel(HTML_PROGRESS2_LABEL_TEXT, $this->_eventName );
        $this->_progressBar->setLabelAttributes('LABEL1',array('value' => 'Files are extracted'));
        //$this->_progressBar->moveStep(100);
        $this->_progressBar->sleep();
        echo '<b>Extraction is end...</b><br />';
        break;

      case('onUpdateFile'):
      case('onCreateFile'):
        if(!$this->_incrementIsSet){
          echo '<b>Start database update...</b><br />';
          $this->_progressBar->setValue(0);
          $this->_progressBar->setIncrement(1);
          $this->_progressBar->setMaximum($obj->unpack_iterator);
          $this->_incrementIsSet = true;
        }

      case('onUpdateFile'):
        $this->_progressBar->addLabel(HTML_PROGRESS2_LABEL_TEXT, $this->_eventName );
        $this->_progressBar->setLabelAttributes('LABEL1',array('value' => 'Update file '.$obj->current_file));
        $this->_progressBar->setIndeterminate(false);
        $this->_progressBar->moveNext();
        break;
      case('onCreateFile'):
        $this->_progressBar->addLabel(HTML_PROGRESS2_LABEL_TEXT, $this->_eventName );
        $this->_progressBar->setLabelAttributes('LABEL1',array('value' => 'Create file '.$obj->current_file));
        $this->_progressBar->setIndeterminate(false);
        $this->_progressBar->moveNext();
        break;
      default:
        return true;
    }
  return $this->_eventName;
  } //End of method

} //End of class

?>
