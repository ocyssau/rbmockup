<?php
require_once './conf/ranchbe_setup.php';//ranchBE configuration
require_once './class/user/userPreferences.php'; //Class to manage user preferences

require_once "GUI/Ranchbe_HTML_QuickForm.php"; //Librairy to easily create forms

//Construct the form with QuickForm lib
$form = new Ranchbe_HTML_QuickForm('form', 'post');

$form->addElement('header', null, tra('User preferences') );

$preference = new userPreferences($usr);
$prefs = $preference->GetPreferences();

$form->setDefaults(array(
  'css_sheet'               => $prefs['css_sheet'],
  'lang'                    => $prefs['lang'],
  'long_date_format'        => $prefs['long_date_format'],
  'short_date_format'       => $prefs['short_date_format'],
  'hour_format'             => $prefs['hour_format'],
  //'time_zone'               => $prefs['time_zone'],
  'wildspace_path'          => $prefs['wildspace_path'],
  'max_record'              => $prefs['max_record'],
));

//Construct select long_date_format
$long_date_format_list = array(
  '%d-%m-%Y %H:%M:%S'=>'28-12-1969 12:45:20',
  '%d/%m/%Y %Hh%Mmn%Ss'=>'28/12/1969 12h45mn20s',
  '%d/%m/%Y %Hh%M'=>'28/12/1969 12h45',
  '%d/%m/%y %Hh%M'=>'28/12/69 12h45',
  '%Y-%m-%d %H:%M:%S'=>'1969-12-28 12:45:20',
  '%d-%m-%Y'=>'29-12-1969',
  '%Y-%m-%d'=>'1969-12-28',
  'default' => tra('default'),
);
$select =& $form->addElement('select', 'long_date_format', tra('long_date_format'), $long_date_format_list );
$select->setSize(1);
$select->setMultiple(false);

//Construct select short_date_format
$short_date_format_list = array(
  '%d-%m-%Y'=>'28-12-1969',
  '%Y-%m-%d'=>'1969-12-28',
  '%d-%m-%y'=>'28-12-69',
  '%y-%m-%d'=>'69-12-28',
  'default' =>tra('default'),
);
$select =& $form->addElement('select', 'short_date_format', tra('short_date_format'), $short_date_format_list );
$select->setSize(1);
$select->setMultiple(false);

//Construct select css_sheet
$css_sheet_list = glob('styles/*.css');
$css_sheet_list = array_combine($css_sheet_list,$css_sheet_list);
$css_sheet_list['default'] = tra('default');
$select =& $form->addElement('select', 'css_sheet', tra('css_sheet'), $css_sheet_list );
$select->setSize(1);
$select->setMultiple(false);

//Construct select lang
$lang_list = array('fr'=>tra('French'),'en'=>tra('English'),'default'=>tra('default'));
$select =& $form->addElement('select', 'lang', tra('lang'), $lang_list );
$select->setSize(1);
$select->setMultiple(false);

/*
//Construct select timezone
$time_zone_list = array('UMT+1','UMT+2','UMT+3');
$time_zone_list = array_combine($time_zone_list,$time_zone_list);
$time_zone_list['default'] = tra('default');
$select =& $form->addElement('select', 'time_zone', tra('time_zone'), $time_zone_list );
$select->setSize(1);
$select->setMultiple(false);
*/

//Construct select max_record
$max_record_list = array(50,100,200,300,400,500);
$max_record_list = array_combine($max_record_list,$max_record_list);
$max_record_list['default'] = tra('default');
$select =& $form->addElement('select', 'max_record', tra('max_record'), $max_record_list );
$select->setSize(1);
$select->setMultiple(false);

$form->addElement('submit', 'submit', 'Validate');

// Try to validate the form
if ($form->validate()){
  $form->process('updatePrefs', true);
} //End of validate form

$form->display();
$preference->error_stack->checkErrors();

function updatePrefs($prefs){
  global $preference;
  //var_dump($prefs);
  $preference->SetPreference('css_sheet', $prefs['css_sheet']);
  $preference->SetPreference('lang', $prefs['lang']);
  $preference->SetPreference('long_date_format', $prefs['long_date_format']);
  $preference->SetPreference('short_date_format', $prefs['short_date_format']);
  //$preference->SetPreference('time_zone', $prefs['time_zone']);
  $preference->SetPreference('max_record', $prefs['max_record']);
  $preference->Freeze();
}

?>
