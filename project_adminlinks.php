<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//TODO: REVOIR LA GESTION DES ERREURS

require_once './conf/ranchbe_setup.php';

$area_id = $projectManager->AREA_ID;

if (!isset($_REQUEST['flag'])) $_REQUEST['flag'] = '';

//Manage actions
switch ($_REQUEST['action']){

//---------------------------------------------------------------------

  case 'suppressLink':   //Suppress a link
  check_ticket('project_links', $area_id);
   foreach ($_REQUEST["checked"] as $type => $values){
     foreach ( $values as $link_object_id ) {
       $projectManager->RemoveLink( $project_id, $link_object_id , $type);
     } //End of loop
   }
     $_REQUEST['action'] = 'listLinks'; //re-assign action and...
     include 'project_adminlinks.php'; //...re-include current script to update the list display

  break; //End of link workitem

//---------------------------------------------------------------------
  case 'listLinks':   //List links of a project
    //Include definition of the code for manage the pagination and filters
    //$default_sort_field = 'group_define_name';    //Default value of the field to sort
    //$default_sort_order = 'ASC'; //Default value of the order ASC = ascendant, DESC = descandant
    //cancel from V0.4.3> include('filterManager.php');

    //Get object linked to project with detail on childs objects
    $Objectlist = $projectManager->GetLinkChildsDetail($project_id);
    $smarty->assign_by_ref('list', $list);

    //Include generic definition of the code for manage the pagination. $list must set...
    //include('paginationManager.php');

    //var_dump($Objectlist);

    //Get detail on each object
    if(is_array($Objectlist))
    foreach($Objectlist as $object){
      //$type = $object['object_type'];
      $type = $object['object_class'];
      switch ( $type ) {
      case 'workitem':
        $list[] = array ( 'number' => $object['workitem_number'] , 'type' => $type , 'id' => $object['workitem_id']);
        break;
      case 'project':
        $list[] = array ( 'number' => $object['project_number'] , 'type' => $type , 'id' => $object['project_id']);
        break;
      case 'partner':
        $list[] = array ( 'number' => $object['partner_number'] , 'type' => $type , 'id' => $object['partner_id']);
        break;
      case 'mockup':
        $list[] = array ( 'number' => $object['mockup_number'] , 'type' => $type , 'id' => $object['mockup_id']);
        break;
      case 'doctype': 
        $list[] = array ( 'number' => $object['doctype_number'] , 'type' => $type , 'id' => $object['doctype_id']);
        break;
      case 'process': 
        $list[] = array ( 'number' => $object['name'].$object['version'] , 'type' => $type , 'id' => $object['pId']);
        break;
      case 'cadlib': 
        $list[] = array ( 'number' => $object['cadlib_number'] , 'type' => $type , 'id' => $object['cadlib_id']);
        break;
      case 'bookshop': 
        $list[] = array ( 'number' => $object['bookshop_number'] , 'type' => $type , 'id' => $object['bookshop_id']);
        break;
      default:
        var_dump($object);
        print 'none manager for '. "$type";
      } //End of switch types
      
    }// End of get detail loop
    
    $smarty->assign('HeaderCol1', 'Type');
    $smarty->assign('HeaderCol2', 'Number');
    $smarty->assign('HeaderCol3', 'Id');
    
    $smarty->assign('id', 'id');
    
    $smarty->assign('col1', 'type');
    $smarty->assign('col2', 'number');
    $smarty->assign('col3', 'id');

  break; //End of list links

//---------------------------------------------------------------------

  case 'linkWorkitem':   //Link a workitem to project
  check_ticket('project_links', $area_id);

    require_once './class/common/container.php'; //Class to manage the projects
    $workitemsManager = container::_factory('workitem');

    if ( $_REQUEST['flag'] == 'validate' ){ //Validate the form of link create request
      foreach ($_REQUEST["checked"] as $workitem_id){
       $workitemsManager->init($workitem_id);
       if(!$workitemsManager->LinkProject($project_id)) //link project object to workitem object
        $erreur = true;
      } //End of loop
      if(isset($erreur)) {$msg = 'An error is occured';$level='Error';}
      else {$msg = 'link workitem success';$level='Info';}
      $error_stack->push(ERROR, $level, array(), $msg);
      $error_stack->checkErrors(array('close_button'=>true));
      die;
    } //End of validate the request
 
    //Include generic definition of the code for manage filters
    //cancel from V0.4.3> include('filterManager.php');
  
    //Get list of workitems
    $list = $workitemsManager->GetAll($params);
    $smarty->assign_by_ref('list', $list);

    $smarty->assign('HeaderCol1', 'Number');
    $smarty->assign('HeaderCol2', 'Description');

    $smarty->assign('id', 'workitem_id');
    $smarty->assign('col1', 'workitem_number');
    $smarty->assign('col2', 'workitem_description');

  break; //End of link workitem

//---------------------------------------------------------------------

  case 'linkPartner':   //Link a partner to project
  check_ticket('project_links', $area_id);

    require_once './class/partner.php'; //Class
    $partnersManager = new partner; //Create new manager

    if ( $_REQUEST['flag'] == 'validate' ){ //Validate the form of link create request
      foreach ($_REQUEST["checked"] as $LO_id) {
        if(!$projectManager->LinkPartner($project_id , $LO_id )) //link project to partner
        $erreur = true;
      }//End of loop
      if(isset($erreur)) {$msg = 'An error is occured';$level='Error';}
      else {$msg = 'link partner success';$level='Info';}
      $error_stack->push(ERROR, $level, array(), $msg);
      $error_stack->checkErrors(array('close_button'=>true));
      die;
    } //End of validate the request
 
    //Include generic definition of the code for manage filters
    //cancel from V0.4.3> include('filterManager.php');
  
    //Get list of partners
    $list = $partnersManager->GetAllPartners($params);
    $smarty->assign_by_ref('list', $list);

    $smarty->assign('HeaderCol1', 'Number');
    $smarty->assign('HeaderCol2', 'Description');

    $smarty->assign('id', 'partner_id');
    $smarty->assign('col1', 'partner_number');
    $smarty->assign('col2', 'description');

  break; //End of link partner

//---------------------------------------------------------------------

  case 'linkMockup':   //Link a mockup to project
  check_ticket('project_links', $area_id);

    if ( empty($_REQUEST['project_id']) ) {print 'none project selected';return false;}

    if ( $_REQUEST['flag'] == 'validate' ){ //Validate the form of link create request
      foreach ($_REQUEST["checked"] as $mockup_id) {
        if (!$projectManager->LinkMockup( $_REQUEST['project_id'], $mockup_id ) ) //link project object to mockup object
        $erreur = true;
      }//End of loop
      if(isset($erreur)) {$msg = 'An error is occured';$level='Error';}
      else {$msg = 'link mockup success';$level='Info';}
      $error_stack->push(ERROR, $level, array(), $msg);
      $error_stack->checkErrors(array('close_button'=>true));
      die;
    } //End of validate the request
 
    require_once './class/common/container.php'; //Class to manage the projects
    $mockupsManager = container::_factory('mockup');

    //Include generic definition of the code for manage filters
    //cancel from V0.4.3> include('filterManager.php');

    //Get list of mockup
    $list = $mockupsManager->GetAll($params);
    $smarty->assign_by_ref('list', $list);

    $smarty->assign('HeaderCol1', 'Number');
    $smarty->assign('HeaderCol2', 'Description');

    $smarty->assign('id', 'mockup_id');
    $smarty->assign('col1', 'mockup_number');
    $smarty->assign('col2', 'description');

  break; //End of link mockup

//---------------------------------------------------------------------

  case 'linkLib':   //Link a librairy to project
  check_ticket('project_links', $area_id);

    if ( empty($_REQUEST['project_id']) ) {print 'none project selected';return false;}

    if ( $_REQUEST['flag'] == 'validate' ){ //Validate the form of link create request
      foreach ($_REQUEST["checked"] as $cadlib_id) {
        if (!$projectManager->LinkCadlib($_REQUEST['project_id'], $cadlib_id )) //link project object to cadlib object
        $erreur = true;
      }//End of loop
      if(isset($erreur)) {$msg = 'An error is occured';$level='Error';}
      else {$msg = 'link cadlib success';$level='Info';}
      $error_stack->push(ERROR, $level, array(), $msg);
      $error_stack->checkErrors(array('close_button'=>true));
      die;
    } //End of validate the request
 
    require_once './class/common/container.php'; //Class to manage the projects
    $LOManager = container::_factory('cadlib');

    //Include generic definition of the code for manage filters
    //cancel from V0.4.3> include('filterManager.php');
  
    //Get list of workitems
    $list = $LOManager->GetAll($params);
    $smarty->assign_by_ref('list', $list);

    $smarty->assign('HeaderCol1', 'Number');
    $smarty->assign('HeaderCol2', 'Description');

    $smarty->assign('id', 'cadlib_id');
    $smarty->assign('col1', 'cadlib_number');
    $smarty->assign('col2', 'cadlib_description');

  break; //End of link partner

//---------------------------------------------------------------------

  case 'linkBib':   //Link a bookshop to project
  check_ticket('project_links', $area_id);

    if(empty($_REQUEST['project_id'])){print 'none project selected';return false;}

    if($_REQUEST['flag'] == 'validate'){ //Validate the form of link create request
      foreach ($_REQUEST["checked"] as $bookshop_id){
        if (!$projectManager->LinkBookshop( $_REQUEST['project_id'], $bookshop_id ) ) //link project object to bookshop object
        $erreur = true;
      }//End of loop
      if(isset($erreur)) {$msg = 'An error is occured';$level='Error';}
      else {$msg = 'link bookshop success';$level='Info';}
      $error_stack->push(ERROR, $level, array(), $msg);
      $error_stack->checkErrors(array('close_button'=>true));
      die;
    } //End of validate the request

    require_once './class/common/container.php';
    $LOManager = container::_factory('bookshop');

    //Include generic definition of the code for manage filters
    //cancel from V0.4.3> include('filterManager.php');
  
    //Get list of workitems
    $list = $LOManager->GetAll($params);
    $smarty->assign_by_ref('list', $list);

    $smarty->assign('HeaderCol1', 'Number');
    $smarty->assign('HeaderCol2', 'Description');

    $smarty->assign('id', 'bookshop_id');
    $smarty->assign('col1', 'bookshop_number');
    $smarty->assign('col2', 'bookshop_description');

  break; //End of link partner

//---------------------------------------------------------------------

  case 'linkDoctype':   //Link a doctype to project
  check_ticket('project_links', $area_id);
  die;
  break; //End of link doctype

//---------------------------------------------------------------------

  case 'linkDefaultProcess':   //Link a default process to project
  check_ticket('project_links', $area_id);

    require_once ('conf/galaxia_setup.php');
    include_once ('lib/Galaxia/ProcessMonitor.php');
    
    //require_once './class/common/containersManager.php'; //Class
    //$LOManager = new containersManager; //Create new manager
    
      if ( $_REQUEST['flag'] == 'validate' ){ //Validate the form of link create request
        if (count($_REQUEST[checked]) == 1){
          $projectManager->linkDefaultProcess($project_id , $_REQUEST["checked"][0]); //link project to partner
          $msg = 'link process success';$level='Info';
        }else{$msg = 'An error is occured';$level='Error';}
      $error_stack->push(ERROR, $level, array(), $msg);
      $error_stack->checkErrors(array('close_button'=>true));
      die;
      } //End of validate the request
 
  $popup = 1;
  include 'galaxia_monitor_processes.php';
  $smarty->assign('popup', 1);
  $smarty->display('header.tpl');
  $smarty->display('project_addlink_process.tpl');
  
  break; //End of link process

} //End of switch between action


$projectManager->error_stack->checkErrors();

?>
