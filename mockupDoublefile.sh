#!/bin/bash
cd /var/www/rbmockup
find /MAQUETTE/Ranchbe_mockup/ -type f -exec basename {} \; | sort | uniq -d > /var/log/rbmockup/mockupDoublons.in
echo '' > /var/log/rbmockup/mockupDoublons.out
while read line ; do find /MAQUETTE/Ranchbe_mockup/ -name "$line" >> /var/log/rbmockup/mockupDoublons.out ; done < /var/log/rbmockup/mockupDoublons.in
\rm /var/log/rbmockup/mockupDoublons.in

echo "Result of mockupDoublefile.sh in /var/log/rbmockup/mockupDoublons.out"
