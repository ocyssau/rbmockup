<?php
require_once('conf/ranchbe_setup.php');

require_once('./class/common/attachment.php');
require_once('./class/common/space.php');
require_once('./class/common/document.php');
require_once('./class/common/docfile.php');

if( !empty($_REQUEST['document_id']) ){
	$odocument = new document(new space($_REQUEST['space']), $_REQUEST['document_id']);

	$viewer = new viewer();
	$viewer->init($odocument);
	if( $viewer->initFromPdfDocfile() ){
		$viewer->pushfile();
	}else{
		$odocument->ViewDocument();
	}
	
	$odocument->error_stack->checkErrors();
	die;
}else if( !empty($_REQUEST['file_id']) ){
	if( $_REQUEST['object_class'] == 'docfile' ){
		$recordfile = new docfile( new space($_REQUEST['space']), $_REQUEST['file_id'] );
	}else if( $_REQUEST['object_class'] == 'recordfile' ){
		require_once('./class/common/recordfile.php');
		$recordfile = new recordfile( new space($_REQUEST['space']), $_REQUEST['file_id'] );
	}else if( $_REQUEST['object_class'] == 'recordfileVersion' ){
		require_once('./class/common/recordfileVersion.php');
		$recordfile = new recordfileVersion( new space($_REQUEST['space']), $_REQUEST['file_id'] );
	}else{
		$error_stack->push(LOG, 'Warning', array('datapath'=>$datapath), 'object_class is not set');
		$error_stack->push(ERROR, 'Warning', array('datapath'=>$datapath), 'can not display');
		$error_stack->checkErrors();
		die;
	}
	$fsdata =& $recordfile->initFsdata(); //Get the fsdata from the record file
	$viewer = new viewer(); //Create a new viewer
	$viewer->initFsdata($fsdata); //link the fsdata to the viewer and set property of the viewer
	$viewer->pushfile(); //Get the viewable file
	$error_stack->checkErrors();
	die;
}
?>
