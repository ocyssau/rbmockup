<?php
//----------------------------------------------------------------------------
//Function definition :
//----------------------------------------------------------------------------

function listLinks(){
    //Include definition of the code for manage the pagination and filters
    $default_sort_field = 'group_define_name';    //Default value of the field to sort
    $default_sort_order = 'ASC'; //Default value of the order ASC = ascendant, DESC = descandant
    include('paginationManager.php');
  
    //Get object linked to project with detail on child objects
    $Objectlist = $projectManager->GetLinkChildsDetail($object_id) ;
    $smarty->assign_by_ref('list', $list);

    $link_object_id = $Objectlist['object_id'];
    
    //var_dump ($Objectlist) ;die;
    
    //Get detail on each object
    foreach ( $Objectlist as $object ){
      $type = $object['object_type'];
      $id   = $object['natif_id'];
      $link_object_id = $object['object_id'];
      
      switch ( $type ) {
      case 'workitem': 
        require_once './class/workitemsManager.php';
        $Manager = new workitemsManager;
        $name = $Manager->GetName($id);
        $list[] = array ( 'number' => $name[workitem_number] , 'type' => $type , 'natif_id' => $id , 'link_object_id' => $link_object_id );
        break;
      case 'project': 
        require_once './class/projectsManager.php';
        $Manager = new projectsManager;
        $list[] = array ( 'number' => $name[project_number] , 'type' => $type , 'natif_id' => $id , 'link_object_id' => $link_object_id  );
        break;
      case 'partner': 
        require_once './class/partnersManager.php';
        $Manager = new partnersManager;
        $list[] = array ( 'number' => $name[partner_number] , 'type' => $type , 'natif_id' => $id , 'link_object_id' => $link_object_id  );

        break;
      case 'mockup': 
        require_once './class/mockupsManager.php';
        $Manager = new mockupsManager;
        $list[] = array ( 'number' => $name[mockup_number] , 'type' => $type , 'natif_id' => $id , 'link_object_id' => $link_object_id  );
        break;
      case 'doctype': 
        require_once './class/doctypesManager.php';
        $Manager = new doctypesManager;
        $list[] = array ( 'number' => $name[doctype_name] , 'type' => $type , 'natif_id' => $id , 'link_object_id' => $link_object_id  );
        break;
      default: 
        print 'none manager for '. "$type";
      } //End of switch types
      
    }// End of get detail loop
    
    $smarty->assign('HeaderCol1', 'Type');
    $smarty->assign('HeaderCol2', 'Number');
    $smarty->assign('HeaderCol3', 'Id');
    
    $smarty->assign('id', 'link_object_id');
    
    $smarty->assign('col1', 'type');
    $smarty->assign('col2', 'number');
    $smarty->assign('col3', 'natif_id');

} //End of function listLinks

//--------------------------------------------------------------------

function suppressLink(){
     foreach ($_REQUEST["checked"] as $link_object_id) {

       //link project object to workitem object
       $projectManager->RemoveLink( $object_id, $link_object_id ) ;

     } //End of loop

} //End of function suppressLink

//--------------------------------------------------------------------

function linkWorkitem(){
    require_once './class/workitemsManager.php'; //Class to manage the projects
    $workitemsManager = new workitemsManager; //Create new manager

    if ( $_REQUEST[flag] == 'validate' ){ //Validate the form of link create request

     foreach ($_REQUEST["checked"] as $workitem_id) {
       //Get the object id
       $WI_object_id = $workitemsManager->GetObjectId( $workitem_id, 'workitem' ) ;
       
       print 'link ' . $WI_object_id;

       //link project object to workitem object
       $projectManager->CreateLink( $object_id, $WI_object_id ) ;

     }die; //End of loop
     
    } //End of validate the request
 
    //Include definition of the code for manage the pagination and filters
    include('paginationManager.php');
  
    //Get list of workitems
    $list = $workitemsManager->GetAll($params);
    $smarty->assign_by_ref('list', $list);

    $smarty->assign('HeaderCol1', 'Number');
    $smarty->assign('HeaderCol2', 'Description');

    $smarty->assign('id', 'workitem_id');
    $smarty->assign('col1', 'workitem_number');
    $smarty->assign('col2', 'description');

} //End of function linkWorkitem

?>
