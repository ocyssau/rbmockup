#!/usr/bin/php
<?php 
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_USER_WARNING);

//Setup
require_once './conf/cliboot.php';
require_once './class/common/container.php'; //Class to manage the bookshop
require_once './lib/Date/date.php';
require_once './class/common/import.php'; //Class to manage the importations
require_once('./class/Batchjob.php');
//require_once './lib/Zend/Log.php';
//require_once './class/messu/messulib.php';
require_once './class/datatypes/Package.php';

function batchImport_Import($packageId, $containerId, $file)
{
	echo 'Try to import package '.$packageId . ' to container ' . $containerId . PHP_EOL;
	$Container = new mockup($containerId);
	$errorCount = 0;
	
	$Package = new Rb_Datatype_Package('mockup');
	$Package->load($packageId);
	$ok = $Package->import($Container, false);
	$file = $Package->getProperty('file');
	
	echo PHP_EOL;
	if(!$ok){
		echo 'Import failed for package ' . $packageId . PHP_EOL;
		$errorCount++;
	}
	else{
		echo "File $file of package $packageId is successfully imported" . PHP_EOL;
		$ok = unlink($file);
		if($ok){
			echo "Imported package file $file is suppressed" . PHP_EOL;
		}
		else{
			echo "Unable to suppress package file $file" . PHP_EOL;
		}
	}
	return $errorCount;
}

/*
$logFile = '/var/log/rbmockup/batchImport.log';
$logger =& Log::singleton('composite');
$filelogger =& Log::singleton('file', $logFile);
$logger->addChild($filelogger);
*/

$Manager = container::_factory('mockup'); //Create new manager
$space =& $Manager->space;

$Import = new import($Manager->SPACE_NAME , $Manager);

//get package to run
$batchJob = new Rbplm_Batchjob();
$ctime = time();
$runner = Rbplm_Batchjob::RUNNER_FILEIMPORT;

$sql = "SELECT * FROM batchjob WHERE planned < $ctime AND state='init' AND runner='$runner'";

if(!$rs = $dbranchbe->SelectLimit( $sql , 1000 , 0)){
	echo $dbranchbe->ErrorMsg() . PHP_EOL;
	die;
}

foreach ($rs as $row) {
	$job = new Rbplm_Batchjob();
	$job->loadFromArray($row);
	$job_start = time();
	$job->setStart($job_start);
	$jname = 'batchFileImportInMockup';
	$job->setName( $jname );
	$job->setState( Rbplm_Batchjob::STATE_RUNNING );
	$ok = $job->update();
	if(!$ok ){
		echo 'error during saving' . PHP_EOL;
		die;
	}
	
	$datas = $job->getProperty('data');
	$errorCount = 0;
	$cleanDatas = array();
	
	$jId = $job->getProperty('id');
	
	echo '------------------------------------------------------' . PHP_EOL;
	echo 'Run job ' . $jId . ' at ' . date(DATE_RFC822) . PHP_EOL;
	
	foreach($datas as $i=>$dataItem){
		$packageId = $dataItem[0];
		$containerId = $dataItem[1];
		
		//Unvalid data def
		if( !$containerId || !$packageId ){
			echo "None container or package are defined" . PHP_EOL;
			continue;
		}
		
		echo '++++++++++++++++++++++++++++++++++++++++++++' . PHP_EOL;
		echo "Try to import package $packageId at " . date(DATE_RFC822) . PHP_EOL;
		
		$PackInfos = $Import->GetPackageInfos( $packageId );
		$file = $PackInfos['package_file_path'] . '/' . $PackInfos['package_file_name'];
		$ext = substr( $file, strrpos($file, '.') );
		
		if( !is_file($file) ){
			echo $file . ' is not a file' . PHP_EOL;
			$errorCount++;
			continue;
		}
		
		//uncompress Z files
		if($ext == '.Z'){
			$exec_return = 1;
			unset($exec_out);
			$ret = exec( UNZIPCMD . ' "' . $file . '"' , $exec_out, $exec_return );
			if ( $exec_return == 0 ){
				echo $file . ' is successfully uncompress' . PHP_EOL;
				$file = rtrim($file , '.Z');
				$infos = $Import->RecordImportPackage($file , false);
				$packageId = $infos['import_order'];
				$c = batchImport_Import($packageId, $containerId, $file);
				$errorCount = $errorCount + $c;
			}
			else{
				echo 'unable to uncompress file ' . $file . PHP_EOL;
				echo date(DATE_RFC822) . PHP_EOL;
				echo '------> file size:' . filesize($file) . PHP_EOL;
				echo '------> available disk space:' . disk_free_space(dirname($file)) . PHP_EOL;
				var_dump($ret, $exec_out);
				$errorCount++;
				continue;
			}
		}
		else{
			batchImport_Import($packageId, $containerId, $file);
		}
		
		echo "End of import package $packageId" . PHP_EOL;
		echo '++++++++++++++++++++++++++++++++++++++++++++' . PHP_EOL;
		
		//reaffect new package id to job definition
		$dataItem[0] = $packageId;
		
		//List of data without unvalid defs
		$cleanDatas[] = $dataItem;
		$datas[$i] = $dataItem;
	}
	
	echo 'Update datas of Job at ' . date(DATE_RFC822) . PHP_EOL;
	$job->setData($datas);
	$job->update();
	
	$job->setEnd(time());
	
	if( $errorCount == 0 ){
		$job->setState(Rbplm_Batchjob::STATE_SUCCESS);
		echo 'Success ending of job ' . $jId . PHP_EOL;
	}
	else if($errorCount >= count($datas) ){
		$job->setState(Rbplm_Batchjob::STATE_FAILED);
		echo 'Fails on job ' . $jId . PHP_EOL;
	}
	else if($errorCount > 0 ){
		$job->setState(Rbplm_Batchjob::STATE_UNCOMPLETE);
		echo 'Uncomplete job ' . $jId . PHP_EOL;
	}
	echo 'Update state of Job' . PHP_EOL;
	$job->update();
	
	echo 'End of job' . PHP_EOL;
	echo '------------------------------------------------------' . PHP_EOL;
} //End of foreach on JOBs

//Clean old not imported pacakge
$olderThan = time() - (2 * 7 * 24 * 3600); //Older than 2 weeks
$Import->CleanHistory($olderThan);

//Clean olds jobs
$sql = "DELETE FROM batchjob WHERE submit < $olderThan AND runner='$runner' ";
if( !$result = $dbranchbe->Execute($sql) ){
	echo 'Error during cleaning of Jobs' . PHP_EOL;
	echo $dbranchbe->ErrorMsg() . PHP_EOL;
}
