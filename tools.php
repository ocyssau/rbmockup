<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

// Initialization
$RightName='30';
//Setup
require_once('conf/ranchbe_setup.php');

if ( $_REQUEST['request_page'] == 'SQL_request' ){
  include('inc/sql_user_queries/sql_user_queries.php');
  $smarty->assign('parametersTab', 'active');
	die;
} //End of GenIndices


//-----------------------------------------------------------------
//Profiling
if ( $_REQUEST['action'] == 'sql_profiling' || isset($_REQUEST['do']) ){
  ob_end_flush();
  //$status = ob_get_status  ( true );
  //var_dump($status);die;
  global $dbranchbe;
  $perf = NewPerfMonitor($dbranchbe);
  $perf->UI($pollsecs=5);
  echo '<a href="tools.php"><b>QUIT AND RETURN TO TOOLS PAGE</b></a>';
  die;
} //End of GenIndices

//-----------------------------------------------------------------
//PHP infos
if ( $_REQUEST['action'] == 'phpinfo' ){
  phpinfo();
  die;
} //End of GenIndices

require_once('HTML/QuickForm.php'); //Librairy to easily create forms
require_once('HTML/QuickForm/Renderer/ArraySmarty.php'); //Lib to use Smarty with QuickForm

require_once('class/common/container.php');
$Manager = container::_factory('workitem');

require_once('class/common/docfile.php');
$filesManager = new docfile($Manager->space);

if ($_REQUEST['request_page'] == 'renameNormesAirbus' ) {
  include('custom/sier/renameNormesAirbus.php');
	die;
}


//Manage actions on documents
if (!empty($_REQUEST["action"])) {

  //Get files record without real file
  if ($_REQUEST['action'] == 'GetFilesRecordWithoutFile'){
  	$result = $filesManager->GetFilesRecordWithoutFile();
  	echo '<pre>';
    var_dump($result);
  	echo '</pre>';

  	echo '<pre>';
  	foreach ( $result as $detail){
      echo implode ( ";" , $detail ) . '<br>';    
    }
  	echo '</pre>';

  	die;
	}
  
  //Backup of Database
  if ($_REQUEST['action'] == 'BackupDatabase'){
    
    $host_ranchbe   = $dbranchbe->host;
    $user_ranchbe   = $dbranchbe->user;
    $pass_ranchbe   = $dbranchbe->password;
    $dbs_ranchbe    = $dbranchbe->database;
    $date = date("Y-m-d_H-i-s"); // On d�finit le variable $date ( ici son format )
    
    // Utilise les fonctions syst�me : MySQLdump
    if ($_REQUEST['BackupDatabaseType'] == 'XML' ){
      $backup_file = $dbs_ranchbe. '_backup_'.$date.'.xml';
      $command = DUMPCMD." --xml -h$host_ranchbe -u$user_ranchbe --password=$pass_ranchbe $dbs_ranchbe";
    }else{
      $backup_file = $dbs_ranchbe. '_backup_'.$date.'.sql';
      $command = DUMPCMD." --opt -h$host_ranchbe -u$user_ranchbe --password=$pass_ranchbe $dbs_ranchbe";
    }

    $backup = shell_exec($command);
    
    header("Content-disposition: attachment; filename=$backup_file");
    header("Content-Type: text/rtf");
    header("Content-Transfer-Encoding: $backup_file\n"); // Surtout ne pas enlever le \n
    header("Content-Length: " . strlen($backup) );
    header("Pragma: no-cache");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
    header("Expires: 0");
    echo $backup;
    die;
	}

  //Get files without documents
  if ($_REQUEST['action'] == 'GetFilesWithoutDocuments' ) {
  	$result = $filesManager->GetFilesWithoutDocuments();
  	echo '<pre>';
    var_dump($result);
  	echo '</pre>';

  	echo '<pre>';
  	foreach ( $result as $detail){
      echo implode ( ";" , $detail ) . '<br>';
    }
  	echo '</pre>';

  	die;
	}

  //Get documents without files
  if ($_REQUEST['action'] == 'GetDocumentsWithoutFile' ) {
  	$result = $filesManager->GetDocumentsWithoutFile();

  	echo '<pre>';
    var_dump($result);
  	echo '</pre>';

  	echo '<pre>';
  	foreach ( $result as $detail){
      echo implode ( ";" , $detail ) . '<br>';    
    }
  	echo '</pre>';
  	die;
	}

  //Get documents without container
  if ($_REQUEST['action'] == 'GetDocumentsWithoutContainer' ) {
  	$result = $filesManager->GetDocumentsWithoutContainer();
  	echo '<pre>';
    var_dump($result);
  	echo '</pre>';

  	echo '<pre>';
  	foreach ( $result as $detail){
      echo implode ( ";" , $detail ) . '<br>';    
    }
  	echo '</pre>';

  	die;
	}

  //Get files in Trash
  if($_REQUEST['action'] == 'GetFilesInTrash'){
    require_once('class/common/rdirectory.php');
    $dir = new rdirectory();
    $result = $dir->listDir(DEFAULT_TRASH_DIR , false);

    if(is_array($result))
    foreach($result as $key=>$file){
    //Get the original directory
      $oriPath = filesystem::decodePath($file['file_name']);
      /*
      $oriPath = str_replace( '%2F' , '/', $file['file_name']);
      $oriPath = str_replace( '%2E' , '.', "$oriPath");
      */
      $oriPath = str_replace( '%&_' , '/', "$oriPath");
      $oriPath = ereg_replace( "\([0-9]{1,2}\).",".",$oriPath);
      $result[$key]['original_file'] = $oriPath;
    }

    $smarty->assign('list',$result);
    // Display the template
    $smarty->display("fileInTrash.tpl");
  	die;
	}

  //Update doctypes icons
  if($_REQUEST['action'] == 'UpdateDoctypeIcon'){
    require_once('class/doctype.php');
    $odoctype = new doctype();
    $p['select'] = array('doctype_id','icon', 'doctype_number');
    $result = $odoctype->GetAll($p);
    //var_dump($result);die;
    if(is_array($result))
    foreach($result as $doctype){
      $odoctype->init($doctype['doctype_id']);
      if( $odoctype->CompileIcon( $doctype['icon'] , $doctype['doctype_id']) )
        echo 'compile icon '.$doctype['icon'].' for doctype '.$doctype['doctype_number'].'<br />';
      else
        echo 'COMPILE FAILED for icon '.$doctype['icon'].' for doctype '.$doctype['doctype_number'].'<br />';
    }
  echo '<b>End</b><br />';
  die;
	}

  //To empty the trash
/*
    if ($_REQUEST['action'] == 'EmptyTrash' ) {
  	$result = EmptyTrash();
  	echo '<pre>';
    var_dump($result);
  	echo '</pre>';

  	echo '<pre>';
  	foreach ( $result as $detail){
      echo implode ( ";" , $detail ) . '<br>';    
    }
  	echo '</pre>';

  	die;
	}
*/

  //To export rights to a file
  if ($_REQUEST['action'] == 'ExportRights'){
    require_once('lib/userslib.php');
    if ($userlib->export_rights()) print 'success'; else print 'error in export';
  	die;
	}

  //To generate live user rights table
  if ($_REQUEST['action'] == 'GenRightsTable' ) {
   	echo '<pre>';
    echo 'INSERT INTO `liveuser_rights` (`right_id`, `area_id`, `right_define_name`, `right_description`, `has_implied`) VALUES' ."\n";
  
    $PROJECT_RIGHTS = array(
    "project_create" => "create a new project",
    "project_suppress"=> "suppress a project",
    "project_get"=> "get list of the project",
    "container_archive"=> "archive a project",
    "container_history"=> "manage history of the project",
    "project_permissions"=> "manage users and permissions of project",
    "project_links"=> "manage links of project",
    "container_doctype" => "manage doctypes linked to project",
    );

    $PROJECT_RIGHTS_fr = array(
    "project_create" => "Cr�er un projet",
    "project_suppress"=> "Supprimer un projet",
    "project_get"=> "Voir la liste des projets",
    "container_archive"=> "Archiver un projet",
    "container_history"=> "G�rer les historique de projet",
    "project_permissions"=> "G�rer les permissions des projets",
    "project_links"=> "G�rer les liens des projets",
    "container_doctype" => "G�rer les doctypes des projets",
    );

    $CONTAINER_RIGHTS = array(
    "container_create" => "create/modify a new CONTAINER",
    "container_suppress"=> "suppress a CONTAINER",
    "container_get"=> "get list of the CONTAINER",
    "container_archive"=> "archive a CONTAINER",
    "container_history"=> "manage history of the CONTAINER",
    "container_import"=> "import file in CONTAINER",
    "container_import_history"=> "manage import history",
    "container_file_manage"=> "manage files imported in CONTAINER",
    "container_document_get"=> "list documents of the CONTAINER",
    "container_document_move" => "move document between container",
    "container_document_copy" => "copy document",
    "container_document_manage"=> "create/modify documents in CONTAINER",
    "container_document_suppress"=> "suppress documents of CONTAINER",
    "container_document_assoc"=> "associate manually a file to a document of CONTAINER",
    "container_document_unlock"=> "unlock documents of CONTAINER",
    "container_document_archive"=> "manage documents archive of CONTAINER",
    "container_document_change_indice"=> "change indice of a CONTAINER documents",
    "container_document_change_state"=> "change state of a CONTAINER documents",
    "container_document_change_number"=> "change number of a CONTAINER documents",
    "container_document_link_file"=> "manage files linked to documents",
    "container_document_history"=> "manage documents history",
    "container_document_version"=> "manage documents versions",
    "container_doctype"=> "manage doctypes of the CONTAINER",
    "container_category"=> "manage categories of the CONTAINER",
    "container_get_stat"=> "display stats about the CONTAINER",
    "container_metadata_suppress"=>"suppress metadata of the CONTAINER",
    "container_metadata_create"=>"create metadata of the CONTAINER",
    "container_metadata_modify"=>"modify metadata of the CONTAINER",
    "document_metadata_suppress"=>"suppress metadata of the document of CONTAINER",
    "document_metadata_create"=>"create metadata of the document of CONTAINER",
    "document_metadata_modify"=>"modify metadata of the document of CONTAINER",
    "container_document_docfile_reset"=>"recalculate the doctype of the documents of CONTAINER",
    );

    $CONTAINER_RIGHTS_fr = array(
    "container_create" => "CONTAINER: Cr�er/modifier",
    "container_suppress"=> "CONTAINER: Supprimer",
    "container_get"=> "CONTAINER: Voir la liste",
    "container_archive"=> "CONTAINER: Archiver",
    "container_history"=> "CONTAINER: G�rer les historiques",
    "container_import"=> "CONTAINER: Importer des fichiers",
    "container_import_history"=> "CONTAINER: G�rer les historiques d'importation",
    "container_file_manage"=> "CONTAINER: G�rer les fichiers import�s",
    "container_document_get"=> "CONTAINER: Lister les documents",
    "container_document_move" => "CONTAINER: D�placer les documents",
    "container_document_copy" => "CONTAINER: Copier les documents",
    "container_document_manage"=> "CONTAINER: Cr�er/modifier les documents",
    "container_document_suppress"=> "CONTAINER: Supprimer les documents",
    "container_document_assoc"=> "CONTAINER: Associer un fichier au document",
    "container_document_unlock"=> "CONTAINER: D�verouiller un document",
    "container_document_archive"=> "CONTAINER: Archiver un document",
    "container_document_change_indice"=> "CONTAINER: Faire �voluer les indices des documents",
    "container_document_change_state"=> "CONTAINER: Changer l'�tat des documents",
    "container_document_change_number"=> "CONTAINER: Modifier le num�ro des documents",
    "container_document_link_file"=> "CONTAINER: Supprimer des fichiers li�s au documents",
    "container_document_history"=> "CONTAINER: G�rer les historiques de documents",
    "container_document_version"=> "CONTAINER: Supprimer des fichiers versions",
    "container_doctype"=> "CONTAINER: G�rer les doctypes",
    "container_category"=> "CONTAINER: G�rer les cat�gories",
    "container_get_stat"=> "CONTAINER: Afficher les statistiques",
    "container_metadata_suppress"=>"CONTAINER: Supprimer m�ta-donn�es de conteneur",
    "container_metadata_create"=>"CONTAINER: Cr�er m�ta-donn�e de conteneur",
    "container_metadata_modify"=>"CONTAINER: Modifier m�ta-donn�e de conteneur",
    "document_metadata_suppress"=>"CONTAINER: Supprimer m�ta-donn�es de document",
    "document_metadata_create"=>"CONTAINER: Cr�er m�ta-donn�e de document",
    "document_metadata_modify"=>"CONTAINER: Modifier m�ta-donn�e de document",
    "container_document_docfile_reset"=>"CONTAINER: Recalculer le doctype des documents",
    );


    $WORKITEM_RIGHTS = array(
    "container_create" => "create a new CONTAINER",
    "container_suppress"=> "suppress a CONTAINER",
    "container_get"=> "get list of the CONTAINER",
    "container_archive"=> "archive a CONTAINER",
    "container_history"=> "manage history of the CONTAINER",

    "container_import"=> "import files in CONTAINER",
    "container_import_history"=> "manage import history of CONTAINER",
    "container_file_manage"=> "manage files of CONTAINER",

    "container_doctype"=> "manage doctypes of the CONTAINER",
    "container_category"=> "manage categories of the CONTAINER",
    "container_get_stat"=> "display stats about the CONTAINER",
    "container_metadata_suppress"=>"suppress metadata of the CONTAINER",
    "container_metadata_create"=>"create metadata of the CONTAINER",
    "container_metadata_modify"=>"modify metadata of the CONTAINER",
    "document_metadata_suppress"=>"suppress metadata of the document of CONTAINER",
    "document_metadata_create"=>"create metadata of the document of CONTAINER",
    "document_metadata_modify"=>"modify metadata of the document of CONTAINER",
    "container_document_docfile_reset"=>"recalculate the doctype of the documents of CONTAINER",
    );

    $WORKITEM_RIGHTS_fr = array(
    "container_create" => "CONTAINER: Cr�er/modifier",
    "container_suppress"=> "CONTAINER: Supprimer",
    "container_get"=> "CONTAINER: Voir la liste",
    "container_archive"=> "CONTAINER: Archiver",
    "container_history"=> "CONTAINER: G�rer les historiques",

    "container_import"=> "CONTAINER: Importer des fichiers",
    "container_import_history"=> "CONTAINER: G�rer les historiques d'importation",
    "container_file_manage"=> "CONTAINER: G�rer les fichiers import�s",

    "container_doctype"=> "CONTAINER: G�rer les doctypes",
    "container_category"=> "CONTAINER: G�rer les cat�gories",
    "container_get_stat"=> "CONTAINER: Afficher les statistiques",
    "container_metadata_suppress"=>"CONTAINER: Supprimer m�ta-donn�es de conteneur",
    "container_metadata_create"=>"CONTAINER: Cr�er m�ta-donn�e de conteneur",
    "container_metadata_modify"=>"CONTAINER: Modifier m�ta-donn�e de conteneur",
    "document_metadata_suppress"=>"CONTAINER: Supprimer m�ta-donn�es de document",
    "document_metadata_create"=>"CONTAINER: Cr�er m�ta-donn�e de document",
    "document_metadata_modify"=>"CONTAINER: Modifier m�ta-donn�e de document",
    "container_document_docfile_reset"=>"CONTAINER: Recalculer le doctype des documents",
    );

    $RANCHBE_RIGHTS = array(
    "partner_create" => "create a partner",
    "partner_suppress" => "suppress a partner",
    "partner_modify" => "modify partner properties",
    
    "doctype_create" => "create a doctype",
    "doctype_suppress" => "suppress a doctype",
    "doctype_modify" => "modify doctype properties",

    "user_create" => "create a user",
    "user_suppress" => "suppress a user",
    "user_modify" => "modify user properties",
    "user_assign_group" => "assign a user to a group",
    "user_assign_perm" => "assign permissions to a user",
    
    "group_create" => "create a group",
    "group_suppress" => "suppress a group",
    "group_modify" => "modify group properties",
    "group_assign_perm" => "assign permissions to a group",
    "group_include" => "assign group to a another group",

    "messu-archive" => "archive a message",
    "messu-broadcast" => "send message to all users",
    "messu-compose" => "compose a message",
    "messu-mailbox" => "manage mailbox",
    "messu-sent" => "sent a message",

    "task-create" => "create a task",
    "task-changestate" => "change state of a task",
    "task-trash" => "put a task in trash",
    "task-undotrash" => "untrash a task",
    "task-emptytrash" => "empty task trash",

    "container_document_instance"=> "manage link between workflow and documents",
    );


    $RANCHBE_RIGHTS_fr = array(
    "partner_create" => "Cr�er un contact",
    "partner_suppress" => "Supprimer un contact",
    "partner_modify" => "Modifier les propri�t�s d'un contact",
    
    "doctype_create" => "Cr�er un doctype",
    "doctype_suppress" => "Supprimer un doctype",
    "doctype_modify" => "Modifier un doctype",

    "user_create" => "Cr�er un utilisateur",
    "user_suppress" => "Supprimer un utilisateur",
    "user_modify" => "Modifier un utilisateur",
    "user_assign_group" => "Affecter un utilisateur � un groupe",
    "user_assign_perm" => "Affecter des permissions � un utilisateur",

    
    "group_create" => "Cr�er un groupe",
    "group_suppress" => "Supprimer un groupe",
    "group_modify" => "Modifier un groupe",
    "group_assign_perm" => "Affecter des permissions � un groupe",
    "group_include" => "Affecter un groupe � un autre groupe",

    "messu-archive" => "Archiver un message",
    "messu-broadcast" => "Envoyer un message � tous les utilisateurs",
    "messu-compose" => "Ecrire un message",
    "messu-mailbox" => "G�rer sa boite au lettre",
    "messu-sent" => "Envoyer un message",

    "task-create" => "Cr�er une t�che",
    "task-changestate" => "Changer l'�tat d'une t�che",
    "task-trash" => "Mettre une t�che � la corbeille",
    "task-undotrash" => "R�cup�r� une t�che supprim�e",
    "task-emptytrash" => "Vider la corbeille",

    "container_document_instance"=> "Affecter un workflow � un document",

    );

    $GALAXIA_RIGHTS = array(
    "g-admin-activities" => "workflow:admin activities",
    "g-admin-graph" => "workflow:admin graph",
    "g-admin-instance" => "workflow:admin instance",
    "g-admin-processes" => "workflow:admin process",
    "g-admin-roles" => "workflow:admin roles",
    "g-admin-shared-source" => "workflow:admin program of activities",
    "g-monitor-instances" => "workflow:monitor instances",
    "g-user-instances" => "workflow:admin users mapping",
    );

    $GALAXIA_RIGHTS_fr = array(
    "g-admin-activities" => "workflow:G�rer les activit�s",
    "g-admin-graph" => "workflow:G�rer les graphs",
    "g-admin-instance" => "workflow:G�rer les instances",
    "g-admin-processes" => "workflow:G�rer les process",
    "g-admin-roles" => "workflow:G�rer les r�les",
    "g-admin-shared-source" => "workflow:G�rer les scripts d'activit�s",
    "g-monitor-instances" => "workflow:Surveiller les instances",
    "g-user-instances" => "workflow:G�rer les relations utilisateurs/r�les",
    );

/* For english*/
    $Areas["1"] = array ( "area_define_name" => "ranchbe" , "rights"=> $RANCHBE_RIGHTS);
    $Areas["5"] = array ( "area_define_name" => "project" , "rights"=> $PROJECT_RIGHTS);
    $Areas["10"] = array ( "area_define_name" => "workitem" , "rights"=> $WORKITEM_RIGHTS);
    $Areas["15"] = array ( "area_define_name" => "mockup" , "rights"=> $CONTAINER_RIGHTS);
    $Areas["20"] = array ( "area_define_name" => "bookshop" , "rights"=> $CONTAINER_RIGHTS);
    $Areas["25"] = array ( "area_define_name" => "cadlib" , "rights"=> $CONTAINER_RIGHTS);
    $Areas["30"] = array ( "area_define_name" => "galaxia" , "rights"=> $GALAXIA_RIGHTS);

//Pour le francais
/*
    $Areas[1] = array ( "area_define_name" => "ranchbe" , "rights"=> $RANCHBE_RIGHTS_fr);
    $Areas[5] = array ( "area_define_name" => "project" , "rights"=> $PROJECT_RIGHTS_fr);
    $Areas[10] = array ( "area_define_name" => "workitem" , "rights"=> $WORKITEM_RIGHTS_fr);
    $Areas[15] = array ( "area_define_name" => "mockup" , "rights"=> $CONTAINER_RIGHTS_fr);
    $Areas[20] = array ( "area_define_name" => "bookshop" , "rights"=> $CONTAINER_RIGHTS_fr);
    $Areas[25] = array ( "area_define_name" => "cadlib" , "rights"=> $CONTAINER_RIGHTS_fr);
    $Areas[30] = array ( "area_define_name" => "galaxia" , "rights"=> $GALAXIA_RIGHTS_fr);
*/

    $i = 1;
    foreach ($Areas as $area_id => $details){
      $areas_sql .= "($area_id, 1, \"$details[area_define_name]\")\"," . "\n";
      
      foreach( $details["rights"] as $define_name => $description){
        $description = str_replace( CONTAINER ,  $details['area_define_name'] , $description);
        echo "($i, $area_id, \"$define_name\", \"$description\", 0)," ."\n";
        $i++;
      }
    }

    echo "INSERT INTO `liveuser_areas` (`area_id`, `application_id`, `area_define_name`) VALUES" . "\n";
    echo $areas_sql;
   	echo '</pre>';
    die;
	} //End of GenRightsTable

//-----------------------------------------------------------------
//To generate doctypes table
if ($_REQUEST['action'] == 'GenDoctypes' ) {

  echo 'INSERT INTO `doctypes` (`doctype_id`, `doctype_number`, `doctype_description`, `can_be_composite`, `script_post_store`, `script_pre_store`, `script_post_update`, `script_pre_update`, `recognition_regexp`, `file_extension`, `file_type`, `icon`) VALUES ' ."<br>";

  $file = "./Docs/doctypes.csv";

  $handle = fopen($file, "r");

  $linecount = count(file($file));

  $row = 1;

  while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
    $doctype_number = $data[0];
    $doctype_description = $data[1];
    $file_extension = $data[2];
    $file_type = $data[3];
    $recognition_regexp = $data[4];
    $icon = $data[5];
    $file_extension_list[] = $file_extension;
    echo  "($row, '$doctype_number', '$doctype_description','0','','','','','$recognition_regexp', '$file_extension', '$file_type', '$icon')";
    if ( $linecount !== $row ) echo ",<br>";
    else  echo ";<br>";
    $row++;
  }
  fclose($handle);

  $rows = $row - 1;
  print "
        DROP TABLE `doctypes_seq`; <br>
        CREATE TABLE `doctypes_seq` (
          `sequence` int(11) NOT NULL auto_increment,
          PRIMARY KEY  (`sequence`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=$row ;
        <br>
        INSERT INTO `doctypes_seq` (`sequence`) VALUES 
        ($rows);
        ";

echo '<br />';
foreach($file_extension_list as $extension){
  echo '$valid_file_ext["' .$extension. '"] = "' .$extension. '";';
  echo '<br>';
}

die;
} //End of GenDoctypes


//-----------------------------------------------------------------
//To generate document indice table
if ($_REQUEST['action'] == 'GenIndices'){
die;
} //End of GenIndices

//-----------------------------------------------------------------
//To list connected users
if ($_REQUEST['action'] == 'GetConnectedUsers'){
  print '<b>Function to write...if you like it to do...';
  die;
} //End of GenIndices

//-----------------------------------------------------------------
//To add rights
if($_REQUEST['action'] == 'AddRights'){

echo '<pre>';
print '
this tool can help you to add right definition. Edit the file tools.php and read the section
after the AddRights conditions.';
echo '</pre>';

  /*
  $Areas['1'] = array ( 'area_define_name' => 'ranchbe' , 'rights'=> $RANCHBE_RIGHTS);
  $Areas['5'] = array ( 'area_define_name' => 'project' , 'rights'=> $PROJECT_RIGHTS);
  $Areas['10'] = array ( 'area_define_name' => 'workitem' , 'rights'=> $WORKITEM_RIGHTS);
  $Areas['15'] = array ( 'area_define_name' => 'mockup' , 'rights'=> $CONTAINER_RIGHTS);
  $Areas['20'] = array ( 'area_define_name' => 'bookshop' , 'rights'=> $CONTAINER_RIGHTS);
  $Areas['25'] = array ( 'area_define_name' => 'cadlib' , 'rights'=> $CONTAINER_RIGHTS);
  $Areas['30'] = array ( 'area_define_name' => 'galaxia' , 'rights'=> $GALAXIA_RIGHTS);
  */

  //Require function to administrate user and group permissions
  require_once('lib/userslib.php');

  //Edit this array to define the right name and the right description:
  /*ex:
  $rightsToAdd = array(
    'container_document_move' => 'move document between container',
    'container_document_copy' => 'copy document',
  );
  */
  $rightsToAdd = array();

  //Edit this array to define the area to add the rights
  /*ex:
  $toarea=array(10,15,20,25); to add right for workitem , bookshop, cadlib, mockup areas
  */
  $toarea=array();

  if(empty($rightsToAdd) || empty($toarea)) {print '<br><b>nothing to do...edit the vars $rightsToAdd and $toarea in the tools.php file.<br>'; return false;}

  foreach($toarea as $area_id){
    foreach($rightsToAdd as $right_define_name=>$right_description){
      if(count($userlib->get_right($area_id , $right_define_name)) == 0){
        print 'Add ' .$right_define_name.' to area '.$area_id.'<br />';
        $userlib->add_right( $area_id  , $right_define_name , $right_description);
      }else{
        print $right_define_name.' EXIST IN '.$area_id.'<br />';
      }
    }
  }
  die;
} //End of addrights


//-----------------------------------------------------------------
//To add rights
//get list of scripts and display it in navigator window
if($_REQUEST['action'] == 'getcustom'){
	$list = glob(dirname(__FILE__) . '/custom/*.php');
	
	$html = '<h1>CUSTOM RANCHBE SCRIPTS</h1>';
	$html .= '<a href="http://ranchbe.sierbla.int/viewFile.php?document_id=11395&space=bookshop">HELP</a><br />';
	
	$html .= '<ul>';
	foreach($list as $script){
		$script = basename($script);
		$html .= "<li><a href=./custom.php?module=$script>$script</a></li>";
	}
	$html .= '</ul>';
	$smarty->assign('literalContent', $html);
	$smarty->display('ranchbe_WithoutTabs.tpl');
	die;
} //End of customscripts

} //End manage actions on documents

// Active the tab
$smarty->assign('parametersTab', 'active');
// Display the template
$smarty->assign('mid', 'tools.tpl');
$smarty->display('ranchbe.tpl');
?>
