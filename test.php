#!/usr/bin/php
<?php
require_once './conf/cliboot.php';
ini_set('display_errors' , 1);

require_once './class/common/container.php'; //Class to manage the bookshop
require_once './class/mockup.php'; //Class to manage the bookshop
require_once './lib/Date/date.php';
require_once './class/common/import.php'; //Class to manage the importations
require_once('./class/Batchjob.php');
require_once './class/datatypes/Package.php';

$packageId = 48775;
$containerId = 1;
$Container = new mockup($containerId);
$Package = new Rb_Datatype_Package('mockup');
$Package->load($packageId);
$file = $Package->getProperty('file');
$ext = substr( $file, strrpos($file, '.') );

if( !is_file($file) ){
	echo $file . ' is not a file' . PHP_EOL;
	die;
}

//var_dump( $ok, $file ); var_dump( UNZIPCMD );die;

//uncompress Z files
if($ext == '.Z'){
	$exec_return = 1;
//var_dump( $ok, $file ); var_dump( UNZIPCMD );die;
	system( UNZIPCMD . ' "' . $file . '"' , $exec_return );
	
	if ( $exec_return == 0 ){
		echo $file . ' is successfully uncompress' . PHP_EOL;
		$file = rtrim($file , '.Z');
		$Package->setFile($file);
		$Package->update();
	}
	else{
		echo 'unable to uncompress file ' . $file . PHP_EOL;
		echo '------> file size:' . filesize($file) . PHP_EOL;
		echo '------> available disk space:' . disk_free_space(dirname($file)) . PHP_EOL;
		die;
	}
}

$ok = $Package->import($Container, false);
$file = $Package->getProperty('file');

var_dump( $ok, $file );
die;



$files1 = glob('/DATA6/livraison/reception/Ranchbe/AF-SIER-20111129-A350-CGR-xwb_s1618_aft_fuselage_merged*.Z');
$files2 = glob('/DATA6/livraison/reception/Ranchbe/AF-SIER-20111129-A350-CGR-xwb_s1314_fwd_fuselage_merged*.Z');
$files3 = glob('/DATA6/livraison/reception/Ranchbe/AF-SIER-20111129-A350-CGR-xwb_s21_center_wing_box_part*.Z');
$files4 = glob('/DATA6/livraison/reception/Ranchbe/AF-SIER-20111128-A350-CGR-xwb_s15_center_fuselage_merged*.Z');
$files5 = glob('/DATA6/livraison/reception/Ranchbe/AF-SIER-20111128-A350-V5-xwb_s1618_aft_fuselage_part*.Z');

$files = array_merge($files1, $files2, $files3, $files4, $files5);

//var_dump($files);die;

foreach($files as $file){
	$exec_return = 1;
	system( UNZIPCMD . ' "' . $file . '"' , $exec_return );
	var_dump($exec_return);
	if ( $exec_return == 0 ){
		echo $file . ' is successfully uncompress' . PHP_EOL;
	}
	else{
		echo 'unable to uncompress file ' . $file . PHP_EOL;
		echo 'size:'.filesize($file) . PHP_EOL;
	}
}
die;

require_once './conf/ranchbe_setup.php';

error_reporting( E_ALL );
ini_set('display_errors' , 1);
//set_include_path(get_include_path().PATH_SEPARATOR.'/usr/share/php/libzend-framework-php');

require_once './class/Mail.php';

$to = 'olivier.cyssau@sierbla.com';
$from = 'rb@sierbla.com';
$subject = 'test';
$textMessage = 'un test';
$htmlMessage = '<b>un test</b>';

$mail = new Rb_Mail();
$mail->setBodyText($textMessage);
$mail->setBodyHtml($htmlMessage);
$mail->setFrom($from, 'Ranchbe');
$mail->addTo($to, 'Olivier CYSSAU');
$mail->setSubject($subject);
$mail->send();

