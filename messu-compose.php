<?php

// $Header: /cvsroot/ranchbe/ranchbe/messu-compose.php,v 1.6 2008/09/25 16:35:01 ranchbe Exp $

// Copyright (c) 2002-2005, Luis Argerich, Garland Foster, Eduardo Polidor, et. al.
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.
// require_once ('tiki-setup.php');

require_once './conf/ranchbe_setup.php';
include_once './class/messu/messulib.php';

$area_id = 1;
check_ticket('messu-compose' , $area_id);

/*
if (!$user) {
	$smarty->assign('msg', tra("You are not logged in"));
	$smarty->display("error.tpl");
	die;
}

if ($feature_messages != 'y') {
	$smarty->assign('msg', tra("This feature is disabled").": feature_messages");
	$smarty->display("error.tpl");
	die;
}

if ($tiki_p_messages != 'y') {
	$smarty->assign('msg', tra("Permission denied"));
	$smarty->display("error.tpl");
	die;
}

if ($allowmsg_is_optional == 'y') {
	if ($tikilib->get_user_preference($user, 'allowMsgs', 'y') != 'y') {
	$smarty->assign('msg', tra("You have to be able to receive messages in order to send them. Goto your user preferences and enable 'Allow messages from other users'"));
	$smarty->display("error.tpl");
	die;
	}
}
*/

// Strip Re:Re:Re: from subject
if(isset($_REQUEST['reply']) || isset($_REQUEST['replyall'])) {
  $msg = $messulib->get_message($user, $_REQUEST['msgId']);
	$_REQUEST['subject'] = $msg['subject'];
	$_REQUEST['body'] = $msg['body']; 
}

if (($messu_sent_size>0) && ($messulib->count_messages($user, 'sent')>=$messu_sent_size)){
	$smarty->assign('msg', tra('Sent box is full. Archive or delete some sent messages first if you want to send more messages.'));
	$smarty->display("error.tpl");
	die;
}

if (!isset($_REQUEST['to']))
	$_REQUEST['to'] = '';

if (!isset($_REQUEST['cc']))
	$_REQUEST['cc'] = '';

if (!isset($_REQUEST['bcc']))
	$_REQUEST['bcc'] = '';

if (!isset($_REQUEST['subject']))
	$_REQUEST['subject'] = '';

if (!isset($_REQUEST['body']))
	$_REQUEST['body'] = ''; 

if (!isset($_REQUEST['replyto_hash']))
	$_REQUEST['replyto_hash'] = '';

if (!isset($_REQUEST['priority']))
	$_REQUEST['priority'] = 3;

if (isset($_REQUEST['to_all'])){
	$_REQUEST['to'] = '';
  global $LUA;
  $LUparams = array ( 'container' => 'auth',
                      'fields' => array('handle','auth_user_id')
                    );
  $all_users = $LUA->getUsers($LUparams);

  foreach ( $all_users as $values ){
  	$_REQUEST['to'] .= $values['handle'] . ' ';
  }
}

// Strip Re:Re:Re: from subject
if(isset($_REQUEST['reply']) || isset($_REQUEST['replyall'])) {
	$_REQUEST['subject'] = tra("Re:") . ereg_replace("^(".tra("Re:").")+", "", $_REQUEST['subject']);
}

//Clean the body text
$_REQUEST['body'] = $messulib->clean_put_text($_REQUEST['body']);

foreach( array( 'to', 'cc', 'bcc' ) as $dest )
	if( is_array( $_REQUEST[$dest] ) )
		$_REQUEST[$dest] = implode( ', ', array_filter( $_REQUEST[$dest], 'ctype_alnum' ) );

$smarty->assign('to', $_REQUEST['to']);
$smarty->assign('cc', $_REQUEST['cc']);
$smarty->assign('bcc', $_REQUEST['bcc']);
$smarty->assign('subject', $_REQUEST['subject']);
$smarty->assign('body', $_REQUEST['body']);
$smarty->assign('priority', $_REQUEST['priority']);
$smarty->assign('replyto_hash', $_REQUEST['replyto_hash']);

$smarty->assign('mid', 'messu-compose.tpl');

$smarty->assign('sent', 0);

if (isset($_REQUEST['send'])) {
	$smarty->assign('sent', 1);
	$message = '';
	// Validation:
	// must have a subject or body non-empty (or both)
	if (empty($_REQUEST['subject']) && empty($_REQUEST['body'])) {
		$smarty->assign('msg', tra('ERROR: Either the subject or body must be non-empty'));
		$smarty->display("error_simple.tpl");
		die;
	}

	// Parse the to, cc and bcc fields into an array
	$arr_to = preg_split('/\s*(,|\s)\s*/', $_REQUEST['to']);
	$arr_cc = preg_split('/\s*(,|\s)\s*/', $_REQUEST['cc']);
	$arr_bcc = preg_split('/\s*(,|\s)\s*/', $_REQUEST['bcc']);

	// Remove invalid users from the to, cc and bcc fields
	$users = array();

  //Initialise the arrays. Necessary for function array_merge;
  if (! is_array($arr_to) ) $arr_to = array();
  if (! is_array($arr_cc) ) $arr_cc = array();
  if (! is_array($arr_bcc) ) $arr_bcc = array();
  $to_cc_ccc = array_merge($arr_to, $arr_cc, $arr_bcc);

	foreach ($to_cc_ccc as $a_user) {
		if (!empty($a_user)) {
			if ($messulib->user_exists($a_user)) {
					// only send mail if nox mailbox size is defined or not reached yet
					if (($messulib->count_messages($a_user)<$messu_mailbox_size) || ($messu_mailbox_size==0)) {
						$users[] = $a_user;
					} else {
						$message .= sprintf(tra("User %s can not receive messages, mailbox is full"),$a_user)."<br />";
					}

			} else {
				$message .= sprintf(tra("Invalid user: %s"),$a_user)."<br />";
			}
		}
	}

/*
	foreach ($arr_to as $a_user) {
		if (!empty($a_user)) {
			if ($messulib->user_exists($a_user)) {
				// mail only to users with activated message feature
				if ($messulib->get_user_preference($a_user, 'allowMsgs', 'y') == 'y') {
					// only send mail if nox mailbox size is defined or not reached yet
					if (($messulib->count_messages($a_user)<$messu_mailbox_size) || ($messu_mailbox_size==0)) {
						$users[] = $a_user;
					} else {
						$message .= sprintf(tra("User %s can not receive messages, mailbox is full"),$a_user)."<br />";
					}
				} else {
					$message .= sprintf(tra("User %s can not receive messages"),$a_user)."<br />";
				}

			} else {
				$message .= sprintf(tra("Invalid user: %s"),$a_user)."<br />";
			}
		}
	}

	foreach ($arr_cc as $a_user) {
		if (!empty($a_user)) {
			if ($messulib->user_exists($a_user)) {
				// mail only to users with activated message feature
				if ($messulib->get_user_preference($a_user, 'allowMsgs', 'y') == 'y') {
					// only send mail if nox mailbox size is defined or not reached yet
					if (($messulib->count_messages($a_user)<$messu_mailbox_size) || ($messu_mailbox_size==0)) {
						$users[] = $a_user;
					} else {
						$message .= sprintf(tra("User %s can not receive messages, mailbox is full"),$a_user)."<br />";
					}
				} else {
					$message .= sprintf(tra("User %s can not receive messages"),$a_user)."<br />";
				}
			} else {
				$message .= sprintf(tra("Invalid user: %s"),$a_user)."<br />";
			}
		}
	}

	foreach ($arr_bcc as $a_user) {
		if (!empty($a_user)) {
			if ($messulib->user_exists($a_user)) {
				// mail only to users with activated message feature
				if ($messulib->get_user_preference($a_user, 'allowMsgs', 'y') == 'y') {
					// only send mail if nox mailbox size is defined or not reached yet
					if (($messulib->count_messages($a_user)<$messu_mailbox_size) || ($messu_mailbox_size==0)) {
						$users[] = $a_user;
					} else {
						$message .= sprintf(tra("User %s can not receive messages, mailbox is full"),$a_user)."<br />";
					}
				} else {
					$message .= sprintf(tra("User %s can not receive messages"),$a_user)."<br />";
				}
			} else {
				$message .= sprintf(tra("Invalid user: %s"),$a_user)."<br />";
			}
		}
	}
*/

	$users = array_unique($users);

	// Validation: either to, cc or bcc must have a valid user
	if (count($users) > 0) {
		$message .= tra("Message will be sent to: "). implode(',', $users). "<br />";
	} else {
		$message .= tra('ERROR: No valid users to send the message');
		$smarty->assign('msg', $message);
		$smarty->display("error_simple.tpl");
		die;
	}

	// Insert the message in the inboxes of each user
	foreach ($users as $a_user) {
		$messulib->post_message(
			$a_user, $user, $_REQUEST['to'], $_REQUEST['cc'], $_REQUEST['subject'], $_REQUEST['body'],
			$_REQUEST['priority'], $_REQUEST['replyto_hash']);

/*
    		if ($feature_score == 'y') {
	            $tikilib->score_event($user, 'message_send');
			    $tikilib->score_event($a_user, 'message_receive');
	        }
*/

		// if this is a reply flag the original messages replied to
		if ($_REQUEST['replyto_hash']<>'') {	
			$messulib->mark_replied($a_user, $_REQUEST['replyto_hash']);
		}
	}
	
	// Insert a copy of the message in the sent box of the sender
	$messulib->save_sent_message(
		$user, $user, $_REQUEST['to'], $_REQUEST['cc'], $_REQUEST['subject'], $_REQUEST['body'],
		$_REQUEST['priority'], $_REQUEST['replyto_hash']);

	$smarty->assign('message', $message);
}

//$allowMsgs = $messulib->get_user_preference($user, 'allowMsgs', 'y');
//$smarty->assign('allowMsgs', $allowMsgs);

/*
$section = 'user_messages';
include_once ('tiki-section_options.php');

ask_ticket('messu-compose');

include_once ('tiki-mytiki_shared.php');

$smarty->display("tiki.tpl");
*/


$smarty->assign('accueilTab', 'active');
$smarty->assign('mid', 'messu/messu-compose.tpl');
$smarty->display("ranchbe.tpl");

?>
