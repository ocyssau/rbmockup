<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once './conf/ranchbe_setup.php';//ranchBE configuration
require_once './class/common/container.php'; //Class to manage the container

if(isset($_REQUEST['container_id']))
  $Manager = container::_factory('bookshop', $_REQUEST['container_id']); //Create new manager
else
  $Manager = container::_factory('bookshop'); //Create new manager

$space =& $Manager->space;

//$OptionalFieldsHeader = array('project_id'=>'Project', 'info_1'=>'Interlocuteur client 1', 'info_2'=>'Interlocuteur client 2', 'info_3'=>'Interlocuteur interne', 'info_4'=>'Comments');
//$OptionalFieldsBody   = array('project_id'=>'project', 'info_1'=>'partner', 'info_2'=>'partner', 'info_3'=>'partner', 'info_4'=>'nofilter');

include './inc/containers.php';

?>
