<?php
$RightName='30';
require_once './conf/ranchbe_setup.php';

if (isset ($_REQUEST["rightAdd"])) {
  $area_id=$_REQUEST["area_id"];
  $right_id=$_REQUEST["right_id"];
  $right_name=$_REQUEST["right_name"];
  $has_implied=$_REQUEST["has_implied"];

    echo '<p>New right with :</p>';
    echo '<ul>';
    echo '<li>area id: ' . $area_id . '</li>'; 
    echo '<li>right id: ' . $right_id . '</li>'; 
    echo '<li>right name: &laquo;' . $right_name . '&raquo;</li>'; 
    echo '<li>has an implied right (Y=1/N=0): ' . $has_implied . '</li>'; 
    echo '</ul>';

    $data = array('area_id' => $area_id,
                  'right_id' => $right_id,
                  'right_define_name' => $right_name,
                  'has_implied' => $has_implied);

    $rightId = $LUA->perm->addRight($data);

    if ($rightId == false)
      {
      echo '<p>Add_right: error on line: '.__LINE__.'</p>';
      print_r($LUA->getErrors());
      }
    else
      {
      echo '<p>Right with id <em>' . $rightId . '</em> created</p>';
      }
}

?>
<?xml VERSION="1.0" ENCODING="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta name="generator" content="Dev-PHP 2.0.11" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="styles.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="lib.js"></script>
</head>

<body>

  <div id="Boites">
    <p class = entete >Cr�er un droit</p>
     <form name="userCreate" action="rightAdd.php" method="post">
      <ul>
       <li>Aire fonctionnelle :<br />
				<select name="area_id">
          <?php
          $areas = $LUA->perm->getAreas();
          while (list($key, $val) = each($areas))
          {
          echo "<option>" . "$val[area_id] $val[area_define_name]" . "</option>";
          }
          ?>
				</select>
       </li>

       <li>rightID :<br />
        <input type="text" name="right_id" />
       </li>

       <li>right name :<br />
			  <input type="text" name="right_name" />
       </li>

       <li>Heritage :<br />
       <input type="checkbox" name="has_implied" value="1" />
       </li>
      </ul>
      <p><input type="submit" name="rightAdd" value="Envoyer" /></p>
     </form>
  </div>

</body>
</html>