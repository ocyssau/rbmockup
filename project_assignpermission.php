<?php

// $Header: /cvsroot/ranchbe/ranchbe/project_assignpermission.php,v 1.5 2007/11/19 18:26:01 ranchbe Exp $

// Copyright (c) 2002-2005, Luis Argerich, Garland Foster, Eduardo Polidor, et. al.
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.

// This script is used to assign permissions to a particular group
// ASSIGN PERMISSIONS TO GROUPS

// Initialization

//Setup
//require_once './conf/ranchbe_setup.php';

//Require function to administrate user and group permissions
require_once './lib/userslib.php';

//require_once './class/projectsManager.php'; //Class to manage the projects
//$projectManager = new projectsManager(); //Create new manager

$area_id = $projectManager->AREA_ID;

//Check if a group id is send by user.
if (!isset($_REQUEST["group"])) {
	$smarty->assign('msg', tra("Unknown group"));
	$smarty->display("error.tpl");
	die;
}

//$group is the group id
$group = $_REQUEST["group"];

$smarty->assign_by_ref('group', $group);

//Assign a permission to a group
if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'assign') {
	$userlib->assign_permission_to_group($_REQUEST["perm"], $group);
	//$logslib->add_log('perms',"assigned perm ".$_REQUEST['perm']." to group $group_name");
}

//Remove a permission from a group
if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'remove') {
	//$area = 'delpermassign';
	if ($feature_ticketlib2 != 'y' or (isset($_POST['daconfirm']) and isset($_SESSION["ticket_$area"]))) {
		//key_check($area);
		$userlib->remove_permission_from_group($_REQUEST["permission"], $group);
		$logslib->add_log('perms',"unassigned perm ".$_REQUEST['permission']." from group $group");
	} else {
		key_get($area);
	}
}

//Update the permissions
if (isset($_REQUEST['update'])) {
	check_ticket('project_permissions' , $area_id);
	foreach (array_keys($_REQUEST['permName'])as $per) {
		if (isset($_REQUEST['perm'][$per])) {
			$userlib->assign_permission_to_group($per, $group, $LUA);
		} else {
			$userlib->remove_permission_from_group($per, $group, $LUA);
		}
		//$logslib->add_log('perms',"changed perms for group $group");
	}

$smarty->assign('msg', 'Permissions updated successful');

}

//Process the Sort and Limition items to display
$default_sort_field = 'right_define_name';    //Default value of the field to sort
$default_sort_order = 'ASC'; //Default value of the order ASC = ascendant, DESC = descandant
include('filterManager.php');

//Get the permissions of the group
$group_perms = $userlib->get_group_permissions($group, $LUA);
$smarty->assign('group_perms', $group_perms);

//Get all permissions
$perms = $userlib->get_permissions(0, 999, 'right_define_name', 'ASC' , '' , '' , $LUA , $project_area_id);
$smarty->assign_by_ref('perms', $perms);

//Get all groups of system
$groups = $userlib->get_groups('0', '500', 'group_define_name', 'ASC', ' ', $LUA);
$smarty->assign('groups', $groups);

// Get infos about current selected group
$group_info = $userlib->get_group_info($group, $LUA);
$smarty->assign_by_ref('group_info', $group_info);

//$smarty->assign('project_id', $_REQUEST[project_id]);

?>
