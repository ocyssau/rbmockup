<?php

// $Header: /cvsroot/ranchbe/ranchbe/tiki-adminusers.php,v 1.5 2008/09/15 11:18:07 ranchbe Exp $

// Copyright (c) 2002-2005, Luis Argerich, Garland Foster, Eduardo Polidor, et. al.
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.

//Setup
require_once './conf/ranchbe_setup.php';

//Require function to administrate user and group permissions
require_once './lib/userslib.php';
$area_id = $userlib->AREA_ID;

//--------------------------------------------------------------------
function discardUser($u, $reason) {
	$u['reason'] = $reason;
	return $u;
}


function batchImportUsers() {
	global $userlib, $smarty, $user;

	$fname = $_FILES['csvlist']['tmp_name'];
	$fhandle = fopen($fname, "r");
	$fields = fgetcsv($fhandle, 1000);
	if (!$fields[0]) {
		$smarty->assign('msg', tra("The file is not a CSV file or has not a correct syntax"));
		$smarty->display("error.tpl");
		die;
	}
	while (!feof($fhandle)) {
		$data = fgetcsv($fhandle, 1000);
		$temp_max = count($fields);
		for ($i = 0; $i < $temp_max; $i++) {
			if ($fields[$i] == "login" && function_exists("mb_detect_encoding") && mb_detect_encoding($data[$i], "ASCII, UTF-8, ISO-8859-1") ==  "ISO-8859-1") {
				$data[$i] = utf8_encode($data[$i]);
			}
			@$ar[$fields[$i]] = $data[$i];
		}
		$userrecs[] = $ar;
	}
	fclose ($fhandle);

	if (!is_array($userrecs)) {
		$smarty->assign('msg', tra("No records were found. Check the file please!"));
		$smarty->display("error.tpl");
		die;
	}
	$added = 0;
	$errors = array();
/*
	if ($tiki_p_admin != 'y')
		$userGroups = $userlib->get_user_groups_inclusion($user);
*/
	foreach ($userrecs as $u) {
		if (empty($u['login'])) {
			$discarded[] = discardUser($u, tra("User login is required"));
		} elseif (empty($u['password'])) {
			$discarded[] = discardUser($u, tra("Password is required"));
		} elseif (empty($u['email'])) {
			$discarded[] = discardUser($u, tra("Email is required"));
		} elseif ($userlib->user_exists($u['login'])and (!isset($_REQUEST['overwrite']))) {
			$discarded[] = discardUser($u, tra("User is duplicated"));
		} else {
			if (!$userlib->user_exists($u['login'])) {
				$userlib->add_user($u['login'], $u['password'], $u['password'], $u['email']);
				//$logslib->add_log('users',sprintf(tra("Created account %s <%s>"),$u["login"], $u["email"]));
			}

			//$userlib->set_user_fields($u);

			if (@$u['groups']) {
				$grps = explode(",", $u['groups']);

				foreach ($grps as $grp) {
					$grp = preg_replace("/^ *(.*) *$/u", "$1", $grp);
					if (!$userlib->group_exists($grp)) {
						$err = tra("Unknown").": $grp";
						if (!in_array($err, $errors))
								$errors[] = $err;
/*
					} elseif ($tiki_p_admin != 'y' &&  !array_key_exists($grp, $userGroups)) {
						$err = tra("Permission denied").": $grp";
						if (!in_array($err, $errors))
								$errors[] = $err;
*/
					} else {

						$userlib->assign_user_to_group($u['login'], $grp);
						//$logslib->add_log('perms',sprintf(tra("Assigned %s in group %s"),$u["login"], $grp));
					}
				}
			}
			$added++;
		}
	}
	$smarty->assign('added', $added);
	if (@is_array($discarded)) {
		$smarty->assign('discarded', count($discarded));
	}
	@$smarty->assign('discardlist', $discarded);
	if (count($errors)) {
		array_unique($errors);
		$smarty->assign_by_ref('errors', $errors);
	}
} //End of function batch import
//--------------------------------------------------------------------


//--------------------------------------------------------------------
// Process the form to add a user here
if (isset($_POST["newuser"])) {
  check_ticket( 'user_create' , $area_id );
	// if no user data entered, check if it's a batch upload  
	if ((!$_REQUEST["name"]) and (is_uploaded_file($_FILES['csvlist']['tmp_name'])))
		batchImportUsers();

  // Check if the user already exists
	if ($_REQUEST["pass"] != $_REQUEST["pass2"]) {
		$tikifeedback[] = array('num'=>1,'mes'=>tra("The passwords don't match"));
	} else {
    if ($userlib->user_exists($_REQUEST["name"], $LUA)) {
			$tikifeedback[] = array('num'=>1,'mes'=>sprintf(tra("User %s already exists"),$_REQUEST["name"]));
		} else {
			if ($userlib->add_user($_REQUEST["name"], $_REQUEST["pass"], $_REQUEST["pass2"], $_REQUEST["email"], $LUA)) {
				$tikifeedback[] = array('num'=>0,'mes'=>sprintf(tra("New %s created with %s %s."),tra("user"),tra("username"),$_REQUEST["name"]));
			} else {
				$tikifeedback[] = array('num'=>1,'mes'=>sprintf(tra("Impossible to create new %s with %s %s."),tra("user"),tra("username"),$_REQUEST["name"]));
			}
		}
	}
	if (isset($tikifeedback[0]['msg'])) {
		$logslib->add_log('adminusers','',$tikifeedback[0]['msg']);
	}
}

//--------------------------------------------------------------------
//Process the form to edit a user
	if (isset($_REQUEST['edituser'])) {

	if ($_REQUEST['edituser'] == 'me'){
    $edituser = $usr->GetProperty('perm_user_id');
  } else $edituser = $_REQUEST['edituser'];

  if($usr->GetProperty('perm_user_id') != $edituser){ //the current user can modify his property
    check_ticket( 'user_modify' , $area_id );
  }

	$edituserInfo = $userlib->get_userid_info($edituser);

  if(isset($_REQUEST['submit'])){
    //Check if name change
		if ($_REQUEST['name']) {
      check_ticket( 'user_modify' , $area_id );
			if ($edituserInfo['handle'] != $_REQUEST['name']) {
				if ($userlib->user_exists($_REQUEST['name'], $LUA)) {
					$smarty->assign('msg', tra("User already exists"));
			  	$smarty->display("error.tpl");
					die;
				}
        $user_data_update['handle'] = $_REQUEST['name'];			
			}
		}

    //Check password syntax and security roles and update password
		if (isset($_REQUEST['pass']) &&  $_REQUEST["pass"]) {
			if ($_REQUEST["pass"] != $_REQUEST["pass2"]) {
				$smarty->assign('msg', tra("The passwords dont match"));
				$smarty->display("error.tpl");
				die;
			}
			if (strlen($_REQUEST["pass"])< MIN_PASS_LENGTH ) {
				$smarty->assign('msg',tra("Password should be at least").' '. MIN_PASS_LENGTH .' '.tra("characters long"));
				$smarty->display("error.tpl");
				die; 	
			}
			if (!preg_match_all( '/'. DEFAULT_PASS_MASK .'/' , $_REQUEST["pass"] , $foo)) {
					$smarty->assign('msg',DEFAULT_PASS_MASK_HELP);
					$smarty->display("error.tpl");
					die;
			}
			/*if ($userlib->change_user_password($_POST['name'],$_POST["pass"])) {
				$tikifeedback[] = array('num'=>0,'mes'=>sprintf(tra("%s modified successfully."),tra("password")));
			} else {
				$tikifeedback[] = array('num'=>0,'mes'=>sprintf(tra("%s modification failed."),tra("password")));
			}*/
      $user_data_update['passwd'] = $_REQUEST['pass'];
		}

    //Check if mail change
		if ($edituserInfo['email'] != $_REQUEST['email']) {
			/*if ($userlib->change_user_email($_POST['name'],$_POST['email'],'')) {
				$tikifeedback[] = array('num'=>0,'mes'=>sprintf(tra("%s changed from %s to %s"),tra("email"),$userinfo['email'],$_POST["email"]));
				//$logslib->add_log('adminusers','','changed email for '.$_POST['name'].' from '.$userinfo['email'].' to '.$_POST["email"]);
				$userinfo['email'] = $_POST['email'];
			} else {
				$tikifeedback[] = array('num'=>1,'mes'=>sprintf(tra("Impossible to change %s from %s to %s"),tra("email"),$userinfo['email'],$_POST["email"]));
			}*/
      $user_data_update['email'] = $_REQUEST['email'];
		}
		/*if ($chlogin) {
			if ($userlib->change_login($userinfo['perm_user_id'],$_POST['name'],$LUA)) {
				$tikifeedback[] = array('num'=>0,'mes'=>sprintf(tra("%s changed from %s to %s"),tra("login"),$userinfo['login'],$_POST["name"]));
				$userinfo['handle'] = $_POST['name'];
			} else {
				$tikifeedback[] = array('num'=>1,'mes'=>sprintf(tra("Impossible to change %s from %s to %s"),tra("login"),$userinfo['email'],$_POST["email"]));
			}
		}*/

    //Process the change request
			if ($userlib->update_user($edituserInfo['perm_user_id'],$user_data_update)) {
				$tikifeedback[] = array('num'=>0,'mes'=>sprintf(tra("%s changed from %s to %s"),tra("login"),$edituserInfo['login'],$_REQUEST["name"]));
			} else {
				$tikifeedback[] = array('num'=>1,'mes'=>sprintf(tra("Impossible to change %s from %s to %s"),tra("login"),$edituserInfo['email'],$_REQUEST["email"]));
			}
  } //End of submit condition

	$edituserInfo = $userlib->get_userid_info($edituser);

  $smarty->assign('edituser', $_REQUEST['edituser']);
  $smarty->assign('userinfo', $edituserInfo);
}

//--------------------------------------------------------------------
//Process the form to suppress user actions request
if ($_REQUEST["action"] == 'delete') {
  check_ticket( 'user_suppress' , $area_id );
	if ($userlib->remove_user($_REQUEST["user"],$LUA)){
  	$tikifeedback[] = array('num'=>0,'mes'=>sprintf(tra("%s %s successfully deleted."),tra("user"),$_REQUEST["user"]));
  } else {
  	$tikifeedback[] = array('num'=>0,'mes'=>sprintf(tra("%s %s deleted failed."),tra("user"),$_REQUEST["user"]));
  }
}
  
//--------------------------------------------------------------------
//Process the form to suppress user from group
if ($_REQUEST["action"] == 'removegroup') {
  check_ticket( 'user_assign_group' , $area_id );
	if ($userlib->remove_user_from_group($_REQUEST["user"], $_REQUEST["group"])) {
		$tikifeedback[] = array('num'=>0,'mes'=>sprintf(tra("%s %s removed from %s %s."),tra("user"),$_REQUEST["user"],tra("group"),$_REQUEST["group"]));
  } else {
		$tikifeedback[] = array('num'=>0,'mes'=>sprintf(tra("%s %s removed from %s %s."),tra("user"),$_REQUEST["user"],tra("group"),$_REQUEST["group"]));
  } 
}

//--------------------------------------------------------------------
//Process action on multi-selection request
if (!empty($_REQUEST["submit_mult"]) && !empty($_REQUEST["checked"])) {
  //Suppress
	if ($_REQUEST['submit_mult'] == 'remove_users' ) {
     check_ticket( 'user_suppress' , $area_id );
			foreach ($_REQUEST["checked"] as $deleteuser) {
				$userlib->remove_user($deleteuser, $LUA);
				$tikifeedback[] = array('num'=>0,'mes'=>sprintf(tra("%s <b>%s</b> successfully deleted."),tra("user"),$deleteuser));
			}
		}
	//Assign group
	} elseif ($_REQUEST['submit_mult'] == 'assign_groups') {
    check_ticket( 'user_assign_group' , $area_id );

		$group_management_mode = TRUE;
		$smarty->assign('group_management_mode', 'y');
		$sort_mode = 'groupName_asc';
		$initial = '';
		$find = '';
		$groups = $userlib->get_groups(0, -1, $sort_mode, $find, $initial, 'n', $userGroups);
		$smarty->assign('groups', $groups['data']);
	} elseif ($_REQUEST['submit_mult'] == 'set_default_groups') {
		$set_default_groups_mode = TRUE;
		$smarty->assign('set_default_groups_mode', 'y');
		$sort_mode = 'groupName_asc';
		$initial = '';
		$find = '';
		$groups = $userlib->get_groups(0, -1, $sort_mode, $find, $initial, 'n', $userGroups);
		$smarty->assign('groups', $groups['data']);
	}

//Process the code for pagination and display management
$default_sort_field = 'handle';    //Default value of the field to sort
$default_sort_order = 'ASC'; //Default value of the order ASC = ascendant, DESC = descandant
include('filterManager.php');

//Display the users list of system
  $list = $userlib->get_users($offset, $numrows, $sort_field, $sort_order, $find, $initial, $LUA);
  $smarty->assign_by_ref('users', $list);

//and get informations about the include groups
  foreach ($list as $user_detail){
    $groups = $userlib->get_users_groups($user_detail['perm_user_id']);
    $include_groups[$user_detail['perm_user_id']] = $groups;
  }
  $smarty->assign_by_ref('include_groups', $include_groups);

/*
//Display the form to edit a user
	if (isset($_GET["edituser"])) {
    check_ticket( 'user_modify' , $area_id );
    $edituser = $_GET["edituser"];
  	$userinfo = $userlib->get_userid_info($edituser, $LUA);
    $smarty->assign('edituser', $edituser);
}
*/

//Include generic definition of the code for manage the pagination. $list must set...
include('paginationManager.php');

$smarty->assign('userId', $_REQUEST["user"]);
//$smarty->assign('username', $username);
//$smarty->assign('usermail', $usermail);

$smarty->assign_by_ref('tikifeedback', $tikifeedback);

// Active the tab
$smarty->assign('usersTab', 'active');

// Display the template
if($_REQUEST['edituser']=='me'){
  $smarty->display("header.tpl");
  $smarty->display("user_adminProperties.tpl");
}else{
  $smarty->assign('mid', 'tiki-adminusers.tpl');
  $smarty->display("ranchbe.tpl");
}

?>
