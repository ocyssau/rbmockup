<?php
session_start();
?>


<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta name="generator" content="Dev-PHP 2.0.11" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="styles.css" rel="stylesheet" type="text/css">
<script language="javascript" src="lib.js">
</head>

<body>

   <div id="BoiteASelectionRappel">
    <p class = entete >Projet séléctionné</p>
    <ul>
      <li class = selected>ProjetA400M</li>
      <li class = comment>Installation système pour a400m</li>
    </ul>
   </div>

   <div id="BoiteASelectionRappel">
    <p class = entete >Affaire séléctionné</p>
    <ul>
      <?php
        echo "<li class = selected>$_SESSION[SelectedAffaire]";
      ?>
			</li>
			<li class = comment>$description de l'affaire</li>
    </ul>
   </div>

   <div id="BoiteASelectionRappel">
    <p class = entete >Maquettes séléctionnées</p>
    <ul>
      <li class = selected>Maq06001</li>
      <li class = comment>Pointe avant A400M</li>
      <li class = selected>Maq06002</li>
      <li class = comment>SAM Pointe avant A400M</li>
    </ul>
   </div>

   <div id="BoiteASelectionRappel">
    <p class = entete >Bibliothèques séléctionnées</p>
    <ul>
      <li class = selected>Lib06001</li>
      <li class = comment>Standard AIRBUS</li>
      <li class = selected>Lib06002</li>
      <li class = comment>Standard SIER</li>
    </ul>
   </div>

   <div id="BoiteASelectionRappel">
    <p class = entete >Utilisateurs et rôles</p>
    <ul>
      <li class = selected>User1</li>
      <li class = comment>Administrateur de projet</li>
      <li class = selected>User2</li>
      <li class = comment>Chargé d'affaire</li>
      <li class = selected>User3</li>
      <li class = comment>Chef de groupe</li>
      <li class = selected>User4</li>
      <li class = comment>Chef de groupe</li>
      <li class = selected>User5</li>
      <li class = comment>Dessinateur</li>
      <li class = selected>User6</li>
      <li class = comment>Dessinateur</li>
      <li class = selected>User7</li>
      <li class = comment>Dessinateur</li>
      <li class = selected>User8</li>
      <li class = comment>Dessinateur</li>
      <li class = selected>User9</li>
      <li class = comment>Dessinateur</li>
      <li class = selected>User10</li>
      <li class = comment>Dessinateur</li>
      <li class = selected>User11</li>
      <li class = comment>Dessinateur</li>
      <li class = selected>User12</li>
      <li class = comment>Dessinateur</li>
      <li class = selected>User13</li>
      <li class = comment>Dessinateur</li>
      <li class = selected>User14</li>
      <li class = comment>Dessinateur</li>
      <li class = selected>User15</li>
      <li class = comment>Dessinateur</li>
      <li class = selected>User16</li>
      <li class = comment>Dessinateur</li>
      <li class = selected>User17</li>
      <li class = comment>Dessinateur</li>
      <li class = selected>User18</li>
      <li class = comment>Dessinateur</li>
      <li class = selected>User19</li>
      <li class = comment>Dessinateur</li>
      <li class = selected>User20</li>
      <li class = comment>Dessinateur</li>
      <li class = selected>User21</li>
      <li class = comment>Dessinateur</li>
    </ul>
   </div>

</body>
</html>