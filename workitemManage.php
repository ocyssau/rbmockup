<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//Setup
require_once './conf/ranchbe_setup.php';
require_once './class/common/container.php'; //Class to manage the container
require_once "HTML/QuickForm.php"; //Librairy to easily create forms
require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm
require_once './lib/Date/date.php';

$Manager = container::_factory('workitem',$_REQUEST['workitem_id']); //Create new manager
$space =& $Manager->space;

$area_id = $Manager->AREA_ID;

check_ticket ( 'container_create', $area_id );

require_once ('conf/galaxia_setup.php');
include_once ('lib/Galaxia/ProcessMonitor.php');
$processList = $processMonitor->monitor_list_processes(0, 9999 , 'lastModif_desc', $find='' , $where='');

require_once './class/project.php'; //Class to manage the projects
$projectManager = new project($space); //Create new manager
$projectList = $projectManager->GetAllProjects();

//Construct the form with QuickForm lib
 $form = new HTML_QuickForm('form', 'post');

//Set defaults values of elements if modify request
  if ($_REQUEST['action'] == 'modify'){

      //Get infos
      $Infos = $Manager->GetInfos();

      $fsCloseDate = $Infos['forseen_close_date'];

      $form->setDefaults(array(
        'workitem_number'      => $Infos['workitem_number'],
        'workitem_description' => $Infos['workitem_description'],
        'project_id'           => $Infos['project_id'],
        'submit'      => 'modify',
      ));

      //Add hidden fields
      $form->addElement('hidden', 'workitem_id', $_REQUEST['workitem_id']);
      $form->addElement('hidden', 'action', 'modify');
      //Assign var 'action' to smarty to manage display
      $smarty->assign('action', 'modify');
  }

//Set defaults values of elements if create request
  if ($_REQUEST['action'] == 'create'){
    $fsCloseDate = time() + ( 3600 * 24 * $DEFAULT_LIFE_TIME );
    $form->setDefaults(array(
      'workitem_description'  => 'New workitem',
      'submit'  => 'create',
    ));

    //Add hidden fields
    $form->addElement('hidden', 'action', 'create');
    //Assign var 'action' to smarty to manage display
    $smarty->assign('action', 'create');
  }

//Add fields for input informations in all case

  $form->addElement('text', 'workitem_number', tra('Number'));
  $form->addElement('textarea', 'workitem_description', tra('Description'), array('size'=>40));

  $smarty->assign('number_help', DEFAULT_WORKITEM_MASK_HELP);

  require_once('./GUI/GUI.php');

  $proc_params = array(
            'field_name' => 'default_process_id',
            'default_value' => $Infos['default_process_id'],
            'field_size' => 1,
            'field_multiple' => false,
            'field_required' => false,
  );
  construct_select_process($proc_params , $form);

  //Construct array for set select project options
  $projectSelectSet['NULL'] = ''; //Leave a blank option for default none selected
  foreach ( $projectList as $project ){
    $projectSelectSet[$project['project_id']] = $project['project_number'];
  }
  $select =& $form->addElement('select', 'project_id', tra('Project'), $projectSelectSet );
  $select->setSize(1);
  $select->setMultiple(false);

  //Contruct date select field
  $date_params = array(
            'field_name' => 'forseen_close_date',
            'default_value' => $fsCloseDate,
            'field_required' => true,
  );
  construct_select_date($date_params , $form);

  //Get fields for custom metadata
  $optionalFields = $Manager->GetMetadata();
  $smarty->assign( 'optionalFields' , $optionalFields);
  foreach($optionalFields as $field){
    if ($_REQUEST['action'] == 'modify')
      $field['default_value'] = $Infos[$field['field_name']];
    construct_element($field , $form); //function declared in GUI.php
  }

  $form->addElement('reset', 'reset', 'reset');
  $form->addElement('submit', 'submit', 'Go');

//Add validation rules to check input data
  //$form->addRule('fsCloseDate', 'fsCloseDate is required', 'required', null, 'server');
  //$form->addRule('fsCloseDate', 'fsCloseDate must be a date', 'date', null, 'server');
  $form->addRule('workitem_number', 'Number is required', 'required', null, 'server');
  $mask = DEFAULT_WORKITEM_MASK;
  $form->addRule('workitem_number', 'This number is not valid', 'regex', "/$mask/" , 'server');
  $form->addRule('project_id', 'Project is required', 'required', null, 'server');
  $form->addRule('project_id', 'Project is required', 'numeric', null, 'server');
  $form->addRule('project_id', 'Project is required', 'nonzero', null, 'server');

// Try to validate the form
if ($form->validate()){
  $form->freeze(); //and freeze it

  $form->setConstants(array( //Re-set value for display correct date in freeze form
   'fsCloseDate' => $form->getSubmitValue('fsCloseDate'),
  ));

// Form is validated, then processes the modify request
  if ($_REQUEST['action'] == 'modify'){
    $form->process('modifyWI', true);
  }

// Form is validated, then processes the create request
  if ($_REQUEST['action'] == 'create'){
    $form->process('createWI', true);
  }
} //End of validate form

//---------------------------
function modifyWI($values){
  global $Manager;
  global $smarty;
  $values = serializeProperties($values);
  $Manager->Update($values);
  //Assign var 'action' to smarty to manage display
  $smarty->assign('action', 'modify');
}

//---------------------------
function createWI($values){
  global $Manager;
  global $smarty;
  $values = serializeProperties($values);
  $Manager->Create($values);
  //Assign var 'action' to smarty to manage display
  $smarty->assign('action', 'create');
}

//---------------------------
function serializeProperties($values){
  if(!is_numeric($values['forseen_close_date']))
    $values['forseen_close_date'] = getTSFromDate( $values['forseen_close_date']);
  foreach($values as $key=>$value){
    if( is_array($value) ){
      $values[$key] = implode('#' , $value);
    }
  }
  return $values;
} //End of function

//Set the renderer for display QuickForm form in a smarty template
include 'QuickFormRendererSet.php';

$Manager->error_stack->checkErrors();

// Display the template
$smarty->assign('workitemsTab', 'active');
$smarty->assign('mid', 'workitemManage.tpl');
$smarty->display('workitemManage.tpl');
//$smarty->display('ranchbe.tpl');

?>
