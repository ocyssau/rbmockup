<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//Setup
require_once './conf/ranchbe_setup.php';
require_once "HTML/QuickForm.php"; //Librairy to easily create forms
require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm
require_once './class/project.php'; //Class to manage the projects

$projectManager = project::_factory();
$area_id = $projectManager->AREA_ID;

check_ticket('project_get', $area_id);

//Manage actions
if (!empty($_REQUEST["action"]) && !empty($_REQUEST["checked"])){

  //Suppress
  if ($_REQUEST['action'] == 'suppress'){
    check_ticket( 'project_suppress'  , $area_id );
    foreach ($_REQUEST["checked"] as $delProject_id){
    	$projectManager->Suppress($delProject_id);
    }
	}//End of Suppress

  //Assign permissions
  if ($_REQUEST['action'] == 'assignPerms'){
    check_ticket( 'project_permissions'  , $area_id );
    foreach ($_REQUEST["checked"] as $Project_id){
      include 'tiki-admingroups.php'; die;
    }//End of loop
	}//End of Assign permissions

} //End of manage action

//Include generic definition of the code for manage filters
$default_sort_field='project_number';    //Default value of the field to sort
$default_sort_order='ASC';    //Default value of the order ASC = ascendant, DESC = descandant
include('filterManager.php');

//Set the option for advanced search of the searchBar
$date_elements = array(
              	'open_date',
              	'close_date',
              	'forseen_close_date',
                );
$smarty->assign_by_ref('date_elements', $date_elements);

//Set the option for advanced search of the searchBar
$user_elements = array(
              	'open_by',
              	'close_by',
                );
$smarty->assign_by_ref('user_elements', $user_elements);

//Get list of users for generate user select in advanced serachBar
global $LUA;
$LUparams = array ( 'container' => 'auth',
                  'orders' => array('handle' => 'ASC'),
                  'limit' => 9999,
                  'offset' => 0,
                  'fields' => array('handle','auth_user_id')
                  );
$smarty->assign_by_ref('user_list', $LUA->getUsers($LUparams));


//get all projects
  $list = $projectManager->GetAllProjects($params);
  $smarty->assign_by_ref('list', $list);

//var_dump($list);

//Include generic definition of the code for manage the pagination. $list must set...
include('paginationManager.php');

//Define select option for "find"
$all_field = array ('project_number' => 'Number',
                    'project_description' => 'Designation',
                    'project_state' => 'State',
                   );
$smarty->assign('all_field', $all_field);

// Display the template
$smarty->assign('projectsTab', 'active');
$smarty->assign('mid', 'projects.tpl');
$smarty->display("ranchbe.tpl");

?>
