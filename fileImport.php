<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+


if( !function_exists(json_decode) ){
  function json_decode ($json)
  {
      $json = str_replace(array("\\\\", "\\\""), array("&#92;", "&#34;"), $json);
      $parts = preg_split("@(\"[^\"]*\")|([\[\]\{\},:])|\s@is", $json, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
      foreach ($parts as $index => $part)
      {
          if (strlen($part) == 1)
          {
              switch ($part)
              {
                  case "[":
                  case "{":
                      $parts[$index] = "array(";
                      break;
                  case "]":
                  case "}":
                      $parts[$index] = ")";
                      break;
                  case ":":
                    $parts[$index] = "=>";
                    break;   
                  case ",":
                    break;
                  default:
                      return null;
              }
          }
          else
          {
              if ((substr($part, 0, 1) != "\"") || (substr($part, -1, 1) != "\""))
              {
                  return null;
              }
          }
      }
      $json = str_replace(array("&#92;", "&#34;", "$"), array("\\\\", "\\\"", "\\$"), implode("", $parts));
      return eval("return $json;");
  } 
}


/**
 * 
 * @param import $Import
 * @param container $Manager
 * @param smarty $smarty
 */
function fileimport_action_upload($Import, $Manager, $smarty)
{
	check_ticket( 'container_import' , $Manager->AREA_ID );
	$dstfile = $Import->IMPORT_DIR .'/'. basename($file['name']);
	require_once('./class/common/file.php');
	file::UploadFile($_FILES['uploadFile'] , $_REQUEST['overwrite'], $dstfile);
}

/**
 * 
 * @param import $Import
 * @param container $Manager
 * @param smarty $smarty
 */
function fileimport_action_suppresspackage($Import, $Manager, $smarty)
{
	check_ticket( 'container_import' , $Manager->AREA_ID );
	if ( empty($_REQUEST['checked']) ) {
		print 'none selected package';
		return;
	}

	foreach ($_REQUEST['checked'] as $import_order ){
		$Import->SuppressPackage($import_order);
	}
}

/**
 * 
 * @param import $Import
 * @param container $Manager
 * @param smarty $smarty
 */
function fileimport_action_uncompresspackage($Import, $Manager, $smarty)
{
	check_ticket( 'container_import' , $Manager->AREA_ID );
	if ( empty($_REQUEST['checked']) ) {
		print 'none selected package';
		return;
	}

	foreach ($_REQUEST['checked'] as $import_order){
		$Import->UncompressPackage($import_order , NULL);
	}
}

/**
 * 
 * @param import $Import
 * @param container $Manager
 * @param smarty $smarty
 */
function fileimport_action_unpack($Import, $Manager, $smarty)
{
	check_ticket( 'container_import' , $Manager->AREA_ID );
	if(empty($_REQUEST['checked'])){
		print 'none selected package';
		return;
	}
	
	set_time_limit(24*3600); //To increase default TimeOut.No effect if php is in safe_mode
	$start_time = time();

	//If we want lunch the process in background
	//See chapter 40 of php documentation
	if($_REQUEST['background'] ){ //Tache execute en arriere plan
		session_write_close(); //To close the current session and unlock the Session. If not do that, its impossible to continue work with another scripts of ranchbe
		ignore_user_abort(true); //Continue execution after deconnection of user

		require_once './lib/Date/date.php';
		print formatDate($start_time).'
		       - La tache s\'execute en arriere plan. Vous recevrez un message lorsqu\'elle sera termin�e.<br />
		       Vous pouvez fermer cette fen�tre.<br />
		       <a href="javascript:window.close()">Close Window</a><br />';
		flush(); // On vide le buffer de sortie
	}else{
		ob_end_flush(); //Switch off flush tempo
	}

	$container_id = $Manager->GetProperty('container_id');
	
	//unpack each package
	foreach ($_REQUEST['checked'] as $import_order){
		if(!$_REQUEST['background'] && USE_HTML_PROGRESS){
			require_once 'HTML/Progress2.php';
			$pb = new HTML_Progress2(null, HTML_PROGRESS2_BAR_HORIZONTAL, 0, 100, false);
			//$pb = new HTML_Progress2();
			$pb->setAnimSpeed(200);
			$pb->setIncrement(10);
			$pb->setIndeterminate(true);
			$pb->addLabel(HTML_PROGRESS2_LABEL_TEXT, 'LABEL1');
			$pb->setLabelAttributes('LABEL1', 'valign=right');

			$add_header = '<style type="text/css"><!--'.$pb->getStyle().'--></style>';
			$add_header .= $pb->getScript(false);
			$smarty->assign_by_ref('additionnal_header' , $add_header);

			require_once 'GUI/progressObserver.php';
			$pbObserver = new progressObserver($pb);

			$Import->attach_all($pbObserver);

			$smarty->display('header.tpl');
			$pb->display();
			//$pb->run();

			echo '<br /><br />';
		}

		if ( !$Import->ImportPackage($import_order, $container_id, $_REQUEST['target']) ){
			//if ( !$import_result ){
			$body = '<b>Not Imported package : </b>'.$Import->PackInfos['package_file_name'].'<br/>';
			$body.= '<b>An error is occured during import task</b><br />';
			$subject = 'Error in import task of '.$Import->PackInfos['package_file_name'];
		}else{
			$body = '<b>Imported package : </b>'.$Import->PackInfos['package_file_name'].'<br/>';
			$subject = 'End of import task of '.$Import->PackInfos['package_file_name'];
		}
		$end_time = time();
		$duration = ($end_time - $start_time);

		include_once './lib/Date/date.php';
		$body.= '<b>target dir : </b>'.$Import->target_dir.'<br/>';
		$body.= '<b>target container : </b>'.$Import->target_container_num.'<br/>';
		$body.= '<b>By : </b>'.$Import->GetUserName().'<br/>';
		$body.= '<b>start to : </b>'.formatDate($start_time).'<br/>';
		$body.= '<b>end to : </b>'.formatDate($end_time).'<br/>';
		$body.= '<b>duration : </b>'.$duration.' sec<br/>';

		if (!$_REQUEST['background']){ //Tache execute en arriere plan
			$error_stack->checkErrors();
			echo $body;
		}

		//Send message to user
		include_once './class/messu/messulib.php';
		$to = $user;
		$from = $user;
		$cc   = '';
		//$body = str_replace('<br/>',"\n",ob_get_contents());
		$body = str_replace('<br/>',"\n",$body);
		$priority = 3;
		$messulib->post_message($to, $from, $to, $cc, $subject, $body, $priority);
	} //End of loop
	die;
}

/**
 * 
 * @param import $Import
 * @param container $Manager
 * @param smarty $smarty
 */
function _fileimport_unpack($Import, $containerId, $packageIds, $type='InContainer')
{
	//unpack each package
	foreach ($packageIds as $packageId){
		if ( !$Import->ImportPackage($packageId, $containerId, $type) ){
			$body = '<b>Not Imported package : </b>'.$Import->PackInfos['package_file_name'].'<br/>';
			$body.= '<b>An error is occured during import task</b><br />';
			$subject = 'Error in import task of '.$Import->PackInfos['package_file_name'];
		}else{
			$body = '<b>Imported package : </b>'.$Import->PackInfos['package_file_name'].'<br/>';
			$subject = 'End of import task of '.$Import->PackInfos['package_file_name'];
		}
		$end_time = time();
		$duration = ($end_time - $start_time);

		include_once './lib/Date/date.php';
		$body.= '<b>target dir : </b>'.$Import->target_dir.'<br/>';
		$body.= '<b>target container : </b>'.$Import->target_container_num.'<br/>';
		$body.= '<b>By : </b>'.$Import->GetUserName().'<br/>';
		$body.= '<b>start to : </b>'.formatDate($start_time).'<br/>';
		$body.= '<b>end to : </b>'.formatDate($end_time).'<br/>';
		$body.= '<b>duration : </b>'.$duration.' sec<br/>';

		if (!$_REQUEST['background']){ //Tache execute en arriere plan
			$error_stack->checkErrors();
			echo $body;
		}

		//Send message to user
		include_once './class/messu/messulib.php';
		$to = $user;
		$from = $user;
		$cc   = '';
		//$body = str_replace('<br/>',"\n",ob_get_contents());
		$body = str_replace('<br/>',"\n",$body);
		$priority = 3;
		$messulib->post_message($to, $from, $to, $cc, $subject, $body, $priority);
	} //End of loop
	die;
}




/**
 * 
 * @param import $Import
 * @param container $Manager
 * @param smarty $smarty
 */
function fileimport_action_viewcontent($Import, $Manager, $smarty)
{
	if(empty($_REQUEST['file'])){
		die('none package selected');
	}
	
	$list = $Import->ListContentImportPackage($_REQUEST['file'], false);
	
	print('<h1><b>Content of '.$_REQUEST['file'].'</b></h1><ul>');
	print '<table border=1>';
	print('<tr><td><b>file_name</b></td><td><b>mtime</b></td><td><b>size</b></td><td><b>gid</b></td><td><b>uid</b></td></tr>');
	$count = 0;
	while($list->next()){
		$count++;
		$stat = $list->getStat();
		print '<tr>';
		print('<td><b>'.$list->getFilename().'</b></td>'); //Get the name of the unpack file
		print('<td>'.strftime(LONG_DATE_FORMAT, $stat['mtime']).'</b></td>');
		print('<td>'.sprintf ("%.2f k%s",($stat['size']/1024),"o").'</b></td>');
		print('<td>'.$stat['gid'].'</b></td>');
		print('<td>'.$stat['uid'].'</b></td>');
		print '</tr>';
	}
	print '</table>';
	print '<b>'.$count.' elements in this package<br /></b>';
	die;
}

/**
 * 
 * @param import $Import
 * @param container $Manager
 * @param smarty $smarty
 */
function fileimport_action_viewImported($Import, $Manager, $smarty)
{
	if(!empty($_REQUEST['import_order'])){
		$smarty->assign( 'import_order' , $_REQUEST['import_order']);
		$all_field = array ('file_name' => 'Name',
                          'file_extension' => 'Extensions',
                          'file_path' => 'Path',
                          'file_open_by' => 'Created by',
                          'file_open_date' => 'Created date',
                          'file_update_by' => 'Last update by',
                          'file_update_date' => 'Last update date',
                          'file_access_code' => 'Access',
                          'file_state' => 'State',
                          'file_version' => 'Version',
                          'file_type' => 'Type',
                          'file_size' => 'Size',
                          'file_md5' => 'Md5',
		);
		$smarty->assign('all_field', $all_field);

		//Assign name to particular fields
		$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);

		//Include generic definition of the code for manage filters
		include('filterManager_simple.php');

		//get all files
		$list = $Import->ListImportedFiles($_REQUEST['import_order'] , $params);
		$smarty->assign_by_ref('list', $list);

		//Include generic definition of the code for manage the pagination. $list must set...
		include('paginationManager.php');

		//Assign additional var to add to URL when redisplay the same url
		$sameurl_elements[]='action';
		$sameurl_elements[]='import_order';

		$smarty->display("importedFiles.tpl");
	}
	else{
		die('none package selected');
	}
	die;
}

/**
 * 
 * @param import $Import
 * @param container $Manager
 * @param smarty $smarty
 */
function fileimport_action_adddescription($Import, $Manager, $smarty){
	check_ticket( 'container_import' , $Manager->AREA_ID );
	
	if ( empty($_REQUEST['import_order']) ){
		die('none package selected');
	}

	require_once "HTML/QuickForm.php"; //Librairy to easily create forms
	$form = new HTML_QuickForm('form', 'post'); //Construct the form with QuickForm lib

	//Get the current description
	$desc = $Import->GetPackageDescription( $_REQUEST['import_order'] );

	//Set defaults values of elements
	$form->setDefaults(array('description' => $desc));

	//Add hidden fields
	$form->addElement('hidden', 'import_order', $_REQUEST['import_order']);
	$form->addElement('hidden', 'action', 'addDescription');

	//Add fields for input informations
	$form->addElement('textarea', 'description', 'Description', array('rows' => 3, 'cols' => 20));

	$form->addElement('reset', 'reset', 'reset');
	$form->addElement('submit', 'submit', 'Go');

	// Try to validate the form
	if ($form->validate()) {
		$form->freeze(); //and freeze it
		// Process the modify request
		$Import->AddImportPackageDescription($_REQUEST['description'], $_REQUEST['import_order']);
	} //End of validate form

	//Set the renderer for display QuickForm form in a smarty template
	include 'QuickFormRendererSet.php';

	// Display the template
	$smarty->display('importAddDescription.tpl');
	die;
}

/**
 * 
 * @param import $Import
 * @param container $Manager
 * @param smarty $smarty
 */
function fileimport_action_createbatchjob($Import, $Manager, $smarty)
{
	//Create the job
	if( isset($_REQUEST['data-jsonencoded']) ){
		require_once('class/Batchjob.php');
		
		$data = json_decode($_REQUEST['data-jsonencoded']);
		//$data = array();
		$batchJob = new Rbplm_Batchjob( array('data'=>$data, 'runner'=>Rbplm_Batchjob::RUNNER_FILEIMPORT) );
		$batchJob->setPlanned( time() );
		$jId = $batchJob->create();
		if(!$jId){
			header('HTTP/1.1 500 Internal Server Error');
		}
		else{
			echo 'Id of new JOB is :' . $jId;
		}
		
		//attach job to package
		foreach($data as $map){
			$packageId = $map[0];
			$targetId = $map[1];
			$Import->UpdateImportPackage(array('jobid'=>$jId) , $packageId);
		}
	}
	//Display creation screen
	else{
		if(empty($_REQUEST['checked'])){
			print 'none selected package';
			return;
		}
		
		$list = array();
		foreach ($_REQUEST['checked'] as $import_order){
			$list[] = $Import->GetPackageInfos( $import_order );
		}
		$smarty->assign_by_ref('list', $list);
		
		$mockups = $Manager->GetAll( array('select'=>array('mockup_id', 'mockup_number', 'mockup_description'), 'where'=>'file_only=1' ) );
		$smarty->assign('mockups', $mockups);
		
		// Display the template
		$smarty->display('fileimport_createbatchjob.tpl');
	}
	die;
}


/**
 * 
 * @param import $Import
 * @param container $Manager
 * @param smarty $smarty
 */
function fileimport_action_viewLogFile($Import, $Manager, $smarty)
{
	$logFile = $Import->GetLogFile($_REQUEST['import_order']);
	require_once('class/common/fsdata.php');
	$fsdata = new fsdata($logFile);
	if($fsdata->DownloadFile()){
		die;
	}
	else{
		$Import->error_stack->push(ERROR, 'Warning', array('element'=>$logFile), 'The logfile %element% don\'t exist');
	}
}

/**
 * 
 * @param import $Import
 * @param container $Manager
 * @param smarty $smarty
 */
function fileimport_action_selectcontainerforimport($Import, $Manager, $smarty)
{
	//Re assign the containerId
	$smarty->assign( 'container_id' , $_REQUEST['container_id'] );
}

/**
 * 
 * @param import $Import
 * @param container $Manager
 * @param smarty $smarty
 */
function fileimport_action_refresh($Import, $Manager, $smarty){
}

/**
 * 
 * @param import $Import
 * @param container $Manager
 * @param smarty $smarty
 */
function fileimport_action_display($Import, $Manager, $smarty)
{
	//Include generic definition of the code for manage filters
	include('filterManager.php');

	set_time_limit(24*3600); //To increase default TimeOut.No effect if php is in safe_mode

	$list = $Import->GetAllImportPackage($params); //get infos on all package in deposit directories
	$smarty->assign_by_ref('list', $list);

	//Assign name to particular fields
	$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
	$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number' );
	$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description' );
	$smarty->assign('CONTAINER_STATE' , $Manager->SPACE_NAME . '_state' );
	$smarty->assign('CONTAINER_ID' , $Manager->SPACE_NAME . '_id' );

	$smarty->assign('mid', 'fileImport.tpl');

	$Manager->error_stack->checkErrors();

	// Display the template
	$smarty->assign('randWindowName', 'file import');
	$_SESSION['myspace'] = array('activePage'=>$_SERVER['PHP_SELF']);
	$smarty->assign('documentManage' , 'active');
	$smarty->assign('mid', 'fileImport.tpl');
	$smarty->display('ranchbe.tpl');
}


//this script may only be included - so its better to die if called directly.
if (strpos($_SERVER["SCRIPT_NAME"], basename(__FILE__)) !== false) {
	header("location: index.php");
	exit;
}

if ( !isset($Manager) ) {
	print '$Manager is not set '; 
	die;
}

require_once './class/common/import.php'; //Class to manage the importations
$Import = new import($Manager->SPACE_NAME , $Manager);

//$smarty->assign( 'container_id' , $Manager->GetProperty('container_id'));
//$smarty->assign( 'container_number' , $Manager->GetProperty('container_number'));
$smarty->assign( 'action' , $_REQUEST['action']);

//Call the action
if( $_REQUEST['action'] ){
	$actionFunc = 'fileimport_action_' . strtolower($_REQUEST['action']);
	call_user_func_array($actionFunc, array($Import, $Manager, $smarty));
}

//display
fileimport_action_display($Import, $Manager, $smarty);


