<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//this script may only be included - so its better to die if called directly.
if (strpos($_SERVER["SCRIPT_NAME"],basename(__FILE__)) !== false) {
  header("location: index.php");
  exit;
}

// Lib for user administration, groups and permissions
// This lib uses pear so the constructor requieres
// a pear DB object

class UsersLib {

var $AREA_ID = 1;

//To check if user exist with LiveUser Admin class and return userid
  function user_exists($user) {

      global $LUA;

      $filterUser = array ('handle' => $user,);
      $params = array ( 'container' => 'auth',
                       'filters' => $filterUser
                      );
      
      if ($users = $LUA->getUsers($params)) {
       //return $users[perm_user_id];
       return true;
      }
      else {
       return false;
      }
  }

//----------------------------------------------------------------

//To get information about the user
  function get_userid_info($perm_user_id) {
  
      global $LUA;
  
      $filterUser = array ('perm_user_id' => $perm_user_id);
      $params = array ( 'container' => 'perm',
                        'filters' => $filterUser,
                      );
      if ($usersInfos = $LUA->getUsers($params)) {
       return $usersInfos[0];
      }
      else {
       return false;
      }
  }

//----------------------------------------------------------------

//To get information about the user
  function get_user_name($perm_user_id) {
  
      global $LUA;
  
      $filterUser = array ('perm_user_id' => $perm_user_id);
      $params = array ( 'container' => 'perm',
                        'filters' => $filterUser,
                        'field' => array('handle'),
                      );
      if ($usersInfos = $LUA->getUsers($params)) {
       return $usersInfos[0]['handle'];
      }
      else {
       return false;
      }
  } //End of method

//----------------------------------------------------------------

//To get information about the user
  function get_user_email($user) {
  
      global $LUA;
  
      $filterUser = array ('perm_user_id' => $user);
      $params = array ( 'container' => 'auth',
                        'filters' => $filterUser,
                      );
      if ($usersInfos = $LUA->getUsers($params)) {
       return $usersInfos[0]['email'];
      }

      return false;

  } //End of method

//----------------------------------------------------------------

//To get information about the user
  function get_user_id($handle) {
  
      global $LUA;
  
      $filterUser = array ('handle' => $handle);
      $params = array ( 'container' => 'auth',
                        'fields' => array('auth_user_id'),
                        'filters' => $filterUser,
                      );
      if ($usersInfos = $LUA->getUsers($params)) {
       return $usersInfos[0]['auth_user_id'];
      }

      return false;

  } //End of method

//----------------------------------------------------------------

//To get perm_user_id from a user
  function get_perm_user_id($handle) {
  
      global $LUA, $dbranchbe;

      $query = "SELECT perm_user_id FROM liveuser_perm_users, liveuser_users WHERE
      liveuser_users.auth_user_id = liveuser_perm_users.auth_user_id
      AND
      liveuser_users.handle = '$handle' 
      ";
      
      if(!$infos = $dbranchbe->Execute($query)){
        print $dbranchbe->ErrorMsg().'<BR>';
        return false;
      }

      $infos = $infos->GetArray(); //To transform object result in array;
      return $infos[0]['perm_user_id'];

  } //End of method

//----------------------------------------------------------------

//To add a user
  function add_user($user, $pass, $pass2, $email) {
  	// Generate a unique hash; this is also done below in set_user_fields()
  	//$hash = md5($user . $pass . $email);

    global $LUA;

  	$hash = md5($user . $pass);
  
  	if ($feature_clear_passwords == 'n')
  	    $pass = '';
  
     $data = array('handle'    => $user,
                   'passwd'    => $pass,
                   'email'     => $email,
                   'is_active' => 1,
                   'perm_type' => 0,
                   );
     $userId = $LUA->addUser($data);
      if ($userId === false)
        {
        return false;
        }
      else
        {
        return "$userId";
        }
  }

//----------------------------------------------------------------

//To update a user
  function update_user($perm_user_id, $data) {
    global $LUA;
    return $LUA->updateUser($data, $perm_user_id);
  }

//----------------------------------------------------------------
 
//To remove a user of LiveUser tables
  function remove_user($permId, $LiveUserObject) {
      //$userid = $this -> user_exists($user, $LiveUserObject);
      if ( $LiveUserObject->removeUser($permId) ){
       return true;
      }
      else {
      return false;
      }
  }

//----------------------------------------------------------------
 
//To remove a group of LiveUser tables
  function remove_group($groupId, $LiveUserObject) {
      $filter = array ('group_id' => $groupId);
      if ( $LiveUserObject->perm->removeGroup($filter)) {
       return true;
      }
      else {
      return false;
      }
  }

//----------------------------------------------------------------

//To list the users
  function get_users($offset=0, $numrows=100, $sort_field='handle', $sort_order='ASC', $find='', $select=NULL) {

  global $LUA;

  $sort_array = array($sort_field => $sort_order);
  //$find = 'john';
  //$find_array = array ($field => $find); //exemple : $field = 'handle' et $find = 'jean'
    //limit of items number to display, offset items from number one for display, order of sort
    //container is internal to liveuser
    //filters dont work correctly
  $params = array ( 'container' => 'auth',
                    'orders' => $sort_array,
                    'limit' => $numrows,
                    'offset' => $offset,
                  );
  return ($users = $LUA->getUsers($params));
      if ($users === false)
        {
        echo '<p>get_users: error on line: '.__LINE__.'</p>';
        print_r($LUA->getErrors());
        }
  }

//----------------------------------------------------------------

//To list the groups of all users
  function get_users_groups($user_id) {
  //Get LiveUser users with parameter rekey = return a array with key=perm_user_id,
  //Group parameter : For groups all record that have same perm_user_id in a array.
  //So this function return a users => groups list.

  global $LUA;

  $params = array ( 'fields' => array('perm_user_id', 'group_id'),
                    'rekey' => 'true',
                    'group' => 'true',
                    );

  $params = array( 'filters' => array(
                   'perm_user_id' => $user_id
                  ) );

  $groups = $LUA->perm->getGroups($params);
  if ($groups === false)
    {
    echo '<p>get_users_groups: error on line: '.__LINE__.'</p>';
    print_r($LUA->getErrors());
    }
  else return $groups;

  } //End of method

//----------------------------------------------------------------

//To list the groups
  function get_groups($offset='0', $numrows='1000', $sort_field='group_define_name', $sort_order='ASC', $find='', $LiveUserObject='') {

    global $LUA;
    if (empty ($LiveUserObject)) $LiveUserObject = $LUA;
    
    $sort_array = array($sort_field => $sort_order);
    $params = array ( 'container' => 'auth',
                      'orders' => $sort_array,
                      'limit' => $numrows,
                      'offset' => $offset,
                    );
    return ($groups = $LiveUserObject->getGroups($params));
        if ($groups === false)
          {
          echo '<p>get_groups: error on line: '.__LINE__.'</p>';
          print_r($LUA->getErrors());
          }
  }

//----------------------------------------------------------------

//To list the groups
  function get_all_groups($offset='0', $numrows='1000', $sort_field='group_define_name', $sort_order='ASC', $find='') {
    global $LUA;
    $sort_array = array($sort_field => $sort_order);
    $params = array ( 'container' => 'auth',
                      'orders' => $sort_array,
                      'limit' => $numrows,
                      'offset' => $offset,
                    );
    $groups = $LUA->getGroups($params);
      if ($groups === false)
        {
        echo '<p>get_groups: error on line: '.__LINE__.'</p>';
        print_r($LUA->getErrors());
        }else return $groups;

  } //End of method

//----------------------------------------------------------------

//To get group infos
  function get_group_info($groupid, $LiveUserObject) {

   if (is_numeric($groupid) ) {
    $filterUser = array ('group_id' => $groupid);
   }
   else {
    $filterUser = array ('group_define_name' => $groupid);
   }

   $params = array ( 'container' => 'auth',
                     'filters' => $filterUser
                   );

   if ($group = $LiveUserObject->getGroups($params)) {
    return ($group);
   }
   else {
    echo '<p>get_group_info: error on line: '.__LINE__.'</p>';
    print_r($LiveUserObject->getErrors());
    return false;
   }
  }

//----------------------------------------------------------------

//To get group id
  function get_group_id($group_define_name) {

    global $LUA;
  
    $filter = array('filters' => array('group_define_name' => $group_define_name));
    
    if ($group = $LUA->perm->getGroups($filter)){
      return ($group[0]['group_id']);}
    else {
      echo '<p>get_group_id: error on line: '.__LINE__.'</p>';
      print_r($LUA->getErrors());
      return false;
    }
  }

//----------------------------------------------------------------


//To check if a group exist
  function group_exists($group) {

    global $LUA;

    if(is_numeric($group))
    $params = array ( 'filters' => array ('group_id' => $group)
                     );
    else
    $params = array ( 'filters' => array ('group_define_name' => $group)
                     );

    if ($LUA->perm->getGroups($params)) {
     return true;
    } else {
     return false;
    }
  }

//----------------------------------------------------------------

//To change a group
  function change_group($group_id, $group_define_name, $GroupDesc, $LiveUserObject) {
    $filters  = array ('group_id' => $group_id);
    $params = array ( 'group_define_name' => $group_define_name,
                      'group_description' => $GroupDesc,
                    );
    if ($LiveUserObject->perm->updateGroup($params, $filters)) {
     return true;
    } else {
     return false;
    }
  }

//----------------------------------------------------------------

//To retrieve groups include from perm_user_id
  function get_user_groups_inclusion($perm_user_id) {

    global $LUA;

    $params = array ( 'filters' => array ('perm_user_id' => $perm_user_id)
                    );
    $group_list =  $LUA->perm->getGroups($params);

    if ($group_list) {
     return $group_list;
    } else {
     return false;
    }
  }

//----------------------------------------------------------------

//To retrieve groups include from perm_user_id
  function get_group_users($group='') {

    global $dbranchbe;

    $query = "SELECT * FROM `liveuser_groups` JOIN `liveuser_groupusers`
              ON `liveuser_groups`.`group_id` = `liveuser_groupusers`.`group_id` JOIN
              `liveuser_perm_users` ON `liveuser_perm_users`.`perm_user_id` = `liveuser_groupusers`.`perm_user_id`
              WHERE `liveuser_groups`.`group_id` = $group
              ";

    if(!$all = $dbranchbe->Execute($query)){
      print 'error get_group_users: '.$dbranchbe->ErrorMsg().'<BR>';
      return FALSE;
    }else{
      $all = $all->GetArray(); //To transform object result in array;
      return $all;
    }


  }//End of method

//----------------------------------------------------------------

//To assign a user to a group
  function assign_user_to_group($user, $group) {

    global $LUA;

    if(!is_numeric($user))
      $perm_user_id = $this->get_perm_user_id($user);
    else
      $perm_user_id = $user;
    if(!is_numeric($group))
      $group_id = $this->get_group_id($group);
    else
      $group_id = $group;

    $params = array ( 'perm_user_id' => $perm_user_id,
                      'group_id' => $group_id,
                    );
    if ($LUA->perm->addUserToGroup($params)) {
     return true;
    } else {
     return false;
    }
  }

//----------------------------------------------------------------

//To remove a user from a group
  function remove_user_from_group($perm_user_id, $group_id, $LiveUserObject) {
    $params = array ( 'perm_user_id' => $perm_user_id,
                      'group_id' => $group_id,
                    );
    if ($LiveUserObject->perm->removeUserFromGroup($params)) {
     return true;
    } else {
     return false;
    }
  }

//----------------------------------------------------------------

//To add a group
  function add_group($GroupName, $GroupDesc, $LiveUserObject) {
    $params = array ( 'group_define_name' => $GroupName,
                      'group_id' => $group_id,
                      'group_description' => $GroupDesc,
                    );
    if ($Group_id = $LiveUserObject->perm->addGroup($params)) {
     return true;
    } else {
     return false;
    }
  }

//----------------------------------------------------------------

//To list permisions types
  function get_permissions_types() {
  }

//----------------------------------------------------------------

//To list permissions level
  function get_permission_levels() {
  }

//----------------------------------------------------------------

//To list permissions
  function get_permissions($offset, $maxRecords, $sort_field, $sort_order, $find, $type, $LiveUserObject, $area_id ) {

    global $LUA;

    $sort_array = array($sort_field => $sort_order);
    $params['orders'] = $sort_array;
    $params['limit'] = $numrows;
    $params['offset'] = $offset;
    if (!empty($area_id)) $params['filters'] = array('area_id'=>$area_id);

    if (!$rights = $LUA->perm->getRights($params)){
      echo '<p>get_permissions: error on line: '.__LINE__.'</p>';
      print_r($LUA->getErrors());
      return false;
      }
    return $rights;
  }

//----------------------------------------------------------------

//To list permissions of a group 
  function get_group_permissions( $group, $LiveUserObject ) {
    $params = array ( 'fields' => array('group_id', 'right_id'),
                      'with' => array ('right_id' => ''),
                      'filters' => array ('group_id' => $group  ),
                    );

    return ($rights = $LiveUserObject->getGroups($params));
        if ($rights === false)
          {
          echo '<p>get_group_permissions: error on line: '.__LINE__.'</p>';
          print_r($LUA->getErrors());
          }
  }

//----------------------------------------------------------------

//To change permission level
		function change_permission_level(){
    }

//----------------------------------------------------------------

//To assign permission to a group
		function assign_permission_to_group($right_id, $group_id, $LiveUserObject){
      $params = array ('right_id' => $right_id,
                       'group_id' => $group_id,
                      );
      return ($added = $LiveUserObject->perm->grantGroupRight($params));
          if ($rights === false)
            {
            echo '<p>assign_permission_to_group: error on line: '.__LINE__.'</p>';
            print_r($LUA->getErrors());
            }
    }

//----------------------------------------------------------------

//To remove permission from a group
		function remove_permission_from_group($right_id, $group_id, $LiveUserObject){
      $params = array ('right_id' => $right_id,
                       'group_id' => $group_id,
                      );
      return ($suppress = $LiveUserObject->perm->revokeGroupRight($params));
          if ($rights === false)
            {
            echo '<p>assign_permission_to_group: error on line: '.__LINE__.'</p>';
            print_r($LUA->getErrors());
            }
    }//End of function

//----------------------------------------------------------------

//To exports the right in a conf file for use comprehensible name
		function export_rights( $area_id = 1 ) {

		  global $LUA;
		  
		  //Get the area define name
      //$filter = array('filters' => array('area_id' => $area_id));
		  //$area_infos = $LUA->perm->getAreas( $filter );
      
      $filename = 'rights_definition/area_' . "$area_id" . '.php'; //See createlu.php, function load_rights_def.		  
      $type = 'php';
      $mode = 'file';
      $params = array ('filename' => "$filename" ,
                       'area'     => "$area_id" ,
                       );

      $output = $LUA->perm->outputRightsConstants($type, $params, $mode );
      if ($output === false){
        echo '<p>export_rights : error on line: '.__LINE__.'</p>';
        print_r($LUA->getErrors());
        return false;
        } else return true;
    }//End of function

//----------------------------------------------------------------

//To get functionnal areas
		function get_area() {

		  global $LUA;
		  return $area_infos = $LUA->perm->getAreas();

    }//End of function

//----------------------------------------------------------------

//To add a functionnal area
		function add_area( $define_name) {

		  global $LUA;
		  
      $params = array ('area_define_name' => $define_name ,
                       'application_id'   => 1 ,
                       );

      $areaId = $LUA->perm->addArea($params);

      if ($areaId === false){
        echo '<p>add_area : error on line: '.__LINE__.'</p>';
        print_r($LUA->getErrors());
        return false;
        } else return $areaId;

    }//End of function

//----------------------------------------------------------------

//To modify a functionnal area
		function modify_area( $define_name , $area_id ) {

		  global $LUA;
		  
      $filter = array('area_id' => $area_id);
      $params = array ('area_define_name' => "$define_name",
                       );
      $areaId = $LUA->perm->updateArea($params , $filter);

      if ($areaId === false){
        echo '<p>modify_area : error on line: '.__LINE__.'</p>';
        print_r($LUA->getErrors());
        return false;
        } else return true;

    }//End of function

//----------------------------------------------------------------

//To suppress a functionnal area and rights associated
		function suppress_area( $area_id ) {

		  global $LUA;

      $this->suppress_right( $area_id );

      $filter = array('area_id' => $area_id);

      $result = $LUA->perm->removeArea($filter);

      if ($result === false){
        echo '<p>suppress_area : error on line: '.__LINE__.'</p>';
        print_r($LUA->getErrors());
        return false;
      } else return true;

    }//End of function

//----------------------------------------------------------------

//To add a right
		function add_right( $area_id  , $right_name , $right_description) {

		  global $LUA;

      $params = array('area_id' => $area_id,
                      'right_define_name' => $right_name,
                      'right_description' => $right_description,
                      'has_implied' => '0');

      $rightId = $LUA->perm->addRight($params);

      if ($rightId === false){
        echo '<p>add_right : error on line: '.__LINE__.'</p>';
        print_r($LUA->getErrors());
        return false;
        } else return $rightId;

    }//End of function

//----------------------------------------------------------------

//To get rights
		function get_right($area_id , $right_define_name) {

      global $LUA;
    
      $params = array ('filters'=>array(
                          'area_id' => $area_id,
                          'right_define_name' => $right_define_name,
                        )
                      );
      $rights = $LUA->perm->getRights($params);
      if ($rights === false){
        echo '<p>get_right: error on line: '.__LINE__.'</p>';
          print_r($LUA->getErrors());
        }else return $rights;

    }//End of function

//----------------------------------------------------------------

//To suppress a right 
		function suppress_right( $area_id ) {

		  global $LUA;

      $filter = array('area_id' => $area_id);
      $removeRight = $LUA->perm->removeRight($filter);

      if ($removeRight === false){
        echo '<p>suppress_right : error on line: '.__LINE__.'</p>';
        print_r($LUA->getErrors());
        return false;
      } else return true;

    }//End of function

} //End of class

$userlib = new UsersLib();

?>
