// Fonction pour afficher une fenetre pop-up
	 function popup(page_name, window_name)
         {
          window.open (page_name, window_name, config='height=300, width=900, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no')
         }

// Fonction pour afficher une fenetre pop-up large
	 function popup2(page_name, window_name)
         {
          window.open (page_name, window_name, config='height=300, width=800, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no')
         }

// Fonction pour afficher une fenetre pop-up large
	 function popup3(page_name, window_name, window_width)
         {
          window.open (page_name, window_name, "height=200, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no")
         }

// Fonction pour afficher une fenetre pop-up
	 function popup8x6(page_name, window_name)
         {
          window.open (page_name, window_name, config='height=600, width=800, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no');
         }

// Fonction pour afficher une fenetre pop-up dont la taille est parametrable
  function popupP(page_name, window_name , height , width)
    {
    var option = "height=" + height + ", width=" + width + ", toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no";
    window.open (page_name, window_name, config=option);
    }

// fonction pour selections multiples d'options
     function SversD() 
      {
      indexS=document.trans.source.options.selectedIndex;
      if (indexS < 0) return;
      valeur=document.trans.source.options[indexS].text;
      document.trans.source.options[indexS]=null;
      a = new Option(valeur);
      indexD=document.trans.MetaAffProjectContentList.options.length;
      document.trans.MetaAffProjectContentList.options[indexD]=a;
      }

     function DversS() 
      {
      indexD=document.trans.MetaAffProjectContentList.options.selectedIndex;
      if (indexD < 0) return;
      valeur=document.trans.MetaAffProjectContentList.options[indexD].text;
      document.trans.MetaAffProjectContentList.options[indexD]=null;
      a = new Option(valeur);
      indexS=document.trans.source.options.length;
      document.trans.source.options[indexS]=a;
      }

// fonction pour clore une fenetre
      function closeWindow()
      {
      window.close();
      }

// fonction pour ouvrir un formulaire dans un popup
//http://www.asp-php.net/ressources/codes/JavaScript-Ouvrir+un+popup+avec+un+envoi+POST.aspx
//Merci CrazyCat
      function pop_it(the_form , height , width) {
        if(typeof height == 'undefined') height=300;
        if(typeof width == 'undefined') width=900;
         my_form = eval(the_form);
         popupP("./wait.php", "popup" , height , width);
         my_form.target = "popup";
         my_form.submit();
      }

      function pop_no(my_form) {
         my_form.target = '';
         my_form.submit();
      }

/**
 * This array is used to remember mark status of rows in browse mode
 */
var marked_row = new Array;

//
//Check / Uncheck all Checkboxes
//
function switchCheckboxes(tform, elements_name, state) {
	// checkboxes need to have the same name elements_name
	// e.g. <input type="checkbox" name="my_ename[]">, will arrive as Array in php.
	for (var i = 0; i < tform.length; i++) {
		if (tform.elements[i].name == elements_name) {
			tform.elements[i].checked = state
		}
	}
	PMA_markRowsInit();
	return true;
}


/**
 * enables highlight and marking of rows in data tables
 * Reused from function.js scripts of phpmyadmin : @version $Id: lib.js,v 1.4 2008/05/15 13:22:06 ranchbe Exp $
 * http://www.phpmyadmin.net
 *
 */
function PMA_markRowsInit() {
    // for every table row ...
    var rows = document.getElementsByTagName('tr');
    var count = Rb_countSelectedRowsInit();
    var displaybox = document.getElementById( 'displaySelectedRowCount' );

    for ( var i = 0; i < rows.length; i++ ) {
        if ( 'odd' != rows[i].className.substr(0,3) && 'even' != rows[i].className.substr(0,4) ) {
            continue;
        }
        var checkbox = rows[i].getElementsByTagName('input')[0];
        if ( checkbox && checkbox.type == 'checkbox' ) {
            unique_id = checkbox.name + checkbox.value;
            marked_row[unique_id] = checkbox.checked;
            if(checkbox.checked){
            	rows[i].className = rows[i].className.replace(' marked', '');
                rows[i].className += ' marked';
            }else{
            	rows[i].className = rows[i].className.replace(' marked', '');
            }
        }
    }
    
    for ( var i = 0; i < rows.length; i++ ) {
        // ... with the class 'odd' or 'even' ...
        if ( 'odd' != rows[i].className.substr(0,3) && 'even' != rows[i].className.substr(0,4) ) {
            continue;
        }
        // ... add event listeners ...
        // ... to highlight the row on mouseover ...
        if ( navigator.appName == 'Microsoft Internet Explorer' ) {
            // but only for IE, other browsers are handled by :hover in css
            rows[i].onmouseover = function() {
                this.className += ' hover';
            }
            rows[i].onmouseout = function() {
                this.className = this.className.replace( ' hover', '' );
            }
        }
        // Do not set click events if not wanted
        if (rows[i].className.search(/noclick/) != -1) {
            continue;
        }
        // ... and to mark the row on click ...
        rows[i].onmousedown = function() {
            var unique_id;
            var checkbox;

            checkbox = this.getElementsByTagName( 'input' )[0];
            if ( checkbox && checkbox.type == 'checkbox' ) {
                unique_id = checkbox.name + checkbox.value;
            } else if ( this.id.length > 0 ) {
                unique_id = this.id;
            } else {
                return;
            }

            if ( typeof(marked_row[unique_id]) == 'undefined' || !marked_row[unique_id] ) {
                marked_row[unique_id] = true;
            } else {
                marked_row[unique_id] = false;
            }

            if ( marked_row[unique_id] ) {
                this.className += ' marked';
            } else {
                this.className = this.className.replace(' marked', '');
            }

            if ( checkbox && checkbox.disabled == false ) {
                checkbox.checked = marked_row[unique_id];
            }
            
            // update count
            if(displaybox){
                if(checkbox.checked == true){
                	count = count + 1;
            	}else{
                	count = count - 1;
                }
                displaybox.innerHTML = count;
            }
            
        }

        // ... and disable label ...
        var labeltag = rows[i].getElementsByTagName('label')[0];
        if ( labeltag ) {
            labeltag.onclick = function() {
                return false;
            }
        }
        
        // .. and checkbox clicks
        var checkbox = rows[i].getElementsByTagName('input')[0];
        if ( checkbox ) {
            checkbox.onclick = function() {
                // opera does not recognize return false;
                this.checked =! this.checked;
            }
        }
        
        //... and update the display ...
        if(displaybox){
            displaybox.innerHTML = count;
        }
        
    }
}
window.onload=PMA_markRowsInit;


/**
 * Display the count of row selected by PMA_markRowsInit
 */
function Rb_countSelectedRowsInit() {
    // for every table row ...
    var count = 0;
    var rows = document.getElementsByTagName('tr');
    for ( var i = 0; i < rows.length; i++ ) {
        // ... with the class 'odd' or 'even' ...
        if ( 'odd' != rows[i].className.substr(0,3) && 'even' != rows[i].className.substr(0,4) ) {
            continue;
        }
        var checkbox;
        checkbox = rows[i].getElementsByTagName( 'input' )[0];
        if ( checkbox && checkbox.disabled == false && checkbox.checked == true ) {
        	count++;
        }
    }
    return count;
}



/** Ajax function to get query DocumentLiveSearchServer
 * 
 * @param string
 * @param string
 * @param string
 * @param string
 * @return
 */
function ls_showResult(str, inputId, listId, serverUrl){
	
	document.getElementById(listId).style.display = 'block';
	
	if (str.length==0)
	{
	  document.getElementById(listId).innerHTML = "";
	  document.getElementById(listId).style.border = "0px";
	  //document.getElementById(inputId).value = "";
	  return;
	}
	
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp = new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
	  xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.onreadystatechange = function()
	{
	    if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
		    document.getElementById(listId).innerHTML = xmlhttp.responseText;
		    document.getElementById(listId).style.border = "1px solid #A5ACB2";
		    //document.getElementById(inputId).value = str;
	    }
	}
	if (serverUrl.length > 0){
		var reg = new RegExp("(%string%)", "g");
		serverUrl = serverUrl.replace(reg, str); //replace %string% by str
		xmlhttp.open("GET", serverUrl, true);
		xmlhttp.send();
	}else{
	    document.getElementById(inputId).value = str;
	}
	
}


/** To input value in element id with method value
 * 
 * @param string
 * @param string
 * @param string
 * @return
 */
function ls_selectResult(inputstring, outputstring, inputId){
	
	outputId = inputId + "_output";
	
	if (inputstring.length==0)
	{
	  document.getElementById(inputId).value = "";
	  document.getElementById(outputId).value = "";
	}else{
	    document.getElementById(inputId).value = inputstring;
	    document.getElementById(outputId).value = outputstring;
	}
	return;
}

