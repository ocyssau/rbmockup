{*Smarty template*}

<h1 class="pagetitle">
{tr}Assign permissions to group{/tr}: {$group_info.0.group_define_name}</a>
<br>
<a href="projectExplore.php?project_id={$project_id}&action=listGroups" class="linkbut">{tr}Back to groups{/tr}</a><br />
</h1>

{$msg}<br />

{*----------------Permissions list header--------------------------*}
    <form name="project_assignpermission.php" method="post">
    <input type="hidden" name="group" value="{$group|escape}" />
    <input type="hidden" name="type" value="{$type|escape}" />
    <input type="submit" name="update" value="{tr}validate{/tr}" /><br />
    <table class="normal">

    <tr>
    <td class="heading">&nbsp;</td>
    <td class="heading"><a class="tableheading">{tr}desc{/tr}</td>
    </tr>

{*----------------Permissions list body-------------------------*}
    {cycle values="odd,even" print=false}
    {section name=user loop=$perms}
      <input type="hidden" name="permName[{$perms[user].right_id}]" />
      <tr>
        <td class="{cycle advance=false}"> 
        <input type="checkbox" name="perm[{$perms[user].right_id}]"
        {section name=group_perms loop=$group_perms}
         {if $group_perms[group_perms].right_id eq $perms[user].right_id}
          checked="checked"
         {/if}
        {/section}
        />
        {*$perms[user].right_id*}
        </td>
        <td class="{cycle}">{tr}{$perms[user].right_description}{/tr}</td>
      </tr>
    {/section}
  </table>
  <input type="submit" name="update" value="{tr}validate{/tr}" />
</form>
<br />
