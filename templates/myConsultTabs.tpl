{*Smarty template*}

<ul id="tabnav">
{php}
  if( is_array($_SESSION['objectList']) )
  foreach($_SESSION['objectList'] as $object){
    $space_name = $object['object_class'];
    if (!($space_name == 'bookshop' || $space_name == 'cadlib' || $space_name == 'mockup' || $space_name == 'workitem')) continue;
    $container_id = $object[$object['object_class'].'_id'];
    $container_number = $object[$object['object_class'].'_number'];
    if($_SESSION['myActiveConsult']['container_id'] == $container_id && $_SESSION['myActiveConsult']['space'] == $space_name) {
      $class = 'active';
    }else{
      $class = '';
    }

    //Dont show tab if equal to current activate container
    if( $container_id == $_REQUEST['SelectedContainer'] && $space_name == $_REQUEST['space'] ){
      continue;
    }else{
      echo '<li class="'.$class.'"><a href="containerConsult.php?space='.$space_name.'&container_id='.$container_id.'">'.$container_number.'</a></li>';
    }
  }
{/php}

</ul>
