{*Smarty template*}

{*--------------------Search Bar defintion--------------------------*}
<div id="sitesearchbar_doc">
<fieldset>
<form id="filterf" action="{$smarty.server.PHP_SELF}" method="post">

{sameurlpost}
  <input type="hidden" name="displayHistory" value="0" />
  <label><input type="checkbox" name="displayHistory" value="1" {if $displayHistory}"checked"{/if} onChange='javascript:getElementById("filterf").submit()';/>
  <small>{tr}Display history{/tr}</small></label>

  <input type="hidden" name="onlyMy" value="0" />
  <label><input type="checkbox" name="onlyMy" value="1" {if $onlyMy}"checked"{/if} onChange='javascript:getElementById("filterf").submit()';/>
  <small>{tr}Only me{/tr}</small></label>

  <input type="hidden" name="checkByMe" value="0" />
  <label><input type="checkbox" name="checkByMe" value="1" {if $checkByMe}"checked"{/if} onChange='javascript:getElementById("filterf").submit()';/>
  <small>{tr}Only check by me{/tr}</small></label>

  <input type="hidden" name="displayThumbs" value="0" />
  <label><input type="checkbox" name="displayThumbs" value="1" {if $displayThumbs}"checked"{/if} onChange='javascript:getElementById("filterf").submit()';/>
  <small>{tr}Display thumbnails{/tr}</small></label>

  <label>
  <input size="4" type="text" name="numrows" value="{$numrows|escape}" />
  <small>{tr}rows to display{/tr}</small></label>
<br />

  {capture name='help1'}{tr}searchBar_help_1{/tr}{/capture}
  <img border="0" alt="help" src="img/icons/help.png" 
    {popup text=$smarty.capture.help1}
  />

  <label for="find_document_number"><small>{tr}Number{/tr}</small></label>
  <input size="16" type="text" name="find_document_number" value="{$find_document_number}" />

  <label for="find_document_designation"><small>{tr}Designation{/tr}</small></label>
  <input size="16" type="text" name="find_document_designation" value="{$find_document_designation}" />

  <label for="find_doctype"><small>{tr}Doctype{/tr}</small></label>
  <input size="8" type="text" name="find_doctype" value="{$find_doctype|escape}" />

  <label for="find_category"><small>{tr}Category{/tr}</small></label>
	<select name="find_category">
	<option {if '' eq $find_field}selected="selected"{/if} value=""></option>
	{foreach from=$category_list item=name key=field}
   <option {if $field eq $find_category}selected="selected"{/if} value="{$field|escape}">{$name}</option>
	{/foreach}
	</select>

  <label for="find_document_access_code"><small>{tr}Access{/tr}</small></label>
	<select name="find_document_access_code">
	   <option value=""></option>
	   <option {if $find_document_access_code eq 'free'}selected="selected"{/if} value="free">{tr}Free{/tr}</option>
	   <option {if $find_document_access_code eq 1}selected="selected"{/if}value="1">{tr}CheckedOut{/tr}</option>
	   <option {if $find_document_access_code eq 5}selected="selected"{/if} value="5">{tr}InWorkflow{/tr}</option>
	   <option {if $find_document_access_code eq 10}selected="selected"{/if} value="10">{tr}Validated{/tr}</option>
	   <option {if $find_document_access_code eq 11}selected="selected"{/if} value="11">{tr}Locked{/tr}</option>
	   <option {if $find_document_access_code eq 12}selected="selected"{/if} value="12">{tr}Marked to suppress{/tr}</option>
	   <option {if $find_document_access_code eq 15}selected="selected"{/if} value="15">{tr}Historical{/tr}</option>
  </select>

  <label for="find_document_state"><small>{tr}State{/tr}</small></label>
  <input size="8" type="text" name="find_document_state" value="{$find_document_state|escape}" />

  <input type="submit" name="filter" value="{tr}filter{/tr}" />
  <input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" />

<br />
{literal}
<script language="JavaScript">
function displayOptionDocFilter(){

      var a,b;

      var a = document.getElementById("f_adv_search_cb");
      var b = document.getElementById("f_adv_search_span");
      if(a.checked==true)
          b.style.display = "block";
      else b.style.display = "none";
}


function displayOption(){

      var a,b;

      var a = document.getElementById("f_dateAndTime_cb");
      var b = document.getElementById("f_dateAndTime_span");
      if(a.checked==true)
          b.style.display = "block";
      else b.style.display = "none";

    if(document.getElementById("f_check_out_date_sel")){
      var a = document.getElementById("f_check_out_date_sel");
      var b = document.getElementById("f_check_out_date_span");
      if(a.checked==true)
          b.style.display = "block";
      else b.style.display = "none";
    }

    if(document.getElementById("f_update_date_sel")){
      var a = document.getElementById("f_update_date_sel");
      var b = document.getElementById("f_update_date_span");
      if(a.checked==true)
          b.style.display = "block";
      else b.style.display = "none";
    }

    if(document.getElementById("f_open_date_sel")){
      var a = document.getElementById("f_open_date_sel");
      var b = document.getElementById("f_open_date_span");
      if(a.checked==true)
          b.style.display = "block";
      else b.style.display = "none";
    }

    if(document.getElementById("f_close_date_sel")){
      var a = document.getElementById("f_close_date_sel");
      var b = document.getElementById("f_close_date_span");
      if(a.checked==true)
          b.style.display = "block";
      else b.style.display = "none";
    }

    if(document.getElementById("f_fsclose_date_sel")){
      var a = document.getElementById("f_fsclose_date_sel");
      var b = document.getElementById("f_fsclose_date_span");
      if(a.checked==true)
          b.style.display = "block";
      else b.style.display = "none";
    }
}

</script>
{/literal}

  <input type="hidden" name="f_adv_search_cb" value="0" />
<label><small>{tr}Advanced filter{/tr}</small>
  <input type="checkbox" name="f_adv_search_cb" value="1" {if $f_adv_search_cb}"checked"{/if} id="f_adv_search_cb" onClick="displayOptionDocFilter();" />
</label>
<span id=f_adv_search_span style="display: none">
<fieldset>

  <label for="find"><small>{tr}find{/tr}</small></label>
  <input size="16" type="text" name="find" value="{$find|escape}" />

  <label for="find_field"><small>{tr}In{/tr}</small></label>
	<select name="find_field">
	<option {if '' eq $find_field}selected="selected"{/if} value=""></option>
	{foreach from=$all_field item=name key=field}
   <option {if $field eq $find_field}selected="selected"{/if} value="{$field|escape}">{$name}</option>
	{/foreach}
	</select>

  <label for="f_action_field"><small>{tr}Action{/tr}</small></label>
	<select name="f_action_field">
	<option {if $f_action_field eq ''}selected="selected"{/if} value=""></option>
	{foreach from=$user_elements item=name key=field}
   <option value="{$name}" {if $f_action_field eq $name}selected="selected"{/if}>{tr}{$name}{/tr}</option>
	{/foreach}
	</select>

  <label for="f_action_user_name"><small>{tr}By{/tr}</small></label>
	<select name="f_action_user_name">
	<option {if $f_action_user_name eq ''}selected="selected"{/if} value=""></option>
	{foreach from=$user_list item=name}
   <option value="{$name.auth_user_id}" {if $f_action_user_name eq $name.auth_user_id}selected="selected"{/if}>{$name.handle}</option>
	{/foreach}
	</select>



<label><small>{tr}Date and time{/tr}</small>
<input type="checkbox" name="f_dateAndTime_cb" value="1" id="f_dateAndTime_cb" onClick="displayOption();" />
</label>
<span id=f_dateAndTime_span style="display: none">
<fieldset>


<link href="./lib/jscalendar/calendar-win2k-1.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./lib/jscalendar/calendar.js"></script>
<script type="text/javascript" src="./lib/jscalendar/lang/calendar-fr.js"></script>
<script type="text/javascript" src="./lib/jscalendar/calendar-setup.js"></script>

{if in_array('check_out_date',$date_elements)}
  <label><small>{tr}CheckOut date{/tr}</small>
  <input type="checkbox" name="f_check_out_date_sel" value="1" id="f_check_out_date_sel" onClick="displayOption();" />
  </label>

  <span id=f_check_out_date_span style="display: none">
  <fieldset>
  <input type="hidden" name="f_check_out_date" value="" id="f_check_out_date"/>

  <label><input type="radio" name="f_check_out_date_cond" "checked" value=">"/>
  <small>{tr}Superior to{/tr}</small></label>

  <label><input type="radio" name="f_check_out_date_cond" value="<" />
  <small>{tr}Inferior to{/tr}</small></label>
  <span style="background-color: rgb(255, 255, 136);" id="show_check_out_date">Click</span>
  </fieldset>
  </span>

  {literal}
  <script type="text/javascript">
    Calendar.setup({
        inputField     :    "f_check_out_date",     // id of the input field
        displayArea    :    "show_check_out_date",       // ID of the span where the date is to be shown
        ifFormat       :    "%s",     // format of the input field (even if hidden, this format will be honored)
        daFormat       :    "%Y/%m/%d %H:%M:%S",// format of the displayed date
        showsTime      :    true, // display hours 
        singleClick    :    false
    });
  </script>
  {/literal}
<br />
{/if}

{if in_array('update_date',$date_elements)}
  <label><small>{tr}Update date{/tr}</small>
  <input type="checkbox" name="f_update_date_sel" value="1" id="f_update_date_sel" onClick="displayOption();" />
  </label>

  <span id=f_update_date_span style="display: none">
  <fieldset>
  <input type="hidden" name="f_update_date" value="" id="f_update_date"/>

  <label><input type="radio" name="f_update_date_cond" "checked" value=">" />
  <small>{tr}Superior to{/tr}</small></label>

  <label><input type="radio" name="f_update_date_cond" value="<" />
  <small>{tr}Inferior to{/tr}</small></label>

  <span style="background-color: rgb(255, 255, 136);" id="show_update_date">Click</span>
  </fieldset>
  </span>

  {literal}
  <script type="text/javascript">
    Calendar.setup({
      inputField     :    "f_update_date",     // id of the input field
      displayArea    :    "show_update_date",       // ID of the span where the date is to be shown
      ifFormat       :    "%s",     // format of the input field (even if hidden, this format will be honored)
      daFormat       :    "%Y/%m/%d %H:%M:%S",// format of the displayed date
      showsTime      :    true, // display hours 
      singleClick    :    false
  });
  </script>
  {/literal}
<br />
{/if}

{if in_array('open_date',$date_elements)}
  <label><small>{tr}Create date{/tr}</small>
  <input type="checkbox" name="f_open_date_sel" value="1" id="f_open_date_sel" onClick="displayOption();" />
  </label>

  <span id=f_open_date_span style="display: none">
  <fieldset>
  <input type="hidden" name="f_open_date" value="" id="f_open_date"/>

  <label><input type="radio" name="f_open_date_cond" "checked" value=">" />
  <small>{tr}Superior to{/tr}</small></label>

  <label><input type="radio" name="f_open_date_cond" value="<" />
  <small>{tr}Inferior to{/tr}</small></label>

  <span style="background-color: rgb(255, 255, 136);" id="show_open_date">Click</span>
  </fieldset>
  </span>

  {literal}
  <script type="text/javascript">
    Calendar.setup({
        inputField     :    "f_open_date",     // id of the input field
        displayArea    :    "show_open_date",       // ID of the span where the date is to be shown
        ifFormat       :    "%s",     // format of the input field (even if hidden, this format will be honored)
        daFormat       :    "%Y/%m/%d %H:%M:%S",// format of the displayed date
        showsTime      :    true, // display hours
        singleClick    :    false
    });
  </script>
  {/literal}
{/if}

{if in_array('close_date',$date_elements)}
<br />
  <label><small>{tr}Close date{/tr}</small>
  <input type="checkbox" name="f_close_date_sel" value="1" id="f_close_date_sel" onClick="displayOption();" />
  </label>

  <span id=f_close_date_span style="display: none">
  <fieldset>
  <input type="hidden" name="f_close_date" value="" id="f_close_date"/>

  <label><input type="radio" name="f_close_date_cond" "checked" value=">" />
  <small>{tr}Superior to{/tr}</small></label>

  <label><input type="radio" name="f_close_date_cond" value="<" />
  <small>{tr}Inferior to{/tr}</small></label>

  <span style="background-color: rgb(255, 255, 136);" id="show_close_date">Click</span>
  </fieldset>
  </span>

  {literal}
  <script type="text/javascript">
    Calendar.setup({
        inputField     :    "f_close_date",     // id of the input field
        displayArea    :    "show_close_date",       // ID of the span where the date is to be shown
        ifFormat       :    "%s",     // format of the input field (even if hidden, this format will be honored)
        daFormat       :    "%Y/%m/%d %H:%M:%S",// format of the displayed date
        showsTime      :    true, // display hours 
        singleClick    :    false
    });
  </script>
  {/literal}

{/if}

{if in_array('forseen_close_date',$date_elements)}
<br />
  <label><small>{tr}Forseen close date{/tr}</small>
  <input type="checkbox" name="f_fsclose_date_sel" value="1" id="f_fsclose_date_sel" onClick="displayOption();" />
  </label>

  <span id=f_fsclose_date_span style="display: none">
  <fieldset>
  <input type="hidden" name="f_fsclose_date" value="" id="f_fsclose_date"/>

  <label><input type="radio" name="f_fsclose_date_cond" "checked" value=">" />
  <small>{tr}Superior to{/tr}</small></label>

  <label><input type="radio" name="f_fsclose_date_cond" value="<" />
  <small>{tr}Inferior to{/tr}</small></label>

  <span style="background-color: rgb(255, 255, 136);" id="show_fsclose_date">Click</span>
  </fieldset>
  </span>

  {literal}
  <script type="text/javascript">
    Calendar.setup({
        inputField     :    "f_fsclose_date",     // id of the input field
        displayArea    :    "show_fsclose_date",       // ID of the span where the date is to be shown
        ifFormat       :    "%s",     // format of the input field (even if hidden, this format will be honored)
        daFormat       :    "%Y/%m/%d %H:%M:%S",// format of the displayed date
        showsTime      :    true, // display hours 
        singleClick    :    false
    });
  </script>
  {/literal}

{/if}


<br />

</fieldset>

</span>

</fieldset>
</span>

</fieldset>

</form>

</div>

{literal}
<script language="JavaScript">
  displayOptionDocFilter();
</script>
{/literal}
