{*Smarty template*}

<h1 class="pagetitle">{tr}Wildspace of{/tr} {$currentUserName}</h1>

{*--------------------Search Bar defintion--------------------------*}
<div id="searchbar">
<form id="filterf" action="{$smarty.server.PHP_SELF}" method="post">

{sameurlpost}

<table>

 <tr>
  <td><small>{tr}find{/tr}</small></td>
  <td><small>{tr}In{/tr} :</small></td>
  <td></td>
  <td><label><input type="checkbox" name="displayMd5" value="1" {if $displayMd5}"checked"{/if} onChange='javascript:getElementById("filterf").submit()';/>
  <small>{tr}Display md5{/tr}</small></label></td>
  <!--  <td><small>{tr}Number of displayed rows{/tr}</small></td>-->
 </tr>

 <tr>
  <td ><input size="16" type="text" name="find" value="{$find|escape}" /></td>
  <td >
  	<select name="find_field" onchange='javascript:getElementById("filterf").submit();'>
  	<option {if '' eq $find_field}selected="selected"{/if} value=""></option>
  	{foreach from=$all_field item=name key=field}
     <option {if $field eq $find_field}selected="selected"{/if} value="{$field|escape}">{$name}</option>
  	{/foreach}
  	</select>
  </td>
  <!--  <td ><input size="4" type="text" name="numrows" value="{$numrows|escape}" /></td>-->
  <td ><input type="submit" name="filter" value="{tr}filter{/tr}" /></td>
</form>
<td>

<form id="resetf" action="{$smarty.server.PHP_SELF}" method="post">
  {sameurlpost}
  <input type="hidden" name="offset" value="0" />
  <input type="hidden" name="sort_field" value="" />
  <input type="hidden" name="sort_order" value="" />
  <input type="hidden" name="where" value="" />
  <input type="hidden" name="find" value="" />
  <input type="hidden" name="find_field" value="" />
  <input type="hidden" name="numrows" value="" />
  <input type="hidden" name="displayMd5" value="0" />

  <input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" />
</form>
</td>
</tr>
</table>	

<!--
<form id="filterf" action="{$smarty.server.PHP_SELF}" method="post">
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="sort_mode" value="{$sort_mode|escape}" />
<table>
  <tr>
  <td></td>
  <td><small>{tr}Number of displayed rows{/tr}</small></td>
  <td></td>
  </tr>
  
  <tr><td>
  <form name="update" method="post" action="wildspace.php">
   <button class="mult_submit" type="submit" name="action" value="update" title="{tr}Refresh{/tr}">
   <img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
   </button>
  </form>
  </td>
  
  <td align="right"><input size="4" type="text" name="numrows" value="{$numrows|escape}" /></td>
  <td ><input type="submit" name="filter" value="{tr}filter{/tr}" /></td>
  </tr>
</div>

</table>	
</form>
-->


{* -------------------Pagination------------------------ *}
{*<!--{include file='pagination.tpl'}-->*}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="wildspace.php">
<table class="normal">
 <tr>
  <th class="heading auto">
{literal}
<script language='Javascript' type='text/javascript'>
<!--
// check / uncheck all.
document.write("<input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/>");
//-->                     
</script>

<script language='Javascript' type='text/javascript'>
<!--
function newNameInput(monlien,file_name,title){
  var newName = prompt(title, file_name );
  if(newName == null){
    document.location.href = '#';
    return false;
  }
  monlien.search = monlien.search+'&newName='+newName;
  //alert(monlien.href);
  document.location.href = monlien.href;
  return true;
}
//-->
</script>

<script language='Javascript' type='text/javascript'>
<!--
function toggleLine(id,img){
  element = document.getElementById(id);
  if(element.style.height == '20px'){
    element.style.height = 'auto';
    img.src = 'img/icons/untoggle.png';
  }else{
    element.style.height = '20px';
    img.src = 'img/icons/toggle.png';
  }
  //alert('le champ a pour valeur : "'+element.style.height+"'");
  return true;
}

//-->
</script>

{/literal}
  
  </th>
  <th class="heading auto"></th>
  <th class="heading auto"></th>

  {*-Specifics fields-*}
 <th class="heading" nowrap><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_name&displayMd5={$displayMd5}">
  {tr}file_name{/tr}</a>.
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_extension&displayMd5={$displayMd5}">
  {tr}file_extension{/tr}</a>
  <a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_type&displayMd5={$displayMd5}">
  ({tr}file_type{/tr})</a>
  </th>

  <!--<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_path&displayMd5={$displayMd5}">
  {tr}base dir{/tr}</a></th>-->

  <th class="heading">{tr}doc_infos{/tr}
  <!--
  <a {popup text='<b>Be careful, the designation display here can be wrong</b>'}
    class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=designation&displayMd5={$displayMd5}">
    {tr}Designation{/tr}</a><br />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=check_out_by&displayMd5={$displayMd5}">
    {tr}check_out_by{/tr}</a><br />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=container_type&displayMd5={$displayMd5}">
    {tr}container_type{/tr}</a>
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=container_id&displayMd5={$displayMd5}">
    {tr}container_number{/tr}</a>
  -->
  </th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_mtime&displayMd5={$displayMd5}">
  {tr}mtime{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_size&displayMd5={$displayMd5}">
  {tr}file_size{/tr}</a></th>

  {if $displayMd5}
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_md5&displayMd5={$displayMd5}">
  {tr}md5{/tr}</a></th>
  {/if}
 </tr>
{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].file_name}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

    <td class="thin"><a href="javascript:popupP('smartStore.php?action=Store&checked[]={$list[list].file_name}&wildspace={$wildspace|escape}','workitemStore', 600 , 1200)" title="{tr}Store{/tr}">
    <img border="0" alt="{tr}Store{/tr}" src="img/icons/document/document_add.png" /></a>
    </td>

    <td class="link"><a href="wildspace.php?action=downloadFile&checked[]={$list[list].file_name}&wildspace={$wildspace|escape}" title="{tr}Download{/tr}">
    {file_icon extension=$list[list].file_extension icondir=$file_icons_dir icontype='.gif'}
    </a>
    </td>

    {*-Specifics fields-*}
    <td nowrap>
    <div id="{$list[list].natif_file_name}" style="overflow:hidden;height:20px;margin:0;padding:0;">
    <a class="link" href="wildspace.php?action=downloadFile&checked[]={$list[list].file_name}&wildspace={$wildspace|escape}" title="{tr}Download{/tr}">
    {$list[list].natif_file_name}</a> ({$list[list].file_type})
    <img src="img/icons/toggle.png" title="{tr}toggle action{/tr}" alt="{tr}actions{/tr}" onclick="toggleLine('{$list[list].natif_file_name}',this)">
    <!-- Action hide menu -->
    <ul class="puce1">
      <li>
        <a href='wildspace.php?action=renameQueryFile&checked={$list[list].file_name}&ticket={$ticket}' title='{tr}Rename{/tr}'
         onClick="newNameInput(this,'{$list[list].file_name}','{tr}Rename: input the new file name{/tr}');return false;">
        {tr}Rename{/tr}
        </a>
      </li>
      <li>
        <a href="wildspace.php?action=copyQueryFile&checked={$list[list].file_name}&ticket={$ticket}" title="{tr}Copy{/tr}"
         onClick="newNameInput(this,'{$list[list].file_name}','{tr}Copy: input the new file name{/tr}');return false;">
        {tr}Copy{/tr}
        </a>
      </li>
    </ul>


    </div>
    </td>



    <!--<td class="link"><a href="file:///{$localPathToWildspace}/{$list[list].natif_file_name}" title="{tr}Download{/tr}">
    {$list[list].natif_file_name}</a></td>-->
    <!--<td class="thin">{$list[list].file_path}</a></td> -->

    <td class="thin">
    {if $list[list].document_id}
      <i>{$list[list].designation}</i><br />
      <b>Checkout by : </b>{$list[list].check_out_by|username}<br />
      <b>In : </b> {tr}{$list[list].container_type}{/tr} {$list[list].container_number}
      {if $list[list].check_out_by == $currentUserId}
        <a href="DocManage.php?action=CheckInDoc&checked[]={$list[list].document_id}&container_id={$list[list].container_id}&space={$list[list].container_type}&ticket={$ticket}">
          <img border=0 src=./img/icons/document/document_in.png />CheckIn</a>
      {/if}
    {/if}
    </td>

    <td class="thin">{$list[list].file_mtime|date_format}</td>
    <td class="thin">{$list[list].file_size|filesize_format}</td>
    {if $displayMd5}<td class="thin">{$list[list].file_md5}</td>{/if}
   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}
{*<!--{include file='pagination.tpl'}-->*}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
{*
      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr} : </label></td></tr>");
      //-->                     
      </script>

<br>
*}
{*Multiselection select action form *}
{*
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />
<i>{tr}Perform action{/tr} :</i>
*}

<div id="action-menu">

<button class="mult_submit" type="submit" name="action" value="SuppressFile" title="{tr}Suppress{/tr}" id="01"
 onclick="if(confirm('{tr}Do you want really suppress this files{/tr}'))
 {ldelim}document.checkform.action='wildspace.php'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="StoreMulti" title="{tr}Multi Store{/tr}" id="02"
 onclick="document.checkform.action='smartStore.php'; pop_it(checkform , 600 , 1024)">
<img class="icon" src="./img/icons/document/document_add.png" title="{tr}Store{/tr}" alt="{tr}Store{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="DownloadZip" title="{tr}Download zip{/tr}" id="05"
 onclick="document.checkform.action='wildspace.php'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_zip.png" title="{tr}Download zip{/tr}" alt="{tr}Download zip{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="Uncompress" title="{tr}Uncompress{/tr}" onclick="pop_no(checkform)" />
<img class="icon" src="./img/icons/package/package_uncompress.png" title="{tr}Uncompress{/tr}" alt="{tr}Uncompress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="CheckUUID" title="{tr}CheckUUID{/tr}" id="03"
 onclick="document.checkform.action='wildspace.php'; pop_it(checkform)">
<img class="icon" src="./img/icons/uuid.png" title="{tr}CheckUUID{/tr}" alt="{tr}CheckUUID{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="update" title="{tr}Refresh{/tr}" id="O4"
 onclick="document.checkform.action='wildspace.php'; pop_no(checkform)">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

</div>

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="wildspace" value="{$wildspace|escape}" />
<input type="hidden" name="displayMd5" value="{$displayMd5}" />
<input type="hidden" name="ticket" value="{$ticket}" />
</form>


{* ------------------- Upload section ------------------------ *}
<fieldset>
<legend align="top"><i>{tr}Add a file to wildspace{/tr}</i></legend>
<form action="wildspace.php" method="post" name="upload" enctype="multipart/form-data">
    <input type="file" name="uploadFile"/>
    <input type="submit" value="upload" name="action" />
    <br />{tr}Overwrite{/tr}: <input type="checkbox" name="overwrite" />
</fieldset>
<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="wildspace" value="{$wildspace|escape}" />
<input type="hidden" name="displayMd5" value="{$displayMd5}" />
</form>
