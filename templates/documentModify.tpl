{*Smarty template*}

{include file="header.tpl"}

{$form.javascript}

<form {$form.attributes}>
{$form.hidden}

<h2>{tr}{$form.header.infos}{/tr}</h2>

<table class="normal">

  <tr class="formcolor">
    <td>{tr}{$form.designation.label}{/tr}:</td>
    <td>{$form.designation.html}</td>
  </tr>

  <tr class="formcolor">
    <td>{tr}{$form.category_id.label}{/tr}:</td>
    <td>{$form.category_id.html}</td>
  </tr>

 {if not $form.frozen}
  <tr class="formcolor">
    <td> </td>
    <td>{$form.reset.html}&nbsp;{$form.submit.html}</td>
  </tr>
 {/if}

  <tr>
    <td>{tr}{$form.requirednote}{/tr}</td>
  </tr>

</table>
</form>

<form name="close">
  <input type="button" onclick="window.close()" value="Close">
</form>

<br />

{foreach key=name item=error from=$form.errors}
 <b>Collected Errors:</b> <br />
 <font color="red">{$error}</font> in element [{$name}]<br />
{/foreach}
