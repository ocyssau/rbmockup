{*Smarty template*}

{include file="header.tpl"}
<div id="tiki-center">

<form {$form.attributes}>
{$form.hidden}

{if $action == 'modify'}
  <h2>{tr}Modify a project{/tr}</h2>
{else}
  <h2>{tr}Create a project{/tr}</h2>
{/if}

<table class="normal">
 
  <tr>
    <td class="formcolor">{tr}{$form.project_number.label}{/tr}:</td>
    <td class="formcolor">{$form.project_number.html}
      <br /><i>{$number_help}</i>
    </td>
  </tr>
 
  <tr>
    <td class="formcolor">{tr}{$form.project_description.label}{/tr}:</td>
    <td class="formcolor">{$form.project_description.html}</td>
  </tr>
  
  <tr>
    <td class="formcolor">{tr}{$form.forseen_close_date.label}{/tr}:</td>
    <td class="formcolor">{$form.forseen_close_date.html}</td>
  </tr>

 {if not $form.frozen}
  <tr>
    <td class="formcolor"> </td>
    <td class="formcolor">{$form.reset.html}&nbsp;{$form.submit.html}</td>
  </tr>
 {/if}

  <tr>
       <td class="formcolor">{tr}{$form.requirednote}{/tr}</td>
  </tr>

</table>
</form>

<form name="close">
  <input type="button" onclick="window.close()" value="Close">
</form>

<br />

{if $form.errors}
<b>Collected Errors:</b><br />
{foreach key=name item=error from=$form.errors}
  <font color="red">{$error}</font> in element [{$name}]<br />
{/foreach}
<br />
{/if}
</div>
</body>
