{*Smarty template*} 
 
{include file=galaxia_html_header.tpl}

{popup_init src="lib/overlib.js"}

<h1 class="pagetitle">{tr}Doctypes list{/tr}</h1>

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=doctype_number">
   {tr}Number{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=doctype_description">
   {tr}Description{/tr}</a></th>
  <th class="heading">{tr}script post store{/tr}</th>
  <th class="heading">{tr}script pre store{/tr}</th>
  <th class="heading">{tr}script pre update{/tr}</th>
  <th class="heading">{tr}script post update{/tr}</th>
  <th class="heading">{tr}recognition regexp{/tr}</th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_extension">
   {tr}file extension{/tr}</a></th>
  <th class="heading">{tr}icon{/tr}</th>
  <th class="heading">{tr}can be composite{/tr}</a></th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].doctype_id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

    <td class="thin">{$list[list].doctype_number}</td>
    <td class="thin">{$list[list].doctype_description}</td>
    <td class="thin">{$list[list].script_post_store}</td>
    <td class="thin">{$list[list].script_pre_store}</td>
    <td class="thin">{$list[list].script_pre_update}</td>
    <td class="thin">{$list[list].script_post_update}</td>
    <td class="thin">{$list[list].recognition_regexp}</td>
    <td class="thin">{$list[list].file_extension}</td>
    <td class="thin">{$list[list].icon}</td>
    <td class="thin">{$list[list].can_be_composite}</td>
   </tr>
  {/section}
  </table>

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
      //-->                     
      </script>

{*Multiselection select action form *}
<br />
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />
<i>{tr}Perform action{/tr} :</i>
<input type="submit" name="action" value="linkDoctype" />

{*Submit checkform form*}

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="action" value="{$action}" />
<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="flag" value="1" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />

</form>

<form name="cancel" method="post" action="{$smarty.server.PHP_SELF}">
<input type="hidden" name="action" value="getDoctypeLinks" />
<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
<input type="submit" name="cancel" value="cancel" />
</form>
