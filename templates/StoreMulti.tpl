{*Smarty template*}
{include file="header.tpl"}

{literal}
<script>
function getElementByClassName(formName,className) {
  var elements=new Array();
  for (i=0;i<document.forms[formName].elements.length;i++) { //Calcul le nombre d'element dans le formulaire
  	if (document.forms[formName].elements[className+'['+i+']']) {
      elements.push(document.forms[formName].elements[className+'['+i+']']);
    }
  }
  return elements;
}

function setClassName(element){
  name = element.name;
  //name = 'toto[1][2][3]';
  var reg=new RegExp("(\[[0-9]+\]){1}$", "g");
  var ret=name.split(reg);
  //alert(ret[0]);
  return ret[0];
}

function getValue(element){
  switch(element.type){
  case 'text':
  case 'textarea':
    return element.value;
    break;
  case 'select-one':
    return element.selectedIndex;
    break;
  case 'checkbox':
    return element.checked;
    break;
  }
}

//Set value of all element with same class name to the current value of element
//ClassName is the name of element without ending '[n]'
function setValue(elementId){
  element = document.getElementById(elementId);
  var formName = element.form.name;
  var className = setClassName(element);
  var elements = getElementByClassName(formName,className);
  //alert(element.type);
  switch(element.type){
  case 'text':
  case 'textarea':
    for (i=0;i<elements.length;i++) {
      elements[i].value = getValue(element);
    }
    break;

  case 'select-one':
    for (i=0;i<elements.length;i++) {
      elements[i].selectedIndex = getValue(element);
    }
    break;

  case 'checkbox':
    for (i=0;i<elements.length;i++) {
      elements[i].checked = getValue(element);
    }
    break;

  }

}
</script>
{/literal}

<div id="tiki-center">

{$createForm}

<form name="close"><input type="button" onclick="window.close()" value="Close"></form>

</div>
</body>
