<div class="cbox">
  <br />
  <div class="cbox-data">
    <h3>{$confirmation_text}</h3>
    <form action="{$confirmation_formaction}" method="post">
      {if $ticket}<input value="{$ticket}" name="ticket" type="hidden" />{/if}
      <textarea rows="5" cols="50" name="comment"></textarea>
      <ul>
      {section name=list loop=$list}
        <li><input type="checkbox" name="checked[]" value="{$list[list].document_id}" id="checkbox_tbl_{$smarty.section.list.index}" checked="checked" />
        <label for="checkbox_tbl_{$smarty.section.list.index}">
        <b>{$list[list].document_number} - {$list[list].document_indice_id|document_indice} v{$list[list].document_version}</b>
        <i> {$list[list].designation}</i>
        </label>
        </li>
      {/section}
      </ul>
      <input value="{$confirmation_action}" name="action" type="hidden" />
      <input type="submit" name="{$confirmation_return}" value="ok" />
      <input type="submit" name="action" value="cancel" />
    </form>
  </div>
</div>
