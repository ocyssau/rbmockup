<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <title>RanchBE</title>
  <meta http-equiv="Content-Type" content="text/html; charset={$charset}" />
  <meta http-equiv="Content-Script-Type" content="text/javascript" />
  <link href="styles/default/ranchbe_default.css" rel="stylesheet" type="text/css">
  <link href="{$css_sheet}" rel="stylesheet" type="text/css">
  <link href="styles/PhpLayerMenu.css" rel="stylesheet" type="text/css">
  <link rel="shortcut icon" href="{$shortcut_icon}" />
  <script type="text/javascript" src="lib/tiki-js.js"></script>
  <script type="text/javascript" src="lib/lib.js"></script>
  <script type="text/javascript" src="lib/PEAR/data/HTML/QuickForm/qfamsHandler.js"></script>
  
  {$additionnal_header}
  
  <noscript>
  BE CAREFUL : You must enable javascript for use RanchBE and enable pop-up.
  </noscript>

</head>
<body>

{popup_init src="lib/overlib.js"}
