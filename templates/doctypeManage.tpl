{*Smarty template*}

{include file="header.tpl"}


{literal}
<script language="JavaScript">
function afficherExtensions(type) {
    if (type == 'file'){
      var a = document.getElementById("file_extension");
      if (a.style.display == "none")
          a.style.display = "block";
      else a.style.display = "none";
    }
}
</script>
{/literal}

<form {$form.attributes}>
{$form.hidden}

{if $action == 'modify'}
  <h2>{tr}Modify a doctype{/tr}</h2>
{else}
  <h2>{tr}Create a doctype{/tr}</h2>
{/if}

<fieldset>
  <legend>Basic</legend>
  <ul>
    <label>{tr}{$form.doctype_number.label}{/tr}:</label>
    {$form.doctype_number.html}
    <br /><i>{$number_help}</i><br />
    <label>{tr}{$form.doctype_description.label}{/tr}:</label>
    {$form.doctype_description.html}
  </ul>
</fieldset>

<fieldset>
  <legend>Regex</legend>
  <ul>
    <label>{tr}{$form.recognition_regexp.label}{/tr}:</label>
    {$form.recognition_regexp.html}<br />
    <a href="javascript:popup('./lib/testRegExpr.htm','testRegExpr')">{tr}Need help?{/tr}</a></p>
  </ul>
</fieldset>

<fieldset>
  <legend>Scripts</legend>
  <ul>
    <label>{tr}{$form.script_pre_store.label}{/tr}:</label>
    {$form.script_pre_store.html}<br />
    
    <label>{tr}{$form.script_post_store.label}{/tr}:</label>
    {$form.script_post_store.html}<br />
    
    <label>{tr}{$form.script_pre_update.label}{/tr}:</label>
    {$form.script_pre_update.html}<br />
    
    <label>{tr}{$form.script_post_update.label}{/tr}:</label>
    {$form.script_post_update.html}<br />
  </ul>
</fieldset>

<fieldset>
  <legend>File</legend>
  <ul>
    <label>{tr}{$form.file_type.label}{/tr}:</label><br />
    {$form.file_type.html}
    <br />
    <span id=file_extension {if $fileselect}style="display: block"{else}style="display: none"{/if}>
      <label>{tr}{$form.file_extension.label}{/tr}:</label>
      {$form.file_extension.html}
    </span>
    {tr}{$form.can_be_composite.label}{/tr}:
    {$form.can_be_composite.html}
  </ul>
</fieldset>

<fieldset>
  <legend>Visualisation file</legend>
  <ul>
    <label>{tr}{$form.visu_file_extension.label}{/tr}:</label>
    {$form.visu_file_extension.html}
  </ul>
</fieldset>


{*
{tr}{$form.file_type_ext.label}{/tr}:
{$form.file_type_ext.html}
*}

<fieldset>
  <legend>Icon</legend>
  <ul>
    <label>{tr}{$form.icon.label}{/tr}:</label>
    {$form.icon.html}
  </ul>
</fieldset>

{tr}{$form.requirednote}{/tr}<br />

<fieldset>
  <ul>
  {if not $form.frozen}
    {$form.reset.html}&nbsp;{$form.submit.html}<br />{/if}
  </ul>
</fieldset>
</form>

<form name="close">
  <input type="button" onclick="window.close()" value="Close">
</form>

<br />

{if $form.errors}
<b>Collected Errors:</b><br />
{foreach key=name item=error from=$form.errors}
  <font color="red">{$error}</font> in element [{$name}]<br />
{/foreach}
<br />
{/if}


<table class="normal">
<form action="./doctypeManage.php" method="post" name="import" enctype="multipart/form-data">
  <tr>
   <td>
    {tr}Batch upload{/tr} (CSV file<a {popup text='doctype_number;doctype_description;file_extension<br />doctype1;description1;.doc<br />doctype2;description2;.xls'}><img src="img/icons/help.png" border="0" height="16" width="16" alt='{tr}help{/tr}' /></a>):
   </td>
   <td>
    <input type="file" name="csvlist"/>
    <input type="submit" value="csvimport" name="action" />
    <br />{tr}Overwrite{/tr}: <input type="checkbox" name="overwrite" checked="checked" />
   </td>
  </tr>
</table>
</form>

<br />
