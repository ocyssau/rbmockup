{*Smarty template*}

<div id="BoiteActions">
  <span class="button2"><a href="tiki-admingroups.php" class="linkbut">{tr}Admin groups{/tr}</a></span>
  <span class="button2"><a href="tiki-adminusers.php" class="linkbut">{tr}Admin users{/tr}</a></span>
  <span class="button2"><a href="tiki-admingroups.php#2" class="linkbut">{tr}Add a new group{/tr}</a></span>
</div>

<h1 class="pagetitle">{tr}Admin groups{/tr}</h1>

<br />

<div id="page-bar">
<span id="tab1" class="tabmark" style="border-color: white;"><a href="javascript:tikitabs(1,3);">{tr}Groups{/tr}</a></span>
{if $editgroup}
  <span id="tab2" class="tabmark" style="border-color: white;"><a href="javascript:tikitabs(2,3);">{tr}Edit group{/tr} {$groupname}</a></span>
{else}
  <span id="tab2" class="tabmark" style="border-color: white;"><a href="javascript:tikitabs(2,3);">{tr}Add new group{/tr}</a></span>
{/if}
</div>

{if $editgroup || $add}
 <div id="content1" class="tabcontent" style="display: none;">
{else}
 <div id="content1" class="tabcontent" style="display: block;">
{/if}

<h2>{tr}List of existing groups{/tr}</h2>

{*----------------Visualisation filter form--------------------------*}
    <form method="get" action="tiki-admingroups.php">
      <table class="findtable">
        <tr>
          <td><label for="groups_find">{tr}Find{/tr}</label></td>
          <td><input type="text" name="find" id="groups_find" value="{$find|escape}" /></td>
          <td><input type="submit" value="{tr}find{/tr}" name="search" /></td>
          <td>{tr}Number of displayed rows{/tr}</td>
          <td><input type="text" size="4" name="numrows" value="{$numrows|escape}" /></td>
        </tr>
      </table>
     <input type="hidden" name="sort_order" value="{$sort_order|escape}" />
     <input type="hidden" name="sort_field" value="{$sort_field|escape}" />
    </form>

{*--------------Pagination------------------------*}
      <div class="mini">
        <div align="center">
        {if $prev_offset >= 0}
          <a class="prevnext" href="tiki-admingroups.php?offset={$prev_offset}&numrows={$numrows}&sort_field={$sort_field}&sort_order={$sort_order}">{tr}prev{/tr}</a>
        {/if}
        {tr}Page{/tr}: {$actual_page}
        {if $cant_pages == 1}
          <a class="prevnext" href="tiki-admingroups.php?offset={$next_offset}&numrows={$numrows}&sort_field={$sort_field}&sort_order={$sort_order}">{tr}next{/tr}</a>
        {/if}
        </div>
      </div> 

{*--------------------Group list header----------------*}
<table class="normal">
<tr>
    <td class="heading" style="width: 20px;">&nbsp;</td>
    <td class="heading"><a class="tableheading" href="tiki-admingroups.php?offset={$offset}&numrows={$numrows}&sort_field=group_define_name&sort_order={if $sort_order eq 'ASC'}DESC{else}ASC{/if}">{tr}name{/tr}</a></td>
    <td class="heading"><a class="tableheading" href="tiki-admingroups.php?offset={$offset}&numrows={$numrows}&sort_field=group_description&sort_order={if $sort_order eq 'ASC'}DESC{else}ASC{/if}">{tr}desc{/tr}</a></td>
    <td class="heading">{tr}Includes{/tr}</td>
    <td class="heading">{tr}Permissions{/tr}</td>
    <td class="heading" style="width: 20px;">&nbsp;</td>
</tr>

{*--------------------Group list body----------------*}
{cycle values="even,odd" print=false} {* ---SmartyCode to alternate colors of rows---*}
  {section name=user loop=$users}
   <tr class="{cycle}">
      <td style="width: 20px;">
       <a class="link" 
          href="tiki-admingroups.php?editgroup={$users[user].group_id}&offset={$offset}&numrows={$numrows}&sort_field={$sort_field}&sort_order={$sort_order}#2" 
          title="{tr}edit{/tr}">
          <img src="img/icons/edit.png" 
          alt='{tr}edit{/tr}'
          border="0" 
          width="20" 
          height="16"  />
       </a>
      </td>
      <td>
       <a class="link" 
          href="tiki-admingroups.php?editgroup={$users[user].group_id}&offset={$offset}&numrows={$numrows}&sort_field={$sort_field}&sort_order={$sort_order}#2"
          title="{tr}edit{/tr}">{$users[user].group_define_name}
       </a>
      </td>
      <td>{tr}{$users[user].group_description}{/tr}</td>
      <td>
          {section name=ix loop=$users[user].included}
           {$users[user].included[ix]}<br />
          {/section}
      </td>
      <td>
       <a class="link" 
          href="tiki-assignpermission.php?group={$users[user].group_id}" 
          title="{tr}permissions{/tr}">
          <img border="0" 
          alt="{tr}permissions{/tr}" 
          src="img/icons/key.png" />
          {$users[user].permcant}
       </a>
      </td>
      <td style="width: 20px;">
       <a class="link" 
          href="tiki-admingroups.php?offset={$offset}&sort_field={$sort_field}&sort_order={$sort_order}&action=delete&group={$users[user].group_id}"
          title="{tr}delete{/tr}">
          <img border="0"
          alt="{tr}remove{/tr}" 
          src="img/icons/trash.png" />
       </a>
      </td>
   </tr>
  {/section}
</table>
</div>

{* ----------------------- Add groups form ------------------- *}

<a name="2" ></a>


{if $editgroup}
<div id="content2" class="tabcontent" style="display: block;">
  <h2>{tr}Edit group{/tr} {$groupname}</h2>
  <a class="linkbut" href="tiki-assignpermission.php?group={$groupname}">{tr}assign permissions{/tr}</a>
{else}
  {if $add}<div id="content2" class="tabcontent" style="display: block;">
  {else}   <div id="content2" class="tabcontent" style="display: none;">{/if}
  <h2>{tr}Add new group{/tr}</h2>
{/if}

<form action="tiki-admingroups.php" method="post">
  <table class="normal">
    <tr class="formcolor"><td><label for="groups_group">{tr}Group{/tr}:</label></td><td><input type="text" name="name" id="groups_group" value="{$groupname|escape}" /></td></tr>
    <tr class="formcolor"><td><label for="groups_desc">{tr}Description{/tr}:</label></td><td><textarea rows="5" cols="20" name="desc" id="groups_desc">{$groupdesc}</textarea></td></tr>
    <tr class="formcolor"><td><label for="groups_inc">{tr}Include{/tr}:</label></td><td>
        <select name="include_groups[]" id="groups_inc" multiple="multiple" size="4">
          {foreach key=gr item=yn from=$includegroup}
           <option value="{$gr|escape}" {if $yn eq 'y'} selected="selected"{/if}>{$gr|truncate:"52":" ..."}</option>
          {/foreach}
        </select>
    </td></tr>
  
    {if $editgroup}
       <tr class="formcolor">
       <td><input type="hidden" name="olgroup" value="{$editgroup|escape}" /></td>
       <td><input type="submit" name="save" value="{tr}Save{/tr}" /></td>
       </tr>
    {else}
       <tr class="formcolor">
       <td ></td>
       <td><input type="submit" name="newgroup" value="{tr}Add{/tr}" /></td>
       </tr>
    {/if}
    
  </table>
</form>
<br />

{* ----------------------- Edit a group form------------------------------- *}
{if $groupname}
  <h2>{tr}Members List{/tr}: {$groupname}</h2>
  <table class="normal"><tr>
  {if $memberslist}
    {cycle name=table values=',,,,</tr><tr>' print=false advance=false}
    {section name=ix loop=$memberslist}
      <td class="formcolor auto"><a href="tiki-adminusers.php?user={$memberslist[ix]|escape:"url"}&action=removegroup&group={$groupname}{if $feature_tabs ne 'y'}#2{/if}" class="link" title="{tr}remove from group{/tr}"><img src="img/icons/trash.png" border="0" width="16" height="16"  alt='{tr}remove{/tr}'></a> <a href="tiki-adminusers.php?user={$memberslist[ix]|escape:"url"}{if $feature_tabs ne 'y'}#2{/if}" class="link" title="{tr}edit{/tr}"><img src="img/icons/edit.png" border="0" width="20" height="16"  alt='{tr}edit{/tr}'></a> {$memberslist[ix]}</td>{cycle name=table}
    {/section}
    </tr></table>
    <div class="box">{$smarty.section.ix.total} {tr}users in group{/tr} {$groupname}</div>
  {else}
    <td class="formcolor auto"><a href="tiki-admingroups.php?group={$groupname}&show=1{if $feature_tabs ne 'y'}#3{/if}" class="linkbut">{tr}List all members{/tr}</a></td>
    </tr></table>
  {/if}
{/if}

</div>
