{*Smarty template*}

{include file="header.tpl"}

<h1 class="pagetitle">{tr}Batch Import of documents in{/tr} {$container_number}</h1>

<h2 class="pagetitle">{tr}STEP{/tr} {$step} : {tr}{$step_description}{/tr}</h1>

{$body}

{if $step eq 1}
<i>
The cvs file must be construct in accordance to this rules :<br />
<ul>
<li>The first line (header) define the name of the fields to import in the database.</li>
<li>If the header define a field 'file', this file will be store in the vault. This file must be in the wildspace</li>
<li>Be careful : the header define the field name and not the description show to the user</li>
<li>The next lines defines the value of each field.</li>
<li>You can define the header 'category_name' if you prefer set the usual name of the category in place to the id</li>
</ul>
</i>

<form action="{$smarty.server.PHP_SELF}" method="post" name="step1" enctype="multipart/form-data">
<table class="normal">
  <tr>
   <td>
   {assign var='helphead' value='<b>document_number;file;natif_issue;category_id;document_supplier;document_indice_id;designation</b><br />'}
   {assign var='helpcore' value='number;file name;issue;category;supplier;indice;designation'}
   {assign var='helphead' value='<b>document_number;file;natif_issue;category_id;document_supplier;document_indice_id;designation</b><br />'}
   {assign var='helpcore' value='numero;nom du fichier;issue;id de la categorie;fournisseur;id de l\'indice;designation'}
    {tr}Batch upload{/tr} (CSV file<a {popup text="$helphead $helpcore"}><img src="img/icons/help.png" border="0" height="16" width="16" alt='{tr}help{/tr}' /></a>):
   </td>
   <td>
    <input type="file" name="importDocumentCsvList"/>
    <input type="submit" value="visucvs" name="action" />
    <input type="hidden" name="container_id" value="{$container_id}" />
    <input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
   </td>
  </tr>
</table>
</form>


<h2 class="pagetitle">{tr}Or select a zip file{/tr}</h1>
<form action="{$smarty.server.PHP_SELF}" method="post" name="step1" enctype="multipart/form-data">
<table class="normal">
  <tr>
   <td>
    {assign var='helpheadzip' value='The zip file must contains the files to import without subdirectory<br />'}
    {assign var='helpcorezip' value=''}
    {tr}Batch upload{/tr}(zip file<a {popup text="$helpheadzip $helpcorezip"}><img src="img/icons/help.png" border="0" height="16" width="16" alt='{tr}help{/tr}' /></a>):
   </td>
   <td>
    <input type="file" name="importzipfile"/>
    <input type="submit" value="visuzipfile" name="action" />
    <input type="hidden" name="container_id" value="{$container_id}" />
    <input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
   </td>
  </tr>
</table>
</form>


<h2 class="pagetitle">{tr}Or select RbXmlTree{/tr}</h1>
<form action="/ranchbe/rbxmlImport.php" method="post" name="step1" enctype="multipart/form-data">
<table class="normal">
  <tr>
   <td>
    {assign var='helpheadzip' value='The rbXml file is a xml file to define the childs documents of each document<br />'}
    {assign var='helpcorezip' value=''}
    {tr}Batch upload{/tr}(zip file<a {popup text="$helpheadzip $helpcorezip"}><img src="img/icons/help.png" border="0" height="16" width="16" alt='{tr}help{/tr}' /></a>):
   </td>
   <td>
    <input type="file" name="rbxmltreefile"/>
    <input type="submit" value="visualize" name="action" />
    <input type="hidden" name="container_id" value="{$container_id}" />
    <input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
   </td>
  </tr>
</table>
</form>


<!-- 
<h2 class="pagetitle">{tr}Or select the 'transfert data package(TDP)' file{/tr}</h1>
<form action="{$smarty.server.PHP_SELF}" method="post" name="step1" enctype="multipart/form-data">
<table class="normal">
  <tr>
   <td>
    {assign var='helpheadtdp' value='The tdp must be in accordance to the ECSS-M-50B<br />'}
    {assign var='helpcoretdp' value=''}
    {tr}Batch upload{/tr} (CSV file<a {popup text="$helpheadtdp $helpcoretdp"}><img src="img/icons/help.png" border="0" height="16" width="16" alt='{tr}help{/tr}' /></a>):
   </td>
   <td>
    <input type="file" name="importtdpfile"/>
    <input type="submit" value="visutdp" name="action" />
    <input type="hidden" name="container_id" value="{$container_id}" />
   </td>
  </tr>
</table>
</form>
-->
{/if}

{if $step eq 2.1}
  {include file="documentImportZipfile.tpl"}
{/if}

{if $step eq 2}
{*--------------------list header----------------------------------*}
<form name="step2" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <!-- Display fields -->
    {foreach item=field_name from=$fields}
      <input type="hidden" name="fields[{$smarty.section.list.index}]" value="{$field_name}" />
      <th class="heading">
      {tr}{$field_name}{/tr}</a></th>
    {/foreach}

    <th class="heading">
    {tr}Result and Actions{/tr}</a></th>
 </tr>
{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    {foreach item=field_name from=$fields}
      {if $field_name eq category_id}
      <td class="thin"><input type="text" name="{$field_name}[{$smarty.section.list.index}]" value="{$list[list].category_id}" readonly="1"> ({filter_select id=$list[list].$field_name type=category})
      {elseif $field_name eq document_indice_id}
      <td class="thin"><input type="text" name="{$field_name}[{$smarty.section.list.index}]" value="{$list[list].document_indice_id}" readonly="1"> ({filter_select id=$list[list].$field_name type=document_indice})
      {elseif $field_name eq document_number}
      <td class="thin"><input type="text" name="{$field_name}[{$smarty.section.list.index}]" value="{$list[list].document_number}" readonly="1">
      {elseif $field_name eq file}
      <td class="thin"><input type="text" name={$field_name}[{$smarty.section.list.index}]" value="{$list[list].file}" readonly="1">
      {else}
      <td class="thin"><input type="text" name="{$field_name}[{$smarty.section.list.index}]" value="{$list[list].$field_name}">
      {/if}
      <br><i>Currently:{$currentdata[$smarty.section.list.index].$field_name}</i>
      </td>
    {/foreach}

    <td>
    <br />
    <font color="brown"><i> <!--Errors-->
      {foreach item=error from=$list[list].errors}
        - {$error}<br />
      {/foreach}</i>
    </font>
    {if $list[list].doctype.doctype_number}<!--Doctype-->
    <font color="green"><i>Recognize doctype : <b>{$list[list].doctype.doctype_number}</b></i><br /></font>{/if}
    <!--Actions-->
    <br />
  	<select name="docaction[{$smarty.section.list.index}]">
        {foreach item=action key=return from=$list[list].actions}
          <option value="{$return}">{$action}</option>
        {/foreach}
    </select>
    <br /><br />
   </td>
   
   </tr>

   <input type="hidden" name="loop" value="{$smarty.section.list.loop}" />
   <input type="hidden" name="document_id[{$smarty.section.list.index}]" value="{$list[list].document_id}" />
   <input type="hidden" name="file_id[{$smarty.section.list.index}]" value="{$list[list].file_id}" />
  {sectionelse}
    <tr><td colspan="6">{tr}Nothing to display{/tr}</td></tr>
  {/section}
  </table>
  <br />
  <input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
  <input type="hidden" value="{$cvsfile}" name="cvsfile" />
  <input type="submit" value="validatecsv" name="action" />
  <input type="submit" value="cancel" name="action" />
</form>

{/if}

{if $step eq 3}
<form name="step3" method="post" action="{$smarty.server.PHP_SELF}">
<table>

<tr><td>{tr}Running mode{/tr} :<br></td></tr>
<tr class="formcolor"><td>
  <input type="texte" name="maxcsvrow" value="{$maxcsvrow}" id="maxcsvrow" readonly disabled>
   <label for="maxcsvrow">{tr}Max number of rows to import from a csv file{/tr}</label></td></tr>

<tr><td>{tr}Running mode{/tr} :<br></td></tr>
<tr class="formcolor"><td>
  <input type="radio" name="background" value="1" id="background1">
   <label for="background1">{tr}Launch Update in hidden mode{/tr}</label></td></tr>

<tr class="formcolor"><td>
  <input type="radio" name="background" value="0" id="background2">
   <label for="background2">{tr}Direct update{/tr}</label></td></tr>

<tr class="formcolor"><td>
  <input type="checkbox" name="create_category" value="1" id="create_category">
   <label for="create_category">{tr}Create category if not exist{/tr}</label></td></tr>

<tr class="formcolor"><td>
  <input type="checkbox" name="lock" value="1" id="lock">
   <label for="lock">{tr}Lock document after store{/tr}</label></td></tr>

<tr><td>
  <input type="submit" value="run" name="action" />
  <input type="submit" value="cancel" name="action" />
</td>

</tr>
</table>
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
<input type="hidden" value="{$cvsfile}" name="cvsfile" />
</form>
{/if}

<p></p>
