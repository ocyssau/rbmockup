{*Smarty template*}

<div id="page-bar">
     <span class="button2"><a class="linkbut" href="javascript:popup('doctypeManage.php?action=create','workitemManage')">{tr}Create{/tr}</a></span>
  </div>

<br>

<h1 class="pagetitle">{tr}Doctypes list{/tr}</h1>

{*--------------------Search Bar defintion--------------------------*}
{include file='searchBar_simple.tpl'}

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading auto"></th>
  <th class="heading auto"></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=doctype_number">
  {tr}Number{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=doctype_description">
  {tr}Description{/tr}</a></th>

  <!--
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=script_pre_store">
  {tr}script_pre_store{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=script_post_store">
  {tr}script_post_store{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=script_pre_update">
  {tr}script_pre_update{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=script_post_update">
  {tr}script_post_update{/tr}</a></th>
  -->

  <th class="heading">
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=script_pre_store">
  {tr}script_pre_store{/tr}</a><br />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=script_post_store">
  {tr}script_post_store{/tr}</a><br />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=script_pre_update">
  {tr}script_pre_update{/tr}</a><br />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=script_post_update">
  {tr}script_post_update{/tr}</a><br />
  </th>





  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=recognition_regexp">
  {tr}recognition_regexp{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_extension">
  {tr}file_extension{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_type">
  {tr}file_type{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=icon">
  {tr}icon{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=visu_file_extension">
  {tr}visu_file_extension{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=can_be_composite">
  {tr}can_be_composite{/tr}</a></th>

 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].doctype_id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

    <td class="thin"><a href="javascript:popup('doctypeManage.php?action=modify&doctype_id={$list[list].doctype_id}','doctypeManage')" title="{tr}edit{/tr}">
                     <img border="0" alt="{tr}edit{/tr}" src="img/icons/edit.png" />
                     </a>
    </td>

    <td class="thin"><a href="doctypes.php?action=suppress&checked[]={$list[list].doctype_id}" title="{tr}Suppress{/tr}"
                      onclick="return confirm('{tr}Do you want really suppress{/tr} {$list[list].doctype_number}')">
                     <img border="0" alt="{tr}Suppress{/tr}" src="img/icons/trash.png" />
                     </a>
    </td>

    <td class="thin">{$list[list].doctype_number}</td>
    <td class="thin">{$list[list].doctype_description}</td>

    <td class="thin">
      {$list[list].script_pre_store}<br />
      {$list[list].script_post_store}<br />
      {$list[list].script_pre_update}<br />
      {$list[list].script_post_update}<br />
    </td>

    <!--
    <td class="thin">{$list[list].script_pre_store}</td>
    <td class="thin">{$list[list].script_post_store}</td>
    <td class="thin">{$list[list].script_pre_update}</td>
    <td class="thin">{$list[list].script_post_update}</td>
    -->
    <td class="thin">{$list[list].recognition_regexp}</td>
    <td class="thin">{$list[list].file_extension}</td>
    <td class="thin">{$list[list].file_type}</td>
    <td class="thin"><i>{$list[list].icon}</i><br /><img border="0" alt="{$list[list].icon}" src="{$icons_dir}/{$list[list].icon}" /></td>
    <td class="thin">{$list[list].visu_file_extension}</td>
    <td class="thin">{if $list[list].can_be_composite == 1}{tr}Yes{/tr}{else}{tr}No{/tr}{/if}</td>
   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
      //-->                     
      </script>

<br />
{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}" id="01"
onclick="if(confirm('{tr}Do you want really suppress this doctypes{/tr}')){ldelim}document.checkform.action='{$smarty.server.PHP_SELF}'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

{*Submit checkform form*}

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
</form>
