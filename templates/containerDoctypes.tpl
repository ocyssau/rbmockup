{*Smarty template*}

{include file="header.tpl"}

<form name="close"><input type="button" onclick="window.close()" value="Close"></form>
<hr />

<h2>{tr}Linked doctypes{/tr}</h2>

{* -------------------Pagination------------------------ *}
{*{include file='pagination.tpl'}*}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading">{tr}Container{/tr}</th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=doctype_number">
   {tr}Doctype{/tr}</a></th>
  <th class="heading">{tr}Process{/tr}</th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="link_id[]" value="{$list[list].link_id}" /></td>
    <td class="thin">{$container_number}</td>
    <td class="thin">{$list[list].doctype_number}</td>
    <td class="thin">{$list[list].process_id|process}</td>
   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}

{*{include file='pagination.tpl'}*}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'link_id[]',this.checked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
      //-->                     
      </script>

<br>
{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="linkDoctype" title="{tr}Link doctype{/tr}" id="02">
<img class="icon" src="./img/icons/doctype/doctype_link.png" title="{tr}Link doctype{/tr}" alt="{tr}Link doctype{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="unlinkDoctype" title="{tr}Unlink doctype{/tr}" id="01">
<img class="icon" src="./img/icons/doctype/doctype_unlink.png" title="{tr}Unlink doctype{/tr}" alt="{tr}Unlink doctype{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="linkProcess" title="{tr}Link process{/tr}" id="02">
<img class="icon" src="./img/icons/process/process_link.png" title="{tr}Link process{/tr}" alt="{tr}Link process{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="unlinkDoctypeProcess" title="{tr}Unlink process{/tr}" id="02">
<img class="icon" src="./img/icons/process/process_unlink.png" title="{tr}Unlink process{/tr}" alt="{tr}Unlink process{/tr}" width="16" height="16" />
</button>

<hr />

{*Submit checkform form*}

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
</form>


<form name="close"><input type="button" onclick="window.close()" value="Close"></form>
