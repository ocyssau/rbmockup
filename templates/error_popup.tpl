{include file="header.tpl"}

<div id="tiki-main">
  <div id="tiki-mid"> <br />
    <div class="cbox">
      <div class="cbox-title">
        {tr}Error{/tr}
      </div>
      <div class="cbox-data"> <br />
        {$msg}
        <br /><br />
        <a href="javascript:window.close()" class="linkmenu">{tr}Close Window{/tr}</a><br /><br />
      </div>
    </div>
  </div>
</div>
