{*Smarty template*}

{include file="header.tpl"}

<h1 class="pagetitle">{tr}{$listHeader}{/tr}</h1>

<p><i>{$message}</i></p>

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">

<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading">{tr}{$HeaderCol1}{/tr}</th>
  <th class="heading">{tr}{$HeaderCol2}{/tr}</th>
  <th class="heading">{tr}{$HeaderCol3}{/tr}</th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].$id}" /></td>
    <td class="thin">{$list[list].$col1}</td>
    <td class="thin">{$list[list].$col2}</td>
    <td class="thin">{$list[list].$col3}</td>
   </tr>
  {/section}
  </table>

{*Multiselection select action form *}

<input type=hidden name="action" value="{$action}">
<input type=hidden name="flag" value="validate">
<input type=hidden name="project_id" value="{$project_id}">

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />

<i>{tr}Perform action{/tr} :</i>

<input type="submit" name="checkform" value="{tr}Link{/tr}" />
<input type="reset" name="checkform" value="{tr}Close{/tr}" onclick="window.close()"/>

</form>

<p></p>

