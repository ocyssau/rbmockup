{*Smarty template*}

{include file="header.tpl"}

<div id="tiki-main">
<div id="tiki-top">
<div id="tiki-mid">
<div id="tiki-center">

<h2>{tr}Linked Categories to{/tr} {$container_number}</h2>

{* -------------------Pagination------------------------ *}
{*{include file='pagination.tpl'}*}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">

{*--------------------list header----------------------------------*}

<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=category_number">
   {tr}category_number{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=category_description">
   {tr}category_description{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=category_icon">
   {tr}category_icon{/tr}</a></th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="link_id[]" value="{$list[list].link_id}" /></td>

    {*-Specifics fields-*}
    <td class="thin">{$list[list].category_number}</td>
    <td class="thin">{$list[list].category_description}</td>
    <td class="thin">{$list[list].category_icon}</td>
   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}

{*{include file='pagination.tpl'}*}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
<script language='Javascript' type='text/javascript'>
<!--
// check / uncheck all.
// in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
// for now those people just have to check every single box
document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'link_id[]',this.checked)\"/></td>");
document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
//-->                     
</script>

<br>
{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="linkCategory" title="{tr}Link category{/tr}" id="02">
<img class="icon" src="./img/icons/metadata/link.png" title="{tr}Link category{/tr}" alt="{tr}Link category{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="unlinkCategory" title="{tr}Unlink category{/tr}" id="01">
<img class="icon" src="./img/icons/metadata/unlink.png" title="{tr}Unlink category{/tr}" alt="{tr}Unlink category{/tr}" width="16" height="16" />
</button>

<hr />

{*Submit checkform form*}

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
</form>


<form name="close">
  <input type="button" onclick="window.close()" value="Close">
</form>

</div>
</div>
</div>
</div>
</body>
