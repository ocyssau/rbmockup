{*Smarty template*}

{* -------------------Pagination------------------------ *}
<div class="mini">
<div align="center">
{if $prev_offset >= 0}
    <a class="prevnext" href="{sameurl offset=$prev_offset}">{tr}prev{/tr}</a>
{/if}

{tr}Page{/tr}: {$actual_page}
{if $cant_pages == 1}
  {if $next_offset}
    <a class="prevnext" href="{sameurl offset=$next_offset}">{tr}next{/tr}</a>
  {/if}
{/if}

</div>
</div>
