{*Smarty template*}

<div id="page-bar">
  <span class="button2"><a class="linkbut" href="javascript:popupP('ImportHistory.php?action=getImportHistory&space={$CONTAINER_TYPE}','{$randWindowName}',800 , 1200)">{tr}History import{/tr}</a></span>
  <span class="button2"><a class="linkbut" href="javascript:popupP('BatchjobsManager.php?action=list','{$randWindowName}',800 , 1200)">{tr}Jobs{/tr}</a></span>
</div>

<h1 class="pagetitle">{tr}Import a package in{/tr} {$CONTAINER_TYPE}</h1>

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading auto"></th>
  <th class="heading auto"></th>
  <th class="heading auto"></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=import_order">
  {tr}import_order{/tr}</a></th>

  <th class="heading">
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=package_file_name">
  {tr}package_file_name{/tr}</a>.
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=package_file_extension">
  {tr}package_file_extension{/tr}</a>
  </th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=description">
  {tr}description{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=mockup_id">
  {tr}mockup_id{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=state">
  {tr}state{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=import_by">
  {tr}import_by{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=import_date">
  {tr}import_date{/tr}</a></th>
  <!--{*<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=import_errors_report">
  {tr}import_errors_report{/tr}</a></th>*}-->
  <th class="heading">
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=import_logfiles_file">
  {tr}import_logfiles_file{/tr}</a><br />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=import_errors_report">
  {tr}import_errors_report{/tr}</a>
  </th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=package_file_path">
  {tr}package_file_path{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=package_file_mtime">
  {tr}package_file_mtime{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=package_file_size">
  {tr}package_file_size{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=package_file_md5">
  {tr}package_file_md5{/tr}</a></th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].import_order}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>
    </td>

    <td class="thin">
      <a href="javascript:popup('ImportManager.php?space={$CONTAINER_TYPE}&action=addDescription&import_order={$list[list].import_order}','AddDescription')" title="{tr}Add a description{/tr}">
      <img border="0" alt="{tr}addDescription{/tr}: {$list[list].import_order}" src="img/icons/edit.png" />
      </a>
    </td>

    <td class="thin">
      <a href="javascript:popup('ImportManager.php?space={$CONTAINER_TYPE}&action=viewContents&file={$list[list].package_file_path}/{$list[list].package_file_name}','viewContents')" title="{tr}See the contents{/tr}">
      <img border="0" alt="{tr}ViewContents{/tr}" src="img/icons/eye.png" />
      </a>
    </td>

    <td class="thin">
      <a href="javascript:popup('ImportManager.php?space={$CONTAINER_TYPE}&action=viewImported&import_order={$list[list].import_order}','viewImported')" title="{tr}See linked imported files{/tr}">
      <img border="0" alt="{tr}viewImported{/tr}" src="img/icons/zoom.gif" />
      </a>
    </td>

      <td class="thin">{$list[list].import_order}</td>
      <td class="thin">{$list[list].package_file_name}</td>
      <td class="thin">{$list[list].description}</td>
      <td class="thin">{filter_select id=$list[list].$CONTAINER_ID type=$CONTAINER_TYPE}</a></td>
      <td class="thin">{$list[list].state}</td>
      <td class="thin">{$list[list].import_by|username}</td>
      <td class="thin">{$list[list].import_date|date_format}</td>
      <!--{*<td class="thin">{$list[list].import_errors_report}</td>*}-->

      <td class="thin">
      {if $list[list].import_logfiles_file}
        <a href="ImportManager.php?space={$CONTAINER_TYPE}&action=viewLogFile&import_order={$list[list].import_order}" title="{tr}Log{/tr}">
        ViewLog</a><br />
      {/if}
      {if $list[list].import_errors_report}
        <a href="ImportHistory.php?space={$CONTAINER_TYPE}&action=viewErrorFile&import_order={$list[list].import_order}" title="{tr}Errors{/tr}">
        ViewErrors</a>
      {/if}
      </td>

      <td class="thin">{$list[list].package_file_path}</td>
      <td class="thin">{$list[list].package_file_mtime|date_format}</td>
      <td class="thin">{$list[list].package_file_size|filesize_format}</td>
      <td class="thin">{$list[list].package_file_md5}</td>
   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}

{include file='pagination.tpl'}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr} : </label></td></tr>");
      //-->                     
      </script>

<br>
{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>
<button class="mult_submit" type="submit" name="action" value="suppressPackage" title="{tr}Suppress{/tr}"
onclick="if(confirm('{tr}Do you want really suppress this file{/tr}')){ldelim}pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="uncompressPackage" title="{tr}Uncompress{/tr}" onclick="pop_no(checkform)" />
<img class="icon" src="./img/icons/package/package_uncompress.png" title="{tr}Uncompress{/tr}" alt="{tr}Uncompress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="createbatchjob" title="{tr}Create batch job{/tr}" onclick="pop_it(checkform, 500, 1800)" />
<img class="icon" src="./img/icons/package/package_batchjob.png" title="{tr}Create batch job{/tr}" alt="{tr}Create batch job{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="refresh" title="{tr}Refresh{/tr}" onclick="pop_no(checkform)" />
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<h2>
Free space on mockups directories: {$freeMockupDiskSpace|filesize_format}<br/>
Free space on import directories: {$freeImportDiskSpace|filesize_format}<br/>
</h2>

<table>
<tr><td>{tr}Data type{/tr} :<br></td></tr>
<tr class="formcolor"><td>
  <input type="radio" name="target" value="InTempdir" id="datatype1">
   <label for="datatype1">{tr}To put in a temp dir{/tr}</label></td></tr>

<tr class="formcolor"><td>
  <input type="radio" name="target" value="InContainer" id="datatype2">
   <label for="datatype2">{tr}To put in{/tr} {$CONTAINER_TYPE}</label></td></tr>

<tr><td>{tr}Running mode{/tr} :<br></td></tr>
<tr class="formcolor"><td>
  <input type="radio" name="background" value="1" id=background1>
   <label for="background1">{tr}Launch Update in hidden mode{/tr}</label></td></tr>

<tr class="formcolor"><td>
  <input type="radio" name="background" value="0" id=background2>
   <label for="background2">{tr}Direct update{/tr}</label></td></tr>

<tr><td>
  <button class="mult_submit" type="submit" name="action" value="unpack" title="{tr}Update mockup{/tr}" id="01"
  onclick="pop_it(checkform)">
  <img class="icon" src="./img/icons/tick.png" title="{tr}Update mockup{/tr}" alt="{tr}Update mockup{/tr}" width="16" height="16" />
  </button>
 </td>

</tr>
</table>

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
</form>


{* ------------------- Upload section ------------------------ *}
<fieldset>
<legend align="top"><i>{tr}Add a package to the package deposit dir{/tr}</i></legend>
<form action="{$smarty.server.PHP_SELF}" method="post" name="upload" enctype="multipart/form-data">
    <input type="file" name="uploadFile"/>
    <input type="submit" value="upload" name="action" />
    <br />{tr}Overwrite{/tr}: <input type="checkbox" name="overwrite" />
</fieldset>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
</form>
