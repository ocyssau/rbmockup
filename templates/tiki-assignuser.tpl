{*Smarty template*}

<div id="BoiteActions">
  <span class="button2"><a href="tiki-admingroups.php" class="linkbut">{tr}Admin groups{/tr}</a></span>
  <span class="button2"><a href="tiki-adminusers.php" class="linkbut">{tr}Admin users{/tr}</a></span>
</div>

<h1 class="pagetitle">{tr}Assign user{/tr} {$assign_user_handle} {tr}to groups{/tr}</a></h1>

<br />

{*--------------------User informations----------------*}
<h2>{tr}User Information{/tr}</h2>
<table class="normal">
<tr><td class="even">{tr}Login{/tr}:</td><td class="odd">{$user_info.handle}</td></tr>
<tr><td class="even">{tr}Email{/tr}:</td><td class="odd">{$user_info.email}</td></tr>
<tr><td class="even">{tr}Groups{/tr}:</td><td class="odd">
  {section name=include_groups loop=$include_groups}
   {$include_groups[include_groups].group_define_name}
    (<a class="link" href="tiki-assignuser.php?offset={$offset}&sort_mode={$sort_mode}&assign_user={$assign_user_id|escape:url}&action=removegroup&group={$include_groups[include_groups].group_id|escape:url}" 
        title="remove">x</a>)
  &nbsp;&nbsp;
  {/section}
</td></tr>

<form method="post" action="tiki-assignuser.php?assign_user={$assign_user_id}">
<tr><td class="even">{tr}Default Group{/tr}:</td><td class="odd">
 <select name="defaultgroup">
   <option value=""></option>
   {foreach from=$user_info.groups key=name item=included}
    <option value="{$name}" {if $name eq $user_info.default_group}selected="selected"{/if}>{$name}</option>
   {/foreach}
 </select>
<input type="hidden" value="{$user_info.handle}" name="handle" />
<input type="submit" value="{tr}set{/tr}" name="set_default" />
</form>
</td></tr>
</table>

<br />
<div align="center">
<table class="findtable">
<tr><td class="findtable">{tr}Find{/tr}</td>
   <td class="findtable">
   <form method="get" action="tiki-assignuser.php">
     <input type="text" name="find" value="{$find|escape}" />
     <input type="submit" value="{tr}find{/tr}" name="search" />
     <input type="hidden" name="sort_mode" value="{$sort_mode|escape}" />
     <input type="hidden" name="assign_user" value="{$assign_user_id|escape}" />
   </form>
   </td>
</tr>
</table>

{*--------------------Group list header----------------*}
<div align="left"><h2>{tr}Available groups{/tr}</h2></div>
<table class="normal">
  <tr>
    <td class="heading"><a class="tableheading" href="tiki-assignuser.php?assign_user={$assign_user_id|escape:url}&amp;offset={$offset}&amp;sort_mode={if $sort_mode eq 'groupName_desc'}groupName_asc{else}groupName_desc{/if}">{tr}name{/tr}</a></td>
    <td class="heading"><a class="tableheading" href="tiki-assignuser.php?assign_user={$assign_user_id|escape:url}&amp;offset={$offset}&amp;sort_mode={if $sort_mode eq 'groupDesc_desc'}groupDesc_asc{else}groupDesc_desc{/if}">{tr}desc{/tr}</a></td>
    <td class="heading">{tr}action{/tr}</td>
  </tr>

{*--------------------Group list body----------------*}
 {cycle values="even,odd" print=false}
  {section name=groups loop=$groups}
    <tr>
      <td class="{cycle advance=false}">{$groups[groups].group_define_name}
        &nbsp;&nbsp;(
        <a class="link" href="tiki-assignpermission.php?group={$groups[groups].group_id|escape:url}">{tr}assign perms to this group{/tr}</a>)</td>
      <td class="{cycle advance=false}">{tr}{$groups[groups].group_define_name}{/tr}</td>
      <td class="{cycle}">
        <a class="link" href="tiki-assignuser.php?offset={$offset}&sort_mode={$sort_mode}&action=assign&group={$groups[groups].group_id|escape:url}&assign_user={$assign_user_id|escape:url}">
          {tr}assign{/tr} {$user_info.handle} {tr}to{/tr} "{$groups[groups].group_define_name}"
        </a>
      </td>
    </tr>
  {/section}
</table>

<br />

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

</div>
</div>
