{*Smarty template*}

{*--------------------Search Bar defintion--------------------------*}
<div id="sitesearchbar">
<form id="filterf" action="{$smarty.server.PHP_SELF}" method="post">
<fieldset>
{sameurlpost}
<br />
  <label>
  <input size="4" type="text" name="numrows" value="{$numrows|escape}" />
  <small>{tr}rows to display{/tr}</small></label>
<br />

<label for="find"><small>{tr}find{/tr}</small></label>
<input size="16" type="text" name="find" value="{$find|escape}" />

<label for="find_field"><small>{tr}In{/tr}</small></label>
<select name="find_field" onchange='javascript:getElementById("filterf").submit();'>
<option {if '' eq $find_field}selected="selected"{/if} value=""></option>
{foreach from=$all_field item=name key=field}
 <option {if $field eq $find_field}selected="selected"{/if} value="{$field|escape}">{$name}</option>
{/foreach}
</select>

<input type="submit" name="filter" value="{tr}filter{/tr}" />

<br />
</form>
</fieldset>

</div>
