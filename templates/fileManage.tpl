{*Smarty template*}

<div id="page-bar">
  <span class="button2"><a class="linkbut" href="javascript:popupP('ImportHistory.php?action=getImportHistory&space={$CONTAINER_TYPE}&fcontainer={$container_id}','{$randWindowName}',800 , 1200)">{tr}History import{/tr}</a></span>
  <span class="button2"><a class="linkbut" href="ImportManager.php?space={$CONTAINER_TYPE}">{tr}Import{/tr}</a></span>
</div>

<h1 class="pagetitle">{tr}Files manager{/tr}</h1>

{*--------------------Search Bar defintion--------------------------*}
{include file='searchBar_simple.tpl'}

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_name">
  {tr}File Name{/tr}</a> .
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_extension">
  {tr}File Extension{/tr}</a> -
  <a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_version">
  {tr}Version{/tr}</a><br />
  <a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_type">
  (<i>{tr}Type{/tr}</i>)</a><br />
  <!--<a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_md5">
  <font size=-3>[{tr}MD5{/tr}]</font></a>-->
  </th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=import_order">
  {tr}Imported from{/tr}</a></th>

  <th class="heading">
  {tr}Open{/tr}
  <a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_open_by">
  {tr}By{/tr}</a>/<a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_open_date">{tr}Date{/tr}</a>
  <br />
  {tr}Update{/tr}
  <a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_update_by">
  {tr}By{/tr}</a>/<a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_update_date">{tr}Date{/tr}</a>
  <br />
  <a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_mtime">
  {tr}Mtime{/tr}</a>
  </th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq DESC}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=file_size">
  {tr}Size{/tr}</a></th>

 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].file_id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

    <td class="thin">
    <a class="link" href="DocManage.php?space={$CONTAINER_TYPE}&action=viewFile&checked[]={$list[list].file_id}&ticket={$ticket}" title="{tr}View file{/tr}">
    {file_icon extension=$list[list].file_extension icondir=$file_icons_dir icontype='.gif'}
    {$list[list].file_name} - {$list[list].file_version}</a>
    <br />
    <i>({$list[list].file_type})</i>
    <br />
    <!--<font size=-3>[{$list[list].file_md5}]</font></a>-->
    </td>

    <td class="thin">

      <a href="javascript:popupP('ImportHistory.php?action=getImportHistory&space={$CONTAINER_TYPE}&find_field=package_file_name&find={$list[list].import_order|import_package_name}','viewImported', 600 , 1024)" title="{tr}See package details{/tr}">
      {$list[list].import_order|import_package_name}</a> <br />
      <font size="-3">(<i>{tr}Order{/tr} : {$list[list].import_order}</i>)</font>
    </td>

    <td class="thin">
    <b>{tr}Created{/tr} : </b>{$list[list].file_open_by|username} - {$list[list].file_open_date|date_format}<br />
    <b>{tr}Updated{/tr} : </b>{$list[list].file_update_by|username} - {$list[list].file_update_date|date_format}<br />
    <b>{tr}mtime{/tr} : </b>{$list[list].file_mtime|date_format}<br />
    </td>

    <td class="thin">{$list[list].file_size|filesize_format}</td>
   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}

{include file='pagination.tpl'}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
      //-->                     
      </script>
        <br>

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}" id="01"
onclick="if(confirm('{tr}Do you want really suppress this file{/tr}')){ldelim}document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
 onclick="document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform)">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="{$CONTAINER_ID}" value="{$container_id}" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
</form>
