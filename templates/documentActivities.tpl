{*Smarty template*}
 
{include file=galaxia_html_header.tpl}

{popup_init src="lib/overlib.js"}

{*INFOS ABOUT DOCUMENT*}
<table class="normal">
<tr><td><h4>{tr}Document{/tr}:</h4></td></tr>
<tr>
	<td class="formcolor">{tr}Number{/tr}:</td>
	<td class="formcolor">{$document_number}</td>
</tr><tr>
	<td class="formcolor">{tr}Indice{/tr}:</td>
	<td class="formcolor">{$document_indice|document_indice}</td>
</tr><tr>
	<td class="formcolor">{tr}Designation{/tr}:</td>
	<td class="formcolor">{$document_designation}</td>
</tr>
</table>
<br />


{*INFOS ABOUT PROCESS*}
<table class="normal">
<tr><td><h4>{tr}Process informations{/tr}:</h4></td></tr>
<tr>
	<td class="formcolor">{tr}Process{/tr}:</td>
	<td class="formcolor">{$proc_info.name} {$proc_info.version}</td>
</tr>
</table>
<br />

{*INFOS ABOUT INSTANCE*}
{if $ins_info.name}
<table class="normal">
<tr><td><h4>{tr}Instance informations{/tr}:</h4></td></tr>

<tr>
	<td class="formcolor">{tr}Instance{/tr}:</td>
	<td class="formcolor">{$ins_info.name}</td>
</tr>

<tr>
	<td class="formcolor">{tr}Created{/tr}:</td>
	<td class="formcolor">{$ins_info.started|date_format}</td>
</tr>

<tr>
	<td class="formcolor">{tr}Status{/tr}:</td>
	<td class="formcolor">{$ins_info.status}</td>
</tr>

<tr>
	<td class="formcolor">{tr}Owner{/tr}:</td>
	<td class="formcolor">{$ins_info.owner}</td>
</tr>

</table>
<br />
{/if}

{*ACTIVITIES*}
<table class="normal">
<tr align="center"><td><h4>{tr}Activities{/tr}</h4></td></tr>
<tr><td>
{section name=ix loop=$acts}
    {if $acts[ix].type eq 'standalone'}
    <a href="DocManage.php?action=RunActivity&activityId={$acts[ix].activityId}&checked[]={$document_id}&space={$CONTAINER_TYPE}&page_id=DocManage_changestate&start_page={$start_page}">
    {$acts[ix].type|act_icon:$acts[ix].isInteractive} {$acts[ix].name}</a>
    </a>

  {elseif $acts[ix].type eq 'start' && !$ins_info.name}
    <b>Start</b><img src="./img/icons/arrow_right.png" />
	  <a href="DocManage.php?action=RunActivity&activityId={$acts[ix].activityId}&createInstance=1&name={$document_number}&checked[]={$document_id}&space={$CONTAINER_TYPE}&page_id=DocManage_changestate&start_page={$start_page}">
    {$acts[ix].type|act_icon:$acts[ix].isInteractive} {$acts[ix].name}</a>

  {elseif $acts[ix].actstatus eq 'running'}
    <a href="DocManage.php?action=RunActivity&iid={$iid}&amp;activityId={$acts[ix].activityId}&checked[]={$document_id}&space={$CONTAINER_TYPE}&page_id=DocManage_changestate&start_page={$start_page}">
    {$acts[ix].type|act_icon:$acts[ix].isInteractive} {$acts[ix].name}</a>

  {/if}
{sectionelse}
</td>
<td class="{cycle advance=false}" colspan="6">
	{tr}No processes defined yet{/tr}
</td>
{/section}
<td>
<a href="javascript:popupP('DocManage.php?action=ResetProcess&document_id={$document_id}&iid={$iid}&space={$CONTAINER_TYPE}&page_id=DocManage_changestate','ResetProcess' )" title="{tr}Reset the current process{/tr}"
  onclick="return confirm('{tr}Do you want really to reset this instance{/tr}')">
<img border="0" alt="{tr}ResetProcess{/tr}" src="./img/icons/state/state_delete.png" title="{tr}Reset the current process{/tr}"/>
</a>
</td>
</tr>
</table>

<br />
<form>
  <input type="button" value="Refresh" OnClick="javascript:window.location.reload()">
  <input type="button" onclick="window.close()" value="Close">
  <input type="hidden" name="ticket" value="{$ticket}" />
  <input type="hidden" name="page_id" value="DocManage_changestate" />
  <input type="hidden" name="start_page" value="{$start_page}" />
</form>
<br />

<table class="normal">
<tr>
	<td colspan="6" align="center">
  <img src="{$graph}" title="{tr}Graph{/tr}" alt="{tr}Graph is not accessible{/tr}"/>
</td>
</tr>	
</table>

