<ul id="tabnav">
	<li class="{$accueilTab}"><a href="accueil.php">{tr}Welcome{/tr}</a></li>
	
	{if $projectModule == true}
		<li class="{$projectsTab}"><a href="projects.php">{tr}Projects{/tr}</a></li>
	{/if}

	{if $workitemModule == true}
		<li class="{$workitemTab}"><a href="workitems.php">{tr}WorkItems{/tr}</a></li>
	{/if}

	{if $wildspaceTab == 'active' || $documentManage == 'active' ||
	$docfileManage == 'active' || $containerConsultTab == 'active'} 
	{assign var="mySpace" value='active'} {/if}
	{php}
	global $smarty;
	if(isset($_SESSION['myspace']['activePage']))
		$smarty->assign('mySpaceActivePage', $_SESSION['myspace']['activePage']);
	else
		$smarty->assign('mySpaceActivePage', 'wildspace.php');
	$smarty->assign('DisplayDocfileTab', $_SESSION['DisplayDocfileTab']);
	{/php}

	<li class="{$mySpace}"><a href="{$mySpaceActivePage}">{tr}My space{/tr}</a></li>
	
	{if $mockupModule == true}
		<li class="{$mockupTab}"><a href="mockups.php">{tr}Mockups{/tr}</a></li>
	{/if}

	{if $cadlidModule == true}
		<li class="{$cadlibTab}"><a href="cadlibs.php">{tr}Cadlibs{/tr}</a></li>
	{/if}

	{if $bookshopModule == true}
		<li class="{$bookshopTab}"><a href="bookshops.php">{tr}Bookshops{/tr}</a></li>
	{/if}

	{if $partnerModule == true}
		<li class="{$partnersTab}"><a href="partners.php">{tr}Partners{/tr}</a></li>
	{/if}

	{if $adminTab=='active'||$doctypesTab=='active'||$workflowTab=='active'||$usersTab=='active'||$parametersTab== 'active'} 
		{assign var="adminTab" value='active'}
	{/if}

	{if $adminModule == true}
		<li class="{$adminTab}"><a href="admin.php">{tr}Admin{/tr}</a></li>
	{/if}
</ul>

{if $mySpace=='active'}{include file='mySpaceTabs.tpl'}{/if}

{if $adminTab=='active'}{include file='adminTabs.tpl'}{/if}


{* OLD tabs.tpl
<ul id="tabnav">
  <li class="{$accueilTab}"><a href="accueil.php">{tr}Welcome{/tr}</a></li>
  <li class="{$projectsTab}"><a href="projects.php">{tr}Projects{/tr}</a></li>
  <li class="{$workitemTab}"><a href="workitems.php">{tr}WorkItems{/tr}</a></li>

  {if $wildspaceTab == 'active' || $documentManage == 'active' || $docfileManage == 'active' || $containerConsultTab == 'active'}
    {assign var="mySpace" value='active'}
  {/if}

  {php}
  global $smarty;
  if (isset($_SESSION['myspace']['activePage']))
    $smarty->assign('mySpaceActivePage', $_SESSION['myspace']['activePage']);
  else
    $smarty->assign('mySpaceActivePage', 'wildspace.php');
  $smarty->assign('DisplayDocfileTab', $_SESSION['DisplayDocfileTab']);
  {/php}
  
	  <li class="{$mySpace}"><a href="{$mySpaceActivePage}" >{tr}My space{/tr}</a></li>
	  <li class="{$mockupTab}"><a href="mockups.php">{tr}Mockups{/tr}</a></li>
	  <li class="{$cadlibTab}"><a href="cadlibs.php" >{tr}Cadlibs{/tr}</a></li>
	  <li class="{$bookshopTab}"><a href="bookshops.php" >{tr}Bookshops{/tr}</a></li>
	  <li class="{$partnersTab}"><a href="partners.php" >{tr}Partners{/tr}</a></li>
	
  {if $adminTab=='active'||$doctypesTab=='active'||$workflowTab=='active'||$usersTab=='active'||$parametersTab == 'active'}
    {assign var="adminTab" value='active'}
  {/if}

  <li class="{$adminTab}"><a href="admin.php" >{tr}Admin{/tr}</a></li>
</ul>

{if $mySpace=='active'}{include file='mySpaceTabs.tpl'}{/if}
{if $adminTab=='active'}{include file='adminTabs.tpl'}{/if}
*}