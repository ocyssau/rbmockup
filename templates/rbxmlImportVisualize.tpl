{*Smarty template*}


{include file="header.tpl"}


{$form.javascript}

<div id="tiki-main">

<div id="tiki-top">{include file="tiki-top_bar.tpl"}</div>

<div id="tiki-mid">{include file="tabs.tpl"} <br>

<div id="tiki-center">

<h1>Root product: {$rootProductName}</h1>

{*--------------------list header----------------------------------*}
<form name="step2" method="post" action="{$smarty.server.PHP_SELF}">

{foreach item=field_name from=$fields} <input type="hidden"
	name="fields[]" value="{$field_name}" /> {/foreach}

<table class="normal">
	<tr>
		{foreach item=field_name from=$fields}
		<th class="heading auto">{$field_name}</th>
		{/foreach}
	</tr>
	{*--------------------list Body----------------------------------*}
	{cycle print=false values="even,odd"}
	{section name=list loop=$list}
		<tr class="{cycle}">
			{foreach item=field_name from=$fields}
				<td>{$list[list].$field_name}</td>
			{/foreach}
		</tr>
	{/section}
</table>

<input type="hidden" name="xmlfile" value="{$xmlfile}" />
<input type="submit" value="run" name="action" />
<input type="submit" value="cancel" name="action" />
<input type="hidden" name="page_id" value="documentImportRbXml" />

</form>

</div>
</div>

<div id="tiki-bot">{include file="tiki-bot_bar.tpl"}</div>

</div>
