{*Smarty template*}

{include file="header.tpl"}

<br>

<h1 class="pagetitle">{tr}{$page_title}{/tr}</h1>

{*--------------------Search Bar defintion--------------------------*}
{include file='searchBar_simple.tpl'}

<form id="resetf" action="{$smarty.server.PHP_SELF}" method="post">
  <input type="hidden" name="container_id" value="{$container_id}" />
  <input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
  <input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" />
</form>

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading auto"></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=histo_order">
  {tr}histo_order{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=action_name">
  {tr}action_name{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=action_by">
  {tr}action_by{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=action_date">
  {tr}action_date{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field={$id_map}">
  {tr}Id{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field={$number_map}">
  {tr}Number{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field={$description_map}">
  {tr}Description{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field={$state_map}">
  {tr}State{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field={$indice_map}">
  {tr}Indice{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field={$version_map}">
  {tr}Version{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=open_date">
   {tr}Creation date{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=open_by">
   {tr}Created by{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=forseen_close_date">
   {tr}Forseen close date{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=close_date">
   {tr}Close date{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field={$father_map_id}">
   {tr}Father{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=default_file_path">
   {tr}Default file path{/tr}</a></th>

 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="histo_order[]" value="{$list[list].histo_order}" /></td>

    <td class="thin"><a href="ContainerHistory.php?action=suppress&histo_order[]={$list[list].histo_order}&checked[]={$checked}&space={$CONTAINER_TYPE}" title="{tr}Suppress{/tr}: {$list[list].histo_order}"
                      onclick="return confirm('{tr}Do you want really suppress{/tr} history {$list[list].histo_order}')">
                     <img border="0" alt="{tr}Suppress{/tr}: {$list[list].histo_order}" src="img/icons/trash.png" />
                     </a>
    </td>

    <td class="thin" class="link" >{$list[list].histo_order}</td>
    <td class="thin" class="link" >{$list[list].action_name}</td>
    <td class="thin" class="link" >{$list[list].action_by}</td>
    <td class="thin" class="link" >{$list[list].action_date|date_format}</td>

    <td class="thin" class="link" >{$list[list].$id_map}</td>
    <td class="thin" class="link" >{$list[list].$number_map}</td>
    <td class="thin" class="link" >{$list[list].$description_map}</td>
    <td class="thin" class="link" >{$list[list].$state_map}</td>
    <td class="thin" class="link" >{$list[list].$indice_map}</td>
    <td class="thin" class="link" >{$list[list].$version_map}</td>
    <td class="thin" class="link" >{$list[list].open_date|date_format}</td>
    <td class="thin" class="link" >{$list[list].open_by}</td>
    <td class="thin" class="link" >{$list[list].forseen_close_date|date_format}</td>
    <td class="thin" class="link" >{$list[list].close_date|date_format}</td>
    <td class="thin" class="link" >{$list[list].$father_map_id}</td>
    <td class="thin" class="link" >{$list[list].default_file_path}</td>
   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
    <script language='Javascript' type='text/javascript'>
    <!--
    // check / uncheck all.
    // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
    // for now those people just have to check every single box
    document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'histo_order[]',this.checked)\"/></td>");
    document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
    //-->                     
    </script>

    <br>

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}"
 onclick="if(confirm('{tr}Do you want really suppress this history{/tr}')){ldelim}pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}">
  <img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<p>
</p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_mode" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />

{section name=checked loop=$checked}
<input type="hidden" name="checked[]" value="{$checked[checked]}" />
{/section}

</form>

<p>
<form name="close">
  <input type="button" onclick="window.close()" value="Close">
</form>
</p>
