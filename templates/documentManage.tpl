{*Smarty template*}

<div id="page-bar">
    <span class="button2"><a class="linkbut" href="javascript:popup('{$CONTAINER_TYPE}DocHistory.php','DocumentHistory')">{tr}History of all{/tr}</a></span>
    <span class="button2"><a class="linkbut" href="{$CONTAINER_TYPE}Import.php">{tr}Import{/tr}</a></span>
</div>

<br>

<!--
<b>{tr}Access code legend{/tr}</b> : 
<ul>
  <li><b>0</b>:{tr}All access free{/tr}</li>
  <li><b>1</b>:{tr}Modification in progress, checkout forbidden{/tr}</li>
  <li><b>5</b>:{tr}workflow in progress, checkout forbidden{/tr}</li>
  <li><b>10</b>:{tr}Document validate, checkout and worflow request forbidden{/tr}</li>
  <li><b>11</b>:{tr}Lock by user, checkout and worflow request forbidden{/tr}</li>
  <li><b>15</b>:{tr}Indice is upgraded, all action forbidden{/tr}</li>
</ul>
<br>
-->

<h1 class="pagetitle">{tr}Documents list{/tr}</h1>

{*--------------------Search Bar defintion--------------------------*}
{*assign var="histoOption" value="1"*}
{include file='searchBar_doc.tpl'}
{*include file='searchBar.tpl'*}


{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading auto" {popup text='Various actions document...'}>Ac</th>

<!--  
  <th class="heading auto" {popup text='Edit the document...'}>{tr}Ed{/tr}</th>
  <th class="heading auto" {popup text='Historic of the document...'}>Hi</th>
  <th class="heading auto" {popup text='Change state...'}>WF</th>
  <th class="heading auto" {popup text='Change indice...'}>Ind</th>
-->
  <th class="heading auto" {popup text='Suppress the document...'}>S</th>

  <th class="heading auto" {popup text='View the document...'}>View</th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=document_number">
  {tr}Number{/tr}</a></th>

  <th width="50" class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=designation">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{tr}Designation{/tr}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=document_indice_id">
  {tr}Indice{/tr}</a><hr />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=document_version">
  {tr}Version{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=category_id">
  {tr}Category{/tr}</a></th>

  <th class="heading">{tr}CheckOut{/tr}
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=check_out_by">
  {tr}By{/tr}</a><hr />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=check_out_date">{tr}Date{/tr}</a>
  </th>

  <th class="heading"><a 
  {popup text='<b>Access code legend</b> :<ul>
  <li><b>0</b>:All access free</li>
  <li><b>1</b>:Modification in progress, checkout forbidden</li>
  <li><b>5</b>:workflow in progress, checkout forbidden</li>
  <li><b>10</b>:Document validate, checkout and workflow request forbidden</li>
  <li><b>11</b>:Lock by user, checkout and workflow request forbidden</li>
  <li><b>15</b>:Indice is upgraded, all action forbidden</li></ul>'}
  class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=document_access_code">
  {tr}access{/tr}</a><hr />

  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=document_state">
  {tr}State{/tr}</a></th>

  <th class="heading">{tr}Created{/tr}
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=open_by">
  {tr}By{/tr}</a><hr />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=open_date">{tr}Date{/tr}</a>
  </th>

  <th class="heading">{tr}Last Update{/tr}
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=update_by">
  {tr}By{/tr}</a><hr />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=update_date">{tr}Date{/tr}</a>
  </th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=doctype_id">
  {tr}Type{/tr}</a></th>

  <!-- Display optionnals fields -->
  {section name=of loop=$optionalFields}
    <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field={$optionalFields[OF].field_name}">
   {tr}{$optionalFields[of].field_description}{/tr}</a></th>
  {/section}
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin">
    <input type="checkbox" name="checked[]" value="{$list[list].document_id}" id="checkbox_tbl_{$smarty.section.list.index}" {if $list[list].checked eq 'y'}checked="checked" {/if} /><br />
    {$list[list].document_access_code|access_code_concis}</td>

    <!--Contextual menu defined with overlib librairy -->
    <td class="thin">
    <span class="button2" style="white-space:nowrap;"><a 

    {popup text="<ul>
      <li><a href='`$CONTAINER_TYPE`DocManage.php?action=CheckOut&checked[]=`$list[list].document_id`&ticket=`$ticket`'><img border=0 src=./img/icons/document/document_out.png />CheckOut</a></li>
      <li><a href='`$CONTAINER_TYPE`DocManage.php?action=CheckIn&checked[]=`$list[list].document_id`&ticket=`$ticket`'><img border=0 src=./img/icons/document/document_in.png />CheckIn</a></li>
      <li><a href='`$CONTAINER_TYPE`DocManage.php?action=Update&checked[]=`$list[list].document_id`&ticket=`$ticket`'><img border=0 src=./img/icons/document/document_refresh.png />Update</a></li>
      <li><a href='`$CONTAINER_TYPE`DocManage.php?action=Reset&checked[]=`$list[list].document_id`&ticket=`$ticket`'><img border=0 src=./img/icons/document/document_cancel.png />CancelCheckOut</a></li>
      <li><a href='`$CONTAINER_TYPE`DocManage.php?action=putInWs&checked[]=`$list[list].document_id`&ticket=`$ticket`'><img border=0 src=./img/icons/document/document_read.png />Put in Wildspace</a></li>
      <li><a href='`$CONTAINER_TYPE`DocManage.php?action=assocFile&checked[]=`$list[list].document_id`&ticket=`$ticket`'><img border=0 src=./img/icons/file/file_attach.png />Associate a file</a></li>
<!--      <li><a href='`$CONTAINER_TYPE`DocManage.php?action=assocDocument&checked[]=`$list[list].document_id`&ticket=`$ticket`'>Associate a document</a></li> -->
      <li><a href='`$CONTAINER_TYPE`DocManage.php?action=Lock&checked[]=`$list[list].document_id`&ticket=`$ticket`'><img border=0 src=./img/icons/lock.png />Lock</a></li>
      <li><a href='`$CONTAINER_TYPE`DocManage.php?action=unLock&checked[]=`$list[list].document_id`&ticket=`$ticket`'><img border=0 src=./img/icons/lock_open.png />Unlock</a></li>
      <li><a href='`$CONTAINER_TYPE`DocManage.php?action=reset_doctype&checked[]=`$list[list].document_id`&ticket=`$ticket`'><img border=0 src=./img/icons/doctype/doctype_reset.png />Recalculate the doctype</a></li>
<!--
      <li><a href='`$CONTAINER_TYPE`DocManage.php?action=modifyDoc&checked[]=`$list[list].document_id`'>Edit</a></li>
      <li><a href='javascript:popup('`$CONTAINER_TYPE`DocHistory.php?action=getHistory&document_id=`$list[list].document_id`','DocumentManage')'>History</a></li>
-->
    </ul>"
    sticky=1 caption=`$list[list].document_number` trigger='onClick' fgcolor=white closeclick=1}
    <b>Act</b></a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <hr>

    <a href="javascript:popupP('{$CONTAINER_TYPE}DocManage.php?action=modifyDoc&checked[]={$list[list].document_id}&page_id=DocManage_edit','DocumentManage', 700 , 600)" title="{tr}edit{/tr}">
                     <img border="0" alt="{tr}edit{/tr}" src="img/icons/edit.png" />
                     </a>

    <a href="javascript:popupP('{$CONTAINER_TYPE}DocHistory.php?action=getHistory&document_id={$list[list].document_id}&page_id=DocManage_history_{$list[list].document_id}','history', 1024 , 750)" title="{tr}Get history{/tr}">
                     <img border="0" alt="{tr}Get history{/tr}" src="img/icons/history.png" />
                     </a>

    <a href="javascript:popupP('{$CONTAINER_TYPE}DocManage.php?action=ChangeState&checked[]={$list[list].document_id}&page_id=DocManage_changestate','ChangeState', 800 , 500)" title="{tr}Change state{/tr}">
    <img border="0" alt="{tr}Change state{/tr}" src="./img/icons/document/document_gear.png" /></a>

    <a href="javascript:popupP('{$CONTAINER_TYPE}DocManage.php?action=ChangeIndice&checked[]={$list[list].document_id}&page_id=DocManage_changeindice','ChangeIndice', 300 , 400)" title="{tr}Change indice{/tr}">
    <img border="0" alt="{tr}Change indice{/tr}" src="./img/icons/document/document_upindice.png" /></a>

  </td>

    <td class="thin">
    <a href="{$CONTAINER_TYPE}DocManage.php?action=suppress&checked[]={$list[list].document_id}&ticket={$ticket}" title="{tr}Suppress{/tr}"
                      onclick="return confirm('{tr}Do you want really suppress{/tr} {$list[list].document_number}')">
                     <img border="0" alt="{tr}Suppress{/tr}: {$list[list].document_number}" src="img/icons/trash.png" />
                     </a></td>

    <td class="thin"><a href="{$CONTAINER_TYPE}DocManage.php?action=viewDocument&checked[]={$list[list].document_id}&ticket={$ticket}" title="{tr}View document{/tr}">
    <img border="0" alt="no icon" src="{$icons_dir}/C/{$list[list].doctype_id}.gif" />
    {if $displayThumbs}
      <img border="0" src="{get_thumbnail imagefile_path=$thumbs_dir|cat:'/'|cat:$list[list].document_id|cat:$thumbs_extension}" />
      <!-- <img border="0" src="{$thumbs_dir}/{$list[list].document_id}{$thumbs_extension}" /> -->
    {/if}
    </a>
    </td>

    <td class="thin"><a class="link" href="javascript:popupP('{$CONTAINER_TYPE}DocManage.php?documentDetail=1&document_id={$list[list].document_id}','documentDetailWindow', 600 , 1024)">{$list[list].document_number}</a></td>
    <td class="thin">{$list[list].designation}</td>
    <td class="thin">{$list[list].document_indice_id|document_indice}<hr />v{$list[list].document_version}</td>
    <td class="thin">{$list[list].category_id|category}</td>
    <td class="thin">{$list[list].check_out_by|username}<hr />{$list[list].check_out_date|date_format}</td>
    <td class="thin">{$list[list].document_access_code|access_code}<hr />
    {$list[list].document_state}</td>
    <td class="thin">{$list[list].open_by|username}<hr />{$list[list].open_date|date_format}</td>
    <td class="thin">{$list[list].update_by|username}<hr />{$list[list].update_date|date_format}</td>
    <td class="thin">{$list[list].doctype_id|type}</td>

    <!-- Display optionnals fields -->
    {foreach key=key item=type from=$optionalFields}
      {assign var="fn" value=$type.field_name}
      <td class="thin">{filter_select id=$list[list].$fn type=$type.field_type}</td>
    {/foreach}

   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}

{include file='pagination.tpl'}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
      //-->                     
      </script>
        <br>

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<br />
<button class="mult_submit" type="submit" name="action" value="createDoc" title="{tr}Create a new document{/tr}"
 onclick="document.checkform.action='{$CONTAINER_TYPE}DocManage.php?page_id=DocManage_store'; pop_it(checkform, 340 , 550)">
<img class="icon" src="./img/icons/document/document_add.png" title="{tr}Create a new document{/tr}" alt="{tr}createDoc{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}" 
 onclick="if(confirm('{tr}Do you want really suppress this documents{/tr}')){ldelim}document.checkform.action='{$CONTAINER_TYPE}DocManage.php'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="CheckOut" title="{tr}CheckOut{/tr}"
 onclick="document.checkform.action='{$CONTAINER_TYPE}DocManage.php'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_out.png" title="{tr}CheckOut{/tr}" alt="{tr}CheckOut{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="CheckIn" title="{tr}CheckIn{/tr}" id="03"
 onclick="document.checkform.action='{$CONTAINER_TYPE}DocManage.php'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_in.png" title="{tr}CheckIn{/tr}" alt="{tr}CheckIn{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="Update" title="{tr}Update{/tr}" id="04"
 onclick="document.checkform.action='{$CONTAINER_TYPE}DocManage.php'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_refresh.png" title="{tr}Update{/tr}" alt="{tr}Update{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="Reset" title="{tr}Reset{/tr}" id="05"
 onclick="document.checkform.action='{$CONTAINER_TYPE}DocManage.php'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_cancel.png" title="{tr}Reset{/tr}" alt="{tr}Reset{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="putInWs" title="{tr}Put in wildspace{/tr}" id="06"
 onclick="document.checkform.action='{$CONTAINER_TYPE}DocManage.php'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_read.png" title="{tr}Put in wildspace{/tr}" alt="{tr}Put in wildspace{/tr}" width="16" height="16" />
</button>

<!--
<button class="mult_submit" type="submit" name="action" value="assocFile" title="{tr}Associate a file{/tr}"
 onclick="document.checkform.action='{$CONTAINER_TYPE}DocManage.php'; pop_no(checkform)">
<img class="icon" src="./img/icons/file/file_attach.png" title="{tr}Associate a file{/tr}" alt="{tr}assocFile{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="assocDocument" title="{tr}Reference to document{/tr}"
 onclick="document.checkform.action='{$CONTAINER_TYPE}DocManage.php'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_attach.png" title="{tr}Reference to document{/tr}" alt="{tr}assocDocument{/tr}" width="16" height="16" />
</button>

-->

<button class="mult_submit" type="submit" name="action" value="MoveDocument" title="{tr}Move{/tr}" id="09"
 onclick="document.checkform.action='{$CONTAINER_TYPE}DocManage.php?page_id=DocManage_movedoc'; pop_it(checkform, 350 , 280)">
<img class="icon" src="./img/icons/document/document_move.png" title="{tr}Move{/tr}" alt="{tr}Move{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="CopyDocument" title="{tr}Copy{/tr}" id="10"
 onclick="document.checkform.action='{$CONTAINER_TYPE}DocManage.php?page_id=DocManage_copydoc'; pop_it(checkform , 350 , 280)">
<img class="icon" src="./img/icons/document/document_copy.png" title="{tr}Copy{/tr}" alt="{tr}Copy{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="multi_ChangeIndice" title="{tr}Change indice{/tr}" id="11"
 onclick="if(confirm('{tr}Are you sure?{/tr}')){ldelim}document.checkform.action='{$CONTAINER_TYPE}DocManage.php?page_id=DocManage_changeindice'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/document/document_upindice.png" title="{tr}Change indice{/tr}" alt="{tr}Change indice{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="Lock" title="{tr}Lock definitivly the document{/tr}"
 onclick="if(confirm('{tr}The lock is definitif. Are you sure?{/tr}')){ldelim}document.checkform.action='{$CONTAINER_TYPE}DocManage.php'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/lock.png" title="{tr}Lock definitivly the document{/tr}" alt="{tr}Lock{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="unLock" title="{tr}Unlock the document{/tr}"
 onclick="if(confirm('{tr}Are you sure?{/tr}')){ldelim}document.checkform.action='{$CONTAINER_TYPE}DocManage.php'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/lock_open.png" title="{tr}Unlock the document{/tr}" alt="{tr}Unlock{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="reset_doctype" title="{tr}Recalculate the doctype{/tr}" id="07"
 onclick="document.checkform.action='{$CONTAINER_TYPE}DocManage.php'; pop_no(checkform)">
<img class="icon" src="./img/icons/doctype/doctype_reset.png" title="{tr}Recalculate the doctype{/tr}" alt="{tr}Recalculate the doctype{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="downloadListing" title="{tr}Download listing{/tr}"
 onclick="document.checkform.action='{$CONTAINER_TYPE}DocManage.php?page_id=DocManage_downloadcsv'; pop_no(checkform)">
<img class="icon" src="./img/icons/page_white_excel.png" title="{tr}Download listing{/tr}" alt="{tr}Download listing{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
 onclick="document.checkform.action='{$CONTAINER_TYPE}DocManage.php'; pop_no(checkform)">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_field|escape}" />
<input type="hidden" name="sort_order" value="{$sort_order|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="ticket" value="{$ticket}" />
</form>
