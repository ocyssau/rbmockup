{*Smarty template*}

{*--------------------Search Bar defintion--------------------------*}
<div id="sitesearchbar_document">
<form id="filterf" action="{$smarty.server.PHP_SELF}" method="post">

{sameurlpost}
{if $histoOption}
  <label><input type="checkbox" name="displayHistory" value="1" {if $displayHistory}"checked"{/if} onChange='javascript:getElementById("filterf").submit()';/>
  <small>{tr}Display history{/tr}</small></label>
{/if}

<br />
  <label>
  <input size="4" type="text" name="numrows" value="{$numrows|escape}" />
  <small>{tr}rows to display{/tr}</small></label>
<br />

  <label for="find"><small>{tr}find{/tr}</small></label>
  <input size="16" type="text" name="find" value="{$find|escape}" />

  <label for="find_field"><small>{tr}In{/tr}</small></label>
	<select name="find_field" onchange='javascript:getElementById("filterf").submit();'>
	<option {if '' eq $find_field}selected="selected"{/if} value=""></option>
	{foreach from=$all_field item=name key=field}
   <option {if $field eq $find_field}selected="selected"{/if} value="{$field|escape}">{$name}</option>
	{/foreach}
	</select>

  <label for="f_action_field"><small>{tr}Action{/tr}</small></label>
	<select name="f_action_field">
	<option {if $f_action_field eq ''}selected="selected"{/if} value=""></option>
	{foreach from=$user_elements item=name key=field}
   <option value="{$name}" {if $f_action_field eq $name}selected="selected"{/if}>{$name}</option>
	{/foreach}
	</select>

  <label for="f_action_user_name"><small>{tr}By{/tr}</small></label>
	<select name="f_action_user_name">
	<option {if $f_action_user_name eq ''}selected="selected"{/if} value=""></option>
	{foreach from=$user_list item=name}
   <option value="{$name.auth_user_id}" {if $f_action_user_name eq $name.auth_user_id}selected="selected"{/if}>{$name.handle}</option>
	{/foreach}
	</select>

  <input type="submit" name="filter" value="{tr}filter{/tr}" />

<br />

{literal}
<script language="JavaScript">
function displayOption(){

      var a,b;

      var a = document.getElementById("f_adv_search_cb");
      var b = document.getElementById("f_adv_search_span");
      if(a.checked==true)
          b.style.display = "block";
      else b.style.display = "none";

    if(document.getElementById("f_check_out_date_sel")){
      var a = document.getElementById("f_check_out_date_sel");
      var b = document.getElementById("f_check_out_date_span");
      if(a.checked==true)
          b.style.display = "block";
      else b.style.display = "none";
    }

    if(document.getElementById("f_update_date_sel")){
      var a = document.getElementById("f_update_date_sel");
      var b = document.getElementById("f_update_date_span");
      if(a.checked==true)
          b.style.display = "block";
      else b.style.display = "none";
    }

    if(document.getElementById("f_open_date_sel")){
      var a = document.getElementById("f_open_date_sel");
      var b = document.getElementById("f_open_date_span");
      if(a.checked==true)
          b.style.display = "block";
      else b.style.display = "none";
    }

    if(document.getElementById("f_close_date_sel")){
      var a = document.getElementById("f_close_date_sel");
      var b = document.getElementById("f_close_date_span");
      if(a.checked==true)
          b.style.display = "block";
      else b.style.display = "none";
    }

    if(document.getElementById("f_fsclose_date_sel")){
      var a = document.getElementById("f_fsclose_date_sel");
      var b = document.getElementById("f_fsclose_date_span");
      if(a.checked==true)
          b.style.display = "block";
      else b.style.display = "none";
    }
}
</script>
{/literal}

<label><small>{tr}Advanced search{/tr}</small>
<input type="checkbox" name="f_adv_search_cb" value="1" id="f_adv_search_cb" onClick="displayOption();" />
</label>
<span id=f_adv_search_span style="display: none">
<fieldset>


<link href="./lib/jscalendar/calendar-win2k-1.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./lib/jscalendar/calendar.js"></script>
<script type="text/javascript" src="./lib/jscalendar/lang/calendar-fr.js"></script>
<script type="text/javascript" src="./lib/jscalendar/calendar-setup.js"></script>

{if in_array('check_out_date',$date_elements)}
  <label><small>{tr}CheckOut date{/tr}</small>
  <input type="checkbox" name="f_check_out_date_sel" value="1" id="f_check_out_date_sel" onClick="displayOption();" />
  </label>

  <span id=f_check_out_date_span style="display: none">
  <fieldset>
  <input type="hidden" name="f_check_out_date" value="" id="f_check_out_date"/>

  <label><input type="radio" name="f_check_out_date_cond" "checked" value=">"/>
  <small>{tr}Superior to{/tr}</small></label>

  <label><input type="radio" name="f_check_out_date_cond" value="<" />
  <small>{tr}Inferior to{/tr}</small></label>
  <span style="background-color: rgb(255, 255, 136);" id="show_check_out_date">Click</span>
  </fieldset>
  </span>

  {literal}
  <script type="text/javascript">
    Calendar.setup({
        inputField     :    "f_check_out_date",     // id of the input field
        displayArea    :    "show_check_out_date",       // ID of the span where the date is to be shown
        ifFormat       :    "%s",     // format of the input field (even if hidden, this format will be honored)
        daFormat       :    "%Y/%m/%d %H:%M:%S",// format of the displayed date
        showsTime      :    true, // display hours 
        singleClick    :    false
    });
  </script>
  {/literal}
<br />
{/if}

{if in_array('update_date',$date_elements)}
  <label><small>{tr}Update date{/tr}</small>
  <input type="checkbox" name="f_update_date_sel" value="1" id="f_update_date_sel" onClick="displayOption();" />
  </label>

  <span id=f_update_date_span style="display: none">
  <fieldset>
  <input type="hidden" name="f_update_date" value="" id="f_update_date"/>

  <label><input type="radio" name="f_update_date_cond" "checked" value=">" />
  <small>{tr}Superior to{/tr}</small></label>

  <label><input type="radio" name="f_update_date_cond" value="<" />
  <small>{tr}Inferior to{/tr}</small></label>

  <span style="background-color: rgb(255, 255, 136);" id="show_update_date">Click</span>
  </fieldset>
  </span>

  {literal}
  <script type="text/javascript">
    Calendar.setup({
      inputField     :    "f_update_date",     // id of the input field
      displayArea    :    "show_update_date",       // ID of the span where the date is to be shown
      ifFormat       :    "%s",     // format of the input field (even if hidden, this format will be honored)
      daFormat       :    "%Y/%m/%d %H:%M:%S",// format of the displayed date
      showsTime      :    true, // display hours 
      singleClick    :    false
  });
  </script>
  {/literal}
<br />
{/if}

{if in_array('open_date',$date_elements)}
  <label><small>{tr}Create date{/tr}</small>
  <input type="checkbox" name="f_open_date_sel" value="1" id="f_open_date_sel" onClick="displayOption();" />
  </label>

  <span id=f_open_date_span style="display: none">
  <fieldset>
  <input type="hidden" name="f_open_date" value="" id="f_open_date"/>

  <label><input type="radio" name="f_open_date_cond" "checked" value=">" />
  <small>{tr}Superior to{/tr}</small></label>

  <label><input type="radio" name="f_open_date_cond" value="<" />
  <small>{tr}Inferior to{/tr}</small></label>

  <span style="background-color: rgb(255, 255, 136);" id="show_open_date">Click</span>
  </fieldset>
  </span>

  {literal}
  <script type="text/javascript">
    Calendar.setup({
        inputField     :    "f_open_date",     // id of the input field
        displayArea    :    "show_open_date",       // ID of the span where the date is to be shown
        ifFormat       :    "%s",     // format of the input field (even if hidden, this format will be honored)
        daFormat       :    "%Y/%m/%d %H:%M:%S",// format of the displayed date
        showsTime      :    true, // display hours
        singleClick    :    false
    });
  </script>
  {/literal}
{/if}

{if in_array('close_date',$date_elements)}
<br />
  <label><small>{tr}Close date{/tr}</small>
  <input type="checkbox" name="f_close_date_sel" value="1" id="f_close_date_sel" onClick="displayOption();" />
  </label>

  <span id=f_close_date_span style="display: none">
  <fieldset>
  <input type="hidden" name="f_close_date" value="" id="f_close_date"/>

  <label><input type="radio" name="f_close_date_cond" "checked" value=">" />
  <small>{tr}Superior to{/tr}</small></label>

  <label><input type="radio" name="f_close_date_cond" value="<" />
  <small>{tr}Inferior to{/tr}</small></label>

  <span style="background-color: rgb(255, 255, 136);" id="show_close_date">Click</span>
  </fieldset>
  </span>

  {literal}
  <script type="text/javascript">
    Calendar.setup({
        inputField     :    "f_close_date",     // id of the input field
        displayArea    :    "show_close_date",       // ID of the span where the date is to be shown
        ifFormat       :    "%s",     // format of the input field (even if hidden, this format will be honored)
        daFormat       :    "%Y/%m/%d %H:%M:%S",// format of the displayed date
        showsTime      :    true, // display hours 
        singleClick    :    false
    });
  </script>
  {/literal}

{/if}

{if in_array('forseen_close_date',$date_elements)}
<br />
  <label><small>{tr}Forseen close date{/tr}</small>
  <input type="checkbox" name="f_fsclose_date_sel" value="1" id="f_fsclose_date_sel" onClick="displayOption();" />
  </label>

  <span id=f_fsclose_date_span style="display: none">
  <fieldset>
  <input type="hidden" name="f_fsclose_date" value="" id="f_fsclose_date"/>

  <label><input type="radio" name="f_fsclose_date_cond" "checked" value=">" />
  <small>{tr}Superior to{/tr}</small></label>

  <label><input type="radio" name="f_fsclose_date_cond" value="<" />
  <small>{tr}Inferior to{/tr}</small></label>

  <span style="background-color: rgb(255, 255, 136);" id="show_fsclose_date">Click</span>
  </fieldset>
  </span>

  {literal}
  <script type="text/javascript">
    Calendar.setup({
        inputField     :    "f_fsclose_date",     // id of the input field
        displayArea    :    "show_fsclose_date",       // ID of the span where the date is to be shown
        ifFormat       :    "%s",     // format of the input field (even if hidden, this format will be honored)
        daFormat       :    "%Y/%m/%d %H:%M:%S",// format of the displayed date
        showsTime      :    true, // display hours 
        singleClick    :    false
    });
  </script>
  {/literal}

{/if}


<br />

</fieldset>

</span>

</form>

<form id="resetf" action="{$smarty.server.PHP_SELF}" method="post">
  {sameurlpost}
  <input type="hidden" name="offset" value="0" />
  <input type="hidden" name="sort_field" value="" />
  <input type="hidden" name="sort_order" value="" />
  <input type="hidden" name="where" value="" />
  <input type="hidden" name="find" value="" />
  <input type="hidden" name="find_field" value="" />
  <input type="hidden" name="numrows" value="" />
  <input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" />
</form>
</div>
