{*Smarty template*}

{include file="header.tpl"}
<div id="tiki-center">

<form {$form.attributes}>
{$form.hidden}

{if $action == 'modify'}
  <h2>{tr}Modify a partner{/tr}</h2>
{else}
  <h2>{tr}Create a partner{/tr}</h2>
{/if}

<table class="normal">
  <tr>
    <td class="formcolor">{tr}{$form.partner_number.label}{/tr}:</td>
    <td class="formcolor">{$form.partner_number.html}
      <br /><i>{$number_help}</i>
    </td>
  </tr>

  <tr>
    <td class="formcolor">{tr}{$form.partner_type.label}{/tr}:</td>
    <td class="formcolor">{$form.partner_type.html}</td>
  </tr>



  <tr>
    <td class="formcolor">{tr}{$form.first_name.label}{/tr}:</td>
    <td class="formcolor">{$form.first_name.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{tr}{$form.last_name.label}{/tr}:</td>
    <td class="formcolor">{$form.last_name.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{tr}{$form.adress.label}{/tr}:</td>
    <td class="formcolor">{$form.adress.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{tr}{$form.city.label}{/tr}:</td>
    <td class="formcolor">{$form.city.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{tr}{$form.zip_code.label}{/tr}:</td>
    <td class="formcolor">{$form.zip_code.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{tr}{$form.phone.label}{/tr}:</td>
    <td class="formcolor">{$form.phone.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{tr}{$form.cell_phone.label}{/tr}:</td>
    <td class="formcolor">{$form.cell_phone.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{tr}{$form.mail.label}{/tr}:</td>
    <td class="formcolor">{$form.mail.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{tr}{$form.web_site.label}{/tr}:</td>
    <td class="formcolor">{$form.web_site.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{tr}{$form.activity.label}{/tr}:</td>
    <td class="formcolor">{$form.activity.html}</td>
  </tr>
  <tr>
    <td class="formcolor">{tr}{$form.company.label}{/tr}:</td>
    <td class="formcolor">{$form.company.html}</td>
  </tr>

 {if not $form.frozen}
  <tr>
    <td class="formcolor"> </td>
    <td class="formcolor">{$form.reset.html}&nbsp;{$form.submit.html}</td>
  </tr>
 {/if}

  <tr>
       <td class="formcolor">{tr}{$form.requirednote}{/tr}</td>
  </tr>

</table>
</form>

<form name="close">
  <input type="button" onclick="window.close()" value="Close">
</form>

<br />

{if $form.errors}
<b>Collected Errors:</b><br />
{foreach key=name item=error from=$form.errors}
  <font color="red">{$error}</font> in element [{$name}]<br />
{/foreach}
<br />
{/if}

<table class="normal">
<form action="./partnerManage.php" method="post" name="import" enctype="multipart/form-data">
  <tr>
   <td>
    {tr}Import csv{/tr} (CSV file<a {popup text='first_name;last_name;adress;city;zip_code;...<br />andre;malraux;panth�on;PARIS;75000;...'}><img src="img/icons/help.png" border="0" height="16" width="16" alt='{tr}help{/tr}' /></a>):
   </td>
   <td>
    <input type="file" name="csvlist"/>
    <input type="submit" value="csvimport" name="action" />
    <br />{tr}Overwrite{/tr}: <input type="checkbox" name="overwrite" checked="checked" />
   </td>
  </tr>
</table>
</form>

</div>
</body>
