{*Smarty template*}

<div id="page-bar">
   <span class="button2"><a class="linkbut" href="javascript:popupP('partnerManage.php?action=create','partnerManage', 560 , 540)">{tr}Create{/tr}</a></span>
</div>

<br />

<h1 class="pagetitle">{tr}Partners manager{/tr}</h1>

{*--------------------Search Bar defintion--------------------------*}
{include file='searchBar_simple.tpl'}

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
<tr>
<th class="heading auto"></th>
<th class="heading auto"></th>
<th class="heading auto"></th>

<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=partner_type">
{tr}partner_type{/tr}</a></th>
<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=partner_number">
{tr}partner_number{/tr}</a></th>
<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=first_name">
{tr}first_name{/tr}</a></th>
<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=last_name">
{tr}last_name{/tr}</a></th>
<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=adress">
{tr}adress{/tr}</a></th>
<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=city">
{tr}city{/tr}</a></th>
<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=zip_code">
{tr}zip_code{/tr}</a></th>
<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=phone">
{tr}phone{/tr}</a></th>
<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=cell_phone">
{tr}cell_phone{/tr}</a></th>
<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=mail">
{tr}mail{/tr}</a></th>
<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=web_site">
{tr}web_site{/tr}</a></th>
<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=activity">
{tr}activity{/tr}</a></th>
<th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=company">
{tr}company{/tr}</a></th>
</tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
{section name=list loop=$list}
 <tr class="{cycle}">
  <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].partner_id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

  <td class="thin"><a href="javascript:popup('partnerManage.php?action=modify&partner_id={$list[list].partner_id}','partnerManage')" title="{tr}edit{/tr}">
                   <img border="0" alt="{tr}edit{/tr}" src="img/icons/edit.png" />
                   </a>
  </td>
  <td class="thin"><a href="partners.php?action=suppress&checked[]={$list[list].partner_id}" title="{tr}Suppress{/tr}"
                    onclick="return confirm('{tr}Do you want really suppress{/tr} {$list[list].partner_number}')">
                   <img border="0" alt="{tr}Suppress{/tr}" src="img/icons/trash.png" />
                   </a>
  </td>

  <td class="thin">{$list[list].partner_type}</td>
  <td class="thin">{$list[list].partner_number}</td>
  <td class="thin">{$list[list].first_name}</td>
  <td class="thin">{$list[list].last_name}</td>
  <td class="thin">{$list[list].adress}</td>
  <td class="thin">{$list[list].city}</td>
  <td class="thin">{$list[list].zip_code}</td>
  <td class="thin">{$list[list].phone}</td>
  <td class="thin">{$list[list].cell_phone}</td>
  <td class="thin"><a href="mailto:{$list[list].mail}">{$list[list].mail}</a></td>
  <td class="thin"><a href="http://{$list[list].web_site}">{$list[list].web_site}</a></td>
  <!--<td class="thin"><a href="ftp://{$list[list].ftp_site}">{$list[list].ftp_site}</a></td>-->
  <td class="thin">{$list[list].activity}</td>
  <td class="thin">{$list[list].company}</td>
 </tr>
{/section}
</table>

{* -------------------Pagination------------------------ *}

{include file='pagination.tpl'}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
      //-->                     
      </script>

<br />
{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}" id="01"
onclick="if(confirm('{tr}Do you want really suppress this partner{/tr}')){ldelim}document.checkform.action='{$smarty.server.PHP_SELF}'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="export" title="{tr}Export{/tr}"
 onclick="document.checkform.action='partners.php'; pop_no(checkform)">
<img class="icon" src="./img/icons/page_white_excel.png" title="{tr}Export{/tr}" alt="{tr}Export{/tr}" width="16" height="16" />
</button>

{*Submit checkform form*}

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
</form>
