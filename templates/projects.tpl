{*Smarty template*}

<div id="page-bar">
   <span class="button2"><a class="linkbut" href="javascript:popup('projectManage.php?action=create','projectCreate')">{tr}Create{/tr}</a></span>
   <span class="button2"><a class="linkbut" href="javascript:popup('projectHistory.php','projetHistory')">{tr}History{/tr}</a></span>
</div>

<br>

<h1 class="pagetitle">{tr}Project manager{/tr}</h1>

{*--------------------Search Bar defintion--------------------------*}
{include file='searchBar.tpl'}

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
<tr>
  <th class="heading auto"></th>
  <th class="heading auto"></th>
  <th class="heading auto"></th>
  <th class="heading auto"></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=project_number">{tr}Number{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=project_description">{tr}Description{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=project_state">{tr}State{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=open_date">{tr}Creation date{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=open_by">{tr}Created by{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=forseen_close_date">{tr}Forseen close date{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=close_date">{tr}Close date{/tr}</a></th>
</tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=projects loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[projects].project_id}" {if $projects[projects].checked eq 'y'}checked="checked" {/if}/></td>

    <td class="thin">
     <a href="javascript:popup('projectManage.php?action=modify&project_id={$list[projects].project_id}','projectManage')" title="{tr}edit project{/tr}">
       <img border="0" alt="{tr}edit project{/tr}" src="img/icons/edit.png" />
     </a>
    </td>

    <td class="thin"><a href="javascript:popup('projectHistory.php?action=getHistory&container_id={$list[projects].project_id}','history')" title="{tr}Get history{/tr}">
                     <img border="0" alt="{tr}Get history{/tr}" src="img/icons/history.png" />
                     </a>
    </td>

    <td class="thin">
     <a href="projects.php?action=suppress&checked[]={$list[projects].project_id}" title="{tr}Suppress project{/tr}"
     onclick="return confirm('{tr}Do you want really suppress{/tr} {$list[projects].project_number}')">
       <img border="0" alt="{tr}Suppress project{/tr}" src="img/icons/trash.png" />
     </a>
    </td>
    <td class="thin"><a class="link" href="projectExplore.php?project_id={$list[projects].project_id}" title="{tr}Number{/tr}: {$list[projects].project_number}">{$list[projects].project_number}</a></td>
    <td class="thin" class="link" >{$list[projects].project_description}</td>
    <td class="thin" class="link" >{$list[projects].project_state}</td>
    <td class="thin" class="link" >{$list[projects].open_date|date_format}</td>
    <td class="thin" class="link" >{$list[projects].open_by|username}</td>
    <td class="thin" class="link" >{$list[projects].forseen_close_date|date_format}</td>
    <td class="thin" class="link" >{$list[projects].close_date|date_format}</td>
   </tr>
  {sectionelse}
    <tr><td colspan="6">{tr}No projects to display{/tr}</td></tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}

{include file='pagination.tpl'}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
      //-->                     
      </script>
<br>

{*Multiselection select action form *}

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}" id="01"
onclick="if(confirm('{tr}Do you want really suppress this projects{/tr}')){ldelim}document.checkform.action='{$smarty.server.PHP_SELF}'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="ChangeState" title="{tr}Change state{/tr}" id="08"
 onclick="document.checkform.action='none.php'; pop_it(checkform)">
<img class="icon" src="./img/icons/state/state.png" title="{tr}Change state{/tr}" alt="{tr}Change state{/tr}" width="24" height="16" />
</button>

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
</form>
