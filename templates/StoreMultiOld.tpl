{*Smarty template*}
{if $displayHeader eq 1}
  {include file="header.tpl"}
  
  {$form.javascript}
  
  <form {$form.attributes}>
  
  <h2>{tr}{$form.header.infos}{/tr}</h2>
  {$form.hidden}
  
  <table class="normal">
    <tr>
      <th></th>
      <th>{tr}{$form.document_number[$loop_id].label}{/tr}:<hr />
      {tr}{$form.file[$loop_id].label}{/tr}:</th>
      <th>{tr}{$form.designation[$loop_id].label}{/tr}:</th>
      <th>{tr}{$form.document_indice_id[$loop_id].label}{/tr}:</th>
      <th>{tr}{$form.category_id[$loop_id].label}{/tr}:</th>
      {section name=of loop=$optionalFields}
        {assign var="fn" value=$optionalFields[of].field_name}
        <th>{tr}{$form.$fn[$loop_id].label}{/tr}:</th>
      {/section}
      <th>{tr}{$form.lock[$loop_id].label}{/tr}:</th>
    </tr>

{/if}

{if $displayFooter eq 0}
  <input name="isStored[{$loop_id}]" type="hidden" value="{$isStored}" />
  <tr>
    <td>{if $isStored eq 1}<b>Success</b>{/if}</td>
    <td>{$form.document_number[$loop_id].html} <br />
        {$form.file[$loop_id].html}</td>
    <td>{$form.designation[$loop_id].html}</td>
    <td>{$form.document_indice_id[$loop_id].html}</td>
    <td>{$form.category_id[$loop_id].html}</td>
    <!-- Display optionnals fields -->
    <!-- TODO : revoir boucle imbriqu�e -->
    {section name=of loop=$optionalFields}
      {assign var="fn" value=$optionalFields[of].field_name}
      <td>{$form.$fn[$loop_id].html}</td>
    {/section}
    <td>{$form.lock[$loop_id].html}</td>
  </tr>

{/if}

{if $displayFooter eq 1}

  </table>

 {if not $allCompleted}
  {$form.reset.html}&nbsp;{$form.action.html}
 {/if}

 <br />{tr}{$form.requirednote}{/tr}

  <input type="hidden" name="page_id" value="DocManage_store" />

  </form>

  <br />

  <form name="close">
  <input type="button" onclick="window.close()" value="Close">
  </form>

  <br />

{foreach key=name item=error from=$form.errors}
 <b>Collected Errors:</b> <br />
 <font color="red">{$error}</font> in element [{$name}]<br />
{/foreach}

{/if}
