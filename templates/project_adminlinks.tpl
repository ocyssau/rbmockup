{*Smarty template*}

<div id="BoiteActions">
   <span class="button2"><a class="linkbut" href="javascript:popup('projectExplore.php?project_id={$project_id}&action=linkWorkitem' , 'linkWorkitem')">{tr}Link a workitem{/tr}</a></span>
   <span class="button2"><a class="linkbut" href="javascript:popup('projectExplore.php?project_id={$project_id}&action=linkMockup' , 'linkMockup')">{tr}Link a mock-Up{/tr}</a></span>
   <span class="button2"><a class="linkbut" href="javascript:popup('projectExplore.php?project_id={$project_id}&action=linkPartner' , 'linkPartner')">{tr}Link a partner{/tr}</a></span>
   <span class="button2"><a class="linkbut" href="javascript:popup('projectExplore.php?project_id={$project_id}&action=linkLib' , 'linkLib')">{tr}Link a librairy{/tr}</a></span>
   <span class="button2"><a class="linkbut" href="javascript:popup('projectExplore.php?project_id={$project_id}&action=linkBib' , 'linkBookshop')">{tr}Link a bookshop{/tr}</a></span>
   <span class="button2"><a class="linkbut" href="javascript:popup('projectLinkManage.php?container_id={$project_id}&action=getDoctypeLinks' , 'linkDoctype')">{tr}Link a doctype{/tr}</a></span>
   <span class="button2"><a class="linkbut" href="javascript:popup('projectExplore.php?project_id={$project_id}&action=linkDefaultProcess' , 'linkDefaultProcess')">{tr}Link a default process{/tr}</a></span>
</div>

<h1 class="pagetitle">{tr}Links objects list{/tr}</h1>

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">

<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading">{tr}{$HeaderCol1}{/tr}</th>
  <th class="heading">{tr}{$HeaderCol2}{/tr}</th>
  <th class="heading">{tr}{$HeaderCol3}{/tr}</th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[{$list[list].$col1}][]" value="{$list[list].$id}" /></td>
    <td class="thin">{$list[list].$col1}</td>
    <td class="thin">{$list[list].$col2}</td>
    <td class="thin">{$list[list].$col3}</td>
   </tr>
  {/section}
  </table>

<input type=hidden name="project_id" value="{$project_id}">

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />

<button class="mult_submit" type="submit" name="action" value="suppressLink" title="{tr}Suppress link{/tr}" id="01">
<img class="icon" src="./img/icons/link_break.png" title="{tr}Suppress link{/tr}" alt="{tr}Suppress link{/tr}" width="16" height="16" />
</button>

</form>
