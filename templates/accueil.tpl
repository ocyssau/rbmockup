{*Smarty template*}

<div id="page-bar">
   <span class="button2"><a class="linkbut" href="messu-mailbox.php">{tr}Messages{/tr}</a></span>
</div>

<table border=0>
<tr>
  <td><img src="{$logo}"></td>
  
  <td><h1 class="pagetitle">{tr}Welcome{/tr} {$current_user_name}, <br />
  {tr}You are connected on{/tr} <i>RANCHBE</i>&#169; {$ranchbe_version} {$ranchbe_build}
  </h1>
  <h2> ...{tr}The design office ranch{/tr}</h2> 
</td></tr>
</table>

<p></p>

  <div class="rbbox">
  <h2>{tr}New messages{/tr}</h2>
  <table class="normal">
    <tr>
      <td class="heading" >&nbsp;</td>
      <td class="heading" ><a class="tableheading" href="messu-mailbox.php?flag={$flag}&amp;priority={$priority}&amp;flagval={$flagval}&amp;find={$find}&amp;offset={$offset}&amp;sort_mode={if $sort_mode eq 'user_from_desc'}user_from_asc{else}user_from_desc{/if}">{tr}sender{/tr}</a></td>
      <td class="heading" ><a class="tableheading" href="messu-mailbox.php?flag={$flag}&amp;priority={$priority}&amp;flagval={$flagval}&amp;find={$find}&amp;offset={$offset}&amp;sort_mode={if $sort_mode eq 'subject_desc'}subject_asc{else}subject_desc{/if}">{tr}subject{/tr}</a></td>
      <td class="heading" ><a class="tableheading" href="messu-mailbox.php?flag={$flag}&amp;priority={$priority}&amp;flagval={$flagval}&amp;find={$find}&amp;offset={$offset}&amp;sort_mode={if $sort_mode eq 'date_desc'}date_asc{else}date_desc{/if}">{tr}date{/tr}</a></td>
      <td class="heading" ><a class="tableheading" href="messu-mailbox.php?flag={$flag}&amp;priority={$priority}&amp;flagval={$flagval}&amp;find={$find}&amp;offset={$offset}&amp;sort_mode={if $sort_mode eq 'size_desc'}size_asc{else}size_desc{/if}">{tr}size{/tr}</a></td>
    {cycle values="odd,even" print=false}
    {section name=user loop=$items}
    <tr>
      <td class="prio{$items[user].priority}">{if $items[user].isFlagged eq 'y'}<img src="img/icons/flagged.png" border="0" width="16" height="16" alt='{tr}flagged{/tr}' />{/if}</td>
      <td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}">{$items[user].user_from}</td>
      <td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}"><a class="readlink" href="messu-read.php?offset={$offset}&amp;flag={$flag}&amp;priority={$items[user].priority}&amp;flagval={$flagval}&amp;sort_mode={$sort_mode}&amp;find={$find}&amp;msgId={$items[user].msgId}">{$items[user].subject}</a></td>
      <td {if $items[user].isRead eq 'n'}style="font-weight:bold"{/if} class="prio{$items[user].priority}">{$items[user].date|date_format}</td>
      <td  style="text-align:right;{if $items[user].isRead eq 'n'}font-weight:bold;{/if}" class="prio{$items[user].priority}">{$items[user].len}</td>
    </tr>
    {sectionelse}
    <tr><td colspan="6">{tr}No messages to display{/tr}</td></tr>
    {/section}
  </table>
  </div>
