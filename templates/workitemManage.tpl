{*Smarty template*}

{include file="header.tpl"}
<div id="tiki-center">

<form {$form.attributes}>
{$form.hidden}

{if $action == 'modify'}
  <h2>{tr}Modify a workitem{/tr}</h2>
{else}
  <h2>{tr}Create a workitem{/tr}</h2>
{/if}

<table class="normal">

  <tr>
    <td class="formcolor">{tr}{$form.workitem_number.label}{/tr}:</td>
    <td class="formcolor">
      {$form.workitem_number.html}
      <br /><i>{$number_help}</i>
    </td>
  </tr>
  <tr>
    <td class="formcolor">{tr}{$form.workitem_description.label}{/tr}:</td>
    <td class="formcolor">{$form.workitem_description.html}</td>
  </tr>

  <tr>
    <td class="formcolor">{tr}{$form.default_process_id.label}{/tr}:</td>
    <td class="formcolor">{$form.default_process_id.html}</td>
  </tr>

  <tr>
    <td class="formcolor">{tr}{$form.project_id.label}{/tr}:</td>
    <td class="formcolor">{$form.project_id.html}</td>
  </tr>


  <tr>
    <td class="formcolor">{tr}{$form.forseen_close_date.label}{/tr}:</td>
    <td class="formcolor">{$form.forseen_close_date.html}</td>
  </tr>

  <!-- Display optionnals fields -->
  <!-- TODO : revoir boucle imbriqu�e -->
  {section name=of loop=$optionalFields}
  {assign var="fn" value=$optionalFields[of].field_name}
  <tr class="formcolor">
    <td>{tr}{$form.$fn.label}{/tr}:</td>
    <td>{$form.$fn.html}</td>
  </tr>
  {/section}

 {if not $form.frozen}
  <tr>
    <td class="formcolor"> </td>
    <td class="formcolor">{$form.reset.html}&nbsp;{$form.submit.html}</td>
  </tr>
 {/if}

  <tr>
    <td class="formcolor">{tr}{$form.requirednote}{/tr}</td>
  </tr>

</table>
</form>

<form name="close">
  <input type="button" onclick="window.close()" value="Close">
</form>

<br />

{if $form.errors}
<b>Collected Errors:</b><br />
{foreach key=name item=error from=$form.errors}
  <font color="red">{$error}</font> in element [{$name}]<br />
{/foreach}
<br />
{/if}

</div>
</body>
