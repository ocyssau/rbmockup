{*Smarty template*}

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>ranchBE</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <link href="styles/jalist.css" rel="stylesheet" type="text/css">
    <link href="styles/PhpLayerMenu.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="img/vache_30.png" />
    <script type="text/javascript" src="lib/tiki-js.js"></script>
    <script type="text/javascript" src="lib/lib.js"></script>
    <noscript>
    BE CAREFUL : You must enable javascript for use RanchBE and enable pop-up.
    </noscript>

</head>
<body>

{popup_init src="lib/overlib.js"}

<div id="tiki-main">

  <div id="tiki-top">
    <img src="./img/docsier/docsier.jpg" /><br />
    {tr}This is{/tr} RanchBE application ver:{$ranchbe_version}  &#169; 2007 For SIER
    | {$smarty.now|date_format:"%A %d %B %Y"}
  </div>

  <div id="tiki-mid">
    <div id="tiki-center">



    <h1 class="pagetitle">RECHERCHE DANS LA DOCUMENTATION FOURNISSEUR</h1>

    <form method="post" name="form" action="{$smarty.server.PHP_SELF}?#result">
    {$form.hidden}

    <fieldset>
    <legend align="top"><i>Recherche rapide</i></legend>
      <label>{$form.quick_search.label}:</label>
      {$form.quick_search.html}
    </fieldset>

    

    <fieldset>
    <legend align="top"><i>Recherche d�taill�e</i></legend>
      <label>{$form.docsier_keyword.label}:</label>
      {$form.docsier_keyword.html}
      <br />
      <label>{$form.docsier_fournisseur.label}:</label>
      {$form.docsier_fournisseur.html}
      <br />
      <label>{$form.docsier_famille.label}:</label>
      {$form.docsier_famille.html}
    </fieldset>

    {$form.reset.html}&nbsp;{$form.submit.html}

    </form>

    <hr />
  

    <a name="result" ></a>
    <h1 class="pagetitle">RESULTATS</h1>
    {*--------------------Display messages------------------------------*}
    <b><font color="red">{$search_return}</font></b><br />

    {*--------------------Display result--------------------------------*}
    
    {*
    <form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
    {section name=list loop=$list}
      {foreach item=field from=$select}
        {tr}{$field}{/tr} :
        {filter_select id=$list[list].$field type=$type.field_type field_name=$field}<br />
      {/foreach}
      <hr />
    {/section}
    *}

    <form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
    {section name=list loop=$list}
      ID : 
      {filter_select id=$list[list].document_number type=$type.field_type field_name=document_number}<br />
      Annee : 
      {filter_select id=$list[list].docsier_date type=$type.field_type field_name=docsier_date}<br />
      Famille : 
      {filter_select id=$list[list].docsier_famille type=$type.field_type field_name=docsier_famille}<br />
      Description : 
      {filter_select id=$list[list].designation type=$type.field_type field_name=designation}<br />
      Mots cl�s :
      {filter_select id=$list[list].docsier_keyword type=$type.field_type field_name=docsier_keyword}<br />
      <hr />
    {/section}
    </form>

    </div>
  </div>
  
  <div id="tiki-bot">
  </div>

</div>
