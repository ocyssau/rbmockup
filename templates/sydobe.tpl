{* $Header: /cvsroot/ranchbe/ranchbe/templates/sydobe.tpl,v 1.4 2007/11/19 19:01:09 ranchbe Exp $ *}

{*Smarty template*}

{include file="header.tpl"}

<div id="tiki-main">

  <div id="tiki-top">
    {include file="tiki-top_bar.tpl"}
  </div>

  <div id="tiki-mid">
    {include file="tabs.tpl"}
      {if $tool_bar}
      <div id="BoiteActions">
        {include file="$tool_bar"}
      </div>
      {/if}
    {include file=$mid}
  </div>
  
  <div id="tiki-bot">
    {include file="tiki-bot_bar.tpl"}
  </div>

</div>
