{*Smarty template*}

{include file="header.tpl"}
<div id="tiki-center">

{literal}
<script language="JavaScript">
function toggle(elementId1, elementId2) {
  element1 = document.getElementById(elementId1);
  element2 = document.getElementById(elementId2);
  actionInput = document.getElementById('propset_mode');
  if(actionInput.value == 'existing'){
    element1.style.visibility = 'visible';
    element1.style.display = 'block';
    element2.style.visibility = 'hidden';
    element2.style.display = 'none';
    actionInput.value ='create_new';
    //alert('le champ a pour valeur : '+actionInput.value);
  }else{
    element1.style.visibility = 'hidden';
    element1.style.display = 'none';
    element2.style.visibility = 'visible';
    element2.style.display = 'block';
    actionInput.value ='existing';
  }
  return true;
}

function afficherAutre() {

  var c = document.getElementById("size");
  c.style.display = "block";

  var a = document.getElementById("select_param");
  if (document.createForm.field_type.value == 'select'){
    if (a.style.display == "none")
        a.style.display = "block";
  }else{
        a.style.display = "none";
  }

  var a = document.getElementById("select_fromDB");
  if (document.createForm.field_type.value == 'selectFromDB' ||
      document.createForm.field_type.value == 'liveSearch')
  {
    if (a.style.display == "none")
        a.style.display = "block";
  }else{
        a.style.display = "none";
  }

  var a = document.getElementById("select_predef");
  if (document.createForm.field_type.value == 'partner' ||
      document.createForm.field_type.value == 'doctype' ||
      document.createForm.field_type.value == 'user' ||
      document.createForm.field_type.value == 'process' ||
      document.createForm.field_type.value == 'category' ||
      document.createForm.field_type.value == 'document_indice')
  {
    if (a.style.display == "none")
        a.style.display = "block";
  }else{
        a.style.display = "none";
  }

  var b = document.getElementById("select_common");
  if (document.createForm.field_type.value == 'partner' ||
      document.createForm.field_type.value == 'select'  ||
      document.createForm.field_type.value == 'doctype' ||
      document.createForm.field_type.value == 'user' ||
      document.createForm.field_type.value == 'process' ||
      document.createForm.field_type.value == 'category' ||
      document.createForm.field_type.value == 'selectFromDB' ||
      document.createForm.field_type.value == 'document_indice')
  {
    if (b.style.display == "none")
        b.style.display = "block";
  }else{
        b.style.display = "none";
  }

  var a = document.getElementById("text");
  if (document.createForm.field_type.value == 'text' ||
      document.createForm.field_type.value == 'long_text')
  {
    if (a.style.display == "none")
        a.style.display = "block";
  }else{
        a.style.display = "none";
  }

  if (document.createForm.field_type.value == 'integer' ||
      document.createForm.field_type.value == 'decimal')
  {
  }

  var a = document.getElementById("date");
  if (document.createForm.field_type.value == 'date')
  {
    if (a.style.display == "none")
        a.style.display = "block";
  }else{
        a.style.display = "none";
  }

}
</script>
{/literal}

{popup_init src="lib/overlib.js"}

{$form.javascript}

<h1 class="pagetitle">{tr}Datafields{/tr} :</h1>

{*--------------------list header----------------------------------*}

<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading auto"></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=field_name">
   {tr}Field{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=field_description">
   {tr}Description{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=field_type">
   {tr}Type{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=field_size">
   {tr}field_size{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=field_regex">
   {tr}field_regex{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=field_required">
   {tr}field_required{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=field_multiple">
   {tr}field_multiple{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=return_name">
   {tr}return_name{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=field_where">
   {tr}field_where{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=field_list">
   {tr}field_list{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=table_name">
   {tr}table_name{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=field_for_value">
   {tr}field_for_value{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=field_for_display">
   {tr}field_for_display{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=date_format">
   {tr}date_format{/tr}</a></th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].field_name}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

    <td class="thin"><a href="{$smarty.server.PHP_SELF}?action=modify&field_name={$list[list].field_name}&space={$CONTAINER_TYPE}" title="{tr}edit{/tr}">
                     <img border="0" alt="{tr}edit{/tr}" src="img/icons/edit.png" /></a>
    </td>

    {*-Specifics fields-*}
    <td class="thin">{$list[list].field_name}</td>
    <td class="thin">{$list[list].field_description}</td>
    <td class="thin">{$list[list].field_type}</td>
    <td class="thin">{$list[list].field_size}</td>
    <td class="thin">{$list[list].field_regex}</td>
    <td class="thin">{$list[list].field_required|yesorno}</td>
    <td class="thin">{$list[list].field_multiple|yesorno}</td>
    <td class="thin">{$list[list].return_name|yesorno}</td>
    <td class="thin">{$list[list].field_where}</td>
    <td class="thin">{$list[list].field_list}</td>
    <td class="thin">{$list[list].table_name}</td>
    <td class="thin">{$list[list].field_for_value}</td>
    <td class="thin">{$list[list].field_for_display}</td>
    <td class="thin">{$list[list].date_format}</td>
   </tr>
  {/section}
  </table>

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
<script language='Javascript' type='text/javascript'>
<!--
// check / uncheck all.
// in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
// for now those people just have to check every single box
document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/></td>");
document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr} : </label></td></tr>");
//-->                     
</script>

<br>
{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}"
 onclick="if(confirm('{tr}All data of this field will be lost. Are you sure?{/tr}')){ldelim}pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
</form>

<hr />

{*Create form*}
<form {$form.attributes} name="{$formName}"> <!--FIXME: Bug de HTML_QUICKFORM : le name ne se gen�re pas-->
{$form.hidden}

{if $action == 'modify'}
  <h2>{tr}Modify a datafield{/tr}
  <span class="button2">
  <a class="linkbut" href="{$smarty.server.PHP_SELF}?action=create&space={$CONTAINER_TYPE}">
  {tr}Return to create{/tr}</a></span>
  </h2>
{else}
  <h2>{tr}Create a datafield{/tr}</h2>
{/if}

    <b>{tr}{$form.field_type.label}{/tr}:</b><br />
    {$form.field_type.html}

    <br /><br />
    <b>{tr}{$form.field_name.label}{/tr}:</b><br />
    {$form.field_name.html}
  
    <br /><br />
    <b>{tr}{$form.field_description.label}{/tr}:</b><br />
    {$form.field_description.html}

  <span id=size style="display: block">
    <br /><br />
    <b>{tr}{$form.field_size.label}{/tr}:</b><br />
    <i>Max size of the field or number of line to display for select field</i><br />
    {$form.field_size.html}
  </span>

  <span id=text style="display: block">
    <br />
    <b>{tr}{$form.field_regex.label}{/tr}:</b><br />
    <i>You can input here a regular expression to check input information</i><br />
    {$form.field_regex.html}
    <a href="javascript:popup('./lib/testRegExpr.htm','testRegExpr')">{tr}Need help?{/tr}</a></p>
  </span>

    <br /><br />
    <b>{tr}{$form.field_required.label}{/tr}:</b>
    {$form.field_required.html}<br />
    <i>Require the information</i>

  <span id=select_param style="display: none">
    <br />
    <b>{tr}{$form.field_list.label}{/tr}:</b><br />
    <i>You must separate each word/sentence by '#' without space.</i><br />
    {$form.field_list.html}
  </span>

  <span id=select_common style="display: none">
    <br />
    <b>{tr}{$form.field_multiple.label}{/tr}:</b>
    {$form.field_multiple.html}<br />
    <i>Enable the multi selection with select field.</i>

    <br /><br />
    <b>{tr}{$form.adv_select.label}{/tr}:</b>
    {$form.adv_select.html}<br />
    <i>Use the advanced multi select feature</i>

    <br /><br />
    <b>{tr}{$form.return_name.label}{/tr}:</b>
    {$form.return_name.html}<br />
    <i>Return the normalized name or return id</i>
  </span>

  <span id=date style="display: none">
    <br />
    <b>{tr}{$form.date_format.label}{/tr}:</b>
    {$form.date_format.html}<br />
    <i>Format of date. i.e: %Y/%m/%d display 2005/05/28 i.e</i>
  </span>

  <span id=select_predef style="display: none">
    <br />
    <b>{tr}{$form.field_where.label}{/tr}:</b><br />
    <i>You can indicate here a SQL where close to limit list of the select element<br />
        exemple: partner_type = 'customer' to limit the select to the customers partner type</i><br />
    {$form.field_where.html}
  </span>

  <span id=select_fromDB style="display: none">
    <br />
    <b>{tr}{$form.table_name.label}{/tr}:</b>
    {$form.table_name.html}<br />
    <i>Name of the table from where to gets datas.</i>

    <br /><br />
    <b>{tr}{$form.field_for_value.label}{/tr}:</b>
    {$form.field_for_value.html}<br />
    <i>Name of the field used for set the value return by the select</i>

    <br /><br />
    <b>{tr}{$form.field_for_display.label}{/tr}:</b>
    {$form.field_for_display.html}<br />
    <i>Name of the field used for set the text to display</i>
  </span>

  {if $form.propset_name}
  <br /><br />
  <label for="propset_assign_mode"><b>assign to properties set</b>
  <a href="1" />
  <a href="#1" onClick="javascript:toggle('propset_create','propset_select');">Created or existing</a><br />
  </label>
  <span id=propset_select style="display: block">
    <b>{tr}{$form.assign_to.label}{/tr}:</b><br />
    {$form.assign_to.html}
  </span>
  <span id=propset_create style="display: none">
    <b>{tr}{$form.propset_name.label}{/tr}:</b><br />
    {$form.propset_name.html}
  </span>
  <input type="hidden" name="propset_mode" id="propset_mode" value="existing">
  {/if}

 <br /><br />

 {if not $form.frozen}
     
    {$form.reset.html}&nbsp;{$form.action.html}
  
 {/if}

  <br />
    {tr}{$form.requirednote}{/tr}

</form>

<form name="close">
  <input type="button" onclick="window.close()" value="Close">
</form>

<br />

<b>Collected Errors:</b><br />
  {foreach key=name item=error from=$form.errors}
      <font color="red">{$error}</font> in element [{$name}]<br />
  {/foreach}

{literal}
<script language="JavaScript">
  afficherAutre();
</script>
{/literal}

</div>
</body>
