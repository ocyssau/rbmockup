{*Smarty template*}

{include file="header.tpl"}

{$form.javascript}

<form {$form.attributes}>
{$form.hidden}

<h2>{tr}{$form.header.infos}{/tr}</h2>

<table class="normal">

 {if $action eq 'createDoc' || $action eq 'Store' || $modifyDocumentNumber eq 1}
  <tr>
    <td>{tr}{$form.document_number.label}{/tr}:</td>
    <td>{$form.document_number.html}</td>
  </tr>
 {/if}

 {if $action eq 'Store'}
  <tr>
    <td>{tr}{$form.file.label}{/tr}:</td>
    <td>{$form.file.html}</td>
  </tr>
 {/if}

  <tr>
    <td>{tr}{$form.designation.label}{/tr}:</td>
    <td>{$form.designation.html}</td>
  </tr>

 {if $action eq 'createDoc' || $action eq 'Store'}
  <tr>
    <td>{tr}{$form.document_indice_id.label}{/tr}:</td>
    <td>{$form.document_indice_id.html}</td>
  </tr>
 {/if}

  <tr>
    <td>{tr}{$form.category_id.label}{/tr}:</td>
    <td>{$form.category_id.html}</td>
  </tr>

  <!-- Display optionnals fields -->
  <!-- TODO : revoir boucle imbriqu�e -->
  {section name=of loop=$optionalFields}
  {assign var="fn" value=$optionalFields[of].field_name}
  <tr class="formcolor">
    <td>{tr}{$form.$fn.label}{/tr}:</td>
    <td>{$form.$fn.html}</td>
  </tr>
  {/section}

 {if $action eq 'createDoc' || $action eq 'Store'}
  <tr>
    <td>{tr}{$form.lock.label}{/tr}:</td>
    <td>{$form.lock.html}</td>
  </tr>
 {/if}

 {if not $form.frozen}
  <tr>
    <td> </td>
    <td>{$form.reset.html}&nbsp;{$form.action.html}</td>
  </tr>
 {/if}

  <tr>
    <td>{tr}{$form.requirednote}{/tr}</td>
  </tr>

</table>

<input type="hidden" name="page_id" value="DocManage_store" />

</form>

<form name="close">
  <input type="button" onclick="window.close()" value="Close">
</form>

<br />

{foreach key=name item=error from=$form.errors}
 <b>Collected Errors:</b> <br />
 <font color="red">{$error}</font> in element [{$name}]<br />
{/foreach}
