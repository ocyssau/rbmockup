{*Smarty template*}

<div id="BoiteActions">
  <span class="button2"><a href="tiki-admingroups.php" class="linkbut">{tr}Admin groups{/tr}</a></span>
  <span class="button2"><a href="tiki-adminusers.php" class="linkbut">{tr}Admin users{/tr}</a></span>
  <span class="button2"><a href="tiki-adminusers.php#2" class="linkbut">{tr}Add a new user{/tr}</a></span>
</div>

<h1 class="pagetitle">{tr}Admin users{/tr}</h1>

<br />

<div id="page-bar">
<span id="tab1" class="tabmark" style="border-color: white;"><a href="javascript:tikitabs(1,3);">{tr}Users{/tr}</a></span>
{if $edituser}
<span id="tab1" class="tabmark" style="border-color: white;"><a href="javascript:tikitabs(2,3);">{tr}Edit user{/tr} <i>{$userinfo.login}</i></a></span>
{else}
<span id="tab1" class="tabmark" style="border-color: white;"><a href="javascript:tikitabs(2,3);">{tr}Add a new user{/tr}</a></span>
{/if}
</span>
</div>

{* ---------------------- tab with list -------------------- *}
{if $edituser || $add}
 <div id="content1" class="tabcontent" style="display: none;">
{else}
 <div id="content1" class="tabcontent" style="display: block;">
{/if}

<h2>{tr}Users{/tr}</h2>

{*----------------Visualisation filter form--------------------------*}
    <form method="get" action="tiki-adminusers.php">
    <table class="findtable"><tr>
      <td>{tr}Find{/tr}</td>
      <td><input type="text" name="find" value="{$find|escape}" /></td>
      <td><input type="submit" value="{tr}find{/tr}" name="search" /></td>
      <td>{tr}Number of displayed rows{/tr}</td>
      <td><input type="text" size="4" name="numrows" value="{$numrows|escape}" />
      <input type="hidden" name="sort_mode" value="{$sort_mode|escape}" /></td>
      </tr>
    </table>
    </form>


{*--------------------User list header----------------------------------*}
    
    <form name="checkform" method="post" action="{$smarty.server.PHP_SELF}{if $group_management_mode ne  'y' and $set_default_groups_mode ne 'y'}#multiple{/if}">
    <table class="normal">
    <tr>
    <td class="heading auto">&nbsp;</td>
    <td class="heading">&nbsp;</td>
    <td class="heading"><a class="tableheading" href="tiki-adminusers.php?offset={$offset}&numrows={$numrows}&sort_field=handle&sort_order={if $sort_order eq 'DESC'}ASC{else}DESC{/if}">{tr}Name{/tr}</a></td>
    <td class="heading"><a class="tableheading" href="tiki-adminusers.php?offset={$offset}&numrows={$numrows}&sort_field=email&sort_order={if $sort_order eq 'DESC'}ASC{else}DESC{/if}">{tr}Email{/tr}</a></td>
    <td class="heading"><a class="tableheading" href="tiki-adminusers.php?offset={$offset}&numrows={$numrows}&sort_field=lastlogin&sort_order={if $sort_order eq 'DESC'}ASC{else}DESC{/if}">{tr}Last login{/tr}</a></td>
    <td class="heading">&nbsp;</td>
    <td class="heading">{tr}Groups{/tr}</td>
    <td class="heading">&nbsp;</td>
    </tr>

{*--------------------User list body---------------------------*}
    {cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
      {section name=user loop=$users}
       <tr class="{cycle}">
        <td class="thin">
         <input type="checkbox" 
                name="checked[]" 
                value="{$users[user].perm_user_id}" {if $users[user].checked eq 'y'}checked="checked" {/if}/>
        </td>

{*
        <td class="thin">
          <a class="link" 
             href="tiki-user_preferences.php?view_user={$users[user].perm_user_id}"
             title="{tr}Change user preferences{/tr}: {$users[user].handle}">
             <img border="0" 
                  alt="{tr}Change user preferences{/tr}: {$users[user].handle}" 
                  src="img/icons/config.gif" />
          </a>
        </td>
*}
        <td class="thin">
          <a class="link" 
             href="tiki-adminusers.php?offset={$offset}&numrows={$numrows}&sort_field={$sort_field}&sort_order={$sort_order}&edituser={$users[user].perm_user_id}#2"
             title="{tr}edit account settings{/tr}: {$users[user].handle}">
             <img border="0" 
                  alt="{tr}edit account settings{/tr}: {$users[user].handle}"
                  src="img/icons/edit.png" />
          </a>
        </td>
        <td>
          <a class="link" 
             href="tiki-adminusers.php?offset={$offset}&numrows={$numrows}&sort_field={$sort_field}&sort_order={$sort_order}&edituser={$users[user].perm_user_id}#2" 
             title="{tr}edit account settings{/tr}">
             {$users[user].handle}
          </a>
        </td>
        <td>{$users[user].email}</td>
        <td>{$users[user].lastlogin}</td>
        <td class="thin">
          <a class="link" 
            href="tiki-assignuser.php?assign_user={$users[user].perm_user_id|escape:url}" 
            title="{tr}Assign Group{/tr}">
            <img border="0" alt="{tr}Assign Group{/tr}" 
                            src="img/icons/group_add.png" />
          </a>
        </td>
        <td>
          {assign var="current_user_id" value=$users[user].perm_user_id}
          {foreach from=$include_groups.$current_user_id item=grp}
          <a class="link" href="tiki-assignuser.php?assign_user={$current_user_id|escape:url}" title="permissions">
            <img border="0" alt="{tr}Assign Group{/tr}" src="img/icons/group_link.png" />
            {$grp.group_define_name}
          </a>
          {/foreach}
        </td>
        <td  class="thin">
        <a class="link" 
           href="tiki-adminusers.php?offset={$offset}&amp;numrows={$numrows}&amp;sort_mode={$sort_mode}&amp;action=delete&amp;user={$users[user].perm_user_id}"
           title="{tr}delete{/tr}">
           <img src="img/icons/trash.png" border="0" height="16" width="16" alt='{tr}delete{/tr}' />
           </a>
        </td>
        </tr>
      {/section}

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

      {* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
      //-->                     
      </script>

{*Multiselection select action form *}
      <tr>
        <td class="form" colspan="18">
        <a name="multiple"></a><p align="left"> {*on the left to have it close to the checkboxes*}
        {tr}Perform action{/tr} :
        <select name="submit_mult">
          <option value="" selected>-</option>
          <option value="remove_users" >{tr}remove{/tr}</option>
        </select>
  
{*Submit checkform form*}
        <input type="submit" value="{tr}ok{/tr}" />
        </td>
      </tr>
      </table>
  
      <input type="hidden" name="find" value="{$find|escape}" />
      <input type="hidden" name="numrows" value="{$numrows|escape}" />
      <input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
      <input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
      <input type="hidden" name="offset" value="{$offset|escape}" />
      </form>
</div>


{* ---------------------- Add /Edit user form-------------------- *}
<a name="2" ></a>
{if $edituser}
  <div id="content2" class="tabcontent" style="display: block;">
  <h2>{tr}Edit user{/tr}: {$userinfo.handle}</h2>
  <a class="linkbut" href="tiki-assignuser.php?assign_user={$userinfo.perm_user_id|escape:url}">{tr}assign to groups{/tr}: {$userinfo.handle}</a>
{else}
  {if $add}<div id="content2" class="tabcontent" style="display: block;">
  {else}   <div id="content2" class="tabcontent" style="display: none;">{/if}
  <h2>{tr}Add a new user{/tr}</h2>
{/if}

<form action="tiki-adminusers.php" method="post" enctype="multipart/form-data">
<table class="normal">
<tr class="formcolor"><td>{tr}User{/tr}:</td><td><input type="text" name="name"  value="{$userinfo.handle|escape}" /><br />

{if $userinfo.perm_user_id}
    <i>{tr}Warning: changing the username will require the user to change his password and will mess with slave intertiki sites that use this one as master{/tr}</i>
  {else}
    <i>{tr}Warning: changing the username will require the user to change his password{/tr}</i>
{/if}
</td></tr>
<tr class="formcolor"><td>{tr}Pass{/tr}:</td><td><input type="password" name="pass" id="pass" /></td></tr>
<tr class="formcolor"><td>{tr}Again{/tr}:</td><td><input type="password" name="pass2" id="pass2" /></td></tr>
<tr class="formcolor"><td>{tr}Email{/tr}:</td><td><input type="text" name="email" size="30"  value="{$userinfo.email|escape}" /></td></tr>

{if $edituser}
  <tr class="formcolor"><td>&nbsp;</td><td>
  <input type="hidden" name="edituser" value="{$edituser}" />
  <input type="submit" name="submit" value="{tr}Save{/tr}" />

{else}
  <tr class="formcolor">
   <td>
    {tr}Batch upload (CSV file<a {popup text='login,password,email,groups<br />user1,password1,email1,&quot;group1,group2&quot;<br />user2, password2,email2'}><img src="img/icons/help.png" border="0" height="16" width="16" alt='{tr}help{/tr}' /></a>){/tr}:
   </td>
   <td>
    <input type="file" name="csvlist"/><br />{tr}Overwrite{/tr}: <input type="checkbox" name="overwrite" checked="checked" />
   </td>
  </tr>
  <tr class="formcolor"><td>&nbsp;</td><td>
  <input type="hidden" name="newuser" value="1" />
  <input type="submit" name="submit" value="{tr}Add{/tr}" />
{/if}
</td></tr>
</table>
</form>
<br /><br />

</div>
