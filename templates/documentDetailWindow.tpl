{*Smarty template*}

{if $display eq init}

{include file="header.tpl"}

<div id="tiki-mid">
<div id="tiki-center">

<div id="page-bar">
  <span id="tab1" class="tabmark" style="border-color: white;"><a href="javascript:tikitabs(1,10);">{tr}Detail of the document{/tr}</a></span>
  <span id="tab2" class="tabmark" style="border-color: white;"><a href="javascript:tikitabs(2,10);">{tr}Associated files to{/tr}</a></span>
  <span id="tab3" class="tabmark" style="border-color: white;"><a href="javascript:tikitabs(3,10);">{tr}Document relations{/tr}</a></span>
  <span id="tab4" class="tabmark" style="border-color: white;"><a href="javascript:tikitabs(4,10);">{tr}Detail instance{/tr}</a></span>
</div>

{*================================= DETAIL OF DOC ===================================================*}
<a name="1" ></a>
<div id="content1" class="tabcontent" style="display: {$content1display};">
  <h3>{$docinfos.document_number} - {$docinfos.document_indice_id|document_indice}.{$docinfos.document_version}<br />
   <i>{$docinfos.designation}</i></h3>

  <table><tr>
  <td width="50%">
    <table class="normal">
      {cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
      <tr class="{cycle}"><td class="heading" colspan="2">{tr}Basic properties{/tr}</td></tr>
      <tr class="{cycle}"><td>{tr}Number{/tr}</td><td class="thin">{$docinfos.document_number}</td></tr>
      <tr class="{cycle}"><td>{tr}Designation{/tr}</td><td class="thin">{$docinfos.designation}</td></tr>
      <tr class="{cycle}"><td>{tr}Indice{/tr} - {tr}Version{/tr}</b></td><td class="thin">{$docinfos.document_indice_id|document_indice} - v{$docinfos.document_version}</td></tr>
      <tr class="{cycle}"><td>{tr}Category{/tr}</td><td class="thin">{$docinfos.category_id|category}</td></tr>
      <tr class="{cycle}"><td>{tr}Doctype{/tr}</td><td class="thin">{$docinfos.doctype_id|type}</td></tr>

      <tr class="{cycle}"><td class="heading" colspan="2">{tr}Life cycle{/tr}</td></tr>
      <tr class="{cycle}"><td>{tr}access{/tr}</td><td class="thin">{$docinfos.document_access_code|access_code}</td></tr>
      <tr class="{cycle}"><td>{tr}State{/tr}</td><td class="thin">{$docinfos.document_state}</td></tr>
      <tr class="{cycle}"><td>{tr}default_process{/tr}</td><td class="thin"> {$docinfos.default_process_id}</td></tr>

      <tr class="{cycle}"><td class="heading" colspan="2">{tr}Date and time{/tr}</td></tr>
      {if $docinfos.check_out_by}
      <tr class="{cycle}"><td>{tr}CheckOut{/tr}</td><td class="thin">{tr}By{/tr} {$docinfos.check_out_by|username} {tr}at{/tr} {$docinfos.check_out_date|date_format}</td></tr>
      {/if}
      <tr class="{cycle}"><td>{tr}Create{/tr}</td><td class="thin">{tr}By{/tr} {$docinfos.open_by|username} {tr}at{/tr} {$docinfos.open_date|date_format}</td></tr>
      <tr class="{cycle}"><td>{tr}Last Update{/tr}</td><td class="thin">{tr}By{/tr} {$docinfos.update_by|username} {tr}at{/tr} {$docinfos.update_date|date_format}</td></tr>

      <tr class="{cycle}"><td class="heading" colspan="2">{tr}Other properties{/tr}</td></tr>
      <tr class="{cycle}"><td>{tr}document_id{/tr}</td><td class="thin">{$docinfos.document_id}</td></tr>
      {if $docinfos.from_document_id}
      <tr class="{cycle}">
        <td>{tr}from_document_id{/tr}</td>
        <td><a href="javascript:popupP('DocManage.php?documentDetail=1&document_id={$docinfos.from_document_id}&space={$CONTAINER_TYPE}','documentDetailWindow',600,1024);">
        {$docinfos.from_document_id}</a></td>
      </tr>
      {/if}
    </table>
  </td>


  <td width="50%"> <!-- Display optionnals fields -->
    <table class="normal">
      <tr class="{cycle}"><td class="heading" colspan="2">{tr}Extends properties{/tr}</td></tr>
      {foreach item=type from=$optionalFields}
      {assign var="fn" value=$type.field_name}
      <tr class="{cycle}">
        <td>{tr}{$type.field_description}{/tr}</td>
        <td class="thin">{filter_select id=$docinfos.$fn type=$type.field_type}</td>
      </tr>
      {/foreach}
    </table>  
  </td>
  <td width="50%"> <!-- Display optionnals fields -->
    <table class="normal">
      <tr>
        <td>
        <!--{if $documentVisu}{$documentVisu}{else}{$documentPicture}{/if}-->
        {get_visu document_id=$docinfos.document_id odocument=$odocument}
        </td>
      </tr>
    </table>
  </td>
  </tr></table>
</div>

{*================================= ASSOCIATED FILES ===================================================*}
<a name="2" ></a>
<div id="content2" class="tabcontent" style="display: {$content2display};">
<h3>{tr}Associated files to{/tr} : {$document_name}</h3>

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading">
    {tr}file_name{/tr} .
    {tr}file_extension{/tr} - 
    {tr}file_version{/tr}<br />
    {tr}file_type{/tr}<br />
    {tr}file_access_code{/tr} - {tr}file_state{/tr}
  </th>
  <th class="heading">
  {tr}Action - user - date{/tr}
  </th>
  <th class="heading">
  {tr}infos{/tr}
  </th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].file_id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

    <td class="thin">
      <a class="link" a href="viewFile.php?file_id={$list[list].file_id}&space={$CONTAINER_TYPE}&object_class=docfile&ticket={$ticket}" title="{tr}View file{/tr}">
      {file_icon extension=$list[list].file_extension icondir=$file_icons_dir icontype='.gif'}
      {$list[list].file_name} - V{$list[list].file_version}</a>
      <a href="javascript:popup('Versions.php?action=versions&file_id={$list[list].file_id}&space={$CONTAINER_TYPE}','{$CONTAINER_TYPE}Versions')" title="{tr}Versions{/tr}">
      {tr}See old versions{/tr}</a>
      <br />{$list[list].file_type}
      <br />{$list[list].file_access_code|access_code} - {$list[list].file_state}
    </td>

    <td class="thin">
      {if $list[list].file_checkout_by }
      <b>{tr}Checkout{/tr} : </b>{$list[list].file_checkout_by|username} - {$list[list].file_checkout_date|date_format}
      <br />{/if}
      <b>{tr}Last Update{/tr} : </b>{$list[list].file_update_by|username} - {$list[list].file_update_date|date_format}
      <br />
      <b>{tr}Create{/tr} : </b>{$list[list].file_open_by|username} - {$list[list].file_open_date|date_format}
    </td>

    <td class="thin">
      <i>{tr}file_path{/tr}:</i> {$list[list].file_path}<br />
      <i>{tr}file_size{/tr}:</i> {$list[list].file_size|filesize_format}<br />
      <i>{tr}file_mtime{/tr}:</i> {$list[list].file_mtime|date_format}<br />
      <i>{tr}file_md5{/tr}:</i> {$list[list].file_md5}<br />
    </td>
   </tr>
  {/section}
</table>

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
{*      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
      //-->                     
      </script>
        <br>
*}

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="SuppressLink" title="{tr}Suppress link{/tr}" id="01"
 onclick="if(confirm('{tr}Do you want really suppress this file links{/tr}')){ldelim}document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress link{/tr}" alt="{tr}Suppress link{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="ResetFile" title="{tr}Reset{/tr}" id="03"
 onclick="document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_cancel.png" title="{tr}Reset{/tr}" alt="{tr}Reset{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="putInWs" title="{tr}Put in wildspace{/tr}" id="06"
 onclick="document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_read.png" title="{tr}Put in wildspace{/tr}" alt="{tr}Put in wildspace{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="document_id" value="{$document_id|escape}" />
<input type="hidden" name="documentDetail" value="1" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="page_id" value="detailWindow" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
</form>

{*================================= EMBEDED VIEWER ===================================================*}

  {if $embededViewer}
    {$embededViewer}
  {else}
    <b>{tr}documentDetailWindow_message1{/tr}</b><br />
  {/if}
 
{*===================== DOCUMENT STRUCTURE ==============================================================*}
{elseif $display eq doclink}
</div>
<div id="content3" class="tabcontent" style="display: {$content3display};">
<a name="3"></a>
<h3>{tr}Son documents of{/tr} : {$document_name}</h3>

{*--------------------list header----------------------------------*}
<form name="linkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading">{tr}Number{/tr}</th>

  <th width="50" class="heading" nowrap>{tr}Designation{/tr}</th>
  <th class="heading">{tr}Indice{/tr} - {tr}Version{/tr}</th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$doclinked_list}
   <tr class="{cycle}">
   <!--{*
    <td class="thin">{$doclinked_list[list].rs_link_id}<input type="checkbox" 
                            name="rs_link_id[]"
                            value="{$doclinked_list[list].rs_link_id}" {if $doclinked_list[list].rs_link_id eq 'y'}checked="checked" {/if}/>
    </td>
    <td class="thin"><a class="link" href="javascript:popupP('DocManage.php?documentDetail=1&document_id={$doclinked_list[list].document_id}&space={$doclinked_list[list].doc_space}','documentDetailWindow', 600 , 1024)">{$doclinked_list[list].document_number}</a></td>
    <td class="thin">{$doclinked_list[list].designation}</td>
    <td class="thin">{$doclinked_list[list].document_indice_id|document_indice}.{$doclinked_list[list].document_version}
    {if $doclinked_list[list].rs_access_code > 14}<img class="icon" src="./img/icons/lock.png" />{/if}
    </td>
    *}-->

    <td class="thin">{$doclinked_list[list].dr_link_id}<input type="checkbox" 
                            name="dr_link_id[]"
                            value="{$doclinked_list[list].dr_link_id}" {if $doclinked_list[list].dr_link_id eq 'y'}checked="checked" {/if}/>
    </td>
    <td class="thin"><a class="link" href="javascript:popupP('DocManage.php?documentDetail=1&document_id={$doclinked_list[list].document_id}&space={$doclinked_list[list].doc_space}','documentDetailWindow', 600 , 1024)">{$doclinked_list[list].document_number}</a></td>
    <td class="thin" nowrap>{$doclinked_list[list].designation}</td>
    <td class="thin">{$doclinked_list[list].document_indice_id|document_indice}.{$doclinked_list[list].document_version}
    {if $doclinked_list[list].dr_access_code > 14}<img class="icon" src="./img/icons/lock.png" />{/if}
    </td>


   </tr>

  {sectionelse}
  <tr>
  	<td class="{cycle advance=false}" colspan="7">
  	{tr}No document is linked to this document{/tr}
  	</td>
  </tr>	
  {/section}
</table>

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
{*
      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'lchecked[]',this.lchecked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
      //-->                     
      </script>
<br>
*}

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="AddSon" title="{tr}Add son{/tr}" id="01"
 onclick="pop_no(linkform)">
<img class="icon" src="./img/icons/doclink/addson.png" title="{tr}Add son{/tr}" alt="{tr}Add son{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="SuppressSon" title="{tr}Suppress son{/tr}" id="01"
 onclick="pop_no(linkform)">
<img class="icon" src="./img/icons/doclink/delson.png" title="{tr}Suppress son{/tr}" alt="{tr}Suppress son{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="ViewFatherDoc" title="{tr}View father document{/tr}" id="01"
 onclick="pop_no(linkform)">
<img class="icon" src="./img/icons/doclink/document_father.png" title="{tr}View father document{/tr}" alt="{tr}View father document{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="document_id" value="{$document_id|escape}" />
<input type="hidden" name="documentDetail" value="1" />
<input type="hidden" name="frompage" value="{$smarty.template}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="page_id" value="detailWindow" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />

</form>

{if isset($father_documents)}

<h3>{tr}Father documents of{/tr} : {$document_name}</h3>

  <table class="normal">
   <tr>
    <th class="heading"></th>
    <th class="heading">{tr}Number{/tr}</th>
  
    <th width="50" class="heading">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{tr}Designation{/tr}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
    <th class="heading">{tr}Indice{/tr} - {tr}Version{/tr}</th>
   </tr>
  
  {*--------------------list body---------------------------*}
  {cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
    {section name=list loop=$father_documents}
     <tr class="{cycle}">
      <td class="thin"></td>
      <td class="thin"><a class="link" href="javascript:popupP('DocManage.php?documentDetail=1&document_id={$father_documents[list].document_id}&space={$father_documents[list].doc_space}','documentDetailWindow', 600 , 1024)">{$father_documents[list].document_number}</a></td>
      <td class="thin">{$father_documents[list].designation}</td>
      <td class="thin">{$father_documents[list].document_indice_id|document_indice}.{$father_documents[list].document_version}</td>
     </tr>
  
    {sectionelse}
    <tr>
    	<td class="{cycle advance=false}" colspan="7">
    	{tr}No document is linked to this document{/tr}
    	</td>
    </tr>	
    {/section}
  </table>
{/if}

{*===================== DOCUMENT ALTERNATIVE ==============================================================*}
{elseif $display eq alternative}

<div id="content3" class="tabcontent" style="display: block;">
<h3>{tr}Alternatives of{/tr} : {$document_name}</h3>

{*--------------------list header----------------------------------*}
<form name="linkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading">{tr}Number{/tr}</th>

  <th width="50" class="heading">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{tr}Designation{/tr}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
  <th class="heading">{tr}Indice{/tr} - {tr}Version{/tr}</th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$alternative_list}
   <tr class="{cycle}">
    <td class="thin">{$alternative_list[list].alt_link_id}<input type="checkbox"
      name="alt_link_id[]"
      value="{$alternative_list[list].alt_link_id}" {if $alternative_list[list].alt_link_id eq 'y'}checked="checked" {/if}/>
    </td>
    <td class="thin"><a class="link" href="javascript:popupP('DocManage.php?documentDetail=1&document_id={$alternative_list[list].document_id}&space={$alternative_list[list].doc_space}','documentDetailWindow', 600 , 1024)">{$alternative_list[list].document_number}</a></td>
    <td class="thin">{$alternative_list[list].designation}</td>
    <td class="thin">{$alternative_list[list].document_indice_id|document_indice}.{$alternative_list[list].document_version}</td>
   </tr>
  {sectionelse}
  <tr>
  	<td class="{cycle advance=false}" colspan="7">
  	{tr}No alternatives for this document{/tr}
  	</td>
  </tr>	
  {/section}
</table>

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="AddAlternative" title="{tr}Add alternative{/tr}" id="01"
 onclick="pop_no(linkform)">
<img class="icon" src="./img/icons/doclink/addson.png" title="{tr}Add alternative{/tr}" alt="{tr}Add alternative{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="SuppressAlter" title="{tr}Suppress alternative{/tr}" id="01"
 onclick="pop_no(linkform)">
<img class="icon" src="./img/icons/doclink/delson.png" title="{tr}Suppress alternative{/tr}" alt="{tr}Suppress alternative{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="document_id" value="{$document_id|escape}" />
<input type="hidden" name="documentDetail" value="1" />
<input type="hidden" name="frompage" value="{$smarty.template}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="page_id" value="detailWindow" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
</form>

{*=================================LISTING OF INSTANCES===============================================*}
{elseif $display eq instance}
</div>

<a name="4" ></a>

<a href="DocManage.php?documentDetail=1&displayWf=1&document_id={$docinfos.document_id}&space={$CONTAINER_TYPE}">Display Instances of Workflow</a>

<div id="content4" class="tabcontent" style="display: {$content4display};">
<h3>{tr}Monitor instances of process{/tr} : {$proc_info.name} {$proc_info.version}</h3>

<br />

{*LISTING*}
<form action="galaxia_monitor_instances.php" method="post">
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="where" value="{$where|escape}" />
<input type="hidden" name="sort_mode" value="{$sort_mode|escape}" />
<table class="normal">
<tr>
<th class="heading" >{tr}Id{/tr}</a></th>
{*<th class="heading" >{tr}Activity{/tr}</a></th>*}
<th class="heading" >{tr}Name{/tr}</a></th>
<th class="heading" >{tr}Process{/tr}</a></th>
<th class="heading" >{tr}Started{/tr}</a></th>
<th class="heading" >{tr}Status{/tr}</a></th>
<th class="heading" >{tr}Ended{/tr}</a></th>
<th class="heading" >{tr}Creator{/tr}</a></th>
</tr>
{cycle values="odd,even" print=false}
{foreach from=$items item=proc}
<tr>
	<td class="{cycle advance=false}">
	  	{$proc.instanceId}
	</td>

	<td class="{cycle advance=false}" style="text-align:center;">
		<a class="link" href="{$smarty.server.PHP_SELF}?iid={$proc.instanceId}&document_id={$document_id}&documentDetail=1#2">{$proc.insName}
	</td>

	<td class="{cycle advance=false}" style="text-align:center;">
		{tr}{$proc.name}{/tr}
	</td>

	<td class="{cycle advance=false}" style="text-align:center;">
		{$proc.started|date_format}
	</td>

  <td class="{cycle advance=false}" style="text-align:center;">
		{$proc.status}
	</td>
  
  <td class="{cycle advance=false}" style="text-align:center;">
		{if $proc.ended eq 0} {tr}Not ended{/tr} {else} {$proc.ended|date_format} {/if}
	</td>

	<td class="{cycle advance=false}" style="text-align:center;">
	{$proc.owner}
	</td>
</tr>
{foreachelse}
<tr>
	<td class="{cycle advance=false}" colspan="7">
	{tr}No instances created yet{/tr}
	</td>
</tr>	
{/foreach}
</table>
</form>
{* END OF LISTING OF INSTANCES *}

{elseif $display eq detailinstance}

{*========================= DETAIL OF INSTANCES ======================================================*}

<a name="2" ></a>

<a href="DocManage.php?documentDetail=1&displayWf=1&document_id={$docinfos.document_id}&space={$CONTAINER_TYPE}">Display Instances of Workflow</a>


<h3>{tr}Detail instance{/tr} : {$iid}</h3>

<table class="normal">
<tr><td>
</td></tr>
<tr>
	<td class="formcolor" colspan="2">
		{if count($acts)}
		<table class="normal">
			<tr>
				<td class="formcolor">{tr}Name{/tr}</td>
				<td class="formcolor">{tr}Started{/tr}</td>
				<td class="formcolor">{tr}Act status{/tr}</td>
				<td class="formcolor">{tr}Expiration Date{/tr}</td>
				<td class="formcolor">{tr}Ended{/tr}</td>
				<td class="formcolor">{tr}User{/tr}</td>
			</tr>
		{section name=ix loop=$acts}
			<tr>
				
        {*<td class="{cycle values='odd,even' advance=false}"><a href="galaxia_admin_instance_activity.php?iid={$iid}&aid={$acts[ix].activityId}">{$acts[ix].name}</a></td>*}
				
        <td class="{cycle values='odd,even' advance=false}"><a href="{$smarty.server.PHP_SELF}?iid={$iid}&document_id={$document_id}&aid={$acts[ix].activityId}&documentDetail=1#3">{$acts[ix].name}</a></td>

				<td class="{cycle advance=false}">{$acts[ix].iaStarted|date_format:"%b %e, %Y - %H:%M"|capitalize}</td>
				<td class="{cycle advance=false}">{$acts[ix].actstatus}</td>
				<td class="{cycle advance=false}">{if $acts[ix].exptime eq 0 && $acts[ix].type eq 'activity' && $acts[ix].isInteractive eq 'y'}{tr}Not Defined{/tr}{elseif $acts[ix].type != 'activity'}&lt;{$acts[ix].type}&gt;{elseif $acts[ix].isInteractive eq 'n'}{tr}Not Interactive{/tr}{else}{$acts[ix].exptime|date_format:"%b %e, %Y - %H:%M"|capitalize}{/if}</td>
				<td class="{cycle advance=false}">{if $acts[ix].ended eq 0}{tr}Not Ended{/tr}{else}{$acts[ix].ended|date_format:"%b %e, %Y - %H:%M"|capitalize}{/if}</td>
				<td class="{cycle values='odd,even'}">{$acts[ix].user}</td>
		{/section}
		{/if}
		</table>
	</td>
</tr>	
</tr>	
</table>


{elseif $display eq detailActivity}

{*========================== DETAIL OF ACTIVITY ======================================================*}

<a name="3" ></a>

//'DocManage.php?documentDetail=1&document_id=20191&space=workitem','documentDetailWindow',%20600%20,%201024

	{if $displayWf eq 1}
	
	<h3>{tr}Detail activity{/tr} : {$aid}</h3>
	<h3>{tr}Comments{/tr}</h3>
	{section name=ix loop=$comments}
	<table class="email">
	        <tr>
		    	<td class="heading">{tr}From{/tr}:</td>
			<td class="formcolor">{$comments[ix].user|capitalize:true}</td>
	      	        <td class="closeButton">
			    <form method="POST" target="email" action="galaxia_view_comment.php">
			    <input type="hidden" name="__user" value="{$comments[ix].user}" />
				  <input type="hidden" name="__title" value="{$comments[ix].title}" />
				  <input type="hidden" name="__comment" value="{$comments[ix].comment}" />
				  <input type="hidden" name="__timestamp" value="{$comments[ix].timestamp}" />
      	    	  <input type="submit" name="view" title="{tr}Pop-up{/tr}" galaxia_view_comment.php','email','HEIGHT=400,width=400,resizable=0,menubar=no,location=no,scrollbars=1')" value="&#164">
      	    	  <input type="hidden" name="displayWf" value="1" />
			    </form>
			</td>
			<td class="closeButton">    
		      	    <form method="POST" action="galaxia_admin_instance_activity.php?iid={$iid}&aid={$aid}">
			    	  <input type="hidden" name="__removecomment" value="{$comments[ix].cId}" />
		      	    	  <input type="submit" name="eraser" value="X" title="{tr}erase{/tr}">
		      	    	  <input type="hidden" name="displayWf" value="1" />
		      	    </form>
			</td>
		</tr>
		<tr>
		    	<td class="heading">{tr}Date{/tr}:</td>
			<td class="formcolor" colspan="3">{$comments[ix].timestamp|date_format:"%A %e de %B, %Y %H:%M:%S"|capitalize:true}</td>
		</tr>
		<tr>
			<td class="heading">{tr}Subject{/tr}:</td>
			<td class="formcolor" colspan="3">{$comments[ix].title}</td>
		</tr>
		<tr>
			{*<td class="body">{tr}Body{/tr}:</td>*}
			<td class="body" colspan="4">{$comments[ix].comment}</td>
		</tr>
	</table>
	{/section}
	
	</div>
	</div>
	</div>
	{/if}

{else}

</div>

<form name="close">
<input type="button" onclick="window.close()" value="Close window">
</form>

</div>
</div>

{/if}

