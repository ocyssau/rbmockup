{*Smarty template*}
{popup_init src="./lib/overlib.js"}

{*--------------------Actions defintion--------------------------*}
{include file='containerActions.tpl'}
<br />

<h1 class="pagetitle">{tr}{$PageTitle}{/tr}</h1>

{*--------------------Search Bar defintion--------------------------*}
{include file='searchBar.tpl'}

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{*--------------------list header----------------------------------*}

<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading auto"></th>
  <th class="heading auto"></th>
  <th class="heading auto"></th>
  <th class="heading auto"></th>
  <th class="heading auto"></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field={$CONTAINER_TYPE}_number">
   {tr}Number{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order=DESC}{/if}&sort_field={$CONTAINER_TYPE}_description">
   {tr}Description{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order=DESC}{/if}&sort_field=project_id">
   {tr}Project{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order=DESC}{/if}&sort_field={$CONTAINER_TYPE}_state">
   {tr}State{/tr}</a></th>

  <!-- Display optionnals fields -->
  {section name=of loop=$optionalFields}
    <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field={$optionalFields[of].field_name}">
   {tr}{$optionalFields[of].field_description}{/tr}</a></th>
  {/section}

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order=DESC}{/if}&sort_field=open_date">
   {tr}Created date{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order=DESC}{/if}&sort_field=open_by">
   {tr}Created by{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order=DESC}{/if}&sort_field=forseen_close_date">
   {tr}Forseen close date{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order=DESC}{/if}&sort_field=close_date">
   {tr}Close date{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order=DESC}{/if}&sort_field=close_by">
   {tr}Close by{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order=DESC}{/if}&sort_field=default_file_path">
  {tr}Deposit dir.{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order=DESC}{/if}&sort_field=default_process_id">
  {tr}Default process{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order=DESC}{/if}&sort_field=file_only">
  {tr}Manage file or doc{/tr}</a></th>

 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].$CONTAINER_ID}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>
    
    {if $list[list].object_class eq $CONTAINER_TYPE }
    <td class="thin"><a href="javascript:popup('{$CONTAINER_TYPE}Manage.php?action=modify&{$CONTAINER_TYPE}_id={$list[list].$CONTAINER_ID}','{$CONTAINER_TYPE}Manage')" title="{tr}edit{/tr}">
                     <img border="0" alt="{tr}edit{/tr}" src="img/icons/edit.png" />
                     </a>
    </td>

    <td class="thin"><a href="javascript:popup('ContainerHistory.php?action=getHistory&container_id={$list[list].$CONTAINER_ID}&space={$CONTAINER_TYPE}','history')" title="{tr}Get history{/tr}">
                     <img border="0" alt="{tr}Get history{/tr}" src="img/icons/history.png" />
                     </a>
    </td>

    <td class="thin"><a href="{$CONTAINER_TYPE}s.php?action=suppress&checked[]={$list[list].$CONTAINER_ID}&ticket={$ticket}" title="{tr}Suppress{/tr}" 
                      onclick="return confirm('{tr}Do you want really suppress{/tr} {$list[list].$CONTAINER_NUMBER}')">
                     <img border="0" alt="{tr}Suppress{/tr}" src="img/icons/trash.png" />
                     </a>
    </td>

    {elseif $list[list].object_class eq $CONTAINER_TYPE|cat:'Alias'}
    <td class="thin"><a href="javascript:popup('ContainerAliasManage.php?action=modifyAlias&checked={$list[list].alias_id}&space={$CONTAINER_TYPE}','{$CONTAINER_TYPE}Manage')" title="{tr}edit{/tr}">
                     <img border="0" alt="{tr}edit{/tr}" src="img/icons/edit.png" />
                     </a>
    </td>

    <td class="thin">
    </td>
    <td class="thin"><a href="ContainerAliasManage.php?action=suppressAlias&checked[]={$list[list].alias_id}&ticket={$ticket}&space={$CONTAINER_TYPE}" title="{tr}Suppress{/tr}" 
                      onclick="return confirm('{tr}Do you want really suppress{/tr} {$list[list].$CONTAINER_NUMBER}')">
                     <img border="0" alt="{tr}Suppress{/tr}" src="img/icons/trash.png" />
                     </a>
    </td>
    {else }

    <td class="thin"></td>
    <td class="thin"></td>
    <td class="thin"></td>

    {/if}

    <td class="thin" "nowrap">
    <a href="javascript:popupP('ContainerLinkManage.php?action=getMetadataLinks&container_id={$list[list].$CONTAINER_ID}&space={$CONTAINER_TYPE}',900,450,'Metadatas')" title="{tr}Metadatas{/tr}">
                     <img border="0" alt="{tr}Metadatas{/tr}" src="img/icons/link_edit.png" />
                     </a>
    <a href="javascript:popupP('ContainerLinkManage.php?action=getDoctypeLinks&container_id={$list[list].$CONTAINER_ID}&space={$CONTAINER_TYPE}',900,450,'Doctypes')" title="{tr}Doctypes{/tr}">
                     <img border="0" alt="{tr}Doctypes{/tr}" src="img/icons/link_edit.png" />
                     </a>
    <a href="javascript:popupP('ContainerLinkManage.php?action=getPropsetLinks&container_id={$list[list].$CONTAINER_ID}&space={$CONTAINER_TYPE}',900,450,'Doctypes')" title="{tr}Propsets{/tr}">
                     <img border="0" alt="{tr}Propsets{/tr}" src="img/icons/link_edit.png" />
                     </a>
    <a href="javascript:popupP('ContainerLinkManage.php?action=getCategoryLinks&container_id={$list[list].$CONTAINER_ID}&space={$CONTAINER_TYPE}',900,450,'Categories')" title="{tr}Categories{/tr}">
                     <img border="0" alt="{tr}Categories{/tr}" src="img/icons/link_edit.png" />
                     </a>
    </td>

    <td class="thin"><a href="{$CONTAINER_TYPE}s.php?action=getStats&container_id={$list[list].$CONTAINER_ID}&ticket={$ticket}" title="{tr}Statistics{/tr}">
                     <img border="0" alt="{tr}Statistics{/tr}" src="img/icons/statistics.png" />
                     </a>
    </td>


    {*-Specifics fields-*}
    <td><a class="link" href="DocManage.php?SelectedContainer={$list[list].$CONTAINER_ID}&SelectedContainerNum={$list[list].$CONTAINER_NUMBER}&space={$CONTAINER_TYPE}" >{$list[list].$CONTAINER_NUMBER}</a></span></td>
    <td>{$list[list].$CONTAINER_DESCRIPTION}</td>
    <td>{$list[list].project_id|project}</td>
    <td>{$list[list].$CONTAINER_STATE}</td>

    <!-- Display optionnals fields -->
    {foreach key=key item=type from=$optionalFields}
      {assign var="fn" value=$type.field_name}
      <td class="thin" class="link" >{filter_select id=$list[list].$fn type=$type.field_type}</td>
    {/foreach}

    <td>{$list[list].open_date|date_format}</td>
    <td>{$list[list].open_by|username}</td>
    <td>{$list[list].forseen_close_date|date_format}</td>
    <td>{$list[list].close_date|date_format}</td>
    <td>{$list[list].close_by|username}</td>
    <td>{$list[list].default_file_path}</td>
    <td>{$list[list].default_process_id|process}</td>
    {if $list[list].file_only}
    <td>file</td>{else}<td>doc</td>{/if}

    <!-- Display optionnals fields 
    {foreach key=name item=type from=$OptionalFieldsBody}
    <td class="thin">{filter_select id=$list[list].$name type=$type}</a></td>
    {/foreach}-->

   </tr>
  {sectionelse}
    <tr><td colspan="6">{tr}No containers to display{/tr}</td></tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}

{include file='pagination.tpl'}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
<script language='Javascript' type='text/javascript'>
<!--
// check / uncheck all.
// in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
// for now those people just have to check every single box
document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/></td>");
document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr} : </label></td></tr>");
//-->                     
</script>

<br>
{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}"
onclick="if(confirm('{tr}Do you want really suppress this containers{/tr}')){ldelim}document.checkform.action='{$smarty.server.PHP_SELF}'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="changeState" title="{tr}Change state{/tr}"
onclick="document.checkform.action='none.php' ; pop_it(checkform)">
<img class="icon" src="./img/icons/state/state.png" title="{tr}Change state{/tr}" alt="{tr}Change state{/tr}" width="16" height="16" />
</button>

<!--
<button class="mult_submit" type="submit" name="action" value="selectContainerForImport" title="{tr}Import{/tr}"
onclick="document.checkform.action='{$CONTAINER_TYPE}ImportFileManager.php'">
<img class="icon" src="./img/icons/package/package.png" title="{tr}Import{/tr}" alt="Import" width="16" height="16" />
</button>
-->

<button class="mult_submit" type="submit" name="action" value="Archive" title="{tr}DocToArchive{/tr}" 
 onclick="if(confirm('{tr}Do you want really archive this documents{/tr}')){ldelim}document.checkform.action='{$smarty.server.PHP_SELF}'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/document/toarchive.png" title="{tr}DocToArchive{/tr}" alt="{tr}DocToArchive{/tr}" width="16" height="16" />
</button>


<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
onclick="document.checkform.action='{$smarty.server.PHP_SELF}'; pop_no(checkform)">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>




<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_field|escape}" />
<input type="hidden" name="sort_order" value="{$sort_order|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="ticket" value="{$ticket}" />
</form>
