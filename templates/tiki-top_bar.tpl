{*Smarty template*}
RanchBE ver:{$ranchbe_version} build:{$ranchbe_build} {$ranchbe_copyright} <a href="http://www.ranchbe.com" title="RanchBE">RanchBE Group</a>
| {$smarty.now|date_format:"%A %d %B %Y"}
| {tr}Connected as{/tr} {$current_user_name}
|<a font href="?logout=1">{tr}Logout{/tr}</a>
|<a href="javascript:popupP('tiki-adminusers.php?edituser=me','myPassword',280,380)">{tr}Change password{/tr}</a>
{if $allow_user_prefs}
|<a href="javascript:popupP('userPreferences.php?edituser=me','myPassword',280,380)">{tr}Preferences{/tr}</a>
{/if}
|<a href="javascript:popup('context.php','Context')">{tr}Context{/tr}</a>
|<a href="javascript:popup('about.php','About')">{tr}About Ranchbe{/tr}</a>
</font>
