{*Smarty template*}

{include file="header.tpl"}

{literal}
<style type="text/css">
.listyle {
	text-indent: -20px;
	z-index: 1;
	padding-right: 15px;
	padding-bottom: 2px;
	padding-top: 2px;
	padding-left: 20px;
	line-height: 15px;
	margin-bottom: 0px;
}

.ulstyle {
	list-style-type: none;
	position: relative;
	right: 0px;
	z-index: 1;
	margin: 0px;
	padding: 0px;
}

.divstyle {
	z-index: 1;
	min-width: 140px;
	margin: 0px 0 0px 0px !important;
	padding: 1px;
	display: none;
}
</style>
{/literal}


<div id="tiki-main">

  <div id="tiki-top">
    {include file="tiki-top_bar.tpl"}
  </div>

  <div id="tiki-mid">
    <div id="tiki-center">
      {if $tool_bar}
        <div id="wikitopline">{include file="$tool_bar"}</div>
      {/if}
      {if $mid}
        {include file=$mid}
      {/if}
      {if $midcontent}
		{$midcontent}
      {/if}
    </div>
  </div>

  <div id="tiki-bot">
    {include file="tiki-bot_bar.tpl"}
  </div>

</div>
</body>
