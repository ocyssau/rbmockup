{*Smarty template*}

{include file="header.tpl"}

<br>

<h1 class="pagetitle">{tr}Document history{/tr}</h1>

{*--------------------Search Bar defintion--------------------------*}
{include file='searchBar_simple.tpl'}

<form id="resetf" action="{$smarty.server.PHP_SELF}" method="post">
  <input type="hidden" name="document_id" value="{$document_id}" />
  <input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
  <input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" />
</form>

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading auto"></th>
  <th class="heading auto"></th>

  <th class="heading">
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC' document_id=$document_id}{else}{sameurl sort_order='DESC' document_id=$document_id}{/if}&sort_field=histo_order">
  {tr}histo_order{/tr}</a>
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC' document_id=$document_id}{else}{sameurl sort_order='DESC' document_id=$document_id}{/if}&sort_field=action_name">
  {tr}action_name{/tr}</a>
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC' document_id=$document_id}{else}{sameurl sort_order='DESC' document_id=$document_id}{/if}&sort_field=action_by">
  {tr}action_by{/tr}</a>
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC' document_id=$document_id}{else}{sameurl sort_order='DESC' document_id=$document_id}{/if}&sort_field=action_date">
  {tr}action_date{/tr}</a>
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC' document_id=$document_id}{else}{sameurl sort_order='DESC' document_id=$document_id}{/if}&sort_field=comment">
  {tr}comment{/tr}</a>
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC' document_id=$document_id}{else}{sameurl sort_order='DESC' document_id=$document_id}{/if}&sort_field=from_document_id">
  {tr}from_document_id{/tr}</a>
  </th>

  <th class="heading">
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC' document_id=$document_id}{else}{sameurl sort_order='DESC' document_id=$document_id}{/if}&sort_field=document_number">
  {tr}Number{/tr}</a> - 
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC' document_id=$document_id}{else}{sameurl sort_order='DESC' document_id=$document_id}{/if}&sort_field=document_indice_id">
  {tr}Indice{/tr}</a>.
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC' document_id=$document_id}{else}{sameurl sort_order='DESC' document_id=$document_id}{/if}&sort_field=document_version">
  {tr}Version{/tr}</a> - 
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC' document_id=$document_id}{else}{sameurl sort_order='DESC' document_id=$document_id}{/if}&sort_field={$container_map_id}">
  {tr}Container{/tr}</a>
  <br />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC' document_id=$document_id}{else}{sameurl sort_order='DESC' document_id=$document_id}{/if}&sort_field=designation">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{tr}Designation{/tr}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
  <br />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC' document_id=$document_id}{else}{sameurl sort_order='DESC' document_id=$document_id}{/if}&sort_field=doctype_id">
  {tr}Type{/tr}</a>
  <br />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC' document_id=$document_id}{else}{sameurl sort_order='DESC' document_id=$document_id}{/if}&sort_field=document_state">
  {tr}State{/tr}</a>
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC' document_id=$document_id}{else}{sameurl sort_order='DESC' document_id=$document_id}{/if}&sort_field=document_access_code">
  {tr}access{/tr}</a>
  </th>


  <th class="heading">
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC' document_id=$document_id}{else}{sameurl sort_order='DESC' document_id=$document_id}{/if}&sort_field=activity_id">
  {tr}Activity_id{/tr}</a>.
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC' document_id=$document_id}{else}{sameurl sort_order='DESC' document_id=$document_id}{/if}&sort_field=instance_id">
  {tr}Instance_id{/tr}</a>
  </th>

 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="histo_order[]" value="{$list[list].histo_order}" /></td>

    <td class="thin"><a href="DocHistory.php?action=suppress&histo_order[]={$list[list].histo_order}&document_id={$document_id}&space={$CONTAINER_TYPE}" title="{tr}Suppress{/tr}: {$list[list].histo_order}"
                      onclick="return confirm('{tr}Do you want really suppress{/tr} history {$list[list].histo_order}')">
                     <img border="0" alt="{tr}Suppress{/tr}: {$list[list].histo_order}" src="img/icons/trash.png" />
                     </a>
    </td>

    <td class="thin"><a href="javascript:popup('DocHistory.php?action=getComment&activity_id={$list[list].activity_id}&instance_id={$list[list].instance_id}&space={$CONTAINER_TYPE}','comments')" title="{tr}Get comment{/tr}">
                     <img border="0" alt="{tr}Get comment{/tr}" src="img/icons/comment.gif" />
                     </a>
    </td>

    <td class="thin">
    {$list[list].histo_order}<br />
    <b>{$list[list].action_name}</b> <i>by</i> <b>{$list[list].action_by}</b><br />
    {$list[list].action_date|date_format}
    {if !empty($list[list].comment)}
      <hr />
      <em>{tr}Comment{/tr} :<br />
      <b>{$list[list].comment}</b></em>
    {/if}
    {if !empty($list[list].from_document_id)}
      <hr />
      <a href="javascript:popupP('DocManage.php?documentDetail=1&document_id={$list[list].from_document_id}&space={$CONTAINER_TYPE}','documentDetailWindow',600,1024);">
        <em>{tr}from_document_id{/tr} :
        <b>{$list[list].from_document_id}</b></em>
      </a>
    {/if}
    </td>



    <td class="thin">
    <a class="link" href="javascript:popupP('DocManage.php?documentDetail=1&document_id={$list[list].document_id}&space={$CONTAINER_TYPE}','documentDetailWindow', 600 , 1024)">
    {$list[list].document_number} - {$list[list].document_indice_id|document_indice}.{$list[list].document_version} - in {$list[list].$CONTAINER_ID|container}</a>
    <br />
    <i>{$list[list].designation}</i>
    <br />
    {$list[list].doctype_id|type}
    <br />
    {$list[list].document_access_code|access_code} - {$list[list].document_state}
    </td>


    <td class="thin">
    {$list[list].activity_id}.{$list[list].instance_id}
    </td>

   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'histo_order[]',this.checked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
      //-->                     
      </script>
      <br>

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<br />

<button class="mult_submit" type="submit" name="action" value="suppress" title="{tr}Suppress{/tr}" 
 onclick="if(confirm('{tr}Do you want really suppress this history{/tr}')){ldelim}pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

<p>
</p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_mode" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="document_id" value="{$document_id}" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />

</form>

<p>
<form name="close">
  <input type="button" onclick="window.close()" value="Close">
</form>
</p>
