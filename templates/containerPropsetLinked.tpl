{*Smarty template*}

{include file="header.tpl"}

<div id="tiki-main">
<div id="tiki-top">
<div id="tiki-mid">
<div id="tiki-center">

<form name="close"><input type="button" onclick="window.close()" value="Close">
<a href="DocMetadata.php?action=modify&field_name={$list[list].field_name}&space={$CONTAINER_TYPE}" title="{tr}edit{/tr}">
<img border="0" alt="{tr}edit{/tr}" src="img/icons/edit.png" />{tr}Jump to metadatas manager{/tr}</a>
<hr />
</form>

<h2>{tr}Linked properties set to{/tr} {$container_number}</h2>

{* -------------------Pagination------------------------ *}
{* {include file='pagination.tpl'} *}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=link_id">
   {tr}link_id{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=propset_id">
   {tr}propset_id{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=propset_name">
   {tr}propset_name{/tr}</a></th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="link_id[]" value="{$list[list].link_id}" /></td>

    {*-Specifics fields-*}
    <td class="thin">{$list[list].link_id}</td>
    <td class="thin">{$list[list].propset_id}</td>
    <td class="thin">{$list[list].propset_name}</td>
   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}
{*{include file='pagination.tpl'}*}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
<script language='Javascript' type='text/javascript'>
<!--
// check / uncheck all.
// in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
// for now those people just have to check every single box
document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'link_id[]',this.checked)\"/></td>");
document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
//-->                     
</script>

<br>
{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="linkPropset" title="{tr}Link propset{/tr}" id="02">
<img class="icon" src="./img/icons/metadata/link.png" title="{tr}Link propset{/tr}" alt="{tr}Link propset{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="unlinkPropset" title="{tr}Unlink propset{/tr}" id="01">
<img class="icon" src="./img/icons/metadata/unlink.png" title="{tr}Unlink propset{/tr}" alt="{tr}Unlink propset{/tr}" width="16" height="16" />
</button>

<hr />

{*Submit checkform form*}

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
</form>


<form name="close">
  <input type="button" onclick="window.close()" value="Close">
</form>

</div>
</div>
</div>
</body>
