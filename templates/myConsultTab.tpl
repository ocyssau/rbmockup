{*Smarty template*}

<ul id="tabnav">
  {if isset($SelectedProduct)}
  <li class="{$productTab}"><a href="ProductManage.php?space={$SelectedContainerType}">{$SelectedProduct}</a></li>
  {/if}
  {if isset($SelectedContainer)}
  <li class="{$documentManage}"><a href="DocManage.php?space={$SelectedContainerType}&docfileManage=0">{$SelectedContainer}</a></li>
    {if $DisplayDocfileTab}
      <li class="{$docfileManage}"><a href="DocManage.php?space={$SelectedContainerType}&docfileManage=1">{$SelectedContainer}_Files</a></li>
    {/if}
  {/if}
  <li class="{$wildspaceTab}"><a href="wildspace.php" >{tr}Wildspace{/tr}</a></li>

  {if isset($SelectedContainer)}
    <li class="{$containerConsultTab}"><a href="containerConsult.php">{tr}Consultation{/tr}</a></li>
  {/if}

  {if $containerConsultTab=='active'}{include file='myConsultTabs.tpl'}{/if}

</ul>
