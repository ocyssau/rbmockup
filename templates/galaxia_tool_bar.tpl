{*Smarty template*}

<div id="page-bar">
  <h3>Actions</h3>
  <span class="button2"><a class="linkbut" href="galaxia_admin_processes.php">{tr}Admin process{/tr}</a></span>
  <span class="button2"><a class="linkbut" href="galaxia_monitor_processes.php">{tr}Monitor process{/tr}</a></span>
  <span class="button2"><a class="linkbut" href="galaxia_monitor_activities.php">{tr}Monitor activities{/tr}</a></span>
  <span class="button2"><a class="linkbut" href="galaxia_monitor_instances.php">{tr}Monitor instances{/tr}</a></span>
  <span class="button2"><a class="linkbut" href="galaxia_user_processes.php">{tr}User process{/tr}</a></span>
  <span class="button2"><a class="linkbut" href="galaxia_user_activities.php">{tr}User activities{/tr}</a></span>
  <span class="button2"><a class="linkbut" href="galaxia_user_instances.php">{tr}User instance{/tr}</a></span>
</div>
