{*Smarty template*}

{include file="header.tpl"}

<div id="tiki-main">

  <div id="tiki-top">
    {include file="tiki-top_bar.tpl"}
    <br/>
  </div>

  <div id="tiki-mid">
    {include file="tabs.tpl"}
    <br>

    <div id="tiki-center">
      {if $tool_bar}
        <div id="wikitopline">{include file="$tool_bar"}</div>
      {/if}
      {if $mid}
        {include file=$mid}
      {/if}
      {if $literalContent}
        {$literalContent}
      {/if}
    </div>
  </div>

  <div id="tiki-bot">
    {include file="tiki-bot_bar.tpl"}
  </div>

</div>
</body>
