{*Smarty template*} 

{include file="header.tpl"}

<div id="tiki-main">
  <div id="tiki-top">
    {include file="tiki-top_bar.tpl"}
  </div>
  <div id="tiki-mid">
    <div id="tiki-center">


<h1 class="pagetitle">{tr}Create a new job{/tr}</h1>
<div>Utilisez la souris pour faire glisser les fichiers dans l'ordre souhait�.</div>

<form name="close"><input type="button" onclick="window.close()" value="Close"></form>

{literal}
<link rel="stylesheet" href="js/jqueryui/css/base/jquery-ui.css" type="text/css" media="all" />
<link rel="stylesheet" href="js/jqueryui/css/base/jquery.ui.theme.css" type="text/css" media="all" />
<link rel="stylesheet" href="js/jqgrid/css/ui.jqgrid.css" type="text/css" media="all" />

<style>
	#jobbatch_packagelist { list-style-type: none; margin: 0; padding: 0; width: 100%; }
	#jobbatch_packagelist li { margin: 0 3px 3px 3px; padding: 5px; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
	#jobbatch_packagelist li span { position: absolute; margin-left: -1.3em; }
	
	#jobbatch_packagelist .ui-selecting { background: #FECA40; }
	#jobbatch_packagelist .ui-selected { background: #F39814; color: white; }
</style>

<script type="text/javascript" src="js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="js/jqueryui/jquery-ui.custom.min.js"></script>

<script src="js/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jqgrid/jquery.jqGrid.min.js"></script>
<script type="text/javascript">
	jQuery.jgrid.no_legacy_api = true;
</script>

<script type="text/javascript" src="js/jquery/jquery.json.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$( "#mockups-select-win" ).hide();
		$( "#jobbatch_packagelist" ).sortable();
		$( "#jobbatch_packagelist" ).disableSelection();
		
		$( "#button-ok" ).bind('click', null, function(e){
			myArray = new Array();
			$.each($('#jobbatch_packagelist li'), function(index, li){
				packageId = li.children.item(1).attributes.getNamedItem('value').value;
				targetId = li.children.item(4).attributes.getNamedItem('targetId').value;
				targetName = li.children.item(4).attributes.getNamedItem('targetName').value;
				myArray.push( new Array(packageId, targetId) );
			});
			
			//alert( myArray );
			
			//$.ajax({
			//	type: "POST",
			//	url: '{/literal}{$smarty.server.PHP_SELF}{literal}?action=createbatchjob',
			//	data: 'data-jsonencoded=' + $.JSON.encode(myArray),
			//	success: function(data, textStatus, jqXHR){
			//		alert('SUCCESS');
			//	},
			//	error: function(data, textStatus, jqXHR){
			//		alert('FAILED');
			//	}
			//});
			
			//$.get(
			//	'{/literal}{$smarty.server.PHP_SELF}{literal}?action=createbatchjob',
			//	'data-jsonencoded=' + $.JSON.encode(myArray),
			//	function(data, textStatus, jqXHR){
			//		alert('SUCCESS');
			//	}
			//);
			
			$('#mockups-create-result').load(
				'{/literal}{$smarty.server.PHP_SELF}{literal}?action=createbatchjob',
				'data-jsonencoded=' + $.JSON.encode(myArray),
				function(data, textStatus, jqXHR){
				}
			);
		});

		$("#jobbatch_packagelist li p.tid").bind('click', null, function(e){
			var li = this.parentNode;
			$( "#mockups-select-win" ).dialog({title:'Select a target', width:1000, buttons: [{
				text: "Ok",
				click: function(e) { 
					//get the selected line id of grid
					var selrow = jQuery('#mockups').jqGrid('getGridParam', 'selrow');
					//alert(selrow);
					//get name of the target
					targetName = jQuery('#mockups').getCell(selrow, 1);
					targetId = jQuery('#mockups').getCell(selrow, 0);
					cbAllSame = jQuery('#cb-mockups-select-allsame').get(0).checked;
					if(targetName != false){
						if(cbAllSame == false){
							targetElemt = li.children.item(4);
							targetElemt.innerHTML = '<b>' + targetName + '</b>';
							targetElemt.attributes.getNamedItem('targetId').value = targetId;
							targetElemt.attributes.getNamedItem('targetName').value = targetName;
						}
						else{
							$.each( jQuery('.jobbatch-packagelist-li'), function(index, item){
								targetElemt = item.children.item(4);
								targetElemt.innerHTML = '<b>' + targetName + '</b>';
								targetElemt.attributes.getNamedItem('targetId').value = targetId;
								targetElemt.attributes.getNamedItem('targetName').value = targetName;
								} )
						}
					}
					$(this).dialog("close"); 
					}}]});
		});
		{/literal}
        var mydata = [
					{section name=mockups loop=$mockups}
						[ "{$mockups[mockups].mockup_id}","{$mockups[mockups].mockup_number}","{$mockups[mockups].mockup_description}" ],
					{/section}
					["", "", ""]
                  ];
        {literal}
		$('#mockups').jqGrid({ 
			datatype: 'local',
			data: mydata,
			colNames:['Id','Number', 'Description'],
			colModel:[
					{name:'id',index:'id', width:20},
					{name:'number',index:'number', width:300},
					{name:'description',index:'description', width:600}
					],
			sortname: 'number',
			sortorder: 'desc',
			viewrecords: true,
			caption:'Mockups',
			multiselect: false,
			autowidth: false,
			rowNum: 1000,
			afterInsertRow: function(rowid, rowdata, rowelem){
			},
			ondblClickRow: function(ids) {
			},
            localReader: {
                repeatitems: true,
                cell: "",
                id: 0
            }
			});
	});
</script>
{/literal}


<ul id="jobbatch_packagelist">
{section name=list loop=$list}
	<li class="ui-state-default jobbatch-packagelist-li">
		<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
		<p class="pid" style="display:inline" value="{$list[list].import_order}">[{$list[list].import_order}]</p>
		<p class="pname" style="display:inline" value="{$list[list].package_file_name}">{$list[list].package_file_name}</p>
		<p class="description" style="display:inline" value="{$list[list].description}">{$list[list].description}</p>
		<p class="tid" style="display:inline" id="starget-{$list[list].import_order}" targetName="" targetId="">Click to select target</p>
	</li>
{sectionelse}
	<li>{tr}No containers to display{/tr}</li>
{/section}
</ul>

<div id="mockups-select-win">
	<table id="mockups"></table>
	<input type="checkbox" id="cb-mockups-select-allsame">Check all items with same</input>
</div>

<div id="mockups-create-result" class="rbbox">
</div>


<button id="button-ok">OK</button>
<button id="button-cancel">Cancel</button>




  </div>
  </div>
  <div id="tiki-bot">
    {include file="tiki-bot_bar.tpl"}
  </div>
</div>
</body></html>
