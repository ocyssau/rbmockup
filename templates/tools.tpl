{*Smarty template*}

  <div id="BoiteActions">
     <p><span class="button2"><a class="linkbut" href="#" onCLick="javascript:window.open('./Docs/API_ranchbe/html/index.html', 'Doc API RanchBE');return false;">{tr}Ranchbe API documentation{/tr}</a></span></p>
     <p><span class="button2"><a class="linkbut" href="#" onCLick="javascript:window.open('./Docs/API_galaxia/html/index.html', 'Doc API Galaxia');return false;">{tr}Galaxia API documentation{/tr}</a></span></p>
     <!--<p><span class="button2"><a class="linkbut" href="javascript:popup('tools.php?action=GetFilesRecordWithoutFile','GetFilesRecordWithoutFile')">{tr}Find all records wich have not real file{/tr}</a></span></p>-->
     <!--<p><span class="button2"><a class="linkbut" href="javascript:popup('tools.php?action=GetFilesWithoutDocuments','GetFilesWithoutDocuments')">{tr}Find all records wich have not associated document{/tr}</a></span><br /></p>-->
     <!--<p><span class="button2"><a class="linkbut" href="javascript:popup('tools.php?action=GetDocumentsWithoutFile','GetDocumentsWithoutFile')">{tr}Find all documents wich have not associated file{/tr}</a></span><br /></p>-->
     <!--<p><span class="button2"><a class="linkbut" href="javascript:popup('tools.php?action=GetDocumentsWithoutContainer','GetDocumentsWithoutContainer')">{tr}Find all documents wich have not associated container{/tr}</a></span></p>-->
     <!--<p><span class="button2"><a class="linkbut" href="javascript:popup('tools.php?action=ExportRights','ExportRights')">{tr}Export rights to file{/tr}</a></span></p>-->
     <p><span class="button2"><a class="linkbut" href="tools.php?action=BackupDatabase&BackupDatabaseType=SQL">{tr}Backup of database in SQL format{/tr}</a></span></p>
     <p><span class="button2"><a class="linkbut" href="tools.php?action=BackupDatabase&BackupDatabaseType=XML">{tr}Backup of database in XML format{/tr}</a></span></p>
     <p><span class="button2"><a class="linkbut" href="javascript:popup('tools.php?action=GenRightsTable','GenRightsTable')">{tr}Generate right table{/tr}</a></span></p>
     <p><span class="button2"><a class="linkbut" href="javascript:popup('tools.php?action=GenDoctypes','GenDoctypes')">{tr}Generate doctype table{/tr}</a></span></p>
     <p><span class="button2"><a class="linkbut" href="javascript:popup('tools.php?action=GetFilesInTrash','GetDocumentsWithoutContainer')">{tr}Get files in trash{/tr}</a></span></p>
     <!--<p><span class="button2"><a class="linkbut" href="javascript:popup('tools.php?action=EmptyTrash','GetDocumentsWithoutContainer')">{tr}Empty trash{/tr}</a></span></p>-->
     <p><span class="button2"><a class="linkbut" href="javascript:popup('tools.php?action=GetConnectedUsers','GetConnectedUsers')">{tr}List connected users{/tr}</a></span></p>
     <p><span class="button2"><a class="linkbut" href="javascript:popup('tools.php?action=AddRights','AddRights')">{tr}Add rights{/tr}</a></span></p>
     <p><span class="button2"><a class="linkbut" href="javascript:popup('tools.php?action=UpdateDoctypeIcon','UpdateDoctypeIcon')">{tr}Update doctypes icon{/tr}</a></span></p>
     <p><span class="button2"><a class="linkbut" href="tools.php?action=sql_profiling">{tr}See SQL profiling{/tr}</a></span></p>
     <!--<p><span class="button2"><a class="linkbut" href="tools.php?request_page=renameNormesAirbus">{tr}Renommer les normes{/tr}</a></span></p>-->
     <p><span class="button2"><a class="linkbut" href="tools.php?request_page=SQL_request">{tr}SQL Request{/tr}</a></span></p>
     <p><span class="button2"><a class="linkbut" href="javascript:popupP('tools.php?action=getcustom','custom_sctipts', 1024, 800)">{tr}Custom scripts{/tr}</a></span></p>
     <p><span class="button2"><a class="linkbut" href="javascript:popupP('tools.php?action=phpinfo','phpinfo', 1024, 800)">{tr}PHP Infos{/tr}</a></span></p>
     <p><span class="button2"><a class="linkbut" href="javascript:popup('./lib/testRegExpr.htm','testRegExpr')">{tr}Regexp tester{/tr}</a></span></p>
  </div>
