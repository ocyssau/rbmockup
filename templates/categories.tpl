{*Smarty template*}

{include file="header.tpl"}

{$form.javascript}

<h1 class="pagetitle">{tr}Categories{/tr}</h1>
<form name="close"><input type="button" onclick="window.close()" value="Close"></form>

{*--------------------Search Bar defintion--------------------------*}
{include file='searchBar_simple.tpl'}
<form id="resetf" action="{$smarty.server.PHP_SELF}" method="post">
  <input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
  <input type="submit" name="resetf" value="{tr}Reset all filters{/tr}" />
</form>

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{*--------------------list header----------------------------------*}

<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>
  <th class="heading auto"></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=category_number">
   {tr}Name{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=category_description">
   {tr}Description{/tr}</a></th>
  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=category_icon">
   {tr}Icon{/tr}</a></th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].category_id}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

    <td class="thin"><a href="Category.php?action=modify&category_id={$list[list].category_id}&space={$CONTAINER_TYPE}" title="{tr}edit{/tr}">
                     <img border="0" alt="{tr}edit{/tr}" src="img/icons/edit.png" /></a>
    </td>

    {*-Specifics fields-*}
    <td class="thin">{$list[list].category_number}</td>
    <td class="thin">{$list[list].category_description}</td>
    <td class="thin">{$list[list].category_icon}</td>
   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}

{include file='pagination.tpl'}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
<script language='Javascript' type='text/javascript'>
<!--
// check / uncheck all.
// in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
// for now those people just have to check every single box
document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/></td>");
document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr} : </label></td></tr>");
//-->                     
</script>

<br>
{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="suppressCat" title="{tr}Suppress{/tr}" 
 onclick="if(confirm('{tr}Do you want really suppress this category{/tr}')){ldelim}pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
</form>

<hr />

{*Create form*}

<form {$form.attributes}>
{$form.hidden}
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />

{if $action == 'modify'}
  <h2>{tr}Modify a category{/tr}
  <span class="button2">
  <a class="linkbut" href="Category.php?action=create&space={$CONTAINER_TYPE}">
  {tr}Return to create{/tr}</a></span>
  </h2>

{else}
  <h2>{tr}Create a category{/tr}</h2>
{/if}

<table class="normal">

  <tr>
    <td class="formcolor">{tr}{$form.category_number.label}{/tr}:</td>
    <td class="formcolor">{$form.category_number.html}</td>
  </tr>

  <tr>
    <td class="formcolor">{tr}{$form.category_description.label}{/tr}:</td>
    <td class="formcolor">{$form.category_description.html}</td>
  </tr>

 {if not $form.frozen}
  <tr>
    <td class="formcolor"> </td>
    <td class="formcolor">{$form.reset.html}&nbsp;{$form.action.html}</td>
  </tr>
 {/if}

  <tr>
    <td class="formcolor">{tr}{$form.requirednote}{/tr}</td>
  </tr>

</table>
</form>

<form name="close"><input type="button" onclick="window.close()" value="Close"></form>

<br />

<b>Collected Errors:</b><br />
  {foreach key=name item=error from=$form.errors}
      <font color="red">{$error}</font> in element [{$name}]<br />
  {/foreach}
