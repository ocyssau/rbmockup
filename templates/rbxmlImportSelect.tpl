{*Smarty template*}

{include file="header.tpl"}

{$form.javascript}

<div id="tiki-main">

<div id="tiki-top">{include file="tiki-top_bar.tpl"}</div>

<div id="tiki-mid">{include file="tabs.tpl"} <br>

<div id="tiki-center">

<h1>Select</h1>

{*--------------------list header----------------------------------*}
<form name="step2" method="post" action="{$smarty.server.PHP_SELF}">

{foreach item=field_name from=$fields} <input type="hidden"
	name="fields[]" value="{$field_name}" /> {/foreach}

<table class="normal">
	<tr>
		<th>	
			<input name="switcher" id="clickall" type="checkbox" onclick="switchCheckboxes(this.form,'files[]',this.checked);"/>
		</th>
		<th>File</th>
	</tr>
	{*--------------------list Body----------------------------------*}
	{cycle print=false values="even,odd"}
	{foreach item=filename from=$list}
		<tr class="{cycle}">
			<td><input type="checkbox" name="files[]" value="{$filename}" /></td>
			<td>{$filename}</td>
		</tr>
	{/foreach}
</table>

<input type="submit" value="batchrun" name="action" />
<input type="submit" value="cancel" name="action" />
<input type="hidden" name="page_id" value="documentImportRbXml" />

</form>

</div>
</div>

<div id="tiki-bot">{include file="tiki-bot_bar.tpl"}</div>

</div>
