{*Smarty template*}

{literal}
<script language='Javascript' type='text/javascript'>
<!--
function FreezeNameInput(monlien,title){
  var freezeName = prompt(title,'');
  if(freezeName == null){
    document.location.href = '#';
    return false;
  }
  monlien.search = monlien.search+'&freezeName='+freezeName;
  //alert(monlien.href);
  document.location.href = monlien.href;
  return true;
}

function FreezeConfirm(monlien,title){
  if( confirm(title) ){
    document.location.href = monlien.href;
    return true;
  }else{
    document.location.href = '#';
    return false;
  }
}
//-->
</script>



<script language='Javascript' type='text/javascript'>
<!--
function InputPostit(monlien,comment,title){
  var newComment = prompt( title, comment );
  if(newComment == null){
    document.location.href = '#';
    return false;
  }
  monlien.search = monlien.search+'&newComment='+newComment;
  //alert(monlien.href);
  document.location.href = monlien.href;
  return true;
}
//-->
</script>


{/literal}

<div id="page-bar">
    <span class="button2"><a class="linkbut" href="DocManage.php?space={$CONTAINER_TYPE}&docfileManage=1">{tr}Files manager{/tr}</a></span>
    <span class="button2"><a class="linkbut" href="javascript:popup('DocHistory.php?space={$CONTAINER_TYPE}','DocumentHistory')">{tr}History of all{/tr}</a></span>
    <span class="button2"><a class="linkbut" href="ImportManager.php?space={$CONTAINER_TYPE}">{tr}Import{/tr}</a></span>
    <span class="button2">
    <a class="linkbut" href="DocManage.php?space={$CONTAINER_TYPE}&docfileManage=0&ticket={$ticket}&action=freezeAll"
     onclick="FreezeConfirm(this,'{tr}Do want really freeze all documents{/tr}');return false;">
    {tr}Freeze all{/tr}
    </a>
    </span>
</div>

<div id="page-title">
  <h1 class="pagetitle">{tr}Documents manager{/tr}</h1>
</div>

{*--------------------Search Bar defintion--------------------------*}
{*assign var="histoOption" value="1"*}
{include file='searchBar_doc.tpl'}
{*include file='searchBar.tpl'*}

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto">
	{*check / uncheck all*}
	<input name="switcher" id="clickall" type="checkbox" onclick="switchCheckboxes(this.form,'checked[]',this.checked);"/>
	<div id="displaySelectedRowCount"></div>
  </th>
  <th class="heading auto">
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=doctype_id">
  {tr}Doctype{/tr}</a><br />
  </th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=document_number">
  {tr}Number{/tr}</a> - 
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=document_indice_id">
  {tr}Indice{/tr}</a>.
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=document_version">
  {tr}Version{/tr}</a>
  <br />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=designation">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>{tr}Designation{/tr}</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
  <br />
  <a 
  {popup text='<b>Access code legend</b> :<ul>
  <li><b>0</b>:All access free</li>
  <li><b>1</b>:Modification in progress, checkout forbidden</li>
  <li><b>5</b>:workflow in progress, checkout forbidden</li>
  <li><b>10</b>:Document validate, checkout and workflow request forbidden</li>
  <li><b>11</b>:Lock by user, checkout and workflow request forbidden</li>
  <li><b>15</b>:Indice is upgraded, all action forbidden</li></ul>'}
  class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=document_access_code">
  {tr}Access{/tr}</a> -
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=document_state">
  {tr}State{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=category_id">
  {tr}Category{/tr}</a></th>

  <th class="heading">
  {tr}CheckOut{/tr}
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=check_out_by">
  {tr}By{/tr}</a> - 
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=check_out_date">{tr}Date{/tr}</a>
  <br />
  {tr}Last Update{/tr}
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=update_by">
  {tr}By{/tr}</a> - 
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=update_date">{tr}Date{/tr}</a>
  <br />
  {tr}Created{/tr}
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=open_by">
  {tr}By{/tr}</a> - 
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=open_date">{tr}Date{/tr}</a>
  </th>


  <!-- Display optionnals fields -->
  {section name=of loop=$optionalFields}
    <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field={$optionalFields[of].field_name}">
   {tr}{$optionalFields[of].field_description}{/tr}</a></th>
  {/section}
 </tr>

{*--------------------list Body----------------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}

  {section name=list loop=$list}
  
  {if $list[list].document_access_code > 14 && $list[list].document_access_code < 100}
   	<tr class="historical">
  {else}
   	<tr class="{cycle}">
   {/if}
   
    <td class="thin">
    <input type="checkbox" name="checked[]" value="{$list[list].document_id}" id="checkbox_tbl_{$smarty.section.list.index}" {if $list[list].checked eq 'y'}checked="checked" {/if} /><br />
    {$list[list].document_access_code|access_code_concis}</td>

    <td class="thin">
    <a href="viewFile.php?document_id={$list[list].document_id}&ticket={$ticket}&space={$CONTAINER_TYPE}" title="{tr}View document{/tr}">
    <img border="0" alt="no icon" src="{$icons_dir}/C/{$list[list].doctype_id}.gif" />
    {if $displayThumbs}
      {get_thumbnail imagefile_path=$thumbs_dir|cat:'/'|cat:$list[list].document_id|cat:$thumbs_extension}
    {/if}
    </a>
    <font size="-3"><br /><i>{$list[list].doctype_id|type}</i></font>
    </td>

    <td class="thin">
<div style="white-space: nowrap;">
    <a class="link" href="viewFile.php?document_id={$list[list].document_id}&ticket={$ticket}&space={$CONTAINER_TYPE}" title="{tr}View document{/tr}">
    {$list[list].document_number} - {$list[list].document_indice_id|document_indice}.{$list[list].document_version}</a>{get_doc_postit document_id=$list[list].document_id space=$CONTAINER_TYPE}

    <a href="javascript:popupP('DocManage.php?documentDetail=1&document_id={$list[list].document_id}&space={$CONTAINER_TYPE}','documentDetailWindow', 600 , 1024)">
    {tr}See details{/tr}
    </a>
<br />
</div>

    <i>{$list[list].designation}</i><br />
    {$list[list].document_access_code|access_code} - {$list[list].document_state}<br />
    
    
	<div style="text-wrap:none; white-space: nowrap">
		<a
			href="javascript:popupP('DocHistory.php?action=getHistory&document_id={$list[list].document_id}&space={$CONTAINER_TYPE}&page_id=DocManage_history_{$list[list].document_id}','history', 1024 , 750)"
			title="{tr}Get history{/tr}"> <img border="0"
			alt="{tr}Get history{/tr}" src="img/icons/history.png" /> </a> 
		<a
			href="javascript:popupP('DocManage.php?action=modifyDoc&checked[]={$list[list].document_id}&page_id=DocManage_edit&space={$CONTAINER_TYPE}','DocumentManage', 700 , 600)"
			title="{tr}edit{/tr}"> <img border="0" alt="{tr}edit{/tr}"
			src="img/icons/edit.png" /> </a>

		{if ($list[list].document_access_code == 12) } <!-- No action displayed for historical documents-->
		<a
			href='DocManage.php?action=SuppressDoc&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}&ticket={$ticket}')><img
			border=0 src=./img/icons/trash.png title="{tr}Suppress{/tr}" /></a> 
		<a
			href='DocManage.php?action=UnMarkToSuppressDoc&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}&ticket={$ticket}')><img
			border=0 src=./img/icons/document/unMarkToSuppress.png
			title="{tr}Unmark to suppress{/tr}" alt="unMarkToSuppress" /></a> 
		{else} 
		
		{if ( $list[list].document_access_code < 15 ) } <!-- No action displayed for historical documents-->
			<a
				href="DocManage.php?action=ChangeState&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}&page_id=DocManage_changestate&step=step1&ticket={$ticket}"
				title="{tr}Change state{/tr}"> <img border="0"
				alt="{tr}Change state{/tr}"
				src="./img/icons/document/document_gear.png" /></a>
			<a
				href="javascript:popupP('DocManage.php?action=ChangeIndice&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}&page_id=DocManage_changeindice','ChangeIndice', 300 , 400)"
				title="{tr}Change indice{/tr}"> <img border="0"
				alt="{tr}Change indice{/tr}"
				src="./img/icons/document/document_upindice.png" /></a>
			<a
				href="CommentManage.php?action=addComment&document_id={$list[list].document_id}&space={$CONTAINER_TYPE}&ticket={$ticket}"
				onClick="InputPostit(this,'','Input comment');return false;"><img
				border=0 src=./img/icons/comment/comment_add.png
				title="{tr}Add a postit{/tr}" /></a>
		{/if}


	<!--Contextual menu defined with overlib librairy -->
    <span class="button2" style="white-space:nowrap;"><a 
    {capture name=some_actions assign=popText}
      <ul>
        {if $list[list].check_out_by}
        <li><a href='DocManage.php?action=CheckInDoc&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}&ticket={$ticket}'><img border=0 src=./img/icons/document/document_in.png />{tr}CheckIn{/tr}</a></li>
        <li><a href='DocManage.php?action=UpdateDoc&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}&ticket={$ticket}'><img border=0 src=./img/icons/document/document_refresh.png />{tr}Update{/tr}</a></li>
        <li><a href='DocManage.php?action=ResetDoc&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}&ticket={$ticket}'><img border=0 src=./img/icons/document/document_cancel.png />{tr}Reset{/tr}</a></li>
        {else}
	        {if ($list[list].document_access_code == 0) }
	          <li><a href='DocManage.php?action=CheckOutDoc&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}&ticket={$ticket}'><img border=0 src=./img/icons/document/document_out.png />{tr}CheckOut{/tr}</a></li>
	          <li><a href='DocManage.php?action=LockDoc&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}&ticket={$ticket}'><img border=0 src=./img/icons/lock.png />{tr}Lock{/tr}</a></li>
	        {/if}
	        
	        {if (($list[list].document_access_code > 5) && ($list[list].document_access_code < 15) ) }
	          <li><a href='DocManage.php?action=UnLockDoc&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}&ticket={$ticket}'><img border=0 src=./img/icons/lock_open.png />{tr}Unlock{/tr}</a></li>
	        {/if}
	        
	        {if ( $list[list].document_access_code < 15 ) }
	          <li><a href='DocManage.php?action=assocFile&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}&ticket={$ticket}'><img border=0 src=./img/icons/file/file_attach.png />{tr}Associate a file{/tr}</a></li>
	        {/if}
	        
        {/if}
        <li><a href='DocManage.php?action=putInWs&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}&ticket={$ticket}'><img border=0 src=./img/icons/document/document_read.png />{tr}Put in wildspace{/tr}</a></li>
        <li><a href='DocManage.php?action=reset_doctype&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}&ticket={$ticket}'><img border=0 src=./img/icons/doctype/doctype_reset.png />{tr}Recalculate the doctype{/tr}</a></li>
        <li><a href='DocManage.php?action=convert&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}&ticket={$ticket}'><img border=0 src=./img/icons/document/document_convert.png />{tr}Convert{/tr}</a></li>
        {if ($list[list].document_access_code == 0) }
          <li><a href='DocManage.php?action=SuppressDoc&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}&ticket={$ticket}')><img border=0 src=./img/icons/trash.png />{tr}Suppress{/tr}</a></li>
          <li><a href='DocManage.php?action=MarkToSuppressDoc&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}&ticket={$ticket}')><img border=0 src=./img/icons/document/markToSuppress.png />{tr}Mark to suppress{/tr}</a></li>
        {/if}
        {if ($list[list].document_access_code < 100) }
        	<li><a href='DocManage.php?action=Archive&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}&ticket={$ticket}')><img border=0 src=./img/icons/document/toarchive.png />{tr}DocToArchive{/tr}</a></li>
        {/if}
      </ul>
    {/capture}

    $list[list].document_access_code

    {popup text=$popText
    sticky=1 caption=`$list[list].document_number` trigger='onClick' fgcolor=white closeclick=1}

    <!--{*
    {popup text="<ul>
      <li><a href='DocManage.php?action=CheckOutDoc&checked[]=`$list[list].document_id`&space=`$CONTAINER_TYPE`&ticket=`$ticket`'><img border=0 src=./img/icons/document/document_out.png />$CheckOut</a></li>
      <li><a href='DocManage.php?action=CheckInDoc&checked[]=`$list[list].document_id`&space=`$CONTAINER_TYPE`&ticket=`$ticket`'><img border=0 src=./img/icons/document/document_in.png />$CheckIn</a></li>
      <li><a href='DocManage.php?action=UpdateDoc&checked[]=`$list[list].document_id`&space=`$CONTAINER_TYPE`&ticket=`$ticket`'><img border=0 src=./img/icons/document/document_refresh.png />$Update</a></li>
      <li><a href='DocManage.php?action=ResetDoc&checked[]=`$list[list].document_id`&space=`$CONTAINER_TYPE`&ticket=`$ticket`'><img border=0 src=./img/icons/document/document_cancel.png />$CancelCheckOut</a></li>
      <li><a href='DocManage.php?action=putInWs&checked[]=`$list[list].document_id`&space=`$CONTAINER_TYPE`&ticket=`$ticket`'><img border=0 src=./img/icons/document/document_read.png />$PutInWildspace</a></li>
      <li><a href='DocManage.php?action=assocFile&checked[]=`$list[list].document_id`&space=`$CONTAINER_TYPE`&ticket=`$ticket`'><img border=0 src=./img/icons/file/file_attach.png />$AssociateFile</a></li>
      <li><a href='DocManage.php?action=LockDoc&checked[]=`$list[list].document_id`&space=`$CONTAINER_TYPE`&ticket=`$ticket`'><img border=0 src=./img/icons/lock.png />$Lock</a></li>
      <li><a href='DocManage.php?action=UnLockDoc&checked[]=`$list[list].document_id`&space=`$CONTAINER_TYPE`&ticket=`$ticket`'><img border=0 src=./img/icons/lock_open.png />$Unlock</a></li>
      <li><a href='DocManage.php?action=reset_doctype&checked[]=`$list[list].document_id`&space=`$CONTAINER_TYPE`&ticket=`$ticket`'><img border=0 src=./img/icons/doctype/doctype_reset.png />$RecalculateDoctype</a></li>
      <li><a href=&quot;DocManage.php?action=convert&checked[]=`$list[list].document_id`&space=`$CONTAINER_TYPE`&ticket=`$ticket`&quot;)><img border=0 src=./img/icons/document/document_convert.png />$Convert</a></li>
      <li><a href=&quot;DocManage.php?action=suppress&checked[]=`$list[list].document_id`&space=`$CONTAINER_TYPE`&ticket=`$ticket`&quot;)><img border=0 src=./img/icons/trash.png />$Suppress</a></li>
    </ul>"
    sticky=1 caption=`$list[list].document_number` trigger='onClick' fgcolor=white closeclick=1}
    *}-->

    <b>{tr}More actions{/tr}</b></a></span>
    {/if}
</div>
    </td>

    <td class="thin">{$list[list].category_id|category}</td>

    <td class="thin" nowrap>
      {if !empty($list[list].check_out_by)}
      <b>{tr}Checkout{/tr} : </b>{$list[list].check_out_by|username} - {$list[list].check_out_date|date_format}
      <br />{/if}
      <b>{tr}Last Update{/tr} : </b>{$list[list].update_by|username} - {$list[list].update_date|date_format}
      <br />
      <b>{tr}Created{/tr} : </b>{$list[list].open_by|username} - {$list[list].open_date|date_format}
    </td>

    <!-- Display optionnals fields -->
    {foreach key=key item=type from=$optionalFields}
      {assign var="fn" value=$type.field_name}
      <td class="thin">{filter_select id=$list[list].$fn type=$type.field_type}</td>
    {/foreach}

   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}

{include file='pagination.tpl'}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
{*
<script language='Javascript' type='text/javascript'>
<!--
// check / uncheck all.
// in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
// for now those people just have to check every single box
document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/></td>");
document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
//-->                     
</script>
<br>
*}
<!--
{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />
<i>{tr}Perform action{/tr} :</i>
-->

</div>
</div>
<div id="action-menu">

<button class="mult_submit" type="submit" name="action" value="createDoc" title="{tr}Create a new document{/tr}"
 onclick="document.checkform.action='DocManage.php?page_id=DocManage_store&space={$CONTAINER_TYPE}'; pop_it(checkform, 340 , 550)">
<img class="icon" src="./img/icons/document/document_add.png" title="{tr}Create a new document{/tr}" alt="{tr}createDoc{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="SuppressDoc" title="{tr}Suppress{/tr}" 
 onclick="if(confirm('{tr}Do you want really suppress this documents{/tr}')){ldelim}document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/trash.png" title="{tr}Suppress{/tr}" alt="{tr}Suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="MarkToSuppressDoc" title="{tr}Mark to suppress{/tr}" 
 onclick="document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/markToSuppress.png" title="{tr}Mark to suppress{/tr}" alt="{tr}Mark to suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="UnMarkToSuppressDoc" title="{tr}Unmark to suppress{/tr}" 
 onclick="document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/unMarkToSuppress.png" title="{tr}Unmark to suppress{/tr}" alt="{tr}Unmark to suppress{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="CheckOutDoc" title="{tr}CheckOut{/tr}"
 onclick="document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_out.png" title="{tr}CheckOut{/tr}" alt="{tr}CheckOut{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="CheckInDoc" title="{tr}CheckIn{/tr}" id="03"
 onclick="document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_in.png" title="{tr}CheckIn{/tr}" alt="{tr}CheckIn{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="UpdateDoc" title="{tr}Update{/tr}" id="04"
 onclick="document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_refresh.png" title="{tr}Update{/tr}" alt="{tr}Update{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="ResetDoc" title="{tr}Reset{/tr}" id="05"
 onclick="document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_cancel.png" title="{tr}Reset{/tr}" alt="{tr}Reset{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="putInWs" title="{tr}Put in wildspace{/tr}" id="06"
 onclick="document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_read.png" title="{tr}Put in wildspace{/tr}" alt="{tr}Put in wildspace{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="freeze" title="{tr}Freeze{/tr}" id="20"
 onclick="document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_freeze.png" title="{tr}Freeze{/tr}" alt="{tr}Freeze{/tr}" width="16" height="16" />
</button>

<!--
<button class="mult_submit" type="submit" name="action" value="assocFile" title="{tr}Associate a file{/tr}"
 onclick="document.checkform.action='{$CONTAINER_TYPE}DocManage.php'; pop_no(checkform)">
<img class="icon" src="./img/icons/file/file_attach.png" title="{tr}Associate a file{/tr}" alt="{tr}assocFile{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="assocDocument" title="{tr}Reference to document{/tr}"
 onclick="document.checkform.action='{$CONTAINER_TYPE}DocManage.php'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_attach.png" title="{tr}Reference to document{/tr}" alt="{tr}assocDocument{/tr}" width="16" height="16" />
</button>
-->

<button class="mult_submit" type="submit" name="action" value="MoveDocument" title="{tr}Move{/tr}" id="09"
 onclick="document.checkform.action='DocManage.php?page_id=DocManage_movedoc&space={$CONTAINER_TYPE}'; pop_it(checkform, 350 , 280)">
<img class="icon" src="./img/icons/document/document_move.png" title="{tr}Move{/tr}" alt="{tr}Move{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="CopyDocument" title="{tr}Copy{/tr}" id="10"
 onclick="document.checkform.action='DocManage.php?page_id=DocManage_copydoc&space={$CONTAINER_TYPE}'; pop_it(checkform , 350 , 280)">
<img class="icon" src="./img/icons/document/document_copy.png" title="{tr}Copy{/tr}" alt="{tr}Copy{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="multi_ChangeIndice" title="{tr}Change indice{/tr}" id="11"
 onclick="if(confirm('{tr}Are you sure?{/tr}')){ldelim}document.checkform.action='DocManage.php?page_id=DocManage_changeindice&space={$CONTAINER_TYPE}'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/document/document_upindice.png" title="{tr}Change indice{/tr}" alt="{tr}Change indice{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="multi_ChangeState" title="{tr}Change state{/tr}" id="08"
 onclick="document.checkform.action='DocManage.php?page_id=DocManage_changestate&space={$CONTAINER_TYPE}&action=multi_ChangeState&step=step1&ticket={$ticket}'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_gear.png" title="{tr}Change state{/tr}" alt="{tr}Change state{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="LockDoc" title="{tr}Lock definitivly the document{/tr}"
 onclick="if(confirm('{tr}The lock is definitif. Are you sure?{/tr}')){ldelim}document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/lock.png" title="{tr}Lock definitivly the document{/tr}" alt="{tr}Lock{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="UnLockDoc" title="{tr}Unlock the document{/tr}"
 onclick="if(confirm('{tr}Are you sure?{/tr}')){ldelim}document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/lock_open.png" title="{tr}Unlock the document{/tr}" alt="{tr}Unlock{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="reset_doctype" title="{tr}Recalculate the doctype{/tr}" id="07"
 onclick="document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform)">
<img class="icon" src="./img/icons/doctype/doctype_reset.png" title="{tr}Recalculate the doctype{/tr}" alt="{tr}Recalculate the doctype{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="action" value="Archive" title="{tr}DocToArchive{/tr}" 
 onclick="if(confirm('{tr}Do you want really archive this documents{/tr}')){ldelim}document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform){rdelim}else {ldelim}return false;{rdelim}">
<img class="icon" src="./img/icons/document/toarchive.png" title="{tr}DocToArchive{/tr}" alt="{tr}DocToArchive{/tr}" width="16" height="16" />
</button>


<button class="mult_submit" type="submit" name="action" value="downloadListing" title="{tr}Download listing{/tr}"
 onclick="document.checkform.action='DocManage.php?page_id=DocManage_downloadcsv&space={$CONTAINER_TYPE}'; pop_no(checkform)">
<img class="icon" src="./img/icons/page_white_excel.png" title="{tr}Download listing{/tr}" alt="{tr}Download listing{/tr}" width="16" height="16" />
</button>

<button class="mult_submit" type="submit" name="refresh" value="refresh" title="{tr}Refresh{/tr}"
 onclick="document.checkform.action='DocManage.php?space={$CONTAINER_TYPE}'; pop_no(checkform)">
<img class="icon" src="./img/icons/refresh.png" title="{tr}Refresh{/tr}" alt="{tr}Refresh{/tr}" width="16" height="16" />
</button>

</div>

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_field|escape}" />
<input type="hidden" name="sort_order" value="{$sort_order|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="ticket" value="{$ticket}" />
</form>
