{*Smarty template*}

<h1 class="pagetitle">{tr}Context{/tr}</h1>

<div id="page-bar">
   <span class="button2"><a class="linkbut" href="context.php?action=GenerateCATIASearchOrder">{tr}Generate SearchOrder for CATIA{/tr}</a></span>
</div>

{if isset($searchOrder)}
  <br />
  <form action="context.php" method="post" name="saveSearchOrder" enctype="multipart/form-data">
  <fieldset>
  <legend align="top"><i>{tr}Search order for CATIA{/tr} :</i></legend>
  <i>Generate for <b>{$CLIENT_OS}</b> system</i>
  <pre>{$searchOrder}</pre>
  <input type="submit" value="save" name="action" />
  <input type="hidden" name="searchOrder" value="{$searchOrder}" />
  </fieldset>
  </form>
{/if}

<br />

<table class="normal">
<tr>
  <th>Area</th><td class="odd">{$area_id}</td>
  <th>Project</th><td class="odd">{$project_number}</td>
  <th>{$container_type}</th><td class="odd">{$container_number}</td>
</tr>
</table>

<br />

{*--------------------list header----------------------------------*}
<table class="normal">
<tr>

<th class="heading"><a class="tableheading" href="{$smarty.server.PHP_SELF}?offset={$offset}&numrows={$numrows}&sort_field=workitem_number&sort_order={if $sort_order eq 'DESC'}ASC{else}DESC{/if}">
{tr}{$HeaderCol1}{/tr}</a></th>

<th class="heading"><a class="tableheading" href="{$smarty.server.PHP_SELF}?offset={$offset}&numrows={$numrows}&sort_field=description&sort_order={if $sort_order eq 'DESC'}ASC{else}DESC{/if}">
{tr}{$HeaderCol2}{/tr}</a></th>

<th class="heading"><a class="tableheading" href="{$smarty.server.PHP_SELF}?offset={$offset}&numrows={$numrows}&sort_field=deposit_dir&sort_order={if $sort_order eq 'DESC'}ASC{else}DESC{/if}">
{tr}{$HeaderCol3}{/tr}</a></th>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
{section name=list loop=$list}
<tr class="{cycle}">

<td class="thin">{$list[list].$col1}</td>

<td class="thin">{$list[list].$col2}</td>

<td class="thin">{$list[list].$col3}</td>

</tr>
{/section}
</table>





<p>
<form name="close">
  <input type="button" onclick="window.close()" value="Close">
</form>
</p>

