{*Smarty template*} 
 
{include file=header.tpl}

{popup_init src="lib/overlib.js"}

<div id="tiki-main">
<div id="tiki-top">
<div id="tiki-mid">
<div id="tiki-center">

<h1 class="pagetitle">{tr}Metadatas list{/tr}</h1>


{*--------------------list header----------------------------------*}

<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto"></th>

  <th class="heading">{tr}Field{/tr}</th>
  <th class="heading">{tr}Description{/tr}</th>
  <th class="heading">{tr}Type{/tr}</th>
  <th class="heading">{tr}field_size{/tr}</th>
  <th class="heading">{tr}field_regex{/tr}</th>
  <th class="heading">{tr}field_required{/tr}</th>
  <th class="heading">{tr}field_multiple{/tr}</th>
  <th class="heading">{tr}return_name{/tr}</th>
  <th class="heading">{tr}field_where{/tr}</th>
  <th class="heading">{tr}field_list{/tr}</th>
  <th class="heading">{tr}table_name{/tr}</th>
  <th class="heading">{tr}field_for_value{/tr}</th>
  <th class="heading">{tr}field_for_display{/tr}</th>
  <th class="heading">{tr}date_format{/tr}</th>
 </tr>

{*--------------------list body---------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
   <tr class="{cycle}">
    <td class="thin"><input type="checkbox" name="checked[]" value="{$list[list].field_name}" {if $list[list].checked eq 'y'}checked="checked" {/if}/></td>

    {*-Specifics fields-*}
    <td class="thin">{$list[list].field_name}</td>
    <td class="thin">{$list[list].field_description}</td>
    <td class="thin">{$list[list].field_type}</td>
    <td class="thin">{$list[list].field_size}</td>
    <td class="thin">{$list[list].field_regex}</td>
    <td class="thin">{$list[list].field_required|yesorno}</td>
    <td class="thin">{$list[list].field_multiple|yesorno}</td>
    <td class="thin">{$list[list].return_name|yesorno}</td>
    <td class="thin">{$list[list].field_where}</td>
    <td class="thin">{$list[list].field_list}</td>
    <td class="thin">{$list[list].table_name}</td>
    <td class="thin">{$list[list].field_for_value}</td>
    <td class="thin">{$list[list].field_for_display}</td>
    <td class="thin">{$list[list].date_format}</td>
   </tr>
  {/section}
  </table>

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
      //-->                     
      </script>

{*Multiselection select action form *}
<br />
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />
<i>{tr}Perform action{/tr} :</i>

<input type="submit" name="action" value="linkMetadata" />

{*Submit checkform form*}
<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_mode|escape}" />
<input type="hidden" name="sort_order" value="{$sort_mode|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="action" value="{$action}" />
<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="flag" value="1" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />

</form>

<form name="cancel" method="post" action="{$smarty.server.PHP_SELF}">
<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="action" value="getMetadataLinks" />
<input type="hidden" name="space" value="{$CONTAINER_TYPE}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="submit" name="cancel" value="cancel" />
</form>

</div>
</div>
</div>
</div>
</body>
