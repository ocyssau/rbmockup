{*Smarty template*}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>DocSIER</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <link href="styles/DocSier.css" rel="stylesheet" type="text/css">
    <link href="styles/PhpLayerMenu.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="img/vache_30.png" />
    <script type="text/javascript" src="lib/tiki-js.js"></script>
    <script type="text/javascript" src="lib/lib.js"></script>
    <noscript>
    BE CAREFUL : You must enable javascript for use RanchBE and enable pop-up.
    </noscript>

</head>
<body>

{popup_init src="lib/overlib.js"}

<div id="tiki-main">
   
   <div id="tiki-top">
    {*{tr}This is{/tr} RanchBE ver:{$ranchbe_version}  &#169; 2007 {tr}by the{/tr} <a href="" title="RanchBE">{tr}RanchBE group{/tr}</a>
    | {$smarty.now|date_format:"%A %d %B %Y"}*}
    <img src="./img/docsier/ManuelAirbus.jpg" />
  </div> 
  
  <div id="tiki-mid">
    <div id="tiki-center">

    <h1 class="pagetitle">RECHERCHE DE NORMES AIRBUS</h1>

    <form method="post" name="form" action="{$smarty.server.PHP_SELF}?#result">
    {$form.hidden}
    
    <fieldset>
    <legend align="top" class="texte"><i>Recherche d�taill�e</i></legend>
      <b>NOTE:</b> <i>Ne pas utiliser le caract�re "*" dans les champs de recherche.</i><br />
      <label class="texte">{$form.numrows.label}:</label>
      {$form.numrows.html}
      <br /><br>
      <label class="texte">{$form.document_number.label}:</label>
      {$form.document_number.html}
      <br /><br>
      <label class="texte">{$form.designation.label}:</label>
      {$form.designation.html}
      <br /><br>
      <b>NOTE:</b> <i>L'applicabilit� n'est pas pour l'instant renseign� pour l'ensemble des documents</i><br />
      <label class="texte">{$form.applicabilite.label}:</label>
      {$form.applicabilite.html}

    </fieldset>

    {$form.reset.html}&nbsp;{$form.submit.html}

    </form>

    <hr />
  
    <a name="result" ></a>
    <h1 class="pagetitle">RESULTATS</h1>
    <b><font color="white">{$CountOutput} documents trouv�s</font></b><br />

    {*--------------------Display messages------------------------------*}
    <b><font color="red">{$search_return}</font></b><br />

    {*--------------------Display result--------------------------------*}
    
    <form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
    {section name=list loop=$list}<span class="texteresult">
      <div class="divbleu"><hr size ="1" color ="#FFFF99" />ID : 
      {$list[list].document_number}<br />
      Description :
      {$list[list].designation}<br />
      Indice majeur :
      {$list[list].maj_int_ind}<br />
      Indice mineur :
      {$list[list].min_int_ind}<br />
      Etat :
      {$list[list].document_state}<br />
      <a href="ManuelAirbus_Search.php?action=viewDocument&checked[]={$list[list].document_id}&ticket={$ticket}" title="{tr}View document{/tr}">
      <img border="0" alt="no icon" src="{$icons_dir}/C/{$list[list].doctype_id}.gif" />
      <img border="0" src="{$deposit_dir}/_thumbs/{$list[list].document_id}.png" />
      </a>

      <hr size ="1" color ="#FFFF99" />
    </span></div>
    {/section}    
    </form>    

    </div>
  </div>
  
  <div id="tiki-bot">
  </div>

</div>
