{*Smarty template*}

<div id="page-title">
  <h1 class="pagetitle">{tr}Containers consultation{/tr}</h1>
</div>

{*--------------------Search Bar defintion--------------------------*}
{*assign var="histoOption" value="1"*}
{include file='searchBar_doc.tpl'}
{*include file='searchBar.tpl'*}

{* -------------------Pagination------------------------ *}
{include file='pagination.tpl'}

{*--------------------list header----------------------------------*}
<form name="checkform" method="post" action="{$smarty.server.PHP_SELF}">
<table class="normal">
 <tr>
  <th class="heading auto">Sort: </th>
  <th class="heading auto">
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=doctype_id">
  {tr}Doctype{/tr}</a><br />
  </th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=document_number">
  {tr}Number{/tr}</a>
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=document_indice_id">
  {tr}Indice{/tr}</a>
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=document_version">
  {tr}Version{/tr}</a>
  <br />
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=designation">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{tr}Designation{/tr}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
  <br />
  <a 
  {popup text='<b>Access code legend</b> :<ul>
  <li><b>0</b>:All access free</li>
  <li><b>1</b>:Modification in progress, checkout forbidden</li>
  <li><b>5</b>:workflow in progress, checkout forbidden</li>
  <li><b>10</b>:Document validate, checkout and workflow request forbidden</li>
  <li><b>11</b>:Lock by user, checkout and workflow request forbidden</li>
  <li><b>15</b>:Indice is upgraded, all action forbidden</li></ul>'}
  class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=document_access_code">
  {tr}Access{/tr}</a>
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=document_state">
  {tr}State{/tr}</a></th>

  <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=category_id">
  {tr}Category{/tr}</a></th>

  <th class="heading">
  {tr}CheckOut{/tr}
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=check_out_by">
  {tr}By{/tr}</a> - 
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=check_out_date">{tr}Date{/tr}</a>
  <br />
  {tr}Last Update{/tr}
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=update_by">
  {tr}By{/tr}</a> - 
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=update_date">{tr}Date{/tr}</a>
  <br />
  {tr}Created{/tr}
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=open_by">
  {tr}By{/tr}</a> - 
  <a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field=open_date">{tr}Date{/tr}</a>
  </th>


  <!-- Display optionnals fields -->
  {section name=of loop=$optionalFields}
    <th class="heading"><a class="tableheading" href="{if $sort_order eq 'DESC'}{sameurl sort_order='ASC'}{else}{sameurl sort_order='DESC'}{/if}&sort_field={$optionalFields[of].field_name}">
   {tr}{$optionalFields[of].field_description}{/tr}</a></th>
  {/section}
 </tr>

{*--------------------list Body----------------------------------*}
{cycle print=false values="even,odd"} {* ---SmartyCode to alternate colors of rows---*}
  {section name=list loop=$list}
  {if $list[list].document_access_code > 14}
   <tr class="historical">
  {else}
   <tr class="{cycle}">{/if}
    <td class="thin">
    <input type="checkbox" name="checked[]" value="{$list[list].document_id}" id="checkbox_tbl_{$smarty.section.list.index}" {if $list[list].checked eq 'y'}checked="checked" {/if} /><br />
    {$list[list].document_access_code|access_code_concis}</td>

    <td class="thin">
    <a href="viewFile.php?document_id={$list[list].document_id}&ticket={$ticket}&space={$CONTAINER_TYPE}" title="{tr}View document{/tr}">
    <img border="0" alt="no icon" src="{$icons_dir}/C/{$list[list].doctype_id}.gif" />
    {if $displayThumbs}
      <img border="0" src="{$thumbs_dir}/{$list[list].document_id}{$thumbs_extension}" />
    {/if}
    </a>
    <font size="-3"><br /><i>{$list[list].doctype_id|type}</i></font>
    </td>

    <td class="thin">
      <a class="link" href="viewFile.php?document_id={$list[list].document_id}&ticket={$ticket}&space={$CONTAINER_TYPE}" title="{tr}View document{/tr}">
      {$list[list].document_number} - {$list[list].document_indice_id|document_indice}.{$list[list].document_version}</a>
      <a href="javascript:popupP('DocManage.php?documentDetail=1&document_id={$list[list].document_id}&space={$CONTAINER_TYPE}','documentDetailWindow', 600 , 1024)">
      {tr}See details{/tr}
      </a>
      <br />
      <i>{$list[list].designation}</i>
      <br />
      {$list[list].document_access_code|access_code} - 
      {$list[list].document_state}
      <br />
      <a href="javascript:popupP('DocHistory.php?action=getHistory&document_id={$list[list].document_id}&space={$CONTAINER_TYPE}&page_id=DocManage_history_{$list[list].document_id}','history', 1024 , 750)" title="{tr}Get history{/tr}">
                       <img border="0" alt="{tr}Get history{/tr}" src="img/icons/history.png" /></a>
      <a href='containerConsult.php?action=putInWs&checked[]={$list[list].document_id}&space={$CONTAINER_TYPE}&ticket={$ticket}'><img border=0 src=./img/icons/document/document_read.png /></a>
    </td>

    <td class="thin">{$list[list].category_id|category}</td>

    <td class="thin">
      {if !empty($list[list].check_out_by)}
      <b>{tr}Checkout{/tr} : </b>{$list[list].check_out_by|username} - {$list[list].check_out_date|date_format}
      <br />{/if}
      <b>{tr}Last Update{/tr} : </b>{$list[list].update_by|username} - {$list[list].update_date|date_format}
      <br />
      <b>{tr}Created{/tr} : </b>{$list[list].open_by|username} - {$list[list].open_date|date_format}
    </td>

    <!-- Display optionnals fields -->
    {foreach key=key item=type from=$optionalFields}
      {assign var="fn" value=$type.field_name}
      <td class="thin">{filter_select id=$list[list].$fn type=$type.field_type}</td>
    {/foreach}

   </tr>
  {/section}
  </table>

{* -------------------Pagination------------------------ *}

{include file='pagination.tpl'}

{* ----Scripts to active all checkbox, see lib/tiki-js.js------- *}
      <script language='Javascript' type='text/javascript'>
      <!--
      // check / uncheck all.
      // in the future, we could extend this to happen serverside as well for the convenience of people w/o javascript.
      // for now those people just have to check every single box
      document.write("<tr><td class=\"thin\"><input name=\"switcher\" id=\"clickall\" type=\"checkbox\" onclick=\"switchCheckboxes(this.form,'checked[]',this.checked)\"/></td>");
      document.write("<td class=\"form\" colspan=\"18\"><label for=\"clickall\">{tr}select all{/tr}</label></td></tr>");
      //-->                     
      </script>
        <br>

{*Multiselection select action form *}
<img class="icon" src="./img/icons/arrow_turn_right_down.png" />

<i>{tr}Perform action{/tr} :</i>

<button class="mult_submit" type="submit" name="action" value="putInWs" title="{tr}Put in wildspace{/tr}" id="06"
 onclick="document.checkform.action='containerConsult.php?space={$space}'; pop_no(checkform)">
<img class="icon" src="./img/icons/document/document_read.png" title="{tr}Put in wildspace{/tr}" alt="{tr}Put in wildspace{/tr}" width="16" height="16" />
</button>

<p></p>

<input type="hidden" name="find" value="{$find|escape}" />
<input type="hidden" name="find_field" value="{$find_field|escape}" />
<input type="hidden" name="numrows" value="{$numrows|escape}" />
<input type="hidden" name="sort_field" value="{$sort_field|escape}" />
<input type="hidden" name="sort_order" value="{$sort_order|escape}" />
<input type="hidden" name="offset" value="{$offset|escape}" />
<input type="hidden" name="ticket" value="{$ticket}" />
<input type="hidden" name="container_id" value="{$container_id}" />
<input type="hidden" name="space" value="{$space}" />
</form>
