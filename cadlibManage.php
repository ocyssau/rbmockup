<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//Setup
require_once './conf/ranchbe_setup.php';
require_once "HTML/QuickForm.php"; //Librairy to easily create forms
require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm
require_once './lib/Date/date.php';
require_once('./GUI/GUI.php');

require_once './class/common/container.php';
$Manager = container::_factory('cadlib',$_REQUEST['cadlib_id']); //Create new manager
$space =& $Manager->space;

$area_id = $Manager->AREA_ID;

check_ticket ( 'container_create', $area_id );

require_once ('conf/galaxia_setup.php');
include_once ('lib/Galaxia/ProcessMonitor.php');
$processList = $processMonitor->monitor_list_processes(0, 9999 , 'lastModif_desc', $find , $where);

//Construct the form with QuickForm lib
 $form = new HTML_QuickForm('form', 'post');

//Set defaults values of elements if modify request
  if ($_REQUEST['action'] == 'modify'){

      //Get infos
      $Infos = $Manager->GetInfos();

      $fsCloseDate = $Infos['forseen_close_date'];

      $form->setDefaults(array(
        'cadlib_number' => $Infos['cadlib_number'],
        'cadlib_description' => $Infos['cadlib_description'],
        'default_process_id' => $Infos['default_process_id'],
        'doctype_id' => $selectedDoctypes,
        'submit'      => 'modify',
      ));

      //Add hidden fields
      $form->addElement('hidden', 'cadlib_id', $_REQUEST['cadlib_id']);
      $form->addElement('hidden', 'action', 'modify');
      //Assign var 'action' to smarty to manage display
      $smarty->assign('action', 'modify');
      $smarty->assign('cadlib_number', $Infos['cadlib_number']);
  }

//Set defaults values of elements if create request
  if($_REQUEST['action'] == 'create'){

    //Construct array for set doctype select options
    $proc_params = array(
              'field_description' => tra('Valid doctypes'),
              'field_name' => 'doctype_id',
              'field_size' => 5,
              'field_multiple' => true,
              'field_required' => false,
              'return_name' => false,
    );
    construct_select_doctype($proc_params , $form);

    $fsCloseDate = time() + ( 3600 * 24 * $DEFAULT_LIFE_TIME ); //Set the default forseen close date 1 year after current date
    $form->setDefaults(array(
      'cadlib_description'  => 'New cadlib',
      'submit'  => 'create',
    ));

    //Add hidden fields
     $form->addElement('hidden', 'action', 'create');
    
    //Check box to set the container type to file manager
    $form->addElement('checkbox', 'file_only', 'Manage only file');

    //Assign var 'action' to smarty to manage display
     $smarty->assign('action', 'create');
  }

//Add fields for input informations in all case
  $form->addElement('text', 'cadlib_number', array(tra('Number'), 'note'=>'note' ));
  $form->addElement('text', 'cadlib_description', tra('description'));
  $form->addElement('text', 'source', 'source');

  $smarty->assign('number_help', DEFAULT_CADLIB_MASK_HELP);

  //Construct array for set process select options
  $proc_params = array(
            'field_description' => tra('Default process'),
            'field_name' => 'default_process_id',
            'field_size' => 1,
            'field_multiple' => false,
            'field_required' => false,
            'return_name' => false,
  );
  construct_select_process($proc_params , $form);

  //Contruct date select field
  $date_params = array(
            'field_name' => 'forseen_close_date',
            'default_value' => $fsCloseDate,
            'field_required' => true,
  );
  construct_select_date($date_params , $form);

//Add v0.4.3<
  //Get fields for custom metadata
  $optionalFields = $Manager->GetMetadata();
  $smarty->assign( 'optionalFields' , $optionalFields);
  foreach($optionalFields as $field){
    if ($_REQUEST['action'] == 'modify')
      $field['default_value'] = $Infos[$field['field_name']];
    construct_element($field , $form); //function declared in GUI.php
  }
//>Add v0.4.3

  $form->addElement('reset', 'reset', 'reset');
  $form->addElement('submit', 'submit', 'Go');

  if ($_REQUEST['action'] == 'create')
    $form->addRule('cadlib_number', tra('Number is required'), 'required');
    $mask = DEFAULT_CADLIB_MASK;
    $form->addRule('cadlib_number', tra('This number is not valid'), 'regex', "/$mask/" , 'server');

// Try to validate the form
if ($form->validate()) {
  $form->freeze(); //and freeze it

  $form->setConstants(array( //Re-set value for display correct date in freeze form
   'fsCloseDate' => $form->getSubmitValue('fsCloseDate'),
  ));

// Form is validated, then processes the modify request
  if ($_REQUEST['action'] == 'modify'){
    $form->process('modifyMO', true);
  }

// Form is validated, then processes the create request
  if ($_REQUEST['action'] == 'create'){
    $form->process('createMO', true);
  }
} //End of validate form

function modifyMO($values){
  global $Manager;
  global $smarty;
  $values = serializeProperties($values);
  $Manager->Update($values);
  //Assign var 'action' to smarty to manage display
  $smarty->assign('action', 'modify');
}

function createMO($values){
  global $Manager;
  global $smarty;
  $values = serializeProperties($values);
  $Manager->Create($values);
  //Assign var 'action' to smarty to manage display
  $smarty->assign('action', 'create');
}

//---------------------------
function serializeProperties($values){
  if(!is_numeric($values['forseen_close_date']))
    $values['forseen_close_date'] = getTSFromDate( $values['forseen_close_date']);
  foreach($values as $key=>$value){
    if( is_array($value) ){
      $values[$key] = implode('#' , $value);
    }
  }
  return $values;
} //End of function

//Set the renderer for display QuickForm form in a smarty template
include 'QuickFormRendererSet.php';

$Manager->error_stack->checkErrors();
    
// Display the template
$smarty->assign('cadlibsTab', 'active');
$smarty->assign('mid', 'cadlibManage.tpl');
$smarty->display('cadlibManage.tpl');
//$smarty->display('ranchbe.tpl');

?>
