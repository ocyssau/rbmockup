<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
CREATE TABLE `workitem_categories` (
  `category_id` int(11) NOT NULL default '0',
  `category_number` varchar(32)  NOT NULL default '',
  `category_description` text ,
  `category_icon` varchar(32)  default NULL,
  PRIMARY KEY  (`category_id`)
) ENGINE=InnoDB ;
*/

require_once('./class/common/basic.php');

/*! \brief A category define sub unit of organisation in the container. Each space has a set of categories.
*
*/
class category extends basic{

protected $category_id; //(Integer)
protected $dbranchbe; //(Object)
protected $usr; //(Object)
public $error_stack; //(Object)
private $catTable;
private $relTable;
protected $container; //(Object container)

function __construct(space &$space , $category_id=NULL){
  global $dbranchbe;
  $this->dbranchbe =& $dbranchbe;
  $this->space =& $space;
  $this->error_stack =& $this->space->error_stack;

  $this->catTable  = $this->space->SPACE_NAME .'_categories';

  global $dbranchbe;
  $this->dbranchbe =& $dbranchbe;

  global $usr;
  $this->usr =& $usr;

  if(!is_null($category_id))
    $this->init($category_id);

  $this->OBJECT_TABLE  = $this->space->SPACE_NAME .'_categories';

  $this->FIELDS_MAP_ID = 'category_id';
  $this->FIELDS_MAP_NUM =  'category_number';
  $this->FIELDS_MAP_DESC =  'category_description';

  $this->FIELDS = array('category_id'=>'category_id',
                        'category_number'=>'category_number',
                        'category_description'=>'category_description',
                        'category_icon'=>'category_icon',
                        );

}//End of method

//----------------------------------------------------------
  function init($category_id){
    $this->category_id = $category_id;
    return true;
  }//End of method

//----------------------------------------------------------
  /*
  function SetContainer(container &$container){
    $this->container =& $container;
  }//End of method
  */

//----------------------------------------------------------
/*!\brief
 * Get list of categories links to space
 *
 \param $params(array) is use for manage the display. See parameters function of GetQueryOptions()
*/
function GetCategories($params=''){
  return $this->GetAllBasic( $params );
}//End of method

//----------------------------------------------------------
/*!\brief
 * Get infos about one category
 *
 \param $category_id(integer) Primary key of category
*/
//function GetCategoryInfos($category_id){
function GetCategoryInfos($category_id = NULL){
  if( empty($category_id) ){
    if( empty($this->category_id) ){
      $this->error_stack->push(LOG, 'Error', array(), '$this->category_id is not set');
      return false;
    }else{
      $category_id = $this->category_id;
    }
  }
  return $this->GetBasicInfos( $category_id );
}//End of method

//----------------------------------------------------------
/*!\brief
 * Create a category
 *
 \param $data(array) , its an array where keys/values are fields/values of the record to create.
*/
function CreateCategory($data){
  return $this->BasicCreate($data);
}//End of method

//----------------------------------------------------------

/*!\brief
 * Modify a category
 *
 \param $data(array) , its an array where keys/values are fields/values of the record to modify.
*/
function ModifyCategory($data){
  if(!isset($this->category_id)){
    $this->error_stack->push(ERROR, 'Error', array(), '$this->category_id is not set');
    return false;}

  unset($data['category_id']);
  return $this->BasicUpdate($data, $this->category_id);

}//End of method

//----------------------------------------------------------

/*!\brief
 * Suppress a category
 *
 \param $category_id(integer) Primary key of category
*/
function SuppressCategory($category_id){
  /*
  $query = 'DELETE FROM '.$this->OBJECT_TABLE." WHERE category_id = '".$category_id."'";
  if(!$links = $this->dbranchbe->Execute($query)){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
    return false;
  }
  return true;
  */

  return $this->BasicSuppress($category_id);

}//End of method

//----------------------------------------------------------
/*!\brief
 * Return the id of the categorie $category_number if exist. Else return false.
 *
 \param $category_number(string) Number of the category
*/
function GetCategoryId($category_number){

  return $this->GetBasicId($category_number);
}//End of method

} //End of class
?>
