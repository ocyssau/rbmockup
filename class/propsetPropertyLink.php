<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

class propsetPropertyLink extends basic{

/*
DROP TABLE `workitem_propset_property_rel`;
CREATE TABLE `workitem_propset_property_rel` (
  `link_id` INT NOT NULL ,
  `propset_id` INT NOT NULL ,
  `property_id` INT NOT NULL ,
PRIMARY KEY  (`link_id`)
) ENGINE=InnoDB;
ALTER TABLE `workitem_propset_property_rel` 
ADD UNIQUE `workitem_propset_property_rel_uniq1` ( `propset_id` , `property_id` );


CREATE TABLE `bookshop_propset_property_rel` (
  `link_id` INT NOT NULL ,
  `propset_id` INT NOT NULL ,
  `property_id` INT NOT NULL ,
PRIMARY KEY  (`link_id`)
) ENGINE=InnoDB;
ALTER TABLE `bookshop_propset_property_rel` 
ADD UNIQUE `bookshop_propset_property_rel_uniq1` ( `propset_id` , `property_id` );


CREATE TABLE `cadlib_propset_property_rel` (
  `link_id` INT NOT NULL ,
  `propset_id` INT NOT NULL ,
  `property_id` INT NOT NULL ,
PRIMARY KEY  (`link_id`)
) ENGINE=InnoDB;
ALTER TABLE `cadlib_propset_property_rel` 
ADD UNIQUE `cadlib_propset_property_rel_uniq1` ( `propset_id` , `property_id` );


CREATE TABLE `mockup_propset_property_rel` (
  `link_id` INT NOT NULL ,
  `propset_id` INT NOT NULL ,
  `property_id` INT NOT NULL ,
PRIMARY KEY  (`link_id`)
) ENGINE=InnoDB;
ALTER TABLE `mockup_propset_property_rel` 
ADD UNIQUE `mockup_propset_property_rel_uniq1` ( `propset_id` , `property_id` );
*/

function __construct(space &$space){
  
  global $dbranchbe;
  $this->dbranchbe =& $dbranchbe;
  $this->space =& $space;
  $this->error_stack =& $this->space->error_stack;
  
  $this->OBJECT_TABLE = $this->space->SPACE_NAME.'_propset_property_rel';
  $this->FIELDS_MAP_ID = 'link_id';
  
  $this->LEFT_OBJECT_TABLE = $this->space->SPACE_NAME.'_propset';
  $this->LEFT_FIELDS_MAP_ID = 'propset_id';
  
  $this->RIGHT_OBJECT_TABLE = $this->space->SPACE_NAME.'_metadata';
  $this->RIGHT_FIELDS_MAP_ID = 'property_id';
  
}//End of method

//--------------------------------------------------------------------
/*! \brief Add a propset link
* 
* \param $params(array)
*/
function AddLink( $params ){
  //Filter input
  $data['propset_id'] = $params['propset_id'];
  $data['property_id'] = $params['property_id'];
  return $this->BasicCreate($data);
}//End of method

//----------------------------------------------------------
/*! \brief Update a propset link
* 
  \param $propset_id(integer)
  \param $data(array)
*/
function ModifyLink( $link_id , $data ){
  unset($data['link_id']); //to prevent the change of id
  return $this->BasicUpdate($data , $link_id);
}//End of method

//----------------------------------------------------------
/*! \brief Suppress a propset link
* 
  \param $propset_id(integer)
*/
function SuppressLink( $link_id ){
  return $this->BasicSuppress($link_id);
}//End of method

//----------------------------------------------------------
/*!\brief Get the propset links
*/
function GetPropset( $propset_id ){
  $params['exact_find']['propset_id'] = $propset_id;
  return $this->GetAllBasic($params);
}//End of method

//----------------------------------------------------------
/*!\brief Get the propset links
*/
function GetProperty( $property_id ){
  $params['exact_find']['property_id'] = $property_id;
  return $this->GetAllBasic($params);
}//End of method

} //End of class

?>
