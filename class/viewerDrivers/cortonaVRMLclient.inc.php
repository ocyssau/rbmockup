<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*! \brief 
*
*/
require_once('./class/viewerDrivers/defaultViewer.inc.php');

class cortonaVRMLclient extends defaultViewer {

  function __construct($Viewer){
    $this->Viewer = $Viewer;
    $this->Manager &= $Viewer->getProperty('Manager');
  }//End of method

  static function embededViewer($file){
    return ('<OBJECT DATA="getFile.php?file='.$file.'&file_mime_type=x-world/x-vrml&object_class=visu"
            TYPE="x-world/x-vrml"
            TITLE="'.$file.'"
            WIDTH=300
            HEIGHT=300>
            </OBJECT>');
  }//End of method

} //End of class
?>
