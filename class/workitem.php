<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once './class/common/space.php';
require_once('./class/common/container.php');

/*
CREATE TABLE `workitems` (
  `workitem_id` int(11) NOT NULL default '0',
  `workitem_number` varchar(16)  NOT NULL default '',
  `workitem_state` varchar(16)  NOT NULL default 'init',
  `workitem_description` varchar(128)  default NULL,
  `file_only` tinyint(1) NOT NULL default '0',
  `workitem_indice_id` int(11) default NULL,
  `project_id` int(11) default NULL,
  `default_process_id` int(11) default NULL,
  `default_file_path` text  NOT NULL,
  `open_date` int(11) default NULL,
  `open_by` int(11) default NULL,
  `forseen_close_date` int(11) default NULL,
  `close_date` int(11) default NULL,
  `close_by` int(11) default NULL,
  `class_object` varchar(10)  NOT NULL default 'workitem',
  `alias_id` int(11) default NULL,
  PRIMARY KEY  (`workitem_id`),
  UNIQUE KEY `UC_workitem_number` (`workitem_number`),
  KEY `FK_workitems_1` (`workitem_indice_id`),
  KEY `FK_workitems_2` (`project_id`)
) ENGINE=InnoDB ;
*/


/*! \brief container of workitem space. Workitem is linked to only and at least one project. The other containers type can be linked to 0 or n projects
*/
class workitem extends container{
protected $project; //Object of the father project

//-------------------------------------------------------------------------
function __construct($container_id = NULL){
  $space = new space('workitem');
  parent::__construct($space , $container_id);

  if( !empty($container_id) ){ //If a container is set, get the area_id from project
    $this->initProject();
    $this->AREA_ID = array( 10, $this->project->GetProjectArea() );
  }
}//End of method

//----------------------------------------------------------
/*! \brief Get all doctypes linked to this container.
* Return an array with the doctypes of the container and of the father project
* 
\param $extension(string) limit result to doctype wich accept extension.
\param $type(string) limit result to doctype wich accept the type.
\param $params(array) is use for manage the display. See parameters function of GetQueryOptions().
\param $recursive(bool) true to get the doctype defined on the father project.
*/
function GetDoctypes($extension='' , $type='', $params=array(), $recursive=false){
  $cdoctypes = container::GetDoctypes($extension , $type , $params);
  if($recursive){
    $this->initProject();
    $pdoctypes = $this->project->GetDoctypes($extension,$type,$params); //-- Get doctype linked to father project
    $doctypes = array_merge($cdoctypes, $pdoctypes);
    return $doctypes;
  }
  return $cdoctypes;
}//End of method

//----------------------------------------------------------
/*!\brief
* Get the process linked to the doctype for the container
*
\param $doctype_id(integer) Primary key of doctype.
*/
function GetDoctypesProcess($doctype_id, $recursive=false){
  if(!isset($this->container_id)){
    $this->error_stack->push(ERROR, 'Error', array(), '$this->container_id is not set');
    return false;}
  //Get the process linked to the doctype/container
  $p1['exact_find']['workitem_doctype_process.doctype_id'] = $doctype_id;
  $p1['select'] = array('process_id', 'link_id');
  $res = $this->GetDoctypes(NULL, NULL, $p1, false);
  if(!empty($res[0]['process_id'])) return $res[0]['process_id'];

  if($recursive){ //Get the process linked to the doctype/project
    $p2['exact_find']['project_doctype_process.doctype_id'] = $doctype_id;
    $p2['select'] = array('process_id', 'link_id');
    $this->initProject();
    $res = $this->project->GetDoctypes(NULL, NULL, $p2); //-- Get doctype linked to father project
    if(!empty($res[0]['process_id'])) return $res[0]['process_id'];
  }

  return false;
}//End of method

//----------------------------------------------------------
/*!\brief
* Check if there is at least one doctype linked to container
* Return false or true
*/
function HasDoctype(){
  if(!isset($this->container_id)){
    $this->error_stack->push(ERROR, 'Error', array(), '$this->container_id is not set');
    return false;}
  $query= 'SELECT doctype_id FROM workitem_doctype_process WHERE workitem_id = '.$this->container_id.'
        UNION 
        SELECT doctype_id FROM project_doctype_process WHERE project_id = '.$this->GetFatherProject();
  $one = $this->dbranchbe->GetOne($query);
  if($one === false) return false;
  if(empty($one))return false;
  return true;
}//End of method

//----------------------------------------------------------
/*! \brief Get the father project of the workitem.
* Return the project id, else return false.
*    
*/
function GetFatherProject(){
  return $this->GetProperty('project_id');
}//End of method

//----------------------------------------------------------
/*! \brief Define the father project of the workitem.
* 
  \param $project_id(integer) Id of the project.
  \param $workitem_id(integer) Id of the container.
*/
function LinkProject($project_id){
  if(!isset($this->container_id)){
    $this->error_stack->push(ERROR, 'Error', array(), '$this->container_id is not set');
    return false;}
  $data['project_id'] = $project_id;
  return $this->BasicUpdate($data, $this->container_id);
  //Write history
  $this->history['action_name'] = 'LinkProject';
  $this->WriteHistory();
}//End of method

//--------------------------------------------------------
/*!\brief
 * Get the project object and link it to the current container object
 * return a project instance if success, else fase.
 * Set the container object $this->project
 * The method init() must be call before.
 *   
*/
function initProject($project_id=NULL){
  if(is_null($project_id)){
    if($project_id = $this->GetProperty('project_id')){
      if(!isset($this->project)){
        require_once('./class/project.php');
        return $this->project = new project($this->space, $project_id);
      }
      return $this->project;
    }
  }else{
    require_once('./class/project.php');
    if($this->project = new project($this->space, $project_id)){
      $this->core_props[$this->FIELDS_MAP_FATHER] = $project_id;
      return $this->project;}
  }
  return false;
}//End of method

//--------------------------------------------------------
/*!\brief
* Alias of initProject
*/
function initFather($father_id=NULL){
  return $this->initProject($father_id);
}//End of method

//-------------------------------------------------------------------------
/*! \brief init the property of the container
* 
* \param $container_id(integer) Id of the container
*/
function init($container_id){
  parent::init($container_id);
  $this->initProject();
  if( is_object( $this->project ) )
    $this->AREA_ID = array( 10, $this->project->GetProjectArea() );
  else
    $this->AREA_ID = array( 10 );
  return true;
}//End of method


}//End of class



?>
