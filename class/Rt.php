<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2011 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once './class/common/document.php';
require_once('./class/common/basic.php');

/*
 CREATE TABLE IF NOT EXISTS `rt_aif_code_a350` (
 `code` char(15) COLLATE latin1_general_ci NOT NULL,
 `itp_cluster` char(4) COLLATE latin1_general_ci DEFAULT NULL,
 `check` char(2) COLLATE latin1_general_ci DEFAULT NULL,
 `mistake` char(2) COLLATE latin1_general_ci DEFAULT NULL,
 `type` enum('B','I','OS') COLLATE latin1_general_ci NOT NULL,
 `status` char(1) COLLATE latin1_general_ci DEFAULT NULL,
 `description` varchar(255) COLLATE latin1_general_ci NOT NULL,
 PRIMARY KEY (`code`)
 ) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='Codes erreurs pour le programme A350';
 */



Class Rt extends objects{

	public static $_doctype_id = 188;
	public static $_appliedStatus = 'rt_applied';
	public static $_confirmedStatus = 'rt_confirmed';
	
	function __construct($pkey = null, $table = 'rt_aif_code_a350'){
		global $error_stack;
		$this->error_stack =& $error_stack;
		
		global $dbranchbe;
		$this->dbranchbe =& $dbranchbe;

		global $logger;
		$this->logger =& $logger;

		global $usr;
		$this->usr =& $usr;
		
		$this->OBJECT_TABLE  = $table;
		$this->FIELDS_MAP_ID = 'code';
		$this->FIELDS_MAP_NUM = 'code';
		$this->FIELDS_MAP_DESC = 'description';
		
		if( !is_null($pkey) ){
			$this->pkey = $pkey;
			$this->_props = $this->GetBasicInfos($this->pkey);
		}
	}//End of method


	/**
	 * @param integer
	 * @param integer
	 * @param boolean
	 * @param array
	 * @return ADORecordSet
	 * 
	 */
	function GetRtForPeriod($fromDate, $toDate, $onlyCount=false, $rset_return=false, $select=array()){
		//$fromDate = ;
		//$toDate = ;

		$select = implode(',', $select);
		if(!$select) $select = '*';
		
		$view_name = 'view_rt';
		
		if($onlyCount){
			$sql = 'SELECT COUNT(rt_id) FROM ' . $view_name;
		}else{
			$sql = 'SELECT '.$select.' FROM ' . $view_name;
		}

		//$sql .= ' WHERE doctype_id='.self::$_doctype_id;
		$sql .= ' WHERE rt_open_date > '. $fromDate;
		$sql .= ' AND rt_open_date < '. $toDate;
		
		//echo $sql;
		if($onlyCount){
			$one = $this->dbranchbe->GetOne($sql);
			return $one;
		}else{
			$rs = $this->dbranchbe->execute($sql);
			if($rset_return) return $rs;
			return $rs->GetArray();
		}
	}//End of method
	
	/**
	 * @param integer
	 * @param integer
	 * @param boolean
	 * @param array
	 * @return ADORecordSet
	 */
	function GetRtApplyToDocument(document $document, $onlyCount=false, $rset_return=false, $select=array(), $filter=''){
		
		//$select = array_merge( array('rt_id', 'rt_number'), $select);
		$select = implode(',', $select);
		if(!$select) $select = 'rt_id, rt_number, rt_status, rt_access_code, rt_open_date, applyto_id, applyto_number, applyto_indice, applyto_version, applyto_status, from_number, from_version';
		
		$document_number = $document->GetDocProperty('document_number');
		
		$view_name 	= 'view_rt';
		
		if($onlyCount){
			$sql = 'SELECT COUNT(rt_id) FROM '. $view_name;
		}else{
			$sql = 'SELECT '.$select.' FROM '.$view_name;
		}
		$sql .= ' WHERE applyto_number=\'' . $document_number . '\'';
		
		if($filter){
			$sql .= ' AND ' . $filter;
		}
		
		if($onlyCount){
			$one = $this->dbranchbe->GetOne($sql);
			return $one;
		}else{
			$rs = $this->dbranchbe->execute($sql);
			if($rset_return){
				return $rs;
			}
			return $rs->GetArray();
		}
	}//End of method
	
	
	
	//----------------------------------------------------------
	/*! \brief Return properties of current object
	 *  Return array or false
	 */
	function GetProperties(){
		if(!isset($this->_props)) return false;
		else return $this->_props;
	}//End of method


	//----------------------------------------------------------
	/*! \brief Get the property of the document by the property name. init() must be call before
	 *
	 * \param $property_name(string) = document_id, document_number, ...etc
	 */
	function GetProperty($property_name){
		return $this->_props[$property_name];
	}//End of method

	//----------------------------------------------------------
	/*! \brief Set property of the document by the property name.
	 * Return true or false
	 *
	 * \param $property_name(string) = document_id, document_number, ...etc
	 * \param $property_value(string)
	 */
	function SetProperty($property_name, $property_value){
		$this->_props[$property_name] = $property_value;
		return true;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Unset a property of the document.
	 * Return true or false
	 *
	 * \param $property_name(string) = document_id, document_number, ...etc
	 */
	function UnSetDocProperty($property_name){
		unset($this->_props[$property_name]);
		return true;
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * Return the document id
	 * else return false.
	 *
	 */
	function GetId(){
		return $this->pkey;
	}//End of method
	
	//--------------------------------------------------------
	/*!\brief
	 *
	 */
	function GetAll( $params = array() ){
		return $this->Get($this->OBJECT_TABLE, $params);
	}//End of method
	
	
	
}






