<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

class propset extends basic{

/*
DROP TABLE `workitem_propset`;
CREATE TABLE `workitem_propset` (
  `propset_id` INT NOT NULL ,
  `propset_name` VARCHAR(64) NOT NULL ,
PRIMARY KEY  (`propset_id`)
) ENGINE=InnoDB;
ALTER TABLE `workitem_propset` ADD UNIQUE `workitem_propset_uniq1` ( `propset_name` );


DROP TABLE `bookshop_propset`;
CREATE TABLE `bookshop_propset` (
  `propset_id` INT NOT NULL ,
  `propset_name` VARCHAR(64) NOT NULL ,
PRIMARY KEY  (`propset_id`)
) ENGINE=InnoDB;
ALTER TABLE `bookshop_propset` ADD UNIQUE `bookshop_propset_uniq1` ( `propset_name` );

DROP TABLE `cadlib_propset`;
CREATE TABLE `cadlib_propset` (
  `propset_id` INT NOT NULL ,
  `propset_name` VARCHAR(64) NOT NULL ,
PRIMARY KEY  (`propset_id`)
) ENGINE=InnoDB;
ALTER TABLE `cadlib_propset` ADD UNIQUE `cadlib_propset_uniq1` ( `propset_name` );

DROP TABLE `mockup_propset`;
CREATE TABLE `mockup_propset` (
  `propset_id` INT NOT NULL ,
  `propset_name` VARCHAR(64) NOT NULL ,
PRIMARY KEY  (`propset_id`)
) ENGINE=InnoDB;
ALTER TABLE `mockup_propset` ADD UNIQUE `mockup_propset_uniq1` ( `propset_name` );

*/

//--------------------------------------------------------------------
/*! \brief constructor
* 
* \param $space(object)
*/
function __construct(space &$space){
  
  $this->error_stack =& $space->error_stack;
  $this->space =& $space;
  global $dbranchbe;
  $this->dbranchbe =& $dbranchbe;
  global $usr;
  $this->usr =& $usr;
  
  $this->OBJECT_TABLE = $this->space->SPACE_NAME.'_propset';
  $this->FIELDS_MAP_ID = 'propset_id';
  
  $this->LEFT_OBJECT_TABLE = $this->space->SPACE_NAME.'_propset_property_rel';
  $this->LEFT_FIELDS_MAP_ID = 'link_id';
  $this->RIGHT_OBJECT_TABLE = $this->space->SPACE_NAME.'_metadata';
  $this->RIGHT_FIELDS_MAP_ID = 'property_id';
  
}//End of method

//--------------------------------------------------------------------
/*! \brief Create a property set
* 
* \param $params(array)
*/
function CreateSet( $params ){
  //Filter input
  $data['propset_name'] = $params['propset_name'];
  return $this->BasicCreate($data);
}//End of method

//----------------------------------------------------------
/*! \brief Update a property set
* 
  \param $propset_id(integer)
  \param $data(array)
*/
function ModifySet( $propset_id , $data ){
  unset($data['propset_id']); //to prevent the change of id
  return $this->BasicUpdate($data , $propset_id);
}//End of method

//----------------------------------------------------------
/*! \brief Suppress a property set
* 
  \param $propset_id(integer)
*/
function SuppressSet( $propset_id ){
  return $this->BasicSuppress($propset_id);
}//End of method

//----------------------------------------------------------
/*!\brief Get the propset
*/
function GetPropset( $params=array() ){
  return $this->GetAllBasic($params);
}//End of method

//----------------------------------------------------------
/*!\brief Get infos
* return false or array
*/
function GetInfos( $propset_id, $selectClose = array() ){
  return $this->GetBasicInfos($propset_id , $selectClose);
}//End of method

//----------------------------------------------------------
/*!\brief Get the propset
*/
function GetPropsetRecursive( $params=array() ){

  $query =
  "SELECT propset.propset_name, propset.propset_id, property.field_name, property.field_description, property.property_id, rel.link_id
  FROM $this->OBJECT_TABLE as propset
  LEFT OUTER JOIN $this->LEFT_OBJECT_TABLE as rel ON propset.propset_id = rel.propset_id
  LEFT OUTER JOIN $this->RIGHT_OBJECT_TABLE as property ON property.property_id = rel.property_id
  ";

  if(!$rs = $this->dbranchbe->execute( $query )){
    $this->error_stack->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg());
    return false;
  }

  //return $rs->GetArray(); //To transform result in array;
  $i=0;
  while( $row = $rs->FetchRow() ){
    if($row['propset_id'] == $prev_propset_id){
      if( !empty($row['field_name']) )
        $res[$i-1]['properties'][] = array('field_description'=>$row['field_description'] , 'field_name'=>$row['field_name'] , 'property_id'=>$row['property_id'] , 'link_id'=>$row['link_id'] );
    }else{
      $res[$i] = $row;
      if( !empty($row['field_name']) )
        $res[$i]['properties'][] = array('field_description'=>$row['field_description'] , 'field_name'=>$row['field_name']  , 'property_id'=>$row['property_id'] , 'link_id'=>$row['link_id'] );
      unset($res[$i]['property_id']);
      unset($res[$i]['field_name']);
      unset($res[$i]['field_description']);
      unset($res[$i]['link_id']);
      $i++;
    }
  $prev_propset_id = $row['propset_id'];
  }
  return $res;

}//End of method

//----------------------------------------------------------
/*! \brief Get the property of the container by the property name. init() must be call before
* 
* \param $property_name(string) = container_id,container_number,container_description,container_state,container_indice_id.
*/
function GetProperty($property_name, $propset_id){
  $p['exact_find']['propset_id'] = $propset_id;
  $p['select'] = array($property_name);
  return $this->Get($this->OBJECT_TABLE , $p , 'one' , false);
}//End of method

} //End of class

?>
