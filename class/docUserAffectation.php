<?php

/*
CREATE TABLE `doc_user_affectation` (
  `document_id` int(11) NOT NULL,
  `document_space` char(8) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` varchar(256) default NULL,
  PRIMARY KEY  (`document_id`),
  KEY `doc_user_affectation_1` (`user_id`),
  KEY `doc_user_affectation_2` (`document_id`,`document_space`,`user_id`)
) ENGINE=InnoDB;
*/

require_once('class/common/basic.php');
class docUserAffectation extends basic{

  public $document_id;
  public $document_space;
  public $user_id;
  protected $message;

  function __construct(){
  } //end of method

  function init($document_id, $document_space, $user_id){
  } //end of method

  function freeze(){
  } //end of method

  function SetMessage($message){
  } //end of method

  function GetMessage(){
  } //end of method

  function IsAffected(){
  } //end of method

} //End of class

?>
