<?php 

class Ranchbe{
	private static $_logger = null;
	private static $_view = null;
	private static $_db = null;
	private static $_errorStack = null;
	private static $_cuser = null;
	private static $_lua = null;
	
	
	/**
	 * @return Log
	 */
	public static function getLogger(){
		return self::$_logger;
	}
	
	public static function setLogger($logger){
		self::$_logger = $logger;
	}
	
	public static function log($msg){
		if(self::$_logger){
			self::$_logger->log( $msg );
		}
	}
	
	/**
	 * @return Smarty
	 */
	public static function getView(){
		return self::$_view;
	}
	
	public static function setView($view){
		self::$_view = $view;
	}
	
	/**
	 * @return ADODB_mysqli
	 */
	public static function getDb(){
		return self::$_db;
	}
	
	public static function setDb($db){
		self::$_db = $db;
	}
	
	/**
	 * @return errorManager
	 */
	public static function getErrorStack(){
		return self::$_errorStack;
	}
	
	public static function setErrorStack($e){
		self::$_errorStack = $e;
	}
	
	/**
	 * @return LiveUser
	 */
	public static function getCurrentUser(){
		return self::$_cuser;
	}
	
	public static function setCurrentUser($u){
		self::$_cuser = $u;
	}
	
	/**
	 * @return LiveUserAdmin
	 */
	public static function getLiveUserAdmin(){
		return self::$_lua;
	}
	
	public static function setLiveUserAdmin($lua){
		self::$_lua = $lua;
	}
	
} 
