<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('./class/common/basic.php');

/*
ALTER TABLE `workitem_category_rel` DROP FOREIGN KEY `workitem_category_rel_ibfk_1`;
ALTER TABLE `workitem_category_rel` DROP FOREIGN KEY `workitem_category_rel_ibfk_2`;
DROP TABLE `workitem_category_rel`;

CREATE TABLE `workitem_category_rel` (
  `link_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `workitem_id` int(11) NOT NULL,
  PRIMARY KEY  (`link_id`),
KEY `K_workitem_categories_rel_1` (`category_id`),
KEY `K_workitem_categories_rel_2` (`workitem_id`),
UNIQUE `UNIQ_workitem_categories_rel_1` ( `category_id` , `workitem_id` ),
FOREIGN KEY ( `category_id` ) REFERENCES `workitem_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ( `workitem_id` ) REFERENCES `workitems` (`workitem_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB ;

DROP TABLE `bookshop_category_rel`;
CREATE TABLE `bookshop_category_rel` (
  `link_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `bookshop_id` int(11) NOT NULL,
  PRIMARY KEY  (`link_id`),
KEY `K_bookshop_categories_rel_1` (`category_id`),
KEY `K_bookshop_categories_rel_2` (`bookshop_id`),
UNIQUE `UNIQ_bookshop_categories_rel_1` ( `category_id` , `bookshop_id` ),
FOREIGN KEY ( `category_id` ) REFERENCES `bookshop_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ( `bookshop_id` ) REFERENCES `bookshops` (`bookshop_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB ;

DROP TABLE `cadlib_category_rel`;
CREATE TABLE `cadlib_category_rel` (
  `link_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `cadlib_id` int(11) NOT NULL,
  PRIMARY KEY  (`link_id`),
KEY `K_cadlib_categories_rel_1` (`category_id`),
KEY `K_cadlib_categories_rel_2` (`cadlib_id`),
UNIQUE `UNIQ_cadlib_categories_rel_1` ( `category_id` , `cadlib_id` ),
FOREIGN KEY ( `category_id` ) REFERENCES `cadlib_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ( `cadlib_id` ) REFERENCES `cadlibs` (`cadlib_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB ;

DROP TABLE `mockup_category_rel`;
CREATE TABLE `mockup_category_rel` (
  `link_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `mockup_id` int(11) NOT NULL,
  PRIMARY KEY  (`link_id`),
KEY `K_mockup_categories_rel_1` (`category_id`),
KEY `K_mockup_categories_rel_2` (`mockup_id`),
UNIQUE `UNIQ_mockup_categories_rel_1` ( `category_id` , `mockup_id` ),
FOREIGN KEY ( `category_id` ) REFERENCES `mockup_categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY ( `mockup_id` ) REFERENCES `mockups` (`mockup_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB ;


ALTER TABLE `workitem_category_rel` ADD FOREIGN KEY ( `category_id` ) REFERENCES `ranchbe0-6`.`workitem_categories` (
`category_id`
) ON DELETE CASCADE ON UPDATE CASCADE ;
ALTER TABLE `workitem_category_rel` ADD FOREIGN KEY ( `workitem_id` ) REFERENCES `ranchbe0-6`.`workitems` (
`workitem_id`
) ON DELETE CASCADE ON UPDATE CASCADE ;
*/

/*! \brief A category define sub unit of organisation in the container. Each space has a set of categories.
*
*/
class categoryContainerLink extends basic{

function __construct(container &$container , $link_id=NULL){
  global $dbranchbe;
  $this->dbranchbe =& $dbranchbe;
  $this->container =& $container;
  $this->space =& $container->space;
  $this->error_stack =& $this->space->error_stack;

  $this->OBJECT_TABLE = $this->space->SPACE_NAME.'_category_rel';
  $this->FIELDS_MAP_ID = 'link_id';
  
  $this->LEFT_OBJECT_TABLE = $this->space->SPACE_NAME.'_categories';
  $this->LEFT_FIELDS_MAP_ID = 'category_id';
  
  $this->RIGHT_OBJECT_TABLE = $this->space->SPACE_NAME.'s';
  $this->RIGHT_FIELDS_MAP_ID = $this->space->SPACE_NAME.'_id';

  $this->FIELDS = array();
  
  if(!empty($link_id)){
    $this->init($link_id);
  }
}//End of method

//--------------------------------------------------------------------
function init($link_id){
  $this->link_id = $link_id;
  if(isset($this->core_prop)) unset($this->core_prop);
  return true;
}//End of method

//----------------------------------------------------------
/*!\brief
* Link a category to a container
*
\param $data['container_id'](integer)
\param $data['category_id'](integer)
*/
function AddLink( $data ){
  foreach($data as $key=>$val){
    $this->SetProperty($key, $val);
  }
  return $this->BasicCreate( $this->core_prop );
}//End of method

//----------------------------------------------------------
/*! \brief Update a link
* 
\param $data['container_id'](integer)
\param $data['category_id'](integer)
\param $link_id(integer)
*/
function ModifyLink( $link_id , $data ){
  unset($data['link_id']); //to prevent the change of id
  return $this->BasicUpdate($data , $link_id);
}//End of method

//----------------------------------------------------------
/*! \brief Suppress a link
* 
  \param $propset_id(integer)
*/
function SuppressLink( $link_id ){
  return $this->BasicSuppress($link_id);
}//End of method

//----------------------------------------------------------
/*! \brief Get the property of the container by the property name. init() must be call before
* 
* \param $property_name(string) = container_id,container_number,container_description,container_state,container_indice_id.
*/
function GetProperty($property_name){
  if(!isset($this->link_id)){
    $this->error_stack->push(LOG, 'Error', array(), '$this->alias_id is not set');
    return false;
  }
  
  switch($property_name){
    case 'container_id':
        if(isset($this->core_prop[ $this->RIGHT_FIELDS_MAP_ID ]))
          return $this->core_prop[ $this->RIGHT_FIELDS_MAP_ID ];
        else $realName = $this->RIGHT_FIELDS_MAP_ID;
    break;  

  	default:
      if(isset($this->core_prop[$property_name]))
        return $this->core_prop[$property_name];
      else $realName = $property_name;
    break;  
  } //End of switch
  
  $query = 'SELECT '.$realName.' FROM '.$this->OBJECT_TABLE.' WHERE '.$this->FIELDS_MAP_ID.' = \''.$this->alias_id.'\'';
  $res = $this->dbranchbe->GetOne($query);
  if($res === false){
    $this->error_stack->push(ERROR, 'Fatal', array('query'=>$query , 'debug'=>array($this->alias_id)), $this->dbranchbe->ErrorMsg());
    return false;
  }
  else
    return $this->core_prop[$realName] = $res;
}//End of method

//----------------------------------------------------------
/*! \brief Get the property of the container by the property name. init() must be call before
* 
* \param $property_name(string)
*/
function SetProperty($property_name, $property_value){
  switch($property_name){
    case 'container_id':
      return $this->core_prop[$this->RIGHT_FIELDS_MAP_ID] = $property_value;
      break;

  	default:
      return $this->core_prop[$property_name] = $property_value;
      break;
  } //End of switch
}//End of method

//----------------------------------------------------------
/*!\brief Get the propset links
*/
function GetLinks( $params ){
  $params['with'][0]['type'] = 'INNER';
  $params['with'][0]['table'] = $this->RIGHT_OBJECT_TABLE; //Container
  $params['with'][0]['col'] = $this->RIGHT_FIELDS_MAP_ID;

  $params['with'][1]['type'] = 'INNER';
  $params['with'][1]['table'] = $this->LEFT_OBJECT_TABLE; //Category
  $params['with'][1]['col'] = $this->LEFT_FIELDS_MAP_ID;

  $params['select'] = array(
                            $this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_ID,
                            $this->RIGHT_OBJECT_TABLE.'.'.$this->RIGHT_FIELDS_MAP_ID,
                            $this->LEFT_OBJECT_TABLE.'.'.$this->LEFT_FIELDS_MAP_ID,
                            $this->LEFT_OBJECT_TABLE.'.category_number',
                            $this->LEFT_OBJECT_TABLE.'.category_description',
                            $this->LEFT_OBJECT_TABLE.'.category_icon',
                            );
  $params['exact_find'][$this->OBJECT_TABLE.'.'.$this->RIGHT_FIELDS_MAP_ID] = $this->container->GetId();
  return $this->GetAllBasic($params);
}//End of method

//----------------------------------------------------------
/*!\brief Get cetgories linked to container or if none return all categories of the current space
*/
function GetCategories($params=''){
  $ret = $this->GetLinks( $params );
  if( count($ret) == 0) {
    require_once('./class/category.php');
    $category = new category($this->space);
    return $category->GetCategories($params);
  }else
    return $ret;
}//End of method

} //End of class
?>
