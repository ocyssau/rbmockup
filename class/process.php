<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('./class/common/basic.php');

/*! \brief This class manage the process.
This object complement the galaxia class.
Process is a set of activities and transitions from the workflow.
*/
class process extends basic {

  protected $SPACE_NAME  = 'process';
  protected $OBJECT_TABLE = 'galaxia_processes';
  protected $FIELDS_MAP_ID = 'pId';
  protected $FIELDS_MAP_NUM = 'name';
  public $AREA_ID = 1;

  function __construct(space &$space , $process_id=NULL){
    $this->space =& $space;
    $this->error_stack =& $this->space->error_stack;

    /*foreach(get_object_vars($this->space) as $var=>$value){
      $this->$var = $value;
    }*/

    global $dbranchbe;
    $this->dbranchbe =& $dbranchbe;

    global $usr;
    $this->usr =& $usr;

    if(!is_null($process_id))
        $this->init($container_id);

  }//End of method

//-------------------------------------------------------------------------

  /*! \brief init the property of the container
  *  Return core_prop(array)
  * 
  * \param $container_id(integer) Id of the container
  */
  function init($process_id){
    if($this->core_prop = $this->GetInfos($process_id)){ //Get data about process
      $this->process_id =& $this->core_prop[$this->FIELDS_MAP_ID];
      return true;
    }else{
      $this->error_stack->push(ERROR, 'Warning', array('element'=>$container_id, 'debug'=>array()), 'there are no records for process_id %element%');
      return false;
    }
  }//End of method

//----------------------------------------------------------

  /*!\brief Get list of all process.
   * return a array if success, else return false.
   *    
    \param $params(array) is use for manage the display. See parameters function of GetQueryOptions()
  */
  function GetAll($params){
    return $this->GetAllBasic($params);
  }//End of method

//----------------------------------------------------------

  /*!\brief Get info about a single process.
   * return a array if success, else return false.
   *    
    \param $process_id(integer) id of the process.
  */
  function GetInfos($process_id) {
    $params['exact_find']["$this->FIELDS_MAP_ID"]  = "$process_id" ;
    return $this->GetAllBasic( $params );
  }//End of method

}//End of class

?>
