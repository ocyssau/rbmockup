<?php

require_once 'Zend/Mail.php';
require_once 'Zend/Mail/Transport/Smtp.php';

class Rb_Mail extends Zend_Mail{
	
	protected static $init_flag = false;
	
	function __construct(){
		parent::__construct();
		if(self::$init_flag == false){
			$mailTransport = new Zend_Mail_Transport_Smtp(SMTP_SERVER);
			Zend_Mail::setDefaultTransport($mailTransport);
			self::$init_flag = true;
		}
		
		$this->setFrom(RB_ADMIN_MAIL, 'Ranchbe');
		
	}
	
}


