<?php
require_once('class/observers/script_caller.php');

//Execute evenement scripts
//To associate a scripts on evenement just create a scripts in the datatype_scripts dir
//with name datatypeScript_[event name].php and adjust access right to this file : chown [ranchbe_user] [script file]; chmod 500 [script file];
//In this file, create function datatypeScript_[name of datatype]_[event](recordfile &$recordfile).
//This function must return a bool.
class datatypeScript extends script_caller{

  protected $recordFile; //(object recordfile)

  function __construct(recordfile &$recordFile){
    //echo 'construct datascript<br>';
    global $error_stack;
    $this->error_stack =& $error_stack; //pass by reference
    $this->recordFile =& $recordFile; //pass by reference
    $this->args = array(&$recordFile); //pass by reference
    $this->scriptsDir = DEFAULT_DATATYPE_SCRIPTS_DIR;
    $this->eventName = NULL;
    $this->scriptFile = NULL;
    $this->functionName = NULL;
    $this->className = NULL;
  }//End of method

  function notify($event, &$message){
    //echo 'notify datascript '.$event.'<br>';
    $this->eventName = $event;
    $type = $this->recordFile->GetProperty('file_type');
    $this->scriptFile = $this->scriptsDir.'/'.$type.'.php';
    $this->functionName = $this->eventName;
    $this->className = 'datatypeScript_'.$type;
    if( !$this->execute() ){
      $this->error_stack->push(ERROR, 'Warning', array('element1'=>$this->recordFile->GetProperty('file_name'), 'element2'=>$this->scriptFile, 'function'=>$this->functionName),
      'File %element1% : Script %element2% failed or have none return. Add "return true" at function %function% end');
      return false;
    }
    return true;
  } //End of method

} //End of class

?>
