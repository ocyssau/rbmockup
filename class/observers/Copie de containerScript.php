<?php
require_once('./class/observers/script_caller.php');

//Execute evenement scripts
//To associate a scripts on evenement just create a scripts in the events_scripts dir of the deposit_dir of container
//with name containerScript_[event name].php and adjust access right to this file : chown [ranchbe_user] [script file]; chmod 500 [script file];
class containerScript extends script_caller{

  protected $document; //(object document)

  function __construct(document &$document){
    global $error_stack;
    $this->error_stack =& $error_stack; //pass by reference
    $this->document =& $document; //pass by reference
    $this->args = array(&$document); //pass by reference
    $container =& $this->document->GetContainer();
    $this->scriptsDir = $container->GetProperty('default_file_path').'/__events_scripts';
    $this->eventName = NULL;
    $this->scriptFile = NULL;
    $this->functionName = NULL;
  }//End of method

  function notify($event, &$message){
    $this->eventName = $event;
    $this->scriptFile = $this->scriptsDir.'/containerScript_'.$this->eventName.'.php';
    $this->functionName = 'containerScript_'.$this->eventName;
    if( !$this->execute() ){
      $this->error_stack->push(ERROR, 'Warning', array('element1'=>$this->document->GetDocProperty('document_number'), 'element2'=>$this->scriptFile),
      'Document %element1% : Script %element2% failed or have none return. Add "return true" at function end');
      return false;
    }
    return true;
  } //End of method

} //End of class

?>
