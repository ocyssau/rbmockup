<?php
require_once'./class/datatypes/adraw.php';

/*! \brief a cadds data directory
*
*/
class adrawc5 extends adraw{

  protected $path; //(string) full path to cadds data directory
  protected $mainfile; //(string) full path to cadds main file

  public $displayMd5 = false; //true if you want return the md5 property of the file
  public $file_props; //(array) content all properties of the file.

//----------------------------------------------------------
  function __construct($path){
    $this->path = $path;
    $this->mainfile = $this->path.'/_fd';
    $this->file_props = array();
    $this->file_props['file_type'] = 'adrawc5';
    global $error_stack;
    $this->error_stack =& $error_stack;
    $this->CheckPath(); //check path to prevent lost of data
  }//End of method

} //End of class
?>
