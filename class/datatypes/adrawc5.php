<?php
require_once'./class/datatypes/adraw.php';

/*! \brief a cadds data directory
*
*/
class adrawc5 extends adraw{

//----------------------------------------------------------
  function __construct($path){
    $this->path = $path;
    $this->mainfile = $this->path.'/_fd';
    $this->file_props = array();
    $this->file_props['file_type'] = 'adrawc5';
    global $error_stack;
    $this->error_stack =& $error_stack;
    $this->CheckPath(); //check path to prevent lost of data
    $this->zipAdrawExt = '.adrawc5';
    $this->zipAdrawDatatype = 'zipadrawc5';
    //$this->initZipadraw();

    return true;
    
  }//End of method
  
} //End of class
?>
