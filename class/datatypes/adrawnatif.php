<?php
require_once'./class/datatypes/cadds.php';

/*! \brief a cadds data directory
*
*/
class adrawnatif extends cadds{

  protected $path; //(string) full path to cadds data directory
  protected $mainfile; //(string) full path to cadds main file

  public $displayMd5 = false; //true if you want return the md5 property of the file
  public $file_props; //(array) content all properties of the file.

//----------------------------------------------------------
  function __construct($path){
    $this->path = $path;
    $this->mainfile = $this->path.'/_pd';
    $this->file_props = array();
    $this->file_props['file_type'] = 'adrawc4';
    global $error_stack;
    $this->error_stack =& $error_stack;
    $this->CheckPath(); //check path to prevent lost of data
  }//End of method

//----------------------------------------------------------
/*! \brief
* 
*/
function getProperty($property_name){
	if(!is_dir($this->path))
	 return false;
  switch ($property_name){

    case 'file_name':
      if(isset($this->file_props['file_name'])) return $this->file_props['file_name'];
      return $this->file_props['file_name'] = basename(dirname($this->path)) .'/'. basename($this->path);
    break;
    
    case 'file_path':
      if(isset($this->file_props['file_path'])) return $this->file_props['file_path'];
      return $this->file_props['file_path'] = dirname(dirname($this->path));
    break;
  
    case 'file_root_name':
      if(isset($this->file_props['file_root_name'])) return $this->file_props['file_root_name'];
      return $this->file_props['file_root_name'] = basename(dirname($this->path)) .'/'. basename($this->path);
    break;

    case 'doc_name':
      if(isset($this->file_props['doc_name'])) return $this->file_props['doc_name'];
      return $this->file_props['doc_name'] = basename(dirname($this->path)).'.'.basename($this->path);
    break;

    case 'camu_path':
      if(isset($this->file_props['camu_path'])) return $this->file_props['camu_path'];
      return $this->file_props['camu_path'] = dirname($this->path);
    break;

    case 'camu_name':
      if(isset($this->file_props['camu_name'])) return $this->file_props['camu_name'];
      return $this->file_props['camu_name'] = dirname( $this->getProperty('file_name') );
    break;

    case 'adraw_name':
      if(isset($this->file_props['adraw_name'])) return $this->file_props['adraw_name'];
      return $this->file_props['adraw_name'] = basename( $this->getProperty('file_name') );
    break;

    default:
      return parent::getProperty($property_name);
    break;

  } //End of switch
}//End of method

//----------------------------------------------------------
/*! \brief move the current cadds part
* 
* \param $dst(string) fullpath to new file
* \param $replace(bool) true for replace file
*/
function move($dst , $replace=false){
  return false;
  /*
  if(!is_dir($this->path)){
    $this->error_stack->push(ERROR, 'Error', array('element'=>$this->file, 'debug'=>array()), '%element% is not a adraw directory');
    return false;
  }

  if (!filesystem::limitDir($dst)){
    $this->error_stack->push(ERROR, 'Error', array('element'=>$dst, 'debug'=>array()), 'you have no right access on file %element%');
    return false;
  }

  //Copy datas
  if($this->copy($dst,0755, $replace)){
    $this->suppress(); //suppress data
    $this->path = $dst;
    unset($this->file_props['file_name']);
    unset($this->file_props['file_path']);
    unset($this->file_props['file_root_name']);
    return true;
  }else return false;
  */
}//End of method

//----------------------------------------------------------
/*! \brief copy the current file
* 
* \param $dst(string) fullpath to new file
* \param $mode(integer) mode of the new file
* \param $replace(bool) true for replace existing file
*/
function copy($dst,$mode=0755,$replace=true){
  return false;
  /*
  if(!filesystem::limitDir($this->path)){
    $this->error_stack->push(ERROR, 'Error', array('element'=>$this->file, 'debug'=>array()), 'copy error : you have no right access on file %element%');
    return false;
  }

  if(!filesystem::limitDir($dst)){
    $this->error_stack->push(ERROR, 'Error', array('element'=>$dst, 'debug'=>array()), 'copy error : you have no right access on file %element%');
    return false;
  }

	if(is_file($dst.'/'.$this->GetProperty('file_extension')) && !$replace){
    $this->error_stack->push(ERROR, 'Error', array('element'=>$dst.'/'.$this->GetProperty('file_extension'), 'debug'=>array()), 'copy error: file %element% exist');
    return false;
	}

  if( !is_dir( dirname($dst) ) ){ //create the camu directory if not exist
    if( !rdirectory::createdir( dirname($dst) ) ) {
      $this->error_stack->push(ERROR, 'Error', array('element'=>dirname($dst)), 'can not create camu directory %element%');
      return false;
    }
  }

	if(is_dir($this->path)){
    if(rdirectory::dircopy($this->path, $dst ,false, true, true, 0755)){
      touch($dst.'/'.$this->getProperty('file_extension'), filemtime($this->mainfile));
      chmod ($dst.'/'.$this->getProperty('file_extension') , $mode);
      return true;
    }
	}

  $this->error_stack->push(ERROR, 'Error', array('element'=>$this->file, 'debug'=>array()), 'copy error on cadds part %element%');
  return false;
  */
}//End of method

} //End of class
?>
