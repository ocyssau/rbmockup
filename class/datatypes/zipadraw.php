<?php
require_once ('class/common/file.php');

/*! \brief an zipped adraw cadds data file
*
*/
class zipadraw extends file{

  //protected $path; //(string) full path to cadds data directory
  protected $mainfile; //(string) full path to cadds main file
  protected $reposit_sub_dir='__caddsDatas'; //string sub directory of reposit dir where are stored the cadds datas

//----------------------------------------------------------
function __construct($file){
  $this->file = $file;
  $this->file_props = array();
  $this->file_props['file_type'] = 'zipadraw';
  $this->file_type =& $this->file_props['file_type'];
  global $error_stack;
  $this->error_stack =& $error_stack;

  return true;
}//End of method


} //End of class
?>
