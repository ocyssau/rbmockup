<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*Events :
 onExtractBegin, //Begin the extraction of archive file
 onExtractEnd, //End of extraction
 onUpdateFile, //Update file record in database
 onCreateFile, //Create a new file in database
 */

//require_once('./class/common/basic.php');
require_once('class/common/observable.php');
require_once('class/common/recordfile.php');
require_once('class/Import/Log.php');
require_once 'File/Archive.php'; //File_Archive PEAR package extension


/*! \brief This class manage the import of files or documents in the containers.
 */

/*! L'importation en 3 etapes :
 1>Lister le contenu du TDP,
 Verifier l'existence du fichier xxxx.tdp (voir normes)
 Pour chaque fichier tester s'il existe dans le conteneur cible ou dans un autre conteneur de l'espace,
 Verifier que le fichier exitant est libre ou que l'on peut l'indicer(acode = 0 ou acode >= 10)
 Tester le doctype de chaque fichier,
 2>Afficher a l'utilisateur les fichiers � importer avec le choix :
 Mettre � jour ou cr�er un nouveaux indice si le fichier existe,
 Cr�er un nouveau document sinon,
 Pour les fichiers qui ne correspondent pas a un doctype, proposer de les renommer,
 Proposer chaque champs de saisie par m�tadonn�e pour saisie,
 Si le fichier existe est qu'il est en cours de modification ou dans une boucle de validation, informer utilisateur et ignorer,
 R�solution des conflits :
 Le tdp d�finis des m�tadonn�es differentes du document existant : le tdp est prioritaire
 sauf dans le cas des champs vide du tdp.
 3>Valider les choix. La mise a jour s'execute, on reverifie les documents qui ont �t� renom�s(doctype) et on repropose les choix et selection.
 Un message de succ�s est retourn�.

 Le TDP : Transfert data package

 */

class Rb_Datatype_Package extends rb_observable
{
	const STATE_INPROGRESS = 'InProgress';
	const STATE_SUCCESS = 'InContainer';
	const STATE_FAILED = 'Failed';
	
	protected $_id = null; //(Integer)
	protected $_state = null;
	protected $_file = null;
	protected $_usr = null; //(Object)
	protected $_fileName = null;
	protected $_filePath = null;
	protected $_extension = null;
	protected $_size = null;
	protected $_mtime = null;
	protected $_md5 = null;
	protected $_importBy = null;
	protected $_imported = null;
	protected $_containerId = null;
	protected $_log = null;
	protected $_errorlog = null;
	protected $_contentlog = null;
	protected $_description = null;
	protected $spaceName = null;
	
	public $lastErrors = array();
	
	protected $dbranchbe = null; //(Object)
	
	function __construct( $spaceName, $properties = array() )
	{
		global $dbranchbe;
		$this->dbranchbe =& $dbranchbe;
		global $error_stack;
		$this->error_stack =& $error_stack;
		global $usr;
		$this->_usr =& $usr;

		$this->OBJECT_TABLE  = $spaceName . '_import_history';

		$this->FIELDS_MAP_ID = 'import_order';
		$this->FIELDS_MAP_NUM = 'package_file_name';
		$this->FIELDS_MAP_DESC = 'description';
		$this->FIELDS_MAP_STATE = 'state';
		
		if($properties){
			$this->loadFromArray($properties);
		}
	} //End of construct
	
	
	/**
	 * @param string
	 */
	function setState($str)
	{
		$this->_state = $str;
	}//End of method
	
	
	/**
	 * @param integer
	 */
	function setJob($integer)
	{
		$this->_jobId = $integer;
	}//End of method
	
	
	/**
	 * @param integer
	 */
	function setContainer($integer)
	{
		$this->_containerId = $integer;
	}//End of method
	
	
	/**
	 * @param string
	 */
	function setDescription($str)
	{
		$this->_description = trim($str);
	}//End of method
	
	
	/**
	 * @param string
	 */
	function setFile($str)
	{
		if( is_file($str) ){
			$this->_file = realpath($str);
			$this->_fileName = basename($this->_file);
			$this->_filePath = dirname($this->_file);
			$this->_extension = substr($this->_file, strrpos($this->_file, '.'));
			$this->_size = filesize($this->_file);
			$this->_mtime = filemtime($this->_file);
			$this->_md5 = md5_file($this->_file);
		}
		else{
			trigger_error($str . ' is not a file');
		}
	}//End of method
	
	
	/**
	 * @param string
	 */
	function getFile()
	{
		return $this->_file;
	}//End of method
	
	
	/**
	 * @return array
	 */
	public function getProperties()
	{
		foreach( $this as $var=>$val){
			if( is_object($var) ){
				continue;
			}
			$var = trim( $var, '_');
			$ret[$var] = $val;
		}
		return $ret;
	}
	
	
	/**
	 * 
	 */
	function getProperty($pname)
	{
		$pname = '_'.$pname;
		return $this->$pname;
	}
	
	
	/**
	 * @param array $params
	 */
	function loadFromArray($datas)
	{
		$this->_id 				= $datas[$this->FIELDS_MAP_ID];
		$this->_description 	= $datas['description'];
		$this->_state 			= $datas['state'];
		$this->_fileName 		= $datas['package_file_name'];
		$this->_filePath 		= $datas['package_file_path'];
		$this->_extension 		= $datas['package_file_extension'];
		$this->_mtime 			= $datas['package_file_mtime'];
		$this->_size 			= $datas['package_file_size'];
		$this->_md5 			= $datas['package_file_md5'];
		$this->_importBy 		= $datas['import_by'];
		$this->_imported 		= $datas['import_date'];
		$this->_log 			= $datas['import_logfiles_file'];
		$this->_errorlog 		= $datas['import_errors_report'];
		$this->_contentlog 		= $datas['import_content'];
		$this->_containerId 	= $datas['mockup_id'];
		$this->_jobId 			= $datas['jobid'];
		$this->_file =  $this->_filePath . '/' . $this->_fileName;
	}//End of method
	
	/**
	 *
	 * @param array $params
	 */
	public function update()
	{
		//if no error, write infos in database.
		//$datas[$this->FIELDS_MAP_ID]  		= $this->_id;
		$data['description']		  		= $this->_description;
		$datas['state']               		= $this->_state;
		$datas['package_file_name']			= $this->_fileName;
		$datas['package_file_path']   		= $this->_filePath;
		$datas['package_file_extension']   	= $this->_extension;
		$datas['package_file_mtime']   		= $this->_mtime;
		$datas['package_file_size']   		= $this->_size;
		$datas['package_file_md5']   		= $this->_md5;
		$datas['import_by']           		= $this->_importBy;
		$datas['import_date']         		= $this->_imported;
		$datas['import_logfiles_file']		= $this->_log;
		$datas['import_errors_report']		= $this->_errorlog;
		$datas['import_content']			= $this->_contentlog;
		$datas['mockup_id']   				= $this->_containerId;
		$datas['jobid']   					= $this->_jobId;
		return $this->BasicUpdate($datas, $this->_id);
	}//End of method
	
	
	/** Import the content of the package file in the container.
	 *  Return true or false.
	 *
	 * This method list the files that they were imported from the package $import_order.
	 * If a file has been imported twice, only the last package source file can be list.
	 * @param container	$Container identifiant of the target container.
	 * @param boolean	$lock If true, the files or the document will be locked after import.
	 * @return boolean
	 */
	public function import($Container, $lock = false)
	{
		$success = true; //false if a critical error is occuring
		$this->lastErrors = array();
		$targetDir = $Container->GetProperty('default_file_path');
		$regex = $Container->GetProperty('package_regex');
		$containerId = $Container->GetId();
		//$targetContainerNum = $Container->GetName();
		$logdir = $Container->GetProperty('default_file_path').'/__import_logs';
		$this->_log = $logdir . '/' . $this->_id . '_log' . '.txt';
		$this->_contentlog = $logdir . '/' . $this->_id . '_content' . '.txt';
		$this->_errorlog = '';
		$LoggerLog = new Rb_Import_Log( $this->_log );
		$LoggerCont = new Rb_Import_Log( $this->_contentlog );
		
		if($regex){
			$ok = preg_match( "/$regex/i", $this->_fileName );
			if( $ok == 0 ){
				$this->error_stack->push( ERROR, 'Warning', array(), 'The package file '.$this->_fileName.' dont match the define regex for this container' );
				return false;
			}
		}
		
		$this->setState(self::STATE_INPROGRESS);
		$this->update();
		
		$PearFileArchive = $this->unpack( $targetDir );
		
		if( $PearFileArchive == false ){
			$this->setState('error');
			$this->update();
			return false;
		}
		
		$fdiskSpace = disk_free_space($targetDir);
		$countFile = count( glob($targetDir .'/*') ) - 2;
		
		while( $PearFileArchive->next() ){
			$file = $PearFileArchive->getFilename();
			$LoggerCont->log($file);
			$recordfile = new recordfile($Container->space);
			$fsdata =& $recordfile->initFsdata($file);
			if( !is_object($fsdata) ) {
				if( is_dir($file) ){
					$this->lastErrors[] = "Error during importing of $file, is a directory";
				}
				else{
					$this->lastErrors[] = 'Error during importing of : '.$file.' : file do not existing on filesystem' . "(FREE DISK SPACE=$fdiskSpace; COUNT FILE=$countFile)";
					$success = false;
				}
				continue;
			}

			if( $file_id = $recordfile->GetFileId( $fsdata->getProperty('file_name') ) ){ //Test if file exist in this container
				$this->notify_all( 'onUpdateFile' , $this, $recordfile );
				$recordfile->init($file_id); //init the find recordfile
				if( $recordfile->GetProperty('file_md5') != $fsdata->getProperty('file_md5') ){ // md5 are differents
					$data['file_version'] = $recordfile->GetProperty('file_version') + 1;
					$data['import_order'] = $import_order;
					$data['file_update_date'] = time();
					$data['file_update_by'] = $this->_usr->getProperty('auth_user_id');
					$data['file_md5'] =  $fsdata->getProperty('file_md5');
					$data['file_size'] = $fsdata->getProperty('file_size');
					$data['file_mtime'] = $fsdata->getProperty('file_mtime');
					$data[$this->spaceName . '_id'] = $containerId;
					$data['file_path'] = $fsdata->getProperty('file_path');
					$recordfile->UpdateRecordFile($data); //update file, upgrade version, record source package
					$LoggerLog->log( 'replace '.$file.' new version : '.$data['file_version'] );
				}
				else{ // md5 are equal, just update container
					$data[$this->spaceName . '_id'] = $containerId;
					$data['file_path'] = $fsdata->getProperty('file_path');
					$data['import_order'] = $import_order;
					$data['file_update_date'] = time();
					$data['file_update_by'] = $this->_usr->getProperty('auth_user_id');
					$recordfile->UpdateRecordFile($data);
					$LoggerLog->log( 'The file '.$file.' is not updated : imported file is same that existing file.' );
				}
			}
			else{ //The file dont exist
				$this->notify_all( 'onCreateFile' , $this );
				$recordfile->SetImportOrder($this->_id);
				$recordfile->SetFatherId($containerId);
				$ok = $recordfile->RecordFile($fsdata);
				if( !$ok ){ //Record the file
					$this->lastErrors[] = 'Error during record of '.$file;
					$success = false;
				}
				else{
					$LoggerLog->log('create '.$file);
				}
			}
			unset($fsdata);
			unset($recordfile);
		}
		
		//Write the errors log
		if( count($this->lastErrors) > 0 ){
			$this->_errorlog = $logdir . '/' . $this->_id . '_errors' . '.txt';
			$LoggerErr = new Rb_Import_Log( $this->_errorlog );
			foreach($this->lastErrors as $err){
				$LoggerErr->log($err);
			}
		}
		
		$this->setState('inContainer');
		$this->_containerId = $containerId;
		$this->_importBy = $this->_usr->getProperty('auth_user_id');
		$this->_imported = time();
		$this->update();
		return $success;
	}
	
	
	/** 
	 * Uncompress package in $targetDir.
	 * Return true or false.
	 *
	 * @param string $file		File path of the package file to uncompress.
	 * @param string $targetDir Path of the dir where put files of the package file.
	 * @return File_Archive type
	 */
	function unpack($targetDir)
	{
		$file = $this->_file;
		
		if( !is_dir($targetDir) ) {
			return false;
		}
		
		if( !is_file( $file ) ) {
			return false;
		}

		if( $this->_extension == '.Z' ){
			if(exec(UNZIPCMD . " $file")){
				$file = rtrim($this->_file , '.Z');
				$this->setFile($file);
				$this->update();
			}else{
				$this->error_stack->push(ERROR, 'Fatal', array('element'=>$file, 'debug'=>array()), 'cant uncompress this file %element%');
				return false;
			}
		}

		$this->notify_all( 'onExtractBegin' , $this );

		if(substr($this->_fileName, strrpos($this->_fileName, '.')) == '.tar'){
			$cmd = 'tar -xvf '.$file.' -C '.$targetDir;
			exec($cmd, $output, $return_var);

			if( !$output ){
				print 'extraction has failed for unknow error<br />';
				print 'or archive is maybe empty<br />';
				return false;
				die;
			}else{
				print '<b>Last tar message: ';
				print end($output).'</b><br />';
			}

			$result = File_Archive::read($file.'/', $targetDir.'/');
		}else{
			//See File_Archive PEAR package:
			File_Archive::extract(
				$result =  File_Archive::read($file.'/', $targetDir.'/'),
				File_Archive::toFiles()
			);
			$result->close();  //Move back to the begining of the source
		}

		//return only files that are in root of archive :
		/*
		$iterator = 0;
		$unpack = array();
		while($result->next()){ //for each unpack file
			$filename = $result->getFilename(); // = c:/mockup_dir/mockup/__imported/caddsPart/_pd
			$unpack[] = $filename; //Get the name of the unpack file
			$iterator ++;
		}
		$unpack = array_unique($unpack);
		$this->notify_all( 'onExtractEnd' , $this , array('count'=>$iterator, 'list'=>$unpack) );
		*/
		$this->notify_all( 'onExtractEnd' , $this , $result );

		if(PEAR::isError($result)){
			$this->error_stack->push(ERROR, 'Fatal', array(), $result->getMessage() .'<br />'. $result->getUserInfo());
			return false;
		}
		
		$result->close();
		return $result;
	}//End of method
	
	/**
	 * 
	 * @param string $dir
	 */
	public static function checkDirCount($directory, $add=0)
	{
		$count = count(glob($directory . "/*")) + $add;
		//$max = MAX_FILES_IN_DIR;
		$max = 60000;
		if( $count > $max ){
			return false;
		}
		else{
			return true;
		}
	}
	
	/**
	 * 
	 * @param string $dir
	 */
	public static function checkDiskSpace($directory, $size)
	{
		$dipo = disk_free_space ( $directory );
		if ($size > $dispo){
			return false;
		}
		else{
			return true;
		}
	}
	
	/**
	 * Display the content of the package file.
	 * 
	 * @param string $file		File path of the package file to uncompress.
	 * @param boolean $getArray	If false, return File_Archive object
	 */
	public static function getContent( $file, $getArray = true )
	{
		if(!is_file($file)) {
			return false; //To prevent lost of data
		}
		//See File_Archive PEAR package :
		$result =  File_Archive::read("$file/");
		$result->close();  //Move back to the begining of the source
		if($getArray){
			$i=0;
			while($result->next()){ //for each unpack file
				$res[$i] = $result->getFilename(); //Get the name of the unpack file
				$res[$i]['stat'] = $result->getStat();
				$i++;
			}
			return $res;
		}else{
			return $result;
		}
	}//End of method

}//End of class

