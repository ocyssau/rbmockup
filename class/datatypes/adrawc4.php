<?php
require_once'./class/datatypes/adraw.php';

/*! \brief a cadds data directory
*
*/
class adrawc4 extends adraw{

//----------------------------------------------------------
  function __construct($path){
    $this->zipAdrawExt = '.adrawc4';
    $this->zipAdrawDatatype = 'zipadrawc4';
    $this->reposit_sub_dir = '__caddsDatas';
    return parent::__construct($path);

  }//End of method

} //End of class
?>
