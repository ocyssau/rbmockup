<?php
require_once'./class/datatypes/cadds.php';

/*! \brief a cadds data directory
*
*/
class camu extends cadds{

  protected $path; //(string) full path to cadds data directory
  protected $mainfile; //(string) full path to cadds main file
  protected $adraws; //(array) fsdata objects of adraws

  public $displayMd5 = false; //true if you want return the md5 property of the file
  public $file_props; //(array) content all properties of the file.

//----------------------------------------------------------
function __construct($path){
    $this->path = $path;
    $this->mainfile = $this->path.'/_db';
    $this->file_props = array();
    $this->file_props['file_type'] = 'camu';
    global $error_stack;
    $this->error_stack =& $error_stack;
    $this->CheckPath(); //check path to prevent lost of data
}//End of method

//----------------------------------------------------------
/*! \brief suppress the current cadds directory
*   Returns TRUE or FALSE
*/
function suppress(){
  //Test if they are adraw in this directory...if true, dont suppress it
  if($this->GetAdraws()){
    $this->error_stack->push(INFO, 'Info', array('element'=>$this->path, 'debug'=>array()), 'camu directory %element% contains adraws. Camu directory will be not suppressed.');
  }
  if(!$this->putInTrash()){
    $this->error_stack->push(ERROR, 'Error', array('element'=>$this->path, 'debug'=>array()), 'suppress of camu directory %element% failed');
    return false;
  }else return true;
}//End of method

//-------------------------------------------------------------------------------
/*! \brief put cadds directory in the trash dir
*   Returns TRUE or FALSE
*/
function putInTrash($verbose = false, $trashDir=DEFAULT_TRASH_DIR){
  //Test if they are adraw in this directory...if true, dont suppress it
  if($this->GetAdraws()){
    $this->error_stack->push(INFO, 'Info', array('element'=>$this->path, 'debug'=>array()), 'camu directory %element% contains adraws. Camu directory will be not suppressed.');
  }
  return rdirectory::putDirInTrash($this->path, $verbose, false, $trashDir);
}//End of function

//----------------------------------------------------------
/*! \brief copy the current file
* 
* \param $dst(string) fullpath to new path
* \param $mode(integer) mode of the new file
* \param $replace(bool) true for replace existing file
*/
function copy($dst,$mode=0755,$replace=true){
  if(!filesystem::limitDir($this->path)){
    $this->error_stack->push(ERROR, 'Error', array('element'=>$this->file, 'debug'=>array()), 'copy error : you have no right access on file %element%');
    return false;
  }

  if(!filesystem::limitDir($dst)){
    $this->error_stack->push(ERROR, 'Error', array('element'=>$dst, 'debug'=>array()), 'copy error : you have no right access on file %element%');
    return false;
  }

	if(is_file($dst.'/'.$this->GetProperty('file_extension')) && !$replace){
    $this->error_stack->push(ERROR, 'Error', array('element'=>$dst.'/'.$this->GetProperty('file_extension'), 'debug'=>array()), 'copy error: file %element% exist');
    return false;
	}

	if(is_file($this->mainfile)){
    if(rdirectory::dircopy($this->path, $dst ,false, true, false, 0755)){
      touch($dst.'/'.$this->getProperty('file_extension'), filemtime($this->mainfile));
      chmod ($dst.'/'.$this->getProperty('file_extension') , $mode);
      return true;
    }
	}

  $this->error_stack->push(ERROR, 'Error', array('element'=>$this->file, 'debug'=>array()), 'copy error on cadds part %element%');
  return false;

}//End of method

//----------------------------------------------------------
/*! \brief move the current cadds part
* 
* \param $dst(string) fullpath to new file
* \param $replace(bool) true for replace file
*/
function move($dst , $replace=false){
  if(!is_dir($this->path)){
    $this->error_stack->push(ERROR, 'Error', array('element'=>$this->file, 'debug'=>array()), '%element% is not a cadds directory');
    return false;
  }

  if (!filesystem::limitDir($dst)){
    $this->error_stack->push(ERROR, 'Error', array('element'=>$dst, 'debug'=>array()), 'you have no right access on file %element%');
    return false;
  }

  if(!is_dir(dirname($dst))){
    $this->error_stack->push(ERROR, 'Error', array('element'=>dirname($dst), 'debug'=>array()), 'the directory %element% dont exist');
    return false;
  }

  //Copy file , and suppress file
  if(rdirectory::dircopy($this->path, $dst ,false, true, false, 0755)){
    $this->suppress();
    $this->path = $dst;
    unset($this->file_props['file_name']);
    unset($this->file_props['file_path']);
    unset($this->file_props['file_root_name']);
    return true;
  }else return false;

}//End of method

//--------------------------------------------------------
/*! \brief get adraw from a camu directory
*   Return an array of fsdata object if no errors else return false
*   
* \param $init(bool) if true, re-scan camu directory, else return existing $adraws array
*/
function GetAdraws($init = false){
  if(isset($this->adraws) && !$init) return $this->adraws;
  if(!$handle  = opendir($this->path)){
    print 'failed to open ' . $this->path . '<br>';
    return false ;
  }
  require_once('./class/common/fsdata.php');
  while ($item = @readdir($handle)){ //list adraw of the directory "camu"
    if( $item == '.' || $item == '..' ) continue;
    $path = $this->path.'/'.$item;
    if(is_dir($path)){
      $datatype=fsdata::SetDataType($path);
      //Check if subdir is a caddspart
      if ($datatype['type'] == 'adrawc4' || $datatype['type'] == 'adrawc5' ){
        $this->adraws[] =& fsdata::_dataFactory($path);
      }else{print 'error: presence of a unknow subdir in camu directory <br />';}
    } //End of is_dir
 	} //End of while

  if(is_array($this->adraws)) return $this->adraws;
  return false;
}//End of method

} //End of class
?>
