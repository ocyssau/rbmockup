<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

class freeze{

protected $container;
protected $freezeReposit;
protected $serializedReposit;

function __construct(container &$container){
  global $error_stack;
  $this->error_stack =& $error_stack;
  $this->container =& $container;
  $this->freezeReposit = $this->container->GetProperty('default_file_path').'/__freeze';
  $this->serializedReposit = $this->freezeReposit.'/__serialized_datas';

  require_once('class/serialize.php');
  $this->serialize = new serialize();

}//End of method

//------------------------------------------------------------------------------
function InitDirectory(){
  if(!is_dir($this->freezeReposit)){ //Check if reposit dir is existing
    if(!rdirectory::createdir($this->freezeReposit, 0755)){ //..and create it if not
      $this->error_stack->push(ERROR, 'Fatal', array( 'element'=>$this->freezeReposit ), 'cant create directory : %element%.');
      return false;
    }
  }
  if(!is_dir($this->serializedReposit)){ //Check if reposit dir is existing
    if(!rdirectory::createdir($this->serializedReposit, 0755)){ //..and create it if not
      $this->error_stack->push(ERROR, 'Fatal', array( 'element'=>$this->serializedReposit ), 'cant create directory : %element%.');
      return false;
    }
  }
  return true;
}//End of method

//------------------------------------------------------------------------------
function freezeDocument( document &$document ){
  if( !$this->InitDirectory() ) return false;

  //Create serialized document file
  $serialized_data_file = $document->GetDocProperty('document_number').'__'.$document->GetDocProperty('document_indice_id').'__'.$document->GetDocProperty('document_version').'.document_serialized.xml';
  $serialized_data_file = $this->serializedReposit.'/'.$serialized_data_file;
  if( !$serialized_data_handle = fopen( $serialized_data_file, 'w' ) ) {
    $this->error_stack->push(ERROR, 'Fatal', array( 'file'=>$serialized_data_file ), 'cant open file : %file%.');
    return false;  
  }
  fwrite($serialized_data_handle, $this->serialize->serializeDocument($document) );
  fclose($serialized_data_handle);

  //Create serialized docfiles file
  $docfiles =& $document->GetDocfiles();
  if( is_array( $docfiles ) )
  foreach( $docfiles as $docfile ){
    if( !$this->freezeRecordfile($docfile) ){
      $this->error_stack->push(ERROR, 'Fatal', array( 'document'=>$document->GetDocProperty('document_normalized_name') ), 'an error is occured during freeze of document  : %document%.');
      return false;
    }
  }

  return true;

}//End of method

//------------------------------------------------------------------------------
function freezeDocfile( docfile &$docfile ){
  if( !$this->InitDirectory() ) return false;

  $serialized_data_file = $docfile->GetProperty('file_name').'__'.$docfile->GetProperty('file_version').'.docfile_serialized.xml';
  $serialized_data_file = $this->serializedReposit.'/'.$serialized_data_file;
  if( !$serialized_data_handle = fopen( $serialized_data_file, 'w' ) ) {
    $this->error_stack->push(ERROR, 'Fatal', array( 'file'=>$serialized_data_file ), 'cant open file : %file%.');
    return false;  
  }
  fwrite( $serialized_data_handle, $this->serialize->serializeDocfile($docfile) );
  fclose( $serialized_data_handle );

  return $docfile->CopyFile( $this->freezeReposit, true ); //replace files if exists

}//End of method

//------------------------------------------------------------------------------
function freezeRecordfile( recordfile &$recordfile ){
  if( !$this->InitDirectory() ) return false;

  $serialized_data_file = $recordfile->GetProperty('file_name').'__'.$recordfile->GetProperty('file_version').'.docfile_serialized.xml';
  $serialized_data_file = $this->serializedReposit.'/'.$serialized_data_file;
  if( !$serialized_data_handle = fopen( $serialized_data_file, 'w' ) ) {
    $this->error_stack->push(ERROR, 'Fatal', array( 'file'=>$serialized_data_file ), 'cant open file : %file%.');
    return false;  
  }
  fwrite( $serialized_data_handle, $this->serialize->serializeObject($recordfile) );
  fclose( $serialized_data_handle );

  return $recordfile->CopyFile( $this->freezeReposit, true ); //replace files if exists

}//End of method


}
?>
