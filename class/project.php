<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('./class/common/basic.php');

/*! \brief This class manage the projects.
* A project group containers and define property and permission common to workitem containers linked to the project.
*/
class project extends objects{

  public $SPACE_NAME  = 'project';
  public $AREA_ID = 5;
  protected $OBJECT_TABLE = 'projects'; //(String)Table where are stored the containers definitions
  protected $HISTORY_TABLE = 'project_history'; //(String)Table were are stored the container histories
  protected $INDICE_TABLE  = 'container_indice'; //(String)Table where are stored the container indices
  //Fields:
  protected $FIELDS_MAP_ID = 'project_id'; //(String)Name of the field of the primary key
  protected $FIELDS_MAP_NUM = 'project_number'; //(String)Name of the field to define the name
  protected $FIELDS_MAP_DESC = 'project_description'; //(String)Name of the field where is store the description
  protected $FIELDS_MAP_STATE = 'project_state'; //(String)Name of the field where is store the state
  protected $FIELDS_MAP_INDICE = 'project_indice_id'; //(String)Name of the field where is store the indices
  protected $FIELDS_MAP_FATHER; //(String)Name of the field where is store the id of the father
  //protected $METADATA_TABLE; //(String)

  protected $dbranchbe; //(Object)
  protected $usr; //(Object)
  public $history; ////(Array) contains the properties of the current container
  public $error_stack; //(Object)
  private $core_prop; //(Array)
  protected $category; ////(Object category)

  //public $TABLE_SEQ     = 'project_seq';
  //public $SPEC_FIELDS = array('project_id' , 'project_number' , 'project_state' , 'open_by' , 'forseen_close_date' , 'open_date' , 'close_date' , 'close_by' , 'project_description' , 'project_indice_id' , 'default_process_id', 'area_id' , 'project_indice_id' , 'default_process_id');

//-------------------------------------------------------------------------
function __construct(space &$space , $project_id=NULL){
  $this->space =& $space;
  $this->error_stack =& $this->space->error_stack;

  /*foreach(get_object_vars($this->space) as $var=>$value){
    $this->$var = $value;
  }*/

  global $dbranchbe;
  $this->dbranchbe =& $dbranchbe;

  global $usr;
  $this->usr =& $usr;

  if(!is_null($project_id))
      $this->init($project_id);

}//End of method

//-------------------------------------------------------------------------
static function _factory($project_id=NULL){
  require_once('./class/common/space.php');
  $space=new space('project');
  return new project($space, $project_id); //Create new manager
}//End of method

//----------------------------------------------------------
/*! \brief Get the property of the container by the property name. init() must be call before
* The property name are mapped with the properties name of the container to call the project property like a call to container property
* This mapping is useful to factorise the links management pages
* \param $property_name(string) = container_id,container_number,container_description,container_state,container_indice_id.
*/
function GetProperty($property_name){
  if(!isset($this->project_id)){
    $this->error_stack->push(ERROR, 'Error', array(), '$this->project_id is not set');
    return false;}
  
  switch($property_name){
    case 'container_number':
        if(isset($this->core_prop[$this->FIELDS_MAP_NUM])){
          return $this->core_prop[$this->FIELDS_MAP_NUM];
        }else $realName = $this->FIELDS_MAP_NUM;
    break;  
  
    case 'container_description':
        if(isset($this->core_prop[$this->FIELDS_MAP_DESC]))
          return $this->core_prop[$this->FIELDS_MAP_DESC];
        else $realName = $this->FIELDS_MAP_DESC;
    break;  
  
    case 'container_state':  
        if(isset($this->core_prop[$this->FIELDS_MAP_STATE]))
          return $this->core_prop[$this->FIELDS_MAP_STATE];
        else $realName = $this->FIELDS_MAP_STATE;
    break;  
    
    case 'container_indice_id':
        if(isset($this->core_prop[$this->FIELDS_MAP_INDICE]))
          return $this->core_prop[$this->FIELDS_MAP_INDICE];
        else $realName = $this->FIELDS_MAP_INDICE;
    break;  
    
  	default:
      if(isset($this->core_prop[$property_name]))
        return $this->core_prop[$property_name];
      else $realName = $property_name;
    break;  
  
  } //End of switch
  
  $query = 'SELECT '.$realName.' FROM '.$this->OBJECT_TABLE.' WHERE '.$this->FIELDS_MAP_ID.' = \''.$this->project_id.'\'';
  $res = $this->dbranchbe->GetOne($query);
  if($res === false){
    $this->error_stack->push(ERROR, 'Fatal', array('query'=>$query , 'debug'=>array($this->container_id)), $this->dbranchbe->ErrorMsg());
    return false;}
  else
    return $this->core_prop[$property_name] = $res;
}//End of method

//-------------------------------------------------------------------------
/*! \brief init the property of the project
*  Return true
* 
* \param $project_id(integer) Id of the project
*/
function init($project_id){
  $this->project_id = $project_id;
  if(isset($this->core_prop)) unset($this->core_prop);
  if(isset($this->category)) unset($this->category);
  if(isset($this->history)) unset($this->history);
  return true;
}//End of method

//-------------------------------------------------------------------------
/*!\brief Return the id of the selected project recorded in session.
*/
static function GetCurrentProjectId(){
  if(isset($_SESSION['SelectedProject']))
    return $_SESSION['SelectedProject'];
  else return false;
}//End of method

//-------------------------------------------------------------------------
/*!\brief Return the number of the selected project recorded in session.
*/
static function GetCurrentProjectNum(){
  if(isset($_SESSION['SelectedProject']))
    return $_SESSION['SelectedProjectNum'];
  else return false;
}//End of method

//-------------------------------------------------------------------------
/*!\brief Create a project.
 * Return the project id if no errors, else return false.
 *    
\param $params(array) field=>value of the properties to record
*/
function Create($params){
  if(!isset($params["$this->FIELDS_MAP_NUM"])) return 'ObjectCreate error : no number specified';
  //Defined rights associated to this project
  $perms = array ( 'area_suffix' => 'project_' ,
                   'rights' => GetInContextRight(), //Defined in ranchbe_setup.php ,
                 );
  $allow_input = array( 'project_number',
                        'project_description',
                        'project_indice',
                        'project_state',
                        'forseen_close_date',
                        'close_date',
                        'close_by',
                        'default_process_id',
                      );
  //Filter input data for prevent maliscious informations
  foreach ( $allow_input as $input) {
    if (isset($params["$input"]))
      $data["$input"] = $params["$input"];
  }
  $data['project_state'] = 'init';
  if(!isset($data['forseen_close_date'])) $data['forseen_close_date']= time() + ( 3600 * 24 * $DEFAULT_LIFE_TIME );
  if(!$project_id = $this->BasicCreate($data,$perms)) return false;
  //Write history
  $this->history['action_name'] = 'Create';
  $this->WriteHistory($project_id);
  return $project_id;
}//End of method

//-------------------------------------------------------------------------
/*!\brief Modify a project.
* Return true or false.
*   
\param $params(array) field=>value of the properties to change.
\param $project_id(integer) id of the project to modify.
*/
function Update($params,$project_id){
  $allow_input = array( 'project_number',
                        'project_description',
                        'project_indice',
                        'project_state',
                        'forseen_close_date',
                        'close_date',
                        'close_by',
                        'default_process_id',
                      );
  //Filter input data for prevent maliscious informations
  foreach ( $allow_input as $input){
    if (isset($params["$input"]))
      $data["$input"] = $params["$input"];
  }
  $ret=$this->BasicUpdate( $data , $project_id);
  //Write history
  $this->history['action_name'] = 'Update';
  $this->WriteHistory($project_id);
  return $ret;
}//End of method

//-------------------------------------------------------------------------
/*!\brief Suppress a project.
 * Return true or false.
 *    
  \param $project_id(integer) id of the project to suppress.
*/
function Suppress($project_id){
  //Get infos about the project
  $this->history = $this->GetProjectInfos($project_id);
  if($ret = $this->BasicSuppress($project_id)){
    //Suppress liveuser area and rights associated
    require_once './lib/userslib.php';
    if(!$userlib->suppress_area($this->history['area_id']))
      $this->error_stack->push(ERROR, 'Fatal', array(), 'error when suppress area');
    //Write history
    $this->history['action_name'] = 'Suppress';
    $this->WriteHistory($project_id);
  }
  return $ret;
}//End of method

//-------------------------------------------------------------------------
/*!\brief Get all projects.
 * Return array or false.
 *    
  \param $params(array) is use for manage the display. See parameters function of GetQueryOptions().
*/
function GetAllProjects($params = ''){
  return $this->GetAllBasic($params);
}//End of method

//-------------------------------------------------------------------------
/*!\brief Get infos about a project.
 * Return array or false.
 *    
  \param $project_id(integer) id of the project.
*/
function GetProjectInfos($project_id){
  return $this->GetBasicInfos($project_id);
}//End of method

//----------------------------------------------------------
/*!\brief Return the functionnal area of the project.
 * Return area_id or false.
 *    
  \param $project_id(integer) id of the project.
*/
function GetProjectArea($project_id = NULL){
  if( is_null($project_id) ) $project_id = $this->project_id;
  $query = 'SELECT area_id'
           .' FROM '.$this->OBJECT_TABLE
           .' WHERE '.$this->FIELDS_MAP_ID.' = \''.$project_id.'\'';
  $one = $this->dbranchbe->GetOne($query);
  if (!$one){return false;}
  else {return $one;}
}//End of method

//----------------------------------------------------------
/*!\brief Return the number of a project from his id.
* Return string or false.
*    
\param $project_id(integer) id of the project.
*/
function GetProjectNumber($project_id){
  return $this->GetBasicNumber($project_id);
}//End of method

function GetName(){
  return $this->GetBasicNumber($this->project_id);
}//End of method
//----------------------------------------------------------
/*!\brief Return the default process id of the project.
* Return integer or false.
*    
\param $project_id(integer) id of the project.
*/
function GetProcessId( $project_id ){
  $select[] = 'default_process_id';
  $result = $this->GetBasicInfos($project_id , $select);
  if (!$result) return false;
  else return $result['default_process_id'];
}//End of method

//-------------------------------------------------------------------------
/*!\brief Get all ranchbe components linked to this project.
* Return array or false.
*
\param $project_id(integer) id of the project.
\param $filter(array)
*/
function GetLinkChildsDetail($project_id , $filter=''){
  require_once './class/common/container.php';
  $workitemsManager  = container::_factory('workitem');
  require_once './class/process.php';
  $processManager    = new process($this->space);
  
  $params['exact_find']['project_id']  = $project_id;
  
  $workitems   = $workitemsManager->GetAllBasic($params);
  $process     = $processManager->GetInfos($this->getProcessId($project_id));
  
  $partners    = $this->GetPartnersLinked($project_id);
  $doctypes    = $this->GetDoctypesLinked($project_id);
  $mockups     = $this->GetMockupsLinked($project_id);
  $cadlibs     = $this->GetCadlibsLinked($project_id);
  $bookshops   = $this->GetBookshopsLinked($project_id);
  
  /*echo 'debub GetLinkChildsDetail in project:<br>';
  var_dump($workitems);
  var_dump($process);
  var_dump($partners);
  var_dump($doctypes);
  var_dump($mockups);
  var_dump($cadlibs);
  var_dump($bookshops);*/
  
  $objectsList=array();
  if ($workitems) $objectsList = $workitems;
  if ($mockups)   $objectsList = array_merge($mockups , $objectsList );
  if ($cadlibs)   $objectsList = array_merge($cadlibs , $objectsList );
  if ($bookshops) $objectsList = array_merge($bookshops , $objectsList );
  if ($partners)  $objectsList = array_merge($partners , $objectsList);
  if ($process)   $objectsList = array_merge($process , $objectsList );
  if ($doctypes)  $objectsList = array_merge($doctypes , $objectsList );
  if(is_null($objectsList))
    return false;
  return $objectsList;
}//End of method

//----------------------------------------------------------
/*!\brief Suppress a link between the project and the ranchbe component.
 * Return array or false.
 *
  \param $project_id(integer) id of the project.
  \param $link_object_id(integer) id of component to unlink.
  \param $type(string) type of linked component(workitem, partner, cadlib...).
*/
function RemoveLink($project_id , $link_object_id , $type){
  $link_object_map_id = $type.'_id';
  switch($type){
    case 'workitem': 
      $this->error_stack->push(ERROR, 'Fatal', array(), 'You cant remove a workitem link by this way');
      return false;
      break;
    case 'partner': 
      $linkTable = 'project_partner_rel';
      break;
    case 'mockup': 
      $linkTable = 'project_mockup_rel';
      break;
    case 'doctype':
      $linkTable = 'project_doctype_process';
      break;
    case 'cadlib':
      $linkTable = 'project_cadlib_rel';
      break;
    case 'bookshop':
      $linkTable = 'project_bookshop_rel';
      break;
    case 'process':
      $data['default_process_id'] = NULL;
      return $this->BasicUpdate( $data , $project_id );
      break;
    default: 
      $this->error_stack->push(ERROR, 'Info', array(), "$type" . 'is unknow' );
      //print "$type" . 'is unknow';
  } //End of switch types
  $query = 'DELETE FROM '.$linkTable.
           ' WHERE ('.$this->FIELDS_MAP_ID.' = \''.$project_id.'\') AND ( '.$link_object_map_id.' = \''.$link_object_id.'\')';
  if(!$links = $this->dbranchbe->Execute($query)){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
    return false;}
  return true;
}//End of method

//----------------------------------------------------------
/*!\brief
* Suppress a link between doctype and a container
* TODO: factorize with the SuppressDoctypeLink function of container
*
\param $link_id(integer) Primary key of the link.
*/
function SuppressDoctypeLink($link_id){
  $query = 'DELETE FROM project_doctype_process WHERE link_id = \''.$link_id.'\'';
  if($this->dbranchbe->Execute($query) === false){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
    return false;}
  return true;
}

//----------------------------------------------------------
/*!\brief Defined the default process of the project.
 * Return true or false.
 *
  \param $project_id(integer) id of the project.
  \param $process_id(integer) id of process to link.
*/
function LinkDefaultProcess($project_id , $process_id){
  $data['default_process_id'] = $process_id;
  return $this->BasicUpdate( $data , $project_id );
}//End of method

//----------------------------------------------------------
/*!\brief
* Link a doctype to a container
* Return false or the new link id(integer)
* TODO : factorize with LinkDoctype function of container.
*
\param $doctype_id(integer) Primary key of doctype to link.
\param $process_id(integer) Primary key of process to link to container for the doctype.
*/
function LinkDoctype($doctype_id , $process_id=NULL){
  if(!isset($this->project_id)){
    $this->error_stack->push(ERROR, 'Error', array(), '$this->project_id is not set');
    return false;}
  $linkTable  = $this->SPACE_NAME .'_doctype_process';
  $this->dbranchbe->StartTrans();
  $id = $this->dbranchbe->GenID("$linkTable".'_seq', 1); //Increment the sequence number
  $data['link_id'] = $id;
  $data[$this->FIELDS_MAP_ID] = $this->project_id;
  $data['doctype_id'] = $doctype_id;
  $data['process_id'] = $process_id;
  if(!$this->dbranchbe->AutoExecute($linkTable, $data, 'INSERT'))
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>'INSERT' . implode(';',$data) , 'debug'=>array($data, $linkTable)), $this->dbranchbe->ErrorMsg());
  if($this->dbranchbe->CompleteTrans() === false)
    return false;
  return $id;
}

//----------------------------------------------------------
/*!\brief
* Link a process to doctype and container
* Return false or the link id(integer)
* TODO : factorize with LinkDoctypesProc function of container.
*
\param $link_id(integer) Primary key of the link.
\param $process_id(integer) Primary key of process to link to container for the doctype.
*/
function LinkDoctypesProc($link_id , $process_id){
  $linkTable  = $this->SPACE_NAME .'_doctype_process';
  $data['process_id'] = $process_id;
  if (!$this->dbranchbe->AutoExecute( $linkTable , $data , 'UPDATE' , 'link_id = '.$link_id )){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>'UPDATE' . implode(';',$data) , 'debug'=>array($data, $linkTable, "link_id = $link_id")), $this->dbranchbe->ErrorMsg());
    return false;}
  return $id;
}//End of method

//----------------------------------------------------------
/*!\brief
* Suppress a link between process/doctype for a container
* Return false or true
* TODO : factorize with SuppressDoctypeProcLink function of container.
*
\param $link_id(integer) Primary key of the link.
*/
function SuppressDoctypeProcLink($link_id){
  return $this->LinkDoctypesProc($link_id , NULL);
}//End of method

//----------------------------------------------------------
/*!\brief Link a partner to project.
 * Return true or false.
 *
  \param $project_id(integer) id of the project.
  \param $partner_id(integer) id of partner to link.
*/
function LinkPartner($project_id , $partner_id ){
  $linkTable = 'project_partner_rel';
  $data['partner_id'] = "$partner_id";
  //link project to partner
  return $this->CreateLink($project_id , $linkTable , $data);
}//End of method

//----------------------------------------------------------
/*!\brief Link a bookshop to project.
 * Return true or false.
 *
  \param $project_id(integer) id of the project.
  \param $bookshop_id(integer) id of bookshop to link.
*/
function LinkBookshop($project_id , $bookshop_id ){
  $linkTable = 'project_bookshop_rel';
  $data['bookshop_id'] = "$bookshop_id";
  //link project to partner
  return $this->CreateLink($project_id , $linkTable , $data);
}//End of method

//----------------------------------------------------------
/*!\brief Link a mockup to project.
 * Return true or false.
 *
  \param $project_id(integer) id of the project.
  \param $mockup_id(integer) id of mockup to link.
*/
function LinkMockup($project_id , $mockup_id ){
  $linkTable = 'project_mockup_rel';
  $data['mockup_id'] = "$mockup_id";
  //link project to partner
  return $this->CreateLink($project_id , $linkTable , $data);
}//End of method

//----------------------------------------------------------
/*!\brief Link a cadlib to project.
 * Return true or false.
 *
  \param $project_id(integer) id of the project.
  \param $cadlib_id(integer) id of cadlib to link.
*/
function LinkCadlib($project_id , $cadlib_id ){
  $linkTable = 'project_cadlib_rel';
  $data['cadlib_id'] = $cadlib_id;
  //link project to partner
  return $this->CreateLink($project_id , $linkTable , $data);
}//End of method

//----------------------------------------------------------
/*!\brief Get all mockups linked to this project.
 * Return array or false.
 *
  \param $project_id(integer) id of the project.
*/  
function GetMockupsLinked($project_id){
  $query = "SELECT * FROM project_mockup_rel 
            JOIN mockups ON project_mockup_rel.mockup_id = mockups.mockup_id
            WHERE project_mockup_rel.$this->FIELDS_MAP_ID = '$project_id'";

  if(!$infos = $this->dbranchbe->Execute($query)){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
    return false;
  }

  $infos = $infos->GetArray(); //To transform object result in array;
  foreach ($infos as $key => $value){
    $infos[$key]['object_class'] = "mockup";
  }

  return $infos;
}//End of method

//----------------------------------------------------------
/*!\brief Get all bookshop linked to this project.
 * Return array or false.
 *
  \param $project_id(integer) id of the project.
*/  
function GetBookshopsLinked($project_id){

  $query = "SELECT * FROM project_bookshop_rel
            JOIN bookshops ON project_bookshop_rel.bookshop_id = bookshops.bookshop_id
            WHERE project_bookshop_rel.$this->FIELDS_MAP_ID = '$project_id'";

  if(!$infos = $this->dbranchbe->Execute($query)){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
    return false;
  }

  $infos = $infos->GetArray(); //To transform object result in array;
  foreach ($infos as $key => $value){
    $infos[$key]['object_class'] = "bookshop";
  }
  return $infos;
}//End of method

//----------------------------------------------------------
/*!\brief Get all cadlib linked to this project.
* Return array or false.
*
\param $project_id(integer) id of the project.
*/  
function GetCadlibsLinked($project_id){

  $query = "SELECT * FROM project_cadlib_rel 
            JOIN cadlibs ON project_cadlib_rel.cadlib_id = cadlibs.cadlib_id
            WHERE project_cadlib_rel.$this->FIELDS_MAP_ID = '$project_id'";

  if(!$infos = $this->dbranchbe->Execute($query)){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
    return false;
  }

  $infos = $infos->GetArray(); //To transform object result in array;
  foreach ($infos as $key => $value){
    $infos[$key]['object_class'] = "cadlib";
  }

  return $infos;

}//End of method

//----------------------------------------------------------
/*!\brief Get all partner linked to this project.
* Return array or false.
*
\param $project_id(integer) id of the project.
*/
function GetPartnersLinked($project_id){
  $query = "SELECT * FROM project_partner_rel
            JOIN partners ON project_partner_rel.partner_id = partners.partner_id
            WHERE project_partner_rel.$this->FIELDS_MAP_ID = '$project_id'";
  
  if(!$infos = $this->dbranchbe->Execute($query)){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
      return false;}
  $infos = $infos->GetArray(); //To transform object result in array;
  foreach ($infos as $key => $value)
    $infos[$key]['object_class'] = 'partner';
  return $infos;
}//End of method

//----------------------------------------------------------
/*!\brief
* Get all doctypes linked to container
* Return false if error or an array with the doctype properties
*
\param $project_id(integer) Primary key of project.
\param $extension(string) limit the result to doctype with extension $extension.
\param $type(string) limit the result to doctype with type $type.
\param $params(array) is use for manage the display. See parameters function of GetQueryOptions().
TODO : This method can be factorized with the method GetDoctypesLinked of class container...
*/
function GetDoctypesLinked($project_id , $extension='' , $type = '' , $params=array()){
  if(!empty($project_id)){
    $params['exact_find'][$this->FIELDS_MAP_ID] = $project_id;
  }else{ 
    $this->error_stack->push(ERROR, 'Fatal', array(), 'none selected project!!!');
    return false;}

  //Get only the doctype with the correct file extension and file type
  if(!empty($extension)) $params['find']['file_extension'] = $extension;
  if(!empty($type)) $params['find']['file_type'] = $type;
  $this->GetQueryOptions($params);
  $linkTable  = $this->SPACE_NAME.'_doctype_process';
  //Junction of tables doctypes and link_table
  $query = "SELECT $this->selectClose FROM $linkTable JOIN doctypes
            ON $linkTable.doctype_id = doctypes.doctype_id
            $this->whereClose
            $this->orderClose
            ";
  if(!$links = $this->dbranchbe->Execute($query)){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
    return false;}
  $links = $links->GetArray(); //To transform object result in array;
  foreach ($links as $key => $value)
    $links[$key]['object_class'] = 'doctype'; //Add the type of object to each key
  return $links;
}//End of method
//----------------------------------------------------------------------------
function GetDoctypes($extension='', $type = '', $params=array()){
  return $this->GetDoctypesLinked($this->project_id,$extension,$type,$params);
}
//---------------------------------------------------------------------------
/*!\brief
 * This method can be used to write a history record for project
 * return true if success, false else.
 *
*/
function WriteHistory($project_id){
  //Write history
  $this->history[$this->FIELDS_MAP_ID] = $project_id;
  $ohistory = new history($this);
  return $ohistory->write($this->history);
}//End of method

}//End of class

?>
