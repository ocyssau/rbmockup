<?php

require_once('./class/common/basic.php');

/*! \brief It's a record in database of a file linked to a container.
*
*/
require_once('./class/common/observable.php');
class recordfile extends rb_observable {

  protected $fsdata; //Object fsdatai, data file object of this recordfile
  protected $vfsdata; //Array , data file object of the version files of this recordfile
  protected $wildspace; //Object wildspace, wildspace object linked to the recordfile
  protected $versions; //(array of fsdata objects) Array of the fsdata object of the old version for the current fsdata
  protected $container; //Object container, father container object of the recordfile
  protected $father; //Object, father object

  protected $file_id; //Integer, id the docfile.
  protected $file_props; //Array, content all properties of the docfile.

  protected $file_name; //Name of the docfile 'file.ext'
  protected $file_size; //Size of the docfile in octets
  protected $file_path; //path to the docfile '/dir/sub_dir'
  protected $file_mtime; //modification time of the docfile
  protected $file_extension; //extension of the docfile '.ext'
  protected $file_root_name; //root name of the docfile 'file'
  protected $file_type; //type of docfile = file, adrawc5, adrawc4, camu, cadds4, cadds5, pstree...
  protected $file_md5; //md5 code of the docfile
  protected $file_version; //version of the docfile

  protected $displayMd5 = false; //true if you want return the md5 property of the file

  protected $dbranchbe;
  protected $usr;

/*! \brief Constructor.
*/
function __construct(space $space , $file_id = NULL, $manage_sub = false){

  $this->space =& $space;
  $this->error_stack =& $this->space->error_stack;

  global $dbranchbe;
  $this->dbranchbe =& $dbranchbe;

  global $logger;
  $this->logger =& $logger;

  global $usr;
  $this->usr =& $usr;

  $this->CurrentUserId   = $this->usr->getProperty('auth_user_id');
  
  $this->MANAGE_SUB = $manage_sub; //if you want manage subdirectories

  $this->OBJECT_TABLE  = $this->space->FILE_TABLE;
  $this->FILE_TABLE  = $this->space->FILE_TABLE;
  //$this->HISTORY_TABLE = $this->space->FILE_HISTORY_TABLE;

  $this->FIELDS_MAP_ID = 'file_id';
  $this->FIELDS_MAP_NUM = 'file_name';
  $this->FIELDS_MAP_DESC = 'file_name';
  $this->FIELDS_MAP_STATE = 'file_state';
  $this->FIELDS_MAP_INDICE = 'file_version';
  $this->FIELDS_MAP_FATHER = $this->space->CONT_FIELDS_MAP_ID;

  if(!is_null($file_id))
    $this->file_id = $file_id;

}//End of method

//--------------------------------------------------------
/*!\brief
 * This method return the file id of the file number if exist.
 *   
 \param $file_name(string) number of the file
*/
function GetId(){
  return $this->file_id;
}//End of method

//--------------------------------------------------------
/*! \brief Return all single files (not linked to document) linked to a container.
    return true or false.
*
\param $container_id(integer) container id of the container to list.
\param $params(array) is use for manage the display. See parameters function of GetQueryOptions().
*/
function GetAllFilesInFather($father_id , $params){
  $params['exact_find'][$this->FIELDS_MAP_FATHER] = $father_id;
  return $this->GetFiles($params);
}//End of method

//--------------------------------------------------------
/*! \brief Return all single files (not linked to document) recorded in database of the current space.
    return true or false.
*
\param $params(array) is use for manage the display. See parameters function of GetQueryOptions().
*/
function GetFiles($params=array()){
  $this->GetQueryOptions($params);
  $query = 'SELECT '.$this->selectClose.' FROM '.$this->FILE_TABLE
            .' '.$this->joinClose
            .' '.$this->whereClose
            .' '.$this->extra
            .' '.$this->orderClose
            ;

  if(!$All = $this->dbranchbe->SelectLimit($query , $this->limit , $this->offset)){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array($this->limit , $this->offset)), $this->dbranchbe->ErrorMsg());
    return false;
  }else{
    return $All->GetArray(); //To transform result in array;
  }
}//End of method

//-------------------------------------------------------
/*! \brief Return fsdata object of the versions files of the current recordfile.
* Return array of fsdata objects with real data on filesytem.
* Set $this->versions var.
*/
/*
function GetVersions(){
  require_once('class/common/fsdataVersion.php');
  $current_version = $this->GetProperty('file_version');
  $i = $current_version -1;
  while($i >= ($current_version - DEFAULT_BACK_VERSIONS_NUM) && $i > 0){
    if ($this->initFsdataVersion($i) === false){ //if data is not on file system...
      unset($this->versions[$i]); //...destruct the version link
    }
    $i -= 1;
  } //End of while
  return $this->versions;
}//End of method
*/
/*
function GetVersions(){
  $current_version = $this->GetProperty('file_version');
  $i = $current_version -1;
  while($i >= ($current_version - DEFAULT_BACK_VERSIONS_NUM) && $i > 0){
    if ($this->initVersion($i) === false){ //if version dont exist
      unset($this->versions[$i]); //...destruct the version link
    }
    $i -= 1;
  } //End of while
  return $this->versions;
}//End of method
*/
//--------------------------------------------------------
/*!\brief
* Get the fsdata object of a version file and link it to the current recordfile
* return a fsdata instance if success, else false.
* Set array of object $this->versions[i] where i is the version number.
* \param $versionId (integer) Number of the version.
* The method init() must be call before.
*/
/*
function initFsdataVersion($versionId){
  require_once('./class/common/fsdataVersion.php');
  if(!isset($this->fsdata)) $this->initFsdata(); //create an fsdata object for current recordfile
  $this->error_stack->push(LOG, 'Info', array('verId'=>$versionId,'file'=>$this->GetProperty('file')),
   'try to init fsdataVersion %verId% from fsdata %file%');
  $fsdataVersion = new fsdataVersion($this->fsdata, $versionId); //Create the fsdataVersion object
  $this->versions[$versionId] = $fsdataVersion; //attach the fsdataVersion to current fsdata
  $this->versions[$versionId]->SetRecordfile($this); //attch the current fsdata to fsdataVersion (bi-directonnal link)
  if($fsdataVersion->getProperty('dataType') !== false) {
    return $this->versions[$versionId];
  }
  return false;
}//End of method
*/

function initVersion($versionId){
  require_once('./class/common/recordfileVersion.php');
  $this->error_stack->push(LOG, 'Info', array('verId'=>$versionId,'file'=>$this->GetProperty('file')),
   'try to init version %verId% from %file%');
  $Version = new recordfileVersion($this->space); //Create the recordfile object
  if( $Version->initVersion($this->file_id, $versionId) ){
    return $this->versions[$versionId] = $Version;
  }else{
    return false;
  }
}//End of method

//-------------------------------------------------------
/*! \brief Move a file from a container to another.
      return true or false.
*
* This method update record in the table []_doc_file with value of the $data param.
    \param $target_container(objet)
    \param $moveVersion(bool) if true move also the versions files of current recordfile
*/
//function MoveFile($target_dir){ //change since ranchbe0.6
function MoveFile(container &$target_container, $moveVersion=true){

  //$this->GetVersions(); //init $this->versions objects array
  //$fsdataVersions = $this->versions; //create a copy of fsdataVersions objects before move main data

  $this->dbranchbe->StartTrans(); //Start a transaction

  if(!isset($this->fsdata)) $this->initFsdata(); //init before execute scripts notification

  $target_dir = $this->GetRepositdirPath($target_container); //init before execute scripts notification
  $dstfile = $target_dir.'/'.$this->GetProperty('file_name'); //init before execute scripts notification

  //Notiy observers on event
  $this->notify_all('recordfile_pre_move', $this);

  $data = array();
  $data['file_path'] = $target_dir;
  if( !$this->UpdateRecordFile($data) ){
    $this->dbranchbe->FailTrans();
    $this->dbranchbe->CompleteTrans();
    return false;
  } //Update file infos

  if(!$this->fsdata->move($dstfile, true)){
    $this->error_stack->push(ERROR, 'Fatal', array('file'=>$this->file_name, 'dstfile'=>$dstfile), 'can\'t move %file% to %dstfile%');
    $this->dbranchbe->FailTrans();
    $this->dbranchbe->CompleteTrans();
    return false;
  }

  $this->dbranchbe->CompleteTrans();
  $this->file_props['file_path'] = $target_dir; //Update property if validate only

  $this->error_stack->push(LOG, 'Info', array('element'=>$data['file_name']), 'data %element% is moved to '.$target_dir);

  /*
  if( count( $fsdataVersions ) != 0 ){ //Move the versions files
    //echo 'move versions<br>';
    require_once('class/common/fsdataVersion.php');
    foreach($fsdataVersions as $oldFsdataDataVersion){
      $ver = $oldFsdataDataVersion->GetProperty('file_version');
      //echo 'version :'.$ver.'<br>';
      $this->versions[$ver] = new fsdataVersion($this->fsdata, $ver ); //create a new version to extract path
      $this->versions[$ver]->InitDirectory();
      $dst = $this->versions[$ver]->getProperty('file');
      //echo 'dst :'.$dst.'<br>';
      $oldFsdataDataVersion->move($dst, true);
    }
  }
  */
  if($moveVersion){
    require_once('class/common/recordfileVersion.php');
    $versionConstructor = new recordfileVersion($this->space);
    $aversions = $versionConstructor->GetVersions($this->file_id, true); //Get objects version
    if( is_array($aversions) )
      foreach($aversions as $version){
        $version->MoveFile($target_container, false);
      }
  }

  //Notiy observers on event
  $this->notify_all('recordfile_post_move', $this);

  return true;
}//End of method

//---------------------------------------------------------------------------
/*! \brief Lock a file.
*   return true or false.
*
* This method is call by the checkoutFile method. When a file is checkOut he is locked to prevent a second checkout.
  
    \param $file_id(integer) file id of the file.
    \param $code(integer) code to set for access_code.
*/
function LockFile($code = 1){
  $data['file_access_code'] = $code;
  if($code == 1){
    $data['file_checkout_by']   = $this->CurrentUserId;
    $data['file_checkout_date'] = time();
  }
  if($code == 0){
    $data['file_checkout_by']   = NULL;
    $data['file_checkout_date'] = NULL;
  }
  //Create entries in table
  if(!$this->dbranchbe->AutoExecute($this->FILE_TABLE , $data , 'UPDATE' , "file_id = $this->file_id")){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>'UPDATE' . implode(';',$data) , 'debug'=>array($data, $this->FILE_TABLE,"file_id = $this->file_id" )), $this->dbranchbe->ErrorMsg());
    return false;
  }else return true;
}//End of method

//-------------------------------------------------------
/*! \brief Check if a file is access free.
  Return the access code (0 for free without restriction , 100 if error).
*
  \param $this->file_id(integer) file id of the file.
*/
function CheckFileAccess(){
  $query = "SELECT file_access_code FROM $this->FILE_TABLE WHERE file_id = $this->file_id";
  $one = $this->dbranchbe->GetOne($query);
  if($one === false){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
    return 100;
  }else{
    return $one;
  }
}//End of method

//-------------------------------------------------------
/*! \brief copy a file to file $dstfile.
  Return a docfile object if success, else return false.
*
  \param $dstfile(string) new name of the file
  \param $father_id(integer) id of the docfile father (document_id or container_id)
  \param $replace(bool) true if you want replace the file in the target directory
*/
function Copy($dstfile, $father_id, $replace=false){
  if(!isset($this->fsdata))
    $this->initFsdata();
  $this->logger->log('copy file : '.$this->GetProperty('file').' to : '.$dstfile);

  //Notiy observers on event
  $this->notify_all('recordfile_pre_copy', $this);

  if(!$this->fsdata->copy($dstfile, 0755, $replace)){
    $this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->fsdata->GetProperty('file')), 'cant copy file %element% to'.$dstfile);
    return false;
  }
  $t_docfile = new docfile($this->space);//Create a new docfile
  $t_docfile->initFsdata($dstfile);
  $t_docfile->file_props[$this->FIELDS_MAP_FATHER]= $father_id;
  $t_docfile->file_props['file_version']= $this->GetProperty('file_version');
  $t_docfile->RecordFile($t_docfile->fsdata); //record fsdata in the new document

  //Notiy observers on event
  $this->notify_all('recordfile_post_copy', $this);

  return $t_docfile;
}//End of method

//-------------------------------------------------------
/*! \brief copy a file to directory $target_dir.
  Return bool.
*
  \param $target_dir(string) directory where copy file.
  \param $replace(bool) true if you want replace the file in the target directory
*/
function CopyFile($target_dir, $replace=false){
  $this->initFsdata();
  $dstfile = $target_dir.'/'.$this->GetProperty('file_name');

  //Notiy observers on event
  $this->notify_all('recordfile_pre_copy', $this);

  if(!$this->fsdata->copy($dstfile, 0755, $replace)){
    $this->error_stack->push(ERROR, 'Fatal', array('element'=>$dstfile), 'cant copy file %element%');
    return false;
  }

  //Notiy observers on event
  $this->notify_all('recordfile_post_copy', $this);

  return true;
}//End of method

//-------------------------------------------------------
/*! \brief Download a file on client computer.
    Return true or false.
*
* This method send the content of the $file to the client browser. It send too the mimeType to help the browser to choose the right application to use for open the file.
* If the mimetype = no_read, display a error.  

  \param $file(string) path of the file to display on the client computer
*/
function DownloadFile(){
}//End of method

//--------------------------------------------------------
/*!\brief
 * This method return the file id of the file number if exist.
 *   
 \param $file_name(string) number of the file
*/
function GetFileId($file_name){
  return $this->GetBasicId($file_name);
}//End of method

//--------------------------------------------------------
/*!\brief
* This method return the file name of the file id if exist.
* else return false.
*   
*/
function GetFileName(){
  $query = "SELECT file_name FROM $this->FILE_TABLE WHERE
            file_id = '$this->file_id'";
  return $this->dbranchbe->GetOne($query);
}//End of method

//--------------------------------------------------------
/*!\brief
 * This method return the document id of the file if exist.
 * else return false.
 *   
 \param $this->file_id(string) id of the file
*/
function GetDocumentId(){
  $query = "SELECT document_id FROM $this->FILE_TABLE WHERE
          file_id = '$this->file_id'";
  return $this->dbranchbe->GetOne($query);
}//End of method

//--------------------------------------------------------
/*! \brief Get infos recorded in database about file.
  return a array or false.
*
*/
function GetInfos($selectClose=''){
  return $this->GetBasicInfos($this->file_id,$selectClose,'row');
}//End of method

//-------------------------------------------------------
/*! \brief Put the file in the wildspace.
    Return true or false.
*
  \param $addPrefix(String) string to add before filename.
* The file is put with a default prefix "consult__"  to prevent lost of data.
*/
function PutFileInWildspace($addPrefix=NULL){
  if(is_null($addPrefix))
    $addPrefix = 'consult__';
  $addPrefix = $addPrefix.$this->GetProperty('file_version').'__';
  if($this->initFsdata())
    return $this->fsdata->putInWildspace($addPrefix, true);
  return false;

  //Notiy observers on event
  $this->notify_all('recordfile_post_PutInWildspace', $this);

  //TODO : Add a confirmation for replace it
}//End of method

//---------------------------------------------------------------------------
/*! \brief Remove a file from a container. Suppress recorded infos in database and suppress the file from the desposit directory of the container.
    return true or false.
*
    \param $delfile(bool) if true suppress the file from the deposit directory of the container.
*/
function RemoveFile($delfile=false, $delversion=false){

  $this->dbranchbe->BeginTrans();

  if($delfile === true){
    if(!isset($this->fsdata)) $this->initFsdata(); //construct the fsdata before suppress database records, else is no more possible
  }

  //Delete record of associated files
  $query = 'DELETE FROM '.$this->FILE_TABLE.' WHERE file_id = '.$this->file_id;
  if(!$this->dbranchbe->Execute($query)){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
    return false;
  }

  if($delfile === true){ //Delete the file if $delfile = true
    if( $this->fsdata->isExisting() ){ //Delete the file if $delfile = true
      if( !$this->fsdata->putInTrash() ){
        $this->error_stack->push(ERROR, 'Fatal', array( 'element'=>$this->GetProperty('file') ), 'can\'t suppress file %element%');
        $this->dbranchbe->RollbackTrans();
        $this->dbranchbe->CommitTrans();
        return false;
      }
    }else{
      $this->error_stack->push(LOG, 'Info', array('file'=>$this->fsdata->getProperty('file_name') ), 'A suppress request occuring on a inexistant file: %file%' );
    }
  }
  $this->dbranchbe->CommitTrans();

  if($delversion === true){ //Delete the versions files
    require_once('class/common/recordfileVersion.php');
    $versionConstructor = new recordfileVersion($this->space);
    $aversions = $versionConstructor->GetVersions($this->file_id, true); //Get objects version
    if( is_array($aversions) )
      foreach($aversions as $version){
        $version->RemoveFile(true, false);
      }
  }

  $this->error_stack->push(LOG, 'Info', array('file'=>$this->GetProperty('file_name') ), 'recordfile %file% has been removed');
  return true;

}//End of method

//---------------------------------------------------------------------------
/*! \brief Unlock a file.
*   return true or false.
*
* This method is call by the checkinFile method. When a file is checkOut is locked to prevent a second checkout.
* When is checkin, is unlock to permit checkout.
    \param $file_id(integer) file id of the file.
*/
function UnLockFile(){
  $data['file_access_code'] = 0;
  if (!$this->dbranchbe->AutoExecute($this->FILE_TABLE , $data , 'UPDATE' , "file_id = $this->file_id")){
   $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>'UPDATE' . implode(';',$data) , 'debug'=>array($data, $this->FILE_TABLE,"file_id = $this->file_id")), $this->dbranchbe->ErrorMsg());
   return false;
  }else return true;
}//End of method

//-------------------------------------------------------
/*! \brief Update the record file info in database.
*
* This method update record in the table []_doc_file with value of the $data param.

\param $data[file_name](string)
\param $data[file_size](integer)
\param $data[file_path](string)
\param $data[file_mtime](integer)
\param $data[file_md5](string)
\param $data[file_extension](string)
\param $data[file_root_name](string)
\param $data[file_version](integer)
\param $data[file_indice](integer)
\param $data[last_update_by](integer)
\param $data[last_update_date](integer)
\param $data[check_out_by](integer)
\param $data[check_out_date](integer)   
\param $file_id(integer) file id of the file.
*/
function UpdateRecordFile($data){
  if (!$this->dbranchbe->AutoExecute($this->FILE_TABLE , $data , 'UPDATE' , "file_id = '$this->file_id'")){
   $this->error_stack->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg());
   return false;
  }
  return true;
}//End of method

//-------------------------------------------------------
/*! \brief Save current object in database with update of current propoerties
* Experimental function, not used.
* 
*/
function Save(){
  $this->UpdateRecordFile($this->file_props);
}//End of method

//-------------------------------------------------------
/*! \brief Check if the uers can update a document.
    Return true if access is free, else return false.
*
  \param $this->file_id(integer) file id of the file.
*/
function CheckFileUpdateRight(){
  $query = 'SELECT file_access_code , file_checkout_by FROM '.$this->FILE_TABLE
           .' WHERE file_id = '.$this->file_id;
  if(!$res = $this->dbranchbe->GetRow($query)){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg() );
    return false;}
  if($res['file_access_code'] == 1) //The file is checkOut...
   if($res['file_checkout_by'] == $this->CurrentUserId) //... by current user
    return true; //Update is permit for current user
  return false;
}//End of method

//-------------------------------------------------------
/*! \brief Record file info in database.
*   return file_id of the new record if success else return false.
*
* This method create a new record of file in _file table.
*/
function RecordFile(fsdata $fsdata){
  $now = time();//Set current time
  $this->file_props['file_name']= $fsdata->getProperty('file_name');
  $this->file_props['file_size']= $fsdata->getProperty('file_size');
  $this->file_props['file_path']= $fsdata->getProperty('default_file_path');
  $this->file_props['file_mtime']= $fsdata->getProperty('file_mtime');
  $this->file_props['file_md5']= $fsdata->getProperty('file_md5');
  $this->file_props['file_extension']= $fsdata->getProperty('file_extension');
  $this->file_props['file_root_name']= $fsdata->getProperty('file_root_name');
  $this->file_props['file_type']= $fsdata->getProperty('file_type');
  $this->file_props['file_path']= $fsdata->getProperty('file_path');
  $this->file_props['file_open_by']= $this->CurrentUserId;
  $this->file_props['file_open_date']= $now;
  $this->file_props['file_update_by']= $this->CurrentUserId;
  $this->file_props['file_update_date']= $now;
  if(!isset($this->file_props['file_version']))
    $this->file_props['file_version']= 1;
  if(!isset($this->file_props['file_state']))
    $this->file_props['file_state']= 'init';
  $id = $this->dbranchbe->GenID($this->FILE_TABLE.'_seq', 1); //Increment the sequence number
  $this->file_props['file_id'] = $id;
  if(!$this->dbranchbe->AutoExecute($this->FILE_TABLE, $this->file_props, 'INSERT')){
    $this->error_stack->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg());
    return false;
  }
  $this->fsdata = $fsdata; //Init the fsdata of current recordfile
  $this->file_id = $id; //init the current recordfile id
  return $id;
}//End of method


/* experimental function, don't use it
*/
function UpdateFromFsdata(fsdata $fsdata){
  $now = time();
  $data['file_name'] = $this->file_props['file_name']= $fsdata->getProperty('file_name');
  $data['file_size'] = $this->file_props['file_size']= $fsdata->getProperty('file_size');
  $data['file_path'] = $this->file_props['file_path']= $fsdata->getProperty('file_path');
  $data['file_mtime'] = $this->file_props['file_mtime']= $fsdata->getProperty('file_mtime');
  $data['file_md5'] = $this->file_props['file_md5']= $fsdata->getProperty('file_md5');
  $data['file_extension'] = $this->file_props['file_extension']= $fsdata->getProperty('file_extension');
  $data['file_root_name'] = $this->file_props['file_root_name']= $fsdata->getProperty('file_root_name');
  $data['file_type'] = $this->file_props['file_type']= $fsdata->getProperty('file_type');
  $data['file_update_by'] = $this->file_props['file_update_by']= $this->CurrentUserId;
  $data['file_update_date'] = $this->file_props['file_update_date']= $now;
  $this->UpdateRecordFile($data);

  /*$recordfile->SetProperty('file_version', $recordfile->GetProperty('file_version') + 1);
  $recordfile->SetProperty('import_order', $import_order);
  $recordfile->SetProperty('file_update_date', time() );
  $recordfile->SetProperty('file_update_by', $this->importUserId );
  $recordfile->SetProperty('file_md5', $fsdata->getProperty('file_md5') );
  $recordfile->SetProperty('file_size', $fsdata->getProperty('file_size') );
  $recordfile->SetProperty('file_mtime', $fsdata->getProperty('file_mtime') );
  $recordfile->SetProperty('file_size', $fsdata->getProperty('file_size') );
  $recordfile->SetProperty('',  );
  */

}
//--------------------------------------------------------
/*!\brief Get the current fsdata object linked to the current object and check if the data path equal the parameter
if not, init the fsdata
*/
function GetFsdata($datapath=NULL){
  if(!is_null($datapath) && isset($this->fsdata)){
    if($this->fsdata->GetProperty('file') == $datapath) return $this->fsdata;
    return $this->initFsdata($datapath);
  }else{
    if(isset($this->fsdata)) return $this->fsdata;
  }
  return false;
}//End of method

//----------------------------------------------------------
/**
 * Init the recordfile
 */
function init($file_id){
  $this->file_id = $file_id;
  if(isset($this->file_props)) unset($this->file_props);
  if(isset($this->document)) unset($this->document);
  if(isset($this->container)) unset($this->container);
  if(isset($this->fsdata)) unset($this->fsdata);
  if(isset($this->wildspace)) unset($this->wildspace);
  if(isset($this->vfsdata)) unset($this->vfsdata);
  if(isset($this->versions)) unset($this->versions);
  if(isset($this->father)) unset($this->father);
  return true;
} //End of method

//----------------------------------------------------------
/**
 * Set the fsdata linked to the current recordfile
 */
function SetFsdata(fsdata &$fsdata){
  $this->fsdata =& $fsdata;
  return true;
} //End of method

//----------------------------------------------------------
/**
 * Set the container linked to the current recordfile
 */
function SetContainer(&$container){
  $this->container =& $container;
  return true;
} //End of method

//--------------------------------------------------------
/*!\brief
* Get the fsdata object and link it to the current recordfile
* return a fsdata instance if success, else false if data is not find on filesystem.
* Set object $this->fsdata
* The method init() must be call before.
*/
function initFsdata($datapath=NULL){
  require_once('./class/common/fsdata.php');
  if(is_null($datapath))
    $datapath = $this->GetProperty('file_path').'/'.$this->GetProperty('file_name');
  $this->error_stack->push(LOG, 'Info', array('datapath'=>$datapath), 'init fsdata from : %datapath%');
  $this->fsdata = new fsdata($datapath);
  $this->fsdata->SetRecordfile($this);
  if($this->fsdata->getProperty('dataType') === false) return false;
  return $this->fsdata;
}//End of method

//--------------------------------------------------------
/*!\brief
*
*/
function initObservers(){
  //Attach object to listen observers to notify
  require_once('./class/observers/datatypeScript.php');
  $dataScript = new datatypeScript($this);
  $this->attach_all($dataScript);
}//End of method

//--------------------------------------------------------
/*!\brief
 * Get the wildspace object
 * return a wildspace instance if success, else fase.
 * Set object $this->wildspace
*/
function initWildspace(){
  require_once('./class/wildspace.php');
  if($this->wildspace = new wildspace())
    return $this->wildspace;
  return false;
}//End of method

function GetWildspace(){
  if( isset($this->wildspace) ) return $this->wildspace;
  else return $this->initWildspace();
}//End of method

//----------------------------------------------------------
/*! \brief Get the property of the recordfile by the property name. init() must be call before.
* 
* \param $property_name(string) = document_id, document_number, ...etc
*/
function GetProperty($property_name){
  if(!isset($this->file_id)){
    $this->error_stack->push(LOG, 'Info', array(), '$this->file_id is not set');
    return false;}
  if(isset($this->file_props[$property_name]))
    return $this->file_props[$property_name];
  else $realName = $property_name;

  if($property_name == 'file') //Dont record this property in a real key of property array to prevent risk of confusion or init forgetting
    return $this->GetProperty('file_path').'/'.$this->GetProperty('file_name');

  if($property_name == 'father_id')
    return $this->GetProperty($this->FIELDS_MAP_FATHER);

  $query = 'SELECT '.$realName.' FROM '.$this->OBJECT_TABLE.' WHERE '.$this->FIELDS_MAP_ID.' = \''.$this->file_id.'\'';
  $res = $this->dbranchbe->GetOne($query);

  if($res === false){
    $this->error_stack->push(ERROR, 'Fatal', array('query'=>$query , 'debug'=>array($this->file_id)), $this->dbranchbe->ErrorMsg());
    return false;
  }else
    return $this->file_props[$property_name] = $res;
}//End of method

//----------------------------------------------------------
/**
 * Set id of father object
 */
function SetFatherId($id){
  $this->file_props[$this->FIELDS_MAP_FATHER] = $id;
} //End of method

//----------------------------------------------------------
/**
 * Set id of version
 */
function SetVersionId($id){
  $this->file_props['file_version'] = $id;
} //End of method

//----------------------------------------------------------
/**
 * Set import order of recordfile
 */
function SetImportOrder($id){
  $this->file_props['import_order'] = $id;
} //End of method

//-------------------------------------------------------
/*! \brief get the full path where store data on server.
    Return string or false.
*
  \param $container(object) the container
*/
function GetRepositdirPath(container &$container){
  if(!isset($this->fsdata) ){
    $this->error_stack->push(LOG, 'Error', array(), '$this->fsdata is not set');
    return false;
  }
  $subDir = $this->fsdata->getProperty('reposit_sub_dir');
  if( empty( $subDir ) )
    return $container->GetProperty('default_file_path');
  else{
    //create subdir if not exist
    require_once('class/common/rdirectory.php');
    if( !rdirectory::createdir($container->GetProperty('default_file_path').'/'.$subDir) ) {
      $this->error_stack->push(LOG, 'Error', array('subdir'=>$subDir, 'repositDir'=>$container->GetProperty('default_file_path')), 'can not create %subdir% in deposit directory %repositDir%');
      return false;
    }
    return $container->GetProperty('default_file_path').'/'.$subDir;
  }
}//End of method

} //End of class
?>
