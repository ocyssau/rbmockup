<?php

require_once('class/common/filesystem.php');

/*! \brief manipulation of the directories on the server
* The directory class existing yet in PHP, so is why this class is named rdirectory(ranchbe direcory)
*
*/
class rdirectory extends filesystem{

/*! \brief A function to copy files from one directory to another one, including subdirectories and
*   nonexisting or newer files. Function returns TRUE or FALSE
*/
static function dircopy($srcdir, $dstdir, $verbose = false, $initial=true, $recursive=true, $mode=0755){

  if (!filesystem::LimitDir($srcdir)){
    print 'dircopy error : you have no right access on file '. $srcdir . ' <br />';
    return false;
  }

  if (!filesystem::LimitDir($dstdir)){
    print 'dircopy error : you have no right access on file '. $dstdir . ' <br />';
    return false;
  }

  if(!is_dir($dstdir)) mkdir($dstdir, $mode, true);
  if($curdir = opendir($srcdir)) {
     while($file = readdir($curdir)) {
       if($file != '.' && $file != '..') {
         $srcfile = "$srcdir" . '/' . "$file";
         $dstfile = "$dstdir" . '/' . "$file";
         if(is_file("$srcfile")) {
             if($verbose) echo "Copying '$srcfile' to '$dstfile'...";
             if(copy("$srcfile", "$dstfile")) {
               touch("$dstfile", filemtime("$srcfile"));
               if($verbose) echo "OK\n";
             }
             else {print "Error: File '$srcfile' could not be copied!\n"; return false;}
         }
         else{
            if(is_dir("$srcfile") && $recursive){
              self::dircopy("$srcfile", "$dstfile", $verbose, false);
            }
         }
       }
     }
     closedir($curdir);
   }
   return true;
}//End of function

//-------------------------------------------------------------------------------
/*! \brief create a new directory recursivly
*   Return TRUE or FALSE
\param $path
\param $mode
\param $secure if true, check that directory to create is in a authorized directory

*/
static function createdir($path, $mode=0755, $secure=true){

  global $error_stack;

  if($secure){
    if (!filesystem::LimitDir($path)){
      $error_stack->push(ERROR, 'Error', array('element'=>$path), 'can create directory : %element%. This path is not authorized by the ranchbe configuration' );
      return false;
    }
  }

  if( file_exists($dir) ){
    $msg = 'a file with same name that directory to create is yet existing. Can not create directory %element%';
    $error_stack->push(ERROR, 'Error', array('element'=>$path), $msg );
    return false;
  }

  //Le "mode" ne fonctionne pas sous windows, ce qui oblige a faire un
  //chmod par la suite.
  //bool mkdir ( string pathname [, int mode [, bool recursive [, resource context]]] )
  if(!is_dir($path)){
    if(mkdir($path, $mode, true)){
      if (! chmod ($path, $mode) ) { //!!chmod ne fonctionne pas avec les fichiers distants!!
        $error_stack->push(LOG, 'Warning', array('element'=>$path), 'chmod impossible on %element%' );
        return true;
      }
      $error_stack->push(LOG, 'Info', array('element'=>$path), 'create directory : %element%.' );
      return true;
    }else{
      $error_stack->push(ERROR, 'Error', array('element'=>$path), 'An unknow error is occuring. directory %element% is not created');
      return false;
    }
  }else{
    $error_stack->push(LOG, 'Warning', array('element'=>$path), 'directory %element% is yet existing');
    return true;
  }

}//End of function

//-------------------------------------------------------------------------------

/*! \brief remove files from one directory to another one, including subdirectories and nonexisting or newer files.
*   Returns TRUE or FALSE
*   
*   This function is a adaptation from the script of "....." visible in PHP help site.
*/
static function removeDir($path, $recursive=false, $verbose = false){
    if (!filesystem::LimitDir($path)) {
      print 'removeDir error : you have no right access on file '. $path;
      return false;
    }

   // Add trailing slash to $path if one is not there
   if (substr($path, -1, 1) != "/"){
       $path .= "/";
   }
   foreach (glob($path . "*") as $file){
       if (is_file($file) === TRUE){
         // Remove each file in this Directory
         if (!unlink($file)){echo "failed to removed File: " . $file . "<br>"; return false;
         }else {if($verbose) echo "Removed File: " . $file . "<br>";}
       }
       else if (is_dir($file) === true && $recursive === true){
           // If this Directory contains a Subdirectory and if recursivity is requiered, run this Function on it
             if (!self::removeDir($file, $recursive, $verbose)){
              if($verbose) echo "failed to removed File: " . $file . "<br>";
              return false;
             }
       }
   }

   // Remove Directory once Files have been removed (If Exists)
   if(is_dir($path)){
     if(@rmdir($path)){
       if($verbose) echo "<br>Removed Directory: " . $path . "<br><br>";
       return true;
     }
   }
   return false;
}//End of function

//-------------------------------------------------------------------------------
/*! \brief remove files from one directory to another one, including subdirectories and put in a trash.
*   Returns TRUE or FALSE
*   
*/
static function putDirInTrash($dir, $verbose = false, $recursive=false, $trashDir=DEFAULT_TRASH_DIR){

  //Check if directories exists and are writable
  if (!is_dir($dir) || !is_dir($trashDir) || !is_writable($trashDir)) {
    print ' error : '. "$dir" . ' don\'t exist or is not writable <br />';
    print ' or    : '. "$trashDir" . ' don\'t exist or is not writable <br />';
    return false;
  }

  //Add original path to name of dstfile
  $oriPath = self::encodePath(dirname($dir));
  /*
  $oriPath = str_replace( '/' , '%2F', "$oriPath" );
  $oriPath = str_replace( '.' , '%2E', "$oriPath" );
  $oriPath = str_replace( ':' , '%3A', "$oriPath" );
  $oriPath = str_replace( '\\' , '%5C', "$oriPath" );
  */
  //Check if there is not conflict name in trash dir else rename it
  //$dstdir = $trashDir .'/'. "$camuDir"  .'_'. basename($dir);
  $dstdir = $trashDir .'/'. "$oriPath" .'%&_'. basename($dir);
  $i = 0;
  if(is_dir($dstdir)){
    //Rename destination dir if exist
    $Renamedstdir = $dstdir;
    while(is_dir($Renamedstdir)){
      $Renamedstdir = $dstdir . '(' . "$i" . ')';
      $i ++;
    }
    $dstdir = $Renamedstdir;
  }
  
  //Copy dir to trash
  if (!self::dircopy($dir , $dstdir, false, false, true, 0755 )){ //copy dir to dirtrash
    print 'error copying dir '. $dir . ' to Trash: '. $dstdir . ' <br />';
    return false;
  }else{ //Suppress original dir
    if (!self::removeDir($dir, $recursive, $verbose)){
      //print 'error suppressing dir :'. $dir . ' <br />';
      return false;
    }
  }
  return true;

}//End of function

//-------------------------------------------------------
/*! \brief get files in a directory and return infos about
*   Return a array if no errors, else return FALSE
*   
*/
static function listDir($path , $getMd5=true){
  if (!$handle  = opendir($path)){
    print 'failed to open ' . $path . '<br>';
    return false ;}
  
  require_once('class/common/fsdata.php');
  while ($file = @readdir($handle)){
  $file.'<br>';
    if($fsdata = fsdata::_dataFactory($path.'/'.$file))
      $infos[] = $fsdata->getInfos($getMd5);
  }
  @closedir($handle);
  return $infos;
}//End of method

//----------------------------------------------------------
/*! \brief This method can be used to get files, cadds parts, camu, adraw in a directory.
*  Return a array if no errors, else return false.
    \param $params[maxRecords](integer) max number of records to display
    \param $params[sort_field](string)
    \param $params[sort_order](string = ASC or DESC)
    \param $params[find](string) 
    \param $params[displayMd5] (bool)
* 
*/
function GetDatas($path = '' , $params){

if(!empty($params['find'])){
  foreach($params['find'] as $key=>$val){
    $find_field=$key;
    $search_word = $val;
  }
}

if(empty($params['sort_field']))
    $params['sort_field'] = 'file_name';

if($params['sort_order'] == 'DESC'){
    $params['sort_order'] = SORT_DESC; //Ne pas quoter???? Serait-ce des constantes. On dirait bien
}else $params['sort_order'] = SORT_ASC;

if(empty($path))
  $path = $this->DEPOSIT_DIR_PATH;

if (!$handle = opendir($path)){
 $this->error_stack->push(ERROR, 'Fatal', array('element'=>$path, 'debug'=>array()), 'failed to open %element%');
 return false;
}

require_once('class/common/fsdata.php');
$count = 0;
while($file = @readdir($handle)){ //list all files of the directory
  if($file == '.' || $file == '..' ) continue;
  $file = $path .'/'. $file;
  if($odata =& fsdata::_dataFactory($file)){
    $get=$odata->GetInfos($params['displayMd5']);
    //If file is a camu, get adraws
    if($get['file_type'] == 'camu'){
      if($adraws = $odata->GetAdraws()){
        foreach($adraws as $oadraw){
          $infos[] = $oadraw->GetInfos($params['displayMd5']);
        }
      }
    }
    if(!empty($get)) {$infos[] = $get; $count ++;}
  }
} //End of while
@closedir($handle);

//Sort files by field
if(!empty($infos)){
  foreach ($infos as $key => $row) { //product a array from field to sort (for use array_multisort function)
     $field[$key]  = $row[$params['sort_field']];
  }

  array_multisort( $field, $params['sort_order'], $infos); //multidimensionnal sort

  //var_dump($field);
  //var_dump($infos);

  //Filter data from $term
  if(!empty($search_word) && !empty($find_field)){
    foreach($infos as $file){
      if(preg_match ( '/'.$search_word.'/i' , $file[$find_field] ))
          $finfos[] = $file;
    }
  return $finfos;
  }

  return $infos;
}

return false;

}//End of method

//-------------------------------------------------------
/*! \brief Alternative (peu performante) � disk_total_space qui ne comprend pas les chemins
* relatifs (sur windows)
* source du scripts : http://fr3.php.net/manual/fr/function.disk-total-space.php
*/
static function dskspace($path){
 $s = filesize($path);
 $size = $s;
 if (is_dir($path)){
   $dh = opendir($path);
   while (($file = readdir($dh)) !== false)
     if ($file != "." and $file != "..")
       $size += self::dskspace($path."/".$file);
   closedir($dh);
 }
 return $size;
}//End of method

} //End of class

?>
