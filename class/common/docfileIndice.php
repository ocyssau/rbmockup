<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('./class/common/recordfile.php');

/*! \brief Its a record in database of a file linked to the document.
*
*/
class docfileIndice extends recordfile{

//-------------------------------------------------------
/*! \brief Copy the file and the record of a file. It use for keep the file information when you create a new indice of the document.
    Return docfile object or false.
*
  \param $current_indice(integer) the current indice of the document
  \param $new_father_id(integer) the new document id
  \param $container(object) the new container object
*
* Le docfile est d�plac� danbs le dossier __indices. Son id est inchang� afin de conserver les liens avec ses versions.
* Un nouveau docfile est cr�� avec ses fichiers dans le new_container
*/
function KeepIndice($current_indice, $next_father_id, &$next_container=NULL){
  $this->logger->log( 'Keep indice file: '.$this->GetProperty('file') );
  if( empty($current_indice) ){
    $this->error_stack->push(ERROR, 'Fatal', array(), 'empty indice');
    return false;
  }

  $filepath = $this->GetProperty('file_path');

  //Init the target directory for new docfile. To do before file modifications to prevent errors
  if( is_a($next_container, 'container') ){
    $next_dstdir = $this->GetRepositdirPath($next_container);
    if($dstdir)
      $next_dstdir = $next_dstdir.'/'.$this->fsdata->getProperty('file_name');
    else return false;
  }else{
    $next_dstdir = $filepath;
  }

  //Create directories if necessary
  if(!is_dir($filepath .'/__indices'))//Create the indice dir
    if(!mkdir($filepath .'/__indices')){
      $this->error_stack->push(ERROR, 'Fatal', array('element'=>$filepath .'/__indices'), 'cant create indices dir %element%');
      return false;
    }
  if(!is_dir($filepath.'/__indices/'.$current_indice)) //Create the indice dir / indice number
    if(!mkdir($filepath.'/__indices/'.$current_indice)){
      $this->error_stack->push(ERROR, 'Fatal', array('element'=>$filepath.'/__indices/'.$current_indice), 'cant create indices dir %element%');
      return false;
    }

  $this->dbranchbe->StartTrans(); //Start a transaction

  //Update database...
  $filepath = $filepath.'/__indices/'.$current_indice;
  $data = array();
  $data['file_path'] = $filepath;
  if( !$this->UpdateRecordFile($data) ){ //Update file infos
    $this->dbranchbe->RollbackTrans();
    return false;
  }
  //...and move file
  $dstfile = $filepath.'/'.$this->GetProperty('file_name');
  if(!$this->fsdata->move($dstfile, true)){ //Move file
    $this->error_stack->push(ERROR, 'Fatal', array('file'=>$this->file_name, 'dstfile'=>$dstfile), 'can\'t move %file% to %dstfile%');
    $this->dbranchbe->RollbackTrans();
    return false;
  }

  $this->dbranchbe->CompleteTrans();
  $this->file_props['file_path'] = $target_dir; //Update property if validate only
  $this->error_stack->push(LOG, 'Info', array('element'=>$this->GetProperty('file_name')), 'data %element% is moved to '.$filepath);

  //Now file of current docfile is in __indices directory
  //Now create a new docfile from this files in $new_container :

  //Create a copy in original container of file
  if(!$next_docfile = $this->Copy($next_dstdir, $next_father_id, false)){
    $this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->file), 'error copy part %element%');
    return false;
  }

  return $next_docfile; //docfile object

}//End of method

}//End of class

?>
