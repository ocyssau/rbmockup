<?php
/*
CREATE TABLE `[space]_doc_files_versions` (
  `file_id` int(11) NOT NULL,
  `father_id` int(11) NOT NULL,
  `file_name` varchar(128)  NOT NULL,
  `file_path` text(128)  NOT NULL,
  `file_version` int(11) NOT NULL default '1',
  `file_root_name` varchar(128)  NOT NULL,
  `file_extension` varchar(16)  default NULL,
  `file_state` varchar(16)  NOT NULL default 'init',
  `file_type` varchar(128)  NOT NULL,
  `file_size` int(11) default NULL,
  `file_mtime` int(11) default NULL,
  `file_md5` varchar(128)  default NULL,
  `file_open_date` int(11) NOT NULL,
  `file_open_by` int(11) NOT NULL,
  `file_update_date` int(11) default NULL,
  `file_update_by` int(11) default NULL,
  PRIMARY KEY  (`file_id`),
  UNIQUE KEY `uniq_filename_filepath` (`file_name`,`file_path`(128)),
  KEY `IK_[space]_doc_files_version_1` (`father_id`),
  KEY `IK_[space]_doc_files_version_2` (`file_name`),
  KEY `IK_[space]_doc_files_version_3` (`file_path`(128)),
  KEY `IK_[space]_doc_files_version_4` (`file_version`,`father_id`)
) ENGINE=InnoDB ;

*/
require_once('class/common/recordfile.php');
/*! \brief It's a record in database of a file linked to a container.
*
*/
class recordfileVersion extends recordfile {

/*! \brief Constructor
*/
function __construct(space $space , $file_id=NULL){

  $this->space =& $space;
  $this->error_stack =& $this->space->error_stack;

  global $dbranchbe;
  $this->dbranchbe =& $dbranchbe;

  global $usr;
  $this->usr =& $usr;

  global $logger;
  $this->logger =& $logger;

  $this->CurrentUserId = $this->usr->getProperty('auth_user_id');
  
  $this->OBJECT_TABLE  = $this->space->SPACE_NAME.'_doc_files_versions';
  $this->FILE_TABLE  =& $this->OBJECT_TABLE;

  $this->FIELDS_MAP_ID = 'file_id';
  $this->FIELDS_MAP_NUM = 'file_name';
  $this->FIELDS_MAP_DESC = 'file_name';
  $this->FIELDS_MAP_FATHER = 'father_id';

  //init file id
  if(!empty( $file_id ) ){
    $this->file_id = $file_id;
  }
  
}//End of method

//-------------------------------------------------------
/*!\brief
* init current object from version recordfile of file id $father_id
* return id of the recordfileVersion or false
*/
function initVersion($father_id, $versionId){
  $query = 'SELECT file_id FROM '.$this->OBJECT_TABLE.' WHERE father_id = \''.$father_id.'\' AND file_version =\''.$versionId.'\'';
  $res = $this->dbranchbe->GetOne($query);
  if($res === false){
    $error_msg = $this->dbranchbe->ErrorMsg();
    if( !empty($error_msg) ){
      $this->error_stack->push(ERROR, 'Fatal', array(), $error_msg );
      return false;
    }
  }else{
    $this->file_props['father_id'] = $father_id;
    $this->father_id =& $this->file_props['father_id'];
  
    $this->file_props['file_version'] = $versionId;
    $this->versionId =& $this->file_props['file_version'];

    return $this->file_id = $res;
  }
}//End of method

//-------------------------------------------------------
/*!\brief
* Get versions recordfile object of the file id $father_id
* return array of objects or false
* $object (bool) if true return an array of recordfileVersion objects, else return array
*/
function GetVersions($father_id, $object = false){
    $query = 'SELECT * FROM '.$this->OBJECT_TABLE.' WHERE father_id = \''.$father_id.'\'';
    $res = $this->dbranchbe->execute($query);
    if($res === false ){
      $this->error_stack->push(ERROR, 'Fatal', array(), $this->dbranchbe->ErrorMsg());
      return false;
    }
    if($object){
      $i=0;
      while($row = $res->FetchRow() ){
        $result[$i] = clone($this); //clone current object
        $result[$i]->init($row['file_id']);
        $i++;
      }
      return $result;
    }else{
      return $res->GetArray();
    }
}//End of method

//--------------------------------------------------------
/*!\brief
* Get the fsdata object and link it to the current recordfile
* return a fsdata instance if success, else false if data is not find on filesystem.
* Set object $this->fsdata
* The method init() must be call before.
*/
/*
function initFsdata($datapath=NULL){
  require_once('./class/common/fsdataVersion.php');
  if(is_null($datapath))
    $datapath = $this->GetProperty('file_path').'/'.$this->GetProperty('file_name');
  $this->error_stack->push(LOG, 'Info', array('datapath'=>$datapath), 'init fsdata from : %datapath%');
  $this->fsdata = new fsdataVersion($datapath, $this->file_version);
  $this->fsdata->SetRecordfile($this);
  if($this->fsdata->getProperty('dataType') === false) return false;
  return $this->fsdata;
}//End of method
*/

/*!\brief create a version of the recordfile $recordfile
*/
function KeepVersion(&$recordfile){
  if(DEFAULT_BACK_VERSIONS_NUM === 0) //Dont keep version if 0
    return false;

  $this->logger->log( 'create version from file: '.$recordfile->GetProperty('file') );

  $current_version = $recordfile->GetProperty('file_version');
  if( empty($current_version) ){
    $this->error_stack->push(ERROR, 'Fatal', array('element'=>$recordfile->GetProperty('file_name')), 'none version for recordfile %element%');
    return false;
  }
  if( !$this->InitDirectory($recordfile, $current_version) )
    return false;

  //Get fstdata object of recordfile
  $fsdata =& $recordfile->GetFsdata();
  if(!is_object($fsdata)){
    $this->error_stack->push(ERROR, 'Fatal', array('element'=>$recordfile->GetProperty('file_name')), 'none valid fsdata for recordfile %element%');
    return false;
  }

  //create a copy of the fsdata in __versions directory
  $dst = $this->versionReposit.'/'.$recordfile->GetProperty('file_name');
  if( !$fsdata->copy($dst,0755,true) ){ //replace file if exist
    $this->error_stack->push(ERROR, 'Fatal', array('element'=>$dst ), 'copy error of file to %element%');
    return false;
  }

  $copyfsdata = fsdata::_dataFactory($dst);
  //create a new record of version from recordfile datas and update father and path
  $this->file_props = $recordfile->GetInfos( array('file_name','file_version','file_root_name',
                'file_extension','file_state','file_type','file_size','file_mtime','file_md5',
                'file_open_date','file_open_by','file_update_date','file_update_by') );
  $this->file_props['father_id'] = $recordfile->GetId();
  $this->file_props['file_path'] = $copyfsdata->getProperty('file_path');
  $this->file_id = $this->BasicCreate($this->file_props);
  
  if(!$this->file_id) return false;

  return $this->file_id;

}//End of method

//--------------------------------------------------------
/*!\brief
* init the directory where store version file copy
*   
*/
function InitDirectory(recordfile &$recordfile, $versionId){

  $this->versionReposit = $recordfile->GetProperty('file_path').'/__versions/'.$versionId;

  if(!is_dir($this->versionReposit)){ //Check if version reposit dir is existing (ie: :/aff01/__versions/)
    if(!rdirectory::createdir($this->versionReposit, 0755)){ //..and create it if not
      $this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->versionReposit), 'cant create version dir : %element%.');
      return false;
    }
  }
/*
  if(!is_dir($this->getProperty('file_path'))){ // check if version dir is existing (ie: c:/aff01/__versions/1/camu)
    if(!rdirectory::createdir($this->getProperty('file_path'), 0755)){ //..and create it if not
      $this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->getProperty('file_path')), 'cant create version dir : %element%.');
      return false;
    }
  }
*/
return true;
}//End of method

//--------------------------------------------------------
/*!\brief get the reposit path
*   
*/
function GetVersionReposit(){
  return $this->versionReposit;
}//End of method

//-------------------------------------------------------
/*! \brief get the full path where store data on server from the container properties
    Return string or false.
*
  \param $container(object) the container
*/
function GetRepositdirPath(container &$container){

  $versionId = $this->GetProperty('file_version');
  $repositDir = $container->GetProperty('default_file_path').'/__versions/'.$versionId;

  //create subdir if not exist
  require_once('class/common/rdirectory.php');
  if(!is_dir($repositDir)){ //Check if version reposit dir is existing (ie: :/aff01/__versions/)
    if(!rdirectory::createdir($repositDir, 0755)){ //..and create it if not
      $this->error_stack->push(ERROR, 'Fatal', array('element'=>$repositDir), 'cant create version dir : %element%.');
      return false;
    }
  }
  return $repositDir;

}//End of method

} //End of class
?>
