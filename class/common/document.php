<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('./class/common/basic.php');

/*
 CREATE TABLE `workitem_documents` (
 `document_id` int(11) NOT NULL default '0',
 `document_number` varchar(128)  NOT NULL default '',
 `document_state` varchar(32)  NOT NULL default 'init',
 `document_access_code` int(11) NOT NULL default '0',
 `document_version` int(11) NOT NULL default '1',
 `workitem_id` int(11) NOT NULL default '0',
 `document_indice_id` int(11) default NULL,
 `instance_id` int(11) default NULL,
 `doctype_id` int(11) NOT NULL default '0',
 `default_process_id` int(11) default NULL,
 `category_id` int(11) default NULL,
 `check_out_by` int(11) default NULL,
 `check_out_date` int(11) default NULL,
 `designation` varchar(128)  default NULL,
 `update_date` int(11) default NULL,
 `update_by` decimal(10,0) default NULL,
 `open_date` int(11) default NULL,
 `open_by` int(11) default NULL,
 `from_document_id` int(11) default NULL,
 PRIMARY KEY  (`document_id`),
 UNIQUE KEY `IDX_workitem_documents_1` (`document_number`,`document_indice_id`),
 KEY `K_workitem_documents_2` (`doctype_id`),
 KEY `K_workitem_documents_3` (`document_indice_id`),
 KEY `K_workitem_documents_4` (`workitem_id`),
 KEY `K_workitem_documents_5` (`category_id`)
 ) ENGINE=InnoDB ;

 ALTER TABLE `workitem_documents`
 ADD CONSTRAINT `FK_workitem_documents_2` FOREIGN KEY (`doctype_id`) REFERENCES `doctypes` (`doctype_id`),
 ADD CONSTRAINT `FK_workitem_documents_3` FOREIGN KEY (`document_indice_id`) REFERENCES `document_indice` (`document_indice_id`),
 ADD CONSTRAINT `FK_workitem_documents_4` FOREIGN KEY (`workitem_id`) REFERENCES `workitems` (`workitem_id`),
 ADD CONSTRAINT `FK_workitem_documents_5` FOREIGN KEY (`category_id`) REFERENCES `workitem_categories` (`category_id`);

 */


//-------------------------------------------------------------------------

/*! \brief This class manage documents.
 *
 * A document is component of ranchbe wich is manage by a container type "document_manager".
 * A container is a "document_manager" if his property "file_only"=false.
 * The document has 0 to n docfiles linked.
 */
class document extends objects{

	protected $OBJECT_TABLE; //(String)Table where are stored the containers definitions
	protected $HISTORY_TABLE; //(String)Table were are stored the container histories
	protected $INDICE_TABLE; //(String)Table where are stored the container indices
	//protected $METADATA_TABLE; //(String)
	//Fields:
	protected $FIELDS_MAP_ID; //(String)Name of the field of the primary key
	protected $FIELDS_MAP_NUM; //(String)Name of the field to define the name
	protected $FIELDS_MAP_DESC; //(String)Name of the field where is store the description
	protected $FIELDS_MAP_STATE; //(String)Name of the field where is store the state
	protected $FIELDS_MAP_INDICE; //(String)Name of the field where is store the indices
	protected $FIELDS_MAP_DEPOSITDIR; //(String)Name of the field where is store the default deposit directory
	protected $FIELDS_MAP_FATHER; //(String)Name of the field where is store the id of the father

	protected $document_id; //(Integer) id of the current document

	private $doc_props; //(Array) contains the properties of the current document
	protected $container; //(Object) object of the container of the current document
	protected $docfiles; //(Array) array of objects of the docfiles of the current document
	protected $metadata; //(Object) Object of the metadata linked to the current document
	protected $doctype; //(Object) Object of the doctype of current document

	private $document_number; //(String) number of the current document
	private $document_version; //(Integer) number of the document version
	private $document_indice_id; //(Integer) id of the document indice
	private $document_access_code; //(Integer) id of the access code

	private $Assocfiles; //(Array) files to associate to document
	private $AssociatedFiles; //(Array) files associated to document

	//--------------------------------------------------------------------
	function __construct(space &$space, $document_id=NULL){
		$this->space =& $space;
		$this->error_stack =& $this->space->error_stack;

		foreach(get_object_vars($this->space) as $var=>$value){
			$this->$var = $value;
		}

		global $dbranchbe;
		$this->dbranchbe =& $dbranchbe;

		global $logger;
		$this->logger =& $logger;

		global $usr;
		$this->usr =& $usr;

		$this->OBJECT_TABLE  = $this->space->DOC_TABLE;
		$this->HISTORY_TABLE = $this->space->DOC_HISTORY_TABLE;
		$this->INDICE_TABLE  = $this->space->DOC_INDICE_TABLE;
		//$this->METADATA_TABLE  = $this->space->DOC_METADATA_TABLE;
		$this->METADATA_TABLE = $this->SPACE_NAME.'_metadata';

		$this->FIELDS_MAP_ID = $this->space->DOC_FIELDS_MAP_ID;
		$this->FIELDS_MAP_NUM = $this->space->DOC_FIELDS_MAP_NUM;
		$this->FIELDS_MAP_DESC = $this->space->DOC_FIELDS_MAP_DESC;
		$this->FIELDS_MAP_STATE = $this->space->DOC_FIELDS_MAP_STATE;
		$this->FIELDS_MAP_INDICE = $this->space->DOC_FIELDS_MAP_INDICE;
		$this->FIELDS_MAP_FATHER = $this->space->DOC_FIELDS_MAP_FATHER;

		$this->FIELDS_MAPPING = array($this->FIELDS_MAP_NUM=>'Number',
		$this->FIELDS_MAP_DESC=>'Description',
		$this->FIELDS_MAP_STATE=>'State',
		);

		$this->FILE_TABLE = $this->DOC_FILE_TABLE;

		if(!is_null($document_id))
		$this->init($document_id);
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief init the properties of the document
	 *  Return boolean
	 *
	 * \param $document_id(integer) Id of the document
	 */
	function init($document_id){
		$this->document_id = $document_id;
		if(isset($this->doc_props)) unset($this->doc_props);
		if(isset($this->container)) unset($this->container);
		if(isset($this->docfiles)) unset($this->docfiles);
		if(isset($this->history)) unset($this->history);
		return true;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Return properties of current object
	 *  Return array or false
	 */
	function GetProperties(){
		if(!isset($this->doc_props)) return false;
		else return $this->doc_props;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Populate properties from values stored in database
	 *  Return array or false
	 */
	function initProperties(){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;}

			return $this->doc_props = $this->GetBasicInfos($this->document_id);

	}//End of method

	//----------------------------------------------------------
	/*! \brief Get the property of the document by the property name. init() must be call before
	 *
	 * \param $property_name(string) = document_id, document_number, ...etc
	 */
	function GetDocProperty($property_name){
		switch($property_name){
			case 'father_id':
			case 'container_id':
				if(isset($this->doc_props[$this->FIELDS_MAP_FATHER]))
				return $this->doc_props[$this->FIELDS_MAP_FATHER];
				else $realName = $this->FIELDS_MAP_FATHER;
				break;

				//TODO : cr�er une fonction pour cr�er la pseudo propri�t�e document_normalized_name.
				//TODO : Il faut cr�er une requete qui transforme le document_indice_id en nom d'indice usuel(A,B,C...).
			case 'document_normalized_name':
				return $this->GetDocProperty('document_number').'-v'.$this->GetDocProperty('document_indice').'.'.$this->GetDocProperty('document_version');
				break;

			case 'document_indice':
				return $this->GetDocProperty('document_indice_id');
				break;

			default:
				if(isset($this->doc_props[$property_name]))
				return $this->doc_props[$property_name];
				else $realName = $property_name;
				break;
		} //End of switch

		if(!isset($this->document_id)){
			$this->error_stack->push(LOG, 'Info', array(), '$this->document_id is not set');
			return false;}

			if(empty($query))
			$query = 'SELECT '.$realName.' FROM '.$this->OBJECT_TABLE.' WHERE '.$this->FIELDS_MAP_ID.' = \''.$this->document_id.'\'';

			$res = $this->dbranchbe->GetOne($query);
			if($res === false){
				$this->error_stack->push(ERROR, 'Fatal', array(), $this->dbranchbe->ErrorMsg() );
				return false;
			}
			else
			return $this->doc_props[$property_name] = $res;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Set property of the document by the property name.
	 * Return true or false
	 *
	 * \param $property_name(string) = document_id, document_number, ...etc
	 * \param $property_value(string)
	 */
	function SetDocProperty($property_name, $property_value){
		switch($property_name){
			case 'container_id':
				$this->doc_props[$this->FIELDS_MAP_FATHER] = $property_value;
				break;
			default:
				$this->doc_props[$property_name] = $property_value;
				break;
		} //End of switch
		return true;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Unset a property of the document.
	 * Return true or false
	 *
	 * \param $property_name(string) = document_id, document_number, ...etc
	 */
	function UnSetDocProperty($property_name){
		switch($property_name){
			case 'container_id':
				unset($this->doc_props[$this->FIELDS_MAP_FATHER]);
				break;
			default:
				unset($this->doc_props[$property_name]);
				break;
		} //End of switch
		return true;
	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 Record document history from $this->history var
	 Return true or false
	 *
	 \param $document_id(integer), id of the document.
	 */
	function WriteHistory(){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;
		}
		$infos = $this->GetDocumentInfos(); //Get infos about document
		$this->history[$this->FIELDS_MAP_ID] = $this->document_id;
		$this->history[$this->CONT_FIELDS_MAP_ID] = $infos[$this->CONT_FIELDS_MAP_ID];
		$this->history['document_number'] = $infos['document_number'];
		if(!isset($this->history['document_indice_id']))
		$this->history['document_indice_id'] = $infos['document_indice_id'];
		$this->history['document_version'] = $infos['document_version'];
		$this->history['doctype_id'] = $infos['doctype_id'];
		$ohistory = new history($this);
		return $ohistory->write($this->history);
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * This method can be used to get list of documents in the current space
	 * return a array if success, else return false.
	 \param $params(array) is use for manage the display. See parameters function of GetQueryOptions()
	 */
	function GetDocuments($params, $displayHistory=false){
		if(!$displayHistory){
			$params['where'][] = 'document_access_code NOT BETWEEN 15 AND 20';
		}
		return $this->GetAllBasic($params);
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * This method can be used to get record in database infos about document.
	 * return an array if success, else return false.
	 * If $document_id is not passed in parameter, get infos about the current document and reinit the properties for current object from the database
	 *
	 \param $document_id(integer) id of the document
	 \param  $selectClose(array) to define the sql select option
	 */
	function GetDocumentInfos($document_id=NULL , $selectClose = ''){
		if(is_null($document_id)){
			if(!isset($this->document_id)){
				$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
				return false;}
				$document_id =& $this->document_id;
				//$check=true;
		}

		//$res = $this->GetBasicInfos($document_id , $selectClose);
		//if($check)
		//$this->doc_props = $res;
		//return $res;
		return $this->GetBasicInfos($document_id , $selectClose);
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * Get the container linked to the current document
	 * return an instance of container if success, else return false.
	 * Set the container object $this->container
	 *
	 */
	function GetContainer(){
		if(isset($this->container)){
			return $this->container;
		}else{
			return $this->initContainer();
		}
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * Get the container object of the current document.
	 * return a container instance if success, else fase.
	 * Set the container object $this->container
	 * The method init() must be call before.
	 * Example :
	 * $container = new container($space, 1);
	 * $doc = new document($space); $doc->init(1); //OU $doc = new document($space, 1);
	 * $container = $doc->initContainer(); //the object $container is linked to the object $doc.
	 *
	 */
	function initContainer($container_id=NULL){
		if(is_null($container_id)){
			if( $container_id = $this->GetDocProperty('container_id') ){
				if( !isset($this->container) ){
					require_once('./class/common/container.php');
					return $this->container = container::_factory($this->SPACE_NAME, $container_id);
				}
				else{
					$this->container->init($container_id);
					return $this->container;
				}
			}else
			return false;
		}else{
			require_once('./class/common/container.php');
			if($this->container = container::_factory($this->SPACE_NAME, $container_id)){
				$this->doc_props[$this->FIELDS_MAP_FATHER] = $container_id;
				return $this->container;
			}
		}
		return false;
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * Alias of initContainer
	 */
	function initFather($father_id=NULL){
		return $this->initContainer($father_id);
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 *
	 */
	function initObservers(){
		//Attach object to listen observers to notify
		require_once('./class/observers/doctypeScript.php');
		$doctypeScript = new doctypeScript($this);
		$this->attach_all($doctypeScript);
		require_once('./class/observers/containerScript.php');
		$containerScript = new containerScript($this);
		$this->attach_all($containerScript);
	}//End of method

	//--------------------------------------------------------
	/*!\brief Create an docfile object and create bidirectionnal links
	 * return a docfile instance if success, else fase.
	 * Set the container object $this->docfiles
	 *
	 */
	function initDocfile($file_id=NULL){
		require_once('./class/common/docfile.php');
		if(is_null($file_id))
		$this->docfiles[] = new docfile($this->space);
		else
		$this->docfiles[] = new docfile($this->space, $file_id);
		$docfile =& end($this->docfiles);
		$docfile->SetDocument($this);
		return $docfile;
	}//End of method

	//--------------------------------------------------------
	/*!\brief Create an doctype object and create links
	 *
	 */
	function initDoctype($doctype_id = 0){
		if($doctype_id == 0)
		$doctype_id = $this->GetDocProperty('doctype_id');
		if(is_a($this->doctype , 'doctype') )
		$this->doctype->init($doctype_id);
		else{
			require_once './class/doctype.php';
			$this->doctype = new doctype($doctype_id); //Create new doctype
		}
		return $this->doctype;
	}//End of method

	//--------------------------------------------------------
	/*!\brief Return doctype of current document. If doctype is not set call initDoctype()
	 *   return doctype object
	 */
	function GetDoctype(){
		if(isset($this->doctype)) return $this->doctype;
		else return $this->initDoctype();
	}//End of method

	//--------------------------------------------------------
	/*!\brief Create an docfile object and create bidirectionnal links
	 * return array of docfile instance if success, else fase.
	 * Set $this->docfile
	 *
	 \param $params is use for manage the display. See parameters function of GetQueryOptions()
	 */
	function InitDocfiles($params=array()){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;}

			$params['exact_find'][$this->FIELDS_MAP_ID] = $this->document_id;
			$this->GetQueryOptions($params);
			$query = 'SELECT file_id FROM '.$this->DOC_FILE_TABLE
			.' '.$this->whereClose
			.' '.$this->orderClose;
			$col = $this->dbranchbe->GetCol($query);
			if($col === false){
				$this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query), $this->dbranchbe->ErrorMsg());
				return false;
			}
			unset($this->docfiles);
			if(count($col) == 0) return false;
			require_once('./class/common/docfile.php');
			foreach($col as $file_id){
				$this->docfiles[] = new docfile($this->space, $file_id);
				$docfile =& end($this->docfiles);
				$docfile->SetDocument($this);
			}
			return $this->docfiles;
	}//End of method

	//--------------------------------------------------------
	/*!\brief Get a docfile object from $this->docfiles property
	 * return a docfile object if success, else fase.
	 *
	 \param $key $this->docfiles array key of docfile to get. 0 for main file
	 */
	function GetDocfile($key=0){
		if(!isset($this->docfiles))
		if(!$this->InitDocfiles()){ //Try to set $this->docfiles
			$this->error_stack->push(ERROR, 'Error', array(), '$this->docfiles is not set');
			return false;
		}
		if(isset($this->docfiles[$key]))
		return $this->docfiles[$key];
		else{
			$this->error_stack->push(ERROR, 'Error', array(), 'no docfile for this key');
			return false;
		}
	}//End of method

	//--------------------------------------------------------
	/*!\brief Get objects collection from $this->docfiles property
	 * return array or false.
	 *
	 */
	function GetDocfiles(){
		if( !isset($this->docfiles) ){
			if(!$this->InitDocfiles()){ //Try to set $this->docfiles
				$this->error_stack->push(LOG, 'Error', array(), '$this->docfiles is not set');
				return false;
			}
		}
		return $this->docfiles;
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * Set the container of the document by reference
	 * Example :
	 * $container = new container($space, 1);
	 * $doc = new document($space, 1);
	 * $doc->SetContainer($container); //The object $container is now link to object $doc
	 *
	 *
	 \param $container(object) object of the container
	 */
	function SetContainer(container &$container){
		$this->container =& $container;
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * Set the docfiles of the document by reference
	 *
	 \param $docfile(object) docfile object
	 */
	function SetDocfile(docfile &$docfile){
		$this->docfiles[] =& $docfile;
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * This method return the document id of the document number in the max indice if exist.
	 * else return false.
	 *
	 \param $document_number(string) number of the document
	 */
	function DocumentExist( $document_number=NULL ){
		if(is_null($document_number))
		if(isset($this->doc_props['document_number']))
		$document_number = $this->doc_props['document_number'];
		else return false;

		/*
		 $query =  'SELECT document_id'
		 .' FROM '.$this->OBJECT_TABLE
		 .' WHERE (document_indice_id, document_number) IN
		 (SELECT MAX(document_indice_id), document_number
		 FROM '.$this->OBJECT_TABLE
		 ." WHERE document_number = '".$document_number
		 ."' GROUP BY document_number)";
		 */

		$query =  "SELECT document_id FROM $this->OBJECT_TABLE
            WHERE document_number = '$document_number'
            AND document_indice_id = 
            (SELECT MAX(document_indice_id)
            FROM $this->OBJECT_TABLE
            WHERE document_number = '$document_number'
            GROUP  BY document_number)";

		$one = $this->dbranchbe->GetOne($query);
		return $one;
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * Return the current indice id for the document_number
	 * else return false.
	 *
	 \param $document_number(string) number of the document
	 */
	function GetCurrentIndice($document_number){
		$query = 'SELECT MAX(document_indice_id)'
		.' FROM '.$this->OBJECT_TABLE
		.' WHERE `document_number` = \''.$document_number.'\''
		.' GROUP BY document_number';
		$one = $this->dbranchbe->GetOne($query);
		return $one;
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * This method return the document number from the document_id
	 * else return false.
	 *
	 \param $document_id(integer) id of the document
	 */
	function GetNumber($document_id=NULL){
		if(is_null($document_id)){
			if(isset($this->document_id))
			$document_id =& $this->document_id;
			else{
				$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
				return false;}
		}

		if(isset($this->doc_props['document_number']))
		return $this->doc_props['document_number'];

		$this->SetFileTable();
		$query = 'SELECT document_number'
		.' FROM '.$this->OBJECT_TABLE
		.' WHERE document_id = '.$document_id;
		$one = $this->dbranchbe->GetOne($query);
		return $one;
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * Return the document id
	 * else return false.
	 *
	 */
	function GetId(){
		if(is_null($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;
		}else return $this->document_id;
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 *
	 */
	function GetClassName(){
		return $this->GetNumber();
	}//End of method

	//--------------------------------------------------------------------
	/*!\brief
	 * To recognize the type of document
	 * Return an array with the doctype infos else return false.
	 * Set $this->doctype object
	 *
	 \param $file_extension(String) extension of the file
	 \param $file_type(String) type of file: file, cadds5, cadds4...
	 */
	//TODO : Return a doctype object
	function SetDocType($file_extension, $file_type){
		if(!isset($this->container) && isset($this->document_id))
		$this->initContainer();

		if(!isset($this->container)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->container is not set');
			return false;}

			//If there is at least one doctype link to container,
			//then doctype choice is restricted to this list.
			//First, test if there is at least one doctype link to container
			if($this->container->HasDoctype())
			$doctypeList = $this->container->GetDoctypes($file_extension , $file_type, array(), true); //Get array of all doctype linked to container
			else{ //If no doctypes links to container, get all doctypes
				require_once './class/doctype.php';
				$this->doctype = new doctype();
				if(!is_null($file_extension)) $dtparams['find']['file_extension'] = $file_extension;
				if(!is_null($file_type)) $dtparams['find']['file_type'] = $file_type;
				$doctypeList = $this->doctype->GetAll($dtparams);
			}
			//var_dump($doctypeList);
			//If no valid doctypes, then this document is not authorized
			if(!$doctypeList){
				$this->error_stack->push(ERROR, 'Fatal', array('element'=>$doctypeList), 'This doctype %element% is not permit');
				return false;
			}

			//For each doctype test if current document corresponding to regex
			foreach($doctypeList as $type)
			if(!empty($type["recognition_regexp"])){
				if(preg_match('/'.$type['recognition_regexp'].'/', $this->GetDocProperty('document_number') ) ){
					$doctype = $type; //assign doctype and exit loop
					break;}
			}else $genericDoctype = $type; //if recognition_regexp field is empty, its a generic doctype

			//If no match doctypes, then assign the generic doctype, else return false
			if(empty($doctype) && !empty($genericDoctype)){
				//But the number must be respect the defined generic regex
				$genericRegEx = DEFAULT_DOCUMENT_MASK;
				if(!empty($genericRegEx)){
					if(preg_match('/'.$genericRegEx['recognition_regexp'].'/', $this->GetDocProperty('document_number') ) )
					$doctype = $genericDoctype;
				}else $doctype = $genericDoctype;
			}
			if(empty($doctype)){
				$this->error_stack->push(ERROR, 'Fatal', array('return'=>$doctypes), 'This doctype is not permit');
				return false;}

				$this->initDoctype($doctype['doctype_id']);
				return $doctype;
	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 * This method create a document in a container.
	 * Return true or false
	 *
	 * \param if $data['file'] is set, it create a document from the file
	 * else create the document $data['document_number']
	 * \param $lock(bool) if = true lock the document after store
	 * \param
	 Parameters to set<br />
	 $Ddata['file'](string) file and his path<br />
	 $Ddata['document_number'](string)<br />
	 $Ddata['document_indice_id'](int)<br />
	 $Ddata['designation'](string)<br />
	 $Ddata['container_id'](int) id of the container where store the new document<br />
	 $Ddata[category_id](int)<br />
	 */
	function Store($Ddata , $lock = false){
		$this->doc_props = array(); //initialise properties

		$allow_input = array('designation',
                        'category_id',
                        'document_number',
                        'document_indice_id',
                        'document_state',
                        'doctype_id',
                        'default_process_id',
                        'from_document_id',
                        'supplier_id',
                        'cad_model_supplier_id');

		require_once('./class/common/metadata.php');
		$this->metadata = new docmetadata($this->space);
		//$optionalFields = $this->metadata->GetMetadataLinked(array('select'=>array('l.field_name')), $Ddata['container_id'] );
		$optionalFields = $this->metadata->GetMetadata( array('select'=>array('field_name')) );
		foreach($optionalFields as $field){
			$allow_input[] = $field['field_name'];
		}

		//Filter input data for prevent maliscious informations
		foreach($allow_input as $input){
			if( isset($Ddata[$input]) )
			$this->doc_props[$input] = $Ddata[$input];
		}

		//$this->doc_props = $Ddata;

		$this->doc_props['document_version'] = 1;
		$this->doc_props['document_access_code'] = 0;
		if(!is_numeric($Ddata['document_indice_id']))
		$this->doc_props['document_indice_id'] = 1;

		if(is_numeric($Ddata['container_id'])){
			$container =& $this->initContainer($Ddata['container_id']);
		}else{
			$this->error_stack->push(ERROR, 'Fatal', array(), 'container_id is not set');
			return false;
		}
		
		if($container->GetProperty('access_code') > 100){
			$this->error_stack->push(ERROR, 'Fatal', array(), 'This container is archived');
			return false;
		}

		$this->initObservers();

		//Create a document from file or just create a document from the document_number
		if(!empty($Ddata['file'])){ //If a file is require
			$this->error_stack->push( LOG, 'Info', array(), 'create document from file : '.$Ddata['file'] );
			$docfile =& $this->initDocfile(); //Get the docfile
			if( !$fsdata =& $docfile->initFsdata($this->WILDSPACE .'/'. $Ddata['file']) ){
				$this->error_stack->push( ERROR, 'Error', array('file'=>$Ddata['file']), '%file% is not in your wildspace' );
				return false;
			}
			$file_extension = $fsdata->getProperty('file_extension');
			$file_type = $fsdata->getProperty('file_type');
			$this->doc_props['document_number'] = $fsdata->GetProperty('doc_name');
			if(!$this->doc_props['document_number']){
				$this->error_stack->push(ERROR, 'Fatal', array(), 'The file is unreachable');
				return false;}
		}else{ // if no file
			$this->logger->log('create document from document number : '.$this->doc_props['document_number']);
			$file_extension = NULL;
			$file_type = 'nofile';
		}

		//Execute evenement scripts
		$this->notify_all('doc_pre_store', $this); //Notify observers on event
		if( $this->stopIt ) { //use by observers to control execution
			return false;
		}

		//Check that document_number is set
		if(empty($this->doc_props['document_number'])){
			$this->error_stack->push(ERROR, 'Fatal', array(), 'document number is not set');
			return false;
		}

		//Check that Document_number is not existing
		if( $id = $this->DocumentExist() ){
			$this->error_stack->push(ERROR, 'Fatal', array(), 'document number existing yet (id=' . $id .')');
			return false;
		}

		//Set the doctype of the current document
		if( !$this->doc_props['doctype_id'] ){
			if($file_extension && $file_type != 'nofile'){
				$doctype = $this->SetDocType($file_extension, $file_type);
				if(!$doctype) return false;
				$this->doc_props['doctype_id'] = $doctype['doctype_id'];
			}else{
				$this->doc_props['doctype_id'] = DEFAULT_DOCTYPE_ID;
			}
		}

		//Check if there are other file to associated to this document
		//$this->Assocfiles = $this->GetFilesToAssociate("$fileInfos[file_root_name]");

		$this->dbranchbe->StartTrans(); //Start a transaction

		//Create a new document
		if (!$this->CreateDocument($this->doc_props)){
			$this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->doc_props['document_number']), 'cant create the document %element%');
			return $this->dbranchbe->CompleteTrans(false);
		}

		//Store the docfiles
		if(is_array($this->docfiles))
		foreach($this->docfiles as $docfile){
			if(!$docfile->StoreFile())
			return $this->dbranchbe->CompleteTrans(false);
		} //End of foreach

		if($this->dbranchbe->CompleteTrans() === false){
			$this->error_stack->push(ERROR, 'Fatal', array(), $this->dbranchbe->ErrorMsg());
			return false;
		}

		//Execute post_store scripts
		/*if(!empty($doctype['script_post_store'])){
		 if(is_file(DEFAULT_DOCTYPE_SCRIPTS_DIR .'/'. $doctype['script_post_store']))
		 include_once ( DEFAULT_DOCTYPE_SCRIPTS_DIR .'/'. $doctype['script_post_store']);
		 if(!doctypeScript_post_store($this)){
		 $this->error_stack->push(ERROR, 'Warning', array('element1'=>$document_number, 'element2'=>$doctypeInfo['script_post_store']),
		 'Document %element1% : Script %element2% failed or have none return. Add "return true" at function end');
		 return false;
		 }
		 }*/
		//Execute evenement scripts
		$this->notify_all('doc_post_store', $this); //Notify observers on event

		if($lock){ //If $lock = true, lock the document
			$this->LockDocument(11);
			$this->history['document_access_code'] = 11;
		}

		//Write history
		$this->history['action_name'] = 'Store';
		$this->WriteHistory();
		return true;
	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 * This method search in wildspace the file with the same root name that $document_number
	 * Return an array with files to associate and set $this->Assocfiles or return false
	 *
	 * \param $document_number(string) number of document to be associate to file
	 */
	private function GetFilesToAssociate($document_number){
		$files = glob( "$this->WILDSPACE" .'/'. "$document_number" . "*" );
		if (!$files) return false;
		$this->Assocfiles = $files;
		return $files;
	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 This method record the file $file from the wildspace in database
	 and copy it in vault and associate it to document $document_id
	 if suppress = true, the file will be suppress of the wildspace
	 *
	 * Return true or false
	 *
	 * @param string $file			path and file to associate
	 * @param boolean $suppress		true for suppress file of the wildspace after associate it
	 * @param boolean $checkAccess	if true, check access_code of document
	 */
	function AssociateFile($file , $suppress = false, $checkAccess = true){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;
		}
		if($checkAccess){
			//check if access is free
			$access = $this->CheckAccess();
			if($access > 0){
				$this->error_stack->push(ERROR, 'Fatal', array('element'=>$access), 'This document is locked with code %element%, associate is not permit');
				return false;
			}
		}
		$container =& $this->initContainer();//Get the container
		$docfile =& $this->initDocfile(); //Get the new docfile
		$docfile->initFsdata($file); //Get the new fsdata
		$this->initObservers();
		//Notify event to observers
		$this->notify_all('doc_pre_associateFile', $this);
		//Create a new file record in database
		$file_id = $docfile->StoreFile( $suppress );
		if(!$file_id){
			$this->error_stack->push(ERROR, 'Fatal', array('element'=>$file), 'cant record the file %element%');
			return false;
		}
		//Notify event to observers
		$this->notify_all('doc_post_associateFile', $this);
		return true;
	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 This method create a document record in database. Return false or document_id.
	 *
	 * Parameters to set:
	 document_number<br />
	 designation<br />
	 access<br />
	 document_indice_id<br />
	 document_version<br />
	 doctype_id<br />
	 document_access_code<br />
	 from_document_id<br />
	 category_id<br />
	 FIELDS_MAP_ID<br />
	 */
	private function CreateDocument($data){
		//Set current time
		$now = time();
		$data['open_by'] = $this->CurrentUserId;
		$data['open_date'] = $now;
		$data['update_by'] = $this->CurrentUserId;
		$data['update_date'] = $now;
		$data['document_state'] = 'init';

		if( empty($data[$this->FIELDS_MAP_FATHER]) && isset($this->container) ){
			$data[$this->FIELDS_MAP_FATHER] = $this->container->GetId();
		}else if ( empty($data[$this->FIELDS_MAP_FATHER]) ){
			$this->error_stack->push(LOG, 'Error', array('element'=>$file), 'none container specified');
			return false;
		}

		//Create entries in table
		$this->document_id = $this->BasicCreate($data);
		$this->doc_props = $data;
		$this->history = $data;
		return $this->document_id;
	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 Modify the document record. Return false or true.
	 *
	 \param $data(array), its an array where keys/values are fields/values of the record to update.
	 */
	function UpdateDocument($data){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;
		}
		return $this->BasicUpdate($data, $this->document_id);
	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 This method can be used to update the meta-informations about a document.
	 Meta-informations are all infos about document which are manualy input.
	 Return false or true.
	 *
	 * The input of this method is protected(not like the UpdateDocument method).
	 * Only designation, category_id, document_number and metadatas can be modify
	 * 
	 * @param $params(array), its an array where keys/values are fields/values of the record to update.
	 */
	function UpdateDocInfos($params){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;}

			if(!isset($this->container)){
				$this->initContainer();
				$this->error_stack->push(LOG, 'Debug', array(), '$this->container has been set');
			}

			$this->doc_props = array(); //initialise properties

			$allow_input = array('designation',
                        'category_id',
                        'document_number',
			);

			require_once('./class/common/metadata.php');
			$this->metadata = new docmetadata($this->space);
			$optionalFields = $this->metadata->GetMetadataLinked(array('select'=>array('l.field_name')), $this->GetDocProperty('container_id'));
			foreach($optionalFields as $field)
			$allow_input[] = $field['field_name'];

			//Filter input data for prevent maliscious informations
			foreach($allow_input as $input){
				if( isset($params["$input"]) )
				$this->doc_props["$input"] = $params["$input"];
			}

			//Prevent to set document_number to NULL
			if(empty($this->doc_props['document_number'])) unset($this->doc_props['document_number']);

			//Execute evenement scripts
			$this->initObservers();
			$this->notify_all('doc_pre_updateProperties', $this);
			if( $this->stopIt ) { //use by observers to control execution
				return false;
			}

			if(count($this->doc_props) == 0) return true;

			if(!$this->UpdateDocument($this->doc_props))
			return false;

			$this->notify_all('doc_post_updateProperties', $this);
			if( $this->stopIt ) { //use by observers to control execution
				return false;
			}

			//Write history
			$this->history['action_name'] = 'UpdateDocInfos';
			$this->WriteHistory();

			return true;
	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 Change the state of a document
	 Return false or true.
	 *
	 \param $state(string), New value of the state.
	 */
	function ChangeState($state='init'){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;}
			$data = array('document_state' => "$state");
			$ret = $this->UpdateDocument($data);
			$this->history['document_state'] = $state; //Use for history
			return $ret;
	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 This method can be used to lock a document.
	 Return false or true.
	 *
	 * The value of the access code permit or deny action:<ul>
	 <li>0: Modification of document is permit.</li>
	 <li>1: Modification of document is in progress. The checkOut is deny. You can only do checkIn.</li>
	 <li>5: A workflow process is in progress. Modification is deny. You can only activate activities.</li>
	 <li>10: The document is lock.You can only upgrade his indice.</li>
	 <li>11: Document is lock. You can only unlock it or upgrade his indice.</li>
	 <li>15: Indice is upgraded. You can only archive it.</li>
	 <li>20: The document is archived.</li>
	 </ul>
	 \param $code(integer), new access code.
	 */
	function LockDocument($code = 1){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;}
			$data['document_access_code'] = $code;
			if($code == 1){
				$data['check_out_by']   = $this->CurrentUserId;
				$data['check_out_date'] = time();
			}
			if($code == 0){
				$data['check_out_by']   = NULL;
				$data['check_out_date'] = NULL;
			}
			//Create entries in table
			if (!$this->dbranchbe->AutoExecute($this->OBJECT_TABLE , $data , 'UPDATE' , "$this->FIELDS_MAP_ID = $this->document_id")){
				$this->error_stack->push(ERROR, 'Fatal', array('query'=>'UPDATE' . implode(';',$data) , 'debug'=>array($data, $this->OBJECT_TABLE, $this->document_id)), $this->dbranchbe->ErrorMsg());
				return false;
			}
			$this->history['document_access_code'] = $code;
			return true;
	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 This method can be used to unlock a document.
	 Return false or true.
	 *
	 */
	function UnLockDocument(){
		return $this->LockDocument(0);
	}//End of method

	//-------------------------------------------------------
	/*!\brief
	 This method can be used to link document to a container.
	 Return false or true.
	 *
	 * A document must be linked to a container. This method set the [container type]_id field.
	 We can link a document to a container of the same space only.
	 \param $container_id(integer), id of the container
	 */
	function LinkDocumentToContainer($container_id){
		$data[$this->CONT_FIELDS_MAP_ID] = $container_id;
		return $this->UpdateDocument($data);
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * Get the list of indice and return array ( indice_id => indice_value)
	 * else return false.
	 \param $params is use for manage the display. See parameters function of GetQueryOptions()
	 */
	function GetIndices($currentIndice_id = 0){
		return $this->GetIndiceList($currentIndice_id);
	}//End of method

	//-------------------------------------------------------
	/*!\brief
	 * Put files of a document from the vault deposit directory to user wildspace and lock the document.
	 * return true or false.
	 *
	 * CheckOut is use for modify a document. The files are put in a directory where user has write access
	 * and the document storing on the protected directory (the vault) is lock. Thus it is impossible that a other user modify it in the same time.
	 */
	function CheckOut($checkoutFile = true){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;}
			if(!isset($this->container))
			$this->initContainer();

			//check if access is free
			$access_code = $this->CheckAccess();
			if($access_code){
				$this->error_stack->push(ERROR, 'Fatal', array('element'=>$access_code), 'this document is locked with code %element%, checkOut is not permit');
				return false;
			}

			$this->initObservers();

			/*About transaction and exception :
			 if an error occurs, rollback operations in database. doc and file are not checkout
			 but file(s) could remain in wildspace
			 */
			$this->dbranchbe->StartTrans(); //Start a smart transaction

			//Notiy observers on event
			$this->notify_all('doc_pre_checkout', $this);
			if( $this->stopIt ) { //use by observers to control execution
				return false;
			}

			//lock the document
			if (!$this->LockDocument(1)){
				$this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->document_id), 'unable to lock document');
				return $this->dbranchbe->CompleteTrans(false);
			}

			//get list of associated files and check access
			if($checkoutFile){
				//get list of associated files. Get only files with checkout access_code(file_acess_code=0)
				$this->InitDocfiles(array('where'=>array('file_access_code = 0')));
				if($this->docfiles === false){
					$this->error_stack->push(ERROR, 'Warning', array('element'=>$this->document_id), 'unable to list associated files');
				}
				//checkOut associated files
				if(is_array($this->docfiles)){
					foreach($this->docfiles as $docfile){
						if (!$docfile->CheckOutFile()){
							$this->error_stack->push(ERROR, 'Warning', array('element'=>$docfile->GetProperty('file_name') , 'debug'=>array($file)), 'unable to checkOut file %element%');
							return $this->dbranchbe->CompleteTrans(false); //Rollback. If one error on one file all transaction is rollback, so all document and his file are not checkout but files may be stay in wildspace.
						}
					}
				}
			}

			$this->dbranchbe->CompleteTrans();

			//Notiy observers on event
			$this->notify_all('doc_post_checkout', $this);

			//Write history
			$this->history['action_name'] = 'CheckOut';
			$this->WriteHistory();

			return true;

	}//End of method

	//-------------------------------------------------------
	/*!\brief
	 * Replace files from the wildspace to vault deposit directory and unlock the document.
	 * return true or false.
	 *
	 * CheckIn is use after modify a document. The files are copy from the user directory (the wildspace)
	 * to the protected and share deposit directory (the vault).
	 *
	 \param $update(bool), if true, copy files from wildspace to vault but dont unlock document.
	 \param $reset(bool), if true, unlock document but dont copy files from wildspace to vault.
	 *$reset and $update can be set true at the same time.
	 */
	function CheckIn($update = false , $reset = false){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;
		}

		$DocInfos = $this->GetDocumentInfos(); //init the properties of the document
		$document_number = $this->GetDocProperty('document_number');

		$this->initObservers();

		//Check if the document is checkOut by me
		$access = $this->CheckUpdateRight();
		if(!$access){
			$this->error_stack->push(ERROR, 'Fatal', array('access'=>$access , 'element'=> $document_number), 'checkIn of doc %element% is not permit');
			return false;
		}

		//get list of associated files. Get only files with checkout access_code(file_acess_code=1)
		if(!$this->InitDocfiles(array('where'=>array('file_access_code = 1')))){
			$this->error_stack->push(ERROR, 'Info', array('element'=>$document_number), 'None associated file to checkin for document %element%');
		}

		$this->dbranchbe->StartTrans(); //Start a smart transaction

		if(!$reset){
			$this->notify_all('doc_pre_update', $this); //Notify observers on event
		}else{ // End of !$reset
			$this->notify_all('doc_pre_reset', $this);
		}
		if( $this->stopIt ) { //use by observers to control execution
			$this->dbranchbe->CompleteTrans(false);
			//$this->dbranchbe->RollbackTrans();
			return false;
		}

		//Stop transaction to let object docfile to manage his transaction
		//because smart transaction is nestable and nested transaction are ignored
		$this->dbranchbe->CompleteTrans();

		//checkIn associated files
		if(is_array($this->docfiles)){
			foreach($this->docfiles as $docfile){
				if(!$docfile->CheckInFile($update , $reset)){
					$this->error_stack->push(ERROR, 'Warning', array('element'=>$docfile->GetProperty('file_name')), 'checkIn of file %element% failed');
					$checkinFileError = true;
					continue;
				}
				//Update document version
				if($docfile->GetProperty('file_version') > $this->GetDocProperty('document_version')){
					$this->doc_props['document_version'] = $docfile->GetProperty('file_version');
				}
			} //End of foreach
		}

		if(!$reset){ //update the update user informations
			$data['update_by']   = $this->CurrentUserId;
			$data['update_date'] = time();
			$data['document_version'] = $this->GetDocProperty('document_version');
			if(!$this->UpdateDocument($data)){
				$this->error_stack->push(ERROR, 'Fatal', array('data'=>$data , 'document_id'=>$document_id, 'element'=> $document_number), 'cant update doc %element%');
				return false;
			}
		}

		/*If a error is occured during chechin of files, get out function
		 file checkin with success are let in checkin state. File failed are in checkout state. Document is let in checkout state.
		 */
		if($checkinFileError){
			$this->error_stack->push(ERROR, 'Fatal', array('document_id'=>$document_id, 'element'=> $document_number), 'checkIn of document %element% failed or is not completed. <br />Correct the errors and retry.');
			return false;
		}

		$this->dbranchbe->StartTrans(); //Restart a smart transaction

		if(!$update){
			//Unlock the document
			if(!$this->UnLockDocument()){
				$this->error_stack->push(ERROR, 'Fatal', array('data'=>$data , 'document_id'=>$document_id, 'element'=> $document_number), 'cant unlock doc %element%');
				$this->dbranchbe->CompleteTrans(false);
				return false;
			}
		}

		if($this->dbranchbe->CompleteTrans() === false){
			$this->error_stack->push(ERROR, 'Fatal', array('element'=> $document_number, 'data'=>$data , 'document_id'=>$document_id), 'checkin of doc %element% is not possible');
			return false;
		}

		if(!$reset){
			$this->notify_all('doc_post_update', $this);
		}else{ // End of !$reset
			$this->notify_all('doc_post_reset', $this);
		}

		//Write history
		if($reset) $this->history['action_name'] = 'Reset';
		else if($update) $this->history['action_name'] = 'Update';
		else $this->history['action_name'] = 'CheckIn';
		$this->WriteHistory();

		return true;
	}//End of method

	//-------------------------------------------------------
	/*!\brief
	 * Unlock the document after a checkOut without copy files from wildspace to vault
	 * return true or false.
	 *
	 */
	function CancelCheckOut(){
		if ($this->CheckIn($update = false , $reset = true))
		return true ;
		else return false;
	}//End of method

	//-------------------------------------------------------
	/*!\brief
	 * This method can be used to check if a document is free.
	 * Return the access code (0 for free without restriction , 100 if error).
	 *
	 */
	function CheckAccess(){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;
		}
		$query = 'SELECT document_access_code FROM '.$this->OBJECT_TABLE
				.' WHERE document_id = '.$this->document_id;
		$One = $this->dbranchbe->GetOne($query);
		if($One === false){
			$this->error_stack->push( ERROR, 'Fatal', array('query'=>$query), $this->dbranchbe->ErrorMsg() );
			return 100;
		}else{
			return intval($One);
		}
	}//End of method

	//-------------------------------------------------------
	/*!\brief
	 This method can be used to check if the uers can update a document
	 Return true if access is free, else return false
	 *
	 \param $document_id(integer), id of the document.
	 */
	function CheckUpdateRight(){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;}

			$query = 'SELECT document_access_code , '.$this->FIELDS_MAP_ID.' , check_out_by , check_out_date FROM '.$this->OBJECT_TABLE
			.' WHERE document_id = '.$this->document_id;

			if(!$row = $this->dbranchbe->GetRow($query)){
				$this->error_stack->push(ERROR, 'Fatal', array('query'=>$query), $this->dbranchbe->ErrorMsg());
				return false;
			}else{
				if($row['document_access_code'] == 1){ //The document is checkOut...
					if($row['check_out_by'] == $this->CurrentUserId ){ //... by current user
						return true; //Update is permit for current user
					}
				}
			}

			return false;

	}//End of method

	//-------------------------------------------------------
	/*!\brief
	 This method can be used to get all the files associated to a document
	 Return a array or false
	 *
	 \param $params is use for manage the display. See parameters function of GetQueryOptions()
	 */
	function GetAssociatedFiles($params=array()){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;}
			$params['exact_find'][$this->FIELDS_MAP_ID] = $this->document_id;
			$this->GetQueryOptions($params);
			$query = 'SELECT '.$this->selectClose.' FROM '.$this->DOC_FILE_TABLE
			.' '.$this->whereClose
			.' '.$this->orderClose;
			if(!$All = $this->dbranchbe->Execute($query)){
				$this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query), $this->dbranchbe->ErrorMsg());
				return false;
			}else{
				if($All->RecordCount() === 0) return 0;
				$this->AssociatedFiles = $All->GetArray(); //To transform result in array
				return $this->AssociatedFiles;
			}
	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 This method can be used to mov a document from container to another.
	 Return true or false.
	 *
	 * A document can be move only between container of the same space.
	 * You can not move a document of a bookshop container to a cadlib container.
	 * This method modify the document record and move the associated files from the
	 * directory of the original container to the directory of the tager container.
	 * Please, consider that this method dont move version files and the previous indice document.

	 \param $document_id(integer), id of the document.
	 \param $target_container_id(integer), id of the target container.
	 */
	function MoveDocument($target_container_id){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;
		}

		//Get infos about target deposit dir
		$target_container = new container($this->space, $target_container_id);
		//$target_containerInfos = $target_container->GetInfos(NULL,array($this->CONT_FIELDS_MAP_NUM , $this->CONT_FIELDS_MAP_ID , 'default_file_path', 'file_only'));

		//Check if the target container is a document manager. If not, exit.
		if( $target_container->IsFileOnly() ){
			$this->error_stack->push(ERROR_DB, 'Fatal', array(), 'the target directory is a file manager type');
			return false;
		}
		//$target_dir = $target_container->GetProperty('default_file_path');

		//check if access is free
		if($this->CheckAccess()){
			$this->error_stack->push(ERROR, 'Fatal', array('element'=>$access), 'this document is locked with code %element%, move is not permit');
			return false;
		}

		$this->initObservers();
		//Notify event to observers
		$this->notify_all('doc_pre_move', $this);

		//TODO: verifier si le document n'existe pas dans la cible
		//>> Si l'on reste dans le meme espace le document doit �tre unique il ne peut donc y avoir de conflit
		//TODO: verifier que le conteneur cible accepte le doctype du document source
		//-- >> faire in store?
		//TODO: deplacer les fichiers de versions aussi

		//Move file from ori deposit_dir to target deposit_dir
		$assoc_files =& $this->InitDocfiles(); //Get the docfile associated to the current document
		if(is_array($assoc_files))
		foreach($assoc_files as $docfile){
			$docfile->MoveFile($target_container);
			//else error report generate by recordfile and continue on next data
		}

		//link document to a container
		$this->LinkDocumentToContainer($target_container_id);

		//Notify event to observers
		$this->notify_all('doc_post_move', $this);

		//Write history
		$this->history['action_name'] = 'MoveDocument';
		$this->WriteHistory();
	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 This method can be used to mov a document from container to another.
	 Return true or false.
	 *
	 * A document can be move only between container of the same space.
	 * You can not move a document of a bookshop container to a cadlib container.
	 * This method modify the document record and move the associated files from the
	 * directory of the original container to the directory of the tager container.
	 * Please, consider that this method dont move version files and the previous indice document.

	 \param $target_container_id(integer), id of the target container.
	 \param $target_name(string), number of the new document to create.
	 \param $target_indice(integer), id of indice for new document.
	 */
	function CopyDocument($target_container_id , $target_name , $target_indice){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;}

			//TODO : revoir la transaction en utilisant rollback pour autoriser les validations intermediaires

			//Get infos about target deposit dir
			$target_container = new container($this->space, $target_container_id);
			$target_containerInfos = $target_container->GetInfos(NULL,array($this->CONT_FIELDS_MAP_NUM , $this->CONT_FIELDS_MAP_ID , 'default_file_path', 'file_only'));

			//Check if the target container is a document manager. If not, exit.
			if($target_containerInfos['file_only']){
				$this->error_stack->push(ERROR_DB, 'Fatal', array('debug'=>array($target_containerInfos)), 'the target directory is a file manager type');
				return false;
			}

			$this->initObservers();
			//Notify event to observers
			$this->notify_all('doc_pre_copy', $this);

			$target_deposit_dir = $target_container->GetProperty('default_file_path');

			//Current user and date
			$now = time();
			//Create new doc from original doc
			$t_docInfos=$this->GetDocumentInfos(); //Get the properties of the current document
			$t_docInfos['document_number'] = $target_name;
			$t_docInfos[$this->CONT_FIELDS_MAP_ID] = $target_container_id;
			$t_docInfos['document_indice_id'] = $target_indice;
			$t_docInfos['open_by'] = $this->CurrentUserId;
			$t_docInfos['open_date'] = $now;
			$t_docInfos['from_document_id'] = $document_id;
			unset($t_docInfos['document_id']);
			unset($t_docInfos['document_state']);
			unset($t_docInfos['document_access_code']);
			unset($t_docInfos['document_version']);
			unset($t_docInfos['previous_indice']);
			unset($t_docInfos['instance_id']);
			unset($t_docInfos['check_out_by']);
			unset($t_docInfos['check_out_date']);
			unset($t_docInfos['update_by']);
			unset($t_docInfos['update_date']);

			$t_odoc = new document($this->space); //Create a new document object
			$t_document_id = $t_odoc->CreateDocument($t_docInfos);
			if(!$t_document_id){
				$this->error_stack->push(ERROR, 'Fatal', array('element'=>$t_docInfos['document_number'] , 'debug'=>array($t_docInfos)), 'cant create the document');
				return false;}

				//Move files from ori deposit_dir to target deposit_dir and create records in database
				$assoc_files =& $this->InitDocfiles(); //Get the docfiles of the current document
				if(is_array($assoc_files)){
					foreach($assoc_files as $docfile){
						$t_file_root_name = str_replace($this->GetDocProperty('document_number'), $target_name , $docfile->GetProperty('file_root_name'));
						$t_file_name = $t_file_root_name . $docfile->GetProperty('file_extension');
						$dstfile = $target_deposit_dir.'/'. $t_file_name;
						if(!$docfile->Copy($dstfile,$t_document_id,false)){
							$this->error_stack->push(ERROR, 'Fatal', array('element'=>$srcfile, 'debug'=>array($srcfile , $dstfile)), 'cant copy file %element%');
							continue;
						}
					}
				}

				require_once ('./class/common/attachment.php');
				//Copy visu file
				$attach = new visu($this);
				$dst = $attach->getProperty('file_path').'/'.$t_document_id.$attach->getProperty('file_extension');
				if(is_file($attach->getProperty('file')))
				$attach->copy($dst,0755,true);
				//Copy picture file
				$attach = new picture($this);
				$dst = $attach->getProperty('file_path').'/'.$t_document_id.$attach->getProperty('file_extension');
				if(is_file($attach->getProperty('file')))
				$attach->copy($dst,0755,true);
				//Copy thumbnail file
				$attach = new thumbnail($this);
				$dst = $attach->getProperty('file_path').'/'.$t_document_id.$attach->getProperty('file_extension');
				if(is_file($attach->getProperty('file')))
				$attach->copy($dst,0755,true);
				//TODO : Copy tree file

				//Copy the doclinks
				require_once('./class/common/doclink.php');
				$doclink = new doclink($this);
				if(!$doclink->CopySons($t_document_id)){
					$this->error_stack->push(ERROR, 'Fatal', array('debug'=>array($this->document_id)), 'cant copy DocLinks');
				}

				//Copy the relationship
				/*
				 require_once('./class/productStructure/relationship.php');
				 $docrel = new relationship($this);
				 if(!$docrel->CopySons($t_document_id)){
				 $this->error_stack->push(ERROR, 'Fatal', array('debug'=>array($this->document_id)), 'cant copy document sons');
				 }
				 */

				//Recalculate the doctype of the new document
				//$doctype = $this->SetType( $t_fileInfos , $Ddata['container_id']);
				//if (!$doctype) return false;
				//else $Ddata['doctype_id'] = $doctype['doctype_id'];

				//Notify event to observers
				$this->notify_all('doc_post_copy', $this);

				//Write history
				$this->history['action_name'] = 'Copy';
				$this->WriteHistory();

	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 This method can be used to remove a document from a container
	 Return true or false.
	 *
	 * This method suppress the record of the document and associated files
	 * and suppress files and version files from the container directory.
	 * If a history indice exist, suppress the current indice and restore the previous indice.
	 \param $document_id(integer), id of the document.
	 */
	function RemoveDocument(){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;
		}
		//$this->history = $this->GetDocumentInfos();//Get infos about suppressed document
		$this->history = $this->initProperties(); //Get infos about suppressed document, init the properties

		//check if access is free
		$access = $this->CheckAccess();
		if(!($access==0 || $access==12)){
			$this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->GetDocProperty('document_number'),'element1'=>$access), 'Document %element% is locked with code %element1%, suppress is not permit');
			return false;
		}

		//Get the previous indice document_id
		$query = 'SELECT document_id, document_indice_id  FROM '.$this->OBJECT_TABLE.' WHERE document_number = \''.$this->GetDocProperty('document_number').'\'';
		if($rset = $this->dbranchbe->Execute($query)){
			$i = 0;
			while ($row = $rset->FetchRow()){
				if(($i < $row['document_indice_id']) && ( $row['document_indice_id'] < $this->GetDocProperty('document_indice_id')))
				$prevIndice = array('indice'=>$row['document_indice_id'], 'document_id'=>$row['document_id']);
				$i = $row['document_indice_id'];
			}
		}else
		$this->error_stack->push(ERROR_DB, 'Info', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());

		//get list of associated files and check access
		if($this->InitDocfiles() === false)
		$this->error_stack->push(ERROR, 'Info', array('element'=>$this->GetDocProperty('document_number'), 'debug'=>array()), 'no associated files to %element%');

		//TODO: Delete record of all process instance
		//TODO: Delete or archive history

		//Delete files of current version and old versions
		if(is_array($this->docfiles))
		foreach($this->docfiles as $adocfile){
			if(!$adocfile->RemoveFile(true, true))
			$this->error_stack->push(ERROR_DB,'Fatal',array('element'=>$adocfile->file),'failed to remove file %element%');
		}

		//Start transaction
		$this->dbranchbe->StartTrans(); //Start a transaction

		require_once ('./class/common/attachment.php');
		//remove visu file
		$attach = new visu($this);
		if(is_file($attach->getProperty('file')))
		$attach->putInTrash();
		//remove picture file
		$attach = new picture($this);
		if(is_file($attach->getProperty('file')))
		$attach->putInTrash();
		//remove thumbnail file
		$attach = new thumbnail($this);
		if(is_file($attach->getProperty('file')))
		$attach->putInTrash();
		//TODO : remove tree file

		//Delete record of document
		if(!$this->BasicSuppress($this->document_id)){
			$this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), 'Suppress of the document record data is failed');
			return $this->dbranchbe->CompleteTrans(false);
		}else{
			$this->dbranchbe->CompleteTrans(true);
			$this->error_stack->push(LOG, 'Info', array('document'=>$this->GetDocProperty('document_normalized_name') ), 'Document %document% has been removed');
		}

		//Restore file of previous indice
		if(!is_null($prevIndice['document_id'])){
			$prevDoc = new document($this->space, $prevIndice['document_id']);
			$prevDocContainer =& $prevDoc->initContainer(); //Get the container object of previous document
			if($prevDoc->InitDocfiles() === false){
				$this->error_stack->push(LOG, 'Info', array('element'=>$prevDoc->GetDocProperty('document_number')), 'no associated files to %element%');}
				if(is_array($prevDoc->docfiles))
				foreach($prevDoc->docfiles as $docfile){
					$docfile->MoveFile($prevDocContainer, false); //Move file from directory __indices/[indice_id]/ to default_deposit_dir of container
					$docfile->LockFile(11);
				}
				//Update access code on previous document
				$prevDoc->LockDocument(11);

				//Write a history for the previous indice file
				$prevDoc->history = $prevIndice;
				$prevDoc->history['action_name'] = 'BackIndice';
				$prevDoc->WriteHistory();
		} //End of previous indice exist condition

		if($this->dbranchbe->CompleteTrans() === false){
			$this->error_stack->push(ERROR_DB, 'Fatal', array(), 'Error is occured'.$this->dbranchbe->ErrorMsg());
			return false;
		}

		//Special history writing for remove case for get all data about document before suppress it
		$this->history['action_name'] = 'RemoveDocument';
		$ohistory = new history($this);
		$ohistory->write($this->history);

		return true;
	}//End of method

	//---------------------------------------------------------------------------
	/*
	 function ChangeDocumentState($document_id , $project_id){
	 require_once './class/projectsManager.php';
	 $projectsManager = new projectsManager;
	 //Get the father project_id
	 $object_id = $projectsManager->GetObjectId( $project_id, 'project' );
	 //Get the process link to father
	 $Objectlist = $projectsManager->GetLinkChildsDetail($object_id , array( 'object_type' => 'process' ));
	 if ( (count ($Objectlist)) > 1 ) {print 'FATAL ERROR : more than one process defined'; die;}
	 $link_process_id = $Objectlist[0]['natif_id'];
	 }//End of method
	 */
	//---------------------------------------------------------------------------

	/*!\brief
	 Link a instance of a process to the document
	 Return true or false.
	 *
	 \param $document_id(integer), id of the document.
	 */
	//  function LinkInstanceToDocument( $document_id , $instance_id) {
	function LinkInstanceToDocument($instance_id){
		/*!
		 This method can be used to change state of process link to document
		 Return FALSE or TRUE
		 */

		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;}
			$document_id =& $this->document_id;

			$this->SetFileTable();

			$data['instance_id'] = $instance_id;

			//Create entries in table
			if (!$this->dbranchbe->AutoExecute("$this->OBJECT_TABLE" , $data , 'UPDATE' , "$this->FIELDS_MAP_ID = $document_id")) {
				$this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>'UPDATE' . implode(';',$data) , 'debug'=>array($data, $this->OBJECT_TABLE, $document_id)), $this->dbranchbe->ErrorMsg() );
				return false;
			}else return true;

	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 Unlink a instance of a process from the document
	 Return true or false.
	 *
	 * Use when instance is terminate. If a terminate instance is linked to a document
	 * is not possible to create and link a new instance to this document
	 \param $document_id(integer), id of the document.
	 */
	//  function UnLinkInstanceToDocument($document_id){
	function UnLinkInstanceToDocument(){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;}
			$query = 'UPDATE '.$this->OBJECT_TABLE.' SET instance_id = NULL WHERE '.$this->FIELDS_MAP_ID.' = '.$this->document_id;
			//Modify database
			if(!$this->dbranchbe->Execute($query)){
				$this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg() );
				return false;}
				return true;
	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 Get the scripts associate to the doctype of the document
	 Return array("script type"=>"path to scripts") or false.
	 *
	 */
	function GetScripts(){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;}

			global $doctypesManager; //TODO : revoir �a!!???
			//Create a manager for doctypes
			if(empty($doctypesManager)){
				require_once './class/doctypesManager.php'; //Class
				$doctypesManager = new doctypesManager; //Create new manager
			}

			//Get the document infos
			$DocInfos = $this->GetDocumentInfos();

			//Get the doctype infos
			$Doctype = $doctypesManager->GetInfos($DocInfos['doctype_id']);

			foreach ( array( 'script_pre_update' , 'script_post_update' , 'script_pre_store' , 'script_post_store' ) as $type )
			{
				if (!empty ( $doctype["$type"]) ){
					// Get paths for script
					$script = 'scripts/' . $doctype["$type"] ;

					//check that script file exist
					if ( is_file ( $script ) ) $scripts["$type"] = $script;
				}
			}
			return $scripts;
	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 Upgrade the indice of the document
	 Return false or the new document_id
	 *
	 * This method check that access_code is >5 and < 15 befor upgrade indice and
	 * check that the new indice is greater than the old indice. When a new indice is
	 * create, a copy of the document record is create in database and the old file is keep in the indice subdirectory
	 * of the container directory. Then this file is record in file_table and associate to the old document record.
	 \param $indice_id(integer), id of the new indice. If Null upgrade current indice of 1.
	 \param $container_id(integer), id of the new container. If Null create new doc in current container
	 */
	function UpgradeIndice($indice_id=NULL, $container_id=NULL){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;}

			$this->dbranchbe->StartTrans(); //Start a transaction

			//Get info on document
			$infos = $this->GetdocumentInfos();

			//Check if the update indice is permit
			$access = $this->CheckAccess();
			if( !($access > 9 && $access < 12) ){
				$this->error_stack->push(ERROR, 'Fatal', array('element'=>$infos['document_number'], 'debug'=>array($indice_id , $infos['document_indice_id'])), tra('You cant change indice of %element%: is not locked'));
				return $this->dbranchbe->CompleteTrans(false);}

				//If none indice in parameter upgrade current indice of 1.
				if(is_null($indice_id)){
					$indice_id = $infos['document_indice_id'] + 1;
				}

				//Check if the new indice is greater than old
				if ($indice_id <= $infos['document_indice_id']){
					$this->error_stack->push(ERROR, 'Fatal', array('element'=>$indice_id, 'debug'=>array($indice_id , $infos['document_indice_id'])), tra('this indice is invalid'));
					return $this->dbranchbe->CompleteTrans(false);}

					$this->initObservers();
					//Notify event to observers
					$this->notify_all('doc_pre_upgradeIndice', $this);

					//Create a new document from the prev indice. This new document is for the new indice
					//The current document object is the old indice
					$odocNew = new document($this->space);
					$data = $infos; //Get prev indice datas
					$data['document_indice_id'] = $indice_id;
					//$data['previous_indice_id'] = $infos['document_indice_id'];
					$data['from_document_id'] = $this->document_id;
					$data['document_access_code'] = 0;
					if( !is_null($container_id) && ( $container_id != $this->GetDocProperty('container_id') ) ){
						$data[$this->FIELDS_MAP_FATHER] = $container_id;
						$new_container =& $odocNew->initContainer($container_id);
					}
					if(!$new_document_id = $odocNew->CreateDocument($data)){
						$this->error_stack->push(ERROR, 'Fatal', array('element'=>$data['document_number']), 'unable to create document %element%');
						return $this->dbranchbe->CompleteTrans(false);
					}
					
					//Lock the previous indice for prevent change indice on
					if(!$this->LockDocument(15)){
						$this->error_stack->push(ERROR, 'Fatal', array('debug'=>array($this->document_id)), 'cant lock the document');
						return $this->dbranchbe->CompleteTrans(false);
					}
					
					if($this->dbranchbe->CompleteTrans() === false){
						$this->error_stack->push(ERROR, 'Fatal', array(), 'cant update indice');
						return false;
					}

					//Get associated files
					if($associatedFiles =& $this->InitDocfiles()){ //its the docfiles of the old indice
						unset($this->docfiles); //Reinit the docfiles and to redefine they
						foreach($associatedFiles as $docfile){
							//Keep docfile of the previous document
							$keepdocfile = $docfile->KeepIndice($infos['document_indice_id'], $new_document_id, $new_container );
							if($keepdocfile === false){
								$this->error_stack->push(ERROR, 'Error', array('file'=>$docfile->GetProperty('file') ), 'Creation of version file from file %file% failed');
								//return $this->dbranchbe->CompleteTrans(false); //Continue: if its the second file and stop operation on error, database and files are no more synchronized
							}
						} //End of foreach
					}

					//$odocNew->LockDocument(0);

					
					//$this->dbranchbe->StartTrans(); //Start a transaction
					
					//Lock the doclinks
					require_once('./class/common/doclink.php');
					$doclink = new doclink($this);
					if(!$doclink->LockAll(15)){
						$this->error_stack->push(ERROR, 'Warning', array(), 'cant lock DocLinks');
					}
					//Copy the doclinks to new document
					if(!$doclink->CopySons($new_document_id)){
						$this->error_stack->push(ERROR, 'Warning', array('debug'=>array($this->document_id)), 'cant copy DocLinks');
					}
					//Update the fathers document linked to previous indice
					if(!$doclink->ReplaceDocLink($this->document_id , $new_document_id)){
						$this->error_stack->push(ERROR, 'Warning', array(), 'cant update DocLinks');
					}
					
					//Copy the relationship
					/*
					 require_once('./class/productStructure/relationship.php');
					 $docrel = new relationship($this);
					 if(!$docrel->CopySons($new_document_id)){
						 $this->error_stack->push(ERROR, 'Fatal', array('debug'=>array($this->document_id)), 'cant copy document sons');
					 }
					 */
					
					/*
					if($this->dbranchbe->CompleteTrans() === false){
						$this->error_stack->push(ERROR, 'Fatal', array(), 'cant update indice');
						return false;
					}
					*/
					
					require_once ('./class/common/attachment.php');
					
					//Copy visu file
					$attach = new visu($this); //Create a new attachment object
					if(is_file($attach->getProperty('file'))){
						$dst = $attach->getProperty('file_path').'/'.$new_document_id.$attach->getProperty('file_extension'); //Set path of new attachment file
						$attach->copy($dst,0755,true); //Copy current attachment file to new path
					}
					//Copy picture file
					$attach = new picture($this);
					if(is_file($attach->getProperty('file'))){
						$dst = $attach->getProperty('file_path').'/'.$new_document_id.$attach->getProperty('file_extension');
						$attach->copy($dst,0755,true);
					}
					//Copy thumbnail file
					$attach = new thumbnail($this);
					if(is_file($attach->getProperty('file'))){
						$dst = $attach->getProperty('file_path').'/'.$new_document_id.$attach->getProperty('file_extension');
						$attach->copy($dst,0755,true);
					}
					//TODO : Copy tree file
					
					//Notify event to observers
					$this->notify_all('doc_post_upgradeIndice', $this);
					
					//Write history
					$this->history['action_name'] = 'UpgradeIndice';
					$this->history['document_indice_id'] = $indice_id;
					$this->WriteHistory();
					$odocNew->history['action_name'] = 'CreateNewIndice';
					$odocNew->history['from_document_id'] = $this->document_id;
					$odocNew->WriteHistory();
					
					return $new_document_id;
	}//End of method

	//-------------------------------------------------------
	/*! \brief View the document file.
	 *
	 * This method send the content of the $file to the client browser.
	 * It send too the mimeType to help the browser to choose the right application to use for open the file.
	 * If the mimetype = no_read, display a error.
	 * Return true or false.
	 */
	function ViewDocument(){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;}
			//Notify event to observers
			$this->initObservers();
			$this->notify_all('doc_pre_view', $this);
			require_once('./class/common/attachment.php');
			$viewer = new viewer();
			$viewer->init($this);
			if(!$viewer->initVisuFile()){ //Try to get the visualisation file
				if(!$viewer->initPictureFile()){ //Try to get the picture file
					//else get main file
					if(!isset($this->docfiles))
					if(!$this->InitDocfiles()){
						$this->error_stack->push(ERROR, 'Warning', array('debug'=>$files), 'There is no associated file to display');
						return false;}
						if(!$fsdata =& $this->docfiles[0]->initFsdata()){
							$this->error_stack->push(ERROR, 'Warning', array('debug'=>$files), 'There is no associated file to display');
							return false;}
							if(!$viewer->initFsdata($fsdata)){
								$this->error_stack->push(ERROR, 'Warning', array('debug'=>$files), 'There is no associated file to display');
								return false;}
				}
			}
			return $viewer->pushfile();
	}//End of method

	//-------------------------------------------------------
	/*!\brief
	 Copy all associated files of the document to directory $target_dir
	 Return true or false
	 *
	 \param $target_dir(string), path to directory where to copy files.
	 \param $replace(bool) true if you want replace the file in the target directory.
	 */
	function CopyAssociatedFiles($target_dir , $replace=false, $mode=0755){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;
		}
		//get the linked docfiles
		if(!$this->InitDocfiles()){
			$this->error_stack->push(ERROR, 'Info', array('element'=>$document_id), 'unable to list associated files');
			return false;
		}
		foreach($this->docfiles as $docfile){
			$docfile->CopyFile($target_dir, $replace, $mode);
		}
	}//End of method

	//----------------------------------------------------------
	/*!\brief
	 * Get all attachments file of the document
	 * Return a array with type=>filepath if success else false
	 *
	 \param $document_id(integer) Primary key of document.
	 */
	function GetAttachments(){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;}

			require_once ('./class/common/attachment.php');

			//Visu file
			$attach = new visu($this);
			if(is_file($attach->getProperty('file')))
			$attachment['visu_file'] = $attach->getProperty('file');

			//Picture file
			$attach = new picture($this);
			if(is_file($attach->getProperty('file')))
			$attachment['picture_file'] = $attach->getProperty('file');

			//Thumbnail file
			$attach = new thumbnail($this);
			if(is_file($attach->getProperty('file')))
			$attachment['thumbnail_file'] = $attach->getProperty('file');

			//TODO : Tree file

			return $attachment;

	}//End of method

	//-------------------------------------------------------
	/*!\brief
	 Copy associated files to the wildspace to consult it.
	 Return true or false
	 *
	 \param $replace(bool) true if you want replace the file in the target directory.
	 \param $params is use for manage the get of associated files. See parameters function of GetQueryOptions()
	 \param $auto false if you want get only the files manually generated. true for get all associated files.
	 */
	function PutInWildspace($params=array()){
		if($this->InitDocfiles($params))
		foreach($this->docfiles as $docfile)
		$docfile->PutFileInWildspace();
	}//End of method

	//-------------------------------------------------------------------------
	/*!\brief
	 * Try to get the default process linked to document else process linked to container and else the process of the context
	 * Return the process_id else return false
	 *
	 */
	function GetProcessId(){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;}
			if(!isset($this->container)){
				$this->error_stack->push(ERROR, 'Error', array(), '$this->container is not set');
				return false;}

				//Check if there are a default process linked to document
				$pid = $this->GetDocProperty('default_process_id');
				if(!empty($pid)) return $pid;

				//Check if there are a default process linked to container/doctype
				$pid = $this->container->GetDoctypesProcess($this->GetDocProperty('doctype_id'), true);
				if(!empty($pid)) return $pid;

				//Check if there are a default process linked to container
				$pid = $this->container->GetProperty('default_process_id');
				if(!empty($pid)) return $pid;

				//Check if there are a default process defined in context
				if(!empty($_SESSION['links']['process'][0]['pId']))
				return $_SESSION['links']['process'][0]['pId'];
				return false;
	}//End of method

	//----------------------------------------------------------
	/*!\brief
	 * Get all metadatas linked to container
	 * Return a array with properties of the metadatas
	 */
	function GetMetadata($params=array()){
		if(!isset($this->document_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->document_id is not set');
			return false;}
			$container_id =& $this->GetProperty('container_id');
			require_once('./class/common/metadata.php');
			$metadata = new docmetadata($this->space);
			return $metadata->GetMetadataLinked(NULL,$container_id);
	}//End of method

}//End of class

//Dont let empty lines after the close tag... ?>
