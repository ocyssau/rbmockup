<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('./class/common/recordfile.php');

/*! \brief Its a record in database of a file linked to the document.
 *
 */
class docfile extends recordfile{

	protected $document; //Object document, father document object linked to the docfile

	/*! \brief Constructor.
	 */
	function __construct(space &$space , $file_id = NULL, $manage_sub = false){

		$this->space =& $space;
		$this->error_stack =& $this->space->error_stack;

		global $dbranchbe;
		$this->dbranchbe =& $dbranchbe;

		global $logger;
		$this->logger =& $logger;

		global $usr;
		$this->usr =& $usr;

		$this->CurrentUserId   = $this->usr->getProperty('auth_user_id');

		$this->MANAGE_SUB = $manage_sub; //if you want manage subdirectories

		$this->OBJECT_TABLE  = $this->space->DOC_FILE_TABLE;
		$this->HISTORY_TABLE = $this->space->DOC_HISTORY_TABLE;
		$this->INDICE_TABLE  = $this->space->DOC_INDICE_TABLE;
		//$this->METADATA_TABLE  = $this->space->DOC_METADATA_TABLE;

		$this->FIELDS_MAP_ID = 'file_id';
		$this->FIELDS_MAP_NUM = 'file_name';
		$this->FIELDS_MAP_DESC = 'file_name';
		$this->FIELDS_MAP_STATE = 'file_state';
		$this->FIELDS_MAP_INDICE = 'file_version';
		$this->FIELDS_MAP_FATHER = $this->space->DOC_FIELDS_MAP_ID;

		$this->FILE_TABLE = $this->OBJECT_TABLE;

		if(!is_null($file_id))
		$this->init($file_id);

		$this->initObservers();

	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * Get the document object and link it to the current docfile
	 * return a document instance if success, else fase.
	 * Set object $this->document
	 * The method init() must be call before.
	 * Example :
	 */
	function initDoc($document_id=NULL){
		if(is_null($document_id) && isset($this->document))
		return $this->document;
		require_once('./class/common/document.php');
		if(is_null($document_id)){
			if($document_id = $this->GetProperty('document_id')){
				$this->document = new document($this->space, $document_id);
			}else{
				$this->document = new document($this->space);
			}
		}else{
			$this->document = new document($this->space, $document_id);
		}
		$this->document->SetDocfile($this);
		return $this->document;
	}//End of method

	//----------------------------------------------------------
	/**
	 * Set the document linked to the current docfile
	 */
	function SetDocument(document &$document){
		$this->document =& $document;
		return true;
	} //End of method

	//-------------------------------------------------------
	/*! \brief CheckOut a file.
	 Return true if success, else return false.
	 *
	 * When a file is checkOut, is copied in the user wildspace and is locked it for prevent a second checkout until the checkin is submit.
	 * If the file exist in wildspace, a error is return.

	 \param $GetFile(bool) if true, files are copy in the wildspace
	 */
	function CheckOutFile($GetFile = true){
		$this->error_stack->push(LOG, 'Info', array(), 'checkout file: '.$this->GetProperty('file').' id: '.$this->file_id);
		$this->dbranchbe->StartTrans(); //Start a smart transaction
		if($this->CheckFileAccess()){
			$this->error_stack->push(ERROR, 'Fatal', array(), 'this file ID= '.$this->file_id.' is locked, checkOut is not permit');
			$this->dbranchbe->CompleteTrans(false);
			return false;
		}

		//Get infos about file
		if(!$fileInfo = $this->GetInfos()){
			$this->error_stack->push(ERROR, 'Fatal', array(), 'cant get infos on file');
			$this->dbranchbe->CompleteTrans(false);
			return false;
		}

		//Notiy observers on event
		$this->notify_all('docfile_pre_checkout', $this);
		if( $this->stopIt ) { //use by observers to control execution
			$this->dbranchbe->CompleteTrans(false);
			return false;
		}

		//lock the file
		if(!$this->LockFile(1)){
			$this->error_stack->push(ERROR, 'Fatal', array(), 'cant lock file');
			$this->dbranchbe->CompleteTrans(false);
			return false;
		}

		if($GetFile){
			//copy file from deposit_dir to wildspace
			$this->initFsdata();
			$this->initWildspace();
			$dstfile = $this->wildspace->GetPath().'/'.$fileInfo['file_name'];
			//TODO : Add a confirmation for replace it
			if(!$this->fsdata->copy($dstfile , 0775, false)){
				$this->error_stack->push(ERROR, 'Fatal', array('element1'=>$this->GetProperty('file'),'element2'=>$dstfile),
       'can\'t copy file <em>%element1%</em> to <em>%element2%</em>');
				$this->dbranchbe->CompleteTrans(false);
				return false;
			}
		}//End of if GetFile

		require_once('./class/checkoutIndex.php');
		$checkoutIndex = new checkoutIndex($this);
		if(!$checkoutIndex->IndexCheckOutAdd()){
			$this->error_stack->push(ERROR, 'Warning', array(), 'cant index file '.$fileInfo['file_name']);
			return false;
		}

		//Notiy observers on event
		$this->notify_all('docfile_post_checkout', $this);

		return $this->dbranchbe->CompleteTrans();

	}//End of method

	//-------------------------------------------------------
	/*! \brief Cancel the checkOut of the file.
	 Return true if success, else return false.
	 *
	 * This method cancel a checkOut without copy file from wildspace to vault.

	 \param $file_id(integer) file id of the file.
	 */
	function ResetFile(){
		return $this->CheckInFile(false, true, true);
	}//End of method

	//-------------------------------------------------------
	/*! \brief Replace a file checkout in the vault and unlock it.
	 Return true if success, else return false.
	 *
	 * The checkIn copy file from the wildspace to vault deposit dir and suppress it from the wildspace. If the file has been changed(check by md5 code comparaison), the file of the vault is keep in
	 the "version"/"version number" directory before copy of the new file. The number of file version to keep is set by the DEFAULT_BACK_VERSIONS_NUM constante(see ranchbe_setup.php).
	 * Then unlock the file access.

	 \param $update(bool) If true, update the file of the vault but let file in checkout state.
	 \param $reset(bool) If true, cancel a checkOut without update file of the vault.
	 \param $checkAccess(bool) If true, the file must be in state checked out for check in it.
	 */
	function CheckInFile($update = false , $reset = false , $checkAccess = true){
		$this->logger->log('checkin file: '.$this->GetProperty('file').' id: '.$this->file_id);
		//check if access of file is free
		$access = true;
		if($checkAccess)
		$access = $this->CheckFileUpdateRight();
		if($reset) $access = true;

		if(!$access){
			$this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->file_id, 'debug'=>array($this->file_id)), 'checkIn is not permit on this file %element%');
			return false;
		}

		//Notiy observers on event
		if($update)
		$this->notify_all('docfile_pre_update', $this);
		else if($reset)
		$this->notify_all('docfile_pre_reset', $this);
		else
		$this->notify_all('docfile_pre_checkin', $this);

		if( $this->stopIt ) { //use by observers to control execution
			return false;
		}

		$this->dbranchbe->StartTrans(); //Start a smart transaction

		if(!$update){
			//update file informations
			$data['file_checkout_by']   = NULL;
			$data['file_checkout_date'] = NULL;
		}

		if(!$reset){
			$data['file_update_by']   = $this->CurrentUserId;
			$data['file_update_date'] = time();
		}

		if(!$this->UpdateRecordFile($data))
		return $this->dbranchbe->CompleteTrans(false);

		//Get infos about file
		//$fileInfo = $this->GetInfos();

		if(!$reset){
			//Current fsdata of file in vault : $this->fsdata
			if($this->initFsdata()){
				$this->initWildspace();
				//fsdata of file in wildspace
				$srcfile = $this->wildspace->GetPath().'/'.$this->GetProperty('file_name'); //The source is element in user wildspace
				if(!$osrcfile = fsdata::_dataFactory( $srcfile ) ){
					$this->error_stack->push(ERROR, 'Fatal', array('element'=>$srcfile), 'Check that file %element% exists');
					return $this->dbranchbe->CompleteTrans(false);
				}

				//Check if file has been changed by md5 code comparaison
				if($this->GetProperty('file_md5') !== $osrcfile->getProperty('file_md5')){ //if md5 are not equal, thats sure, file is changed
					$this->error_stack->push(LOG, 'Info', array(), 'file_md5 are not equals');
					$currentVersion = $this->GetProperty('file_version'); //version of file on reposit directory
					//Create a new version
					require_once('./class/common/recordfileVersion.php');
					$Version = new recordfileVersion($this->space); //Create the recordfile version object
					if( !$Version->KeepVersion($this) ){ //Create a copy of the current file in reposit on version archive directory
						return $this->dbranchbe->CompleteTrans(false); //Add 0.4.6
					}

					/*
					 $this->initFsdataVersion( $currentVersion ); //false if no data(file or directory) on filesystem. Init $this->versions[].
					 if( !$this->versions[$currentVersion]->KeepVersion() ){ //Create a copy of the current file in reposit on version archive directory
					 return $this->dbranchbe->CompleteTrans(false); //Add 0.4.6
					 }
					 */

					//Remove obsolete version
					$obsVerId  = ($this->GetProperty('file_version') - DEFAULT_BACK_VERSIONS_NUM);
					if($obsVerId > 0){
						$obsoleteVersion = new recordfileVersion($this->space); //Create the recordfile version object
						if( $obsoleteVersion->initVersion($this->file_id, $obsVerId) ){ //false if not on filesystem
							$obsoleteVersion->RemoveFile(true, false);
						}
					}

					/*
					 $obsVer  = ($this->GetProperty('file_version') - DEFAULT_BACK_VERSIONS_NUM);
					 if($obsVer > 0){
					 if($obsFile = $this->initFsdataVersion($obsVer)){ //false if not on filesystem
					 $obsFile->putInTrash();
					 }
					 }
					 */

					//If file has been change copy part to vault and increase version
					if(!$osrcfile->copy($this->fsdata->getProperty('file') , 0755, true)){
						$this->error_stack->push(ERROR, 'Fatal', array('element'=>$srcfile, 'debug'=>array()), 'can\'t copy file %element%');
						return $this->dbranchbe->CompleteTrans(false); //Add 0.4.6
					}
					$this->file_props['file_version'] = $this->file_props['file_version'] + 1;
					$data['file_version']  = $this->file_props['file_version'];
					$data['file_size']     = $this->file_props['file_size'] = $osrcfile->getProperty('file_size');
					$data['file_mtime']    = $this->file_props['file_mtime'] = $osrcfile->getProperty('file_mtime');
					$data['file_md5']      = $this->file_props['file_md5'] = $osrcfile->getProperty('file_md5');
					$this->UpdateRecordFile($data , $this->file_id);
				}else{ //End of if md5 different
					$this->logger->log('file_md5 are equals');
					if(!$osrcfile->copy($this->fsdata->getProperty('file') , 0755, true)){ //Just copy part
						$this->error_stack->push(ERROR, 'Fatal', array('element'=>$srcfile, 'debug'=>array($srcfile , $dstfile)), 'can\'t copy file %element%');
						return $this->dbranchbe->CompleteTrans(false); //Add 0.4.6
					}
				}

				if(!$update){
					//Suppress file from WildSpace
					if(!$osrcfile->putInTrash())
					$this->error_stack->push(ERROR, 'Info', array('element'=>$srcfile), 'File %element% has not been suppressed');
				}
			}else{ //End of if not file system datas
				$this->error_stack->push(ERROR, 'Fatal', array('element'=>$srcfile), 'File %element% is not in wildspace');
			}
		} //End of not reset

		if(!$update){
			//Unlock the file access on file
			if(!$this->UnLockFile()){
				$this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->GetProperty('file_name')), 'cant unlock file %element%');
			}
			require_once('./class/checkoutIndex.php');
			$checkoutIndex = new checkoutIndex($this);
			if(!$checkoutIndex->IndexCheckOutDel()){
				$this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->GetProperty('file_name')), 'cant del index file %element%');
			}
		} //End of not update

		$this->dbranchbe->CompleteTrans(); //Add 0.4.6

		//Notiy observers on event
		if($update)
		$this->notify_all('docfile_post_update', $this);
		else if($reset)
		$this->notify_all('docfile_post_reset', $this);
		else
		$this->notify_all('docfile_post_checkin', $this);

		return true;

	}//End of method

	//-------------------------------------------------------
	/*! \brief Create a file from fsdata
	 Return false or the doctype id
	 *
	 \param $suppress(bool) True to suppress the file after store.
	 */
	function StoreFile($suppress=false){
		$this->logger->log('store file: '.$this->GetProperty('file'));
		if(!isset($this->fsdata)){
			$this->error_stack->push(LOG,'Fatal',array(),'$this->fsdata is not set');
			return false;}
			if(!isset($this->document)){
				$this->error_stack->push(LOG,'Fatal',array(),'$this->document is not set');
				return false;
			}

			//Check if file is existing in vault
			//BUG : version init to 1 when suppress/re-associate file on document indice > 1
			$current_id = $this->GetFileId($this->fsdata->getProperty('file_name'));
			if($current_id){
				$current = new docfile($this->space, $current_id);
				$this->file_props['file_version'] = $current->getProperty('file_version') + 1;
				unset($current);
			}

			$container =& $this->document->GetContainer(); //Get the container of the father document

			//Notiy observers on event
			$this->notify_all('docfile_pre_store', $this);
			if( $this->stopIt ) { //use by observers to control execution
				return false;
			}

			//Copy docfiles from wildspace to container dir.
			//A docfiles is juste one files or assy of many single files or a directory
			//in case of cadds data
			$dstdir = $this->GetRepositdirPath($container);
			if($dstdir)
			$dstfile = $dstdir.'/'.$this->fsdata->getProperty('file_name');
			else return false;

			if(!$this->fsdata->copy($dstfile, 0755, true)){
				$this->error_stack->push(ERROR, 'Fatal', array('element'=>$srcfile), 'cant copy file %element%');
				return false;
			}

			$sourceFsdata = $this->fsdata; //Keep a ref to the file in the wildspace
			$this->initFsdata($dstfile); //This docfile is now linked to the file in the deposit_dir

			//Create a new data record in database
			$this->SetFatherId($this->document->GetId());
			$docfile_id = $this->RecordFile($this->fsdata);
			if(!$docfile_id){
				$this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->file_props['file_name']), 'cant record the file %element%');
				return false;
			}

			//Suppress file from WildSpace
			if($suppress = true)
			if(!$sourceFsdata->putInTrash())
			$this->error_stack->push(INFO, 'Info', array('element'=>$sourceFsdata->file), 'File %element% has not been suppressed');

			//Notiy observers on event
			$this->notify_all('docfile_post_store', $this);

			return $docfile_id;
	}//End of method

	//-------------------------------------------------------
	/*! \brief Link a document to a file.
	 Return true or false.
	 *
	 \param $this->file_id(integer) file id of the file.
	 \param $document_id(integer) document id of the document to link.
	 */
	function LinkFileToDocument($document_id){
		$data['document_id'] = $document_id;
		return $this->UpdateRecordFile($data , $this->file_id);
	}//End of method

	//-------------------------------------------------------
	/*! \brief Copy the file and the record of a file. It use for keep the file information when you create a new indice of the document.
	 Return true or false.
	 *
	 \param $current_indice(integer) the current indice of the document
	 \param $new_father_id(integer) the new document id
	 \param $container(object) the new container object
	 */
	function KeepIndiceOld($current_indice, $new_father_id, &$new_container=NULL){
		$this->logger->log( 'Keep indice file: '.$this->GetProperty('file') );
		if( empty($current_indice) ){
			$this->error_stack->push(ERROR, 'Fatal', array(), 'empty indice');
			return false;
		}

		$filepath = $this->GetProperty('file_path');
		//Create directories if necessary
		if(!is_dir($filepath .'/__indices'))//Create the indice dir
		if(!mkdir($filepath .'/__indices')){
			$this->error_stack->push(ERROR, 'Fatal', array('element'=>$filepath .'/__indices'), 'cant create indices dir %element%');
			return false;
		}
		if(!is_dir($filepath.'/__indices/'.$current_indice)) //Create the indice dir / indice number
		if(!mkdir($filepath.'/__indices/'.$current_indice)){
			$this->error_stack->push(ERROR, 'Fatal', array('element'=>$filepath.'/__indices/'.$current_indice), 'cant create indices dir %element%');
			return false;
		}

		//Create a copy in original container of file
		$indiceFile = $filepath.'/__indices/'.$current_indice.'/'.$this->GetProperty('file_name');
		if(!$docfile = $this->Copy($indiceFile, $this->document->GetId(), false)){ //$docfile to keep files of the prev indice
			$this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->GetProperty('file') ), 'error copy part %element%');
			return false;
		}

		//Associate the current docfile to the new document and unlock the file
		$this->UpdateRecordFile( array('document_id'=>$new_father_id, 'file_access_code'=>0) );

		if( is_a($new_container, 'container') ){
			$this->MoveFile($new_container);
		}

		//Return id of the copy for previous indice
		return $docfile;

	}//End of method

	//-------------------------------------------------------
	/*! \brief Copy the file and the record of a file. It use for keep the file information when you create a new indice of the document.
	 Return docfile object or false.
	 *
	 \param $current_indice(integer) the current indice of the document
	 \param $new_father_id(integer) the new document id
	 \param $container(object) the new container object
	 *
	 * Le docfile est d�plac� danbs le dossier __indices. Son id est inchang� afin de conserver les liens avec ses versions.
	 * Un nouveau docfile est cr�� avec ses fichiers dans le new_container
	 */
	function KeepIndice($current_indice, $next_father_id, &$next_container=NULL){
		$this->logger->log( 'Keep indice from file: '.$this->GetProperty('file') );
		if( empty($current_indice) ){
			$this->error_stack->push(ERROR, 'Fatal', array(), 'empty indice');
			return false;
		}
		/*
		 *$currentPath = path to directory where is the original file of current indice
		 * ie: ./deposit_dir/AFF001
		 *$indicePath = path to directory where create a backup of current indice
		 * ie: ./deposit_dir/AFF001/__versions/1
		 *$nextPath = path to directory where create the new file for current object
		 * ie: ./deposit_dir/AFF002
		 *
		 *$currentFile file of current indice
		 * ie: ./deposit_dir/AFF001/file1.file (indice 1 release 5)
		 *$indiceFile
		 * ie: ./deposit_dir/AFF001/__versions/1/file1.file (indice 1 release 5)
		 *$nextFile
		 * ie: ./deposit_dir/AFF002/file1.file (indice 2 release 6)
		 *
		 * At end of this method $currentFile is either suppressed or replaced by $nextFile
		 *
		 */

		if(!isset($this->fsdata)) //set this now because fsdata is used by ::GetRepositdirPath()
		$this->initFsdata();

		$currentPath = $this->GetProperty('file_path');

		//Init the target directory for new docfile. To do before file modifications to prevent errors
		if( is_a($next_container, 'container') ){
			$nextPath = $this->GetRepositdirPath($next_container);
			if($nextPath){
				$nextFile = $nextPath.'/'.$this->fsdata->getProperty('file_name');
			}else{
				$this->error_stack->push(LOG, 'Error', array(), 'target directory is not set');
				return false;
			}
		}else{
			$nextPath = $currentPath;
		}

		$indicePath = $currentPath.'/__indices/'.$current_indice;
		if(!is_dir($indicePath)){ //Check if indice reposit dir is existing (ie: :/AFF001/__indices/1)
			if(!rdirectory::createdir($indicePath, 0755)){ //..and create it if not
				$this->error_stack->push(ERROR, 'Fatal', array('element'=>$indicePath), 'cant create indices dir %element%');
				return false;
			}
		}

		$this->dbranchbe->StartTrans(); //Start a transaction

		//Update database...
		$data = array();
		$data['file_path'] = $indicePath;
		$data['file_access_code'] = 15;
		if( !$this->UpdateRecordFile($data) ){ //Update file infos
			$this->dbranchbe->RollbackTrans();
			return false;
		}

		//...and move file
		$indiceFile = $indicePath.'/'.$this->GetProperty('file_name');
		if(!$this->fsdata->move($indiceFile, true)){ //Move file
			$this->error_stack->push(ERROR, 'Fatal', array('file'=>$this->file_name, 'dstfile'=>$indiceFile), 'can\'t move %file% to %dstfile%');
			$this->dbranchbe->RollbackTrans();
			return false;
		}

		$this->dbranchbe->CompleteTrans();
		unset($this->fsdata); //unset this fsdata, is no more valid since file has been moved
		$this->file_props['file_path'] = $indicePath; //Update property if validate only
		$this->error_stack->push(LOG, 'Info', array('element'=>$this->GetProperty('file_name'), 'path'=>$indicePath ), 'data %element% is moved to %path%' );

		//Now file of current docfile is in __indices directory
		//Now create a new docfile from this files in $new_container :

		//Create a copy in original container of file
		$nextFile = $nextPath.'/'.$this->GetProperty('file_name');
		if(!$next_docfile = $this->Copy($nextFile, $next_father_id, false)){
			$this->error_stack->push(ERROR, 'Fatal', array('file'=>$this->GetProperty('file')), 'error in copy of %file%');
			//return false;
			return $this;
		}
		$next_docfile->LockFile(0); //Unlock the next indice docfile
		return $next_docfile; //docfile object

	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * This method return the file id of the file number if exist.
	 * Return the id of file with the smaller access_code.
	 * If document_id is set, return a file_id of the document.
	 * else return false.
	 *
	 \param $file_name(string) number of the file
	 \param $document_id(int) id of father document of file
	 */
	function GetFileId($file_name, $document_id = 0){
		//FIXME : this request is time expensive. Optimise it.
		/*  $query =  'SELECT file_id FROM '.$this->FILE_TABLE
		 .' WHERE (file_access_code, file_name) IN'
		 .' (SELECT MIN(file_access_code), file_name'
		 .' FROM '.$this->FILE_TABLE
		 ." WHERE file_name = '".$file_name
		 ."' GROUP BY file_name)";*/

		$query =  "SELECT file_id FROM $this->FILE_TABLE
            WHERE file_name = '$file_name'
            AND file_version = 
            (SELECT MAX(file_version)
            FROM $this->FILE_TABLE
            WHERE file_name = '$file_name'
            GROUP  BY file_name)";

		if($document_id != 0){
			$query = $query.' AND document_id = '.$document_id;
		}
		$query = $query.' GROUP BY file_name'; //Seems that is faster with this close???
		return $this->dbranchbe->GetOne($query);
	}//End of method

}//End of class

?>
