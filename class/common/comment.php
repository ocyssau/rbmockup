<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

class doccomment extends basic{

/*
DROP TABLE `workitem_doccomments`;
CREATE TABLE `workitem_doccomments` (
  `comment_id` INT NOT NULL ,
  `document_id` INT NOT NULL ,
  `comment` TEXTE NOT NULL ,
  `open_by` INT NOT NULL ,
PRIMARY KEY  (`comment_id`),
) ENGINE=InnoDB;

ALTER TABLE `workitem_doccomments`
  ADD CONSTRAINT `FK_workitem_doccomments_2` FOREIGN KEY (`document_id`) REFERENCES `workitem_documents` (`document_id`);
*/

//function __construct(document &$document){
function __construct(space &$space){
  
  global $dbranchbe;
  $this->dbranchbe =& $dbranchbe;
  //$this->document =& $document;
  //$this->space =& $this->document->space;
  $this->space =& $space;
  $this->error_stack =& $this->space->error_stack;

  $this->CurrentUserName =& $this->space->CurrentUserName;
  $this->CurrentUserId   =& $this->space->CurrentUserId;
  
  $this->OBJECT_TABLE = $this->space->SPACE_NAME.'_doccomments';
  $this->FIELDS_MAP_ID = 'comment_id';
  
  $this->RIGHT_OBJECT_TABLE = $this->space->SPACE_NAME.'_documents';
  $this->RIGHT_FIELDS_MAP_ID = 'document_id';
  
}//End of method

//--------------------------------------------------------------------
/*! \brief Add a comment
* 
* \param $input(array)
*/
function AddComment( $input ){
  //Filter input
  $data['document_id'] = $input['document_id'];
  //$data['document_id'] = $document_id;
  $data['comment'] = $input['comment'];
  $data['open_by'] = $this->CurrentUserId;
  return $this->BasicCreate($data);
}//End of method

//----------------------------------------------------------
/*! \brief Update a comment
* 
  \param $propset_id(integer)
  \param $input(array)
*/
function ModifyComment( $comment_id , $input ){
  unset($input['comment_id']); //to prevent the change of id
  return $this->BasicUpdate($input , $comment_id);
}//End of method

//----------------------------------------------------------
/*! \brief Suppress a comment
* 
  \param $comment_id(integer)
*/
function SuppressComment( $comment_id ){
  return $this->BasicSuppress( $comment_id );
}//End of method

//----------------------------------------------------------
/*!\brief Get the comments
*/
function GetComment( $document_id , $params=array() ){
  $params['exact_find']['document_id'] = $document_id;
  return $this->GetAllBasic($params);
}//End of method

} //End of class

?>
