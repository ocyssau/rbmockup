<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*Events :
 onExtractBegin, //Begin the extraction of archive file
 onExtractEnd, //End of extraction
 onUpdateFile, //Update file record in database
 onCreateFile, //Create a new file in database
 */

//require_once('./class/common/basic.php');
require_once('./class/common/observable.php');

/*! \brief This class manage the import of files or documents in the containers.
 */

/*! L'importation en 3 etapes :
 1>Lister le contenu du TDP,
 Verifier l'existence du fichier xxxx.tdp (voir normes)
 Pour chaque fichier tester s'il existe dans le conteneur cible ou dans un autre conteneur de l'espace,
 Verifier que le fichier exitant est libre ou que l'on peut l'indicer(acode = 0 ou acode >= 10)
 Tester le doctype de chaque fichier,
 2>Afficher a l'utilisateur les fichiers � importer avec le choix :
 Mettre � jour ou cr�er un nouveaux indice si le fichier existe,
 Cr�er un nouveau document sinon,
 Pour les fichiers qui ne correspondent pas a un doctype, proposer de les renommer,
 Proposer chaque champs de saisie par m�tadonn�e pour saisie,
 Si le fichier existe est qu'il est en cours de modification ou dans une boucle de validation, informer utilisateur et ignorer,
 R�solution des conflits :
 Le tdp d�finis des m�tadonn�es differentes du document existant : le tdp est prioritaire
 sauf dans le cas des champs vide du tdp.
 3>Valider les choix. La mise a jour s'execute, on reverifie les documents qui ont �t� renom�s(doctype) et on repropose les choix et selection.
 Un message de succ�s est retourn�.

 Le TDP : Transfert data package

 */

class import extends rb_observable {

	public $maxcsvrow = 5000; //Max number of rows to import from a csv file
	public $IMPORT_DIR = DEFAULT_IMPORT_DIR; //Directory where are stored the package files
	public $UNPACK_IMPORT_DIR = UNPACK_IMPORT_DIR; //Directory where are put the imported files
	public $lastErrors = array();
	private $IMPORT_HISTORY_TABLE;
	protected $smarty;
	protected $dbranchbe;
	protected $Manager;
	protected $usr;
	protected $importUserName;
	protected $importUserId;
	public $unpack; //Content the list of unpack files
	public $unpack_Root; //Content the list of unpack files/directory but only from root of archive. Ignore contents of subdirectories.
	public $unpack_iterator = 0; //Count the number of files uncompress in directory
	public $imported_iterator = 0; //Count the number of files imported in database

	function __construct($SPACE_NAME = '', &$Container){
		global $smarty;
		$this->smarty =& $smarty;

		global $dbranchbe;
		$this->dbranchbe =& $dbranchbe;

		global $usr;
		$this->usr =& $usr;

		$this->Manager =& $Container;

		$this->IMPORT_HISTORY_TABLE = $this->Manager->SPACE_NAME.'_import_history';
		$this->FIELDS_MAP_ID = $Container->GetFieldName('id');

		global $error_stack;
		$this->error_stack =& $error_stack;

		if( IMPORT_USE_WILDSPACE ){
			$Wildspace = new wildspace();
			$this->IMPORT_DIR = $Wildspace->getPath() . '/imports';

			if( !is_dir($this->IMPORT_DIR) ){
				if ( !mkdir($this->IMPORT_DIR , 0777 , true) ){
					trigger_error( sprintf('can\'t create %s', $this->IMPORT_DIR), E_USER_ERROR );
				}
			}
		}
	} //End of construct

	//---------------------------------------------------------------------------
	/*! \brief Get the current user name
	 */
	function GetUserName() {
		if ( isset($this->importUserName) ) return $this->importUserName;
		return $this->importUserName = $this->usr->getProperty('handle');
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Get the current user id
	 */
	function GetUserId() {
		if ( isset($this->importUserId) ) return $this->importUserId;
		return $this->importUserId = $this->usr->getProperty('auth_user_id');
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Get package deposit in DEFAULT_IMPORT_DIR.
	 *  Return a array if no errors, else return false.
	 *
	 * \param $params(array) is not use for the moment...
	 * \return array
	 */
	function GetAllImportPackage( $params ) {
		if(!empty($params['find'])){
			foreach($params['find'] as $key=>$val){
				$find_field = $key;
				$search_word = $val;
			}
		}

		if(empty($params['sort_field'])){
			$params['sort_field'] = 'file_name';
		}

		if($params['sort_order'] == 'DESC'){
			$params['sort_order'] = SORT_DESC;
		}
		else{
			$params['sort_order'] = SORT_ASC;
		}

		$path = $this->IMPORT_DIR;

		if (!$handle  = opendir($path)){
			$this->error_stack->push(ERROR, 'Fatal', array('element'=>$path, 'debug'=>array()), 'failed to open %element%' );
			return false;
		}

		$tab = array();
		while ( $file = @readdir($handle) )
		{
			if(is_file("$path/$file")){
				//check that the file is record in database and get infos about package
				$importOrder = self::IsRecord($file);
				if ( $importOrder > 0 ){
					$tab[] = $this->GetPackageInfos( $importOrder );
				}
				else{
//echo "NOT IMPORTED FILE $file <br>"; continue;
					$tab[] = $this->RecordImportPackage("$path/$file");
				}
			}
		}
		@closedir($handle);

		//Sort files by field
		$field = array();
		if( $tab ){
			foreach ($tab as $key => $row) { //product a array from field to sort (for use array_multisort function)
				$field[$key]  = $row[$params['sort_field']];
			}

			array_multisort( $field, $params['sort_order'], $tab); //multidimensionnal sort

			//Filter data from $term
			if($search_word && $find_field){
				foreach($tab as $file){
					if(preg_match ( '/'.$search_word.'/i' , $file[$find_field] )){
						$finfos[] = $file;
					}
				}
				return $finfos;
			}
		}
		
		return $tab;
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Get the history of importation
	 *  Return a array if no errors, else return false.
	 *
	 \param $params is use for manage the display. See parameters function of GetQueryOptions()
	 */
	function GetImportHistory($params){
		return $this->Get($this->IMPORT_HISTORY_TABLE , $params);
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Check if the package file $file is recorded in the history table
	 *  Return the histo_order.
	 *
	 \param $file(string) Package file name to check
	 \param $md5(string) Package file md5 to check
	 */
	function IsRecord( $file ) {
		if (is_null ($file))
		$this->error_stack->push(ERROR, 'Fatal', array(), 'Invalid file name' );
		$query = "SELECT import_order FROM $this->IMPORT_HISTORY_TABLE WHERE package_file_name LIKE '$file'";
		return $this->dbranchbe->GetOne($query);
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Record in database the infos about a package file.
	 *  Return the infos(array).
	 *
	 \param $file(string) Package file name to record.
	 \param $data(array) File infos about the package file. This array is the result of the method GetPackageFileInfos.
	 */
	function RecordImportPackage($file , $data=false){
		if(!is_array($data)){
			$data = $this->GetPackageFileInfo( $file );
		}
		$this->dbranchbe->StartTrans();
		$data['import_order'] = $this->dbranchbe->GenID( "$this->IMPORT_HISTORY_TABLE".'_seq', 1 ); //Increment the sequence number
		//Create entries in table
		if(!$this->dbranchbe->AutoExecute( "$this->IMPORT_HISTORY_TABLE" , $data , 'INSERT' )){
			$this->error_stack->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg());
		}
		if ( $this->dbranchbe->CompleteTrans() === false ){
			return false;
		}
		return $data;
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Get infos about a package file.
	 *  Return the infos(array).
	 *
	 \param $file(string) Package file name to record.
	 */
	static function GetPackageFileInfo( $file ){
		if(!is_file($file)) return false;
		$basename = basename("$file");
		$data = array('package_file_name'     => $basename,
                'package_file_size'     => filesize("$file"),
                'package_file_mtime'    => filemtime("$file"),
                'package_file_path'     => dirname("$file"),
                'package_file_md5'      => md5_file("$file"),
                'package_file_extension'=> substr($file, strrpos($file, '.')),
		//'package_name'          => substr($basename, 0, strrpos($basename, '.') + 1),
                'state'                 => 'not imported',
		);
		return $data;
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Update the infos recorded in database about the package file set by $import_order.
	 *  Return true or false.
	 *
	 \param $data(array) Array field=>value of data to update.
	 \param $import_order(integer) Id of the package file recorded []_import_history table.
	 \param $package_file_name(string) Package file name.
	 */
	function UpdateImportPackage($data , $import_order, $package_file_name=NULL) {
		if (!is_null($import_order) && is_null($package_file_name)){
			//Update basics table entries
			if (!$this->dbranchbe->AutoExecute("$this->IMPORT_HISTORY_TABLE" , $data , 'UPDATE', "import_order = '$import_order'")){
				$this->error_stack->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg() );
				//print 'error UpdateImportPackage: '.$dbranchbe->ErrorMsg().'<BR>';
				return false;
			}
			return true;
		}

		if (is_null($import_order) && !is_null($package_file_name)){
			//Update basics table entries
			if (!$this->dbranchbe->AutoExecute("$this->IMPORT_HISTORY_TABLE" , $data , 'UPDATE', "package_file_name = '$package_file_name'")){
				$this->error_stack->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg() );
				//print 'error UpdateImportPackage: '.$dbranchbe->ErrorMsg().'<br />';
				return false;
			}
			return true;
		}
		return false;
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Recored the package descritpion in the []_import_history table.
	 *  Return true or false.
	 *
	 \param $description(string) Description of the package.
	 \param $import_order(integer) Id of the package file recorded in []_import_history table.
	 */
	function AddImportPackageDescription($description , $import_order) {
		$data['description']   = $description;
		return $this->UpdateImportPackage($data , $import_order);
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Uncompress package in $targetDir.
	 *  Return true or false.
	 *
	 \param $file(string) File path of the package file to uncompress.
	 \param $targetDir(string) Path of the dir where put files of the package file.
	 */
	function UnpackImportPackage($file , $targetDir){
		
		if(!is_dir($targetDir)) return false; //To prevent lost of data
		if(!is_file($file)) return false; //To prevent lost of data

		if(substr($file, strrpos($file, '.')) == '.Z'){
			if(exec(UNZIPCMD . " $file")){
				//TODO: suppress $file from history table
				$this->UpdateImportPackage(array('package_file_name'=>$file) , NULL, $file);
				$file = rtrim($file , '.Z');
			}else{
				$this->error_stack->push(ERROR, 'Fatal', array('element'=>$file, 'debug'=>array()), 'cant uncompress this file %element%');
				return false;
			}
		}

		$this->notify_all( 'onExtractBegin' , $this );

		if(substr($file, strrpos($file, '.')) == '.tar'){
			/*
			 define('TARCMD','C:/Program Files/GnuWin32/bin/tar');
			 $cd = getcwd(); //Remember the current directory
			 if( chdir($targetDir) ){
			 system(TARCMD.' -xvf '.$file, $retval);
			 }
			 var_dump(TARCMD.' -xvf '.$file);
			 var_dump($retval);
			 chdir($cd);
			 */

			/*
			 require_once('Archive/Tar.php');
			 $tar = new Archive_Tar($file);
			 $res_tar = $tar->extract($targetDir);
			 if( $res_tar === false ){
			 print 'extraction has failed for unknow error<br />';
			 var_dump($res_tar);die;
			 }
			 unset($tar);
			 */

			$cmd = 'tar -xvf '.$file.' -C '.$targetDir;
			exec($cmd, $output, $return_var);

			if( !$output ){
				var_dump($output);
				print 'extraction has failed for unknow error<br />';
				print 'or archive is maybe empty<br />';
				die;
			}else{
				print '<b>Last tar message: ';
				print end($output).'</b><br />';
			}

			require_once 'File/Archive.php'; //File_Archive PEAR package extension
			$result = File_Archive::read($file.'/', $targetDir.'/');

		}else{
			require_once 'File/Archive.php'; //File_Archive PEAR package extension
			//See File_Archive PEAR package:
			File_Archive::extract(
			$result =  File_Archive::read($file.'/', $targetDir.'/'),
			File_Archive::toFiles()
			);
			$result->close();  //Move back to the begining of the source
			/*while($result->next()){ //for each unpack file
			 $this->unpack[] = $result->getFilename(); //Get the name of the unpack file
			 }*/ //suppress since ranchbe0.6 replace by next code :
		}

		//return only files that are in root of archive :
		$this->unpack_iterator = 0;
		while($result->next()){ //for each unpack file
			//TODO : a refaire pour prendre en compte cadds. L'importation dans un sous repertoire de fichier est permis:
			// valide : ./reposit_dir/MAQ001/cgr/file.cgr
			// valide : ./reposit_dir/MAQ001/file.CATPart
			// les parts cadds devrons �tre vue comme une entit� au niveau du dossier cadds
			// valide : ./reposit_dir/MAQ001/caddspart
			// non valide : ./reposit_dir/MAQ001/caddspart/_pd

			$filename = $result->getFilename(); // = c:/mockup_dir/mockup/__imported/caddsPart/_pd
			//echo $filename.'<br>';
			//$_filename = explode( '/', str_replace( ltrim($targetDir, './').'/', '', $filename ) ); //replace c:/mockup_dir/mockup/__imported/ by nothing, and explode by '/'
			//var_dump($_filename);
			//$this->unpack_Root[] = $targetDir.'/'.$_filename[0]; // = c:/mockup_dir/mockup/__imported/caddsPart
			$this->unpack_Root[] = $filename;
			$this->unpack[] = $filename; //Get the name of the unpack file
			$this->unpack_iterator ++;
		}
		$this->unpack_Root = array_unique($this->unpack_Root);

		$this->notify_all( 'onExtractEnd' , $this );
		//var_dump($this->unpack_Root);

		if(PEAR::isError($result)){
			$this->error_stack->push(ERROR, 'Fatal', array(), $result->getMessage() .'<br />'. $result->getUserInfo());
			return false;
		}

		//var_dump($this->unpack_Root);die;
		return $result;
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Display the content of the package file.
	 *  Return true or false.
	 *
	 \param $file(string) File path of the package file to uncompress.
	 */
	function ListContentImportPackage($file, $getArray = true){
		if(!is_file($file)) return false; //To prevent lost of data
		require_once 'File/Archive.php'; //File_Archive PEAR package extension
		//See File_Archive PEAR package :
		$result =  File_Archive::read("$file/");
		$result->close();  //Move back to the begining of the source
		if($getArray){
			$i=0;
			while($result->next()){ //for each unpack file
				$res[$i] = $result->getFilename(); //Get the name of the unpack file
				$res[$i]['stat'] = $result->getStat();
				$i++;
			}
			return $res;
		}
		return $result;
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Get the imported file of the package file.
	 *  Return array or false.
	 *
	 * This method list the files that they were imported from the package $import_order.
	 * If a file has been imported twice, only the last package source file can be list.
	 \param $import_order(integer) Id of the package file recorded in []_import_history table.
	 \param $params(array) is use for manage the display. See parameters function of GetQueryOptions()
	 */
	function ListImportedFiles($import_order , $params=''){
		$params['exact_find']['import_order']  = $import_order;
		$recordfile =& $this->Manager->initRecordfile();
		return $recordfile->GetFiles($params);
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Import the content of the package file in the container.
	 *  Return true or false.
	 *
	 * This method list the files that they were imported from the package $import_order.
	 * If a file has been imported twice, only the last package source file can be list.
	 \param $import_order(integer) Id of the package file recorded in []_import_history table.
	 \param $container_id(integer) identifiant of the target container.
	 \param $targetType(string) ="InTempdir" if unpack in a temporary dir, ="InContainer" for unpack in deposit dir, ="CreateDocs" for import file and create the associated documents.
	 \param $lock(bool) If true, the files or the document will be locked after import.
	 \return boolean
	 */
	function ImportPackage($import_order , $container_id , $targetType , $lock=false){
		
		$this->PackInfos = $this->GetPackageInfos($import_order); //Get infos about the selected package
		
		$errors = array();
		$success = true; //false if a critical error is occuring
		$this->lastErrors = array();
		$spaceName = $this->Manager->SPACE_NAME;
		
		/*
		 //Check if the package is not in progress
		 if($this->PackInfos['state'] == 'InProgress'){
		 $this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->PackInfos['package_file_name'], 'debug'=>array()), 'this package %element% is in progress');
		 return false;
		 }
		 */

		//Change the state of the package
		$this->UpdateImportPackage(array('state'=>'InProgress') , $import_order);

		//Set TARGET directory function of selected update mode (in container or in tempDir)
		if ($targetType == 'InContainer' || $targetType == 'CreateDocs'){
			$this->target_dir = $this->Manager->GetProperty('default_file_path');
			$this->target_container_num = $this->Manager->GetName();
			$regex = $this->Manager->GetProperty('package_regex');
		}else{
			$this->target_dir = UNPACK_IMPORT_DIR;
			$container_id = 0;
		}

		if($regex){
			$ok = preg_match( "/$regex/i", $this->PackInfos['package_file_name'] );
			if( $ok == 0 ){
				$this->error_stack->push( ERROR, 'Warning', array(), 'The package file '.$this->PackInfos['package_file_name'].' dont match the define regex for this container' );
				return false;
			}
		}

		$packfile = $this->PackInfos['package_file_path'].'/'.$this->PackInfos['package_file_name'];

		if( !$this->UnpackImportPackage($packfile , $this->target_dir) ){
			$this->UpdateImportPackage(array('state'=>'error') , $import_order); //Change the state of the package
			return false;
		}

		if($targetType == 'InContainer'){
			//List of files to records in database
			$this->imported_iterator = 0;
			foreach($this->unpack_Root as $file){
				$this->current_file = $file;
				require_once('./class/common/recordfile.php');
				$recordfile = new recordfile($this->Manager->space); //Construct a recordfile object
				$fsdata =& $recordfile->initFsdata($file); //Construct the fsdata object
				if( !is_object($fsdata) ) {
					if( is_dir($file) ){
						$errors[] = "Error during importing of $file, is a directory";
					}
					else{
						$errors[] = 'Error during importing of : '.$file.' : file do not existing on filesystem';
						$success = false;
					}
					continue;
				}
				if( $file_id = $recordfile->GetFileId( $fsdata->getProperty('file_name') ) ){ //Test if file exist in this container
					$this->notify_all( 'onUpdateFile' , $this );
					$recordfile->init($file_id);//init the find recordfile
					if( $recordfile->GetProperty('file_md5') != $fsdata->getProperty('file_md5') ){ // md5 are differents
						$data['file_version'] = $recordfile->GetProperty('file_version') + 1;
						$data['import_order'] = $import_order;
						$data['file_update_date'] = time();
						$data['file_update_by'] = $this->GetUserId();
						$data['file_md5'] =  $fsdata->getProperty('file_md5');
						$data['file_size'] = $fsdata->getProperty('file_size');
						$data['file_mtime'] = $fsdata->getProperty('file_mtime');
						$data[$spaceName . '_id'] = $container_id;
						$data['file_path'] = $fsdata->getProperty('file_path');
						$recordfile->UpdateRecordFile($data); //update file, upgrade version, record source package
						$imported_log[] = 'replace '.$file.' new version : '.$data['file_version'];
					}
					else{ // md5 are equal
						$errors[] = 'The file '.$file.' is not updated : imported file is same that existing file.';
						$data[$spaceName . '_id'] = $container_id;
						$data['file_path'] = $fsdata->getProperty('file_path');
						$data['import_order'] = $import_order;
						$data['file_update_date'] = time();
						$data['file_update_by'] = $this->GetUserId();
						$recordfile->UpdateRecordFile($data);
					}
					$file = $fsdata->getProperty('file');
					chown ($file, 'caoadmin');
					chgrp ($file, 'caomeca');
					chmod ($file, 0755);
				}
				else{ //The file dont exist
					$this->notify_all( 'onCreateFile' , $this );
					$recordfile->SetImportOrder($import_order);
					$recordfile->SetFatherId($container_id);
					if(!$recordfile->RecordFile($fsdata)){ //Record the file
						$errors[] = 'Error during record of '.$file;
						$success = false;
					}
					else{
						$imported_log[] = 'create '.$file;
					}
				}
				$this->imported_iterator ++;
				unset($fsdata);
				unset($recordfile);
			}//end of foreach
		} //End of InContainer condition

		//Write the log
		$logfile = $this->CreateLog($imported_log , $import_order.'_log');
		$contentfile = $this->CreateLog($this->unpack , $import_order.'_content');
		if( count($errors) > 0 ){
			$errorfile = $this->CreateLog($errors , $import_order.'_errors');
		}
		
		$this->lastErrors = $errors;
		
		//if no error, write infos in database.
		$Packagedata['state']               = $targetType;
		$Packagedata[$this->FIELDS_MAP_ID]  = $container_id;
		$Packagedata['import_by']           = $this->usr->getProperty('auth_user_id');
		$Packagedata['import_date']         = time();
		$Packagedata['import_logfiles_file']= $logfile;
		$Packagedata['import_errors_report']= $errorfile;
		$Packagedata['import_content']= $contentfile;
		
		$this->UpdateImportPackage($Packagedata , $import_order);
		
		return $success;
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief generate a log file
	 *  Return false or log file name
	 *
	 \param $list (array)
	 \param $logName (mixte)
	 */
	function CreateLog( $list, $logName ) {
		if( !is_array($list) ) {
			return false;
		}
		$logdir = $this->Manager->GetProperty('default_file_path').'/__import_logs';
		if( !is_dir($logdir) ){
			require_once('class/common/rdirectory.php');
			rdirectory::createdir($logdir);
		}

		$file = $logdir.'/'.$logName.'.txt';

		if( $handle = fopen($file, 'w') ){
			foreach($list as $importedFile){
				fwrite($handle, $importedFile . PHP_EOL);
			}
			fclose($handle);
			return $file;
		}else{
			$this->error_stack->push(LOG, 'Error', array('file'=>$handle), 'unable to write log file %file%. Check permissions.' );
			return false;
		}

	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Import document in batch mode from a csv file.
	 *  Return true or false.
	 *
	 \param $csvfile(string) Path to csvfile where is set the documents to create.
	 \param $container_id(integer) identifiant of the target container.
	 \param $lock(bool) true for lock document after import.
	 \param $create_category(bool) true for create the category if not exist from the category_name field of csv file.
	 */
	function ImportDocuments( $csvfile , $container_id , $lock=false , $create_category=false) {
		//$currentUser = $this->usr->getProperty('auth_user_id');
		//$this->importUserName = $this->usr->getProperty('handle');

		$extractCvs = $this->ImportCsv($csvfile , $this->maxcsvrow);

		//List of files to records in database
		foreach($extractCvs as $document){
			if(empty($document['document_number']) && empty($document['file']))
			continue;
			//Create category
			if(!empty($document['category_name'])){
				//Check if category exist and get id
				$ocategorie =& $this->Manager->initCategory();
				$category_id = $ocategorie->GetCategoryId($document['category_name']);
				if(!empty($category_id)){ //The categorie exist
					$document['category_id'] = $category_id;
				}else if($create_category) { //Create the category and get the id
					$document['category_id'] = $ocategorie->Manager->CreateCategory(array('category_number'=>$document['category_name']));
				}
			}
			$document['container_id'] = $container_id;

			/*
			 //Check if the document is yet existing
			 if($document_id = $this->DocumentExist($document['document_number'])){
			 //Update the document records
			 $this->UpdateDocument($document)
			 //If file is send, update the file
			 $this->CheckOut(false);
			 $this->CheckIn();
			 }else{
			 $this->Store($document , $lock);
			 }*/

			$this->Manager->Store($document , $lock);
		}

		return true;

	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Uncompress a archive file with the utility defined by UNZIPCMD.
	 *  Return the new file name or false.
	 *
	 \param $file(string) Path to package file.
	 \param $import_order(integer) Id of the package file recorded in []_import_history table.
	 You must set only one of this param.
	 */
	function UncompressPackage($import_order=NULL , $file=NULL){
		if ( !is_null($import_order) && is_null($file) ){
			//get file path
			$PackInfos = $this->GetPackageInfos( $import_order ); //Get infos about the selected package
			$file = $PackInfos['package_file_path'].'/'.$PackInfos['package_file_name'];
		}

		system(UNZIPCMD . ' "' . $file . '"' , $exec_return );

		if ($exec_return == 0){
			$file = rtrim($file , '.Z');
		}else {
			$this->error_stack->push(ERROR, 'Fatal', array('element'=>$file, 'debug'=>array()), 'cant uncompress this file %element%' );
			return false;
		}

		return $file;
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Suppress a package file and recorded infos about it.
	 *  Return true or false.
	 *
	 \param $import_order(integer) Id of the package file recorded in []_import_history table.
	 */
	function SuppressPackage($import_order){
		$fileInfos = $this->GetPackageInfos($import_order);
		$file = $fileInfos['package_file_path'].'/'.$fileInfos['package_file_name'];

		//Suppress file
		if( is_file($file)){
			if(!unlink($file)) $result = false;
		}else{
			echo $file .' is not accessible';
		}

		//Suppress record in database only if package is not imported
		if($fileInfos['state'] !== 'imported')
		if(!$this->SuppressHistory($import_order)){
			$this->error_stack->push(ERROR, 'Fatal', array('element'=>$file, 'debug'=>array()), 'error when suppress package %element%');
			return false;}

			return true;
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Suppress package recorded infos.
	 *  Return true or false.
	 *
	 \param $import_order(integer) Id of the package file recorded in []_import_history table.
	 */
	function SuppressHistory($import_order) {
		$query = "DELETE FROM $this->IMPORT_HISTORY_TABLE WHERE import_order = '$import_order'";
		if(!$result = $this->dbranchbe->Execute($query)){
			$this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
			//print 'error SuppressHistory: '.$dbranchbe->ErrorMsg().'<BR>';
			return false;
		}
		return true;
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Suppress all packages not imported from history.
	 *  Return true or false.
	 *
	 */
	function CleanHistory($olderThan = null) {
		$query = "DELETE FROM $this->IMPORT_HISTORY_TABLE WHERE state = 'not imported'";
		
		if($olderThan > 0){
			$query .= " AND package_file_mtime < $olderThan";
		}
		
		if(!$result = $this->dbranchbe->Execute($query)){
			$this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
			return false;
		}
		return true;
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Get the description of the package.
	 *  Return true or false.
	 *
	 \param $import_order(integer) Id of the package file recorded in []_import_history table.
	 */
	function GetPackageDescription($import_order) {
		$query = "SELECT description FROM $this->IMPORT_HISTORY_TABLE WHERE import_order = '$import_order'";
		if(!$one = $this->dbranchbe->GetOne($query)){
			$this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
			//print 'error GetPackageDescription: '.$dbranchbe->ErrorMsg().'<BR>';
			return false;
		}else
		return $one;
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Get the recorded infos about of the package.
	 *  Return true or false.
	 *
	 \param $import_order(integer) Id of the package file recorded in []_import_history table.
	 */
	function GetPackageInfos( $import_order ) {
		$query = "SELECT * FROM $this->IMPORT_HISTORY_TABLE WHERE import_order = '$import_order'";
		if(!$one = $this->dbranchbe->Execute($query)){
			$this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg() );
			//print 'error GetPackageInfos: '.$dbranchbe->ErrorMsg().'<BR>';
			return FALSE;
		}else{
			$one = $one->GetArray(); //To transform object result in array;
			return $one[0];
		}
	}//End of method

	//---------------------------------------------------------------------------
	/*!
	 */
	function GetLogFile( $import_order ) {
		$query = "SELECT import_logfiles_file FROM $this->IMPORT_HISTORY_TABLE WHERE import_order = '$import_order'";
		$one = $this->dbranchbe->GetOne($query);
		return $one;
	}//End of method

	//---------------------------------------------------------------------------
	/*!
	 */
	function GetErrorFile( $import_order ) {
		$query = "SELECT import_errors_report FROM $this->IMPORT_HISTORY_TABLE WHERE import_order = '$import_order'";
		$one = $this->dbranchbe->GetOne($query);
		return $one;
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief To get the fields from a csv file.
	 *  Return array or false.
	 *
	 \param $csvfile(string) Path to csv file.
	 \param $maxrow(integer) Max number of rows to scan and return.
	 */
	static function ImportCsv( $csvfile , $maxrow=1000){
		global $error_stack;

		if(!$handle = fopen($csvfile, "r")){
			$error_stack->push(ERROR, 'Fatal', array('element'=>$csvfile, 'debug'=>array()), 'cant open %element%' );
			return false;
		}

		//Get the field name from the first line of csv file
		$fields = fgetcsv($handle, 1000, ";");
		if(!$fields[0]){
			global $smarty;
			$smarty->assign('msg', tra("The file is not a CSV file or has not a correct syntax"));
			$smarty->display("error.tpl");
			die;
		}

		while (!feof($handle) && $m != $maxrow) {
			$data = fgetcsv($handle, 1000, ";");
			if(@implode(' ',$data)){ //Test for ignore empty lines
				$temp_max = count($fields);
				for ($i = 0; $i < $temp_max; $i++){
					@$ar[$fields[$i]] = $data[$i];
				}
				$records[] = $ar;
			}
			$m++;
		}
		fclose ($handle);

		if ($m == $maxrow)
		print 'The number of record is greater than '. $maxrow .'. The ' . $maxrow . ' first line only are display here';
		//$this->error_stack->push(ERROR, 'Info', array('element'=>$maxrow, 'debug'=>array()),
		//'The number of record is greater than '. $maxrow .'. The ' . $maxrow . ' first line only are display here' );

		if (!is_array($records)) {
			$error_stack->push(ERROR, 'Fatal', array('element'=>$records, 'debug'=>array()), 'No records were found. Check the file please!' );
			return false;
		}

		return $records;
	}//End of method

}//End of class

