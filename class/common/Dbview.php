<?php

/**
 WHERE inst_name LIKE 'V25S10015%'
 AND
 (
 act_name LIKE 'a_verifier'
 OR
 act_name LIKE 'Verifier'
 )
 */



class Rb_Common_Dbview{

	function __construct($table){
		global $error_stack;
		$this->error_stack =& $error_stack;

		global $logger;
		$this->logger =& $logger;

		global $dbranchbe;
		$this->dbranchbe =& $dbranchbe;

		$this->OBJECT_TABLE = $table;
	}//End of method


	/**
	 *
	 * Request from filter string
	 * @param string $filter
	 * @param array $select
	 * @param string $order_by
	 * @param integer $offset
	 * @param integer $limit
	 * @return ADORecordSet
	 */
	public function getFromFilter($filter, array $select = array(), $order_by='', $offset=0, $limit=9999){

		if($select){
			$select = implode(',', $select);
		}else{
			$select = '*';
		}

		if($filter){
			$filter = ' WHERE ' . $filter;
		}

		if($order_by){
			$sort = ' ORDER BY ' . $order_by . ' ASC ';
		}

		//$pagin = ' LIMIT  ' . $offset . ' , ' . $limit;

		$query = 'SELECT '. $select .' FROM '.$this->OBJECT_TABLE . $filter . $sort;

		if(!$rs = $this->dbranchbe->SelectLimit( $query , $limit , $offset)){
			$this->error_stack->push(ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg());
			return false;
		}else{
			return $rs;
		}
	}

}




