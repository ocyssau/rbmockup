<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//===================================================================
/*! \brief this class manipulate attachments files.
 * An attachments files is picture or 3D visualisation file to show the content of the document without need natif application.
 * Its useful to integrate no-cad user in the conception process.
 * For exemple commercial can see a CATIA(c) file with a simple VRML reader if the CATPart document has been attach to a .wrl file
 * You can to want transform all your microsoft(C) office(C) document in a visualisable format like pdf file (or other format).
 * See too class viewer, thumnail, picture, visu.
 *
 * This files are not recorded in database.
 *
 * For exemple:
 * How to attach a preview picture to a document :
 * $space = new space('workitem');
 * $document = new document($space, 1);
 * $picture = new picture($document);
 * $picture->init_filepath(); //init the file path of the picture to create in ranchbe, set the var $picture->file.
 * $file = /www/ranchbe/wild_space/admin/.rbtmp/document_1.jpg
 * $picture->attach($file); //$file is the file path of the picture file. This file must be reachable by ranchbe.
 * $picture->generateThumb(); //Generate the thumbnail from the picture file.
 *
 */
require_once'./class/common/file.php';

abstract class attachment extends file{

	protected $document; //(object document) attached document object
	protected $document_id; //(integer) id of the attached document
	protected $file; //(string) file path of the attachment file
	protected $file_class;//(string) type of attachment (visu, picture, thumbnail)
	protected $file_extension;//(string) extension of the attachment file

	//-------------------------------------------------------------------------
	/*! \brief Get the file name and path of the attachment file
	 * Use init_filepath to get the path and name of the attachment file
	 *
	 * \param
	 */
	function init_filepath()
	{
		if(!isset($this->document_id)) {print 'document_id is not set';return false;}
		if(!isset($this->file_class)) {print 'file_class is not set';return false;}
		if(!isset($this->file_extension)) {print 'file_extension is not set';return false;}
		//$container =& $this->document->initContainer();
		//$path = $container->GetProperty('default_file_path').'/__attachments/_'.$this->file_class;
		$path = $this->document->DEFAULT_DEPOSIT_DIR.'/__attachments';
		if(!is_dir($path)){
			if(!mkdir($path , 0755)){
				$this->error_stack->push(ERROR, 'Warning', array('debug'=>$files), $path.' is not reachable');
				return false;
			}
		}
		$path = $this->document->DEFAULT_DEPOSIT_DIR.'/__attachments/_'.$this->file_class;
		if(!is_dir($path)){
			if(!mkdir($path , 0755)){
				$this->error_stack->push(ERROR, 'Warning', array('debug'=>$files), $path.' is not reachable');
				return false;
			}
		}
		return $this->file = $path .'/'. $this->document_id . $this->file_extension;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Attach a file to the current document
	 *
	 * \param $file(string) fullpath to file to attach
	 */
	function attach($file){

		//Test file
		if(!is_file($file)){
			$this->error_stack->push(ERROR, 'Warning', array('element'=>$file, 'debug'=>array()), 'the attachment file %element% is not a file');
			return false;
		}

		//Test property
		if(!isset($this->file)){
			$this->error_stack->push(ERROR, 'Warning', array('element'=>$file, 'debug'=>array()), 'the current object is not init');
			return false;
		}

		if(self::getExtension($file) != $this->file_extension){
			$this->error_stack->push(ERROR, 'Warning', array('element'=>$file, 'debug'=>array()), 'this file extension is unknow');
			return false;
		}

		//Copy file to the deposit dir
		$tofile = new file($file);
		if(!$tofile->copy($this->file, $mode=0755, true)){
			return false;
		}

		return true;

	}//End of method

	//----------------------------------------------------------
	/*! \brief Set the extension of the attachment file
	 *
	 * \param $extension(string) file extension with dot (ie: .jpg)
	 */
	function setExtension($extension){
		$this->file_extension = $extension;
		return true;
	}//End of method

} //End of class

//========================================================================================
/*! \brief visualisation 3D of the document. Only one 3Dvisu file by document.
 *
 */
class visu extends attachment {

	function __construct(document &$document)
	{
		$this->document =& $document;
		global $error_stack;
		$this->error_stack =& $error_stack;
		$this->document_id = $this->document->GetDocProperty('document_id');
		$this->file_class = 'visu';
		$doctype =& $document->GetDoctype();
		$this->file_extension = $doctype->GetProperty('visu_file_extension'); //Get visu file extension from doctype property
		if(empty($this->file_extension))
		$this->file_extension = '.wrl';
		$this->init_filepath();
	}//End of method

} //End of class

//========================================================================================

/*! \brief 2D visualisation file of the document
 *
 */
class picture extends attachment {

	public $width; //width of the thumbnail in pixel
	public $height; //height of the thumbnail in pixel

	function __construct(document &$document)
	{
		$this->document =& $document;
		global $error_stack;
		$this->error_stack =& $error_stack;
		$this->document_id = $this->document->GetDocProperty('document_id');
		$this->file_class = 'picture';
		$this->file_extension = '.jpg';
		$this->init_filepath();
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief Create a thumbnail from the picture file. Return the thumbnail object
	 *
	 * \param
	 */
	function generateThumb()
	{
		if(empty($this->file)){
			$this->init_filepath();
		}
		$thumb = new thumbnail($this->document, $this->document_id);
		$thumb->init_filepath();
		$thumb->generate($this->file);
		return $thumb;
	}//End of method

} //End of class

//========================================================================================
/*! \brief Thumbnail of the document to rapid pre-visualisation of the document.
 *
 */
class thumbnail extends picture {

	public $width = '150'; //width of the thumbnail in pixel
	public $height = '150'; //height of the thumbnail in pixel

	function __construct(document &$document)
	{
		$this->document =& $document;
		global $error_stack;
		$this->error_stack =& $error_stack;
		$this->document_id = $this->document->GetDocProperty('document_id');
		$this->file_class = 'thumbs';
		$this->file_extension = '.png';
		$this->init_filepath();
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief generate a thumbnail of the picture
	 *
	 * \param
	 */
	function generate($file)
	{
		if(empty($this->file)){
			$this->error_stack->push(ERROR, 'Fatal', array('debug'=>$this->file), 'you must init_filepath before use of this method');
			return false;
		}

		// Def of width and height
		$width =& $this->width;
		$height =& $this->height;

		//TODO : Verifier que $file est une image

		// Cacul des nouvelles dimensions
		list($width_orig, $height_orig) = getimagesize($file);

		$ratio_orig = $width_orig/$height_orig;
		if ($width/$height > $ratio_orig) {
			$width = $height*$ratio_orig;
		} else {
			$height = $width/$ratio_orig;
		}

		// Redimensionnement
		$image_p = imagecreatetruecolor($width, $height);
		$image = imagecreatefromjpeg($file);
		imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
		imagetruecolortopalette($image_p , true , 16); //convert in 16 colors
		// Create file
		imagepng($image_p, $this->file);

	}//End of method

} //End of class

//===========================================================
/*! \brief Class to view file or attachment file
 * How to use :
 * Get the picture and visu and show it in a html page for document id=1:
 * require_once('./class/common/space.php');
 * require_once('./class/common/document.php');
 * require_once('./class/common/attachment.php');
 * $space = new space('workitem');
 * $document = new document($space, 1);
 * $viewer = new viewer();
 * $viewer->init($document);
 * $documentPicture = $viewer->displayPicture();
 * $smarty->assign('documentPicture', $documentPicture);
 * $documentVisu = $viewer->displayVisu();
 * $smarty->assign('documentVisu', $documentVisu);
 *
 * Send the picture file to end user browser :
 * ...(repeat the first lines of the previous example)...
 * $viewer = new viewer();
 * $viewer->init($document);
 * $viewer->initPictureFile();
 * $viewer->pushfile();
 *
 * Send a file to end user browser :
 * ...(repeat the first lines of the previous example)...
 * $viewer = new viewer();
 * $file = /www/ranchbe/deposit_dir/AFF001/exemple1.txt;
 * $viewer->initFile($file);
 * $viewer->pushfile();
 OR
 * $document->GetDofiles();
 * $fsdata =& $document->dofiles[0]->initFsdata();
 * $viewer->initFsdata($fsdata);
 * $viewer->pushfile();
 *
 */
class viewer{
	private $document;
	private $document_id;
	private $file;
	private $file_extension;
	private $mimetype;
	private $visuFile; //Filename and path to the visu file
	private $thumbFile; //Filename and path to the thumbnail file
	private $pictureFile; //Filename and path to the picture file
	private $driver; //The name of the driver to use
	private $odriver; //The object driver to use

	//----------------------------------------------------------
	function __construct(){
		global $error_stack;
		$this->error_stack =& $error_stack;
	}//End of method

	//----------------------------------------------------------
	function init(document &$odocument){
		$this->document =& $odocument;
		$this->space =& $odocument->space;
	}//End of method

	//----------------------------------------------------------
	function initSpace(space &$space){
		$this->space =& $space;
	}//End of method

	//----------------------------------------------------------
	function getProperty($property_name){
		return $this->$property_name;
	}//End of method

	//----------------------------------------------------------
	/**
	 * Load the code for a specific viewer driver. Private function.
	 */
	private function initDriver()
	{
		if(empty($this->driver))
		$this->driver = 'defaultViewer';

		$file = './class/viewerDrivers/'.$this->driver.'.inc.php';
		if(is_file($file)){
			include_once($file);
			$this->odriver = new $this->driver($this);
			$VIEWER_LASTVIEWER = $this->driver;
			return true;
		}
		return false;
	} //End of method

	//----------------------------------------------------------
	/**
	 * Get the properties of a recorded file
	 */
	/*function initRFile($file_id){
	 if(!isset($this->ofile)) return false;
	 require_once('./class/common/recordfile.php');
	 $recordfile = new recordfile($this->space, $file_id);
	 $this->ofile = $recordfile->initFsdata();
	 $this->file = $this->ofile->getProperty('file');
	 $this->file_extension = $this->ofile->getProperty('file_extension');
	 $this->mimetype = $this->ofile->getProperty('file_mimetype');
	 $this->noread = $this->ofile->getProperty('no_read');
	 $this->driver = $this->ofile->getProperty('viewer_driver');
	 } //End of method*/

	//----------------------------------------------------------
	/**
	 * Get the properties from fsdata object
	 */
	function initFsdata(fsdata $fsdata){
		$this->ofile =& $fsdata;
		$this->file = $this->ofile->getProperty('file');
		$this->file_extension = $this->ofile->getProperty('file_extension');
		$this->mimetype = $this->ofile->getProperty('file_mimetype');
		$this->noread = $this->ofile->getProperty('no_read');
		$this->driver = $this->ofile->getProperty('viewer_driver');
		return true;
	} //End of method

	//----------------------------------------------------------
	/**
	 * Init the properties of the current object from the object ofile
	 */
	private function initFile($file){
		if(is_file($file)){
			require_once'./class/common/file.php';
			$this->ofile = new file($file);
		}else if(is_dir($file)){
			require_once'./class/common/fsdata.php';
			$this->ofile = new fsdata($file);
		}else return false;
		$this->file = $this->ofile->getProperty('file');
		$this->file_extension = $this->ofile->getProperty('file_extension');
		$this->mimetype = $this->ofile->getProperty('file_mimetype');
		$this->noread = $this->ofile->getProperty('no_read');
		$this->driver = $this->ofile->getProperty('viewer_driver');
		return true;
	} //End of method

	//-------------------------------------------------------------------------
	/*! \brief Generate a embeded viewer for the file
	 *  Return html code of the embeded viewer
	 *
	 * \param
	 */
	function embeded(){
		if(isset($this->odriver) && isset($this->file))
		return $this->odriver->embededViewer($this->file);
		else
		return false;
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief Push the file to the client
	 *
	 * This method send the content of the $file to the client browser. It send too the mimeType to help the browser to choose the right application to use for open the file.
	 * If the mimetype = no_read, display a error.
	 * Return true or false.
	 \param $file(string) path of the file to display on the client computer
	 */
	function pushfile(){
		if($this->noread == 'no_read'){
			$this->error_stack->push(ERROR, 'Fatal', array('element'=>$file), 'you cant view this file %element% by this way');
			return false;
		}
		$this->ofile->DownloadFile(); die;
		/*
		 if(!empty($this->file) && is_file($this->file)){
		 $fileName = basename($this->file);
		 header("Content-disposition: attachment; filename=$fileName");
		 header("Content-Type: " . $this->mimetype);
		 header("Content-Transfer-Encoding: $fileName\n"); // Surtout ne pas enlever le \n
		 header("Content-Length: ".filesize($this->file));
		 header("Pragma: no-cache");
		 header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
		 header("Expires: 0");
		 readfile($this->file);
		 die;
		 }else if(!empty($this->file) && is_dir($this->file) && isset($this->ofile) ){
		 $this->ofile->DownloadFile();
		 }else{
		 $this->error_stack->push(ERROR, 'Warning', array('debug'=>$this->file), 'There is no associated file to display');
		 return false;
		 }
		 */
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief Show the picture associate to current document in a html page (embeded)
	 *
	 * \param
	 */
	function displayPicture(){
		if(empty($this->pictureFile)){
			if(!$this->initPictureFile())
			return false;
		}
		return '<img border=0 align="center" src="'.$this->pictureFile.'" height="300" />';
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief Show the visualisation file of the current document in a html page (embeded)
	 *
	 * \param
	 */
	function displayVisu(){
		if(empty($this->visuFile)){
			if(!$this->initVisuFile())
			return false;
		}
		//init the driver for the file
		$this->initDriver();
		//Return the embeded viewer
		return $this->embeded();
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief Get the picture file name and path
	 * Return true or false
	 * \param
	 */
	function initPictureFile(){
		if(!isset($this->document)) return false;
		require_once'./class/common/file.php';
		$this->ofile = new picture($this->document);
		$this->file =& $this->ofile->GetProperty('file');
		if($this->initFile($this->file)){
			$this->pictureFile =& $this->file;
			return true;
		}else return false;
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief Get the thumbnail file name and path
	 *
	 * \param
	 */
	function initThumbFile(){
		if(!isset($this->document)) return false;
		require_once'./class/common/file.php';
		$this->ofile = new thumbnail($this->document);
		$this->file =& $this->ofile->GetProperty('file');
		if($this->initFile($this->file)){
			$this->thumbFile =& $this->file;
			return true;
		}else return false;

		/*if(!isset($this->document)) return false;
		 require_once'./class/common/file.php';
		 $this->ofile = new thumbnail($this->document);
		 $this->document->DEFAULT_DEPOSIT_DIR;
		 $path = $this->document->DEFAULT_DEPOSIT_DIR.'/__attachments/_thumbs';
		 if(is_dir($path)){
		 return $this->file = $path .'/'. $this->document_id . $this->file_extension;
		 }else{
		 $this->error_stack->push(ERROR, 'Warning', array('debug'=>$files), $path.' is not reachable');
		 return false;
		 }
		 if($this->initFile()){
		 $this->thumbFile = $this->file;
		 return true;
		 }
		 return false;*/
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief Get the visualisation file name and path
	 *
	 * \param
	 */
	function initVisuFile(){
		if(!isset($this->document)) return false;
		require_once'./class/common/file.php';
		$this->ofile = new visu($this->document);
		$this->file =& $this->ofile->GetProperty('file');
		if($this->initFile($this->file)){
			$this->visuFile =& $this->file;
			return true;
		} else return false;
	}//End of method

	//-------------------------------------------------------------------------
	/* Pour afficher le pdf si un fichier pdf est associ�
	 * 
	 */
	function initFromPdfDocfile(){
		$params = array();
		$params['exact_find'] = array('file_extension'=>'.pdf', 'document_id'=>$this->document->GetId());
		$params['select'] = array('file_id');
		$docfile = new docfile($this->document->space, 0);
		$dfProps = $docfile->GetFiles($params);
		//var_dump($dfProps);die;
		
		if( $dfProps[0]['file_id'] ){
			$docfile->init( $dfProps[0]['file_id'] );
			$this->initFsdata( $docfile->initFsdata() );
			return true;
		}else{
			return false;
		}
	}
	
	//-------------------------------------------------------------------------
	/*! \brief Alias of initFile()
	 *
	 * \param
	 */
	function initRawFile($file){
		return $this->initFile($file);
	}//End of method

}//End of class Don't let blank line after this

//Dont let empty lines after the close tag...: ?>
