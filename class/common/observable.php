<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('./class/common/basic.php');

//!! Observable
//! An abstract class implementing observable objects
/*!
  \abstract
  Methods to override: NONE
  This class implements the Observer design pattern defining Observable
  objects, when a class extends Observable Observers can be attached to
  the class listening for some event. When an event is detected in any
  method of the derived class the method can call notifyAll($event,$msg)
  to notify all the observers listening for event $event.
  The Observer objects must extend the Observer class and define the
  notify($event,$msg) method.
*/
abstract class rb_observable extends basic{
  private $_observers=Array();
  
  function __construct(){
  } //End of method
  
  /*!
   This method can be used to attach an object to the class listening for
   some specific event. The object will be notified when the specified
   event is triggered by the derived class.
  */
  function attach($event, &$obj){
    if (!is_object($obj)){
    	return false;
    }
    $obj->_observerId = uniqid(rand());
    $this->_observers[$event][$obj->_observerId] = &$obj;
  } //End of method
  
  /*!
   Attaches an object to the class listening for any event.
   The object will be notified when any event occurs in the derived class.
  */
  function attach_all(&$obj){
    if (!is_object($obj)) {
    	return false;
    }
    $obj->_observerId = uniqid(rand());
    $this->_observers['all'][$obj->_observerId] = &$obj;
  } //End of method
  
  /*!
   Detaches an observer from the class.
  */
  function dettach(&$obj){
  	if(isset($this->_observers[$obj->_observerId])){
    	unset($this->_observers[$obj->_observerId]);
    }
  } //End of method
  
  /*!
  \protected
  Method used to notify objects of an event. This is called in the
  methods of the derived class that want to notify some event.
  */
  protected function notify_all($event, $msg){
  	//reset($this->_observers[$event]);
  	if(isset($this->_observers[$event])){
    	foreach ($this->_observers[$event] as $observer){
    		$observer->notify($event,$msg);
    	}
    }
	if(isset($this->_observers['all'])){
    	foreach ($this->_observers['all'] as $observer){
    		$observer->notify($event,$msg);
    	}
    }
  } //End of method

} //End of class
?>
