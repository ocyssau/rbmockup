<?php

require_once('./class/common/fsdata.php');

/*! \brief fsdata create a link between the database record of data (docfile or recordfile)
* and the real data on the filesystem of the server like a file or a directory.
*
*/
class fsdataVersion extends fsdata{

  protected $fsdata; //(fsdata object)
  protected $versionId; //(integer)
  protected $versionReposit; //(string) path to directory where store version file

//----------------------------------------------------------
function __construct(fsdata &$fsdata, $versionId){
  global $error_stack;
  $this->error_stack =& $error_stack;
  
  global $logger;
  $this->logger =& $logger;
  
  $this->versionReposit = $fsdata->getProperty('file_path').'/__versions';
  $dataPath = $this->versionReposit.'/'.$versionId.'/'.$fsdata->GetProperty('file_name');
  $this->init($dataPath);
  $this->SetFsdata($fsdata);
  $this->file_props['file_version'] = $versionId;
  $this->versionId =& $this->file_props['file_version']; //alias
}//End of method

//----------------------------------------------------------
/* !\brief factory method to construct object
*/
/*
static function __construct($datapath, $versionId){
  global $error_stack;
  $this->error_stack =& $error_stack;
  
  global $logger;
  $this->logger =& $logger;
  
  //$this->versionReposit = $fsdata->getProperty('file_path').'/__versions';
  //$dataPath = $this->versionReposit.'/'.$versionId.'/'.$fsdata->GetProperty('file_name');
  $this->init($dataPath);
  //$this->SetFsdata($fsdata);
  $this->file_props['file_version'] = $versionId;
  $this->versionId =& $this->file_props['file_version']; //alias
}//End of method
*/

//--------------------------------------------------------
/*!\brief
* Set the fsdata by reference
*   
\param $fsdata(object) object of the fsdata
*/
function SetFsdata(fsdata &$fsdata){
  $this->fsdata =& $fsdata;
}//End of method


//--------------------------------------------------------
/*!\brief
* init the directory where store version file copy
*   
*/
function InitDirectory(){

  if(!is_dir($this->versionReposit)){ //Check if version reposit dir is existing (ie: :/aff01/__versions/)
    if(!rdirectory::createdir($this->versionReposit, 0755)){ //..and create it if not
      $this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->versionReposit), 'cant create version dir : %element%.');
      return false;
    }
  }
  if(!is_dir($this->getProperty('file_path'))){ // check if version dir is existing (ie: c:/aff01/__versions/1/camu)
    if(!rdirectory::createdir($this->getProperty('file_path'), 0755)){ //..and create it if not
      $this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->getProperty('file_path')), 'cant create version dir : %element%.');
      return false;
    }
  }
  return true;

}//End of method

//--------------------------------------------------------
/*!\brief
* Create a new version copy
*   
*/
function KeepVersion(){
  if(DEFAULT_BACK_VERSIONS_NUM === 0) //Dont keep version if 0
    return false;
  if(!$this->InitDirectory())
    return false;
  //if(!$this->fsdata->Copy($this->GetProperty('file'), 0755, false)){ // Dont replace the existing file
  if(!$this->fsdata->Copy($this->GetProperty('file'), 0755, true)){ // Replace the existing file
    $this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->GetProperty('file')), 'cant create verions file %element%');
    return false;
  }
  return true;
}//End of method

//--------------------------------------------------------
/*!\brief get the reposit path
*   
*/
function GetVersionReposit(){
  return $this->versionReposit;
}//End of method

}//End of class

?>
