<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('./class/common/basic.php');

/*! \brief This class manage Container. A container is a basic ranchbe component that is content in followings spaces :
 workitem, bookshop, cadlib, mockup.
 * Container content documents or recordfiles
 *
 */
class container extends objects{

	public $SPACE_NAME; //(String)

	protected $OBJECT_TABLE; //(String)Table where are stored the containers definitions
	protected $HISTORY_TABLE; //(String)Table were are stored the container histories
	protected $INDICE_TABLE; //(String)Table where are stored the container indices
	//Fields:
	protected $FIELDS_MAP_ID; //(String)Name of the field of the primary key
	protected $FIELDS_MAP_NUM; //(String)Name of the field to define the name
	protected $FIELDS_MAP_DESC; //(String)Name of the field where is store the description
	protected $FIELDS_MAP_STATE; //(String)Name of the field where is store the state
	protected $FIELDS_MAP_INDICE; //(String)Name of the field where is store the indices
	protected $FIELDS_MAP_DEPOSITDIR; //(String)Name of the field where is store the default deposit directory
	protected $FIELDS_MAP_FATHER; //(String)Name of the field where is store the id of the father
	//protected $METADATA_TABLE; //(String)

	protected $dbranchbe; //(Object)
	protected $usr; //(Object)
	public $error_stack; //(Object)
	private $core_prop; //(Array)
	protected $document; ////(Object document) documents of the container
	protected $category; ////(Object category)

	function __construct(space &$space , $container_id=NULL){
		global $dbranchbe;
		$this->dbranchbe =& $dbranchbe;
		$this->space =& $space;
		$this->error_stack =& $this->space->error_stack;
		global $usr;
		$this->usr =& $usr;

		foreach(get_object_vars($this->space) as $var=>$value)
		$this->$var = $value;

		$this->OBJECT_TABLE  = $this->space->CONT_TABLE;
		$this->HISTORY_TABLE = $this->space->CONT_HISTORY_TABLE;
		$this->INDICE_TABLE  = $this->space->CONT_INDICE_TABLE;

		$this->FIELDS_MAP_ID = $this->space->CONT_FIELDS_MAP_ID;
		$this->FIELDS_MAP_NUM = $this->space->CONT_FIELDS_MAP_NUM;
		$this->FIELDS_MAP_DESC = $this->space->CONT_FIELDS_MAP_DESC;
		$this->FIELDS_MAP_STATE = $this->space->CONT_FIELDS_MAP_STATE;
		$this->FIELDS_MAP_INDICE = $this->space->CONT_FIELDS_MAP_INDICE;
		$this->FIELDS_MAP_FATHER = 'project_id';

		//use by GUI to create find select

		$this->FIELDS_MAPPING = array($this->FIELDS_MAP_NUM=>tra('Number'),
		$this->FIELDS_MAP_DESC=>tra('Description'),
		$this->FIELDS_MAP_STATE=>tra('State'),
		);
		/*
		 $this->FIELDS_MAPPING = array('container_number'=>tra('Number'),
		 'container_description'=>tra('Description'),
		 'container_state'=>tra('State'),
		 );

		 $this->FIELDS = array(
		 'container_id'=>$this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_ID,
		 'container_number'=>$this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_NUM,
		 'container_state'=>$this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_STATE,
		 'container_description'=>$this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_DESC,
		 'file_only'=>$this->OBJECT_TABLE.'.file_only',
		 'container_indice_id'=>$this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_INDICE,
		 'project_id'=>$this->OBJECT_TABLE.'.project_id',
		 'default_process_id'=>$this->OBJECT_TABLE.'.default_process_id',
		 'default_file_path'=>$this->OBJECT_TABLE.'.default_file_path',
		 'open_date'=>$this->OBJECT_TABLE.'.open_date',
		 'open_by'=>$this->OBJECT_TABLE.'.open_by',
		 'forseen_close_date'=>$this->OBJECT_TABLE.'.forseen_close_date',
		 'close_date'=>$this->OBJECT_TABLE.'.close_date',
		 'close_by'=>$this->OBJECT_TABLE.'.close_by',
		 'object_class'=>$this->OBJECT_TABLE.'.object_class',
		 'alias_id'=>$this->OBJECT_TABLE.'.alias_id',
		 );
		 */
		$this->FIELDS = array(
		$this->FIELDS_MAP_ID=>$this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_ID,
		$this->FIELDS_MAP_NUM=>$this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_NUM,
		$this->FIELDS_MAP_STATE=>$this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_STATE,
		$this->FIELDS_MAP_DESC=>$this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_DESC,
                        'file_only'=>$this->OBJECT_TABLE.'.file_only',
		$this->FIELDS_MAP_INDICE=>$this->OBJECT_TABLE.'.'.$this->FIELDS_MAP_INDICE,
                        'project_id'=>$this->OBJECT_TABLE.'.project_id',
                        'default_process_id'=>$this->OBJECT_TABLE.'.default_process_id',
                        'default_file_path'=>$this->OBJECT_TABLE.'.default_file_path',
                        'open_date'=>$this->OBJECT_TABLE.'.open_date',
                        'open_by'=>$this->OBJECT_TABLE.'.open_by',
                        'forseen_close_date'=>$this->OBJECT_TABLE.'.forseen_close_date',
                        'close_date'=>$this->OBJECT_TABLE.'.close_date',
                        'close_by'=>$this->OBJECT_TABLE.'.close_by',
                        'object_class'=>$this->OBJECT_TABLE.'.object_class',
                        'alias_id'=>$this->OBJECT_TABLE.'.alias_id',
		);

		if(!is_null($container_id))
		$this->init($container_id);

	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief init the property of the container
	 *  Return core_prop(array)
	 *
	 * \param $container_id(integer) Id of the container
	 */
	static function _factory($SPACE_NAME , $container_id=NULL){
		switch($SPACE_NAME){
			case('cadlib'):
				require_once('./class/cadlib.php');
				return new cadlib($container_id);
				break;
			case('bookshop'):
				require_once('./class/bookshop.php');
				return new bookshop($container_id);
				break;
			case('mockup'):
				require_once('./class/mockup.php');
				return new mockup($container_id);
				break;
			case('workitem'):
				require_once('./class/workitem.php');
				return new workitem($container_id);
				break;
		}
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief init the property of the container
	 *
	 * \param $container_id(integer) Id of the container
	 */
	function init($container_id){
		$this->container_id = $container_id;
		if(isset($this->core_prop)) unset($this->core_prop);
		if(isset($this->document)) unset($this->document);
		if(isset($this->category)) unset($this->category);
		if(isset($this->history)) unset($this->history);
		if(isset($this->project)) unset($this->project);
		return true;

		/*if($this->core_prop = $this->GetInfos($container_id)){ //Get data about container
		 $this->container_id =& $this->core_prop[$this->FIELDS_MAP_ID];
		 $this->container_number =& $this->core_prop[$this->FIELDS_MAP_ID];
		 $this->container_description =& $this->core_prop[$this->FIELDS_MAP_DESC];
		 $this->container_state =& $this->core_prop[$this->FIELDS_MAP_STATE];
		 $this->container_indice_id =& $this->core_prop[$this->FIELDS_MAP_INDICE];
		 if($this->core_prop['file_only'] == 0){
		 $this->space->FILE_TABLE         = $this->space->DOC_FILE_TABLE;
		 }
		 return true;
		 }else{
		 $this->error_stack->push(ERROR, 'Warning', array('element'=>$container_id, 'debug'=>array()), 'there are no records for container_id %element%');
		 return false;
		 }*/
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * Get the document object and link it to the current container object
	 * return a document instance if success, else fase.
	 * Set the container object $this->document
	 * The method init() must be call before.
	 * Example :
	 * $space=new(bookshop); $container=new container($space); $container->init(1) //OU $space=new(bookshop); $container = new container($space, 1); //OU $container=container::__factory('bookshop', 1);
	 *
	 * $doc = $container->initDoc(); //the object $doc is linked to the object $container.
	 * Use ever this method when you have a container object created
	 *
	 */
	function initDoc($document_id=NULL){
		require_once('./class/common/document.php');
		if(isset($this->container_id)){
			$this->document = new document($this->space, $document_id);
			$this->document->SetContainer($this);
			return $this->document;
		}
		print '$this->container_id is not set';
		return false;
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * Get docfile object and link it to the current container object
	 * return a docfile instance if success, else fase.
	 * Set the container object $this->docfile
	 * The method init() must be call before.
	 */
	function initDocfile($file_id=NULL){
		require_once('./class/common/docfile.php');
		if(isset($this->container_id)){
			$this->docfile = new docfile($this->space, $file_id);
			$this->docfile->SetContainer($this);
			return $this->docfile;
		}
		print '$this->container_id is not set';
		return false;
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * Get recordfile object and link it to the current container object
	 * return a recordfile instance if success, else fase.
	 * Set the container object $this->docfile
	 * The method init() must be call before.
	 *
	 */
	function initRecordfile($file_id=NULL){
		require_once('./class/common/recordfile.php');
		if(isset($this->container_id)){
			$this->docfile = new recordfile($this->space, $file_id);
			$this->docfile->SetContainer($this);
			return $this->docfile;
		}
		print '$this->container_id is not set';
		return false;
	}//End of method

	//--------------------------------------------------------
	/*!\brief init category object linked to current container
	 *
	 */
	function initCategory($category_id=NULL){
		/*
		 require_once('./class/category.php');
		 $this->category = new category($this->space, $category_id);
		 $this->category->SetContainer($this);
		 return $this->category;
		 */

		require_once('./class/categoryContainerLink.php');
		$this->category = new categoryContainerLink( $this );
		return $this->category;

	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * Get the document object linked to the current container
	 */
	function GetDoc(){
		if(isset($this->document)){
			return $this->document;
		}else{
			return $this->initDoc();
		}
		return false;
	}//End of method

	//----------------------------------------------------------
	/*! \brief test if is a file manager or document manager container type
	 *  Return bool
	 */
	function IsFileOnly(){
		return $this->GetProperty('file_only');
	}//End of method

	//----------------------------------------------------------
	/*! \brief Get all properties of the current container
	 *  Return a array
	 */
	function GetProperties(){
		if(!isset($this->core_prop)) return false;
		else return $this->core_prop;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Get the property of the container by the property name. init() must be call before
	 *
	 * \param $property_name(string) = container_id,container_number,container_description,container_state,container_indice_id.
	 */
	function GetProperty($property_name){
		if(!isset($this->container_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->container_id is not set');
			return false;}

			switch($property_name){
				case 'id':
				case 'container_id':
					if(isset($this->core_prop[$this->FIELDS_MAP_ID])){
						return $this->core_prop[$this->FIELDS_MAP_ID];
					}
					else{
						$realName = $this->FIELDS_MAP_ID;
					}
					break;

				case 'number':
				case 'container_number':
					if(isset($this->core_prop[$this->FIELDS_MAP_NUM])){
						return $this->core_prop[$this->FIELDS_MAP_NUM];
					}else $realName = $this->FIELDS_MAP_NUM;
					break;

				case 'description':
				case 'container_description':
					if(isset($this->core_prop[$this->FIELDS_MAP_DESC]))
					return $this->core_prop[$this->FIELDS_MAP_DESC];
					else $realName = $this->FIELDS_MAP_DESC;
					break;

				case 'state':
				case 'container_state':
					if(isset($this->core_prop[$this->FIELDS_MAP_STATE]))
					return $this->core_prop[$this->FIELDS_MAP_STATE];
					else $realName = $this->FIELDS_MAP_STATE;
					break;

				case 'indice':
				case 'container_indice_id':
					if(isset($this->core_prop[$this->FIELDS_MAP_INDICE]))
					return $this->core_prop[$this->FIELDS_MAP_INDICE];
					else $realName = $this->FIELDS_MAP_INDICE;
					break;

				default:
					if(isset($this->core_prop[$property_name]))
					return $this->core_prop[$property_name];
					else $realName = $property_name;
					break;

			} //End of switch

			$query = 'SELECT '.$realName.' FROM '.$this->OBJECT_TABLE.' WHERE '.$this->FIELDS_MAP_ID.' = \''.$this->container_id.'\'';
			$res = $this->dbranchbe->GetOne($query);
			if($res === false){
				$this->error_stack->push(ERROR, 'Fatal', array('query'=>$query , 'debug'=>array($this->container_id)), $this->dbranchbe->ErrorMsg());
				return false;
			}
			else
				return $this->core_prop[$realName] = $res;
	}//End of method

	//--------------------------------------------------------------------
	/*!\brief This method can be used to create a container.
	 *   Return the id if no errors, else return FALSE
	 *
	 \param $params[number](integer) The number of the new container
	 \param $params[description](string) ..
	 \param $params[forseen_close_date](integer) ..
	 \param $params[default_process_id](integer) .. the default process id
	 \param $params[doctype_id](array) .. no-assoc array of all doctype_id permit for this container
	 \param $params[file_only](bool) true if the new container must manage file only
	 \param ...And all others metadatas create by users
	 */
	function Create($params){
		//Filter $params var to prevent maliscious informations
		$allow_input = array( $this->FIELDS_MAP_NUM,
		$this->FIELDS_MAP_DESC,
		$this->FIELDS_MAP_INDICE,
                        'forseen_close_date',
                        'default_process_id',
                        'supplier_id',
                        'project_id',
                        'file_only',
		);
		//Autorize the input of metadatas in $allow_input
		$optionalFields = $this->GetMetadata(array('select'=>array('field_name')));
		foreach($optionalFields as $field)
		$allow_input[] = $field['field_name'];

		//Filter input data for prevent maliscious informations
		foreach ( $allow_input as $input){
			if (isset($params[$input]) && !empty($params[$input]))
			$data[$input] = $params[$input];
		}

		if (!is_dir($this->DEFAULT_DEPOSIT_DIR )) {
			$this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->DEFAULT_DEPOSIT_DIR, 'debug'=>array()), 'Deposit dir %element% don\'t exist' );
			return false;
		}

		//Add ranchbe 0.6
		//Create a new directory to store files
		if( $data['file_only'] ){
			$data[$this->FIELDS_MAP_DEPOSITDIR] = $this->DEFAULT_DEPOSIT_DIR . '/' . $data[$this->FIELDS_MAP_NUM] .'/__imported';
		}else{
			$data[$this->FIELDS_MAP_DEPOSITDIR] = $this->DEFAULT_DEPOSIT_DIR . '/' . $data[$this->FIELDS_MAP_NUM];
		}
		$dir = $data[$this->FIELDS_MAP_DEPOSITDIR];
		//Create a new directory to store files
		require_once('class/common/rdirectory.php');
		if( !rdirectory::createdir($dir, 0755) ){
			$this->error_stack->push(ERROR, 'Error', array('dir'=>$dir,'container'=>$data[$this->FIELDS_MAP_NUM]), 'creation of directory %dir% failed. Can not create container %container%' );
			return false;
		}

		if (!isset($data['forseen_close_date'])) $data['forseen_close_date']= time() + ( 3600 * 24 * $DEFAULT_LIFE_TIME );

		//Set the user
		$currentUser = $this->usr->getProperty('auth_user_id');
		//Set current time
		$now = time();

		$data['open_by'] = "$currentUser";
		$data['open_date'] = "$now";
		$data['update_by'] = "$currentUser";
		$data['update_date'] = "$now";

		/*
		 if(empty($dir)){
		 $this->error_stack->push(ERROR, 'Fatal', array('element'=>$dir, 'debug'=>array($data)), 'no defined deposit directory %element%' );
		 return false;}

		 if( file_exists($dir)){
		 $this->error_stack->push(ERROR, 'Warning', array('element'=>$dir, 'debug'=>array($data)), 'directory %element% exist');
		 }else{
		 //Le "mode" ne fonctionne pas sous windows, ce qui oblige a faire un
		 //chmod par la suite.
		 //bool mkdir ( string pathname [, int mode [, bool recursive [, resource context]]] )
		 if(!mkdir("$dir" , 0755, 1)){
		 $this->error_stack->push(ERROR, 'Warning', array('element'=>$dir, 'debug'=>array($data)), 'error when creating deposit_dir %element%' );
		 }else{
		 if (! chmod ("$dir", 0755) ) { //!!chmod ne fonctionne pas avec les fichiers distants!!
		 $this->error_stack->push(ERROR, 'Warning', array('element'=>$dir, 'debug'=>array($data)), 'chmod impossible on %element%' );
		 }
		 }
		 }
		 */

		//Create a basic object
		$this->container_id = $this->BasicCreate($data);

		if ( (empty ($this->container_id) ) || ($this->container_id === false) ){
			$this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->container_id, 'debug'=>array($data)), 'the id is empty %element%');
			return false;}

			//link doctypes
			if (isset($params['doctype_id'])){
				foreach ( $params['doctype_id'] as $doctype_id ){
					$this->LinkDoctype($doctype_id);
				}
			}

			//Write history
			$this->history['action_name'] = 'Create';
			$this->WriteHistory();

			return $id;

	}//End of method

	//--------------------------------------------------------------------
	/*!\brief This method can be used to suppress a container.
	 *   Return false or true
	 *
	 */
	function Suppress(){
		if(!isset($this->container_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->container_id is not set');
			return false;}
			//Get infos about suppressed container
			$this->history = $this->Getinfos();
			if (!$this->BasicSuppress($this->container_id)){ //Suppress container and objects database records
				$this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->container_id, 'debug'=>array($this->history)), 'error in suppress %element%' );
				return false;
			}
			//Write history
			$this->history['action_name'] = 'Suppress';
			$this->WriteHistory ();
			return true;
	}//End of method

	//--------------------------------------------------------------------
	/*!\brief This method can be used to modify a container
	 *   Return false or true
	 *
	 \param $params = array "field-to-modify"=>"New-value"
	 */
	function Update($params){
		if(!isset($this->container_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->container_id is not set');
			return false;
		}
		//Re-map the array $data to filter maliscious informations
		$allow_input = array( $this->FIELDS_MAP_NUM,
							$this->FIELDS_MAP_DESC,
							$this->FIELDS_MAP_INDICE,
	                        'forseen_close_date',
	                        'default_process_id',
	                        'supplier_id',
	                        'project_id',
							'access_code',
							$this->SPACE_NAME . '_state'
							);
		//Autorize the input of metadatas in $allow_input
		$optionalFields = $this->GetMetadata(array('select'=>array('m.field_name')));
		if($optionalFields){
			foreach($optionalFields as $field){
				$allow_input[] = $field['field_name'];
			}
		}
		
		//Filter input data for prevent maliscious informations
		foreach ( $allow_input as $input){
			if (isset($params["$input"])){
				$data["$input"] = $params["$input"];
			}
		}
		
		if (!$ret =  $this->BasicUpdate($data, $this->container_id)){
			return false;
		}
		
		//Write history
		$this->history['action_name'] = 'Update';
		$this->WriteHistory();
		return $ret;
	}//End of method

	//--------------------------------------------------------------------
	/*!\brief
	 * This method can be used to get list of all container.
	 * return a array if success, else return false.
	 \param $params is use for manage the display. See parameters function of GetQueryOptions()
	 */
	function GetAll( $params = array() ){
		return $this->GetAllBasic($params);
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * This method can be used to get list of all document from a container.
	 * return a array if success, else return false.
	 \param $params(array) is use for manage the display. See parameters function of GetQueryOptions()
	 */
	function GetAllDocuments($params, $displayHistory=false){
		if(!isset($this->container_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->container_id is not set');
			return false;
		}
		if(!isset($this->document)){
			$this->initDoc();
		}
		$params['exact_find'][$this->FIELDS_MAP_ID] = $this->container_id;
		return $this->document->GetDocuments($params, $displayHistory);
	}//End of method

	//--------------------------------------------------------------------
	/*!\brief
	 * This method can be used to get info about a single container.
	 * return a array if success, else return false.
	 *
	 \param $container_id(integer) Primary key of container
	 \param  $selectClose(array) to define the sql select option
	 */
	function GetInfos($container_id=NULL , $selectClose = ''){
		if(is_null($container_id)){
			if(!isset($this->container_id)){
				$this->error_stack->push(ERROR, 'Error', array(), '$this->container_id is not set');
				return false;}
				$container_id =& $this->container_id;
				$check=true;
		}
		$rset =& $this->GetBasicInfos($container_id , $selectClose);
		if($check)
		$this->core_prop = $rset;
		return $rset;
	}//End of method

	//---------------------------------------------------------------------
	/*!\brief
	 * This method can be used to get name from the id or one other field from $select
	 * Return false or a string
	 *
	 \param $id(integer) Primary key of container
	 */
	function GetName($id=NULL){
		if(is_null($id))
		return $this->GetProperty('container_number');
		return $this->GetBasicNumber($id);
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * Return the current container id
	 * else return false.
	 *
	 */
	function GetId(){
		if(is_null($this->container_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->container_id is not set');
			return false;
		}else return $this->container_id;
	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 * This method can be used to write a history record for container
	 * return true if success, false else.
	 *
	 \param $container_id(integer) Primary key of container
	 */
	function WriteHistory(){
		if(!isset($this->container_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->container_id is not set');
			return false;}
			if(!isset($this->history[$this->FIELDS_MAP_NUM]))
			$this->history[$this->FIELDS_MAP_NUM] = $this->GetProperty('container_number');
			//Write history
			$this->history[$this->FIELDS_MAP_ID] = $this->container_id;
			$ohistory = new history($this);
			return $ohistory->write($this->history);
	}//End of method

	//---------------------------------------------------------------------------
	/*!\brief
	 * Get all doctypes linked to container
	 * Return false if error or an array with the doctype properties
	 *
	 \param $extension(string) limit the result to doctype with extension $extension.
	 \param $type(string) limit the result to doctype with type $type.
	 \param $params(array) is use for manage the display. See parameters function of GetQueryOptions().
	 */
	function GetDoctypes($extension='' , $type = '' , $params=array()){
		if(!isset($this->container_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->container_id is not set');
			return false;}

			require_once('class/doctypeProcessLink.php');
			$doctypeProcessLink = new doctypeProcessLink($this->space);
			return $doctypeProcessLink->GetDoctypes($extension, $type, $this->container_id, $params );

			/*
			 if(!isset($this->container_id)){
			 $this->error_stack->push(ERROR, 'Error', array(), '$this->container_id is not set');
			 return false;}
			 if(!empty($this->container_id)){
			 $params['exact_find'][$this->FIELDS_MAP_ID] = $this->container_id;
			 }else{
			 $this->error_stack->push(ERROR, 'Fatal', array(), 'none selected container!!!');
			 return false; }
			 //Get only the doctype with the correct file extension
			 if(!empty($extension))
			 $params['find']['file_extension'] = "$extension";
			 if(!empty($type))
			 $params['find']['file_type'] = "$type";
			 $this->GetQueryOptions($params);
			 $linkTable  = $this->SPACE_NAME .'_doctype_process';
			 //Junction of tables doctypes and link_table
			 $query = "SELECT $this->selectClose FROM $linkTable JOIN doctypes
			 ON $linkTable.doctype_id = doctypes.doctype_id
			 $this->whereClose
			 $this->orderClose
			 ";
			 if(!$links = $this->dbranchbe->Execute($query)){
			 $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
			 return false;
			 }
			 $links = $links->GetArray(); //To transform object result in array;
			 foreach($links as $key => $value)
			 $links[$key]['object_type'] = 'doctype'; //Add the type of object to each key
			 return $links;
			 */
	}//End of method

	//----------------------------------------------------------
	/*!\brief
	 * Get the process linked to the doctype for the container
	 *
	 \param $doctype_id(integer) Primary key of doctype.
	 */
	function GetDoctypesProcess($doctype_id){
		if(!isset($this->container_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->container_id is not set');
			return false;}

			require_once('class/doctypeProcessLink.php');
			$doctypeProcessLink = new doctypeProcessLink($this->space);
			return $doctypeProcessLink->GetDoctypesProcess($doctype_id, $this->container_id);

			/*
			 if(!isset($this->container_id)){
			 $this->error_stack->push(ERROR, 'Error', array(), '$this->container_id is not set');
			 return false;}
			 $linkTable  = $this->SPACE_NAME.'_doctype_process';
			 $params['exact_find'][$linkTable.'.'.'doctype_id'] = $doctype_id;
			 $params['select'] = array('process_id', 'link_id');
			 $res = $this->GetDoctypes(NULL, NULL, $params);
			 if(!empty($res[0]['process_id'])) return $res[0]['process_id'];
			 return false;
			 */
	}//End of method

	//----------------------------------------------------------
	/*!\brief
	 * Get all metadatas linked to container
	 * Return a array with properties of the metadatas
	 */
	function GetMetadata( $params=array() ){
		require_once('./class/common/metadata.php');
		$metadata = new contmetadata($this->space);
		$metadatas = $metadata->GetMetadata( $params );
		if( is_array($metadatas) )
		foreach($metadatas as $meta){
			$this->FIELDS[ $meta['field_name'] ] = $this->OBJECT_TABLE.'.'.$meta['field_name'];
		}
		return $metadatas;
	}//End of method

	//----------------------------------------------------------
	/*!\brief
	 * Check if there is at least one doctype linked to container
	 * Return false or true
	 */
	function HasDoctype(){
		/*
		 if(!isset($this->container_id)){
		 $this->error_stack->push(ERROR, 'Error', array(), '$this->container_id is not set');
		 return false;}
		 $linkTable  = $this->SPACE_NAME .'_doctype_process';
		 $query= 'SELECT doctype_id FROM '.$linkTable.'
		 WHERE '.$this->FIELDS_MAP_ID.' = '.$this->container_id;
		 if(!$links = $this->dbranchbe->GetOne($query)){
		 $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
		 return false;
		 }
		 if(empty($links)) return false;
		 return true;
		 */
		if(!isset($this->container_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->container_id is not set');
			return false;}
			$linkTable  = $this->SPACE_NAME .'_doctype_process';
			$query= 'SELECT doctype_id FROM '.$linkTable.'
           WHERE '.$this->FIELDS_MAP_ID.' = '.$this->container_id;
			$one = $this->dbranchbe->GetOne($query);
			if($one === false) return false;
			if(empty($one))return false;
			return true;
	}//End of method

	//----------------------------------------------------------
	/*!\brief
	 * Link a doctype to a container
	 *
	 \param $container_id(integer) Primary key of container.
	 \param $doctype_id(integer) Primary key of doctype to link.
	 \param $process_id(integer) Primary key of process to link to container for the doctype.
	 */
	function LinkDoctype($doctype_id , $process_id=NULL){
		if(!isset($this->container_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->container_id is not set');
			return false;}

			$linkTable  = $this->SPACE_NAME .'_doctype_process';

			$this->dbranchbe->StartTrans();

			$id = $this->dbranchbe->GenID("$linkTable".'_seq', 1); //Increment the sequence number
			$data['link_id'] = $id;
			$data[$this->FIELDS_MAP_ID] = $this->container_id;
			$data['doctype_id'] = $doctype_id;
			$data['process_id'] = $process_id;

			if(!$this->dbranchbe->AutoExecute( "$linkTable" , $data , 'INSERT'))
			$this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>'INSERT' . implode(';',$data) , 'debug'=>array($data, $linkTable)), $this->dbranchbe->ErrorMsg());

			if ( $this->dbranchbe->CompleteTrans() === false ){
				return false;
			}

			return $id;
	}//End of method
	
	
	//----------------------------------------------------------
	/**
	 * 
	 *
	 */
	function move( $toPath ){
		if(!isset($this->container_id)){
			$this->error_stack->push(ERROR, 'Error', array(), '$this->container_id is not set');
			return false;
		}

		$fromPath = $this->GetProperty('default_file_path');
		$fromPath = str_replace('/__imported', '', $fromPath );
		
		if( empty($fromPath) || empty($toPath) ){
			$this->error_stack->push(ERROR, 'Error', array(), 'invalid path. fromPath:' . $fromPath .' , toPath: '. $toPath);
			return false;
		}
		
		/*Check that path is not existing*/
		if(is_dir($toPath)){
			$this->error_stack->push(ERROR, 'Error', array(), 'Target path is existing ' . $toPath);
			return false;
		}
		
		/*update file paths*/
		$this->dbranchbe->StartTrans();
		
		$table = $this->space->SPACE_NAME . '_files';
		$sql = "UPDATE $table SET file_path = REPLACE (file_path,'$fromPath','$toPath') WHERE $this->FIELDS_MAP_ID = $this->container_id";
		$ok = $this->dbranchbe->execute($sql);
		
		if(!$ok){
			$this->error_stack->push( ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg() );
			$this->dbranchbe->CompleteTrans(false);
			return false;
		}

		$table = $this->space->SPACE_NAME . '_import_history';
		$sql = "UPDATE $table SET file_path = REPLACE (import_errors_report,'$fromPath','$toPath') WHERE $this->FIELDS_MAP_ID = $this->container_id";
		$ok = $this->dbranchbe->execute($sql);
		
		if(!$ok){
			$this->error_stack->push( ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg() );
			$this->dbranchbe->CompleteTrans(false);
			return false;
		}
		
		$sql = "UPDATE $table SET file_path = REPLACE (import_logfiles_file,'$fromPath','$toPath') WHERE $this->FIELDS_MAP_ID = $this->container_id";
		$ok = $this->dbranchbe->execute($sql);
		
		if(!$ok){
			$this->error_stack->push( ERROR_DB, 'Fatal', array(), $this->dbranchbe->ErrorMsg() );
			$this->dbranchbe->CompleteTrans(false);
			return false;
		}
		
		/*Move directory*/
		$ok = rename($fromPath, $toPath);
		
		if(!$ok){
			$this->error_stack->push(ERROR, 'Error', array(), 'Unable to rename repsosit directory ' . $fromPath . ' to ' . $toPath);
			$this->dbranchbe->CompleteTrans(false);
			return false;
		}
		
		if ( $this->dbranchbe->CompleteTrans() === false ){
			$this->error_stack->push(ERROR, 'Error', array(), 'Error during transaction validation');
			return false;
		}
		
		return true;
	}//End of method
	

	//----------------------------------------------------------
	/*!\brief
	 * Link a process to doctype and container
	 *
	 \param $link_id(integer) Primary key of the link.
	 \param $process_id(integer) Primary key of process to link to container for the doctype.
	 */
	function LinkDoctypesProc($link_id , $process_id){
		$linkTable  = $this->SPACE_NAME .'_doctype_process';
		$data['process_id'] = $process_id;
		if (!$this->dbranchbe->AutoExecute( "$linkTable" , $data , 'UPDATE' , "link_id = $link_id" )){
			$this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>'UPDATE' . implode(';',$data) , 'debug'=>array($data, $linkTable, "link_id = $link_id")), $this->dbranchbe->ErrorMsg());
			return false;
		}
		return $id;
	}//End of method

	//----------------------------------------------------------
	/*!\brief
	 * Suppress a link between process/doctype for a container
	 *
	 \param $link_id(integer) Primary key of the link.
	 */
	function SuppressDoctypeProcLink($link_id){
		return $this->LinkDoctypesProc($link_id , NULL);
	}//End of method

	//----------------------------------------------------------
	/*!\brief
	 * Suppress a link between doctype and a container
	 *
	 \param $link_id(integer) Primary key of the link.
	 */
	function SuppressDoctypeLink($link_id){
		if (empty($link_id)){
			$this->error_stack->push(ERROR, 'Fatal', array(), 'none selected link');
			return false;}
			$linkTable  = "$this->SPACE_NAME" . '_doctype_process';
			$query = "DELETE FROM $linkTable WHERE link_id = '$link_id'";
			//Update the object data
			if ( $this->dbranchbe->Execute("$query") === false ) {
				$this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
				return false;
			}
			return true;
	}//End of method

	//-------------------------------------------------------------------------
	/*!\brief
	 * Record selected container id/number/type and eventualy his project id/number in session.<br>
	 *
	 *  Create var in session :<br>
	 *  SelectedContainer : id of the current container<br>
	 *  SelectedContainerNum : Number of the current container<br>
	 *  SelectedContainerType : Type of the current container(workitem, bookshop, cadlib, mockup)<br>
	 *  SelectedProject : id of the father project if current container is a workitem<br>
	 *  SelectedProjectNum : number of the father project if current container is a workitem<br>
	 *  area_id : id of the functionnal area. Use by LiveUser to set permissions<br>
	 *  objectList : List of the objects linked to current project(only if selected container is type workitem)<br>
	 *  projectInfos : Infos about current project(only if selected container is type workitem)<br>
	 */
	function RecordSelectedContainer(){
		//    See ranchbe_setup too for assign the container num to smarty.

		if ( $_SESSION['SelectedContainerType'] != $this->SPACE_NAME ){ //init the space name
			unset($_SESSION['SelectedContainer']);
			unset($_SESSION['SelectedContainerType']);
			unset($_SESSION['SelectedContainerNum']);
		}

		if ( !empty($_REQUEST['SelectedContainer']) && $_REQUEST['SelectedContainer'] != $_SESSION['SelectedContainer'] ){
			$_REQUEST['resetf'] = true; //For reinit filter record in session

			if( $project_id = $this->GetFatherProject() ){
				require_once './class/project.php'; //Class to manage the projects
				$projectsManager = new project($this->space, $project_id); //Create new manager

				//Get infos about project
				$projectInfos = $projectsManager->GetBasicInfos($project_id);
				//var_dump($projectInfos);

				//Get object linked to project with detail on childs objects
				$objectList = $projectsManager->GetLinkChildsDetail($project_id);
				//var_dump($objectList);

				//session_unregister ('links');
				unset($_SESSION['links']);

				if($objectList){
					foreach ($objectList as $object){
						//$_SESSION['links'][$object['object_type']][] = $object;
						$_SESSION['links'][$object['object_class']][] = $object;
					}
				}

				$_SESSION['SelectedProject']    = $projectInfos['project_id'];
				$_SESSION['SelectedProjectNum'] = $projectInfos['project_number'];
				$_SESSION['area_id']            = $projectInfos['area_id'];
				$_SESSION['objectList']         = $objectList;
				$_SESSION['projectInfos']       = $projectInfos;
			}else{
				unset($_SESSION['SelectedProject']);
				unset($_SESSION['SelectedProjectNum']);
				unset($_SESSION['area_id']);
				unset($_SESSION['links']);
				unset($_SESSION['objectList']);
				unset($_SESSION['projectInfos']);
			}

			$_SESSION['SelectedContainer']   = $_REQUEST['SelectedContainer']; //Record id in session variable
			$this->container_id = $_REQUEST['SelectedContainer'];
			if( !empty($_REQUEST['SelectedContainerNum']) )
			$_SESSION['SelectedContainerNum'] = $_REQUEST['SelectedContainerNum']; //Record name in session variable
			else
			$_SESSION['SelectedContainerNum'] = $this->GetProperty('container_number'); //Record name in session variable
			$_SESSION['SelectedContainerType'] = "$this->SPACE_NAME"; //Record type in session variable
		}else{
			if( empty($_SESSION['SelectedContainerNum']) ){
				$this->error_stack->push(ERROR, 'Fatal', array(), 'none selected container' );
				die;
			}
		}
	}//End of method

	//----------------------------------------------------------
	/*! \brief Get the father project.
	 *  Return the project id, else return false.
	 *
	 \param $workitem_id[maxRecords](integer) id of the workitem.
	 */
	function GetFatherProject(){
		return false;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Define the father project of the container.
	 *
	 \param $project_id(integer) Id of the project.
	 \param $workitem_id(integer) Id of the container.
	 */
	//function LinkProject($project_id , $container_id){
	function LinkProject($project_id){
		return false;
	}//End of method

}//End of class

