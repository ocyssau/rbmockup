<?php

require_once('class/common/filesystem.php');
require_once('class/interface/fsdatai.php');

/*! \brief represent a file on the filesystem
 *
 */
class file extends filesystem implements fsdatai{

	protected $file; //(string) full path of the file
	protected $file_id; //(string) path and full name of the file.
	protected $file_class;

	protected $file_props; //(array) content all properties of the file.
	protected $file_name; //(string) Name of the file 'file.ext'
	protected $file_path; //(string) path to the file '/dir/sub_dir'
	protected $file_extension; //(string) extension of the file '.ext'
	protected $file_root_name; //(string) root name of the file 'file'
	protected $file_size; //(integer) Size of the file in octets
	protected $file_mtime; //(integer) modification time of the file
	protected $file_type; //(string) type of file = file, adrawc5, adrawc4, camu, cadds4, cadds5, pstree...
	protected $file_md5; //(string) md5 code of the file
	protected $file_version; //(integer) version of the file

	public $displayMd5 = false; //true if you want return the md5 property of the file

	//----------------------------------------------------------
	function __construct($file){
		$this->file = $file;
		$this->file_props['file_type'] = 'file';
		$this->file_type =& $this->file_props['file_type'];
		global $error_stack;
		$this->error_stack =& $error_stack;
	}//End of method

	//----------------------------------------------------------
	/*! \brief Get the property of the document by the property name. init() must be call before
	 *
	 */
	function getProperty($property_name){
		if(!is_file($this->file))
	 return false;
	 switch ($property_name){
	 	case 'natif_file_name':
	 	case 'file_name':
	 		if(isset($this->file_props['file_name'])) return $this->file_props['file_name'];
	 		$this->file_props['file_name'] = basename($this->file);
	 		return $this->file_name =& $this->file_props['file_name'];
	 		break;

	 	case 'file_size':
	 		if(isset($this->file_props['file_size'])) return $this->file_props['file_size'];
	 		$this->file_props['file_size'] = filesize($this->file);
	 		return $this->file_size =& $this->file_props['file_size'];
	 		break;

	 	case 'file_path':
	 		if(isset($this->file_props['file_path'])) return $this->file_props['file_path'];
	 		$this->file_props['file_path'] = dirname($this->file);
	 		return $this->file_path =& $this->file_props['file_path'];
	 		break;

	 	case 'file_version':
	 		if(isset($this->file_props['file_version'])) return $this->file_props['file_version'];
	 		return false;
	 		break;

	 	case 'file_mtime':
	 		if(isset($this->file_props['file_mtime'])) return $this->file_props['file_mtime'];
	 		$this->file_props['file_mtime'] = filemtime($this->file);
	 		return $this->file_mtime =& $this->file_props['file_mtime'];
	 		break;

	 	case 'file_extension':
	 		if(isset($this->file_props['file_extension'])) return $this->file_props['file_extension'];
	 		$this->file_props['file_extension'] = file::GetExtension($this->file);
	 		return $this->file_extension =& $this->file_props['file_extension'];
	 		break;

	 	case 'doc_name':
	 	case 'file_root_name':
	 		if(isset($this->file_props['file_root_name'])) return $this->file_props['file_root_name'];
	 		$this->file_props['file_root_name'] = $this->getRoot();
	 		return $this->file_root_name =& $this->file_props['file_root_name'];
	 		break;

	 	case 'file_type':
	 		return $this->file_props['file_type'];
	 		break;

	 	case 'file':
	 		return $this->file_props['file'] =& $this->file;
	 		break;

	 	case 'file_md5':
	 		if(isset($this->file_props['file_md5'])) return $this->file_props['file_md5'];
	 		$this->file_props['file_md5'] = md5_file($this->file);
	 		return $this->file_md5 =& $this->file_props['file_md5'];
	 		break;

	 	case 'file_mimetype':
	 		if(isset($this->file_props['file_mimetype'])) return $this->file_props['file_mimetype'];
	 		$this->getMimeTypeInfos();
	 		return $this->file_mimetype;
	 		break;

	 	case 'visu2D_extension':
	 		if(isset($this->file_props['visu2D_extension'])) return $this->file_props['visu2D_extension'];
	 		$this->getMimeTypeInfos();
	 		return $this->visu2D_extension;
	 		break;

	 	case 'visu3D_extension':
	 		if(isset($this->file_props['visu3D_extension'])) return $this->file_props['visu3D_extension'];
	 		$this->getMimeTypeInfos();
	 		return $this->visu3D_extension;
	 		break;

	 	case 'no_read':
	 		if(isset($this->file_props['no_read'])) return $this->file_props['no_read'];
	 		$this->getMimeTypeInfos();
	 		return $this->no_read;
	 		break;

	 	case 'viewer_driver':
	 		if(isset($this->file_props['viewer_driver'])) return $this->file_props['viewer_driver'];
	 		$this->getMimeTypeInfos();
	 		return $this->viewer_driver;
	 		break;

	 	case 'reposit_sub_dir':
	 		if( isset( $this->reposit_sub_dir ) ) return $this->reposit_sub_dir;
	 		break;

	 } //End of switch
	}//End of method

	//-------------------------------------------------------
	/*! \brief return the infos record in the conf/mimes.csv file about the extension of the $filename.
	 *
	 * \param $filename(string) Full path to file.
	 */
	private function getMimeTypeInfos() {

		$mimefile = "./conf/mimes.csv";
		//$file_extension=$this->file_extension;
		$this->getProperty('file_extension');

		$handle = fopen($mimefile, "r");

		while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
			//$mimetype = $data[0];
			//$description = $data[1];
			//$extension = $data[2];
			//$no_read = $data[3];
			//viewer_driver = $data[4];
			//$visu2D = $data[5]; experimental
			//$visu3D = $data[6]; experimental
			if (strstr($data[2] , $this->file_extension)){
				$this->file_props['file_mimetype'] = $data[0];
				$this->file_mimetype =& $this->file_props['file_mimetype'];
				$this->file_props['file_mimetype_description'] = $data[1];
				$this->file_mimetype_description =& $this->file_props['file_mimetype_description'];
				$this->file_props['no_read'] = $data[3];
				$this->no_read =& $this->file_props['no_read'];
				$this->file_props['viewer_driver'] = $data[4]; //Experimental
				$this->viewer_driver =& $this->file_props['viewer_driver'];
				//$this->file_props['visu2D_extension'] = $data[5]; //Experimental
				//$this->visu2D_extension =& $this->file_props['visu2D_extension'];
				//$this->file_props['visu3D_extension'] = $data[6]; //Experimental
				//$this->visu3D_extension =& $this->file_props['visu3D_extension'];
				return $data;
				break;
			}
		}
		fclose($handle);

		return false;
	}//End of method

	//-------------------------------------------------------
	/*! \brief Get infos about the file or the cadds part.
	 * Return a array if no errors, else return FALSE.
	 *
	 * \param $displayMd5(bool) true if you want return the md5 code of the file.
	 */
	function getInfos($displayMd5=true){
		//Be careful, never change key name of this array. Else the key name must be existing in all database files table
		if(is_file($this->file)){
			/*  $this->file_props = array('file_name'      => basename($this->file),
			 'file_size'      => filesize($this->file),
			 'file_path'      => dirname($this->file),
			 'file_version'   => $this->getProperty('file_version'),
			 'file_mtime'     => filemtime($this->file),
			 'file_extension' => file::getExtension($this->file),
			 'file_root_name' => file::getRoot(basename($this->file)),
			 'file_type'      => 'file',
			 'file'           => $this->file,
			 );
			 */
			$this->file_props = array('file_name'      => $this->getProperty('file_name'),
                            'natif_file_name'=> $this->getProperty('natif_file_name'),
                            'file_size'      => $this->getProperty('file_size'),
                            'file_path'      => $this->getProperty('file_path'),
                            'file_version'   => $this->getProperty('file_version'),
                            'file_mtime'     => $this->getProperty('file_mtime'),
                            'file_extension' => $this->getProperty('file_extension'),
                            'file_root_name' => $this->getProperty('file_root_name'),
                            'file_type'      => $this->getProperty('file_type'),
                            'file'           => $this->file,
			);

			if($displayMd5)
			$this->file_props['file_md5'] = md5_file($this->file);
			return $this->file_props;
		}else return false;
	}//End of method

	//-------------------------------------------------------
	/*! \brief Return file extension.(ie: /dir/file.ext, return '.ext')
	 *
	 * \param $file(string) File name.
	 */
	function getExtension(){
		return substr($this->file, strrpos($this->file, '.'));
	}//End of function

	//-------------------------------------------------------
	/*! \brief Return root name of file.(ie: /dir/file.ext, return 'file')
	 *
	 * \param $file(string) File name.
	 */
	function getRoot(){
		$file = $this->GetProperty('file_name');
		return substr($file, 0, strrpos($file, '.'));
	}//End of function

	//----------------------------------------------------------
	/*! \brief move the current file
	 *
	 * \param $dst(string) fullpath to new file
	 * \param $replace(bool) true for replace file
	 */
	function move($dst , $replace=false){
		if(!is_file($this->file)){
			$this->error_stack->push(ERROR, 'Error', array('element'=>$this->file, 'debug'=>array()), '%element% is not a file');
			return false;
		}

		if (!filesystem::limitDir($dst)){
			$this->error_stack->push(ERROR, 'Error', array('element'=>$dst, 'debug'=>array()), 'you have no right access on file %element%');
			return false;
		}

		if(!is_dir(dirname($dst))){
			$this->error_stack->push(ERROR, 'Error', array('element'=>dirname($dst), 'debug'=>array()), 'the directory %element% dont exist');
			return false;
		}

		//Copy file , and suppress file
		if($this->copy($dst , 0755 , $replace)){
			$this->suppress();
			$this->file = $dst;
			$this->file_props['file_name'] = basename($this->file);
			$this->file_props['file_path'] = dirname($this->file);
			$this->file_props['file_extension'] = file::getExtension($this->file);
			$this->file_props['file_root_name'] = file::getRoot(basename($this->file));
			return true;
		}else return false;

	}//End of method

	//----------------------------------------------------------
	/*! \brief copy the current file
	 *
	 * \param $dst(string) fullpath to new file
	 * \param $mode(integer) mode of the new file
	 * \param $replace(bool) true for replace existing file
	 */
	function copy($dst,$mode=0755,$replace=true){
		if(!filesystem::limitDir($this->file)){
			$this->error_stack->push(ERROR, 'Error', array('element'=>$this->file, 'debug'=>array()), 'copy error : you have no right access on file %element%');
			return false;
		}

		if(!filesystem::limitDir($dst)){
			$this->error_stack->push(ERROR, 'Error', array('element'=>$dst, 'debug'=>array()), 'copy error : you have no right access on file %element%');
			return false;
		}

		if(is_file($dst) && !$replace){
			$this->error_stack->push(ERROR, 'Error', array('element'=>$dst, 'debug'=>array()), 'copy error: file %element% exist');
			return false;
		}

		if(is_file($this->file)){
			if(copy($this->file , $dst)){
				//Check md5
				$md5_src = md5_file($this->file);
				$md5_dst = md5_file($dst);
				if($md5_src != $md5_dst){
					$this->error_stack->push(ERROR, 'Error', array('src'=>$this->file, 'target'=>$dst ), 'Md5 code do not match for file %src% and %target%');
					return false;
				}
				$this->error_stack->push(LOG, 'Info', array('src'=>$this->file, 'target'=>$dst ), 'No error on copy of %src% to %target%');
				touch($dst, filemtime($this->file));
				chmod ($dst , $mode);
				return true;
			}else{
				$this->error_stack->push(ERROR, 'Error', array('src'=>$this->file, 'target'=>$dst ), 'copy error from file %src% to %target%');
				return false;
			}
		}else{
			$this->error_stack->push(ERROR, 'Error', array('src'=>$this->file), '%src% is not a plain file');
			return false;
		}

	}//End of method

	//----------------------------------------------------------
	/*! \brief suppress the current attachment file
	 *
	 * \param $dst(string) fullpath to new file
	 */
	function suppress(){
		if (!unlink($this->file)){
			$this->error_stack->push(ERROR, 'Error', array('file'=>$this->file, 'target'=>$trashDir ), 'error during suppressing file %file%');
			return false;
		}
		$this->error_stack->push(LOG, 'Info', array('file'=>$this->file), 'file %file% is suppressed');
		return true;
	}//End of method

	//-------------------------------------------------------------------------------
	/*! \brief put file in the trash dir
	 *   Returns TRUE or FALSE
	 */
	function putInTrash($verbose = false, $trashDir=DEFAULT_TRASH_DIR){
		//Check if trash exists and is writable
		if(!is_dir($trashDir) || !is_writable($trashDir)){
			print ' error : '. "$trashDir" . ' don\'t exist <br /> or is not writable';
			return false;
		}

		//Add original path to name of dstfile
		//$oriPath = $this->encodePath(dirname($dir));
		$oriPath = $this->encodePath(dirname($this->file));
		/*
		 $oriPath = str_replace( '/' , '%2F', "$oriPath" );
		 $oriPath = str_replace( '.' , '%2E', "$oriPath" );
		 $oriPath = str_replace( ':' , '%3A', "$oriPath" );
		 $oriPath = str_replace( '\\' , '%5C', "$oriPath" );
		 */

		//Check if there is not conflict name in trash dir else rename it
		$dstfile = $trashDir .'/'. "$oriPath" .'%&_'. basename($this->file);
		$i = 0;
		if(is_file($dstfile)){
			$Renamedstfile = $dstfile; //Rename destination dir if exist
			$path_parts = pathinfo($Renamedstfile);

			while(is_file($Renamedstfile)){
				$Renamedstfile = $path_parts['dirname'] .'/'. $path_parts['filename'] . '(' . $i . ')' . '.' . $path_parts['extension'];
				$i ++;
			}
			$dstfile = $Renamedstfile;
		}

		//Copy file to trash
		if (!$this->copy($dstfile)){ //copy file to dirtrash
			$this->error_stack->push(ERROR, 'Error', array('file'=>$this->file, 'target'=>$trashDir ), 'error copying file %file% to Trash %target%');
			//print 'error copying file '. $this->file . ' to Trash: '. $trashDir . ' <br />';
			return false;
		}else{ //Suppress original file
			chmod($dstfile , 0755);
			if (!unlink($this->file)){
				$this->error_stack->push(ERROR, 'Error', array('file'=>$this->file, 'target'=>$trashDir ), 'error during suppressing file %file%');
				//print 'info: error in suppressing file :'. $this->file . ' <br />';
				return false;
			}
		}

		$this->error_stack->push(LOG, 'Info', array('file'=>$this->file), 'file %file% has been put in trash');
		return true;

	}//End of function

	//-------------------------------------------------------------------
	function DownloadFile(){
		$fileName = $this->getProperty('file_name');
		$mimeType = $this->getProperty('file_mimetype');
		if($mimeType == 'no_read')
		return false;
		header("Content-disposition: attachment; filename=$fileName");
		header("Content-Type: " . $mimeType);
		header("Content-Transfer-Encoding: $fileName\n"); // Surtout ne pas enlever le \n
		header("Content-Length: ".filesize($this->file));
		header("Pragma: no-cache");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
		header("Expires: 0");
		readfile($this->file);
		return true; //no die, put it in fsdata::DownloadFile()
	}//End of method

	//-------------------------------------------------------
	/*! \brief Copy a upload file to $dstfile
	 Return true or false.
	 *
	 * Take the parameters of the uploaded file(from array $file) and create the file in the wildpspace.
	 \param $file(array) :
	 \param $file[name](string) name of the uploaded file
	 \param $file[type](string) mime type of the file
	 \param $file[tmp_name](string) temp name of the file on the server
	 \param $file[error](integer) error code if error occured during transfert(0=no error)
	 \param $file[size](integer) size of the file in octets
	 \param $replace(bool) if true and if the file exist in the wildspace, it will be replaced by the uploaded file.
	 \param $dstdir(string) Path of the dir to put the file.
	 */
	static function UploadFile($file , $replace, $dstdir){
		global $error_stack;
		$dstfile = $dstdir.'/'.$file['name'];
		if(!$replace) //Test if file exist in target dir only if replace=false
		if(is_file($dstfile)){
			$error_stack->push(ERROR, 'Fatal', array('element'=>$dstfile), '%element% file exist');
			return false;}
			if(!is_file($file['tmp_name'])){
				$error_stack->push(ERROR, 'Fatal', array('element'=>$file['tmp_name'], 'debug'=>array($file)), '%element% is not a file');
				return false;}
				if(copy($file['tmp_name'] , $dstfile)){
					chmod ($dstfile , 0775);
					return true;
				}else{
					$error_stack->push(ERROR, 'Fatal', array('element'=>$file['tmp_name'], 'debug'=>array($file['tmp_name'] , $dstfile)), 'can\'t copy file %element%');
					return false;}
	}//End of method

	//----------------------------------------------------------
	/*! \brief rename the current file
	 *
	 * \param $newName(string) fullpath to new file
	 */
	function rename($newName){
		$newName = trim($newName);

		if(!filesystem::limitDir($this->file)){
			$this->error_stack->push(ERROR, 'Error', array('element'=>$this->file, 'debug'=>array()), 'rename error : you have no right access on file %element%');
			return false;
		}

		if(!filesystem::limitDir($newName)){
			$this->error_stack->push(ERROR, 'Error', array('element'=>$newName, 'debug'=>array()), 'rename error : you have no right access on file %element%');
			return false;
		}

		if(empty($newName)){
			$this->error_stack->push(ERROR, 'Error', array('element'=>$newName, 'debug'=>array()), 'rename error: none specified new name');
			return false;
		}

		if(is_file($newName)){
			$this->error_stack->push(ERROR, 'Error', array('element'=>$newName, 'debug'=>array()), 'rename error: file %element% exist');
			return false;
		}

		if (!is_writeable($this->file)) {
			$this->error_stack->push(ERROR, 'Error', array('element'=>$this->file), 'rename error : file %element% is not writable');
			return false;
		}

		if (!@rename($this->file, $newName)) {
			$this->error_stack->push(ERROR, 'Error', array('element'=>$this->file,'newName'=>$newName), 'rename error : can not rename file %element% to %newName%');
			return false;
		}

		return true;

	}//End of method


} //End of class

?>
