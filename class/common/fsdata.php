<?php

require_once('./class/interface/fsdatai.php');

/*! \brief fsdata create a link between the database record of data (docfile or recordfile)
 * and the real data on the filesystem of the server like a file or a directory.
 *
 */
class fsdata implements fsdatai{

	protected $recordfile; //(object) father object
	protected $docfile; //(object) father object, alias of $recordfile
	protected $data; //(object) son object file, cadds, camu... linked
	protected $wildspace; //(object) wildspace of the current user
	protected $dataType; //(string) type of the fsdata: file, cadds, camu...
	protected $versions; //(array of fsdata objects) Array of the fsdata object of the old version for the current fsdata
	protected $dataPath; //(string) Full path to data
	protected $versionId; //(integer) version of current fsdata

	//----------------------------------------------------------
	function __construct($dataPath){
		global $error_stack;
		$this->error_stack =& $error_stack;

		global $logger;
		$this->logger =& $logger;

		$this->init($dataPath);
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief init the fsdata.
	 *  Try to init the data.
	 *  If no data on filesystem, set $this->dataType to false.
	 *  Return bool.
	 *
	 * \param $dataPath(string) full datapath to data
	 */
	function init($dataPath){
		if(isset($this->docfile)) unset($this->docfile);
		if(isset($this->recordfile)) unset($this->recordfile);
		if(isset($this->data)) unset($this->data);
		unset($this->versionId);
		$this->dataPath = $dataPath;

		if(!$this->data = self::_dataFactory($dataPath)){
			$this->error_stack->push(LOG, 'Error', array( 'file'=>$dataPath ),
     'fsdata %file% is not on filesystem');
			return $this->dataType = false;
		}else{
			$this->dataType =& $this->data->getProperty('file_type');
			return true;
		}

	}//End of method

	//--------------------------------------------------------
	/*!\brief return true if file is realy on file system
	 */
	function isExisting(){
		if ( !empty($this->dataPath) ){
			if ( $this->dataType !== false )
			return true;
		}
		return false;
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * Get the wildspace object
	 * return a wildspace instance if success, else fase.
	 * Set object $this->wildspace
	 */
	function initWildspace(){
		require_once('./class/wildspace.php');
		if($this->wildspace = new wildspace())
		return $this->wildspace;
		return false;
	}//End of method

	function GetWildspace(){
		if( isset($this->wildspace) ) return $this->wildspace;
		else return $this->initWildspace();
	}//End of method

	//----------------------------------------------------------
	/* Set type of a data. Return false or a array with "type" and "extension"*/
	static function SetDataType($dataPath){
		if(is_dir($dataPath)){
			//Test for adraw from datatype file
			if(is_file($dataPath.'/ranchbe.adraw')){
				if(is_file($dataPath.'/_fd')){
					return array('type'=>'adrawc5' , 'extension'=>'_fd');
				}
				if(is_file($dataPath.'/_pd')){
					return array('type'=>'adrawc4' , 'extension'=>'_pd');
				}
			}
			//Tests for cadds5 parts and adraw
			if(is_file($dataPath.'/_fd')){
				if(is_file($dataPath.'/../_db' )){//If parent dir is a camu, this part is a adraw
					touch ($dataPath.'/ranchbe.adraw');
					return array('type'=>'adrawc5' , 'extension'=>'_fd');
				}else{
					return array('type'=>'cadds5' , 'extension'=>'_fd');
				}
			}
			else if(is_file($dataPath.'/_pd')){ //Tests for cadds4 parts and adraw
				if(is_file($dataPath.'/../_db')){ //If parent dir is a camu, this part is a adraw
					touch ($dataPath.'/ranchbe.adraw');
					return array('type'=>'adrawc4' , 'extension'=>'_pd');
				}else{
					return array('type'=>'cadds4' , 'extension'=>'_pd');
				}
			}
			else if (is_file("$dataPath/_db"))//Tests for camu
			return array('type'=>'camu' , 'extension'=>'_db');
			else if (is_file("$dataPath/_ps"))//Tests for ps tree
			return array('type'=>'pstree' , 'extension'=>'_ps');
			else return false;

		}else if(is_file($dataPath)){
			$extension = substr($dataPath, strrpos($dataPath, '.'));

			if ($extension == '.adrawc5'){
				return array('type'=>'zipadrawc5' , 'extension'=>'.adrawc5');
			}
			if ($extension == '.adrawc4'){
				return array('type'=>'zipadrawc4' , 'extension'=>'.adrawc4');
			}
			if ($extension == '.adraw'){
				return array('type'=>'zipadraw' , 'extension'=>'.adraw');
			}
			return array('type'=>'file' , 'extension'=>$extension);

		}else{ //if data is not on filesystem or is other thing that file or dir
			$extension = substr($dataPath, strrpos($dataPath, '.'));
			if ($extension == '.adraw')
			return array('type'=>'adrawc4' , 'extension'=>'.adraw');
		}

		global $error_stack;
		$error_stack->push(LOG, 'Error', array( 'path'=>$dataPath ), 'unknow dataType or unreachable data for path : %path%');

		return false;
	}//End of function

	//------------------------------------------------------------------------------
	static function _dataFactory($dataPath){
		global $error_stack;
		$dataType = self::SetDataType($dataPath);
		$error_stack->push(LOG, 'Info', array('dataPath'=>$dataPath, 'type'=>$dataType['type']),
     'create fsdata type %type% from path : %dataPath%');
		switch($dataType['type']){
			case 'cadds5':
				require_once('./class/datatypes/cadds5.php');
				return new cadds5($dataPath);
				break;
			case 'cadds4':
				require_once('./class/datatypes/cadds4.php');
				return new cadds4($dataPath);
				break;

				/*
				 case 'adrawc4':
				 require_once('./class/datatypes/adrawc4.php');
				 return new adrawc4($dataPath);
				 break;
				 case 'adrawc5':
				 require_once('./class/datatypes/adrawc5.php');
				 return new adrawc5($dataPath);
				 break;
				 case 'zipadrawc4':
				 require_once('./class/datatypes/zipadraw.php');
				 return new zipadraw($dataPath);
				 break;
				 case 'zipadrawc5':
				 require_once('./class/datatypes/zipadraw.php');
				 return new zipadraw($dataPath);
				 break;
				 */

			case 'adraw':
			case 'adrawc4':
			case 'adrawc5':
				require_once('./class/datatypes/adraw.php');
				return new adraw($dataPath);
				break;

			case 'zipadraw':
			case 'zipadrawc4':
			case 'zipadrawc5':
				require_once('./class/datatypes/zipadraw.php');
				return new zipadraw($dataPath);
				break;

			case 'camu':
				require_once('./class/datatypes/camu.php');
				return new camu($dataPath);
				break;
			case 'pstree':
				require_once('./class/datatypes/pstree.php');
				return new pstree($dataPath);
				break;
			case 'file':
				require_once('./class/common/file.php');
				return new file($dataPath);
				break;
			default:
				return false;
				break;
		}
	}//End of function

	//--------------------------------------------------------
	/*!\brief
	 * Set the document by reference
	 *
	 \param $recordfile(object) object of the recordfile
	 */
	function SetRecordfile(recordfile &$recordfile){
		$this->docfile =& $recordfile;
		$this->recordfile =& $recordfile;
	}//End of method

	//--------------------------------------------------------
	/*!\brief
	 * Set the document by reference
	 *
	 \param $recordfile(object) object of the recordfile
	 */
	function GetData(){
		if( isset($this->data) ) return $this->data;
		else return false;
	}//End of method

	//-------------------------------------------------------
	/*! \brief Return infos on all version of a file.
	 Return false or a array with the filepath to the version file. Set the var $this->version[$i]
	 where $i is the version number and content the fsdata object of the version files
	 *
	 * Get all version file record in deposit dir for the file $this->file_id. This method read the versions file
	 * directly from the directories "version"/"version num" to retrieve the record version file(dont use the database).
	 */
	/*
	 function GetVersions(){
	 $current_version = $this->recordfile->GetProperty('file_version');
	 $i = $current_version;
	 while($i >= ($current_version - DEFAULT_BACK_VERSIONS_NUM)){
	 $versionDir = $this->getProperty('file_path').'/versions/'.$i;
	 if(is_dir($versionDir)){ //Get file in version dir
	 $versionFile = $versionDir.'/'.$this->getProperty('file_name');
	 if($fsdata = self::_dataFactory($versionFile)){
	 $fsdata->file_props['file_version'] = $i;
	 $this->versions[$i] = $fsdata;
	 }
	 }
	 $i -= 1;
	 } //End of while
	 return $this->versions;
	 }//End of method
	 */

	//-------------------------------------------------------
	/*! \brief Suppress a version file.
	 Return true or false.
	 *
	 \param $file_version(integer) Number of the version to suppress.
	 */
	/*
	 function RemoveVersion($file_version){
	 $i = $file_version;
	 $versionDir = $this->GetProperty('file_path').'/versions/'. $i;
	 if(is_dir($versionDir)){
	 $versionFile = $versionDir.'/'.$this->GetProperty('file_name');
	 if($fsdata = self::_dataFactory($versionFile)){
	 if($fsdata->putInTrash()) return true;
	 }
	 }
	 return false;
	 }//End of method
	 */

	//-------------------------------------------------------
	function getProperty($property_name){
		//$this->logger->log('fsdata::getProperty : call property '.$property_name);
		if($property_name === 'dataType'){
			return $this->dataType;
		}
		if($property_name === 'file_version'){
			return $this->versionId;
		}
		if(is_object($this->data)){
			return $this->data->getProperty($property_name);
		}
		if($property_name === 'file'){
			return $this->dataPath;
		}
		if($property_name === 'file_name'){
			return basename($this->dataPath);
		}
		if($property_name === 'file_path'){
			return dirname($this->dataPath);
		}
		$this->logger->log('fsdata::getProperty : call of undefined property '.$property_name);
		return 'undefined';
	}

	//-------------------------------------------------------
	/*! \brief Get infos about the file or the cadds part.
	 * Return a array if no errors, else return FALSE.
	 *
	 * \param $file(string) Full path to file.
	 * \param $displayMd5(bool) true if you want return the md5 code of the file.
	 */
	function getInfos($displayMd5=true){
		if(is_object($this->data)){
			$infos = $this->data->getInfos($displayMd5);
		}else{
			$infos = $this->fsdata_props;
		}
		$infos['dataType'] = $this->dataType;
		$infos['file_version'] = $this->versionId;
		return $infos;
	}

	//-------------------------------------------------------
	/*! \brief Return file extension.(ie: /dir/file.ext, return '.ext')
	 *
	 * \param $file(string) File name.
	 */
	function getExtension(){
		if( !$this->data ) return false;
		return $this->data->getExtension();
	}

	//-------------------------------------------------------
	/*! \brief Return root name of file.(ie: /dir/file.ext, return 'file')
	 *
	 * \param $file(string) File name.
	 */
	function getRoot(){
		if( !$this->data ) return false;
		return $this->data->getRoot();
	}

	//----------------------------------------------------------
	/*! \brief move the current file
	 *
	 * @param $dst(string) fullpath to new file
	 * @param $replace(bool) true for replace file
	 * @return boolean
	 */
	function move($dst, $replace=false){
		if( !is_object($this->data) ) return false;
		return $this->data->move($dst , $replace);
	}

	//----------------------------------------------------------
	/*! \brief copy the current file
	 *
	 * @param $dst(string) fullpath to new file
	 * @param $mode(integer) mode of the new file
	 * @param $replace(bool) true for replace existing file
	 * @return boolean
	 */
	function copy($dst, $mode=0755, $replace=true){
		if( !$this->data ) return false;
		return $this->data->copy($dst,$mode,$replace);
	}

	/**
	 * rename the current data
	 *
	 * @param string 	fullpath to new file
	 * @return boolean
	 */
	function rename( $dst ){
		if( !$this->data ) return false;
		return $this->data->rename( $dst );
	}


	//----------------------------------------------------------
	/*! \brief suppress the current attachment file
	 *
	 * \param $dst(string) fullpath to new file
	 */
	function suppress(){
		if( !$this->data ) return false;
		return $this->data->suppress();
	}

	//-------------------------------------------------------------------------------
	/*! \brief put file in the trash dir
	 *   Returns TRUE or FALSE
	 */
	function putInTrash($verbose = false, $trashDir=DEFAULT_TRASH_DIR){
		if( !$this->data ) return false;
		return $this->data->putInTrash($verbose, $trashDir);
	}

	//-------------------------------------------------------
	/*! \brief Put the file in the wildspace.
	 Return true or false.
	 *
	 \param $addPrefix(String) string to add before filename.
	 * The file is put with a default prefix "consult__"  to prevent lost of data.
	 */
	function putInWildspace($addPrefix=NULL, $replace=false){
		$this->initWildspace();
		$dstfile = $this->wildspace->GetPath().'/'.$addPrefix.$this->GetProperty('file_name');
		if(!$this->copy($dstfile , 0775, $replace)){
			$this->error_stack->push(ERROR, 'Fatal', array('element'=>$this->file), 'can\'t copy file %element%');
			return false;
		}
	}//End of method

	//-------------------------------------------------------------------------------
	/*! \brief Send the file to navigator
	 */
	function DownloadFile(){
		if( !$this->data ) return false;
		if( $this->data->DownloadFile() ) die;
	}

}//End of class

?>
