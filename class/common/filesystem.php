<?php

/*! \brief manipulation of the filesystem on the server
*
*/
class filesystem {

//----------------------------------------------------------
/*! \brief This function check if the request file is in a authorized directory
*   Return true if it is authorized else return false
* 
* \param $filepath(string) fullpath to test
*/
static function limitDir($filepath){
  //This var set the directories where ranchbe can manipulate files or directories (copy, suppress, mkdir... )
  $AUTHORIZED_DIR[] = DEFAULT_WORKITEM_DIR;
  $AUTHORIZED_DIR[] = DEFAULT_MOCKUP_DIR;
  $AUTHORIZED_DIR[] = DEFAULT_CADLIB_DIR;
  $AUTHORIZED_DIR[] = DEFAULT_BOOKSHOP_DIR;
  $AUTHORIZED_DIR[] = DEFAULT_IMPORT_DIR;
  $AUTHORIZED_DIR[] = UNPACK_IMPORT_DIR;
  $AUTHORIZED_DIR[] = DEFAULT_WILDSPACE_DIR;
  $AUTHORIZED_DIR[] = DEFAULT_DOCTYPE_SCRIPTS_DIR;
  $AUTHORIZED_DIR[] = DEFAULT_DATATYPE_SCRIPTS_DIR;
  $AUTHORIZED_DIR[] = DEFAULT_TRASH_DIR;
  $AUTHORIZED_DIR[] = USER_QUERIES_REPOSIT_DIR;
  
  $AUTHORIZED_DIR[] = DEFAULT_ARCHIVED_BOOKSHOP_DIR;
  $AUTHORIZED_DIR[] = DEFAULT_ARCHIVED_CADLIB_DIR;
  $AUTHORIZED_DIR[] = DEFAULT_ARCHIVED_MOCKUP_DIR;
  $AUTHORIZED_DIR[] = DEFAULT_ARCHIVED_WORKITEM_DIR;
  
  $i=0;
  while( defined('AUTHORIZED_DIR_'.$i) ){
    $AUTHORIZED_DIR[] = constant('AUTHORIZED_DIR_'.$i);
    $i++;
  }

  //global $AUTHORIZED_DIR;
  //".." , "//" "/./" forbidden in the path
  if (strpos("$filepath" , '..' ) ) {echo 'no ..<br/>';return false;}
  if (strpos("$filepath" , '//' ) ) {echo 'no //<br/>';return false;}
  if (strpos("$filepath" , '/./' ) ) {echo 'no /./<br/>';return false;}
  //Check that the path is in a authorized directory
  foreach ( $AUTHORIZED_DIR as $motif ){
    if (empty($motif)) continue;
    $motif = ltrim($motif , './');
    $filepath = ltrim($filepath , './');
    if (rtrim($filepath, '/') == rtrim($motif, '/') ) {echo 'its a sys dir<br/>';return false;} // Check that the system directories are not directly manipulated
    if (strpos("$filepath" , "$motif" ) === 0) return true; // Check that the file is in a directory of the system
  } //End of foreach
  return false;
}//End of function

//----------------------------------------------------------
/*! \brief Generation of uniq identifiant
*/
static function uniq_id(){
  return md5(uniqid(rand()));
}

//----------------------------------------------------------
/*! \brief Encode path
*/
static function encodePath($path){
  $path = str_replace( '/' , '%2F', $path );
  $path = str_replace( '.' , '%2E', $path );
  $path = str_replace( ':' , '%3A', $path );
  $path = str_replace( '\\', '%2F', $path );
  return $path;
}

//----------------------------------------------------------
/*! \brief Decode path
*/
static function decodePath($path){
  $path = str_replace( '%2F' , '/', $path );
  $path = str_replace( '%2E' , '.', $path );
  $path = str_replace( '%3A' , ':', $path );
  return $path;
}




} //End of class

?>
