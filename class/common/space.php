<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//TODO : Revoir utilit� des variables SPEC_FIELD, FIELDS...

/*! \brief This class manage the space: Set database parameters.
* space is use to define database fields name for each family of datas.
* space bookshop, cadlib, mockup, workitem have specific table and field name.
* Exemple : bookshops -> bookshop_id ; workitems -> workitem_id ; workitem_document ; bookshop_document...
* Properties record in space permit to define common methods to access to data in database.
*/
class space{

  public $SPACE_NAME; //(String)Name of the space : workitem, project, bookshop...etc.

//For the files
  //Tables
  public $DOC_FILE_TABLE; //(String)Name of the table where are stored the document files definition
  public $FILE_TABLE; //(String)Name of the table where are stored the files definition
  public $WILDSPACE; //(String)Name of the table where are stored the files definition
  protected $wildspace;

//Others
  public $DEFAULT_DEPOSIT_DIR; //(String)Path to the default deposit dir

  public $LINK_FILE_TABLE; //(String)Name of table where are stored the definition of links between files
  public $LINK_DOC_TABLE; //(String) Name of table where are stored the definition of links between documents

  public $SPEC_FIELDS; //(Array) ????
  public $FIELDS; //(Array) ????

  public $FIELDS_MAPPING; //(Array) To associate a description to the fields name
  public $AREA_ID; //(Integer) Id to used by liveuser to defined the permissions. See liveuser documentation.
  public $error_stack; //(Object errorManager)

//-----------------------------
//For the containers:
  //Tables:
  public $CONT_TABLE; //(String)Table where are stored the containers definitions
  public $CONT_HISTORY_TABLE; //(String)Table were are stored the container histories
  public $CONT_INDICE_TABLE; //(String)Table where are stored the container indices
  //public $CONT_METADATA_TABLE; //(String)Table where are stored the metadatas
  //Fields:
  public $CONT_FIELDS_MAP_ID; //(String)Name of the field of the primary key of the container table
  public $CONT_FIELDS_MAP_NUM; //(String)Name of the field to define the name of the container
  public $CONT_FIELDS_MAP_DESC; //(String)Name of the field where is store the description of the container
  public $CONT_FIELDS_MAP_STATE; //(String)Name of the field where is store the state of the container
  public $CONT_FIELDS_MAP_INDICE; //(String)Name of the field where is store the indice of the container
  public $CONT_FIELDS_MAP_DEPOSITDIR; //(String)Name of the field where is store the default deposit directory of the container
  public $CONT_FIELDS_MAP_FATHER; //(String)Name of the field where is store the id of the father

//For the documents
  //Tables
  public $DOC_TABLE; //(String)Name of the table where are stored the documents
  public $DOC_HISTORY_TABLE; //(String)Name of the table where are stored the documents histories
  public $DOC_INDICE_TABLE; //(String)Table where are stored the container indices
  //public $DOC_METADATA_TABLE; //(String)Table where are stored the metadatas
  //Fields
  public $DOC_FIELDS_MAP_ID; //(String)Name of the field of the primary key of the container table
  public $DOC_FIELDS_MAP_NUM; //(String)Name of the field to define the name of the container
  public $DOC_FIELDS_MAP_DESC; //(String)Name of the field where is store the description of the container
  public $DOC_FIELDS_MAP_STATE; //(String)Name of the field where is store the state of the container
  public $DOC_FIELDS_MAP_INDICE; //(String)Name of the field where is store the indice of the container
  public $DOC_FIELDS_MAP_DEPOSITDIR; //(String)Name of the field where is store the default deposit directory of the container
  public $DOC_FIELDS_MAP_FATHER; //(String)Name of the field where is store the id of the father
  public $DOC_TABLE_ID; //(String)Name of the field of the primary key of the document table

//----------------------------------------------------------
  function __construct($object_type){
    global $error_stack;
    $this->error_stack =& $error_stack;
    $this->SPACE_NAME=$object_type;
    global $usr;
    $this->CurrentUserName = $usr->getProperty('handle');
    $this->CurrentUserId   = $usr->getProperty('auth_user_id');
    switch($this->SPACE_NAME){
      case('bookshop'):
        $this->setCommonProperties();
        $this->setBookshop();
        break;
      case('cadlib'):
        $this->setCommonProperties();
        $this->setCadlib();
        break;
      case('mockup'):
        $this->setCommonProperties();
        $this->setMockup();
        break;
      case('workitem'):
        $this->setCommonProperties();
        $this->setWorkitem();
        break;
      case('wildspace'):
        $this->setWildspace();
        break;
/*      case('product'):
        $this->setProduct();
        break;*/
      case('project'):
        //$this->setProject();
        break;
      default:
        die('Space object : Unknow object type');
        break;
    }
  } //End of method

//----------------------------------------------------------
  /*! \brief 
  *  
  */
  private function setCommonProperties(){
  //Container
    //Tables
    $this->CONT_TABLE   = $this->SPACE_NAME.'s';
    $this->CONT_HISTORY_TABLE   = $this->SPACE_NAME.'_history';
    $this->CONT_INDICE_TABLE    =  'container_indice';
    //$this->CONT_METADATA_TABLE  = $this->SPACE_NAME.'_cont_metadata';
    //Fields
    $this->CONT_FIELDS_MAP_ID = $this->SPACE_NAME.'_id';
    $this->CONT_FIELDS_MAP_NUM = $this->SPACE_NAME.'_number';
    $this->CONT_FIELDS_MAP_DESC = $this->SPACE_NAME.'_description';
    $this->CONT_FIELDS_MAP_STATE = $this->SPACE_NAME.'_state';
    $this->CONT_FIELDS_MAP_INDICE = $this->SPACE_NAME.'_indice_id';
    $this->CONT_FIELDS_MAP_FATHER = 'project_id';

  //Document
    //Tables
    $this->DOC_TABLE   = $this->SPACE_NAME.'_documents';
    $this->DOC_HISTORY_TABLE   = $this->SPACE_NAME.'_documents_history';
    $this->DOC_INDICE_TABLE    =  'document_indice';
    //$this->DOC_METADATA_TABLE  = $this->SPACE_NAME.'_metadata';
    //Fields
    $this->DOC_FIELDS_MAP_ID = 'document_id';
    $this->DOC_FIELDS_MAP_NUM = 'document_number';
    $this->DOC_FIELDS_MAP_DESC = 'document_description';
    $this->DOC_FIELDS_MAP_STATE = 'document_state';
    $this->DOC_FIELDS_MAP_INDICE = 'document_indice_id';
    $this->DOC_FIELDS_MAP_FATHER = $this->CONT_FIELDS_MAP_ID;

  //Files
    $this->DOC_FILE_TABLE     = $this->SPACE_NAME.'_doc_files';
    $this->FILE_TABLE         = $this->SPACE_NAME.'_files';

    $this->LINK_FILE_TABLE = 'documents_files_links';
    $this->LINK_DOC_TABLE = 'documents_container_links';

    $this->READ_DIR = DEFAULT_READ_DIR;

    $this->setWildspace();
  }
  
//----------------------------------------------------------
  /*! \brief 
  *  
  */
  private function setWorkitem(){
    $this->AREA_ID = 10;
    $this->FIELDS_MAP_DEPOSITDIR = 'default_file_path';
    $this->DEFAULT_DEPOSIT_DIR = DEFAULT_WORKITEM_DIR;
    $this->FIELDS_MAP_FATHER = 'project_id';
    $this->FIELDS_MAPPING['project_number'] = 'Project';
    $this->FIELDS_MAPPING['default_file_path'] = 'Deposit dir';
  }
//----------------------------------------------------------
  /*! \brief 
  *  
  */
  private function setBookshop(){
    $this->AREA_ID = 20;
    $this->FIELDS_MAP_DEPOSITDIR = 'default_file_path';
    $this->DEFAULT_DEPOSIT_DIR = DEFAULT_BOOKSHOP_DIR;
    //$this->FIELDS_MAP_FATHER = 'project_id';
    //$this->FIELDS_MAPPING['project_number'] = 'Project';
    $this->FIELDS_MAPPING['default_file_path'] = 'Deposit dir';
  }
//----------------------------------------------------------
  /*! \brief 
  *  
  */
  private function setCadlib(){
    $this->AREA_ID = 25;
    $this->FIELDS_MAP_DEPOSITDIR = 'default_file_path';
    $this->DEFAULT_DEPOSIT_DIR = DEFAULT_CADLIB_DIR;
    //$this->FIELDS_MAP_FATHER = 'project_id';
    //$this->FIELDS_MAPPING['project_number'] = 'Project';
    $this->FIELDS_MAPPING['default_file_path'] = 'Deposit dir';
  }
//----------------------------------------------------------
  /*! \brief 
  *  
  */
  private function setMockup(){
    $this->AREA_ID = 15;
    $this->FIELDS_MAP_DEPOSITDIR = 'default_file_path';
    $this->DEFAULT_DEPOSIT_DIR = DEFAULT_MOCKUP_DIR;
    //$this->FIELDS_MAP_FATHER = 'project_id';
    //$this->FIELDS_MAPPING['project_number'] = 'Project';
    $this->FIELDS_MAPPING['default_file_path'] = 'Deposit dir';
  }
//----------------------------------------------------------
  /*! \brief 
  *  
  */
/*
  private function setDoctype(){
    $this->AREA_ID = 1;
    $this->FIELDS_MAP_DEPOSITDIR = 'default_file_path';
    $this->DEFAULT_DEPOSIT_DIR = DEFAULT_MOCKUP_DIR;
    //$this->FIELDS_MAP_FATHER = 'project_id';
    //$this->FIELDS_MAPPING['project_number'] = 'Project';
    $this->FIELDS_MAPPING['default_file_path'] = 'Deposit dir';
    $this->FIELDS = array('doctype_id', 'doctype_number', 'doctype_description', 'can_be_composite', 'script_post_store', 'script_pre_store', 'script_pre_update', 'script_post_update', 'recognition_regexp', 'file_extension', 'icon');
  }
*/
//----------------------------------------------------------
  /*! \brief 
  *  
  */
/*
  private function setPartner(){
    $this->AREA_ID = 1;
    $this->FIELDS_MAP_DEPOSITDIR = 'default_file_path';
    $this->DEFAULT_DEPOSIT_DIR = DEFAULT_MOCKUP_DIR;
    $this->FIELDS_MAPPING['default_file_path'] = 'Deposit dir';
    $this->FIELDS = array('partner_id', 'partner_number', 'first_name', 'last_name', 'adress', 'city', 'zip_code', 'phone', 'mail', 'web_site', 'activity' );
  }
*/
//----------------------------------------------------------
  /*! \brief 
  *  
  */
  private function setProcess(){
    $this->AREA_ID = 1;
    $this->SPACE_NAME  = 'process';
    $this->OBJECT_TABLE = 'galaxia_processes';
    $this->FIELDS_MAP_ID = 'pId';
    $this->FIELDS_MAP_NUM = 'name';
  }

//----------------------------------------------------------
  /*! \brief 
  *  
  */
  private function setProduct(){
    $this->AREA_ID = 1;
  }
//----------------------------------------------------------
  /*! \brief 
  *  
  */
/*
  private function setProject(){
    $this->AREA_ID = 5;
    $this->SPEC_FIELDS = array( 'project_id' , 'project_number' , 'project_state' , 'open_by' , 'forseen_close_date' , 'open_date' , 'close_date' , 'close_by' , 'project_description' , 'project_indice_id' , 'default_process_id', 'area_id' , 'project_indice_id' , 'default_process_id');
  }
*/

//----------------------------------------------------------
  /*! \brief construct the path to the personnal working directory for the current user.
  *  
  */
  private function setWildspace(){
    if(!isset ($this->wildspace) ){
      require_once('./class/wildspace.php');
      $this->wildspace = new wildspace();
    }
    return $this->WILDSPACE = $this->wildspace->GetPath();
  } //End of method

//----------------------------------------------------------
  /*! \brief get the wildspace
  *  
  */
  function getWildspace(){
    if( isset( $this->WILDSPACE ) ) return $this->WILDSPACE;
    else return $this->setWildspace();
  } //End of method

}//End of class

?>
