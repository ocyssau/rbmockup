<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
CREATE TABLE `bookshop_doc_rel` (
  `dr_link_id` int(11) NOT NULL default '0',
  `dr_document_id` int(11) default NULL,
  `dr_l_document_id` int(11) default NULL,
  `dr_access_code` int(11) default NULL,
  PRIMARY KEY  (`dr_link_id`),
  KEY `FK_bookshop_doc_rel_10` (`dr_document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `cadlib_doc_rel` (
  `dr_link_id` int(11) NOT NULL default '0',
  `dr_document_id` int(11) default NULL,
  `dr_l_document_id` int(11) default NULL,
  `dr_access_code` int(11) default NULL,
  PRIMARY KEY  (`dr_link_id`),
  KEY `FK_cadlib_doc_rel_10` (`dr_document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `mockup_doc_rel` (
  `dr_link_id` int(11) NOT NULL default '0',
  `dr_document_id` int(11) default NULL,
  `dr_l_document_id` int(11) default NULL,
  `dr_access_code` int(11) default NULL,
  PRIMARY KEY  (`dr_link_id`),
  KEY `FK_mockup_doc_rel_10` (`dr_document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE `workitem_doc_rel` (
  `dr_link_id` int(11) NOT NULL default '0',
  `dr_document_id` int(11) default NULL,
  `dr_l_document_id` int(11) default NULL,
  `dr_access_code` int(11) default NULL,
  PRIMARY KEY  (`dr_link_id`),
  UNIQUE KEY `uniq_docid_ldoc_id` (`dr_document_id`,`dr_l_document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- 
-- Contraintes pour la table `bookshop_doc_rel`
-- 
ALTER TABLE `bookshop_doc_rel`
  ADD CONSTRAINT `FK_bookshop_doc_rel_10` FOREIGN KEY (`dr_document_id`) REFERENCES `bookshop_documents` (`document_id`) ON DELETE CASCADE;

ALTER TABLE `cadlib_doc_rel`
  ADD CONSTRAINT `FK_cadlib_doc_rel_10` FOREIGN KEY (`dr_document_id`) REFERENCES `cadlib_documents` (`document_id`) ON DELETE CASCADE;

ALTER TABLE `mockup_doc_rel`
  ADD CONSTRAINT `FK_mockup_doc_rel_10` FOREIGN KEY (`dr_document_id`) REFERENCES `mockup_documents` (`document_id`) ON DELETE CASCADE;

ALTER TABLE `workitem_doc_rel`
  ADD CONSTRAINT `FK_workitem_doc_rel_10` FOREIGN KEY (`dr_document_id`) REFERENCES `workitem_documents` (`document_id`) ON DELETE CASCADE;

*/

require_once('./class/common/basic.php');

//-------------------------------------------------------------------------

/*! \brief This class manage doclink.
*
* A doclink define links betweens documents.
*/
class doclink extends basic{

  protected $OBJECT_TABLE; //(String)Table where are stored the containers definitions

  //Fields:
  protected $FIELDS_MAP_ID; //(String)Name of the field of the primary key

  protected $doclink_id; //(Integer) id of the current document

  protected $document; //(Object) main document

//--------------------------------------------------------------------
function __construct(&$document){
  $this->document =& $document;
  $this->space =& $document->space;
  $this->error_stack =& $this->space->error_stack;
  global $dbranchbe;
  $this->dbranchbe =& $dbranchbe;
  global $usr;
  $this->usr =& $usr;
  $this->OBJECT_TABLE  = $this->space->SPACE_NAME . '_doc_rel';

  $this->FIELDS_MAP_ID = 'dr_link_id';

}//End of method

//----------------------------------------------------------
/*!\brief
 * Link a document to one other. The link is oriented : father document to child document
 * Return the link_id of the new link if success, else false.
 *
\param $document_id(integer) Primary key of document (father).
\param $l_document_id(integer) Primary key of document to links (child).
*/
function AddSon($son_document_id){
  $data['dr_document_id'] = $this->document->GetId();
  $data['dr_l_document_id'] = $son_document_id;
  return $this->BasicCreate($data);
}//End of method

//----------------------------------------------------------
/*!\brief
 * Suppress a link between document
 * Return true or false   
 *
\param $link_id(integer) Primary key of the link.
*/
function SuppressSon($link_id){
  if (empty($link_id)){
    $this->error_stack->push(ERROR, 'Fatal', array(), 'none selected link' );
    return false;}
  return $this->BasicSuppress($link_id);
}//End of method

//----------------------------------------------------------
/*!\brief
 * Update a link between document
 * Return true or false   
 *
 * This method search in the _doc_rel table the document linked with id $l_document_id and replace it
 * by the the $_new_l_document_id
 * This function is used when the document is indice upgraded for always keeps the links.
 * The link is replace only if is not explicitly locked. A link is locked if the value of dr_access_code is > to 14.   
\param $son_id(integer) Id of the linked document.
\param $new_son_id(integer) New id of the linked document.
*/
function ReplaceDocLink($son_id , $new_son_id){
  
  $query = "UPDATE $this->OBJECT_TABLE
            SET `dr_l_document_id` = REPLACE (`dr_l_document_id`, $son_id, $new_son_id)
            WHERE `dr_l_document_id` = '$son_id'
            AND (`dr_access_code` < '15' OR `dr_access_code` IS NULL)
          ";

  //Update the object data
  if ($this->dbranchbe->Execute("$query") === false){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
    return false;
  }

  return true;

}//End of method

//----------------------------------------------------------
/*!\brief
 * Get documents linked to one document
 * Return a array with properties of the linked documents

\param $params(array) is use for manage the display. See parameters function of GetQueryOptions()
*/
function GetSons($params=array()){

  $params['exact_find']['dr.dr_document_id'] = $this->document->GetId();

	$this->GetQueryOptions($params);

  //Junction of tables doctypes and link_table
  $query = 'SELECT '.$this->selectClose.' FROM '.$this->OBJECT_TABLE.' as dr JOIN '.$this->space->DOC_TABLE.' as dt
            ON dr.dr_l_document_id = dt.document_id '.
            $this->whereClose.' '.
            $this->orderClose
            ;

  if(! $links = $this->dbranchbe->Execute($query) ){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
    return false;
  }else{
    $links = $links->GetArray(); //To transform object result in array;
    foreach ($links as $key => $value){
      $links[$key]['object_class'] = 'doclink'; //Add the type of object to each key
    }
    return $links;
  }

  return false;
  
}//End of method

//----------------------------------------------------------
/*!\brief
 * Get links of the document.
 * Return a array with links properties of the links.

\param $params(array) is use for manage the display. See parameters function of GetQueryOptions()
*/
function GetDocumentLinks($params=array()){
  $params['exact_find']['dr_l_document_id'] = $this->document->GetId();
  $this->GetQueryOptions($params);
  $query = "SELECT $this->selectClose FROM $this->OBJECT_TABLE
            $this->whereClose
            $this->orderClose
            ";
  if(!$links = $this->dbranchbe->Execute($query)){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg());
    return false;
  }else{
    $links = $links->GetArray(); //To transform object result in array;
    foreach ($links as $key => $value){
      $links[$key]['object_class'] = 'doclink'; //Add the type of object to each key
    }
    return $links;
  }
  return false;
}//End of method

//----------------------------------------------------------
/*!\brief
 * Get documents father links of one document
 * Return a array with properties of the linked documents
 * 
 * This method return a array with properties of documents calling by link the document with id document_id.
 *    
  \param $params(array) is use for manage the display. See parameters function of GetQueryOptions()
*/
function GetFathers($params=array()){

	$this->GetQueryOptions($params);

  //Junction of tables doctypes and link_table
  $query = 'SELECT '.$this->selectClose.' FROM '.$this->space->DOC_TABLE.' as dt JOIN '.$this->OBJECT_TABLE.' as dr
            ON dr.dr_document_id = dt.document_id 
            WHERE dr.dr_l_document_id = '.$this->document->GetId().' '.
            $this->whereClose.' '.
            $this->orderClose
            ;

  if(! $links = $this->dbranchbe->Execute($query) ){
    $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $this->dbranchbe->ErrorMsg() );
    return false;
  }else{
    $links = $links->GetArray(); //To transform object result in array;
    foreach ($links as $key => $value){
      $links[$key]['object_class'] = 'doclink'; //Add the type of object to each key
    }
    return $links;
  }
  return false;
  
}//End of method

//----------------------------------------------------------
/*!\brief
 * Copy the links of the document to the document t_document_id
 * Return false or true if success.
 * 
  \param $t_document_id(integer) target document id.
*/
function CopySons($t_document_id){
  $links = $this->GetSons();//Get links of the document
  foreach($links as $link){
    $data['dr_document_id'] = $t_document_id;
    $data['dr_l_document_id'] = $link['dr_l_document_id'];
    $this->BasicCreate($data);
  }
  return true;
}//End of method

//----------------------------------------------------------
/*!\brief
 * Lock all links of the document document_id with code access_code
 * When doclink is locked, he is no more following indice upgrade.
 * Return false or true if success.
 * 
  \param $code(integer) Code access of the lock
*/
function LockAll($code = 15){
  $data['dr_access_code'] = $code;
  if(!$this->dbranchbe->AutoExecute($this->OBJECT_TABLE , $data , 'UPDATE' , "dr_document_id = ".$this->document->GetId())) { //Update table
    $this->error_stack->push(ERROR, 'Fatal', array('query'=>'UPDATE' . implode(';',$data) , 'debug'=>array($data, $this->OBJECT_TABLE, $this->document->GetId())), $this->dbranchbe->ErrorMsg());
    return false;
  }
  return true;
}//End of method

//----------------------------------------------------------
/*!\brief
 * Lock one links with code access_code
 * Return false or true if success.
 * 
  \param $link_id(integer) id of link to lock
  \param $code(integer) Code access of the lock
*/
function LockDocLink($link_id,$code = 15){
  $data['dr_access_code'] = $code;
  if(!$this->dbranchbe->AutoExecute($this->OBJECT_TABLE , $data , 'UPDATE' , "dr_link_id = ".$link_id)) { //Update table
    $this->error_stack->push(ERROR, 'Fatal', array('query'=>'UPDATE' . implode(';',$data) , 'debug'=>array($data, $this->OBJECT_TABLE, $this->document->GetId())), $this->dbranchbe->ErrorMsg());
    return false;
  }
  return true;
}//End of method

}//End of class

//Dont let empty lines after the close tag... 
?>
