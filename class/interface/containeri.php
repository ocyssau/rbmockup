<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

interface containeri{

//-------------------------------------------------------------------------
/*! \brief init the property of the container
*  Return false or true
* 
* \param $container_id(integer) Id of the container
*/
function init($container_id);

//----------------------------------------------------------
/*! \brief Get all properties of the current container
*  Return false or array
*/
function GetProperties();

//----------------------------------------------------------
/*! \brief Get the property of the container by the property name. init() must be call before
*  Return false or string
* \param $property_name(string)
*/
function GetProperty($property_name);

//--------------------------------------------------------------------
/*!\brief This method can be used to create a container.
*   Return the id if no errors, else return FALSE
*
\param $params[number](integer) The number of the new container
\param $params[description](string) ..
\param $params[forseen_close_date](integer) ..
\param $params[default_process_id](integer) .. the default process id
\param $params[doctype_id](array) .. no-assoc array of all doctype_id permit for this container
\param $params[file_only](bool) true if the new container must manage file only
\param ...And all others metadatas create by users
*/
function Create($params);

//--------------------------------------------------------------------
/*!\brief This method can be used to suppress a container.
*   Return false or true
*/
function Suppress();

//--------------------------------------------------------------------
/*!\brief This method can be used to modify a container
*   Return false or true
*
\param $params = array "field-to-modify"=>"New-value"
*/
function Update($params);

//--------------------------------------------------------------------
/*!\brief
* This method can be used to get list of all container.
* return a array if success, else return false.
\param $params is use for manage the display. See parameters function of GetQueryOptions()
*/
function GetAll($params);

//--------------------------------------------------------
/*!\brief
* This method can be used to get list of all document from a container.
* return a array if success, else return false.
\param $params(array) is use for manage the display. See parameters function of GetQueryOptions()
\param $displayHistory(bool) true the display the previous indice of the documents
*/
function GetAllDocuments($params, $displayHistory=false);

//--------------------------------------------------------------------
/*!\brief
* This method can be used to get info about a single container.
* return a array if success, else return false.
*   
\param $container_id(integer) Primary key of container
\param  $selectClose(array) to define the sql select option
*/
function GetInfos($container_id=NULL , $selectClose = '');

//---------------------------------------------------------------------
/*!\brief
* This method can be used to get name from the id or one other field from $select
* return a array if success, else return false.
*   
\param $id(integer) Primary key of container
*/
function GetName($id=NULL);

//---------------------------------------------------------------------
/*!\brief
* Get id from name
* return a array if success, else return false.
*   
\param $name(string) container name
*/
function GetId($name);

//---------------------------------------------------------------------------
/*!\brief
* This method can be used to write a history record for container
* return true if success, false else.
*
\param $container_id(integer) Primary key of container
*/
function WriteHistory();

//---------------------------------------------------------------------------
/*!\brief
* Get all doctypes linked to container
* Return array if success, false else.
*
\param $extension(string) limit the result to doctype with extension $extension.
\param $type(string) limit the result to doctype with type $type.
\param $params(array) is use for manage the display. See parameters function of GetQueryOptions().
*/
function GetDoctypes($extension='' , $type = '' , $params=array());

//----------------------------------------------------------
/*!\brief
* Get the process linked to the doctype for the container
*
\param $doctype_id(integer) Primary key of doctype.
*/
function GetDoctypesProcess($doctype_id);

//----------------------------------------------------------
/*!\brief
* Get all metadatas linked to container
* Return a array with properties of the metadatas
* 
\param $params(array) is use for manage the display. See parameters function of GetQueryOptions().
*/
function GetMetadata($params=array());

//----------------------------------------------------------
/*!\brief
* Check if there is at least one doctype linked to container
* Return false or true
*/
function HasDoctype();

//----------------------------------------------------------
/*!\brief
* Link a doctype to a container
* Return false or the new link id(integer)
*
\param $doctype_id(integer) Primary key of doctype to link.
\param $process_id(integer) Primary key of process to link to container for the doctype.
*/
function LinkDoctype($doctype_id , $process_id=NULL);

//----------------------------------------------------------
/*!\brief
* Link a process to doctype and container
* Return false or the link id(integer)
*
\param $link_id(integer) Primary key of the link.
\param $process_id(integer) Primary key of process to link to container for the doctype.
*/
function LinkDoctypesProc($link_id , $process_id);

//----------------------------------------------------------
/*!\brief
* Suppress a link between process/doctype for a container
* Return false or true
*
\param $link_id(integer) Primary key of the link.
*/
function SuppressDoctypeProcLink($link_id);

//----------------------------------------------------------
/*!\brief
 * Suppress a link between doctype and a container
 *
\param $link_id(integer) Primary key of the link.
*/
function SuppressDoctypeLink($link_id);

//----------------------------------------------------------
/*! \brief Get the father of the current object
*  Return the father id, else return false.
*    
*/
function GetFather();

}//End of class

//Dont let more than one empty lines after the close tag...
?>
