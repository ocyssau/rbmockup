<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('./class/common/basic.php');

/*
CREATE TABLE `workitem_doctype_process` (
`link_id` int(11) NOT NULL default '0',
`doctype_id` int(11) default NULL,
`process_id` int(11) default NULL,
`workitem_id` int(11) default NULL,
`category_id` int(11) default NULL,
PRIMARY KEY  (`link_id`),
KEY `FK_workitem_doctype_process_1` (`doctype_id`),
KEY `FK_workitem_doctype_process_3` (`category_id`),
KEY `FK_workitem_doctype_process_4` (`workitem_id`)
) ENGINE=InnoDB ;
*/

/*! \brief This class manage the doctypes.
* A doctype type the document and can be use to restrict the file name, file extension, and file type
* and defined particular action to apply on document respecting a name convention
*/
class doctypeProcessLink extends basic{

function __construct(space &$space, $link_id=NULL){
  global $dbranchbe;
  $this->dbranchbe =& $dbranchbe;
  $this->space =& $space;
  $this->error_stack =& $this->space->error_stack;
  
  $this->OBJECT_TABLE  = $this->space->SPACE_NAME.'_doctype_process';
  $this->FIELDS_MAP_ID = 'link_id';

  $this->RIGHT_OBJECT_TABLE = 'doctypes';
  $this->RIGHT_FIELDS_MAP_ID = 'doctype_id';
  
  $this->FIELDS = array();
  
  if(!empty($link_id)){
    $this->init($link_id);
  }
  
}//End of method

//--------------------------------------------------------------------
function init($link_id){
  $this->link_id = $link_id;
  if(isset($this->core_prop)) unset($this->core_prop);
  return true;
}//End of method

//----------------------------------------------------------
/*!\brief
* Link a doctype to a container
*
\param $container_id(integer) Primary key of doctype to link.
\param $doctype_id(integer) Primary key of doctype to link.
\param $process_id(integer) Primary key of process to link to container for the doctype.
\param $category_id(integer) Primary key of category to link to container for the doctype.
*/
function AddLink($params,$link_id=NULL){
  $data = $params;
  if( !empty($params['container_id']) )
    $data[$this->space->CONT_FIELDS_MAP_ID] = $params['container_id'];

  if( empty($link_id) && !empty($this->link_id) ) $link_id = $this->link_id;

  if( !empty($link_id) )
    return $this->BasicUpdate($data , $link_id);
  else
    return $this->BasicCreate($data);
}//End of method

//----------------------------------------------------------
/*!\brief
* Suppress a link between process/doctype for a container
*
\param $link_id(integer) Primary key of the link.
*/
function SuppressDoctypeProcLink($link_id){
  $data['process_id'] = '';
  $data['doctype_id'] = '';
  return $this->BasicUpdate($data , $link_id);
}//End of method

//----------------------------------------------------------
/*!\brief
* Suppress a link between doctype and a container
*
\param $link_id(integer) Primary key of the link.
*/
function SuppressLink($link_id){
  return $this->BasicSuppress($link_id);
}//End of method

//---------------------------------------------------------------------------
/*!\brief
* Get all doctypes linked to container
* Return false if error or an array with the doctype properties 
*
\param $extension(string) limit the result to doctype with extension $extension.
\param $type(string) limit the result to doctype with type $type.
\param $params(array) is use for manage the display. See parameters function of GetQueryOptions().

JOIN doctypes table and _doctype_process
*/
function GetDoctypes($extension='' , $type = '' ,$container_id='', $params=array()){

  if(!empty($container_id))
    $params['exact_find'][$this->space->CONT_FIELDS_MAP_ID] = $container_id;
  if(!empty($extension)) //Get only the doctype with the correct file extension
    $params['find']['file_extension'] = $extension;
  if(!empty($type))
    $params['find']['file_type'] = $type;

  $params['with']['type'] = 'INNER';
  $params['with']['table'] = $this->RIGHT_OBJECT_TABLE;
  $params['with']['col'] = 'doctype_id';
  
  return $this->GetAllBasic($params);
}//End of method

//----------------------------------------------------------
/*!\brief
 * Get the process linked to the doctype for the container
 *
\param $doctype_id(integer) Primary key of doctype.
*/
function GetDoctypesProcess($doctype_id, $container_id){
  $params['exact_find']['doctype_id'] = $doctype_id;
  $params['select'] = array('process_id', 'link_id');
  $res = $this->GetDoctypes(NULL, NULL, $container_id, $params);
  if(!empty($res[0]['process_id'])) return $res[0]['process_id'];
  return false;
}//End of method

}//End of class

?>
