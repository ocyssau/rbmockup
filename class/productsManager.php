<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('./class/common/containersManager.php');

/*! \brief IS NOT USED.
*/class productsManager extends containersManager {

  var $SPACE_NAME     = 'product';
  var $OBJECT_TABLE    = 'products';
  var $HISTORY_TABLE   = 'product_history';
  var $INDICE_TABLE    = 'container_indice';
  var $TABLE_SEQ       = 'products_seq';
  var $PROJECT_RELATION_TABLE = 'project_container_rel';

  var $FIELDS_MAP_ID  = 'product_id';
  var $FIELDS_MAP_NUM = 'product_number';
  var $FIELDS_MAP_DESC = 'product_description';
  var $FIELDS_MAP_STATE = 'product_state';
  var $FIELDS_MAP_INDICE = 'product_indice_id';

  var $LINK_FILE_TABLE = 'documents_files_links';
  var $LINK_DOC_TABLE  = 'documents_container_links';
  var $SPEC_FIELDS     = array( );
  var $AREA_ID = 1;


  function __construct(){
  $this->error_stack =& new errorManager('workitem');

  }//End of method

}//End of class
?>
