<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//-------------------------------------------------------------------------
require_once (GALAXIA_LIBRARY.'/src/API/Instance.php');

/*! \brief This class manage instancce of a document workflow.
 *
 */
class instanceDocflow extends Instance{

	protected $docflow; //(object)
	protected $usr; //(object)
	protected $user; //(string)

	public $error_stack; //(Object)
	public $logger; //(Object)

	//--------------------------------------------------------------------
	function __construct($db){
		$this->db = $db;

		global $usr;
		$this->user = $usr->getProperty('handle');
		$this->usr =& $usr;

		global $logger;
		$this->logger =& $logger;
		$this->logger->log('create new instance of instanceDocflow');

		global $error_stack;
		$this->error_stack =& $error_stack;
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief Prepare and execute code of activity
	 *  Return boolean
	 *
	 */
	function setDocflow(docflow &$docflow) {
		$this->docflow =& $docflow;
	} //End of method

	//-------------------------------------------------------------------------
	/*! \brief Prepare and execute code of activity
	 *  Return boolean
	 *
	 * this method is call by instance::sendTo methods to execute automatic activity
	 * she dont must be directly call to create a new instance.
	 * This method is used to set environnement of the activities scripts. Vars set here are accessible in activities scripts.
	 * You can use objects $activity, $document, $instance for example in activities scripts.
	 * the objects $this->docflow, $this->docflow->process, $this->docflow->document must be set before call
	 */
	function executeActivity($activityId, $auto) {
		if( !isset($this->docflow) ){
			$this->logger->log('instanceDocflow::executeActivity $this->docflow is not set, execution failed');
			return false;
		}
		
		//set references to object to use in activities scripts
		$baseActivity = new BaseActivity($this->db);
		$activity = $baseActivity->getActivity($activityId);
		$process =& $this->docflow->getProcess();
		$document =& $this->docflow->getDocument();
		$odocument =& $document; //compatibility with ranchbe < 0.5.3
		$container =& $document->GetContainer(); //compatibility with ranchbe < 0.5.3
		$Manager =& $container; //compatibility with ranchbe < 0.5.3
		$docflow =& $this->docflow;
		$instance =& $this;
		
		//set var to use in activities scripts
		$document_id = $odocument->getId();
		$iid = $this->getInstanceId();
		$process_id = $activity->getProcessId();
		$user = $this->usr->getProperty('handle');
		$Documentinfos = $document->GetDocumentInfos();
		$_REQUEST['iid'] = $iid; //this is necessary for galaxia pre_activity script to set instance object
		$_REQUEST['activityId'] = $activityId; //$_REQUEST['activityId'] is use by instance::complete method to defined the activity to run.
		
		//test if a process is set
		if( is_null($process_id) ){
			$this->logger->log('process is not initialized');
			return false;
		}

		$this->logger->log('instanceDocflow::executeActivity : '.$activity->getName());

		if( empty( $iid ) && $activity->getType() != 'start' && $activity->getType() != 'standalone' ) {
			$this->logger->log('instanceDocflow::executeActivity : instanceId is empty');
			return false;
		}

		$__activity_completed = false;

		// Determine the activity using the activityId request parameter and get the activity information
		// load then the compiled version of the activity

		// load then the compiled version of the activity
		//compileActivity is defined in galaxia_setup.php
		global $smarty;
		$codes = compileActivity($process,$activity); //recompile activity if necessary and return path to scripts
		$this->logger->log('include activity script :'.$codes['shared']);
		if(!include ($codes['shared']) ) return false; // Include the shared code
		$this->logger->log('include activity script :'.$codes['source']);
		if(!include ($codes['source']) ) return false; // Now do whatever you have to do in the activity
		
		return $this;

	} //End of method

	//---------------------------
	/* Send a message to next users of next activities
	 */
	function sendMessageNextUsers( $to='' )
	{
		if( !isset($this->message) ) return false;
		if( !isset($this->CnextUsers) ) return false;
		
		if( is_array($to) && is_array( $this->CnextUsers ) ) {
			$toUsers = array_merge( $to , $this->CnextUsers );
		}else if( !empty($to) && is_array( $this->CnextUsers ) ){
			$toUsers = $this->CnextUsers;
			$toUsers[] = $to;
		}else if ( !empty($to) ){
			$toUsers = $to;
		}else if ( is_array( $this->CnextUsers ) ){
			$toUsers = $this->CnextUsers;
		}
		
		//Send message to next users
		include_once './class/messu/messulib.php';
		$messulib = new Messu($this->db);
		$from = $this->user;
		$cc   = '';
		$priority = 3;

		if( is_array($toUsers) ){
			foreach( $toUsers as $to ){
				$messulib->post_message($to, $from, $to, $cc, $this->message['subject'], $this->message['body'], $priority);
			}
		}
	
	}//End of method

	//---------------------------
	/* Send a message to next users of next activities
	 */
	function sendMessage( $to ,$subject, $body )
	{
		//if( !isset($this->message) ) return false;

		if( is_array($to) ) {
			$toUsers = $to;
		}else if( !empty($to) ){
			$toUsers[] = $to;
		}else{
			return false;
		}

		//Send message to next users
		include_once './class/messu/messulib.php';
		$messulib = new Messu($this->db);
		$from = $this->user;
		$cc   = '';
		$priority = 3;

		if( is_array($toUsers) )
		foreach( $toUsers as $to ){
			$messulib->post_message($to, $from, $to, $cc, $subject, $body, $priority);
		}
	}//End of method

	//---------------------------
	/* Update instance comment after validation of post operations
	 */
	function setMessageToNextUsers($subject, $body)
	{
		if( !empty($subject) ) $this->message['subject'] = $subject;
		if( !empty($body) ) $this->message['body'] = $body;
	}//End of method

}//End of class
?>
