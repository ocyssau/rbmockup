<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('class/workflow/workflow.php');

//-------------------------------------------------------------------------
/*! \brief This class manage workflow associate to document.
 *
 * workflow is an agrega of basics objects of the workflow : process, activity, instance
 *
 * How to use :
 * $docflow = new docflow($document); //First construct object from a document object
 * $docflow->initProcess(); //init the process from the document or from the instance if init before
 *
 * $docflow->initInstance(); //init the instance linked to document or return an not init instance object
 * or
 * $docflow->initInstance($instanceId);
 *
 * $docflow->execute($activityId); //execute the activity
 *
 *
 */
class docflow extends workflow{

	protected $document; //(document object) father document

	//--------------------------------------------------------------------
	
	function __construct(document &$document = null){
		global $usr;
		$this->usr =& $usr;

		global $error_stack;
		$this->error_stack =& $error_stack;

		global $logger;
		$this->logger =& $logger;

		global $dbGalaxia;
		$this->dbGalaxia =& $dbGalaxia;

		$this->document =& $document;

	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief attach process
	 *  Return object
	 *
	 */
	function initProcess($process_id = 0){
		if( $process_id == 0 ){
			//if( !isset($this->instance) ) $this->initInstance();
			if( isset($this->instance) )
			$process_id = $this->instance->pId; //try to get process_id from instance
			if( empty($process_id) )
			$process_id = $this->document->GetProcessId(); //try to get process id from document
		}
		return workflow::initProcess($process_id);
	}//End of method
	
	//-------------------------------------------------------------------------
	/*! \brief attach activity
	 *  Return object Activity or BaseActivity
	 *
	 */
	function initActivity($activityId = 0){
		if( !isset($this->activity) ){
			require_once ('./class/workflow/Activity.php');
			$this->activity = new Rb_Workflow_Activity($this->dbGalaxia);
		}

		if($activityId != 0)
		$this->activity = $this->activity->getActivity($activityId);
		return $this->activity;
	}//End of method
	
	

	//-------------------------------------------------------------------------
	/*! \brief attach instance
	 *  Return object
	 *
	 */
	function initInstance($instance_id=0){
		if( !isset($this->instance) ){
			require_once ('class/workflow/instanceDocflow.php');
			$this->instance = new instanceDocflow($this->dbGalaxia);
			$this->instance->setDocflow($this); //bi-Directionnal link : instance is link to docflow, docflow is link to instance
		}

		if( $instance_id == 0 ){ //try to get the instance_id from the document
			$instance_id = $this->document->GetDocProperty('instance_id');
		}

		if( $instance_id != 0 ){ //try to get the instance_id from the document
			$this->instance->getInstance($instance_id);
		}

		return $this->instance;

	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief get
	 *  Return object
	 *
	 */
	function getDocument(){
		return $this->document;
	}//End of method

	//-------------------------------------------------------------------------
	/*! \brief execute the activity
	 *
	 */
	function execute($activityId, $iid=NULL, $name=NULL){
		$iid = $this->document->GetDocProperty('instance_id');
		$name = $this->document->GetDocProperty('document_number').'_'.
		$this->document->GetDocProperty('document_indice_id');

		$accessCode = $this->document->GetDocProperty('document_access_code');
		if($accessCode > 9) return false; //can not execute activity on locked document

		$this->logger->log( 'docflow::execute activity id: '.$activityId );
		if( !workflow::execute($activityId, $iid, $name) ) return false;
		
		if(empty($iid)) { // If its empty, then link the current instance to document
			$iid = $this->instance->instanceId;
			$this->document->LinkInstanceToDocument($iid); //...link the instance
			$this->logger->log('docflow::execute :link instance : '.$iid.' to document : '. $this->document->GetDocProperty('document_number'));
		}
		$this->logger->log('docflow::execute : current linked instance id: '.$iid.' to document: '. $this->document->GetDocProperty('document_number'));

		//Write history
		if (isset( $this->document->history)){
			$this->document->history['instance_id'] = $iid;
			$this->document->history['activity_id'] = $activityId;
			$this->document->history['action_name'] = 'ChangeState';
			$this->document->WriteHistory();
		}

		return $this->instance;

	}//End of method


	//---------------------------
	/* reset the process, unlink instance of document
	 *
	 * \param $instanceId (int) optionnel. if omit get instance_id from the document
	 */
	function resetProcess( $instanceId = 0){

		if( isset($this->document) ){
			$this->logger->log('docflow::resetProcess : unlink process from document :'.$this->document->GetNumber() );
			if( $instanceId == 0 )
			$instanceId = $this->document->GetDocProperty('instance_id');
			$this->document->UnLinkInstanceToDocument();
			$this->document->LockDocument(0); //Change access code of document
			$this->document->ChangeState('aborted'); //Change state of document
			//Write history
			if (isset($this->document->history)){
				$this->document->history['instance_id'] = $instanceId;
				$this->document->history['action_name'] = 'ChangeState';
				$this->document->WriteHistory();
			}
		}

		workflow::resetProcess($instanceId);

		return true;
	} //End of function

}//End of class
?>
