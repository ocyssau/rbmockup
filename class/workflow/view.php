<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

class Workflow_View extends workflow{

	/** Path to template
	 * 
	 * @var string
	 */
	public $template;
	
	/** Path to view script
	 * 
	 * @var string
	 */
	public $script;
	
	/** Path to view script
	 * 
	 * @var Smarty
	 */
	protected $_smarty;

	//--------------------------------------------------------------------
	function __construct( Smarty $smarty ){
		global $error_stack;
		$this->error_stack =& $error_stack;

		global $logger;
		$this->logger =& $logger;
		
		$this->_smarty = $smarty;
		
	}//End of method
	
	function display(){
		include($this->script);
		$this->_smarty->display($this->template);
	}
	
}//End of class