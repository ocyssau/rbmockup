<?php

require_once('./class/common/rdirectory.php');

/*! \brief Wildspace is the user working directory.
 * Wildspace directory must be writable by the user.
 * The files to store in ranchbe must be copy in the wildspace before.
 */
class wildspace extends rdirectory{

	protected $CurrentUserName;
	protected $CurrentUserId;

	function __construct(){
		global $dbranchbe;
		$this->dbranchbe =& $dbranchbe;

		global $usr;
		$this->usr =& $usr;

		global $error_stack;
		$this->error_stack =& $error_stack;

		$this->SPACE_NAME  = 'wildspace';

		$this->CurrentUserName = $this->usr->getProperty('handle');
		$this->CurrentUserId   = $this->usr->getProperty('auth_user_id');

		//construct the path to the personnal working directory for the current user.
		if(!empty($this->CurrentUserName)){
			$this->WILDSPACE = DEFAULT_WILDSPACE_DIR .'/'. $this->CurrentUserName;
		}else{
			$this->WILDSPACE = DEFAULT_WILDSPACE_DIR .'/'. 'Anonymous';
		}
		if(defined("DEFAULT_SUB_WILDSPACE_DIR")){
			$test = constant("DEFAULT_SUB_WILDSPACE_DIR");
		}

		if(!empty($test)){
			$this->WILDSPACE = $this->WILDSPACE .'/'. DEFAULT_SUB_WILDSPACE_DIR;
		}

		if(!is_dir($this->WILDSPACE)){
			if (!mkdir($this->WILDSPACE , 0775 , true)){
				trigger_error( sprintf('can\'t create %s', $this->IMPORT_DIR), E_USER_ERROR );
			}
		}
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Return the path of the wildspace
	 */
	function GetPath(){
		return $this->WILDSPACE;
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Return the current user id
	 */
	function GetUserId(){
		return $this->CurrentUserId;
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Return the current user name
	 */
	function GetUserName(){
		return $this->CurrentUserName;
	}//End of method

	//---------------------------------------------------------------------------
	/*! \brief Suppress a file from the wildspace.
	 *   return true or false.
	 *
	 \param $file(string) name of the data to suppress.
	 */
	function SuppressWildspaceFile($file){
		$file = $this->WILDSPACE .'/'. $file;
		require_once './class/common/fsdata.php';
		$odata = fsdata::_dataFactory($file);
		return $odata->putInTrash();
	}//End of method

	//--------------------------------------------------------
	/*! \brief This method can be used to get files, cadds parts, adraw, camu in the wildspace
	 *  Return a array if no errors, else return false.
	 *
	 * This method get all files from the Wildspace and get infos about document of the file if this file has been "stored".
	 * Option of the $params permit result filter.
	 \param $params[maxRecords](integer) max number of records to display
	 \param $params[sort_field](string)
	 \param $params[sort_order](string = ASC or DESC)
	 \param $params[find](string)
	 \param $params[find_field](string)
	 \param $params[displayMd5] (bool)
	 */
	function GetDatas($params , $getStoredInfos = true){

		if(is_dir($this->WILDSPACE)){
			$path = $this->WILDSPACE;
		}else{
			$this->error_stack->push(ERROR, 'Fatal', array('element'=>$path, 'debug'=>array()), 'wildspace %element% is not reachable');
			return false;
		}

		if( is_file($path.'/_db') ){ //add ranchbe 0.6 for cadds datas
			$this->error_stack->push(ERROR, 'Error', array('element'=>$path), 'forbidden presence of _db file in wildspace %element%. Delete it and retry.');
			return false;
		}

		$files = rdirectory::GetDatas($path , $params);

		require_once('./class/checkoutIndex.php');
		if(is_array($files) && $getStoredInfos == true){
			foreach($files as $key=>$file){ //get infos from checkout index
				$checkout_infos = checkoutIndex::GetCheckoutIndex($file['file_name']);
				$files[$key]['container_type']   = $checkout_infos['container_type'];
				$files[$key]['container_id']     = $checkout_infos['container_id'];
				$files[$key]['container_number'] = $checkout_infos['container_number'];
				$files[$key]['document_id']      = $checkout_infos['document_id'];
				$files[$key]['check_out_by']     = $checkout_infos['check_out_by'];
				$files[$key]['designation']      = $checkout_infos['designation'];
			}}
			return $files;
	}//End of method

	//-------------------------------------------------------
	/*! \brief Copy a upload file in wildspace.
	 Return true or false.
	 *
	 * Take the parameters of the uploaded file(from array $file) and create the file in the wildpspace.
	 \param $file(array) :
	 \param $file[name](string) name of the uploaded file
	 \param $file[type](string) mime type of the file
	 \param $file[tmp_name](string) temp name of the file on the server
	 \param $file[error](integer) error code if error occured during transfert(0=no error)
	 \param $file[size](integer) size of the file in octets
	 \param $replace(bool) if true and if the file exist in the wildspace, it will be replaced by the uploaded file.
	 */
	function UploadFile($file , $replace = false){
		require_once('./class/common/file.php');
		return file::UploadFile($file , $replace, $this->WILDSPACE);
	}//End of method

}//End of class
?>
