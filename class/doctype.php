<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('./class/common/basic.php');

/*! \brief This class manage the doctypes.
* A doctype type the document and can be use to restrict the file name, file extension, and file type
* and defined particular action to apply on document respecting a name convention
*/
class doctype extends basic{

  public $doctype_id = 0; //(Integer) id of the current doctype
  public $error_stack; //(Object)
  public $FIELDS = array('doctype_id', 'doctype_number', 'doctype_description', 'can_be_composite', 'script_post_store', 'script_pre_store', 'script_pre_update', 'script_post_update', 'recognition_regexp', 'file_extension', 'icon');
  public $AREA_ID = 1;

  private $props; //(Array) properties of current doctype

  protected $SPACE_NAME  = 'doctype';
  protected $OBJECT_TABLE = 'doctypes';
  protected $FIELDS_MAP_ID = 'doctype_id';
  protected $FIELDS_MAP_NUM = 'doctype_number';
  protected $TABLE_SEQ      = 'doctypes_seq';


  function __construct($doctype_id=NULL){
    global $dbranchbe;
    $this->dbranchbe =& $dbranchbe;
    global $usr;
    $this->usr =& $usr;
    global $error_stack;
    $this->error_stack =& $error_stack;
    if(!is_null($doctype_id))
        $this->init($doctype_id);
  }//End of method

  //-------------------------------------------------------------------------
  /*! \brief init the properties of the document
  *  Return doc_prop(array)
  * 
  * \param $document_id(integer) Id of the document
  */
  function init($doctype_id){
    $this->doctype_id = $doctype_id;
    if(isset($this->props)) unset($this->props);
    return true;
  }//End of method

  //----------------------------------------------------------
  /*! \brief Get the property of the document by the property name. init() must be call before
   * 
   * \param $property_name(string) = document_id, document_number, ...etc
   */
  function GetProperty($property_name){
    //echo 'getProperty'.$property_name.'<br>';
    if(isset($this->props[$property_name]))
      return $this->props[$property_name];
    if($this->doctype_id == 0){
      $this->error_stack->push(LOG, 'Error', array(), '$this->doctype_id is not set');
      return false;}
    $query = 'SELECT '.$property_name.' FROM '.$this->OBJECT_TABLE.' WHERE '.$this->FIELDS_MAP_ID.' = \''.$this->doctype_id.'\'';
    $res = $this->dbranchbe->GetOne($query);
    if($res === false){
      $this->error_stack->push(ERROR, 'Fatal', array('query'=>$query , 'debug'=>array($document_id)), $this->dbranchbe->ErrorMsg());
      return false;}
    else
      return $this->props[$property_name] = $res;
  }//End of method

  //----------------------------------------------------------
  /*! \brief Return id of the current doctype
   *  Return integer
   */
  function GetId(){
    if($this->doctype_id == 0){
      $this->error_stack->push(LOG, 'Error', array(), '$this->doctype_id is not set');
      return false;}
    return $this->doctype_id;
  }//End of method
  
  //----------------------------------------------------------
  /*! \brief Get all properties of the current doctype
   *  Return a array
   */
  function GetProperties(){
    if($this->doctype_id == 0){
      $this->error_stack->push(LOG, 'Error', array(), '$this->doctype_id is not set');
      return false;}
    return $this->props = $this->GetInfos($this->doctype_id);
  }//End of method

  //---------------------------------------------------------------------------

  /*!\brief Create a new doctypes from the properties of the param $data.
   * return the id if success, else return false.
   *    
    \param $data(array) field=>value of the properties to records.
    \param  $replace(array) if true, update the existing doctype.
  */
  function Create($data , $replace=false) {
/*
  if (! is_file ($data['script_pre_store'])) unset ($data['script_pre_store']);
  if (! is_file ($data['script_post_store'])) unset ($data['script_post_store']);
  if (! is_file ($data['script_pre_update'])) unset ($data['script_pre_update']);
  if (! is_file ($data['script_post_update'])) unset ($data['script_post_update']);
  if (! is_file ($data['icon'])) unset ($data['icon']);
*/

  if(is_array($data['file_type'])){
    $data['file_type'] = implode(' ' , $data['file_type']);
  }

  if(is_array($data['file_extension'])){
    $data['file_extension'] = implode(' ' , $data['file_extension']);
  }

  if ($replace){
    if ($id = $this->Exist($data['doctype_number'])){
      $this->BasicUpdate($data , $id);
      $this->CompileIcon( $data['icon'] , $id);
      return $id;
    }else $replace = false;
  }

  if (!$replace){
    if (!$id=$this->BasicCreate($data) ) return false;
    else $this->CompileIcon( $data['icon'] , $id);
  }

  return $id;

  }//End of method

//---------------------------------------------------------------------------


  /*!\brief Test if the doctype exist.
   * return doctype_id, else return false.
   *    
    \param $doctype_number(string) doctype number to check.
  */  
  function Exist($doctype_number){

  global $dbranchbe;

  $query = "SELECT doctype_id FROM $this->OBJECT_TABLE WHERE doctype_number = '$doctype_number'
          ";

  if(!$ret = $dbranchbe->GetOne($query)) return false;
  else return $ret;

  }//End of method

//---------------------------------------------------------------------------

  /*!\brief Modify a doctype.
   * return true or false.
   * 
     \param $data(array) field=>value of the properties to records.
     \param $id(integer) doctype id of the doctype to modify.
  */
  function Update($data, $id) {

/*
  if (! is_file ($data['script_pre_store'])) unset ($data['script_pre_store']);
  if (! is_file ($data['script_post_store'])) unset ($data['script_post_store']);
  if (! is_file ($data['script_pre_update'])) unset ($data['script_pre_update']);
  if (! is_file ($data['script_post_update'])) unset ($data['script_post_update']);
  if (! is_file ($data['icon'])) unset ($data['icon']);
*/

  if(is_array($data['file_type'])){
    $data['file_type'] = implode(' ' , $data['file_type']);
  }

  if(is_array($data['file_extension'])){
    $data['file_extension'] = implode(' ' , $data['file_extension']);
  }

  if (! $this->BasicUpdate($data, $id) ) return false;
  
  $this->CompileIcon( $data['icon'] , $id);
  
  }//End of method

//---------------------------------------------------------------------------

  /*!\brief Suppress a doctype.
   * return true or false.
   * 
     \param $id(integer) doctype id of the doctype to modify.
  */
  function Suppress($id) {
    return $this->BasicSuppress($id);
  }//End of method

//---------------------------------------------------------------------------

  /*!\brief Get list of all doctypes.
   * return a array if success, else return false.
   *    
    \param $params(array) is use for manage the display. See parameters function of GetQueryOptions()
  */
  function GetAll($params = ''){
    return $this->GetAllBasic($params);
  }//End of method

//---------------------------------------------------------------------------

  /*!\brief Get info about a single doctype by his doctype_id.
   * return a array if success, else return false.
   *    
    \param $doctype_id(integer) id of the doctype.
    \param  $selectClose(array) to define the sql select option.
  */
  function GetInfos( $doctype_id , $selectClose = ''){
    return $this->GetBasicInfos($doctype_id , $selectClose);
  }//End of method

//---------------------------------------------------------------------------

  /*!\brief Get info about a single doctype by his doctype_number.
   * return a array if success, else return false.
   *    
    \param $doctype_number(string) number of the doctype.
    \param  $selectClose(array) to define the sql select option.
  */
  function GetInfosNum( $doctype_number , $selectClose = '') {
    return $this->GetBasicInfosNum($doctype_number , $selectClose );
  }//End of method

//---------------------------------------------------------------------------

  /*!\brief Create a icon file in "C" dir for icon to associate to doctype.
  *   Return true if no errors or false
  *
  * Create a copy of the icon file in the "C" directory.
  * The copy in C is renamed with the id of the doctype.
    \param $icon(string) name of the icon to associate to doctype.
    \param  $doctype_id(integer) id of the doctype.
  */
  function CompileIcon( $icon , $doctype_id){
    if(is_file ( DEFAULT_DOCTYPES_ICONS_DIR .'/'. $icon )){
      if(filesize(DEFAULT_DOCTYPES_ICONS_DIR .'/'. $icon) > 10000){
        $this->error_stack->push(ERROR, 'Fatal', array('element'=>$var, 'debug'=>array()), 'Icon file must be less than 10Ko <br />');
        return false;}
      $path_parts = pathinfo($icon);
      $compiled_icon_dir = DEFAULT_DOCTYPES_ICONS_DIR.'/C';
      if( !is_dir($compiled_icon_dir)  ){
        if(!mkdir($compiled_icon_dir, 0755)) return false;
      }
      copy(DEFAULT_DOCTYPES_ICONS_DIR.'/'.$icon , $compiled_icon_dir.'/'.$doctype_id.'.'.$path_parts['extension']);
    }
    return true;
  }//End of method

//---------------------------------------------------------------------------

  /*!\brief Import the doctype from a csv file.
  * Return true if no errors or false.
  * 
  * The first line of the csv file define the field of the doctypes table. The next line content the values.    
    \param $csvfile(string) path to csv file.
    \param  $replace(bool) if true, the existing doctypes will be modified.
  */
  function ImportDoctypesCsv( $csvfile , $replace = true) {

  global $dbranchbe;
  global $smarty;

  require_once('./class/common/importManager.php');
  $records = importManager::ImportCsv($csvfile);

	foreach ($records as $u) {
		if (!empty($u['doctype_number']))
    	if(!$this->Create($u , $replace))
    	 $err[] = $u;
  }

  }//End of method

}//End of class

?>
