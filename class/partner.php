<?php
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once('./class/common/basic.php');

/*! \brief a partner is an entry of the ranchbe adress book.
*/
class partner extends basic {

  var $SPACE_NAME   = 'partner';
  var $OBJECT_TABLE  = 'partners';
  var $FIELDS_MAP_ID  = 'partner_id';
  var $FIELDS_MAP_NUM = 'partner_number';
  var $TABLE_SEQ      = 'partners_seq';

  var $FIELDS = array('partner_id', 'partner_number', 'first_name', 'last_name', 'adress', 'city', 'zip_code', 'phone', 'mail', 'web_site', 'activity');
  var $AREA_ID = 1;

  function __construct(){
    global $dbranchbe;
    $this->dbranchbe =& $dbranchbe;

    global $usr;
    $this->usr =& $usr;

    global $error_stack;
    $this->error_stack =& $error_stack;

    //$this->error_stack =& new errorManager('workitem');
  }//End of method

//-------------------------------------------------------------

  /*!\brief Create a new partner from the properties of the param $data.
   * return the id if success, else return false.
   *    
    \param $data(array) field=>value of the properties to records.
  */
  function PartnerCreate($data , $replace=false) {

  $data['partner_number'] = self::composeNumber($data['first_name'] , $data['last_name']);

  if ($replace){
    if ($id = $this->Exist($data['partner_number'])){
      $this->PartnerUpdate($data, $id);
      return $id;
    }else $replace = false;
  }

  if (!$replace){
    if (!$id=$this->BasicCreate($data) ) return false;
    else return $id;
  }

  }//End of method

//-------------------------------------------------------------

  /*!\brief Modify a partner record.
   * return true or false.
   * 
     \param $data(array) field=>value of the properties to update.
     \param $partner_id(integer) partner id of the partner to modify.
  */
  function PartnerUpdate($data, $partner_id) {

    $data['partner_number'] = self::composeNumber($data['first_name'] , $data['last_name']);
    return $this->BasicUpdate($data, $partner_id);

  }//End of method

//-------------------------------------------------------------

  /*!\brief Suppress a partner record.
   * return true or false.
   * 
     \param $partner_id(integer) partner id of the partner to suppress.
  */
  function PartnerSuppress($partner_id) {

    return $this->BasicSuppress($partner_id);

  }//End of method

//-------------------------------------------------------------
/*!\brief Get list of all partners.
 * return a array if success, else return false.
 *    
  \param $params(array) is use for manage the display. See parameters function of GetQueryOptions()
*/
function GetAllPartners($params='') {
  return $this->GetAllBasic($params);
}//End of method

//-------------------------------------------------------------

  /*!\brief Get info about a single partner.
   * return a array if success, else return false.
   *    
    \param $partner_id(integer) id of the partner.
  */
  function GetPartnerInfos($partner_id) {

  return $this->GetBasicInfos($partner_id);

  }//End of method

//-------------------------------------------------------------
  
  /*!\brief Get number or other single infos about a partner.
   * return a string if success, else return false.
   *    
    \param $partner_id(integer) id of the partner.
    \param $select(string) field to get if you other that partner_number.
  */
//  function GetName( $partner_id , $select = 'partner_number'){
  function GetName($partner_id){

    global $dbranchbe;
    
  	if (is_null ($id) && !is_numeric($id)){
     $this->error_stack->push(ERROR, 'Fatal', array(), 'Invalid id' );
  	}
  	
  	$query = "SELECT partner_number FROM partners WHERE partner_id = '$partner_id'";

    if(!$one = $dbranchbe->GetOne($query)){
      $this->error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query, 'debug'=>array()), $dbranchbe->ErrorMsg() );
      return false;
    }

    return $one;

  }//End of method

//---------------------------------------------------------------------------

  /*!\brief Import partners from a csv file.
  * Return true if no errors or false.
  * 
  * The first line of the csv file define the field of the partners table. The next lines content the values.
    \param $csvfile(string) path to csv file.
    \param  $replace(bool) if true, the existing partner will be modified.
  */
  function ImportPartnerCsv( $csvfile , $replace = true) {

  global $dbranchbe;
  global $smarty;

  require_once('./class/common/import.php');
  $records = import::ImportCsv($csvfile);

	foreach ($records as $p) {
  if(!$this->PartnerCreate($p , $replace))
    $err[] = $p;
  }

  }//End of method
  
//---------------------------------------------------------------------------

  /*!\brief Test if the partner exist.
   * return partner_id, else return false.
   *    
    \param $partner_number(string) partner number to check.
  */  
  function Exist($partner_number) {

  global $dbranchbe;

  $query = "SELECT partner_id FROM $this->OBJECT_TABLE WHERE partner_number = '$partner_number'
          ";

  if(!$ret = $dbranchbe->GetOne($query)) return false;
  else return $ret;

  }//End of method


//---------------------------------------------------------------------------

  /*!\brief Compose the partner number from the first and last name.
   * return partner_number, else return false.
   *    
    \param $first_name(string) partner first name.
    \param $last_name(string) partner last name.
  */  
  static function composeNumber($first_name , $last_name){
    if(!empty($first_name) && !empty($last_name) )
       $separator = '_';
    return $partner_number = self::no_accent($first_name . $separator . $last_name);
  }//End of method
  
//---------------------------------------------------------------------------

  /*!\brief Suppress accent of the input string.
   * return the input without accents, else return false.
   *    
    \param $in(string) input string.
  */  
  static function no_accent($in) {
    //thank to 'http://www.wikistuce.info/doku.php/php/supprimer_tous_les_caracteres_speciaux_d-une_chaine'
  	$search = array ('@[����]@','@[���]@','@[��]@','@[���]@','@[��]@','@[����]@','@[���]@','@[��]@','@[��]@','@[��]@','@[�]@i','@[�]@i','@[ ]@i','@[^a-zA-Z0-9_]@');
  	$replace = array ('e','a','i','u','o','E','A','I','U','O','c','C','_','');
  	return preg_replace($search, $replace, $in);
  }//End of method


}//End of class

?>
