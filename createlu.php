<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//PEAR::setErrorHandling(PEAR_ERROR_RETURN);

$usr = LiveUser::singleton($conf);

//Tries to retrieve the auth object from session and checks possible timeouts.
if (!$usr->init()) {
    var_dump($usr->getErrors());
    die();
}

$handle = (array_key_exists('handle', $_REQUEST)) ? $_REQUEST['handle'] : null;
$passwd = (array_key_exists('passwd', $_REQUEST)) ? $_REQUEST['passwd'] : null;
$logout = (array_key_exists('logout', $_REQUEST)) ? $_REQUEST['logout'] : false;

if ($logout) {
    $usr->logout(true);
//} elseif(!$usr->isLoggedIn() || ($handle && $usr->getProperty('handle') != $handle)) {
} elseif(!$usr->isLoggedIn()) {
    if (!$handle) {
        $usr->login(null, null, true);
    } else {
        $usr->login($handle, $passwd);
    }
} //else user_log($usr->getProperty('handle'));

$LUA =& LiveUser_Admin::factory($conf);
$LUA->init();

//Function to record the user activities and log connection
//function user_log($handle){
//  echo $handle . ' is login';
//}

//Function to ckeck permissions
function check_ticket( $right_define_name , $area_id=1 , $secure=true) {
/*If secure = true, die the scripts if user has not the permission
* else return just false
*/
  global $usr;
  global $LUA;
  global $smarty;

  //Get the right_id
  $filter['filters'] = array('right_define_name' => $right_define_name , 'area_id' => $area_id);
  $filter['fields'] = array('right_id');
  $rights = $LUA->perm->getRights($filter);

  if (isset($rights[0]['right_id'])){
    $right_id =  $rights[0]['right_id'];
    //Check permissions
    if (!$usr->checkRight($right_id)){
      if($secure){
      $smarty->assign('msg', tra('You are not authorized to process this request'));
      $smarty->display("error.tpl");
      die;}
      else return false;
    }else return true;
  }else{print 'not defined right: ' . $right_define_name . '<br />'; return false;}
} //End of check ticket


//function check_flood($page_id='') {
//Anti-flood
//Inspired by principle described in http://www.developpez.net/forums/archive/index.php/t-104783.html
//If session jeton is egal to jeton record in form, generate a new jeton.
//session_register('jeton');
/*
echo '<pre>';var_dump($_POST);echo '</pre>';

if($_SESSION['jeton'] == $_POST['jeton']){
  $jeton=md5(uniqid(mt_rand(), true));
  $smarty->assign('jeton', $jeton);
  print 'le jeton est le m�me';
}else{
  $jeton=md5(uniqid(mt_rand(), true));
  $_SESSION['jeton']=$jeton;
  print 'le jeton est different';
}
*/

/*
  global $smarty;

  if(empty($page_id))
    $page_id = (basename($_SERVER['PHP_SELF']));

  //echo $page_id;

  if ($_REQUEST['ticket'] == $_SESSION['ticket'."$page_id"]){
    if (time() - $_SESSION['ticket_time'] < 1){
      //echo 'execution trop rapproche<br>';
      $_SESSION['ticket_time'] = time();
      $check_flood = false;
    }else{
      //echo 'execute code<br>';
      $_SESSION['ticket_time'] = time();
      $check_flood = true;
    }
  }else{
      //echo 'non execute<br>';
      $check_flood = false;
  }
  
  //echo $_SESSION['ticket'."$scriptName"];
  //echo $_REQUEST['ticket'] . '<br>';

  $ticket=md5(uniqid(mt_rand(), true));
  $smarty->assign('ticket', $ticket);
  $_SESSION['ticket'."$page_id"] = $ticket;
  
  return $check_flood;

} //End of check flood
*/

/*
function check_flood($page_id='') {
//Anti-flood V2. Inverse la condition : false si le ticket session = ticket dans post
//Inspired by principle described in http://www.developpez.net/forums/archive/index.php/t-104783.html
//If session jeton is egal to jeton record in form, generate a new jeton.
//session_register('jeton');

//echo '<pre>';var_dump($_POST);echo '</pre>';

  global $smarty;

  if(empty($page_id))
    $page_id = (basename($_SERVER['PHP_SELF']));

  echo 'page id :'.$page_id .'<br>';
  echo '<pre>REQUEST :';echo $_REQUEST['ticket'];echo '</pre>';
  echo '<pre>SESSION :';echo $_SESSION['ticket'."$page_id"];echo '</pre>';

  if(empty($_REQUEST['ticket'])){
    $_REQUEST['ticket'] = md5(uniqid(mt_rand(), true));
  }

  if ($_REQUEST['ticket'] != $_SESSION['ticket'."$page_id"]){
      $_SESSION['ticket'."$page_id"] = $_REQUEST['ticket']; //Assign ticket in $_POST to ticket in $_SESSION
      $smarty->assign('ticket', md5(uniqid(mt_rand(), true))); //Regenerate a ticket and assign it to page
      $check_flood = true;
  }else{
      //$smarty->assign('ticket', md5(uniqid(mt_rand(), true))); //Regenerate a ticket and assign it to page
      $check_flood = false;
  }
  
  //echo $_SESSION['ticket'."$scriptName"];
  //echo $_REQUEST['ticket'] . '<br>';

  return $check_flood;

} //End of check flood
*/

function check_flood($page_id='') {
//Anti-flood V3. Condition existance du ticket en session. Tous les tickets sont conserv�s en session jusqu'a leur utilisation.
//Inspired by principle described in http://www.developpez.net/forums/archive/index.php/t-104783.html
//If session jeton is egal to jeton record in form, generate a new jeton.
//session_register('jeton');

  global $smarty;

  //echo 'page id :'.$page_id .'<br>';
  //echo '<pre>REQUEST :';echo $_REQUEST['ticket'];echo '</pre>';
  //echo '<pre>SESSION :';var_dump($_SESSION['ticket']);echo '</pre>';
  //echo '<pre>NB TICKET IN SESSION :';echo count($_SESSION['ticket']);echo '</pre>';

  //Check time between 2 requests
  if (time() - $_SESSION['ticket_time'] < 1){ // If the page is recall after time < to one second
    print '<b>You cant doing two request in less than 1 second<br />Wait 5 second and refresh this page...</b>';
    $_SESSION['ticket_time'] = time(); //echo 'execution trop rapproche<br>';
    die;
  }else{
    $_SESSION['ticket_time'] = time();
  }

  if(empty($_REQUEST['ticket'])){ //init the ticket
    $ticket = md5(uniqid(mt_rand(), true));
    $_SESSION['ticket'][$ticket] = true;
    $smarty->assign('ticket', $ticket); //Regenerate a ticket and assign it to page
    return false;
  }

  if (isset($_SESSION['ticket'][$_REQUEST['ticket']])){ //Check if ticket is in SESSION
    unset($_SESSION['ticket'][$_REQUEST['ticket']]); // Suppress ticket in session
    $ticket = md5(uniqid(mt_rand(), true)); // generate a new ticket
    $_SESSION['ticket'][$ticket] = true; // Record new ticket in session
    $smarty->assign('ticket', $ticket); // Assign it to page
    return true;
  }else{
    $ticket = md5(uniqid(mt_rand(), true)); // generate a new ticket
    $_SESSION['ticket'][$ticket] = true; // Record new ticket in session
    $smarty->assign('ticket', $ticket); // Assign it to page
    //$smarty->assign('ticket', $_REQUEST['ticket']); // Reassign ticket to page
    return false;
  }
  
  return true;

} //End of check flood


//Valid the access
if (!$usr->isLoggedIn()) {
  $smarty->assign('accueil', 'active');
  $smarty->assign('mid', 'login.tpl');
  $smarty->display("ranchbe.tpl");
  exit;
}

?>
