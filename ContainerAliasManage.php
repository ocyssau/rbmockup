<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once 'conf/ranchbe_setup.php';//ranchBE configuration
require_once 'class/common/space.php'; //Class to manage the container
require_once 'class/common/container.php'; //Class to manage the container
require_once 'class/common/containerAlias.php'; //Class to manage the container
require_once 'class/common/containerAliasDAO.php'; //Class to manage the container

if(empty($_REQUEST['space'])){
  print 'space is not set';
  die;
}

$space = new space( $_REQUEST['space'] );

if(!empty($_REQUEST['container_id'])){
  $container = new container($space, $_REQUEST['container_id'] );
  $Manager = new containerAliasDAO($container); //Create new manager
}else{
  $container = new container( $space );
  $Manager = new containerAliasDAO($container); //Create new manager
}

$area_id = $Manager->AREA_ID;

check_ticket( 'container_get' , $area_id );

$check_flood = check_flood();

switch ( $_REQUEST['action'] ) {
//---------------------------------------------------------------------
  case 'suppressAlias':   //Suppress
    if(!$check_flood) break;
    check_ticket('container_suppress' , $area_id);
    if (empty($_REQUEST['checked'])){
      $Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item' );
    }else{
      foreach ($_REQUEST['checked'] as $alias_id){
      	$Manager->Suppress( $alias_id );
      } //loop
    }
    unset( $_REQUEST['action'] );
    unset ( $_SESSION['ticket_time'] ); //To dont create a anti_flood error with redirection page
    header('Location:'.$Manager->space->SPACE_NAME.'s.php');
    break;

//---------------------------------------------------------------------
  case 'modifyAlias':   //Edit
  case 'createAlias':   //Create
    check_ticket('container_create' , $area_id);

    //Create the form
    require_once 'GUI/Ranchbe_HTML_QuickForm.php'; //Librairy to easily create forms
    require_once 'GUI/GUI.php';
    $form =& new Ranchbe_HTML_QuickForm('editform', 'POST');

    if( $_REQUEST['action'] == 'modifyAlias' ){
      if (empty($_REQUEST['checked'])){
        $Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item' );
        break;
      }
      $alias_id = $_REQUEST['checked'];
      $Manager->init( $alias_id );
      $container_id = $Manager->GetProperty('container_id');

      $form->setDefaults(array(
        'container_number'      => $Manager->GetProperty('container_number'),
        'container_description' => $Manager->GetProperty('container_description'),
        'container_id'          => $container_id,
      ));
  
      $form->addElement('hidden', 'checked', $alias_id );
      $form->addElement('hidden', 'action', 'modifyAlias');
      $form->addElement('header', null, tra('Edit container alias').' '.$Manager->GetProperty('container_number').'<br />');
    }else{
      $form->addElement('hidden', 'action', 'createAlias');
      $form->addElement('header', null, tra('Create a container alias').'<br />');
    }

    $form->addElement('hidden', 'page_id', 'ContainerAliasManage_modifyAlias');
    $form->addElement('hidden', 'space', $_REQUEST['space']);

    $form->addElement('text', 'container_number', tra('container_number'));
    $form->addRule('container_number', tra('Required'), 'required', null, 'client');

    $form->addElement('text', 'container_description', tra('container_description'));
    $form->addRule('container_description', tra('Required'), 'required', null, 'client');
    
    $params = array(
              'field_name'=>'container_id',
              'field_description'=>tra('Alias of'),
              'default_value'=>$container_id,
              'field_multiple'=>false,
              'return_name'=>false,
              'field_required'=>true);
    construct_select_container($params , $form , $container);

    if ($form->validate()){ //Validate the form...
      $form->freeze(); //...and freeze it
      $data = array();
      $data['container_description'] = $_REQUEST['container_description'];
      $data['container_number'] = $_REQUEST['container_number'];
      $data['container_id'] = $_REQUEST['container_id'];
      if( $_REQUEST['action'] == 'modifyAlias' )
        $Manager->Update( $data );
      if( $_REQUEST['action'] == 'createAlias' )
        $Manager->Create( $data );
    }else{
      $form->addElement('submit', null, 'GO');
      $form->applyFilter('__ALL__', 'trim');
    }

    $form->display();
    $Manager->error_stack->checkErrors();

    die;
    break;
} //End of switch

?>
