<?php
//page de recherche dans le conteneur des aides m�moire informatiques.
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

$_REQUEST['handle'] = 'anonymous';
$_REQUEST['passwd'] = 'anonymous00';

//Setup
require_once 'conf/ranchbe_setup.php';
require_once 'class/common/container.php'; //Class to manage the bookshop
require_once 'HTML/QuickForm.php'; //Librairy to easily create forms
require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm
require_once 'GUI/GUI.php';
require_once 'lib/Date/date.php';
require_once 'lib/search.inc.php';//Search engine

$DOCSIERcontainer_id[] = 11; // Id du conteneur NormesAirbus
//$DOCSIERcontainer_id[] = 5; // Id du conteneur DirectivesFAL-GSE
//$DOCSIERcontainer_id[] = 4; // Id du conteneur MemosAirbus
//$DOCSIERcontainer_id[] = 7; // Id du conteneur NormesDassaultBreguet

$Manager = container::_factory( 'bookshop', $DOCSIERcontainer_id[0] ); //Create new manager
$space =& $Manager->space;

//------------------------------------------------------------------------------
//Execute view and download before call to function check_flood
//If not, problem with ticket coherence
if ($_REQUEST['action'] == 'viewDocument'){
    if ( empty($_REQUEST['checked']) ){
      $Manager->error_stack->push(ERROR, 'Fatal', array(), 'Vous devez selectionner au moins un document');
      return false;
    }else{
      require_once('./class/common/document.php');
      $odocument = new document( $Manager->space, $_REQUEST['checked'][0] );
      $odocument->ViewDocument();
    }
}

function no_accent($in) {
  //thank to 'http://www.wikistuce.info/doku.php/php/supprimer_tous_les_caracteres_speciaux_d-une_chaine'
	$search = array ('@[����]@','@[���]@','@[��]@','@[���]@','@[��]@','@[����]@','@[���]@','@[��]@','@[��]@','@[��]@','@[�]@i','@[�]@i');
	$replace = array ('e','a','i','u','o','E','A','I','U','O','c','C');
	return preg_replace($search, $replace, $in);
}//End of method

function clean_keywords($in) {
  //thank to 'http://www.wikistuce.info/doku.php/php/supprimer_tous_les_caracteres_speciaux_d-une_chaine'
	$search = array ('#');
	$replace = array (' ');
	return preg_replace($search, $replace, $in);
}//End of method

function view($document_id){
}

function prepaSearch($values){
  global $Manager;
  global $DOCSIERcontainer_id;
  global $smarty;

  $selectdClose = 'document_id , doctype_id , document_number , designation , document_state, category_id, document_indice_id';
  
  $container_doc = 'bookshop_documents';
  
  //Where container_id
  foreach($DOCSIERcontainer_id as $container_id){
    $wherec[] = "(doc.bookshop_id = '$container_id')";
  }
  if(!empty($wherec))
    $whereClose[] = '(' .implode(' OR ', $wherec) . ')';

  /*
  //Where docsier_keyword. Explode each word and create a separate where option.
  if(!empty($values['docsier_keyword'])){
    $values['docsier_keyword'] = no_accent($values['docsier_keyword']); //Clean accents from user input
    $keywords = explode(' ' , $values['docsier_keyword']);
    foreach($keywords as $word){
      //echo $word.'<br>';
      $word = trim($word); //Clean spaces from user input
      $word = strtolower($word); //Clean majuscules from user input
      $Where[] = "doc.docsier_keyword LIKE '%$word%'";
    }
    if(!empty($Where))
      $whereClose[] = '(' .implode(' AND ', $Where) . ')';
  }
  */

  //Where designation
  $values['designation'] = trim($values['designation']);
  if(!empty($values['designation']))
    $whereClose[] = "(doc.designation LIKE '%$values[designation]%')";

  //Where document_number
  $values['document_number'] = trim($values['document_number']);
  if(!empty($values['document_number']))
    $whereClose[] = "(doc.document_number LIKE '%$values[document_number]%')";

  //Where category_id
  $values['category_id'] = trim($values['category_id']);
  if(!empty($values['category_id']))
    $whereClose[] = "(doc.category_id = '$values[category_id]')";

  //Construct the where close
  if (!empty( $whereClose ) ) {
      $whereClose = ' WHERE ' . implode(' AND ', $whereClose);
  }

  //Define limit and offset
  if (isset($values['numrows']) && !empty($values['numrows']) && ($values['numrows'] <= 5000))
    $limit = $values['numrows'];
  else $limit = '5000';
  if (!isset($offset))
    $offset = '0';

  //var_dump($whereClose);return false;

  $query = 'SELECT '.$selectdClose.' FROM bookshop_documents as doc' . $whereClose;
  $query .= ' ORDER BY document_number ASC';

  //echo $query;

  //Execute the query
  global $dbranchbe;
  
  if(!$Rset = $dbranchbe->SelectLimit( $query , ($limit + 1) , $offset) ){
    print 'error in query: '.$dbranchbe->ErrorMsg().'<br />';
    echo $query . '<br />';
    return false;
  }else{
    $CountOutput = $Rset->RecordCount();
    $smarty->assign('CountOutput', $CountOutput);
    if($CountOutput >= $limit){
      global $search_return;
      $search_return = 'The max return of rows is reached('.$limit.'). Please, review your request.';
      }
    while ($row = $Rset->FetchRow()) {
      $All[] = $row;
    }
  }
  
  return $All;

}//End of function

//-----------------------------------------------------------------------------

//Construct the form with QuickForm lib
$form = new HTML_QuickForm('form', 'post');

//Set default values
$form->setDefaults(array(
  'numrows'      => '100',
));

//Numrow
$form->addElement('text', 'numrows', 'Nombre de resultats a afficher', array('size' => 4, 'maxlength' => 9));

//Designation
$form->addElement('text', 'designation', 'Description', array('size' => 50, 'maxlength' => 50));

//Number
$form->addElement('text', 'document_number', 'Num�ro du document', array('size' => 50, 'maxlength' => 50));

//Category
$params = array(
          'field_multiple' => false,
          'field_name' => 'category_id',
          'field_description' => 'categorie',
          'field_size' => '1',
          'return_name' => false,
);
construct_select_category($params , $form , $Manager);

//Reset and submit
$form->addElement('submit', 'submit', 'Search');

// Try to validate the form
if ($form->validate()) {
  // Form is validated, then process the request
  if ($_REQUEST['submit'] == 'Search'){
    $list=$form->process('prepaSearch', true);
    $smarty->assign_by_ref('list', $list);
  }
} //End of validate form

//Set the renderer for display QuickForm form in a smarty template
include 'QuickFormRendererSet.php';

//Assign name to particular fields
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number' );
$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description' );
if(isset($search_return))
  $smarty->assign('search_return' , $search_return );

$Manager->error_stack->checkErrors();

// Display the template
$smarty->assign('icons_dir', DEFAULT_DOCTYPES_ICONS_DIR);
$smarty->display('qinfo_am_search.tpl');

if($usr->getProperty('handle') == 'anonymous')
  $usr->logout(true);
