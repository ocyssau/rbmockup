<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once 'MDB2.php';
//require_once 'LiveUser/LiveUser.php';
require_once 'LiveUser/Admin.php';

// Please configure the following file according to your environment
//$dsn = '{dbtype}://{user}:{passwd}@{dbhost}/{dbname}';
//$LUdsn = 'mysql://root:@localhost/ranchbe';
//$db = MDB2::connect($LUdsn);

$db = MDB2::connect($LUdsn);

//global $dbranchbe;
//$db = $dbranchbe;

if (PEAR::isError($db)) {
    echo $db->getMessage() . ' ' . $db->getUserInfo();
}

$db->setFetchMode(MDB2_FETCHMODE_ASSOC);

// check if transaction are supported by the MDB2 driver (MDB2 is used by liveUser only)
if ( ! $db->supports('transactions') ) {
    print 'This driver dont support the transactions';
}else $trans = true;

$conf =
    array(
        'debug' => false,
        'session'  => array(
            'name'     => 'LuSession',
            'varname'  => 'ludata'
        ),
        'login' => array(
            'force'    => false,
        ),
        'logout' => array(
            'destroy'  => true,
        ),
        'authContainers' => array(
            'DB' => array(
                'type'          => 'MDB2',
                'expireTime'    => MAX_SESSION_LIFETIME, // max lifetime of a session in seconds
                'idleTime'      => MAX_IDLE_LIFETIME, // max time between 2 requests
                'storage' => array(
                    'dsn' => $LUdsn,
                    'alias' => array(
                        'lastlogin' => 'lastlogin',
                        'is_active' => 'is_active',
                        'email' => 'email',
                        'group_description' => 'group_description',
                    ),
                    'fields' => array(
                        'lastlogin' => 'timestamp',
                        'is_active' => 'boolean',
                        'email' => 'text',
                        'group_description' => 'text',
                    ),
                    'tables' => array(
                        'users' => array(
                            'fields' => array(
                                'lastlogin' => false,
                                'is_active' => false,
                                'email' => false,
                            ),
                        ),
                        'groups' => array(
                            'fields' => array(
                                'group_description' => false,
                            ),
                        ),
                    ),
                )
            )
        ),

    'permContainer' => array(
        'type' => 'Medium',
        'storage' => array('MDB2' =>  array('dsn' => $LUdsn,
                                            'prefix' => 'liveuser_',
                                            'tables' => array(
                                                'rights' => array(
                                                     'fields' => array(
                                                         'right_description' => false,
                                                      ),
                                                 ),
                                                'groups' => array(
                                                     'fields' => array(
                                                         'group_description' => false,
                                                      ),
                                                 ),
                                             ),
                                            'fields' => array(
                                                'right_description' => 'text',
                                                'group_description' => 'text',
                                             ),
                                            'alias' => array(
                                                'right_description' => 'right_description',
                                                'group_description' => 'group_description',
                                             ),
                                           ),
                          ),
                      ),
);

?>
