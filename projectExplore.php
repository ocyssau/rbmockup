<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//Setup
require_once './conf/ranchbe_setup.php';

//$_REQUEST['resetf'] = true; //Add v0.4.3 to destroy the filters record in session(and linked to this page)
//echo basename($_SERVER['PHP_SELF']);
//unset($_SESSION['projectExplore.php']);
//var_dump($_SESSION);

//Assign variables
$project_id = $_REQUEST['project_id'] ;
$smarty->assign('project_id', $project_id );

require_once './class/project.php'; //Class to manage the projects
$projectManager = project::_factory($project_id);

//Set default action
if ( empty( $_REQUEST['action'] ) ) $_REQUEST['action'] = 'listGroups';

//Manage actions
switch ($_REQUEST['action']){

//---------------------------------------------------------------
  case 'assignPerm' : //Assign permissions
    unset ($_REQUEST['action']);

    if ( empty($_REQUEST['area_id'])){
      //Get infos about project
      $projectInfos = $projectManager->GetProjectInfos( $_REQUEST['project_id'] );
      $project_area_id = $projectInfos['area_id'];
      $smarty->assign('project_area_id', $project_area_id);
    } else {
      $project_area_id = $_REQUEST['area_id'];
      $smarty->assign('project_area_id', $project_area_id );
    }
 
    include 'project_assignpermission.php';
  
    $smarty->assign('action', 'assignPerm');
  
    $smarty->assign('mid', 'projectExplore.tpl');
    $smarty->assign('subMid', 'project_assignpermission.tpl');
    $smarty->assign('Groups', 'active');
  
  break; //End of assign perm

//---------------------------------------------------------------
  
  case 'listGroups':   //List groups
    include 'project_admingroups.php';

    $smarty->assign('action', 'listGroups');

    $smarty->assign('mid', 'projectExplore.tpl');
    $smarty->assign('subMid', 'project_admingroups.tpl');
    $smarty->assign('Groups', 'active');
  
  break; //End of list groups

//---------------------------------------------------------------
  case 'listLinks' :   //List links objects
    include 'project_adminlinks.php';
    $smarty->assign('action', 'listLinks');
    $smarty->assign('mid', 'projectExplore.tpl');
    $smarty->assign('subMid', 'project_adminlinks.tpl');
    $smarty->assign('Links', 'active');
  break; //End of list links

//---------------------------------------------------------------
  case 'suppressLink' :   //Suppress links objects
    include 'project_adminlinks.php';
    $smarty->assign('action', 'suppressLink');
    $smarty->assign('mid', 'projectExplore.tpl');
    $smarty->assign('subMid', 'project_adminlinks.tpl');
    $smarty->assign('Links', 'active');
  break; //End of Suppress links

//---------------------------------------------------------------

  case 'linkWorkitem':   //Add a workitem to project
    $smarty->assign('message', 'Be careful : this workitems are maybe linked to anothers projects<br />
    If you link it to this project, he will be unlink from the original project.<br />
    ');
    $smarty->assign('action', 'linkWorkitem');
    include 'project_adminlinks.php';
    $smarty->display("project_addlink.tpl");
    die;
  break; //End of linkWorkitem

//---------------------------------------------------------------

  case 'linkMockup':   //Add a workitem to project
    $smarty->assign('action', 'linkMockup');
    include 'project_adminlinks.php';
    $smarty->display("project_addlink.tpl");
    die;
  break; //End of linkMockup

//---------------------------------------------------------------

  case 'linkPartner':   //Add a workitem to project
    $smarty->assign('action', 'linkPartner');
    include 'project_adminlinks.php';
    $smarty->display("project_addlink.tpl");
    die;
  break; //End of linkPartner

//---------------------------------------------------------------

  case 'linkLib':   //Add a workitem to project
    $smarty->assign('action', 'linkLib');
    include 'project_adminlinks.php';
    $smarty->display("project_addlink.tpl");
    die;
  break; //End of linkLib

//---------------------------------------------------------------
  
  case 'linkBib':   //Add a workitem to project
    $smarty->assign('action', 'linkBib');
    include 'project_adminlinks.php';
    $smarty->display("project_addlink.tpl");
    die;
  break; //End of linkBib

//---------------------------------------------------------------

  case 'linkDoctype':   //Add a process to project
    $smarty->assign('action', 'linkDoctype');
    include 'project_adminlinks.php';
    $smarty->display("project_addlink.tpl");
    die;
  break; //End of linkDoctype

//---------------------------------------------------------------

  case 'linkDefaultProcess':   //Add a process to project
    $smarty->assign('action', 'linkDefaultProcess');
    include 'project_adminlinks.php';
    die;
  break; //End of linkDefaultProcess

} //End of switch

// Display the template
$smarty->assign('projectUser', 'active');
$smarty->assign('projectsTab', 'active');
$smarty->display("ranchbe.tpl");

?>
