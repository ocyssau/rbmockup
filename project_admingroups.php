<?php

// $Header: /cvsroot/ranchbe/ranchbe/project_admingroups.php,v 1.4 2007/11/19 18:26:01 ranchbe Exp $

// Copyright (c) 2002-2005, Luis Argerich, Garland Foster, Eduardo Polidor, et. al.
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.

//Setup
require_once './conf/ranchbe_setup.php';
require_once './lib/userslib.php';

//Process the code for pagination and display management
//$default_sort_field = 'group_define_name';    //Default value of the field to sort
//$default_sort_order = 'ASC'; //Default value of the order ASC = ascendant, DESC = descandant
//Include generic definition of the code for manage filters
//include('filterManager.php');

//Display the group list
//$list = $userlib->get_groups($offset, $numrows, $sort_field, $sort_order, $find, $LUA);
$list = $userlib->get_groups(0, 9999, 'group_define_name', 'ASC', '', $LUA);
  
// Assign the list of groups
$smarty->assign_by_ref('users', $list);

//Include generic definition of the code for manage the pagination. $list must set...
//include('paginationManager.php');

//Assign misc variable for edit group
if ( isset($_REQUEST['editgroup']) && $_REQUEST['editgroup']){
  $edit_group_info = $userlib->get_group_info($_REQUEST['editgroup'], $LUA);
  $smarty->assign('editgroup',$_REQUEST['editgroup']);
  $smarty->assign('groupname',$edit_group_info[0]['group_define_name']);
  $smarty->assign('groupdesc',$edit_group_info[0]['group_description']);
  $smarty->assign('includegroup',$edit_group_info);
}

?>
