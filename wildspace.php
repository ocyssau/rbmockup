<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Ranchbe is distributed in the hope that it will be useful,                |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once './conf/ranchbe_setup.php';
require_once './class/wildspace.php';
$Manager = new wildspace();

$check_flood = check_flood();

//Manage actions on documents

switch ($_REQUEST['action']){

// ----- Suppress a file
case('SuppressFile'):
  if(!$check_flood) break;
  if (!empty($_REQUEST["checked"])){
    foreach($_REQUEST['checked'] as $file){
      //Todo : Add confirmation
      if($Manager->SuppressWildspaceFile($file))
        print '<em>file '.$file.' is suppress</em><br />';
    }
  }
break;

// ---- Rename file
// ---- Copy file
case 'renameQueryFile':
case 'copyQueryFile':
  if(!$check_flood) break;
  if( empty($_REQUEST['checked']) ) break;
  if( empty($_REQUEST['newName']) ) break;
  if( $_REQUEST['newName'] == 'null' ) break;
  require_once 'class/common/file.php';
  $ofile = new file( $Manager->GetPath().'/'.$_REQUEST['checked'] );
  if( $_REQUEST['action'] == 'renameQueryFile' )
    $ofile->rename($Manager->GetPath().'/'.$_REQUEST['newName']);
  if( $_REQUEST['action'] == 'copyQueryFile' )
    $ofile->copy($Manager->GetPath().'/'.$_REQUEST['newName'], 0666, false);
  break;

// ----- Upload a file
case ('upload'):
  $Manager->UploadFile($_FILES['uploadFile'] , $_REQUEST['overwrite']);
break;

// ----- Download a file
case ('downloadFile'):
  require_once('./class/common/attachment.php');
  $viewer = new viewer();
  $viewer->initRawFile($Manager->GetPath().'/'.$_REQUEST['checked'][0]);
  $viewer->pushfile();
break;

// ----- Download multiples files in a zip
case ('DownloadZip'):
  require_once('File/Archive.php');
  File_Archive::setOption('zipCompressionLevel', 9);
  if( is_array($_REQUEST['checked']) ){
    foreach($_REQUEST['checked'] as $file){
      $multiread[] = File_Archive::read($Manager->GetPath().'/'.$file, $file);
    }
  }else{
    break;
  }
  $reader = File_Archive::readMulti($multiread);
  $writer = File_Archive::toArchive('wildspace.zip',File_Archive::toOutput());
  File_Archive::extract($reader, $writer);
  die;
break;

// ----- extract files from archive in the wildspace.
case 'Uncompress':   // Uncompress the package
  if(empty($_REQUEST['checked'])){
    $Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item');
    break;
  }
  foreach ($_REQUEST['checked'] as $package){
    if(!is_file($Manager->GetPath().'/'.$package)) continue; //To prevent lost of data
    $extension = substr($package, strrpos($package, '.'));

    if(  $extension == '.Z' ){
      if(!exec(UNZIPCMD . " $package")){
        $Manager->error_stack->push(ERROR, 'Fatal', array('element'=>$package, 'debug'=>array()), 'cant uncompress this file %element%');
        break;
      }
    }

    if( $extension == '.adraw' ){
      $zip = new ZipArchive;
      if ($zip->open($Manager->GetPath().'/'.$package) === TRUE) {
          $zip->extractTo( $Manager->GetPath() );
          $zip->close();
      }else{
        $Manager->error_stack->push(ERROR, 'Error', array('element'=>$package), 'WARNING : can not uncompress file : %file%' );
      }
    break;
    }

    require_once 'File/Archive.php'; //File_Archive PEAR package extension
    $objSource =  File_Archive::read($Manager->GetPath().'/'.$package.'/', $Manager->GetPath().'/');
    if(PEAR::isError($objSource)){
      $Manager->error_stack->push(ERROR, 'Fatal', array(), $objSource->getMessage() .'<br />'. $objSource->getUserInfo());
      continue;
    }
    $objDest = File_Archive::toFiles();
    //var_dump($result);
    while($objSource->next()){ //for each unpack file
      $strThisFile = $objSource->getFilename();// Get the filename
      if(is_file($strThisFile) || is_dir($strThisFile)){ //Get the name of the unpack file
        $Manager->error_stack->push(INFO, 'Info', array(), 'FILE EXISTS;IS NOT REPLACED: '.$strThisFile);
        continue;
      }
      $Manager->error_stack->push(INFO, 'Info', array(), 'EXTRACTED: '.$strThisFile);
      //echo $strThisFile .'<br>';
      $objDest->newFile($strThisFile);//Create the file
      $objSource->sendData($objDest);//output the source data to it
    }
    $objSource->close();//Now close the reader
    $objDest->close();//Now close the writer
  }
break;

// ----- Get the UUID of a CATIA file
case ('CheckUUID'):
  if(empty($_REQUEST['checked'])){
    $Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item');
    $Manager->error_stack->checkErrors(array('close_button'=>true));
    die;
  }
  set_time_limit(30*60); //To increase default TimeOut.No effect if php is in safe_mode
  include_once ('./lib/CheckUUID.php');
  foreach($_REQUEST['checked'] as $filename){
    $file = $Manager->WILDSPACE .'/'. $filename;
    $smarty->assign('UUID', checkUUID( $file ));
    $smarty->display("checkuuid_result.tpl");
  }
  die;
break;
} // End of switch

//Include generic definition of the code for manage filters
$currentSpace = 'wildspace';
include('filterManager_simple.php');

$params['displayMd5'] = $_REQUEST['displayMd5'];
$smarty->assign('displayMd5',$params['displayMd5']);

//get infos on files
  $list = $Manager->GetDatas($params);
  $smarty->assign_by_ref('list', $list);

//Include definition of the code for manage the pagination and filters
include('paginationManager.php');

//Define select option for "find"
$all_field = array ('file_name' => 'file_name',
                    'designation' => 'designation',
                    'file_size' => 'file_size',
                    'file_path' => 'file_path',
                    'file_mtime' => 'file_mtime' ,
                    'file_md5' => 'file_md5',
                    'file_extension' => 'file_extension',
                    'file_racine' => 'file_racine',
                    'file_type' => 'file_type',
                    );
$smarty->assign('all_field', $all_field);

$Manager->error_stack->checkErrors();

$smarty->assign('currentUserId', $Manager->GetUserId());
$smarty->assign('currentUserName', $Manager->GetUserName());

// Display the template
  $_SESSION['myspace'] = array( 'activePage'=>$_SERVER['PHP_SELF'] );
  $smarty->assign('wildspaceTab', 'active');  // Active the tab

  $smarty->assign('mid', 'wildspace.tpl');
  $smarty->display("ranchbe.tpl");

?>
