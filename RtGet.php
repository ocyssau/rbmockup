<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once './conf/ranchbe_setup.php';

//error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_USER_WARNING);
//error_reporting( E_ALL );
//ini_set ( 'display_errors', 1 );
//ini_set ( 'display_startup_errors', 1 );

require_once './class/common/space.php';
require_once './class/Rt.php';
require_once('./GUI/GUI.php');
require_once 'HTML/QuickForm/livesearch_select.php';

class Form_Select extends HTML_QuickForm{
	function buildForm()
	{
		$this->_formBuilt = true;
		$this->addElement('header', null, 'Que voulez-vous faire?');

		$this->addElement('hidden', 'space', 'workitem');
		$this->addElement('submit', 'byPeriod', 'Voir les refus sur une p�riode');
		$this->addElement('submit', 'byDocument', 'Voir les refus pour un document');
	}

	/**
	 */
	function process()
	{
		if($_REQUEST['byPeriod']){
			  header('Location: /RtGet.php?action=GetRtForPeriod');
		}else if($_REQUEST['byDocument']){
			  header('Location: /RtGet.php?action=GetRtApplyToDocument');
		}
	} // end func process
}


class Form_GetRtForPeriod extends HTML_QuickForm{
	function buildForm()
	{
		$this->_formBuilt = true;
		$this->addElement('header', null, 'Visualiser les refus sur la p�riode');

		$date_params = array(
		            'field_name' => 'from_date',
		            'default_value' => time() - ( 3600 * 24 * 31 ),
		            'field_required' => true,
		);
		construct_select_date($date_params , $this);

		$date_params = array(
		            'field_name' => 'to_date',
		            'default_value' => time(),
		            'field_required' => true,
		);
		construct_select_date($date_params , $this);

		$this->addElement('hidden', 'action', $_REQUEST['action']);
		$this->addElement('submit', 'next', 'Next >>');
	}

	/**
	 * @return ADORecordSet
	 */
	function process()
	{
		$Rt = new Rt();
		$fromDate = $this->getSubmitValue('from_date');
		$toDate = $this->getSubmitValue('to_date');
		
		//Re-set value for display correct date in freeze form
		$this->setConstants(array('from_date' => $fromDate ));
		$this->setConstants(array('to_date' => $toDate ));
		
		$select = array('DISTINCT rt_id, rt_number, rt_status, rt_access_code, rt_open_date, from_number, from_version');
		$filter = 'rt_access_code < 15';
		return $Rt->GetRtForPeriod($fromDate, $toDate, false, true, $select, $filter);
	} // end func process
}

class Form_GetRtApplyToDocument extends HTML_QuickForm{
	
	/**
	 * 
	 * Enter description here ...
	 * @var document
	 */
	public $document;
	
	function buildForm()
	{
		$this->_formBuilt = true;
		$this->addElement('header', null, 'Visualiser les refus d\'un document');
		
		$lsSelectOptions = array('elementId' => 'documentls', //element id, name must be same that method in .class.php
                          'printStyle' => 0, //anything != 0 will render css inline(Default 1), 0 => the default style will not be rendered, you should put it in your style.css(XHTML fix)
                          'autoComplete' => 0, //if 0 the autocomplete attribute will not be set. Default not set;
                          'autoserverPath' => 'DocumentLiveSearchServer.php?q=%string%&inputid=documentls', //path to server scripts. Special word %string% indicate the input string parameter to search.
		);
		$this->addElement('livesearch_select', 'document', 'Document', $lsSelectOptions, array('size' => 32) );
		
		$this->addElement('hidden', 'space', 'workitem');
		$this->addElement('hidden', 'action', $_REQUEST['action']);
		$this->addElement('submit', 'next', 'Next >>');
	}

	/**
	 * @return ADORecordSet
	 */
	function process()
	{
		$Rt = new Rt();
		$document_id = $this->getSubmitValue('document');
		$space_name = $this->getSubmitValue('space');
		
		$space = new space($space_name);
		$this->document = new document($space, $document_id);
		
		$select = array('DISTINCT rt_id, rt_number, rt_status, rt_access_code, rt_open_date, from_number, from_version');
		$filter = 'rt_access_code < 15';
		return $Rt->GetRtApplyToDocument($this->document, false, true, $select, $filter);
	} // end func process
}


function GetRtForPeriod(){
	$form = new Form_GetRtForPeriod('GetRtForPeriod');
	$form->buildForm();
	global $smarty;
	
	// Try to validate the form
	if ( !$form->validate() ) {
		ob_start();
		$form->display();
		$htmlForm = ob_get_clean();
		$smarty->assign('midcontent', $htmlForm);
		$smarty->display('ImportRt.tpl');
		return;
	}
	
	$rs = $form->process();
	
	$i = 0;
	$list = array();
	while( $row = $rs->FetchRow() ){
		$list[$i] = $row;
		$list[$i]['rt_number'] = '<a href="./viewFile.php?document_id='.$row['rt_id'].'&space=workitem">' . $list[$i]['rt_number'] . '</a>';
		$list[$i]['rt_open_date'] = formatDate($row['rt_open_date']);
		$list[$i]['apply_to'] = $row['from_number'] . '.v' . $row['from_version'];
		unset($list[$i]['applyto_number'], $list[$i]['applyto_indice'], $list[$i]['applyto_version'], $list[$i]['applyto_id']);
		$i++;
	}
	
	$smarty->assign( 'headers', array_keys($list[0]) );
	$smarty->assign( 'list', $list );
	$smarty->assign( 'pageTitle', 'Refus sur la periode' );
	$smarty->assign( 'mid', 'RtGet_GetRtForPeriod.tpl' );
	$smarty->display('ranchbe.tpl');
}


function GetRtApplyToDocument(){
	$form = new Form_GetRtApplyToDocument('GetRtApplyToDocument');
	$form->buildForm();
	global $smarty;
		
	// Try to validate the form
	if ( !$form->validate() ) {
		ob_start();
		$form->display();
		$htmlForm = ob_get_clean();
		$smarty->assign('midcontent', $htmlForm);
		$smarty->display('ImportRt.tpl');
		return;
	}
	
	$rs = $form->process();
	
	$i = 0;
	$list = array();
	while( $row = $rs->FetchRow() ){
		$list[$i] = $row;
		$list[$i]['rt_number'] = '<a href="./viewFile.php?document_id='.$row['rt_id'].'&space=workitem">' . $list[$i]['rt_number'] . '</a>';
		$list[$i]['rt_open_date'] = formatDate($row['rt_open_date']);
		$list[$i]['apply_to'] = $row['applyto_number'] .'.v'.$row['applyto_indice'].'.'.$row['applyto_version'] . '('.$row['applyto_id'].')';
		unset($list[$i]['applyto_number'], $list[$i]['applyto_indice'], $list[$i]['applyto_version'], $list[$i]['applyto_id']);
		$i++;
	}
	
	$smarty->assign( 'headers', array_keys($list[0]) );
	$smarty->assign( 'list', $list );
	$smarty->assign( 'pageTitle', 'Refus pour le document '. $form->document->GetDocProperty('document_normalized_name') );
	$smarty->assign( 'mid', 'RtGet_GetRtForPeriod.tpl' );
	$smarty->display('ranchbe.tpl');
}


function Select(){
	$form = new Form_Select('Select');
	$form->buildForm();
	global $smarty;
	
	ob_start();
	$form->display();
	$htmlForm = ob_get_clean();
	
	$smarty->assign('midcontent', $htmlForm);
	$smarty->display('ImportRt.tpl');
	return;
}

if($_REQUEST['byPeriod']){
	  header('Location: RtGet.php?action=GetRtForPeriod');
}
else if($_REQUEST['byDocument']){
	  header('Location: RtGet.php?action=GetRtApplyToDocument');
}
else if($_REQUEST['action'] == 'GetRtForPeriod'){
	return GetRtForPeriod();
}
else if($_REQUEST['action'] == 'GetRtApplyToDocument'){
	return GetRtApplyToDocument();
}
else{
	return Select();
}
