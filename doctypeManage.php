<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//Setup
require_once('conf/ranchbe_setup.php');
require_once('HTML/QuickForm.php'); //Librairy to easily create forms
require_once('HTML/QuickForm/Renderer/ArraySmarty.php'); //Lib to use Smarty with QuickForm
require_once('class/doctype.php'); //Class to manage the doctypes

$Manager = new doctype; //Create new manager

//Construct the form with QuickForm lib
$form = new HTML_QuickForm('form', 'post');

$area_id = 1;

if ($_REQUEST['action'] == 'csvimport'){
  check_ticket ( 'doctype_create' , $area_id );
  $Manager->ImportDoctypesCsv($_FILES['csvlist']['tmp_name'] , $_REQUEST['overwrite']);
  unset($_REQUEST['action']);
}

//Set defaults values of elements if modify request
if ($_REQUEST['action'] == 'modify'){
  check_ticket ( 'doctype_modify' , $area_id );
  
  //Get infos
  $Infos = $Manager->GetInfos($_REQUEST['doctype_id']);
  
  foreach(explode(' ' , $Infos['file_type']) as $type){ //Create a array nfor set defautl value of the file_type field
    $file_type[$type] = true;
  }

  if(isset($file_type['file']))
    $smarty->assign('fileselect' , 1);

  $form->setDefaults(array(
    'doctype_number' => $Infos['doctype_number'],
    'doctype_description' => $Infos['doctype_description'],
    'can_be_composite' => $Infos['can_be_composite'],
    'script_post_store' => $Infos['script_post_store'],
    'script_pre_store' => $Infos['script_pre_store'],
    'script_pre_update' => $Infos['script_pre_update'],
    'script_post_update' => $Infos['script_post_update'],
    'recognition_regexp' => $Infos['recognition_regexp'],
    'file_type' => $file_type,
    'file_extension' => explode(' ' , $Infos['file_extension']),
    'visu_file_extension' => explode(' ' , $Infos['visu_file_extension']),
    //'file_type_ext' => array( explode(' ' , $Infos['file_type']) , explode(' ' , $Infos['file_extension'])),
    'icon' => $Infos['icon'],
    'submit' => 'modify',
  ));

  //Add hidden fields
  $form->addElement('hidden', 'doctype_id', $_REQUEST['doctype_id']);
  $form->addElement('hidden', 'action', 'modify');
  
  //Assign var 'action' to smarty to manage display
  $smarty->assign('action', 'modify');
}

//Set defaults values of elements if create request
if ($_REQUEST['action'] == 'create'){
  check_ticket ( 'doctype_create' , $area_id );

  $form->setDefaults(array(
    'submit'  => 'create',
  ));

  //Add hidden fields
   $form->addElement('hidden', 'action', 'create');

  //Assign var 'action' to smarty to manage display
   $smarty->assign('action', 'create');
}

//Add fields for input informations in all case
  $form->addElement('text', 'doctype_number', tra('Number'));
  $form->addElement('text', 'doctype_description', tra('Description'), array('size'=>20));
  $form->addElement('textarea', 'recognition_regexp', tra('recognition_regexp'), array('cols'=>60));

  $smarty->assign('number_help', DEFAULT_DOCTYPE_MASK_HELP);

  //Select can_be_composite
  $select =& $form->addElement('select', 'can_be_composite', array(tra('can_be_composite'), 'note'=>'' ), array(1=>'Yes' ,0=>'No') );
  $select->setSize(1);
  $select->setMultiple(false);

  //Set file type and file extension
  //$FILE_TYPE_LIST is defined in ranchbe_setup.php;
  //$FILE_EXTENSION_LIST is defined in ranchbe_setup.php;

//RanchBE0.4.3
  // Creates a checkboxes group to select type of document
  // $FILE_TYPE_LIST is defined in ranchbe_setup.php;
  foreach ( $FILE_TYPE_LIST as $filetype ){
    $checkbox[] = &HTML_QuickForm::createElement('checkbox', $filetype, null, $filetype, array('onclick' => "afficherExtensions('$filetype');"));
  }
  $form->addGroup($checkbox, 'file_type', 'File type', '<br />');

  //Create a select list to select extension of file
  $fileExtSelectSet=array_combine($FILE_EXTENSION_LIST['file'] , $FILE_EXTENSION_LIST['file']);
  $select =& $form->addElement('select', 'file_extension', array('File extension', 'note'=>'' ), $fileExtSelectSet );
  $select->setSize(5);
  $select->setMultiple(true);

  //Create a select list to select visu file extension
  $vfileExtSelectSet=array_combine($VISU_FILE_EXTENSION_LIST , $VISU_FILE_EXTENSION_LIST);
  $select =& $form->addElement('select', 'visu_file_extension', array('Visualisation file extension', 'note'=>'' ), $vfileExtSelectSet );
  $select->setSize(1);
  $select->setMultiple(false);

  //Construct array for set script_pre_store
  //DEFAULT_DOCTYPE_SCRIPTS_DIR is defined in ranchbe_setup.php;
  //$scriptsList = glob( DEFAULT_DOCTYPE_SCRIPTS_DIR .'/'. "pre_store*.php");
  $scriptsList = glob( DEFAULT_DOCTYPE_SCRIPTS_DIR .'/'. "*.php");
  $pre_store_SelectSet[NULL] = "";
  foreach ( $scriptsList as $scripts ){
    $scripts = basename($scripts);
    $pre_store_SelectSet["$scripts"] = "$scripts";
  }
  $select =& $form->addElement('select', 'script_pre_store', array(tra('script_pre_store'), 'note'=>'' ), $pre_store_SelectSet );
  $select->setSize(1);
  $select->setMultiple(false);

  //Construct array for set script_post_store
  //DEFAULT_DOCTYPE_SCRIPTS_DIR is defined in ranchbe_setup.php;
  //$scriptsList = glob( DEFAULT_DOCTYPE_SCRIPTS_DIR .'/'. "post_store*.php");
  $post_store_SelectSet[NULL] = "";
  foreach ( $scriptsList as $scripts ){
    $scripts = basename($scripts);
    $post_store_SelectSet["$scripts"] = "$scripts";
  }
  $select =& $form->addElement('select', 'script_post_store', array(tra('script_post_store'), 'note'=>'' ), $post_store_SelectSet );
  $select->setSize(1);
  $select->setMultiple(false);

  //Construct array for set script_pre_update
  //DEFAULT_DOCTYPE_SCRIPTS_DIR is defined in ranchbe_setup.php;
  //$scriptsList = glob( DEFAULT_DOCTYPE_SCRIPTS_DIR .'/'. "pre_update*.php");
  $pre_update_SelectSet[NULL] = "";
  foreach ( $scriptsList as $scripts ){
    $scripts = basename($scripts);
    $pre_update_SelectSet["$scripts"] = "$scripts";
  }
  $select =& $form->addElement('select', 'script_pre_update', array(tra('script_pre_update'), 'note'=>'' ), $pre_update_SelectSet );
  $select->setSize(1);
  $select->setMultiple(false);

  //Construct array for set script_post_update
  //DEFAULT_DOCTYPE_SCRIPTS_DIR is defined in ranchbe_setup.php;
  //$scriptsList = glob( DEFAULT_DOCTYPE_SCRIPTS_DIR .'/'. "post_update*.php");
  $post_update_SelectSet[NULL] = "";
  foreach ( $scriptsList as $scripts ){
    $scripts = basename($scripts);
    $post_update_SelectSet["$scripts"] = "$scripts";
  }
  $select =& $form->addElement('select', 'script_post_update', array(tra('script_post_update'), 'note'=>'' ), $post_update_SelectSet );
  $select->setSize(1);
  $select->setMultiple(false);

  //Construct array for set icon file
  //DEFAULT_DOCTYPES_ICONS_DIR is defined in ranchbe_setup.php;
  $iconsList = glob( DEFAULT_DOCTYPES_ICONS_DIR .'/'. "*.gif");
  $iconsList = array_merge ($iconsList , glob( DEFAULT_DOCTYPES_ICONS_DIR .'/'. "*.png"));
  sort($iconsList);

  $icon_SelectSet[NULL] = "";
  foreach ( $iconsList as $icon ){
    $icon = basename($icon);
    $icon_SelectSet["$icon"] = "$icon";
  }
  $select =& $form->addElement('select', 'icon', array(tra('icon'), 'note'=>'' ), $icon_SelectSet );
  $select->setSize(1);
  $select->setMultiple(false);

  //Submit buttons
  $form->addElement('reset', 'reset', 'reset');
  $form->addElement('submit', 'submit', 'Go');

//Add validation rules to check input data
  $mask = DEFAULT_DOCTYPE_MASK;
  $form->addRule('doctype_number', tra('Number is required'), 'required');
  $form->addRule('doctype_number', tra('Should contain letters, number, _ or - without accents'), 'regex', "/$mask/" , 'server');
  //$form->addRule('file_extension', tra('Extension is required'), 'required');
  $form->addRule('file_type', tra('Type is required'), 'required');

// Try to validate the form
if ($form->validate()) {
  $form->freeze(); //and freeze it

// Form is validate, then processes the modify request
  if ($_REQUEST[action] == 'modify'){
    check_ticket ( 'doctype_modify' , $area_id );
    $form->process('modifyDoctype', true); //
  }

// Form is validate, then processes the create request
  if ($_REQUEST[action] == 'create'){
    check_ticket ( 'doctype_create' , $area_id );
    $form->process('createDoctype', true); //
  }
} //End of validate form

function getValues($values){   // Get the data from the form and format it
  global $FILE_EXTENSION_LIST;
  //var_dump($values['file_extension']);
  //var_dump($values['file_type']);
  
  //unselect file extension list if type file is not selected
  if(!isset($values['file_type']['file']))
    $values['file_extension'] = array();

  //if type file is selected check if file_extension is set
  if(isset($values['file_type']['file']) && !is_array($values['file_extension'])){
    print (tra('Extension is required'));
    return false;
  }

  //Reformat the array send by form from the checkbox selection for file_type
  $values['file_type'] = array_keys($values['file_type']);

  //Add valid extension to file_extension list for type cadds, ps ...etc
  foreach($values['file_type'] as $filetype){
    if($filetype != 'file'){
      $values['file_extension'] = array_merge($values['file_extension'] , array_values($FILE_EXTENSION_LIST[$filetype]));
    }
  }

  //var_dump($values['file_extension']);
  return $values;
}

function modifyDoctype($values){   // Process the modify request

  global $Manager;
  global $smarty;
  
  $values = getValues($values);

  $Manager->Update($values , $_REQUEST['doctype_id']);
  //Assign var 'action' to smarty to manage display
  $smarty->assign('action', 'modify');
}

function createDoctype($values){   // Process the create request

  global $Manager;
  global $smarty;

  $values = getValues($values);

  $doctype_id = $Manager->Create($values);
  //Assign var 'action' to smarty to manage display
  $smarty->assign('action', 'create');
}

//Set the renderer for display QuickForm form in a smarty template
include 'QuickFormRendererSet.php';

$Manager->error_stack->checkErrors();

// Display the template
$smarty->assign('doctypesTab', 'active');
$smarty->assign('mid', 'doctypeManage.tpl');
$smarty->display('doctypeManage.tpl');
//$smarty->display('ranchbe.tpl');

?>
