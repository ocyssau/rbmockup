<?php
define ("DEBUG" , true); /*If true display debug informations.*/
define ("DEBUG_POPUP_LOG" , false); /*If true display log result in a popup.*/
define ("LOGFILE" , 'logs/ranchbe.log'); /*Path to log file.*/
define ("DESACTIVATE" , false); /*if true display an advertissement when user try to connect*/

define ("DEFAULT_LANG" , 'fr'); /*Language to use*/

define ("DB_HOST" , 'localhost'); /* hostname of database server*/
define ("DB_USER" , 'root'); /* Database user */
define ("DB_PASS" , ''); /* Password */
define ("DB_NAME" , 'ranchbe0-5-2'); /* Database table name*/

/*
define ("AUTHORIZED_DIR_0" , '');
define ("AUTHORIZED_DIR_1" , '');
define ("AUTHORIZED_DIR_2" , '');
define ("AUTHORIZED_DIR_3" , '');
*/

//Define extension to determine cadds4 type
$FILE_TYPE_LIST['cadds4'] = 'cadds4'; //Never change this value
$FILE_EXTENSION_LIST['cadds4'] = array('_pd'=>'_pd'); //Valid extension for type cadds4

//Define extension to determine cadds5 type
$FILE_TYPE_LIST['cadds5'] = 'cadds5'; //Never change this value
$FILE_EXTENSION_LIST['cadds5'] = array('_fd'=>'_fd'); //Valid extension for type cadds5

//Define extension to determine camu type
$FILE_TYPE_LIST['camu'] = 'camu'; //Never change this value
$FILE_EXTENSION_LIST['camu'] = array('_db'=>'_db'); //Valid extension for type camu

//Define extension to determine pstree type
$FILE_TYPE_LIST['pstree'] = 'pstree'; //Never change this value
$FILE_EXTENSION_LIST['pstree'] = array('_ps'=>'_ps'); //Valid extension for type pstree

//Define extension to determine adrawc4 type
$FILE_TYPE_LIST['adrawc4'] = 'adrawc4'; //Never change this value
$FILE_EXTENSION_LIST['adrawc4'] = array('_pd'=>'_pd'); //Valid extension for type adrawc4

//Define extension to determine adrawc5 type
$FILE_TYPE_LIST['adrawc5'] = 'adrawc5'; //Never change this value
$FILE_EXTENSION_LIST['adrawc5'] = array('_fd'=>'_fd'); //Valid extension for type adrawc5

//Define extension to determine adrawc4 type
$FILE_TYPE_LIST['zipadrawc4'] = 'zipadrawc4'; //Never change this value
$FILE_EXTENSION_LIST['zipadrawc4'] = array('.adrawc4'=>'.adrawc4'); //Valid extension for type adrawc4

//Define extension to determine adrawc5 type
$FILE_TYPE_LIST['zipadrawc5'] = 'zipadrawc5'; //Never change this value
$FILE_EXTENSION_LIST['zipadrawc5'] = array('.adrawc5'=>'.adrawc5'); //Valid extension for type adrawc5



?>
