<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

//PEAR::setErrorHandling(PEAR_ERROR_RETURN);

$usr = LiveUser::singleton($conf);

if( empty($_REQUEST['space']) && isset($_SESSION['SelectedContainerType']) )
  $_REQUEST['space'] = $_SESSION['SelectedContainerType'];
if( empty($_REQUEST['SelectedContainer']) && isset($_SESSION['SelectedContainer']) )
  $_REQUEST['SelectedContainer'] = $_SESSION['SelectedContainer'];
if( empty($_REQUEST['SelectedContainerNum']) && isset($_SESSION['SelectedContainerNum']) )
  $_REQUEST['SelectedContainerNum'] = $_SESSION['SelectedContainerNum'];

//Tries to retrieve the auth object from session and checks possible timeouts.
if (!$usr->init()) {
    var_dump($usr->getErrors());
    die();
}

//var_dump($usr);
//var_dump($_REQUEST);

$handle = (array_key_exists('handle', $_REQUEST)) ? $_REQUEST['handle'] : null;
$passwd = (array_key_exists('passwd', $_REQUEST)) ? $_REQUEST['passwd'] : null;
$logout = (array_key_exists('logout', $_REQUEST)) ? $_REQUEST['logout'] : false;

if ($logout) { //if user request logout
  $usr->logout(true);

} elseif( !$usr->isLoggedIn() ) { //user is not login
  if (!$handle) { //none handle specifed
    $usr->login(null, null, true);
  } else { //try to connect user
    if( $usr->login($handle, $passwd) ){
      $init_context = true; //use by access validation in ranchbe_setup.php
    }
  }
} //else user_log($usr->getProperty('handle'));

$LUA =& LiveUser_Admin::factory($conf);
$LUA->init();

//Function to record the user activities and log connection
//function user_log($handle){
//  echo $handle . ' is login';
//}

/*
* Ckeck permissions
* If secure = true, die the scripts if user has not the permission
* else return just false
* 
* $area int or array :if is array, search rignt for all areas defined in this array
* Note that only on $right_define_name is extract from a set of area defined by array.
* So is not a inheritance definition.
*/
function check_ticket($right_define_name , $areas=1 , $secure=true){
  global $usr;
  global $LUA;
  global $smarty;

  if( !is_array($areas) ){
    $areas = array($areas);
  }

  $filter['filters'] = array('right_define_name' => $right_define_name , 'area_id' => $areas );
  $filter['fields'] = array('right_id');
  $rights = $LUA->perm->getRights($filter);

  if(isset($rights[0]['right_id'])){
    $right_id =  $rights[0]['right_id'];
    //Check permissions
    if(!$usr->checkRight($right_id)){
      if($secure){
        $smarty->assign('msg', tra('You are not authorized to process this request'));
        $smarty->display("error.tpl");
        die;
      }
      else return false;
    }else return true;
  }else{
    if(DEBUG)
      print 'not defined right: ' . $right_define_name . '<br />';
    return false;
  }

} //End of check ticket

/*
* Serialize the $_request and create hidden fom
*/
function serialize_request_post(){
  $html ='';
  foreach( $_REQUEST as $key=>$val ){
    if($key == 'logout' || $key == 'handle' || $key == 'passwd' || $key == 'handle' || $key == 'tab' ||
    $key == 'kt_language' || $key == 'tz_offset' || $key == 'username' || $key == 'ml_id' || $key == 'PHPSESSID' || $key == 'LuSession'
    ){
      continue;
    }
    if( is_array($val) ){
      foreach( $val as $sub_key=>$sub_sval ){
        $html .= '<input type="hidden" name="'.$key.'['.$sub_key.']'.'" value="'.$sub_sval.'" />'."\n";
      }
    }else{
      $html .= '<input type="hidden" name="'.$key.'" value="'.$val.'" />'."\n";
    }
  }
  return $html;
} //End of function


function check_flood($page_id='') {
//Anti-flood V3. Condition existance du ticket en session. Tous les tickets sont conservés en session jusqu'a leur utilisation.
//Inspired by principle described in http://www.developpez.net/forums/archive/index.php/t-104783.html
//If session jeton is egal to jeton record in form, generate a new jeton.
//session_register('jeton');

  global $smarty;

  //echo 'page id :'.$page_id .'<br>';
  //echo '<pre>REQUEST :';echo $_REQUEST['ticket'];echo '</pre>';
  //echo '<pre>SESSION :';var_dump($_SESSION['ticket']);echo '</pre>';
  //echo '<pre>NB TICKET IN SESSION :';echo count($_SESSION['ticket']);echo '</pre>';

  //Check time between 2 requests
  if (time() - $_SESSION['ticket_time'] < 1){ // If the page is recall after time < to one second
    print '<b>You cant doing two request in less than 1 second<br />Wait 5 second and refresh this page...</b>';
    $_SESSION['ticket_time'] = time(); //echo 'execution trop rapproche<br>';
    die;
  }else{
    $_SESSION['ticket_time'] = time();
  }

  if(empty($_REQUEST['ticket'])){ //init the ticket
    $ticket = md5(uniqid(mt_rand(), true));
    $_SESSION['ticket'][$ticket] = true;
    $smarty->assign('ticket', $ticket); //Regenerate a ticket and assign it to page
    return false;
  }

  if (isset($_SESSION['ticket'][$_REQUEST['ticket']])){ //Check if ticket is in SESSION
    unset($_SESSION['ticket'][$_REQUEST['ticket']]); // Suppress ticket in session
    $ticket = md5(uniqid(mt_rand(), true)); // generate a new ticket
    $_SESSION['ticket'][$ticket] = true; // Record new ticket in session
    $smarty->assign('ticket', $ticket); // Assign it to page
    return true;
  }else{
    $ticket = md5(uniqid(mt_rand(), true)); // generate a new ticket
    $_SESSION['ticket'][$ticket] = true; // Record new ticket in session
    $smarty->assign('ticket', $ticket); // Assign it to page
    //$smarty->assign('ticket', $_REQUEST['ticket']); // Reassign ticket to page
    return false;
  }
  
  return true;

} //End of check flood

?>
