<?php

// $Header: /cvsroot/ranchbe/ranchbe/conf/setup_multilang.php,v 1.2 2008/08/09 17:37:47 ranchbe Exp $

// Copyright (c) 2002-2005, Luis Argerich, Garland Foster, Eduardo Polidor, et. al.
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.

//this script may only be included - so its better to die if called directly.
if (strpos($_SERVER["SCRIPT_NAME"],basename(__FILE__)) !== false) {
  header("location: index.php");
  exit;
	die();
}

/** translate a English string
 * @param $content - English string
 * @param $lg - language - if not specify = global current language
 */

function tra($content, $lg='') {
    global $language;

        if ($lg == "" || $lg == $language) {
         global $lang;
         include_once("lang/$language/language.php");
        }
        else
           include ("lang/$lg/language.php");
        if ($content) {
            if (isset($lang[$content])) {
                return $lang[$content];
            } else {
                if(DEBUG){
                  $handle=fopen("lang/$language/toTraduct.txt", 'a');
                  fwrite($handle, $content.PHP_EOL);
                  fclose($handle);
                }
                return $content;
            }
        }
}
?>
