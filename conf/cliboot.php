<?php
//session_start();
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

/*
 1> define constantes : include local_setup
 2> create adodb object
 3> create LiveUser and LiveUserAdmin objects
 4> get user preferences
 5> setup multi language translation functions and array
 6> create $logger object
 7> create $error_stack object
 8> create smarty object
 */

define ("RANCHBE_VER" , '0.6'); /* Current version of ranchbe */
define ("RANCHBE_BUILD" , ''); /* Build number of ranchbe*/
define ("RANCHBE_COPYRIGHT" , '&#169;2007-2008 by ranchbe group'); /* Copyright */

define ("LOG" , 'LOG');

//** include path
set_include_path('.'. PATH_SEPARATOR . './lib/adodb'. PATH_SEPARATOR .'./lib/PEAR');

//Disable php errors display
ini_set('display_errors' , 1);

//set the default charset
ini_set('default_charset' , 'iso-8859-1');

//Disable auto register global of sessions variables.
//Note that register_globals must be set to false too.
ini_set('session.bug_compat_42', false);

//echo 'register_globals = ' . ini_get('register_globals') . "\n";
if(ini_get('register_globals') == true){
	print 'Before use RanchBE you must set Register_global php directive to off in php.ini file.
  You can too set this directive in .htaccess file. See php documentation.';
	die();
}

//Check the client browser
if (stristr($_SERVER['HTTP_USER_AGENT'] , 'windows')){
	define('CLIENT_OS', 'WINDOWS');
	define('PHP_EOL', '\r\n');
}
else if (stristr($_SERVER['HTTP_USER_AGENT'] , 'macintosh') || stristr($_SERVER['HTTP_USER_AGENT'] , 'mac_powerpc')){
	define('CLIENT_OS', 'MACINTOSH');
	define('PHP_EOL', '\r');
}
else{
	define('CLIENT_OS', 'UNIX');
	define('PHP_EOL', '\n');
}


//------------------------------------------------------------------------------
//Include local configuration
//------------------------------------------------------------------------------
require_once('/etc/rbmockup.conf');
require_once('conf/default.conf');



define(CSS_SHEET , DEFAULT_CSS_SHEET);
define(LANG , DEFAULT_LANG);
define(LONG_DATE_FORMAT , DEFAULT_LONG_DATE_FORMAT);
define(SHORT_DATE_FORMAT , DEFAULT_SHORT_DATE_FORMAT);
define(USE_JSCALENDAR , DEFAULT_USE_JSCALENDAR);
define(CHARSET , DEFAULT_CHARSET);
define(TIME_ZONE , DEFAULT_TIME_ZONE);
define(MAX_RECORD , DEFAULT_MAX_RECORD);


//set_include_path( PEAR_LIB_PATH . PATH_SEPARATOR . get_include_path() . PATH_SEPARATOR . ZEND_LIB_PATH );
set_include_path( get_include_path() . PATH_SEPARATOR . ZEND_LIB_PATH );

//------------------------------------------------------------------------
//Define path mapping
//------------------------------------------------------------------------
/* $path_mapping is used to translate path from server to path use by the client.
 * You can use %user% wich will be replaced by the username
 */
switch (CLIENT_OS) {
	// Windows
	case 'WINDOWS':
		$path_mapping = array(DEFAULT_MOCKUP_DIR=>MOCKUP_WINDOWS_PATH_MAPPING,
		DEFAULT_BOOKSHOP_DIR=>BOOKSHOP_WINDOWS_PATH_MAPPING,
		DEFAULT_CADLIB_DIR=>CADLIB_WINDOWS_PATH_MAPPING,
		DEFAULT_WORKITEM_DIR=>WORKITEM_WINDOWS_PATH_MAPPING,
		DEFAULT_WILDSPACE_DIR=>WILSPACE_WINDOWS_PATH_MAPPING
		);
		break;
		// Mac
	case 'MACINTOSH':
		$path_mapping = array(DEFAULT_MOCKUP_DIR=>MOCKUP_MAC_PATH_MAPPING,
		DEFAULT_BOOKSHOP_DIR=>BOOKSHOP_MAC_PATH_MAPPING,
		DEFAULT_CADLIB_DIR=>CADLIB_MAC_PATH_MAPPING,
		DEFAULT_WORKITEM_DIR=>WORKITEM_MAC_PATH_MAPPING,
		DEFAULT_WILDSPACE_DIR=>WILSPACE_MAC_PATH_MAPPING
		);
		break;
		// Unix
	case 'UNIX':
		$path_mapping = array(DEFAULT_MOCKUP_DIR=>MOCKUP_UNIX_PATH_MAPPING,
		DEFAULT_BOOKSHOP_DIR=>BOOKSHOP_UNIX_PATH_MAPPING,
		DEFAULT_CADLIB_DIR=>CADLIB_UNIX_PATH_MAPPING,
		DEFAULT_WORKITEM_DIR=>WORKITEM_UNIX_PATH_MAPPING,
		DEFAULT_WILDSPACE_DIR=>WILSPACE_UNIX_PATH_MAPPING
		);
		break;
}

//------------------------------------------------------------------------
//Check that necessary directories exists
//------------------------------------------------------------------------
if (!is_dir (DEFAULT_WILDSPACE_DIR) ) { print 'Error : DEFAULT_WILDSPACE_DIR  do not exits'; die;}
if (!is_dir (DEFAULT_WORKITEM_DIR) ) { print 'Error : DEFAULT_WORKITEM_DIR  do not exits'; die;}
if (!is_dir (DEFAULT_MOCKUP_DIR) ) { print 'Error : DEFAULT_MOCKUP_DIR  do not exits'; die;}
if (!is_dir (DEFAULT_CADLIB_DIR) ) { print 'Error : DEFAULT_CADLIB_DIR  do not exits'; die;}
if (!is_dir (DEFAULT_BOOKSHOP_DIR) ) { print 'Error : DEFAULT_BOOKSHOP_DIR  do not exits'; die;}
if (!is_dir (DEFAULT_WILDSPACE_DIR) ) { print 'Error : DEFAULT_WILDSPACE_DIR  do not exits'; die;}
if (!is_dir (DEFAULT_TRASH_DIR) )  { print 'Error : DEFAULT_TRASH_DIR  do not exits'; die;}
if (!is_dir (DEFAULT_IMPORT_DIR) )  { print 'Error : DEFAULT_IMPORT_DIR  do not exits'; die;}
if (!is_dir (UNPACK_IMPORT_DIR) )    { print 'Error : UNPACK_IMPORT_DIR  do not exits'; die;}

//------------------------------------------------------------------------
//Authorized files types
//------------------------------------------------------------------------
//Extract file extension from mimes.conf file. This list restrict possibility of document creation from this file type
$valid_file_ext = array(); //init var
if(!$handle = fopen('conf/mimes.csv', "r")){
	print 'cant open conf/mimes.csv file';
	die;
}else{
	while( !feof($handle) ) {
		$data = fgetcsv($handle, 1000, ";");
		if(@implode(' ',$data)){ //Test for ignore empty lines
			$extensions = explode( ' ',$data[2] );
			$valid_file_ext = array_merge($valid_file_ext, $extensions);
		}
	}
	fclose ($handle);
}

$FILE_TYPE_LIST['file'] = 'file';
$FILE_EXTENSION_LIST['file'] = array_combine($valid_file_ext, $valid_file_ext);  //Valid extension for type file
sort($FILE_EXTENSION_LIST['file']);
unset($valid_file_ext);

//Define extension to determine no file type
$FILE_TYPE_LIST['nofile'] = 'nofile'; //Never change this value
$FILE_EXTENSION_LIST['nofile'] = array('NULL'=>''); //Valid extension for type nofile

$DEFAULT_LIFE_TIME = DEFAULT_LIFE_TIME ; //default life duration in days for all objects. This value permit calcul of the default forseen close date.

$VISU_FILE_EXTENSION_LIST = explode( ';', VISU_FILE_EXTENSION_LIST );
$VISU_FILE_EXTENSION_LIST = array_combine($VISU_FILE_EXTENSION_LIST,$VISU_FILE_EXTENSION_LIST);
sort($VISU_FILE_EXTENSION_LIST);

//------------------------------------------------------------------------
//Set debug mode
//------------------------------------------------------------------------
//Disable php errors display
if(DEBUG === true){
	/*
	 * To use xdebug, install module for php from http://www.xdebug.org
	 * Add this line to end of php ini file :
	 * [XDebug]
	 zend_extension_ts=[path to dll or module for unix] example: "C:\wamp\bin\php\php5.2.5\ext\php_xdebug-2.0.3-5.2.5.dll"
	 xdebug.remote_enable=true
	 xdebug.remote_host=127.0.0.1
	 xdebug.remote_port=9000
	 xdebug.remote_handler=dbgp
	 xdebug.profiler_enable=0 // 1 to enable profiler
	 xdebug.profiler_output_dir=[path to dir where store profiler logfile] example "c:\tmp\"
	 xdebug.auto_trace=1
	 xdebug.trace_output_dir=[path to dir where store logfile] example "c:\tmp\"
	 *
	 */
	ini_set('display_errors' , true);
	ini_set('xdebug.show_local_vars', 1);
	ini_set('xdebug.var_display_max_data', 512);
	ini_set('xdebug.var_display_max_children', 128); //number of array elements or object properties that xdebug displays
	ini_set('xdebug.var_display_max_depth', 15); //three nested levels of array elements and object relations are displayed
	//ini_set('xdebug.max_nesting_level', 100); //three nested levels of array elements and object relations are displayed
	ini_set('xdebug.dump_undefined', 'on'); //Display undefined var too
	ini_set('xdebug.dump.POST', '*'); //Select superglobal var to dump, * to dump all superglobal - use by xdebug_dump_superglobals();
	ini_set('xdebug.dump.GET', '*'); //Select superglobal var to dump, * to dump all superglobal - use by xdebug_dump_superglobals();
	ini_set('xdebug.collect_params' , 4); //controls whether Xdebug should collect the parameters passed to functions when a function call is recorded in either the function trace or the stack trace
	ini_set('xdebug.collect_return' , 1); //controls whether Xdebug should write the return value of function calls to the trace files.
	ini_set('xdebug.show_exception_trace' , 'on');
	ini_set('xdebug.trace_format' , 0); //1: computer readable format / 0: shows a human readable indented trace file with: time index, memory usage, memory delta (if the setting xdebug.show_mem_delta is enabled), level, function name, function parameters (if the setting xdebug.collect_params is enabled, filename and line number.
}

//------------------------------------------------------------------------
//Create Adodb object
//------------------------------------------------------------------------
function create_adodb($db_consult_only=false){
	/*Database connexion parameters*/
	$adodb_driver   = ADODB_DRIVER;
	$host_ranchbe   = DB_HOST;
	$dbs_ranchbe    = DB_NAME;
	if($db_consult_only){ //read only access user
		$user_ranchbe   = DB_SELECTONLY_USER;
		$pass_ranchbe   = DB_SELECTONLY_PASS;
	}else{ //Full access user
		$user_ranchbe   = DB_USER;
		$pass_ranchbe   = DB_PASS;
	}

	define ("ADODB_LANG" , LANG); /*Language to use for adodb*/
	include_once ('adodb.inc.php');
	$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;//This is a global variable that determines how arrays are retrieved by recordsets.See Adodb manual.

	//$dsn = "$adodb_driver://$user_ranchbe:$pass_ranchbe@$host_ranchbe/$dbs_ranchbe";
	$dbranchbe = &ADONewConnection($adodb_driver); //Create a new Adodb object

	//Connect to database
	if ( !$dbranchbe->Connect($host_ranchbe, $user_ranchbe, $pass_ranchbe, $dbs_ranchbe) ) {
		print "RanchBE is not properly set up: Unable to connect to the database !" . PHP_EOL;
		print $dbranchbe->ErrorMsg() . PHP_EOL;
		die;
	}

	if(PROFILING_LOG || PROFILING_DISPLAY){
		$dbranchbe->LogSQL(); // turn on logging
		define('ADODB_PERF_NO_RUN_SQL',1); //Desactive allow users to enter and run SQL interactively via the "Run SQL" link.
	}

	if(ADODB_DEBUG){
		$dbranchbe->debug = true;
	}

	return $dbranchbe;
}

if(!isset($dbranchbe)){
	$dbranchbe =& create_adodb(false);
}

//End of setup adodb

//------------------------------------------------------------------------
//Setup multilang
//------------------------------------------------------------------------
$language = LANG;
require_once ('./conf/setup_multilang.php');

//------------------------------------------------------------------------
//Define the rights associated to PROJECTS (dont change this)
//------------------------------------------------------------------------
function GetInContextRight(){
	$IN_CONTEXT_RIGHTS = array(
    'container_document_get'=> tra('container_document_get'),
    'container_document_manage'=> tra('container_document_manage'),
    'container_document_suppress'=> tra('container_document_suppress'),
    'container_document_move' => tra('container_document_move'),
    'container_document_copy' => tra('container_document_copy'),
    'container_document_assoc'=> tra('container_document_assoc'),
    'container_document_unlock'=> tra('container_document_unlock'),
    'container_document_archive'=> tra('container_document_archive'),
    'container_document_change_indice'=> tra('container_document_change_indice'),
    'container_document_change_state'=> tra('container_document_change_state'),
    'container_document_change_number'=> tra('container_document_change_number'), //v0.4.3
    'container_document_link_file'=> tra('container_document_link_file'),
    'container_document_history'=> tra('container_document_history'),
    'container_document_version'=> tra('container_document_version'),
    'container_document_doctype_reset' => tra('container_document_doctype_reset'), //v0.5
	);
	return $IN_CONTEXT_RIGHTS;
}

//------------------------------------------------------------------------
//Create LiveUser object
//------------------------------------------------------------------------
//Set the driver for MDB2.Liveuser use MDB2 in replacement of adodb
switch (ADODB_DRIVER){
	case 'mysqlt':
		$mdb2_driver = 'mysql';
		break;
	case 'postgres8':
		$mdb2_driver = 'pgsql';
		break;
}
$LUdsn = $mdb2_driver.'://'.DB_USER.':'.DB_PASS.'@'.DB_HOST.'/'.DB_NAME;
require_once './conf/LUconf.php';

//Create LiveUser object and check authentification
require_once './conf/createlu.php';


//------------------------------------------------------------------------
//Create logger object from PEAR::Log
//------------------------------------------------------------------------
require_once 'Log.php';
$conf = array('title' => 'RanchBE Log Output');
$logger =& Log::singleton('composite');

if(LOGFILE !== 0){
	$filelogger =& Log::singleton('file', LOGFILE, 'ranchbe [cli] ['.time().']');
	$logger->addChild($filelogger);
}

if(DEBUG_POPUP_LOG){
	$winlogger =& Log::singleton('win', 'LogWindow', 'ranchbe', $conf); //$handler, $name, $ident, $conf, $maxLevel
	$logger->addChild($winlogger);
}

//------------------------------------------------------------------------
//Create error manager $error_stack object
//------------------------------------------------------------------------
require_once './class/common/basic.php'; //Class basic
$error_stack =& new errorManager('ranchbe');
if(is_object($logger))
$error_stack->setLogger($logger);

//Callback function to chose type of return log or push or push and log
function userPushCallback($err)
{
	switch($err['code']){
		case LOG:
			return PEAR_ERRORSTACK_LOG; //log only
			break;
		default:
			return; //push and log
			break;
	} // switch
}

$error_stack->pushCallback('userPushCallback');


// Forget db info so that malicious PHP may not get password etc.
unset ($host_ranchbe);
unset ($user_ranchbe);
unset ($pass_ranchbe);
unset ($dbs_ranchbe);
unset ($LUdsn);

$cache_usualName = array(); //this var is used by ranchbe smarty plugin modifier to translate id from database in usual name.

//Default max rows to display
$maxRecords = MAX_RECORD;

