<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//Current space is the script name for discriminate display parameters for each page
if(isset($_REQUEST['page_id']))
  $currentSpace = $_REQUEST['page_id'];
else
  $currentSpace = basename($_SERVER['PHP_SELF']);

//Destroy session var to reset filter
if(isset($_REQUEST['resetf'])){
  unset($_SESSION[$currentSpace]);
  unset($_REQUEST);
}

//$sameurl_elements is array use by smarty function 'sameurl'. This array is used to defined witch values of $_REQUEST var must be add to url filtered by sameurl function.
$sameurl_elements = array('space');
$smarty->assign_by_ref('sameurl_elements', $sameurl_elements);

//------------------------------------------------------------------------------------------------
// Process the Sort and Limitation items to display
//------------------------------------------------------------------------------------------------

//Process the sort request
  if(!empty($_REQUEST['sort_field'])){
    $sort_field = $_REQUEST['sort_field'];
    $sort_order = $_REQUEST['sort_order'];
  }else{
    if(isset($_SESSION[$currentSpace]['sort_field']) && isset($_SESSION[$currentSpace]['sort_order'])){
      $sort_field = $_SESSION[$currentSpace]['sort_field'];
      $sort_order = $_SESSION[$currentSpace]['sort_order'];
    }else{
    	$sort_field = $default_sort_field;    //Default value of the field to sort
    	$sort_order = $default_sort_order;    //Default value of the order ASC = ascendant, DESC = descandant
    }
  }
  $smarty->assign_by_ref('sort_order', $sort_order);
  $smarty->assign_by_ref('sort_field', $sort_field);
  $params['sort_field'] = $sort_field;
  $params['sort_order'] = $sort_order;
  $_SESSION[$currentSpace]['sort_field'] = $sort_field;
  $_SESSION[$currentSpace]['sort_order'] = $sort_order;
  $sameurl_elements[] = 'sort_field';//For reused filter when call same url
  $sameurl_elements[] = 'sort_order';//For reused filter when call same url

//Offset of rows to display for pagination
  if(isset($_REQUEST['offset'])){
    $offset = $_REQUEST['offset'];
  }else{ 
    if(isset($_SESSION[$currentSpace]['offset'])){
      $offset = $_SESSION[$currentSpace]['offset'];
    }else $offset = 0;
  }
  $smarty->assign_by_ref('offset',$offset);
  $sameurl_elements[] = 'offset';//For reused filter when call same url
  $_SESSION[$currentSpace]['offset'] = $offset;//Record filter in session
  $params['offset'] =& $offset;

//Limitation of number rows to display
  if(isset($_REQUEST['numrows'])){
    $numrows = rtrim($_REQUEST['numrows']);
    //Check change and force offset to 0 if change
  	if($_REQUEST['numrows'] != $_SESSION[$currentSpace]['numrows'])
      $offset = 0;
  }else if(isset($_SESSION[$currentSpace]['numrows'])){
      $numrows = $_SESSION[$currentSpace]['numrows'];
  }else $numrows = $maxRecords;
  $smarty->assign('numrows',$numrows);
  $sameurl_elements[] = 'numrows';//For reused filter when call same url
  $_SESSION[$currentSpace]['numrows'] = $numrows;//Record filter in session
  $params['numrows'] = $numrows;

//------------------------------------------------------------------------------------------------
//Standard document filter
//------------------------------------------------------------------------------------------------
//Filter to apply
  if(isset($_REQUEST['displayHistory'])){
    $displayHistory = $_REQUEST['displayHistory'];
    //Check change and force offset to 0 if change
  	if($_REQUEST['displayHistory'] != $_SESSION[$currentSpace]['displayHistory'])
      $offset = 0;
  }else if(isset($_SESSION[$currentSpace]['displayHistory'])){
      $displayHistory = $_SESSION[$currentSpace]['displayHistory'];
  }else $displayHistory = false;
  $smarty->assign('displayHistory',$displayHistory);
  $sameurl_elements[] = 'displayHistory';//For reused filter when call same url
  $_SESSION[$currentSpace]['displayHistory'] = $displayHistory;//Record filter in session

//Filter to apply
  if(isset($_REQUEST['onlyMy'])){
    $onlyMy = $_REQUEST['onlyMy'];
    //Check change and force offset to 0 if change
  	if($_REQUEST['onlyMy'] != $_SESSION[$currentSpace]['onlyMy'])
      $offset = 0;
  }else{
    if(isset($_SESSION[$currentSpace]['onlyMy']))
      $onlyMy = $_SESSION[$currentSpace]['onlyMy'];
  }
  if($onlyMy == 1){
    $params['where'][] = '`check_out_by` = '. $usr->getProperty('auth_user_id') .' OR '.
    '`update_by` = '. $usr->getProperty('auth_user_id') .' OR '.
    '`open_by` = '. $usr->getProperty('auth_user_id')
    ;
  }
  $smarty->assign('onlyMy', $onlyMy);
  $sameurl_elements[] = 'onlyMy';//For reused filter when call same url
  $_SESSION[$currentSpace]['onlyMy'] = $onlyMy;//Record filter in session


//Filter to apply
  if(isset($_REQUEST['checkByMe'])){
    $checkByMe = $_REQUEST['checkByMe'];
    //Check change and force offset to 0 if change
  	if($_REQUEST['checkByMe'] != $_SESSION[$currentSpace]['checkByMe'])
      $offset = 0;
  }else{
    if(isset($_SESSION[$currentSpace]['checkByMe']))
      $checkByMe = $_SESSION[$currentSpace]['checkByMe'];
  }
  if($checkByMe == 1){
    $params['exact_find']['check_out_by'] = $usr->getProperty('auth_user_id');
  }
  $smarty->assign('checkByMe', $checkByMe);
  $sameurl_elements[] = 'checkByMe';//For reused filter when call same url
  $_SESSION[$currentSpace]['checkByMe'] = $checkByMe;//Record filter in session


//Filter to apply
  if(isset($_REQUEST['displayThumbs'])){
    $displayThumbs = $_REQUEST['displayThumbs'];
  }else{
    if(isset($_SESSION[$currentSpace]['displayThumbs']))
      $displayThumbs = $_SESSION[$currentSpace]['displayThumbs'];
  }
  $smarty->assign('displayThumbs', $displayThumbs);
  $sameurl_elements[] = 'displayThumbs';//For reused filter when call same url
  $_SESSION[$currentSpace]['displayThumbs'] = $displayThumbs;//Record filter in session


//Filter to apply
  if(isset($_REQUEST['find_document_number'])){
    $searchNumber = $_REQUEST['find_document_number'];
    //Check change and force offset to 0 if change
  	if($_REQUEST['find_document_number'] != $_SESSION[$currentSpace]['find_document_number'])
      $offset = 0;
  }else{
    if(isset($_SESSION[$currentSpace]['find_document_number']))
      $searchNumber = $_SESSION[$currentSpace]['find_document_number'];
  }
  if(!empty($searchNumber)){
    $params['find']['document_number'] = $searchNumber;
  }
  $smarty->assign('find_document_number', $searchNumber);
  $sameurl_elements[] = 'find_document_number';//For reused filter when call same url
  $_SESSION[$currentSpace]['find_document_number'] = $searchNumber;//Record filter in session

//Filter to apply
  if(isset($_REQUEST['find_document_designation'])){
    $searchDesignation = $_REQUEST['find_document_designation'];
    //Check change and force offset to 0 if change
  	if($_REQUEST['find_document_designation'] != $_SESSION[$currentSpace]['find_document_designation'])
      $offset = 0;
  }else{
    if(isset($_SESSION[$currentSpace]['find_document_designation']))
      $searchDesignation = $_SESSION[$currentSpace]['find_document_designation'];
  }
  if(!empty($searchDesignation)){
    $params['find']['designation'] = $searchDesignation;
  }
  $smarty->assign('find_document_designation', $searchDesignation);
  $sameurl_elements[] = 'find_document_designation';//For reused filter when call same url
  $_SESSION[$currentSpace]['find_document_designation'] = $searchDesignation;//Record filter in session

//Filter to apply
  if(isset($_REQUEST['find_doctype'])){
    $searchDoctype = $_REQUEST['find_doctype'];
    //Check change and force offset to 0 if change
  	if($_REQUEST['find_doctype'] != $_SESSION[$currentSpace]['find_doctype'])
      $offset = 0;
  }else{
    if(isset($_SESSION[$currentSpace]['find_doctype']))
      $searchDoctype = $_SESSION[$currentSpace]['find_doctype'];
  }
  if(!empty($searchDoctype)){
    $params['with']['table'] = 'doctypes';
    $params['with']['col'] = 'doctype_id';
    $params['find']['doctype_number'] = $searchDoctype;
  }
  $smarty->assign('find_doctype', $searchDoctype);
  $sameurl_elements[] = 'find_doctype';//For reused filter when call same url
  $_SESSION[$currentSpace]['find_doctype'] = $searchDoctype;//Record filter in session

//Filter to apply
  if(isset($_REQUEST['find_document_access_code'])){
    $searchAccessCode = $_REQUEST['find_document_access_code'];
    //Check change and force offset to 0 if change
  	if($_REQUEST['find_document_access_code'] != $_SESSION[$currentSpace]['find_document_access_code'])
      $offset = 0;
  }else{
    if(isset($_SESSION[$currentSpace]['find_document_access_code']))
      $searchAccessCode = $_SESSION[$currentSpace]['find_document_access_code'];
  }
  if(!empty($searchAccessCode)){
    if ($searchAccessCode == 'free')
      $params['exact_find']['document_access_code'] = 0;
    else
      $params['exact_find']['document_access_code'] = $searchAccessCode;
  }
  $smarty->assign('find_document_access_code', $searchAccessCode);
  $sameurl_elements[] = 'find_document_access_code';
  $_SESSION[$currentSpace]['find_document_access_code'] = $searchAccessCode;

//Filter to apply
  if(isset($_REQUEST['find_document_state'])){
    $searchState = $_REQUEST['find_document_state'];
    //Check change and force offset to 0 if change
  	if($_REQUEST['find_document_state'] != $_SESSION[$currentSpace]['find_document_state'])
      $offset = 0;
  }else{
    if(isset($_SESSION[$currentSpace]['find_document_state']))
      $searchState = $_SESSION[$currentSpace]['find_document_state'];
  }
  if(!empty($searchState)){
    $params['find']['document_state'] = $searchState;
  }
  $smarty->assign('find_document_state', $searchState);
  $_SESSION[$currentSpace]['find_document_state'] = $searchState;
  $sameurl_elements[] = 'find_document_state';

//Filter to apply
  if(isset($_REQUEST['find_category'])){
    $searchCategory = $_REQUEST['find_category'];
    //Check change and force offset to 0 if change
  	if($_REQUEST['find_category'] != $_SESSION[$currentSpace]['find_category'])
      $offset = 0;
  }else{
    if(isset($_SESSION[$currentSpace]['find_category']))
      $searchCategory = $_SESSION[$currentSpace]['find_category'];
  }
  if(!empty($searchCategory)){
    $params['exact_find']['category_id'] = $searchCategory;
  }
  $smarty->assign('find_category', $searchCategory);
  $_SESSION[$currentSpace]['find_category'] = $searchCategory;
  $sameurl_elements[] = 'find_category';

//------------------------------------------------------------------------------------------------
// Advanced document filter
//------------------------------------------------------------------------------------------------
//Filter to apply - check button state advanced search
if(isset($_REQUEST['f_adv_search_cb'])){
  $advSearch = $_REQUEST['f_adv_search_cb'];
}else{
  if(isset($_SESSION[$currentSpace]['f_adv_search_cb']))
    $advSearch = $_SESSION[$currentSpace]['f_adv_search_cb'];
}
if($advSearch == 1){
  $smarty->assign('f_adv_search_cb',$advSearch);
  $sameurl_elements[] = 'f_adv_search_cb';//For reused filter when call same url

  //Filter to apply
  if( isset($_REQUEST['find']) && isset($_REQUEST['find_field']) ){
    if(!empty($_REQUEST['find']) && !empty($_REQUEST['find_field'])){
      $find = rtrim($_REQUEST['find']);
      $find_field = rtrim($_REQUEST['find_field']);
      //Check change and force offset to 0 if change
    	if($_REQUEST['find'] != $_SESSION[$currentSpace]['find'])
        $offset = 0;
    }
  }else{
    if(!empty($_SESSION[$currentSpace]['find']) && !empty($_SESSION[$currentSpace]['find_field'])){
      $find = $_SESSION[$currentSpace]['find'];
      $find_field = $_SESSION[$currentSpace]['find_field'];
    }
  }
  if(isset($find) && isset($find_field)){
    $smarty->assign('find', $find);
    $smarty->assign('find_field', $find_field);
    $sameurl_elements[] = 'find';//For reused filter when call same url
    $sameurl_elements[] = 'find_field';//For reused filter when call same url
    $_SESSION[$currentSpace]['find'] = $find;//Record filter in session
    $_SESSION[$currentSpace]['find_field'] = $find_field;//Record filter in session
    $params['find'][$find_field] = $find;
  }

  //Filter to apply - action and user
  if( isset($_REQUEST['f_action_user_name']) && isset($_REQUEST['f_action_field']) ){
    if(!empty($_REQUEST['f_action_user_name']) && !empty($_REQUEST['f_action_field'])){
      $find_user = $_REQUEST['f_action_user_name'];
      $find_action = $_REQUEST['f_action_field'];
      //Check change and force offset to 0 if change
    	if($_REQUEST['f_action_user_name'] != $_SESSION[$currentSpace]['f_action_user_name'])
        $offset = 0;
      //Check change and force offset to 0 if change
    	if($_REQUEST['f_action_field'] != $_SESSION[$currentSpace]['f_action_field'])
        $offset = 0;
    }
  }else
    if(!empty($_SESSION[$currentSpace]['f_action_user_name']) && !empty($_SESSION[$currentSpace]['f_action_field'])){
      $find_user = $_SESSION[$currentSpace]['f_action_user_name'];
      $find_action = $_SESSION[$currentSpace]['f_action_field'];
  }

  if(isset($find_user) && isset($find_action)){
    $smarty->assign('f_action_user_name', $find_user);
    $smarty->assign('f_action_field', $find_action);
    $_SESSION[$currentSpace]['f_action_user_name'] = $find_user;
    $_SESSION[$currentSpace]['f_action_field'] = $find_action;
    $sameurl_elements[] = 'f_action_user_name';//For reused filter when call same url
    $sameurl_elements[] = 'f_action_field';//For reused filter when call same url
    $params['find'][$find_action] = $find_user;
  }else{
    unset($_SESSION[$currentSpace]['f_action_user_name'] );
    unset($_SESSION[$currentSpace]['f_action_field'] );
  }

  //Set option of date search
  //Filter to apply - check button date and time

  if(isset($_REQUEST['f_dateAndTime_cb'])){
    $advSearchDate = $_REQUEST['f_dateAndTime_cb'];
  }else{
    if(isset($_SESSION[$currentSpace]['f_dateAndTime_cb']))
      $advSearchDate = $_SESSION[$currentSpace]['f_dateAndTime_cb'];
  }
  if($advSearchDate == 1){
    $smarty->assign('f_dateAndTime_cb',$advSearchDate);
    $sameurl_elements[] = 'f_dateAndTime_cb';//For reused filter when call same url
    $_SESSION[$currentSpace]['f_dateAndTime_cb'] = $advSearchDate;//Record filter in session

    if( $_REQUEST['f_check_out_date_sel'] == 1 && !empty($_REQUEST['f_check_out_date']) ){
      $params['where']['check_out_date'] = 'check_out_date ' . $_REQUEST['f_check_out_date_cond'] .' '. $_REQUEST['f_check_out_date'];
    }else{unset($params['where']['check_out_date']);}

    if( $_REQUEST['f_update_date_sel'] == 1 && !empty($_REQUEST['f_update_date']) )
      $params['where']['update_date'] = 'update_date ' . $_REQUEST['f_update_date_cond'] .' '. $_REQUEST['f_update_date'];
    else
      unset($params['where']['update_date']);

    if( $_REQUEST['f_open_date_sel'] == 1 && !empty($_REQUEST['f_open_date']) )
      $params['where']['open_date'] = 'open_date ' . $_REQUEST['f_open_date_cond'] .' '. $_REQUEST['f_open_date'];
    else
      unset($params['where']['open_date']);

    if( $_REQUEST['f_close_date_sel'] == 1 && !empty($_REQUEST['f_close_date']) )
      $params['where']['close_date'] = 'close_date ' . $_REQUEST['f_close_date_cond'] .' '. $_REQUEST['f_close_date'];
    else
      unset($params['where']['close_date']);

    if( $_REQUEST['f_fsclose_date_sel'] == 1 && !empty($_REQUEST['f_fsclose_date']) )
      $params['where']['forseen_close_date'] = 'forseen_close_date ' . $_REQUEST['f_fsclose_date_cond'] .' '. $_REQUEST['f_fsclose_date'];
    else
      unset($params['where']['fsclose_date']);

  $sameurl_elements[] = 'where';//For reused filter when call same url
  }else unset($params['where']['check_out_date'],$params['where']['update_date'],$params['where']['open_date'],$params['where']['close_date'],$params['where']['forseen_close_date']);

} //End of if($advSearch == 1)

$_SESSION[$currentSpace]['f_adv_search_cb'] = $advSearch;//Record filter in session
$_SESSION[$currentSpace]['f_dateAndTime_cb'] = $advSearchDate;//Record filter in session
$_SESSION[$currentSpace]['f_action_user_name'] = $find_user;
$_SESSION[$currentSpace]['f_action_field'] = $find_action;
$_SESSION[$currentSpace]['find'] = $find;//Record filter in session
$_SESSION[$currentSpace]['find_field'] = $find_field;//Record filter in session

//Define junction
  if(is_array($all_field['with'])){
    if(array_key_exists($_REQUEST['find_field'] , $all_field['with'])){
      $params['with']['table'] = $all_field['with'][$_REQUEST['find_field']]['table'];
      $params['with']['col'] = $all_field['with'][$_REQUEST['find_field']]['col'];
    }
  }

?>
