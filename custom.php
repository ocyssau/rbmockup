<?php

require_once './conf/ranchbe_setup.php';

//error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_USER_WARNING);
error_reporting(E_ALL ^ E_NOTICE);
ini_set ( 'display_errors', 1 );
ini_set ( 'display_startup_errors', 1 );

$script = './custom/' . basename( $_REQUEST['module'] );
if( is_file($script) ){
	include $script;
}else{
	echo $script . ' is not existing or it can not be reach <br />';
	echo 'Example of use: custom.php?module=myCustomScript.php <br />';
}
