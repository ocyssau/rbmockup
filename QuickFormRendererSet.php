<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

 require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm

//Set the renderer for display QuickForm form in a smarty template
    $renderer =& new HTML_QuickForm_Renderer_ArraySmarty($smarty, true);
    $renderer->setRequiredTemplate(
       '{if $error}
            <font color="red">{tr}{$label|upper}{/tr}</font>
        {else}
            {$label}
            {if $required}
                <font color="red" size="1">*</font>
            {/if}
        {/if}'
        );
    
    $renderer->setErrorTemplate(
       '{if $error}
            <font color="orange" size="1">{$error}</font><br />
        {/if}{$html}'
        );
    
    $form->accept($renderer);

    // assign array with form data
    $smarty->assign('form', $renderer->toArray());

?>
