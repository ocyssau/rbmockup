<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once './conf/ranchbe_setup.php';//ranchBE configuration
require_once './class/common/container.php'; //Class to manage the container

if( !isset($_REQUEST['space']) && isset($_SESSION['SelectedContainerType']) )
  $_REQUEST['space'] = $_SESSION['SelectedContainerType'];

if( ( !isset($_REQUEST['SelectedContainer']) && !isset($_SESSION['SelectedContainer']) )
    || ( !isset($_REQUEST['space']) && isset($_SESSION['SelectedContainerType']) )
  )
  { //if none selected container redirect to accueil
  unset($_REQUEST);
  include './accueil.php';
}

if ($_REQUEST['documentDetail'] == 1){
  require_once './class/common/space.php';
  require_once './class/common/document.php';
  $space = new space($_REQUEST['space']);
  $odocument = new document($space);
  include './inc/documentDetailWindow.php';
}else{
    $Manager =& container::_factory($_REQUEST['space'] , $_REQUEST['SelectedContainer']); //Create new manager
    $space =& $Manager->space;
  if($Manager->GetProperty('file_only') == 1)
    include './inc/recordfileManage.php';
  else 
    if($_REQUEST['docfileManage'] == true)
      include './inc/docfileManage.php';
    else
      include './inc/documentManage.php';
}

?>
