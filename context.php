<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Ranchbe is distributed in the hope that it will be useful,                |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Ranchbe; if not, write to the Free Software                    |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once './conf/ranchbe_setup.php';

//Assign name to particular fields
$smarty->assign('area_id', $_SESSION['area_id']);
$smarty->assign('project_number', $_SESSION['SelectedProjectNum']);
$smarty->assign('project_id', $_SESSION['SelectedProject']);
$smarty->assign('container_number', $_SESSION['SelectedContainerNum']);
$smarty->assign('container_id', $_SESSION['SelectedContainer']);
$smarty->assign('container_type', $_SESSION['SelectedContainerType']);

//echo '<pre>';
//var_dump($_SESSION['links']);
//echo '</pre>';

function generateSearchOrder(){

  require_once('./class/wildspace.php');
  $wildspace = new wildspace;
  $paths = array($wildspace->GetPath());

  global $path_mapping;
  global $usr;
  
  $path_mapping = str_replace( '%user%' ,  $usr->getProperty('handle') , $path_mapping);
  $path_mapping = array_flip($path_mapping);
  $path_mapping = str_replace( '%user%' ,  $usr->getProperty('handle') , $path_mapping);
  $path_mapping = array_flip($path_mapping);

  $search = array_keys($path_mapping);
  $replace = array_values($path_mapping);

  foreach (array('workitem','mockup','cadlib') as $container_type){
    if(is_array($_SESSION['links'][$container_type]))
    foreach($_SESSION['links'][$container_type] as $container){
      $paths[] =  $container['default_file_path'];
    }
  }
  $res = str_replace( $search ,  $replace , $paths);
  if(CLIENT_OS == 'WINDOWS')
    $res = str_replace( '/' ,  '\\' , $res);
  return $res;
} //End of function

function generateSearchOrderCatia(){
  $SearchOrder = generateSearchOrder();
  return implode(PHP_EOL, $SearchOrder);
} //End of function

if ($_REQUEST['action'] == 'GenerateCATIASearchOrder'){
  $searchOrder = generateSearchOrderCatia();
  $smarty->assign('searchOrder', $searchOrder);
  $smarty->assign('CLIENT_OS', CLIENT_OS);
}

if ($_REQUEST['action'] == 'save'){
  if(CLIENT_OS == 'WINDOWS')
    $_REQUEST['searchOrder'] = str_replace( '\\\\' ,  '\\' , $_REQUEST['searchOrder']);
  header("Content-disposition: attachment; filename=".$_SESSION['SelectedProjectNum'].'_SearchOrder.txt');
  header("Content-Type: " . 'text/plain');
  header("Content-Transfer-Encoding: searchOrder\n"); // Surtout ne pas enlever le \n
  header("Content-Length: ".strlen($_REQUEST['searchOrder']));
  header("Pragma: no-cache");
  header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
  header("Expires: 0");
  print $_REQUEST['searchOrder'] . PHP_EOL;
  return true;
}

//Get detail on each object
if ( isset ($_SESSION['objectList'] ))
  foreach ( $_SESSION['objectList'] as $object ){
    $type = $object['object_class'];
    if ($type == 'process')
      $list[] = array ( 'number' => $object['name'] ,
                        'type' => $type ,
                        'id' => $object['pId'], );
    else
      $list[] = array ( 'number' => $object[$type.'_number'] ,
                        'type' => $type ,
                        'id' => $object[$type.'_id'], );
  }// End of foreach

$smarty->assign_by_ref('list', $list);

$smarty->assign('HeaderCol1', 'Type');
$smarty->assign('HeaderCol2', 'Number');
$smarty->assign('HeaderCol3', 'Id');

$smarty->assign('id', 'id');

$smarty->assign('col1', 'type');
$smarty->assign('col2', 'number');
$smarty->assign('col3', 'id');

$smarty->display('header.tpl');
$smarty->display("context.tpl");

if(DEBUG){
  echo '<hr /><br/><b>DEBUG : Dump of the session :</b><br />';
  echo '<pre>';var_dump($_SESSION);echo '</pre>';
}

?>
