<?php
die;
require_once './conf/ranchbe_setup.php';//ranchBE configuration
require_once './class/common/container.php'; //Class to manage the container

//**************************************************************
//Rename bookshop
//**************************************************************
$container = container::_factory('bookshop'); //Create new manager
$space =& $container->space;

$list = $container->GetAll();

foreach($list as $cont){
  $container_id = $cont['bookshop_id'];
  $container->init($container_id);
  $default_file_path = $container->GetProperty('default_file_path');
  if( is_dir($default_file_path.'/versions') ){
    echo 'rename : '.$default_file_path.'/versions<br />';
    rename($default_file_path.'/versions', $default_file_path.'/__versions');
  }
  if( is_dir($default_file_path.'/indices') ){
    echo 'rename : '.$default_file_path.'/indices<br />';
    rename($default_file_path.'/indices', $default_file_path.'/__indices');
  }

}

//**************************************************************
//Rename workitem
//**************************************************************
$container = container::_factory('workitem'); //Create new manager
$space =& $container->space;

$list = $container->GetAll();

foreach($list as $cont){
  $container_id = $cont['workitem_id'];
  $container->init($container_id);
  $default_file_path = $container->GetProperty('default_file_path');
  if( is_dir($default_file_path.'/versions') ){
    echo 'rename : '.$default_file_path.'/versions<br />';
    rename($default_file_path.'/versions', $default_file_path.'/__versions');
  }
  if( is_dir($default_file_path.'/indices') ){
    echo 'rename : '.$default_file_path.'/indices<br />';
    rename($default_file_path.'/indices', $default_file_path.'/__indices');
  }

}

//**************************************************************
//Rename mockup
//**************************************************************
$container = container::_factory('mockup'); //Create new manager
$space =& $container->space;

$list = $container->GetAll();

foreach($list as $cont){
  $container_id = $cont['mockup_id'];
  $container->init($container_id);
  $default_file_path = $container->GetProperty('default_file_path');
  if( is_dir($default_file_path.'/versions') ){
    echo 'rename : '.$default_file_path.'/versions<br />';
    rename($default_file_path.'/versions', $default_file_path.'/__versions');
  }
  if( is_dir($default_file_path.'/indices') ){
    echo 'rename : '.$default_file_path.'/indices<br />';
    rename($default_file_path.'/indices', $default_file_path.'/__indices');
  }

}


//**************************************************************
//Rename cadlib
//**************************************************************
$container = container::_factory('cadlib'); //Create new manager
$space =& $container->space;

$list = $container->GetAll();

foreach($list as $cont){
  $container_id = $cont['cadlib_id'];
  $container->init($container_id);
  $default_file_path = $container->GetProperty('default_file_path');
  if( is_dir($default_file_path.'/versions') ){
    echo 'rename : '.$default_file_path.'/versions<br />';
    rename($default_file_path.'/versions', $default_file_path.'/__versions');
  }
  if( is_dir($default_file_path.'/indices') ){
    echo 'rename : '.$default_file_path.'/indices<br />';
    rename($default_file_path.'/indices', $default_file_path.'/__indices');
  }

}


?>
