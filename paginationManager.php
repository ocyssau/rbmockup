<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+
  
if($numrows != 0){
//Process the pagination
$cant_pages = (count($list) / $numrows); //Number of find line / Number of max rows to display
$smarty->assign('cant_pages', $cant_pages);
$smarty->assign('actual_page', 1 + ($offset / $numrows));
if ($cant_pages = 1) {
	$smarty->assign('next_offset', $offset + $numrows); //if cant_page is = 1 we supppose that there are other items
}
if ($offset > 0) {
	$smarty->assign('prev_offset', $offset - $numrows); //if offset is > 0 we suppose that they are previous page
} else {
	$smarty->assign('prev_offset', -1);
}

if (!is_int (1 + ($offset / $numrows) )) { //if the page number is a decimal, there is probably a problem...
  $smarty->assign('actual_page', 'pagination error');
  $smarty->assign('offset', 0);
	$smarty->assign('next_offset', $numrows);
	$smarty->assign('prev_offset', 0);
  $offset = 0;
}
}

?>
