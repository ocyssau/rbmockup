<?php

// $Header: /cvsroot/ranchbe/ranchbe/tiki-admingroups.php,v 1.4 2007/11/19 18:27:03 ranchbe Exp $

// Copyright (c) 2002-2005, Luis Argerich, Garland Foster, Eduardo Polidor, et. al.
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.

//Setup
require_once './conf/ranchbe_setup.php';

//Require function to administrate user and group permissions
require_once './lib/userslib.php';
$area_id = $userlib->AREA_ID;

// Process the form to add a group
if (isset($_REQUEST["newgroup"]) and $_REQUEST["name"]) {
  check_ticket( 'group_create' , $area_id );

	// Check if the user already exists
	if ($userlib->group_exists($_REQUEST["name"],$LUA)) {
		$smarty->assign('msg', tra("Group already exists"));
		$smarty->display("error.tpl");
		die;

	} else {

	$userlib->add_group( $_REQUEST["name"], $_REQUEST["desc"] , $LUA);
	if (isset($_REQUEST["include_groups"])) {
		foreach ($_REQUEST["include_groups"] as $include) {
			if ($_REQUEST["name"] != $include) {
				$userlib->group_inclusion($_REQUEST["name"], $include);
			}
		}
	}
}

	$_REQUEST["group"] = $_REQUEST["name"];
	//$logslib->add_log('admingroups','created group '.$_REQUEST["group"]);
}

// Process the form to modify a group
if (isset($_REQUEST["save"]) and isset($_REQUEST["olgroup"]) and !empty($_REQUEST["name"])) {
  check_ticket( 'group_modify' , $area_id );
	$userlib->change_group($_REQUEST["olgroup"],$_REQUEST["name"],$_REQUEST["desc"],$LUA);

	$_REQUEST["group"] = $_REQUEST["name"];
	//$logslib->add_log('admingroups','modified group '.$_REQUEST["olgroup"].' to '.$_REQUEST["group"]);
}

// Process a form to remove a group
if ($_REQUEST["action"] == 'delete') {
  check_ticket( 'group_suppress' , $area_id );
	if (! $userlib->remove_group($_REQUEST['group'] , $LUA ) ) {
	 print 'error delete group : Cant delete this group ' . $_REQUEST['group'];
  }
	unset($_REQUEST['group']);
}

if ($_REQUEST["action"] == 'remove') {
  check_ticket( 'group_assign' , $area_id );
	if ($feature_ticketlib2 != 'y' or (isset($_POST['daconfirm']) and isset($_SESSION["ticket_$area"]))) {
		$userlib->remove_permission_from_group($_REQUEST["permission"], $_REQUEST["group"]);
		$logslib->add_log('admingroups','removed permission '.$_REQUEST["permission"].' from group '.$_REQUEST["group"]);
  }
}


//Process the code for pagination and display management
$default_sort_field = 'group_define_name';    //Default value of the field to sort
$default_sort_order = 'ASC'; //Default value of the order ASC = ascendant, DESC = descandant
include('filterManager.php');

//Display the group list
  $users = $userlib->get_groups($offset, $numrows, $sort_field, $sort_order, $find, $LUA);

//Include generic definition of the code for manage the pagination. $list must set...
include('paginationManager.php');

//To manage position in page to display
if (isset($_REQUEST['add'])) {
$smarty->assign('add','1');
}

//Display or not the members of groups
if ($_REQUEST['group'] and isset($_REQUEST['show'])) {
	$memberslist = $userlib->get_group_users($_REQUEST['group']);
} else {
	$memberslist = '';
}

$smarty->assign('memberslist',$memberslist);

// Assign the list of groups
$smarty->assign_by_ref('users', $users);

//Assign misc variable for edit group
if ( $_REQUEST[editgroup] ){
  $edit_group_info = $userlib->get_group_info($_REQUEST[editgroup], $LUA);
  var_dump ($edit_group_info);
  $smarty->assign('editgroup',$_REQUEST[editgroup]);
  $smarty->assign('groupname',$edit_group_info[0][group_define_name]);
  $smarty->assign('groupdesc',$edit_group_info[0][group_description]);
  $smarty->assign('includegroup',$edit_group_info);
}

// Active the onglet
$smarty->assign('usersTab', 'active');
// Display the template
$smarty->assign('mid', 'tiki-admingroups.tpl');
$smarty->display("ranchbe.tpl");

?>
