<?php

//Destroy session var to reset filter
if(isset($_REQUEST['resetf'])){
  unset($_SESSION[$currentSpace]);
  unset($_REQUEST);
}

//Process the sort request
/*
$smarty->assign('sort_order', $_REQUEST["sort_order"]);
$smarty->assign('sort_field', $_REQUEST["sort_field"]);
$params['sort_field'] = $_REQUEST["sort_field"];
$params['sort_order'] = $_REQUEST["sort_order"];
*/

//Process the sort request
  if(!empty($_REQUEST['sort_field'])){
    $sort_field = $_REQUEST['sort_field'];
    $sort_order = $_REQUEST['sort_order'];
  }else{
  	$sort_field = $default_sort_field;    //Default value of the field to sort
  	$sort_order = $default_sort_order;    //Default value of the order ASC = ascendant, DESC = descandant
  }
  $smarty->assign_by_ref('sort_order', $sort_order);
  $smarty->assign_by_ref('sort_field', $sort_field);
  $params['sort_field'] = $sort_field;
  $params['sort_order'] = $sort_order;

//Limitation of number rows to display
if (!isset($_REQUEST['numrows']) || empty($_REQUEST['numrows'])){
 	$numrows = $maxRecords; //Default value
}else{
  $_REQUEST['numrows'] = rtrim($_REQUEST['numrows']);
	$numrows = $_REQUEST['numrows']; // Max rows to display request value
}
$smarty->assign('numrows', $numrows);
$params['numrows'] = "$numrows";

//Offset of rows to display for pagination
if (!isset($_REQUEST['offset'])){
	$offset = 0; //Default value
}else{
	$offset = $_REQUEST['offset']; //Offset request value
}
$smarty->assign('offset', $offset);
$params['offset']     = $offset;

//Filter to apply
/*
if (empty($_REQUEST['find'])){
	unset($params['find']); //Default value
}else{
  if(!empty($_REQUEST['find']) && !empty($_REQUEST['find_field'])){
    $find = rtrim($_REQUEST['find']);
    $find_field = rtrim($_REQUEST['find_field']);
    $params['find'][$find_field] = $find;
  }else unset($params['find']);
}
$smarty->assign('find', $find);
$smarty->assign('find_field', $find_field);
*/

//Filter to apply
if( isset($_REQUEST['find']) && isset($_REQUEST['find_field']) ){
	if(!empty($_REQUEST['find']) && !empty($_REQUEST['find_field'])){
		$find = rtrim($_REQUEST['find']);
		$find_field = rtrim($_REQUEST['find_field']);
		//Check change and force offset to 0 if change
		if($_REQUEST['find'] != $_SESSION[$currentSpace]['find'])
		$offset = 0;
	}
}else{
	if(!empty($_SESSION[$currentSpace]['find']) && !empty($_SESSION[$currentSpace]['find_field'])){
		$find = $_SESSION[$currentSpace]['find'];
		$find_field = $_SESSION[$currentSpace]['find_field'];
	}
}
if(isset($find) && isset($find_field)){
	$smarty->assign('find', $find);
	$smarty->assign('find_field', $find_field);
	$sameurl_elements[] = 'find';//For reused filter when call same url
	$sameurl_elements[] = 'find_field';//For reused filter when call same url
	$_SESSION[$currentSpace]['find'] = $find;//Record filter in session
	$_SESSION[$currentSpace]['find_field'] = $find_field;//Record filter in session
	$params['find'][$find_field] = $find;
}














//Filter users
if (empty($_REQUEST['f_action_field'])){
	unset($params['f_action_field']); //Default value
}else{
  if(!empty($_REQUEST['f_action_user_name']) && !empty($_REQUEST['f_action_field'])){
    $find = rtrim($_REQUEST['f_action_user_name']);
    $find_field = rtrim($_REQUEST['f_action_field']);
    $params['find'][$find_field] = $find;
  }else unset($params['find']);
}
$smarty->assign('f_action_user_name', $find);
$smarty->assign('f_action_field', $find_field);

//Define junction
if(is_array($all_field['with'])){
  if(array_key_exists($_REQUEST['find_field'] , $all_field['with'])){
    $params['with']['table'] = $all_field['with'][$_REQUEST['find_field']]['table'];
    $params['with']['col'] = $all_field['with'][$_REQUEST['find_field']]['col'];
  }
}

$sameurl_elements = array(
                          	'offset',
                          	'sort_order',
                          	'sort_field',
                          	'where',
                          	'find',
                          	'find_field',
                          	'numrows',
                          	'space',
                          );
$smarty->assign_by_ref('sameurl_elements', $sameurl_elements);

?>
