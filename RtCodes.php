<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once './conf/ranchbe_setup.php';

//error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_USER_WARNING);
//error_reporting( E_ALL );
//ini_set ( 'display_errors', 1 );
//ini_set ( 'display_startup_errors', 1 );

require_once './class/common/space.php';
require_once './class/Rt.php';
require_once('./GUI/GUI.php');
require_once 'HTML/QuickForm/livesearch_select.php';

$Manager = new Rt(); //Create new manager

//Suppress
if ($_REQUEST['action'] == 'suppress' && !empty($_REQUEST["checked"])) {
  foreach ($_REQUEST["checked"] as $del_id) {
 	$Manager->Suppress($del_id);
  }
}

//Include generic definition of the code for manage filters
$default_sort_field='code';    //Default value of the field to sort
$default_sort_order='ASC';    //Default value of the order ASC = ascendant, DESC = descandant
include('filterManager_simple.php');

$list = $Manager->GetAll($params);
$smarty->assign_by_ref('list', $list);

//Include generic definition of the code for manage the pagination. $list must be set...
include('paginationManager.php');

//Define select option for "find"
$all_field = array ('code' => 'Number',
                    'type' => 'Type',
                    'description' => 'Description');
$smarty->assign('all_field', $all_field);

$Manager->error_stack->checkErrors();
if( $list ){
	$smarty->assign( 'headers', array_keys($list[0]) );
}
$smarty->assign( 'list', $list );
$smarty->assign( 'pageTitle', 'Codes des Refus A350' );
$smarty->assign( 'mid', 'RtGet_GetRtForPeriod.tpl' );
$smarty->display('ranchbe.tpl');


