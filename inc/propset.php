<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//this script may only be included - so its better to die if called directly.
if(strpos($_SERVER["SCRIPT_NAME"],basename(__FILE__)) !== false){
  header("location: index.php");
  exit;}

if(!isset($Manager)) {print '$Manager is not set '; die;}
if(!isset($metadata)) {print '$metadata is not set '; die;}
if(!isset($propset)) {print '$propset is not set '; die;}
if(!isset($document)) {print '$document is not set '; die;}

$area_id = $Manager->AREA_ID;
$params=array(); //init var

//Prevent reused old POST data
$check_flood = check_flood($_REQUEST['page_id']);

// Active the tab
$smarty->assign('documentManage', 'active'); //Assign variable to smarty

//Assign name to particular fields
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number');
$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description');
$smarty->assign('CONTAINER_STATE' , $Manager->SPACE_NAME . '_state');
$smarty->assign('CONTAINER_ID' , $Manager->SPACE_NAME . '_id');

//$smarty->assign('container_id', $_REQUEST['container_id']);
//$smarty->assign('container_number', $Manager->GetProperty('container_number'));

function createPropset($values){
  global $propset;
  $propset->CreateSet( $values );
} //End of function

function editPropset($values){
  global $propset;
  $propset->ModifySet( $_REQUEST['checked'] , $values );
} //End of function

//Manage actions
switch ($_REQUEST['action']){
  case 'createPropset':   //Create a propset
    if(!$check_flood) break;
    if(empty($_REQUEST['propset_name'])) break;
    $propset->CreateSet( array('propset_name'=>$_REQUEST['propset_name']) );
    break;

//---------------------------------------------------------------------
  case 'editPropset':   //Create a propset
    if(!$check_flood) break;
    if(empty($_REQUEST['propset_name'])) break;
    if(empty($_REQUEST['checked'])) break;
    $propset->ModifySet( $_REQUEST['checked'] , array('propset_name'=>$_REQUEST['propset_name']) );
    break;

//---------------------------------------------------------------------
  case 'suppressPropset':   //Suppress a propset
    if( !$check_flood ) break;
    if( !is_array($_REQUEST['checked']) ) break;
    check_ticket( $prefix.'_metadata_suppress' , $area_id );
    foreach ( $_REQUEST['checked'] as $propset_id ){
      $propset->SuppressSet( $propset_id );
    }
    break;
//---------------------------------------------------------------------
  case 'linkProperty':   //remove a metadata to this propset
    if (empty($_REQUEST['propset_id'])){
      break;
    }
    require_once "HTML/QuickForm.php"; //Librairy to easily create forms
    require_once "GUI/Ranchbe_HTML_QuickForm.php"; //Librairy to easily create forms
    require_once "GUI/GUI.php"; //Librairy to easily create forms
    require_once "class/propsetPropertyLink.php"; //Librairy to easily create forms
    $form =& new Ranchbe_HTML_QuickForm('selectMetadata', 'POST'); //Construct the form with QuickForm lib
    $form->addElement('header', 'title1', tra('Add a property to properties set').': '.$propset->GetProperty('propset_name', $_REQUEST['propset_id']) );
    
    $p=array(
      'field_name'=>'property_id',
      'field_description'=>tra('Select property to add'),
      'default_value'=>'',
      'field_multiple'=>true,
      'return_name'=>false,
      'field_size'=>5,
      'adv_select'=>true,
    );
    construct_select_property($p , $form , $metadata);
    $form->addElement('hidden', 'propset_id', $_REQUEST['propset_id']);
    $form->addElement('hidden', 'action', 'linkProperty');
    $form->addElement('hidden', 'request_page', 'propset');
    $form->addElement('hidden', 'space', $Manager->SPACE_NAME);
    $form->addElement('hidden', 'ticket', $smarty->get_template_vars('ticket') );
    
    if (isset($_REQUEST['validate'])){
      if ($form->validate()){ //Validate the form...
        if(!$check_flood) break;
        if( is_array($_REQUEST['property_id']) )//Display a select page
        foreach ($_REQUEST['property_id'] as $property_id) {
          $propsetPropertyLink = new propsetPropertyLink($space);
          $propsetPropertyLink->AddLink( array('propset_id'=>$_REQUEST['propset_id'], 'property_id'=>$property_id ) );
        }
      }
    }else{
      $form->addElement('submit', 'validate', 'GO');
      $form->applyFilter('__ALL__', 'trim');
      $form->display();
      die;
    }
    break;

//---------------------------------------------------------------------
  case 'unlinkProperty':   //remove a metadata to this propset
    if (empty($_REQUEST['link_id'])) break;
    require_once "class/propsetPropertyLink.php"; //Librairy to easily create forms
    $propsetPropertyLink = new propsetPropertyLink($space);
    $propsetPropertyLink->SuppressLink( $_REQUEST['link_id'] );
    break;

} //End of switch between action
//---------------------------------------------------------------------
//---------------------------------------------------------------------

//Include generic definition of the code for manage filters
include('filterManager.php');

//$list = $propset->GetPropset($params);
$list = $propset->GetPropsetRecursive($params);
//var_dump($list);
//var_dump($list2);

$smarty->assign_by_ref( 'list', $list );

//Include generic definition of the code for manage the pagination. $list must set...
include('paginationManager.php');

//Define select option for "find"
$all_field = array ('field_name' => 'Field',
                    'field_description' => 'Description',
                    'field_type' => 'Type',
                   );
$smarty->assign('all_field', $all_field);

//Assign name to particular fields
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number');
$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description');
$smarty->assign('CONTAINER_STATE' , $Manager->SPACE_NAME . '_state');
$smarty->assign('CONTAINER_ID' , $Manager->SPACE_NAME . '_id');

$Manager->error_stack->checkErrors();

// Active the tab
// Display the template
//$smarty->assign('mid', 'workitemDocManage.tpl');
$smarty->display("propset.tpl");
?>
