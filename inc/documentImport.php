<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//this script may only be included - so its better to die if called directly.
if (strpos($_SERVER["SCRIPT_NAME"],basename(__FILE__)) !== false){
  header("location: index.php");
  exit;
}
if(!isset($Manager)) {print '$Manager is not set '; die;}

require_once './class/common/import.php'; //Class to manage the importations
$Import = new import($Manager->SPACE_NAME , $Manager);

if (isset( $_SESSION['area_id']) && is_numeric($_SESSION['area_id']))
  $area_id             = $_SESSION['area_id'];
else
  $area_id             = $Manager->AREA_ID;
if (isset( $_SESSION['SelectedProjectNum']) )
  $selected_project    = $_SESSION['SelectedProjectNum'];
if ( isset ( $_SESSION['SelectedProject']) )
  $selected_project_id    = $_SESSION['SelectedProject'];

$container_id = $Manager->GetProperty('container_id');

//Assign name to particular fields
$smarty->assign( 'container_number' , $Manager->GetProperty('container_number'));
$smarty->assign( 'action' , $_REQUEST['action']);
$smarty->assign( 'container_id' , $container_id);
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number');
$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description');
$smarty->assign('CONTAINER_STATE' , $Manager->SPACE_NAME . '_state');
$smarty->assign('CONTAINER_ID' , $Manager->SPACE_NAME . '_id');

//-----------------------------------------------------------------
//Genere une ligne de formulaire dans le cas du store multiple
//Le principe est de renommer chaque champ du formulaire (y compris les champs issues des metadonnees crees par les utilisateurs)
//et d'y ajouter '[$i]' ou $i est un numero incrementale issue de la boucle de parcours de $_REQUEST['checked'].
//Le formulaire retourne alors des variables tableaux. La designation[1] correspondra au fichier[1] etc.
function SetCreateDocForm($document,$loop_id){
  global $Manager;
  global $smarty;
  global $container_id;
  
  $file = $document['file'];

  $fields = array('document_number', 'designation', 'category_id', 'document_indice_id', 'file_id', 'document_id', 'file');

  //Construct the form with QuickForm lib
  require_once('./GUI/GUI.php');
  $form = new HTML_QuickForm('form', 'post');

  $form->addElement('text', 'file['.$loop_id.']', 'File' ,array('readonly', 'value'=>$file, 'size'=>32));
  $mask = DEFAULT_DOCUMENT_MASK;
  $form->addRule('file['.$loop_id.']', 'This file name is not valid', 'regex', "/$mask/" , 'server');
  $form->addElement('text', 'document_number['.$loop_id.']', 'Document_number' , array('readonly', 'size'=>32));

  if(empty($document['designation'])) $document['designation'] = 'undefined';

  $form->setDefaults(array(
    'document_number['.$loop_id.']' => $document['document_number'],
    'designation['.$loop_id.']' => $document['designation'],
    'category_id['.$loop_id.']' => $document['category_id'],
    'document_indice_id['.$loop_id.']' => $document['document_indice_id'],
  ));

  //Add select document indice
  $params = array(
            'field_multiple' => false,
            'field_name' => 'document_indice_id['.$loop_id.']',
            'field_description' => 'indice',
            'field_size' => '1',
            'field_required' => true,
  );
  construct_select_document_indice($params , $form , $Manager);

  $form->addElement('textarea', 'designation['.$loop_id.']' , 'Designation' ,array('rows'=>2,'cols'=>32));
  $form->addRule('designation['.$loop_id.']', 'Designation is required', 'required');
  $form->addRule('document_number['.$loop_id.']', 'This document name is not valid', 'regex', "/$mask/" , 'server');

  //Add select category
  $params = array(
            'field_multiple' => false,
            'field_name' => 'category_id['.$loop_id.']',
            'field_description' => 'category',
            'field_size' => '1',
            'return_name' => false,
            'default_value' => $document['category_id'],
  );
  construct_select_category($params , $form , $Manager);

  //Add select action to perform
  $params = array(
            'field_multiple' => false,
            'field_name' => 'docaction['.$loop_id.']',
            'field_description' => 'Action',
            'field_size' => '1',
            'return_name' => false,
            'default_value' => key($document['actions']), //Default value is the first key of the action array
  );
  construct_select($document['actions'] , $params , $form);

  //Get fields for custom metadata
  require_once('./class/common/metadata.php');
  $metadata = new docmetadata($Manager->space);
  $optionalFields = $metadata->GetMetadataLinked(NULL, $container_id);
  $smarty->assign( 'optionalFields' , $optionalFields);
  foreach($optionalFields as $field){
    $field['default_value'] = $document[$field['field_name']];
    $fields[] = $field['field_name'];
    $field['field_name'] = $field['field_name'] . '['.$loop_id.']';
    construct_element($field , $form); //function declared in GUI.php
  }
  $smarty->assign('fields', $fields);

  //Apply new filters to the element values
  $form->applyFilter('__ALL__', 'trim');

  //Set the renderer for display QuickForm form in a smarty template
  include 'QuickFormRendererSet.php';

} //End of function

//-----------------------------------------------------------------------------------
//Manage actions wich require at last one selection
switch($_REQUEST['action']){

//---------------------------------------------------------------------
  default :  // Step 1
    check_ticket( 'container_document_manage' , $area_id );
    $smarty->assign( 'step' , '1');
    $smarty->assign( 'step_description' , 'Select the csv file');
  break;

//---------------------------------------------------------------------
  case 'visucvs':   // Step 2 select the cvs file and display it for validation and check it

    $smarty->assign( 'step' , '2');
    $smarty->assign( 'step_description' , 'Validate the csv file');

    if(!is_file($_FILES['importDocumentCsvList']['tmp_name'])){
      $smarty->assign( 'step' , '1');
      $smarty->assign( 'step_description' , 'Select the csv file');
      break;}

    if(!is_dir($Manager->WILDSPACE . '/.rbetmp/'))
      if(!mkdir($Manager->WILDSPACE . '/.rbetmp/'))
        {print 'cant create tmpdir';return false;}

    $csvfile = $Manager->WILDSPACE . '/.rbetmp/' . basename($_FILES['importDocumentCsvList']['tmp_name']);
    move_uploaded_file($_FILES['importDocumentCsvList']['tmp_name'], $csvfile);

    $list = $Import->ImportCsv($csvfile);
    $smarty->assign_by_ref( 'list' , $list);
    $smarty->assign( 'cvsfile' , $csvfile);
    foreach($list[0] as $key=>$val) //Construct a array with the header of each cols
      $fields[] =  $key;
    $smarty->assign( 'fields' , $fields);

    //check each file
    $tmp_list = array();
    foreach($list as $document){
      $fileInfos = array();
      $docinfo = array();
      //---------- The TDP specifie a file
      if(!empty($document['file'])){
        if(!is_file($Manager->WILDSPACE . '/' . $document['file'])){
          $document['errors'][] = $document['file'] . ' is not in your Wildspace.';
          $document['actions']['ignore'] = 'Ignore';
          $clean_list[] = $document;
          continue;
        }
        $odocument = $Manager->initDoc();
        $odocfile = $odocument->initDocfile();
        $odocument->SetDocProperty('document_number', $document['document_number']);
        if($file_id = $odocfile->GetFileId($document['file'])){ //- The file is recorded in database
          $odocfile->init($file_id);
          $fdoc_id = $odocfile->GetDocumentId(); //Get info about the father document
          $odocument->init($fdoc_id);
          $docinfo = $odocument->GetDocumentInfos($fdoc_id); //Get the document of this file
          $document['errors'][] = 'file '.$document['file'] . ' exist in this database. ID=' . $file_id;
          $document['errors'][] = 'file '.$document['file'] . ' is linked to document '. $docinfo['document_number'];
          $document['actions']['update_doc'] = 'Update the document '.$docinfo['document_number'].' and files';
          $document['actions']['update_file'] = 'Update the file '.$document['file'].' only';
          //$document['actions']['upgrade_doc'] = 'Create a new indice of '.$docinfo['document_number'];
          $document['document_id'] = $fdoc_id;
          $document['file_id'] = $file_id;
        }else{//-- The file is not recorded in database
          if(empty($document['document_number'])){
            //Generate the number from the file name
            $document['document_number'] = substr($document['file'], 0, strrpos($document['file'], '.'));
            $odocument->SetDocProperty('document_number', $document['document_number']);
          }
          //Check double documents
          if(in_array($document['document_number'] , $tmp_list)){
            $document['errors'][] = $document['document_number'] . ' is not unique in this package';
            $document['actions']['add_file'] = 'Add the file to the document '.$document['document_number'];
          }else{
            $tmp_list[] = $document['document_number'];
            //Check if document exist in database
            if($document_id = $odocument->DocumentExist($document['document_number'])){
              $document['errors'][] = 'The document '.$document['document_number'] . ' exist in this database. ID=' . $document_id;
              $document['actions']['add_file'] = 'Add the file to the document '.$document['document_number'];
              //$document['actions']['upgrade_doc'] = 'Create a new indice of '.$document['document_number'];
              $document['document_id'] = $document_id;
            }else{ //Document dont existing in database
              $fsdata = $odocfile->initFsdata( $Manager->WILDSPACE.'/'.$document['file'] );
              $fileInfos = $fsdata->getInfos(false); //Get info about file
              $document['actions']['create_doc'] = 'Create a new document';
            }
          }
        }
      }else{ //End of if file condition
        //The TDP specifie a document number
        if(!empty($document['document_number'])){
          $fileInfos['file_type'] = 'nofile'; //To set the doctype
          $fileInfos['file_root_name'] = $document['document_number']; //To set the doctype
          //Check if document exist in database
          $odocument = $Manager->initDoc();   
	  if($document_id = $odocument->DocumentExist($document['document_number'])){
            $document['errors'][] = 'The document '.$document['document_number'] . ' exist in this database. ID=' . $document_id;
            $document['actions']['update_doc'] = 'Update the document '.$document['document_number'];
            //$document['actions']['upgrade_doc'] = 'Create a new indice of '.$document['document_number'];
            $document['document_id'] = $document_id;
          }else{
            $document['actions']['create_doc'] = 'Create a new document';
          }
        }
      }

    //Check that category_id is a integer
      if(!empty($document['category_id']))
        if(!ctype_digit($document['category_id'])){
          $document['errors'][] = 'category_id of '. $document['document_number'] . ' is not a integer';
        }
    //Check that document_indice_id is a integer
      if(!empty($document['document_indice_id']))
        if(!ctype_digit($document['document_indice_id'])){
          $document['errors'][] = 'document_indice_id of '. $document['document_number'] . ' is not a integer';
        }
    //Check the doctype for the new document
      if(isset($document['actions']['create_doc'])){
        $document['doctype'] = $odocument->SetDocType($fileInfos['file_extension'], $fileInfos['file_type']);
        if(!$document['doctype']){
          $document['errors'][] = 'This document has a bad doctype';
          unset($document['actions']); //Reinit all actions
        }
      }

    //Add default actions
      $document['actions']['ignore'] = 'Ignore';

    $clean_list[] = $document;
    $currentdata[] = $docinfo; //original values of documents properties.
    }
    $smarty->assign('currentdata', $currentdata);

    $list = $clean_list;

  break;

//---------------------------------------------------------------------
  case 'visuzipfile':   // Step 2 select the zip file and display it for validation and check it

    $smarty->assign( 'step' , '2.1');
    $smarty->assign( 'step_description' , 'Validate the zip file');

    // Display the template
    $_SESSION['myspace'] = array( 'activePage'=>$_SERVER['PHP_SELF']);
    $smarty->assign('documentManage' , 'active');

    if(!is_file($_FILES['importzipfile']['tmp_name'])){
      $smarty->assign( 'step' , '1');
      $smarty->assign( 'step_description' , 'Select the csv file');
      break;}

    if(!is_dir($Manager->WILDSPACE . '/.rbetmp/'))
      if(!mkdir($Manager->WILDSPACE . '/.rbetmp/'))
        {print 'cant create tmpdir';return false;}

    /*$targetDir = $Manager->WILDSPACE.'/.rbetmp/'.time();
    if(!mkdir($targetDir)) {print 'cant create '.$targetDir;return false;}*/
    $targetDir = $Manager->WILDSPACE;

    $zipfile = $Manager->WILDSPACE . '/.rbetmp/' . basename($_FILES['importzipfile']['name']);
    move_uploaded_file($_FILES['importzipfile']['tmp_name'], $zipfile);

    $Import->UnpackImportPackage($zipfile , $targetDir);
    $list = $Import->unpack;

    $tmp_list = array(); //init the var for foreach loop
    $clean_list = array(); //init the var for foreach loop
    $smarty->assign( 'displayHeader' , 1); // true for display header
    $i = 0;

    foreach($list as $file){
      $i = $i + 1;
      if(!is_file($Manager->WILDSPACE.'/'.basename($file))){
        $document['errors'][] = $file.' is not in your Wildspace.';
        $document['actions']['ignore'] = 'Ignore';
        continue;}
      $odocument = $Manager->initDoc();
      $odocfile = $odocument->initDocfile();
      if($file_id = $odocfile->GetFileId(basename($file))){ //- The file is recorded in database
        $odocfile->init($file_id);
        $fdoc_id = $odocfile->GetDocumentId(); //Get info about the father document
        $odocument->init($fdoc_id);
        $document = $odocument->GetDocumentInfos(); //Get the document of this file
        $document['errors'][] = 'file '.$document['file'] . ' exist in this database. ID=' . $file_id;
        $document['errors'][] = 'file '.$document['file'] . ' is linked to document '. $document['document_number'];
        $document['actions']['update_doc'] = 'Update the document '.$document['document_number'].' and files';
        $document['actions']['update_file'] = 'Update the file '.$document['file'].' only';
        //$document['actions']['upgrade_doc'] = 'Create a new indice of '.$document['document_number'];
        $document['file_id'] = $file_id;
        $document['file'] = basename($file);
      }else{//-- The file is not recorded in database
        //echo 'Generate the number from the file name<br>';
        $document['file'] = basename($file);
        $document['document_number'] = basename(substr($document['file'], 0, strrpos($document['file'], '.')));
        //Check double documents
        if(in_array($document['document_number'] , $tmp_list)){
          $document['errors'][] = $document['document_number'] . ' is not unique in this package';
          $document['actions']['add_file'] = 'Add the file to the document '.$document['document_number'];
        }else{
          $tmp_list[] = $document['document_number'];
          //Check if document exist in database
          if($document_id = $odocument->DocumentExist($document['document_number'])){
            $document['errors'][] = 'The document '.$document['document_number'] . ' exist in this database. ID=' . $document_id;
            $document['actions']['add_file'] = 'Add the file to the document '.$document['document_number'];
            //$document['actions']['upgrade_doc'] = 'Create a new indice of '.$document['document_number'];
            $document['document_id'] = $document_id;
          }else{
            $document['actions']['create_doc'] = 'Create a new document';
          }
        }
      }

      //Check the doctype for the new document
      if(isset($document['actions']['create_doc'])){
        $fsdata = $odocfile->initFsdata($file);
        $document['doctype']=$odocument->SetDocType($fsdata->getProperty('file_extension'), $fsdata->getProperty('file_type'));
        if(!$document['doctype']){
          $document['errors'][] = 'This document has a bad doctype';
          unset($document['actions']); //Reinit all actions
        }
      }

      //Add default actions
      $document['actions']['ignore'] = 'Ignore';
      SetCreateDocForm($document, $i);
      $smarty->assign( 'document' , $document);
      $smarty->assign( 'loop_id' , $i);
      $smarty->display('documentImportZipfile.tpl');
      //Display the header only
      $smarty->assign('displayHeader' , 0);
  
      $document = array();

    }//End of foreach
    $smarty->assign( 'displayFooter' , 1);
    $smarty->display('documentImportZipfile.tpl');
  die;
  break;

//---------------------------------------------------------------------
  case 'validatecsv':   // Step 3 select options
    $smarty->assign( 'step' , '3');
    $smarty->assign( 'step_description' , 'Choose options');
    $smarty->assign( 'maxcsvrow' , $Manager->maxcsvrow);

    //Record data in session
    //session_register('import'); //Record variable in session
    $_SESSION['import'] = $_POST;
    /*
    for ($i = 0; $i < $_POST['loop']; $i++){
      echo $i . '-----<br>';
      echo $_POST['docaction'][$i] . '<br />';
      foreach($_POST['fields'] as $field){
        echo $field . ' : ';
        echo $_POST[$field][$i] . '<br />';
      }
    }*/
  break;

//---------------------------------------------------------------------
  case 'run':   // Step 4 Create the documents
  check_ticket( 'container_document_manage' , $area_id );

    //if(isset($_POST['cvsfile'])) $csvfile = $_POST['cvsfile'];
    //else {print 'no csv file selected'; return false;}

    $ImportData = $_SESSION['import'];

    set_time_limit(24*3600); //To increase default TimeOut.No effect if php is in safe_mode
    $start_time = time();

    if ($_REQUEST['background'] ){ //Tache execute en arriere plan
    //If we want lunch the process in background
    //See chapter 40 of php documentation
      session_write_close(); //To close the current session and unlock the Session. If not do that, its impossible to continue work with another scripts of ranchbe
      ignore_user_abort(true); //Continue execution after deconnection of user

      require_once './lib/Date/date.php';
      print formatDate($start_time) . ' La tache s\'execute en arriere plan. Vous recevrez un message lorsqu\'elle sera termin�e.<br />
       <a href="'. $Manager->SPACE_NAME .'DocManage.php">Go back to document manager</a><br />';

      /*$smarty->assign('msg', $msg);
      $smarty->display('error');
      */
       flush(); // On vide le buffer de sortie
    }

    $errors = array(); //init the errors var
    $feedbacks = array(); //init the feedbacks var

    for ($i = 0; $i < $ImportData['loop']; $i++){
      $docaction = $ImportData['docaction'][$i];
      switch($docaction){
        case "ignore": //Ignore
        break;

/*
        case "upgrade_doc": //Create a new indice and update the files if specifed
        if(check_ticket( 'container_document_change_indice' , $area_id , false) === false){
          $feedbacks[] = 'You have not permission to upgrade the document '.$data['document_number'];
          break;}
          foreach($ImportData['fields'] as $field){
            $data[$field] = $ImportData[$field][$i];
          }
          $feedbacks[] = 'Try to upgrade document '.$data['document_number'].' ID='.$ImportData['document_id'][$i];
          //Check access
          $accessCode=$Manager->CheckAccess();
          if($accessCode == 1){
            $errors[] = 'This document is checkout';
            break;}
          if(($accessCode < 10 && $accessCode != 0) || ($accessCode > 14)){
            $errors[] = 'Can not upgrade a document with access code='.$accessCode;
            break;}
          //Lock the current indice
          if($Manager->LockDocument($ImportData['document_id'][$i] , 11)){
            $feedbacks[] = 'the document ID='.$ImportData['document_id'][$i].' has been locked.';
            //Create the new indice
            $current_indice=$Manager->GetCurrentIndice($data['document_number']);
            if($new_doc_id=$Manager->UpgradeIndice($ImportData['document_id'][$i] , ($current_indice + 1))){
                $feedbacks[] = 'the document ID='.$new_doc_id.' has been created.';
              //Update the new indice with metadata to import
              if($Manager->UpdateDocInfos($data , $new_doc_id)){
                $feedbacks[] = 'the document ID='.$new_doc_id.' has been updated.';
                //TODO : Update files
              }
            }else $errors[] = 'Can not create the new indice';
          }else $errors[] = 'Can not lock the document ID='.$ImportData['document_id'][$i];
        break;
*/

        case "update_doc": //Update the document and the file if specified
          foreach($ImportData['fields'] as $field){
            $data[$field] = $ImportData[$field][$i];
          }
          //Update the document record metadatas
          $odocument =& $Manager->initDoc($ImportData['document_id'][$i]);
          if($odocument->UpdateDocInfos($data))
            $feedbacks[] = 'the document ID='.$ImportData['document_id'][$i].' has been updated.';
          //If file is specified, update the file. The file must be stored in the wildspace
          if(!empty($ImportData['file_id'][$i])){
            if($odocfile = $odocument->initDocfile($ImportData['file_id'][$i]))
              if(is_file($Manager->WILDSPACE .'/'.$data['file'])){
                if($odocfile->CheckOutFile(false)){
                  if($odocfile->CheckInFile(false , false , true))
                    $feedbacks[] = $data['file'].' as been updated.';
                  else
                    $errors[] = 'Can not checkin the file ID='. $ImportData['file_id'][$i];
                    $odocfile->CheckInFile(false , true , true); //Try to reset the file
                }else
                  $errors[] = 'Can not checkout the file ID='. $ImportData['file_id'][$i];
              }else{
                $errors[] = 'The file '.$data['file'].' is not in your Wildspace '.$Manager->WILDSPACE;
              }
            }
        break;

        case "update_file": //Update the associated file only
          foreach($ImportData['fields'] as $field){
            $data[$field] = $ImportData[$field][$i];
          }
          $feedbacks[] = 'update_file : '. $data['file'];
          //If file is specified, update the file. The file must be stored in the wildspace
          if(!empty($ImportData['file_id'][$i])){
            $odocfile = new docfile($Manager->space, $ImportData['file_id'][$i]);
              if(is_file($Manager->WILDSPACE .'/'.$data['file'])){
                if($odocfile->CheckOutFile(false)){
                  if($odocfile->CheckInFile(false , false , true))
                    $feedbacks[] = $data['file'].' as been updated.';
                  else
                    $errors[] = 'Can not checkin the file ID='. $ImportData['file_id'][$i];
                    $odocfile->CheckInFile(false , true , true); //Try to reset the file
                }else
                  $errors[] = 'Can not checkout the file ID='. $ImportData['file_id'][$i];
              }else{
                $errors[] = 'The file '.$data['file'].' is not in your Wildspace '.$Manager->WILDSPACE;
              }
          }
        break;

        case "add_file": //Add the file to the document
        case "create_doc": //Create a new document
          foreach($ImportData['fields'] as $field)
            $data[$field] = $ImportData[$field][$i];
          if(is_file($Manager->WILDSPACE .'/'.$data['file'])){
            $odocument = $Manager->initDoc();
            if($document_id=$odocument->DocumentExist($data['document_number'])){ //Dont believe the id give by step 1. The document_id might have changed after an indice upgrade.
              $feedbacks[] = 'add_file : '. $data['file'] .' to '.$data['document_number'];
              $odocument->init($document_id);
              if($odocument->AssociateFile($Manager->WILDSPACE .'/'.$data['file'] , true)){
              //if($odocument->AssociateFile($data['file'] , true)){
                $feedbacks[] = 'the file '.$data['file'].' has been associated to document ID='.$document_id;
              }else
                $errors[] = 'The file '.$data['file'].' can not be associated to the document';
            }else{ //Create the doc
              $feedbacks[] = 'create_doc : '.$data['document_number'];
              if(isset($data['file'])) $element = $data['file'];
              else if(isset($data['document_number'])) $element = $data['document_number'];
              else{$errors[]='Can not create the doc';break;}
              if(!isset($data['container_id'])){
                $data['container_id'] = $container_id;
              }
              $feedbacks[] = 'create_doc from: '.$element;
              if($odocument->Store($data))
                $feedbacks[] = 'The doc '.$element.' has been created';
              else{
                $errors[] = 'Can not create the doc '.$element;
                break;
              }
            } //End of create doc
          }else
            $errors[] = 'The file '.$data['file'].' is not in your Wildspace '.$Manager->WILDSPACE;
        break;

        default:
      }
    } //End 'for' loop

/*    if(!$Manager->ImportDocuments( $csvfile , $container_id , $_POST['lock'] , $_POST['create_category'])){
          $subject = 'Error occuring in batch import document';
          $body = '<b>An error occured during document import task</b><br />';
        }else{
          $subject = 'Success ending of batch documents import';
          $body = '<b>End of batch documents import : </b><br/>';
        }
*/
        require_once './lib/Date/date.php';
        $end_time = time();
        $body.= '<b>To container : </b>'.$Manager->GetProperty('container_number').'<br/>';
        $body.= '<b>By : </b>'.$Manager->importUserName.'<br/>';
        $body.= '<b>start to : </b>'.formatDate($start_time).'<br/>';
        $body.= '<b>end to : </b>'.formatDate($end_time).'<br/>';
        $body.= '<b>duration : </b>'.($end_time - $start_time).' sec<br/>';
        foreach($feedbacks as $feedback){
          $body.= '<font color="green">'.$feedback.'</font><br />';
        }
        foreach($errors as $error){
          $body.= '<font color="red"><b>ERROR : </b>'.$error.'</font><br />';
        }

        // Write a out trace file
        date("y/m/d H:i:s").'- :<br/>';
        $handle = fopen($Manager->WILDSPACE . '/.rbetmp/importDoc.out' , 'a+' );
        fwrite($handle , '================================================='."\n");
        fwrite($handle , date("y/m/d H:i:s")."\n");
        fwrite($handle , strip_tags(str_replace('<br/>',"\n",$subject.'<br/>')));
        fwrite($handle , strip_tags(str_replace('<br/>',"\n",$body)));
        fclose($handle);

      if ($_REQUEST['background'] ){ //Tache execute en arriere plan
        //Send message to user
        include_once './class/messu/messulib.php';
        $to = $user;
        $from = $user;
        $cc   = '';
        //$body = str_replace('<br/>',"\n",ob_get_contents());
        $body = str_replace('<br/>',"\n",$body);
        $priority = 3;
        $messulib->post_message($to, $from, $to, $cc, $subject, $body, $priority);
      }else{
        $smarty->assign( 'step' , 'END');
        $smarty->assign( 'step_description' , 'End of batch importation');
        $smarty->assign( 'body' , $body);
      }

    break;

} //End of switch

//Include generic definition of the code for manage filters
include('filterManager.php');

set_time_limit(24*3600); //To increase default TimeOut.No effect if php is in safe_mode

$Manager->error_stack->checkErrors();

// Display the template
$_SESSION['myspace'] = array('activePage'=>$_SERVER['PHP_SELF']);
$smarty->assign('documentManage' , 'active');
$smarty->assign('mid', 'documentImport.tpl');
$smarty->display('ranchbe.tpl');

?>
