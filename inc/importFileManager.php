<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//this script may only be included - so its better to die if called directly.
if (strpos($_SERVER["SCRIPT_NAME"],basename(__FILE__)) !== false) {
  header("location: index.php");
  exit;
}
if ( ! isset($Manager) ) {print '$Manager is not set '; die;}

$area_id = $Manager->AREA_ID;

//Manage actions wich require at last one selection
if (!empty($_REQUEST["action"])) {

switch ( $_REQUEST['action'] ) {

//---------------------------------------------------------------------
  case 'selectContainerForImport':   //Just select the mockup first

    if ( count($_REQUEST["checked"]) == 1 ){ //check if only one is selected
    
    $smarty->assign( 'container_id' , $_REQUEST["checked"][0] );

    }else die('none container selected');
    break;

//---------------------------------------------------------------------
  case 'suppressPackage':   //Suppress a package file
  check_ticket( 'container_import' , $area_id );
    if ( empty($_REQUEST["checked"]) ) {print 'none selected package'; break; }

    foreach ($_REQUEST['checked'] as $import_order ){
      $Manager->SuppressPackage("$import_order");
    }
    break;


//---------------------------------------------------------------------
  case 'uncompressPackage':   // Uncompress the package
  check_ticket( 'container_import' , $area_id );
    if ( empty($_REQUEST["checked"]) ) {print 'none selected package'; break;}

    foreach ($_REQUEST['checked'] as $import_order ){
      $Manager->UncompressPackage("$import_order" , NULL);
    }
    break;

//---------------------------------------------------------------------
  case 'unpack':   // Import the package
  check_ticket( 'container_import' , $area_id );
    if ( empty($_REQUEST["checked"]) ) {print 'none selected package'; die;}

    set_time_limit(24*3600); //To increase default TimeOut.No effect if php is in safe_mode
    $start_time = time();

    //If we want lunch the process in background
    //See chapter 40 of php documentation
    if ($_REQUEST['background'] ){ //Tache execute en arriere plan
      session_write_close(); //To close the current session and unlock the Session. If not do that, its impossible to continue work with another scripts of ranchbe
      ignore_user_abort(true); //Continue execution after deconnection of user

      require_once './lib/Date/date.php';
      print formatDate($start_time).'
       - La tache s\'execute en arriere plan. Vous recevrez un message lorsqu\'elle sera termin�e.<br />
       Vous pouvez fermer cette fen�tre.<br />
       <a href="javascript:window.close()">Close Window</a><br />';
       flush(); // On vide le buffer de sortie
    }

      //unpack each package
      foreach ($_REQUEST['checked'] as $import_order ){

        if (!$Manager->ImportPackage($import_order, $_REQUEST["container_id"], $_REQUEST['target'] )){
          $body = '<b>Not Imported package : </b>'.$Manager->PackInfos['package_file_name'].'<br/>';
          $body.= '<b>An error is occured during import task</b><br />';
          $subject = 'Error in import task of '.$Manager->PackInfos['package_file_name'];
        }else{
          $body = '<b>Imported package : </b>'.$Manager->PackInfos['package_file_name'].'<br/>';
          $subject = 'End of import task of '.$Manager->PackInfos['package_file_name'];
        }

        $end_time = time();
        
        $duration = ($end_time - $start_time);

        include_once './lib/Date/date.php';
        $body.= '<b>target dir : </b>'.$Manager->target_dir.'<br/>';
        $body.= '<b>target container : </b>'.$Manager->target_container_num.'<br/>';
        $body.= '<b>By : </b>'.$Manager->importUserName.'<br/>';
        $body.= '<b>start to : </b>'.formatDate($start_time).'<br/>';
        $body.= '<b>end to : </b>'.formatDate($end_time).'<br/>';
        $body.= '<b>duration : </b>'.$duration.' sec<br/>';

        if ($_REQUEST['background'] ){ //Tache execute en arriere plan

          /*
          // Write a out trace file
          echo date("y/m/d H:i:s").'- :<br/>';
          $handle = fopen('./import.out' , 'a+' );
          fwrite($handle , $_SERVER['REQUEST_URI'] . str_replace('<br/>',"\n",ob_get_contents()));
          fclose($handle);*/

          //Send message to user
          include_once './class/messu/messulib.php';
          $to = $user;
          $from = $user;
          $cc   = '';
          //$body = str_replace('<br/>',"\n",ob_get_contents());
          $body = str_replace('<br/>',"\n",$body);
          $priority = 3;
          $messulib->post_message($to, $from, $to, $cc, $subject, $body, $priority);

          //ob_end_flush();

        }else echo $body;

      } //End of loop

    die;
    break;

//---------------------------------------------------------------------

  } //End of switch between action
} //End of not empty action condition

//Include generic definition of the code for manage filters
include('filterManager.php');

set_time_limit(24*3600); //To increase default TimeOut.No effect if php is in safe_mode

$list = $Manager->GetAllImportPackage($params); //get infos on all package in deposit directories
$smarty->assign_by_ref('list', $list);

//Smarty var
if ( $_REQUEST['action'] !== 'selectContainerForImport')
  {$smarty->assign( 'container_id' , $_REQUEST['container_id'] );} //Re assign the containerId

//Assign name to particular fields
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number' );
$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description' );
$smarty->assign('CONTAINER_STATE' , $Manager->SPACE_NAME . '_state' );
$smarty->assign('CONTAINER_ID' , $Manager->SPACE_NAME . '_id' );


// Active the tab
$smarty->assign( "$Manager->SPACE_NAME".'Tab' , 'active');

$Manager->error_stack->checkErrors();

// Display the template
$smarty->assign('mockupsTab', 'active');  // Active the tab
$smarty->assign('mid', 'import.tpl');
$smarty->display("ranchbe.tpl");

?>
