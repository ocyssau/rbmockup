<?php
/* !brief Generate a excel file
* param $container(container object) container object from where get documents list
*
*/
function excel_render(container $container){
  $params = array();
  $displayHistory = true;
  $list = $container->GetAllDocuments($params, $displayHistory);

  require_once "Spreadsheet/Excel/Writer.php";
  // Creating a workbook
  $workbook = new Spreadsheet_Excel_Writer();
  // sending HTTP headers
  $workbook->send($container->GetName().'_content.xls');
  // Creating a worksheet
  $worksheet =& $workbook->addWorksheet($container->GetName());

  //The header
  $format =& $workbook->addFormat(array('Size' => 10,
                                        'bold'    => 1,
                                        'Align' => 'center'));

  $worksheet->write(0, 0, 'Number', $format);
  $worksheet->write(0, 1, 'Designation', $format);
  $worksheet->write(0, 2, 'Category', $format);
  $worksheet->write(0, 3, 'CheckOut By', $format);
  $worksheet->write(0, 4, 'CheckOut Date', $format);
  $worksheet->write(0, 5, 'Create By', $format);
  $worksheet->write(0, 6, 'Create Date', $format);
  $worksheet->write(0, 7, 'Last Update By', $format);
  $worksheet->write(0, 8, 'Last Update Date', $format);
  $worksheet->write(0, 9, 'access', $format);
  $worksheet->write(0, 10, 'State', $format);
  $worksheet->write(0, 11, 'Indice', $format);
  $worksheet->write(0, 12, 'Version', $format);
  $worksheet->write(0, 13, 'Type', $format);

  //Get extends metadatas header
  require_once('./class/common/metadata.php');
  $metadata = new docmetadata($container->space);
  $optionalFields = $metadata->GetMetadataLinked(NULL, $container->GetId());
  if(is_array($optionalFields)){
    $col = 14;
    foreach($optionalFields as $field){
      $worksheet->write(0, $col, $field['field_description'], $format);
      $col++;
    }
  }
  
  global $smarty;
  require_once $smarty->_get_plugin_filepath('modifier','category');
  require_once $smarty->_get_plugin_filepath('modifier','username');
  require_once $smarty->_get_plugin_filepath('modifier','date_format');
  require_once $smarty->_get_plugin_filepath('modifier','document_indice');
  require_once $smarty->_get_plugin_filepath('modifier','type');
  require_once $smarty->_get_plugin_filepath('function','filter_select');

  // The actual data
  $i = 0;
  while($lin < count($list)){
    $lin = $i+1;
    $worksheet->write($lin, 0, $list[$i]['document_number']);
    $worksheet->write($lin, 1, $list[$i]['designation']);
    $worksheet->write($lin, 2, smarty_modifier_category($list[$i]['category_id']));
    $worksheet->write($lin, 3, smarty_modifier_username($list[$i]['check_out_by']));
    $worksheet->write($lin, 4, smarty_modifier_date_format($list[$i]['check_out_date']));
    $worksheet->write($lin, 5, smarty_modifier_username($list[$i]['open_by']));
    $worksheet->write($lin, 6, smarty_modifier_date_format($list[$i]['open_date']));
    $worksheet->write($lin, 7, smarty_modifier_username($list[$i]['update_by']));
    $worksheet->write($lin, 8, smarty_modifier_date_format($list[$i]['update_date']));
    $worksheet->write($lin, 9, $list[$i]['document_access_code']);
    $worksheet->write($lin, 10, $list[$i]['document_state']);
    $worksheet->write($lin, 11, smarty_modifier_document_indice($list[$i]['document_indice_id']));
    $worksheet->write($lin, 12, $list[$i]['document_version']);
    $worksheet->write($lin, 13, smarty_modifier_type($list[$i]['doctype_id']));
    $col = 14;
    foreach($optionalFields as $field){
      $p = array( 'id'=>$list[$i][$field['field_name']],
                  'type'=>$field['field_type'] );
      $worksheet->write($lin, $col, smarty_function_filter_select($p));
      $col++;
    }
    $i++;
  }
  // Let's send the file
  $workbook->close();
  return true;
}
?>
