<?php
/* !brief Generate a excel file
* param $container(container object) container object from where get documents list
*
*/
function excel_render(container $container){
  $params = array();
  $displayHistory = true;
  $list = $container->GetAllDocuments($params, $displayHistory);

  require_once "Spreadsheet/Excel/Writer.php";
  // Creating a workbook
  $workbook = new Spreadsheet_Excel_Writer();
  // sending HTTP headers
  $workbook->send($container->GetName().'_content.xls');
  // Creating a worksheet
  $worksheet =& $workbook->addWorksheet( $container->GetName() );

  //Write a big title
  $format_big =& $workbook->addFormat();
  $format_big->setBold();
  $format_big->setSize(20);
  $format_big->setAlign('merge');
  $format_big->setUnderline(1);
  //$format_big->setHAlign('center'); //left, center, right, fill, justify, merge, equal_space
  $format_big->setVAlign('vcenter'); //top, vcenter, bottom, vjustify, vequal_space
  $worksheet->write(0, 2, 'Content of '.$container->GetName(), $format_big);
  $worksheet->write(0, 3, '', $format_big);
  $worksheet->write(0, 4, '', $format_big);

  /* This freezes the first six rows of the worksheet: */
  $worksheet->freezePanes(array(2, 0));

  //Add logo
  //insertBitmap (integer $row, integer $col, string $bitmap [, integer $x=0 [, integer $y=0 [, integer $scale_x=1 [, integer $scale_y=1]]]])
  $worksheet->setRow(0,150);
  $worksheet->setColumn(0,0,30); // row 0 col 0
  $worksheet->insertBitmap(0, 0, 'img/ranchBe_logo_190.bmp', 0, 0, 1, 1);

  //An header and footer
  //Worksheet::setHeader (string $string [, float $margin=0.5])
  $worksheet->setHeader( 'Content of '.$container->GetName() );
  $worksheet->setFooter ( 'From RanchBE' );

  //Set to landscape
  $worksheet->setLandscape();

  //Worksheet::centerHorizontally -- Center the page horizontally.
  $worksheet->centerHorizontally();
  //Worksheet::centerVertically -- Center the page vertically.
  $worksheet->centerVertically();
  //Worksheet::setMargins_LR -- Set the left and right margins to the same value in inches.  
  $worksheet->setMargins_LR(0.5);
  //Worksheet::setMargins_TB -- Set the top and bottom margins to the same value in inches.
  $worksheet->setMargins_TB(0.5);

  //Fir to page 1 in horizontal 1 in vartical
  $worksheet->fitToPages(1,1);
  
  //The lines titles
  $format_title =& $workbook->addFormat(array('Size' => 10,
                                        'bold'    => 1,
                                        'Align' => 'center'));

  $format_title->setColor('yellow');
  $format_title->setPattern(1);
  $format_title->setFgColor('blue');
  $format_title->setTextRotation(270); //0,90,270,-1

  $worksheet->write(1, 0, 'Number', $format_title);
  $worksheet->write(1, 1, 'Designation', $format_title);
  $worksheet->write(1, 2, 'Category', $format_title);
  $worksheet->write(1, 3, 'CheckOut By', $format_title);
  $worksheet->write(1, 4, 'CheckOut Date', $format_title);
  $worksheet->write(1, 5, 'Create By', $format_title);
  $worksheet->write(1, 6, 'Create Date', $format_title);
  $worksheet->write(1, 7, 'Last Update By', $format_title);
  $worksheet->write(1, 8, 'Last Update Date', $format_title);
  $worksheet->write(1, 9, 'access', $format_title);
  $worksheet->write(1, 10, 'State', $format_title);
  $worksheet->write(1, 11, 'Indice', $format_title);
  $worksheet->write(1, 12, 'Version', $format_title);
  $worksheet->write(1, 13, 'Type', $format_title);

  //Set default width for columns
  for ($i = 1; $i <= 20; $i++) {
    $worksheet->setColumn($i,$i,15); // row 0 col 0
  }

  //Get extends metadatas header
  require_once('./class/common/metadata.php');
  $metadata = new docmetadata($container->space);
  $optionalFields = $metadata->GetMetadataLinked(NULL, $container->GetId());
  if(is_array($optionalFields)){
    $col = 14;
    foreach($optionalFields as $field){
      $worksheet->write(1, $col, $field['field_description'], $format);
      $col++;
    }
  }
  
  global $smarty;
  require_once $smarty->_get_plugin_filepath('modifier','category');
  require_once $smarty->_get_plugin_filepath('modifier','username');
  require_once $smarty->_get_plugin_filepath('modifier','date_format');
  require_once $smarty->_get_plugin_filepath('modifier','document_indice');
  require_once $smarty->_get_plugin_filepath('modifier','type');
  require_once $smarty->_get_plugin_filepath('function','filter_select');

  // The actual data
  $i = 1;
  while($lin < count($list)){
    $lin = $i+1;
    $worksheet->write($lin, 0, $list[$i]['document_number']);
    $worksheet->write($lin, 1, $list[$i]['designation']);
    $worksheet->write($lin, 2, smarty_modifier_category($list[$i]['category_id']));
    $worksheet->write($lin, 3, smarty_modifier_username($list[$i]['check_out_by']));
    $worksheet->write($lin, 4, smarty_modifier_date_format($list[$i]['check_out_date']));
    $worksheet->write($lin, 5, smarty_modifier_username($list[$i]['open_by']));
    $worksheet->write($lin, 6, smarty_modifier_date_format($list[$i]['open_date']));
    $worksheet->write($lin, 7, smarty_modifier_username($list[$i]['update_by']));
    $worksheet->write($lin, 8, smarty_modifier_date_format($list[$i]['update_date']));
    $worksheet->write($lin, 9, $list[$i]['document_access_code']);
    $worksheet->write($lin, 10, $list[$i]['document_state']);
    $worksheet->write($lin, 11, smarty_modifier_document_indice($list[$i]['document_indice_id']));
    $worksheet->write($lin, 12, $list[$i]['document_version']);
    $worksheet->write($lin, 13, smarty_modifier_type($list[$i]['doctype_id']));
    $col = 14;
    foreach($optionalFields as $field){
      $p = array( 'id'=>$list[$i][$field['field_name']],
                  'type'=>$field['field_type'] );
      $worksheet->write($lin, $col, smarty_function_filter_select($p));
      $col++;
    }
    $i++;
  }
  // Let's send the file
  $workbook->close();
  return true;
}
?>
