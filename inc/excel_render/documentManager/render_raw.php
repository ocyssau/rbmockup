<?php
/* !brief Generate a excel file
* param $container(container object) container object from where get documents list
*
*/
function excel_render(container $container){
  $params = array();
  $displayHistory = true;
  $list = $container->GetAllDocuments($params, $displayHistory);

  require_once "Spreadsheet/Excel/Writer.php";
  // Creating a workbook
  $workbook = new Spreadsheet_Excel_Writer();
  // sending HTTP headers
  $workbook->send($container->GetName().'_content.xls');
  // Creating a worksheet
  $worksheet =& $workbook->addWorksheet($container->GetName());

  //The header
  $format =& $workbook->addFormat(array('Size' => 10,
                                        'bold'    => 1,
                                        'Align' => 'center'));

  foreach(array_keys($list[0]) as $key){
    $worksheet->write(0, $col, $key, $format);
    $col++;
  }
  
  // The actual data
  $i = 0;
  while($lin < count($list)){
    $col = 0;
    $lin = $i+1;
    foreach($list[$i] as $key=>$val){
      $worksheet->write($lin, $col, $val);
      $col++;
    }
    $i++;
  }
  // Let's send the file
  $workbook->close();
}
?>
