<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Ranchbe is distributed in the hope that it will be useful,                |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//this script may only be included - so its better to die if called directly.
if (strpos($_SERVER["SCRIPT_NAME"],basename(__FILE__)) !== false) {
  header("location: index.php");
  exit;
}

if (!isset($Manager)) {print '$Manager is not set '; die;}
if (!isset($metadata)) {print '$metadata is not set '; die;}

$smarty->assign('number_map', $Manager->GetFieldName('number'));
$smarty->assign('id_map', $Manager->GetFieldName('id'));
$smarty->assign('description_map', $Manager->GetFieldName('description'));
$smarty->assign('state_map', $Manager->GetFieldName('state'));
$smarty->assign('indice_map', $Manager->GetFieldName('indice'));
$smarty->assign('father_map_id', $Manager->GetFieldName('father'));

$area_id = $Manager->AREA_ID;

//Manage actions on history

if (!isset($_REQUEST['action'])) $_REQUEST['action'] = '';

//Process suppress request
if ($_REQUEST['action'] == 'suppress'){
  if (! empty($_REQUEST['checked'])){
    check_ticket( $prefix.'_metadata_suppress' , $area_id );
    foreach ( $_REQUEST['checked'] as $field_name){
      $metadata->SuppressMetadata($field_name);
    }
  }
  unset($_REQUEST['action']); //unset the action.Else quickform validate the form...
} //End of if suppress

//---------------------------------------------------------------------

  //Create formulaire

  require_once "HTML/QuickForm.php"; //Librairy to easily create forms
  require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm
  require_once './lib/Date/date.php';
  require_once './GUI/GUI.php';
  
  //Construct the form with QuickForm lib
  $form = new HTML_QuickForm('createForm', 'post');
  $smarty->assign('formName','createForm');

  if ( empty($_REQUEST['action']) || $_REQUEST['action'] == 'create'){
    $form->setDefaults(array(
      'action'      => 'create',
      ));
  } //End of createCat set default

  //Set defaults values of elements if modify request
  if ($_REQUEST['action'] == 'modify'){
    //Get infos
    $Infos = $metadata->GetMetadataInfos($_REQUEST['field_name']);
    $form->addElement('hidden', 'property_id', $Infos['property_id']);

    $form->setDefaults(array(
      'field_name'      => $Infos['field_name'],
      'field_description' => $Infos['field_description'],
      'field_type' => $Infos['field_type'],
      'field_regex' => $Infos['field_regex'],
      'field_required' => $Infos['field_required'],
      'field_multiple' => $Infos['field_multiple'],
      'return_name' => $Infos['return_name'],
      'field_list' => $Infos['field_list'],
      'field_where' => $Infos['field_where'],
      'field_size' => $Infos['field_size'],
      'table_name' => $Infos['table_name'],
      'field_for_value' => $Infos['field_for_value'],
      'field_for_display' => $Infos['field_for_display'],
      'date_format' => $Infos['date_format'],
      'adv_select' => $Infos['adv_select'],
      'action'      => 'modify',
      ));
    $field_name_access='readonly';
    //Add hidden fields
    $smarty->assign('action', 'modify');
  } //End of modify set default

  //Add submit button
  $form->addElement('reset', 'reset', 'reset');
  $form->addElement('submit', 'action', 'Go');

  //Add form elements
  $form->addElement('text', 'field_name', tra('field_name'), array('size'=>10, $field_name_access));
  $form->addElement('text', 'field_description', tra('field_description'));
  //$SelectSet = array('text', 'long_text', 'partner' , 'doctype' , 'user' , 'document_indice' , 'process' , 'category' , 'date' , 'integer' , 'decimal', 'select');
  $SelectSet = array_combine($metadata->accept_field_type , $metadata->accept_field_type);
  $select =& $form->addElement('select', 'field_type' , tra('field_type'), $SelectSet , array('onChange' => 'afficherAutre()'));
  $select->setSize(1);
  $select->setMultiple(false);
  $form->addElement('textarea', 'field_regex', tra('field_regex'), array('cols'=>40, $field_regex_access));
  $form->addElement('textarea', 'field_list', tra('field_list'), array('cols'=>40, $field_list_access));
  $form->addElement('checkbox', 'field_required', tra('field_required'));
  $form->addElement('checkbox', 'field_multiple', tra('field_multiple'));
  $form->addElement('checkbox', 'return_name', tra('return_name'));
  $form->addElement('checkbox', 'adv_select', tra('adv_select'));
  $form->addElement('text', 'field_where', tra('field_where'), array('size'=>80, $field_where_access));
  $form->addElement('text', 'field_size', tra('field_size'), array('size'=>10, $field_size_access));
  $form->addElement('text', 'table_name', tra('table_name'), array('cols'=>40));
  $form->addElement('text', 'field_for_value', tra('field_for_value'), array('cols'=>40));
  $form->addElement('text', 'field_for_display', tra('field_for_display'), array('cols'=>40));
  $form->addElement('text', 'date_format', tra('date_format'), array('cols'=>20));
  $form->addElement('hidden', 'space', $Manager->SPACE_NAME);

  if( is_a($metadata, 'docmetadata') ){
    require_once('class/propset.php');
    $propset = new propset($space);
    $p = array(
              'field_name' => 'assign_to',
              'field_description'=>tra('assign to property set'),
              'default_value' => $Infos['default_process_id'],
              'field_size' => 5,
              'field_multiple' => true,
              'field_required' => false,
              'field_required' => false,
              'adv_select' => true
    );
    construct_select_propset($p , $form, $propset);
  
    $form->addElement('text', 'propset_name', tra('create properties set'), array('size'=>10));
  }

  //Add validation rules to check input data
  $form->addRule('field_size', 'Should be numeric', 'numeric', null, 'server');
  $form->addRule('field_name', tra('Name is required'), 'required');
  $form->addRule('field_name', tra('field_name should contains lower letters and/or _ only'), 'regex', '/^[a-z_]+$/', 'server');
  $form->addRule('field_description', tra('Description is required'), 'required');
  $form->addRule('field_type', tra('Type is required'), 'required');
  
  // applies new filters to the element values
  $form->applyFilter('__ALL__', 'trim');
  

  if ($_REQUEST['action'] == 'create' || $_REQUEST['action'] == 'modify')
  //var_dump($_REQUEST); die;
  if ($form->validate()) {
    //$form->freeze(); //and freeze it
    // Form is validated, then processes the modify request
    if ($_REQUEST['action'] == 'modify'){
      check_ticket( $prefix.'_metadata_modify' , $area_id );
      $form->process('modifyField', true);
    }
    // Form is validated, then processes the create request
    if ($_REQUEST['action'] == 'create'){
      check_ticket( $prefix.'_metadata_create' , $area_id );
      $form->process('createField', true);
    }
  } //End of validate form

  //Set the renderer for display QuickForm form in a smarty template
  include 'QuickFormRendererSet.php';
  
  function createField($values){
    global $metadata;
    global $area_id;
    //var_dump($values);die;

    $metadata->set_type($values['field_type']);
    $metadata->CreateMetadata( $values );
  } //End of function

  function modifyField($values){
    global $metadata;
    global $space;
    //var_dump($values);die;
    require_once('class/propsetPropertyLink.php');
    $propsetPropertyLink = new propsetPropertyLink($space);
    $values['propset_name'] = trim($values['propset_name']);
    if( $values['propset_mode'] == 'create_new' && !empty( $values['propset_name'] ) ){
      global $propset;
      $propset_id = $propset->CreateSet( array('propset_name'=>$values['propset_name']) );
      if($propset_id){
        $propsetPropertyLink->AddLink( array('propset_id'=>$propset_id,
                                             'property_id'=>$values['property_id']) );
      }
    }else if( is_array($values['assign_to'] ) ){
      foreach( $values['assign_to'] as $propset_id){
        if(!empty($propset_id) ){
          $propsetPropertyLink->AddLink( array('propset_id'=>$propset_id,
                                               'property_id'=>$values['property_id']) );
        }
      }
    }

    if(!isset($values['field_required'])) $values['field_required'] = 0;
    if(!isset($values['field_multiple'])) $values['field_multiple'] = 0;
    if(!isset($values['return_name'])) $values['return_name'] = 0;
    if(!isset($values['adv_select'])) $values['adv_select'] = 0;
    $metadata->ModifyMetadata( $_REQUEST['field_name'] , $values );
  } //End of function

//---------------------------------------------------------------------

//Include generic definition of the code for manage filters
include('filterManager.php');

//Get list of metadata
if (isset($_REQUEST['SelectedContainer']))
  $SelectedContainer = $_REQUEST['SelectedContainer'];
else 
  $SelectedContainer = '';

$list = $metadata->GetMetadata($params);

$smarty->assign_by_ref( 'list', $list );

//Include generic definition of the code for manage the pagination. $list must set...
include('paginationManager.php');

//Define select option for "find"
$all_field = array ('field_name' => 'Field',
                    'field_description' => 'Description',
                    'field_type' => 'Type',
                   );
$smarty->assign('all_field', $all_field);

//Assign name to particular fields
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number');
$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description');
$smarty->assign('CONTAINER_STATE' , $Manager->SPACE_NAME . '_state');
$smarty->assign('CONTAINER_ID' , $Manager->SPACE_NAME . '_id');

$Manager->error_stack->checkErrors();

// Active the tab
// Display the template
//$smarty->assign('mid', 'workitemDocManage.tpl');
$smarty->display("metadata.tpl");

?>
