<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//this script may only be included - so its better to die if called directly.
if(strpos($_SERVER["SCRIPT_NAME"],basename(__FILE__)) !== false){
  header("location: index.php");
  exit;
}
if (!isset($Manager)) {print '$Manager is not set '; die;}
//$ocategory =& $Manager->initCategory();

require_once('class/category.php');
$ocategory = new category( $Manager->space );


/*$smarty->assign('number_map', $Manager->FIELDS_MAP_NUM);
$smarty->assign('id_map', $Manager->FIELDS_MAP_ID);
$smarty->assign('description_map', $Manager->FIELDS_MAP_DESC);
$smarty->assign('state_map', $Manager->FIELDS_MAP_STATE);
$smarty->assign('indice_map', $Manager->FIELDS_MAP_INDICE);
$smarty->assign('father_map_id', $Manager->FIELDS_MAP_FATHER );*/

$area_id = $Manager->AREA_ID;

//Manage actions on history

if(!isset($_REQUEST['action'])) $_REQUEST['action'] = '';

switch($_REQUEST['action']){
//---------------------------------------------------------------------
case 'suppressCat':   //Process suppress request
  if(!empty($_REQUEST['checked'])){
    check_ticket( 'container_category' , $area_id);
    foreach ( $_REQUEST['checked'] as $id){
      $ocategory->SuppressCategory($id);
    }
  }
  $_REQUEST['action'] = '';
  break;

case 'defaut':
  break;
} //End of switch

//---------------------------------------------------------------------

  //Create formulaire

  require_once "HTML/QuickForm.php"; //Librairy to easily create forms
  require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm
  require_once './lib/Date/date.php';
  
  //Construct the form with QuickForm lib
  $form = new HTML_QuickForm('createForm', 'post');

  if(empty($_REQUEST['action']) || $_REQUEST['action'] == 'create'){
    $form->setDefaults(array(
      'action'      => 'create',
      ));
  } //End of createCat set default

  //Set defaults values of elements if modify request
  if($_REQUEST['action'] == 'modify'){
    //Get infos
    $ocategory->init($_REQUEST['category_id']);
    $Infos = $ocategory->GetCategoryInfos();

    $form->setDefaults(array(
      'category_number'      => $Infos['category_number'],
      'category_description' => $Infos['category_description'],
      'action'      => 'modify',
      ));
    //Add hidden fields
    $form->addElement('hidden', 'category_id', $_REQUEST['category_id']);
    $smarty->assign('action', 'modify');
  } //End of modify set default

  //Add submit button
  $form->addElement('reset', 'reset', 'reset');
  $form->addElement('submit', 'action', 'Go');

  //Add form elements
  $form->addElement('text', 'category_number', tra('Name'));
  $form->addElement('text', 'category_description', tra('Description'));
  
  // applies new filters to the element values
  $form->applyFilter('__ALL__', 'trim');
  
  //Add validation rules to check input data
  $form->addRule('category_number', tra('Name is required'), 'required');
  $form->addRule('category_description', tra('Description is required'), 'required');
  
  if ($_REQUEST['action'] == 'create' || $_REQUEST['action'] == 'modify')
  if ($form->validate()) {
  //$form->freeze(); //and freeze it
  
  // Form is validated, then processes the modify request
  if ($_REQUEST['action'] == 'modify'){
    $form->process('modifyCat', true);
  }
  
  // Form is validated, then processes the create request
  if ($_REQUEST['action'] == 'create'){
    $form->process('createCat', true);
  }

  } //End of validate form

  //Set the renderer for display QuickForm form in a smarty template
  include 'QuickFormRendererSet.php';
  
  function createCat($values){
    global $ocategory;
    global $area_id;
    check_ticket('container_category' , $area_id);
    $ocategory->CreateCategory($values);
  } //End of function

  function modifyCat($values){
    global $ocategory;
    global $area_id;
    check_ticket('container_category' , $area_id);

    unset($values['category_id']);
    unset($values['action']);
    $ocategory->init($_REQUEST['category_id']);
    $ocategory->ModifyCategory($values);
  } //End of function

//---------------------------------------------------------------------

//Include generic definition of the code for manage filters
include('filterManager_simple.php');

//Get list of categories
if (isset($_REQUEST['SelectedContainer']))
  $SelectedContainer = $_REQUEST['SelectedContainer'];
else 
  $SelectedContainer = '';

$list = $ocategory->GetCategories($params);

$smarty->assign_by_ref( 'list', $list );

$smarty->assign_by_ref('sameurl_elements', $sameurl_elements);
$sameurl_elements = array('action', 'space');

//Include generic definition of the code for manage the pagination. $list must set...
include('paginationManager.php');

//Define select option for "find"
$all_field = array ('category_number' => 'Name',
                    'category_description' => 'Description',
                   );
$smarty->assign('all_field', $all_field);

//Assign name to particular fields
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);

$Manager->error_stack->checkErrors();

// Active the tab
// Display the template
$smarty->display("categories.tpl");

?>
