<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Ranchbe is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//this script may only be included - so its better to die if called directly.
if (strpos($_SERVER["SCRIPT_NAME"],basename(__FILE__)) !== false) {
  header("location: index.php");
  exit;
}

//Function to count the rows in result of a query
function count_query($query){
  global $dbranchbe;

  if(!$Rset = $dbranchbe->Execute($query)){
    print 'error on query: '.$dbranchbe->ErrorMsg().'<br />'.$query;
    return false;
  }else{
    return $Rset->RecordCount();
  }
} //End of function

function typeName($type_id='0')
{
  global $dbranchbe;
 
  $query = "SELECT doctype_number FROM doctypes WHERE doctype_id = '$type_id'
            ";

  if(!$number = $dbranchbe->GetOne($query)){
    return 'undefined';
  }else{
    return $number;
  }
} //End of function

function indiceName($indice_id='0')
{
  global $dbranchbe;
 
  $query = "SELECT indice_value FROM document_indice WHERE document_indice_id = '$indice_id'";

  if(!$number = $dbranchbe->GetOne($query)){
    return 'undefined';
  }else{
    return $number;
  }
} //End of function

function categoryName($category_id='0')
{

  global $dbranchbe;

  global $Manager;
 
  $TABLE = $Manager->SPACE_NAME.'_categories';
  
  $query = "SELECT category_number FROM $TABLE WHERE category_id = '$category_id'
            ";

  if(!$number = $dbranchbe->GetOne($query)){
    return 'undefined';
  }else{
    return $number;
  }
} //End of function

//-------------------------------------------------------------------------

if(!isset($Manager) ) {
	print '$Manager is not set '; 
	die;
}

$area_id = $Manager->AREA_ID;
$space_name = $Manager->SPACE_NAME;
$container_id = $Manager->GetId();

//var_dump($space_name, $container_id);

check_ticket( 'container_get_stat' , $area_id );

//Select the graphs to display
//construct the list of graphs dispos

$path_queries_scripts = './inc/stats/stats_queries';

//Construct the form with QuickForm lib
require_once "HTML/QuickForm.php"; //Librairy to easily create forms
require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm

$form = new HTML_QuickForm('form', 'post');

$files = glob($path_queries_scripts . "/stats_query_*.php");
foreach ($files as $filename) $list[basename($filename)] = substr(basename(basename($filename), '.php') , 12);
$select =& $form->addElement('select', 'statistics_queries', tra('Queries'), $list );
$select->setSize(10);
$select->setMultiple(true);

//Add hidden fields
$form->addElement('hidden', 'container_id', $container_id);
$form->addElement('hidden', 'space', $space_name);
$form->addElement('hidden', 'action', 'getStats');

//Reset and submit
$form->addElement('reset', 'reset', 'reset');
$form->addElement('submit', 'submit', 'Display');
if($form->validate()){
  $graphs = array();
  foreach ($form->getElementValue('statistics_queries') as $script){
    include $path_queries_scripts . '/' . $script;
    $result = call_user_func(basename($script, '.php'));
    if(is_array($result))
      $graphs = array_merge($result,$graphs);
  }
} //End of validate form
//Set the renderer for display QuickForm form in a smarty template
include 'QuickFormRendererSet.php';

//Assign name to particular fields
$smarty->assign('graphs', $graphs);
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number' );
$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description' );
$smarty->assign('CONTAINER_STATE' , $Manager->SPACE_NAME . '_state' );
$smarty->assign('CONTAINER_ID' , $Manager->SPACE_NAME . '_id' );

$smarty->assign('PageTitle' , 'Stats of ' . $Manager->SPACE_NAME .' '. $Manager->GetName());

// Active the tab
$smarty->assign("$Manager->SPACE_NAME".'Tab' , 'active');

// Display the template
$smarty->assign('randWindowName', 'container_'.uniqid());
$smarty->assign('mid', 'containerStats.tpl');
$smarty->display("ranchbe.tpl");

?>
