<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Ranchbe is distributed in the hope that it will be useful,                |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//this script may only be included - so its better to die if called directly.
if (strpos($_SERVER["SCRIPT_NAME"],basename(__FILE__)) !== false){
	header("location: index.php");
	exit;
}

if(!isset($Manager)){
	print '$Manager is not set<br />';
	print 'You must select a container<br />';
	die;
}

$Manager->RecordSelectedContainer(); //Record the selected container in the session

//Area_id in session = area of the current project. If not set take the area_id of the current container
/*if(isset( $_SESSION['area_id']) && is_numeric($_SESSION['area_id']))
 $area_id             = $_SESSION['area_id'];
 else
 $area_id             = $Manager->AREA_ID;
 */

$area_id             = $Manager->AREA_ID;

if ( isset ( $_SESSION['SelectedProjectNum']))
$selected_project    = $_SESSION['SelectedProjectNum'];
if ( isset ( $_SESSION['SelectedProject']))
$selected_project_id    = $_SESSION['SelectedProject'];

/*v0.5.2
 if(!empty($_SESSION['SelectedContainerNum']))
 $container_number = $_SESSION['SelectedContainerNum'];
 else {$Manager->error_stack->push(ERROR, 'Fatal', array(), 'none container selected' ); print 'none container selected'; return false;}

 if(!empty($_SESSION['SelectedContainer']))
 $container_id = $_SESSION['SelectedContainer'];
 else {$Manager->error_stack->push(ERROR, 'Fatal', array(), 'none container selected!!!' ); return false;}

 //Init the manager object
 $Manager->init($container_id);
 */

//Assign name to particular fields
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number');
$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description');
$smarty->assign('CONTAINER_STATE' , $Manager->SPACE_NAME . '_state');
$smarty->assign('CONTAINER_ID' , $Manager->SPACE_NAME . '_id');

// Active the tab
$smarty->assign('documentManageScript', "$Manager->SPACE_NAME".'DocManage.php'); //Assign variable to smarty
$smarty->assign('documentManage', 'active'); //Assign variable to smarty

check_ticket('container_document_get',$area_id);

//---------------------------
function ChangeIndice($values){
	global $odocument;
	$indice_id     = $values['indice'];
	$document_id   = $values['checked'][0];
	$odocument->UpgradeIndice($indice_id);
	//print '<b>Cette fonction est momentanement hors service. Reessayez plus tard...<br></b>';
} //End of function

//---------------------------
function createDoc($values){
	global $Manager;
	/*
	 $indice             = $values['indice'];
	 $designation        = $values['designation'];
	 $file               = $values['file'];
	 $SelectedContainer  = $values['SelectedContainer'];
	 $category           = $values['category'];
	 $lock               = $values['lock'];
	 */
	$lock = $values['lock']; unset($values['lock']);
	foreach($values as $key=>$val){
		if(is_array($val)){
			global $form;
			if($form->GetElementType($key) == 'date')
			$values[$key] = mktime($val['h'], $val['m'], $val['s'], $val['M'], $val['d'], $val['Y']);
			else $values[$key] = implode('#',$val);
		}
	}
	if( $document =& $Manager->GetDoc() )
	return $document->Store($values , $lock);
	return false;
} //End of function

//---------------------------
function createMultiDoc($values){
	global $Manager;
	//$Manager =& $values['objectManager'];
	/*
	 $indice             = $values['indice'];
	 $designation        = $values['designation'];
	 $file               = $values['file'];
	 $SelectedContainer  = $values['SelectedContainer'];
	 $category           = $values['category'];
	 $lock               = $values['lock'];
	 */
	$lock = $values['lock']; unset($values['lock']);
	$loop_id = $values['loop_id']; unset($values['loop_id']);
	$container_id = $values['container_id']; unset($values['container_id']);

	unset($values['container_number']);
	unset($values['ticket']);
	unset($values['tag']);
	unset($values['isStored']);

	foreach($values as $key=>$val){
		$values[$key] = $val[$loop_id];
	}

	foreach($values as $key=>$val){
		if(is_array($val)){
			global $form;
			if($form->GetElementType($key) == 'date')
			$values[$key] = mktime($val['h'], $val['m'], $val['s'], $val['M'], $val['d'], $val['Y']);
			else $values[$key] = implode('#' , $val);
		}
	}
	$values['container_id'] = $container_id;
	if( $document =& $Manager->initDoc() )
	return $document->Store($values , $lock);
	return false;
} //End of function

//---------------------------
function modifyDoc( $values ){
	global $odocument;
	global $smarty;
	global $area_id;
	if(!check_ticket( 'container_document_change_number' , $area_id , false))
	unset($values['document_number']);

	foreach($values as $key=>$val){
		if(is_array($val)){
			global $form;
			if($form->GetElementType($key) == 'date')
			$values[$key] = mktime($val['h'], $val['m'], $val['s'], $val['M'], $val['d'], $val['Y']);
			else $values[$key] = implode('#' , $val);
		}
	}
	$odocument->init($values['document_id']);
	unset($values['document_id']);
	$odocument->UpdateDocInfos($values);
} //End of function

//-----------------------------------------------------------------
//Genere une ligne de formulaire dans le cas du store multiple
//Le principe est de renommer chaque champ du formulaire (y compris les champs issues des metadonnees crees par les utilisateurs)
//et d'y ajouter '[$i]' ou $i est un numero incrementale issue de la boucle de parcours de $_REQUEST['checked'].
//Le formulaire retourne alors des variables tableaux. La designation[1] correspondra au fichier[1] etc.
function SetCreateDocForm(container $Manager, $file , $defaultValues , $loop_id){
	global $smarty;
	global $area_id;
	global $form;
	global $ticket;

	$container_id = $Manager->GetId();
	$container_number = $Manager->GetName();
	$space =& $Manager->space;

	//Test if file exist
	$wilspacePath = $space->getWildspace();
	require_once './class/common/fsdata.php';
	$odata = fsdata::_dataFactory($wilspacePath.'/'.$file);
	if(!$odata){
		$Manager->error_stack->push(ERROR, 'Error', array('element'=>$file), 'file %element% is not in wildspace.');
		return false;
	}

	$file = $odata->getProperty('file_name');
	$document_number = $odata->getProperty('doc_name');

	//Construct the form with QuickForm lib
	require_once('./GUI/GUI.php');
	$form = new HTML_QuickForm('form', 'post');

	//Add form elements
	$form->setDefaults(array('action' => 'ValidateStore'));

	$form->addElement('header', 'infos', 'Create a document' . ' in '. $container_number);
	$form->addElement('text', 'file['.$loop_id.']', 'File' ,array('readonly', 'value'=>$file, 'size'=>32));
	$mask = DEFAULT_DOCUMENT_MASK;
	$form->addRule('file['.$loop_id.']', 'This file name is not valid', 'regex', "/$mask/" , 'server');
	$form->addElement('text', 'document_number['.$loop_id.']', 'Document_number' , array('readonly', 'value'=>$document_number, 'size'=>32));

	$form->addElement('checkbox', 'lock['.$loop_id.']', 'Lock the document after store', 'yes');

	//Add select document indice
	$params = array(
            'field_multiple' => false,
            'field_name' => 'document_indice_id['.$loop_id.']',
            'field_description' => 'indice',
            'field_size' => '1',
            'field_where' => "document_indice_id > 0",
            'field_required' => true,
            'default_value' => 0,
	);
	construct_select_document_indice($params , $form , $Manager);

	//Common
	$form->addElement('textarea', 'designation['.$loop_id.']', 'Designation' , array('rows'=>2 , 'cols' => 32));
	$form->addRule('designation['.$loop_id.']', 'Designation is required', 'required');
	$form->addRule('document_number['.$loop_id.']', 'This document name is not valid', 'regex', "/$mask/" , 'server');

	//Add select category
	$params = array(
            'field_multiple' => false,
            'field_name' => 'category_id['.$loop_id.']',
            'field_description' => 'category',
            'field_size' => '1',
            'return_name' => false,
            'default_value' => $Infos['category_id'],
	);
	construct_select_category($params , $form , $Manager);

	//Get fields for custom metadata
	require_once('./class/common/metadata.php');
	$metadata = new docmetadata($Manager->space);
	$optionalFields = $metadata->GetMetadataLinked(NULL, $container_id);
	$smarty->assign( 'optionalFields' , $optionalFields);
	foreach($optionalFields as $field){
		$field['field_name'] = $field['field_name'] . '['.$loop_id.']';
		construct_element($field , $form); //function declared in GUI.php
	}

	//Add hidden fields
	$form->addElement('hidden', 'container_number', $container_number);
	$form->addElement('hidden', 'container_id', $container_id);
	$form->addElement('hidden', 'ticket', $ticket);
	$form->addElement('hidden', 'tag', 'validate');
	//$form->addElement('hidden', 'checked['.$loop_id.']', $file);

	//Add submit button
	$form->addElement('reset', 'reset', 'reset');
	$form->addElement('submit', 'action', 'createDoc');

	//Apply new filters to the element values
	$form->applyFilter('__ALL__', 'trim');

	$smarty->assign( 'isStored' , 0);

	// Try to validate the form
	//if(isset($_REQUEST['ValidateStore'])){
	if( $_REQUEST['action'] == 'ValidateStore' ){
		if ($form->validate()){
			if($_REQUEST['isStored'][$loop_id] == 1){
				$smarty->assign( 'isStored' , 1);
				$form->freeze(); //and freeze it
			}else{
				$form->_submitValues['loop_id'] = $loop_id;
				if ($form->process('createMultiDoc', true)){ //Call createMultiDoc function
					$smarty->assign( 'isStored' , 1);
					$form->freeze(); //and freeze it
				}
			}
		}
	}

	//Set the renderer for display QuickForm form in a smarty template
	include 'QuickFormRendererSet.php';

} //End of function

//-----------------------------------------------------------------
//Genere une ligne de formulaire dans le cas du change state multiple
//Le principe est de renommer chaque champ du formulaire
//et d'y ajouter '[$i]' ou $i est un numero incrementale issue de la boucle de parcours de $_REQUEST['checked'].
//Le formulaire retourne alors des variables tableaux. La designation[1] correspondra au fichier[1] etc.

function SetChangeStateForm(docflow &$workflow){
	global $smarty;
	global $logger;
	/*include ('conf/galaxia_setup.php');
	 include ('lib/Galaxia/ProcessMonitor.php');
	 include ('lib/Galaxia/API.php');*/

	$document =& $workflow->getDocument();
	$document_id = $document->GetId();
	$smarty->assign('document_id', $document_id);
	$smarty->assign('doc_info', $document->GetDocumentInfos());

	$instance =& $workflow->initInstance(); //init the instance from the document properties
	if($instance === false)
	$logger->log( 'SetChangeStateForm:: unable to init instance' );

	$iid = $instance->getInstanceId();

	if(!empty($iid)){ //If a workflow instance is running
		$running = true;
		// Get the instance and set instance information
		//$ins_info = $instanceManager->get_instance($iid);
		$ins_info = $workflow->getInstanceInfos();
		$smarty->assign_by_ref('ins_info', $ins_info);

		// Get the process from the instance and set information
		$workflow->initProcess($ins_info['pId']); //init the process
		$proc_info = $workflow->getProcessInfos();
		//Check if process is identical to previous document process : to ensure coherence between documents
		if( !empty($_SESSION['changeStateCaddie']['current']['pId']) ){
			if( $_SESSION['changeStateCaddie']['current']['pId'] != $proc_info['pId']){
				$msg = 'document %element% is rejected : have not same process that other selected document.';
				$e = array('element'=>$document->GetDocProperty('document_number'));
				$document->error_stack->push(ERROR, 'Error', $e, $msg);
				return false;
			}
		}
		$smarty->assign('proc_info', $proc_info);

		//Get infos about activities
		$acts = $workflow->getInstanceActivityInfo();
		$smarty->assign('act_info', $acts['running']); // inform user about running activity
		if(is_array($acts['standalone'])) //merge to create the html select of activity to activiate
		$activities = array_merge($acts['activities'] , $acts['standalone']);
		else $activities = $acts['activities'];
		$running_activity_id = $acts['running']['activityId']; //Set this var to check coherence in next step...

	}else{  // If no instance link get the process and display activities
		$workflow->initProcess(); //init the process

		$running = false;

		//Check if this item has access free only if a start activity is required
		$accessCode = $document->CheckAccess();
		if(!($accessCode == 0 || ($accessCode > 1 && $accessCode <= 5 ))){
			$msg = 'This item is not free';
			$document->error_stack->push(ERROR, 'Error', array(), $msg);
			return false;
		}//Else no access is required for current instance

		//Get the project id from the session record information
		$project_id = $_SESSION['selectedProject'];

		//Check if there is a process linked to doctype,container,project
		$default_process_id = $document->GetProcessId();
		if(!$default_process_id){
			$msg = 'no default process is find';
			$document->error_stack->push(ERROR, 'Error', array(), $msg);
			return false;
		}

		//Get infos about the selected process
		$proc_info = $workflow->getProcessInfos();
		$smarty->assign('proc_info', $proc_info);

		//Get infos about activities
		$acts = $workflow->getInstanceActivityInfo();
		if(is_array($acts['standalone'])) //merge to create the html select of activity to activiate
		$activities = array_merge($acts['activities'] , $acts['standalone']);
		else $activities = $acts['activities'];
		$running_activity_id = 0; //Set this var to check coherence in next step...

	} //End of else

	//Check if activity is identical to previous selected document process : to ensure coherence between selected documents
	if( isset($_SESSION['changeStateCaddie']['current']['activityId']) ){
		if( $_SESSION['changeStateCaddie']['current']['activityId'] !== $running_activity_id ){
			$msg = 'document %element% have not same state that %element2%. it is not selected';
			$e['element'] = $document->GetDocProperty('document_number');
			$e['element2'] = $_SESSION['changeStateCaddie']['current']['document_number'];
			$document->error_stack->push(ERROR, 'Error', $e, $msg);
			return false;
		}
	}else{
		//Get paths for graph
		$graph = 'lib/Galaxia/processes/' . $proc_info['normalized_name'] . "/graph/" . $proc_info['normalized_name'] . ".png";
		$_SESSION['changeStateCaddie']['current']['document_number'] = $document->GetDocProperty('document_number'); //string
		$_SESSION['changeStateCaddie']['current']['activityId'] = $running_activity_id; //int
		$_SESSION['changeStateCaddie']['current']['activities'] = $activities; //array
		$_SESSION['changeStateCaddie']['current']['is_running'] = $running; //bool
		$_SESSION['changeStateCaddie']['current']['pId'] = $proc_info['pId']; //int
		$_SESSION['changeStateCaddie']['current']['pgraph'] = $graph; //string
	}
	return true;

} //End of function

//---------------------------
/*
 */
function __checkErrors(){
	global $smarty;
	global $error_stack;
	if ( $error_stack->hasErrors() ){
		return $error_stack->getErrors(true);
	}
} //End of function

//---------------------------
/* Process comments
 */
function inputComment($activity_id){
	//require_once ('conf/galaxia_setup.php');
	//require_once ('lib/Galaxia/API.php');
	global $error_stack;
	global $smarty;
	global $dbGalaxia;

	if(empty($activity_id)){
		$msg = 'activity_id is empty';
		$error_stack->push(ERROR, 'Info', array(), $msg);
		return false;
	}

	$baseActivity = new BaseActivity($dbGalaxia);
	$activity = $baseActivity->getActivity($activity_id);
	$process = new Process($dbGalaxia);
	$process->getProcess($activity->getProcessId());
	$instance = new Instance($dbGalaxia);
	$__comments = $instance->get_instance_comments($activity->getActivityId());
	$smarty->assign('procname', $process->getName());
	$smarty->assign('procversion', $process->getVersion());
	$smarty->assign('actname', $activity->getName());
	$smarty->assign('actid',$activity->getActivityId());
	$smarty->assign('post','n');
	$smarty->assign('iid',$instance->instanceId);
	//$smarty->assign('CnextUsers',implode('%3B',$instance->CnextUsers));
	//$smarty->assign('start_page', $_REQUEST['start_page']); //Store URL to return to this page from next step pages
	$smarty->assign('completed', true);
	$smarty->assign('mid', 'ChangeStateMulti.tpl');
	$smarty->display("ChangeStateMulti.tpl");

} //End of function

//Execute view and download before call to function check_flood
//If not, problem with ticket coherence
/*
 if ($_REQUEST['action'] == 'viewDocument' || $_REQUEST['action'] == 'downloadFile'){
 if(empty($_REQUEST["checked"]))
 $Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select ar least one item');
 else{
 require_once('./class/common/document.php');
 $odocument = new document($space, $_REQUEST["checked"][0]);
 $odocument->ViewDocument();
 }
 }
 */

//Prevent reused old POST data
$check_flood = check_flood($_REQUEST['page_id']);

//Manage actions on documents
if (isset($_REQUEST['action']) && !empty($_REQUEST['action'])){

	switch ( $_REQUEST['action'] ) {

		//---------------------------------------------------------------------
		case ('Store'):   //Store a file
		case ('StoreMulti'):   //Store any files at one time
			//if(!$check_flood) break;
			if (empty($_REQUEST["checked"])){
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select one file');
				$Manager->error_stack->checkErrors(array('close_button'=>true));
			}else{
				//Display the header and the core
				$smarty->assign( 'displayHeader' , 1); // true for display header
				$i = 0;
				foreach ($_REQUEST["checked"] as $file){
					$i = $i + 1;
					//$file = basename($file); //pourquoi? cela pertube le stockage des adraws
					//$document_number = substr($file, 0, strrpos($file, '.'));
					//Display a recap array to input metadatas
					SetCreateDocForm($Manager, $file , $defaultValues , $i);
					// Display the template
					$smarty->assign( 'loop_id' , $i);
					$smarty->display('StoreMulti.tpl');
					//Display the header only
					$smarty->assign('displayHeader' , 0);
				}
				//Display the footer
				$smarty->assign( 'displayFooter' , 1);
				$smarty->display('StoreMulti.tpl');

				$Manager->error_stack->checkErrors();
				die;
			}
			break;

			//---------------------------------------------------------------------
		case ('ValidateStore'):   //Store any files at one time
			//echo '<pre>';
			//var_dump($_REQUEST);
			//echo '</pre>';

			if (empty($_REQUEST["file"])){
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select one file');
				$Manager->error_stack->checkErrors(array('close_button'=>true));
			}else{
				//Display the header and the core
				$smarty->assign( 'displayHeader' , 1); // true for display header

				$i = 0;
				foreach ($_REQUEST["file"] as $file){
					$i = $i + 1;
					//Display a recap array to input metadatas
					SetCreateDocForm( $Manager , $file , $defaultValues , $i);
					//Display the template
					$smarty->assign( 'loop_id' , $i);
					$smarty->display('StoreMulti.tpl');
					//Display the header only
					$smarty->assign( 'displayHeader' , 0);
				}

				//Display the footer
				$smarty->assign( 'displayFooter' , 1);
				$smarty->display('StoreMulti.tpl');
				$Manager->error_stack->checkErrors();
				die;
			}
			break;

			//---------------------------------------------------------------------
			/*
			 case ('Store'):   //Store a file
			 if (!empty($_REQUEST['checked'][0])){
			 //$wildspace = new wildspace();
			 //require_once('class\common\fsdata.php');
			 //$fsdata = new fsdata($wildspace->GetPath().'/'.$_REQUEST['checked'][0]);

			 //Test if file exist
			 $wilspacePath = $Manager->space->getWildspace();
			 require_once './class/common/fsdata.php';
			 $fsdata = fsdata::_dataFactory($wilspacePath.'/'.$_REQUEST['checked'][0]);
			 if(!$fsdata){
			 $space->error_stack->push(ERROR, 'Error', array('element'=>$_REQUEST['checked'][0]), 'file %element% is not in wildspace.');
			 $Manager->error_stack->checkErrors(array('close_button'=>true));
			 return false;
			 }

			 $file = $fsdata->getProperty('file_name');
			 $document_number = $fsdata->getProperty('doc_name');
			 }else{
			 $Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select one file');
			 $Manager->error_stack->checkErrors(array('close_button'=>true));
			 return false;
			 }
			 //No break : continue execution on next case...
			 */
			//---------------------------------------------------------------------
		case ($_REQUEST['action'] == 'createDoc') || ($_REQUEST['action'] == 'modifyDoc'): //Create a new doc

			//if(!$check_flood) break;
			//Display form to input data
			// -- Get fields to display
			// -- Create document

			//Area_id in session = area of the current project. If not set take the area_id of the current container
			if(isset($_SESSION['area_id']) && is_numeric($_SESSION['area_id']))
			$area_id = $_SESSION['area_id'];
			else
			$area_id = $Manager->AREA_ID;
			check_ticket('container_document_manage' , $area_id);

			if (!empty ($_REQUEST['SelectedContainerNum']))
			$container_number = $_REQUEST['SelectedContainerNum'];
			else {$Manager->error_stack->push(ERROR, 'Fatal', array(), 'none container selected' ); return false;}
			if (!empty ($_REQUEST['SelectedContainer']))
			$container_id = $_REQUEST['SelectedContainer'];
			else {$Manager->error_stack->push(ERROR, 'Fatal', array(), 'none container selected' ); return false;}

			//-- Construct the form with QuickForm lib
			require_once('./GUI/GUI.php');
			$form = new HTML_QuickForm('form', 'post');

			//----- Modify doc -----------------------
			if ($_REQUEST['action'] == 'modifyDoc'){
				$document_id = $_REQUEST['checked'][0];
				$odocument =& $Manager->initDoc($document_id); //Get the document from the current container

				if(check_ticket( 'container_document_change_number' , $area_id , false)){ //Check if user has perm for modify the number. If it is, add a number field.
					$form->addElement('text', 'document_number', 'Document_number' , array('value'=>$document_number, 'size'=>32));
					$smarty->assign( 'modifyDocumentNumber' , 1);
				}

				//Get infos
				$form->setDefaults(array(
				      'document_number' => $odocument->GetDocProperty('document_number'),
				      'designation' => $odocument->GetDocProperty('designation'),
				      'category_id' => $odocument->GetDocProperty('category_id'),
				      'action' => 'modifyDoc'
				      ));
				      //Add hidden fields
				      $form->addElement('hidden', 'checked[]', $document_id);
				      $form->addElement('hidden', 'document_id', $document_id);
				      $form->addElement('header', 'infos', 'Modify '. $odocument->GetDocProperty('document_number'));
			} //End of if modifyDoc

			//----- Create doc -----------------------
			//Add form elements
			if ($_REQUEST['action'] == 'createDoc' || $_REQUEST['action'] == 'Store'){

				$odocument =& $Manager->initDoc(); //Get the document from the current container

				$form->setDefaults(array('action' => $_REQUEST['action']));
				$form->addElement('hidden', 'checked[]', $file);

				if(empty($file)){
					$form->addElement('header', 'infos', 'Create a document in ' . $container_number);
					$form->addElement('text', 'document_number', 'Document_number' , array('value'=>$document_number, 'size'=>32));
				}else{
					$form->addElement('header', 'infos', 'Create a document from '. $file .' in '. $container_number);
					$form->addElement('text', 'file', 'File' ,array('readonly', 'value'=>$file, 'size'=>32));
					$mask = DEFAULT_DOCUMENT_MASK;
					$form->addRule('file', 'This file name is not valid', 'regex', "/$mask/" , 'server');
					$form->addElement('text', 'document_number', 'Document_number' , array('readonly', 'value'=>$document_number, 'size'=>32));
				}
				$form->addElement('checkbox', 'lock', 'Lock the document after store', 'yes');

				//Add select document indice
				$params = array(
              'field_multiple' => false,
              'field_name' => 'document_indice_id',
              'field_description' => 'indice',
              'field_size' => '1',
              'field_where' => "document_indice_id > 0",
              'field_required' => true,
              'default_value' => 0,
				);
				construct_select_document_indice($params , $form , $Manager);

			} //End if createDoc

			//----- Common -----------------------
			//Add hidden fields
			$form->addElement('hidden', 'container_number', $container_number);
			$form->addElement('hidden', 'container_id', $container_id);
			$form->addElement('hidden', 'ticket', $ticket);
			$form->addElement('hidden', 'tag', 'validate');

			$form->addElement('textarea', 'designation', 'Designation' , array('rows'=>5 , 'cols' => 32));
			$form->addRule('designation', 'Designation is required', 'required');
			$form->addRule('document_number', 'This document name is not valid', 'regex', "/$mask/" , 'server');

			//Add select category
			$params = array(
            'field_multiple' => false,
            'field_name' => 'category_id',
            'field_description' => 'category',
            'field_size' => '1',
            'return_name' => false,
			);
			construct_select_category($params , $form , $Manager);

			//Get fields for custom metadata
			require_once('./class/common/metadata.php');
			$metadata = new docmetadata($Manager->space);
			$optionalFields = $metadata->GetMetadataLinked(NULL, $container_id);
			$smarty->assign( 'optionalFields' , $optionalFields);
			foreach($optionalFields as $field){
				if($_REQUEST['action'] == 'modifyDoc')
				$field['default_value'] = $odocument->GetDocProperty($field['field_name']);
				construct_element($field , $form); //function declared in GUI.php
			}

			//Add submit button
			$form->addElement('reset', 'reset', 'reset');
			$form->addElement('submit', 'action', 'createDoc');

			//Apply new filters to the element values
			$form->applyFilter('__ALL__', 'trim');

			// Try to validate the form
			if(isset($_REQUEST['tag']))
			if ($form->validate()){
				//var_dump($_REQUEST);
				$form->freeze(); //and freeze it
				if ($_REQUEST['action'] == 'createDoc' || $_REQUEST['action'] == 'Store')
				$form->process('createDoc', true); //Call createDoc function
				if ($_REQUEST['action'] == 'modifyDoc')
				$form->process('modifyDoc', true); //Call modifyDoc function
			}

			//Set the renderer for display QuickForm form in a smarty template
			include 'QuickFormRendererSet.php';
			$smarty->assign( 'action' , $_REQUEST['action']);
			$Manager->error_stack->checkErrors();

			// Display the template
			$smarty->display('Store.tpl');

			die;
			break;

		//---------------------------------------------------------------------
		case 'SuppressDoc':   //Suppress
			if(!$check_flood) break;
			if (empty($_REQUEST['checked'])){
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item' );
				break;
			}
			require_once('./class/common/document.php');
			$odocument = new document($space);
			if($_REQUEST['validate_suppress']){
				check_ticket( 'container_document_suppress' , $area_id );
				foreach ($_REQUEST['checked'] as $document_id){
					$odocument->init($document_id);
					$odocument->RemoveDocument();
				}
			}else{ //Display validation
				foreach ($_REQUEST['checked'] as $document_id){
					$list[] = $odocument->GetDocumentInfos($document_id);
				}
				$smarty->assign_by_ref('list' , $list);
				$smarty->assign('documentManage' , 'active');
				$smarty->assign('confirmation_text' , 'Are you sure that you want suppress this documents');
				$smarty->assign('confirmation_return' , 'validate_suppress');
				$smarty->assign('confirmation_formaction' , 'DocManage.php?docfileManage=0&space='.$odocument->space->SPACE_NAME);
				$smarty->assign('confirmation_action' , $_REQUEST['action']);
				$smarty->assign('mid', 'confirm_document_action.tpl');
				$smarty->display('ranchbe.tpl');
				die;
			}
			break;

			//---------------------------------------------------------------------
		case 'MarkToSuppressDoc':   //Mark to suppress
			if(!$check_flood) break;
			if (empty($_REQUEST['checked'])){
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item' );
				break;
			}
			check_ticket( 'container_document_manage' , $area_id );
			require_once('./class/common/document.php');
			$odocument = new document($space);
			foreach ($_REQUEST['checked'] as $document_id){
				$odocument->init($document_id);
				//Check if access is free
				$access = $odocument->CheckAccess();
				if($access > 0){
					$Manager->error_stack->push(ERROR, 'Fatal', array(), tra("You cant mark to suppress this document"));
					break;
				}
				$odocument->LockDocument(12);
				//Lock access to files too
				$docfiles = $odocument->GetDocfiles();
				if(is_array($docfiles))
				foreach($docfiles as $docfile){
					$docfile->LockFile(12);
				}
				$odocument->history['action_name'] = 'MarkToSuppressDoc';
				$odocument->WriteHistory();
			}
			break;

			//---------------------------------------------------------------------
		case 'CheckOutDoc':   //CheckOut
			if(!$check_flood) break;
			if (empty($_REQUEST["checked"])){
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item');
			}else{
				check_ticket('container_document_manage' , $area_id);
				require_once('./class/common/document.php');
				$odocument = new document($space);
				foreach ($_REQUEST["checked"] as $document_id){
					$odocument->init($document_id);
					$odocument->CheckOut();
				}
			}
			break;

			//---------------------------------------------------------------------
		case 'CheckInDoc':   //CheckIn
			if(!$check_flood) break;
			if(empty($_REQUEST['checked'])){
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item' );
				break;
			}
			check_ticket( 'container_document_manage' , $area_id );
			require_once('./class/common/document.php');
			$odocument = new document($space);
			$_REQUEST['comment'] = trim($_REQUEST['comment']);
			if($_REQUEST['validate_checkin']){
				require_once('class/doccomment.php');
				$ocomment = new doccomment($odocument->space);
				foreach ($_REQUEST['checked'] as $document_id){
					$odocument->init($document_id);
					if(!empty($_REQUEST['comment'])){
						$odocument->history['comment'] = $_REQUEST['comment'];
						$ocomment->Add( $document_id, $_REQUEST['comment'] );
					}
					$odocument->CheckIn(false , false);
				}
			}else{ //Display validation
				foreach ($_REQUEST['checked'] as $document_id){
					$list[] = $odocument->GetDocumentInfos($document_id);
				}
				$smarty->assign_by_ref('list' , $list);
				$smarty->assign('documentManage' , 'active');
				$smarty->assign('confirmation_text' , 'Checkin this documents. Add comments');
				$smarty->assign('confirmation_return' , 'validate_checkin');
				$smarty->assign('confirmation_formaction' , 'DocManage.php?space='.$odocument->space->SPACE_NAME);
				$smarty->assign('confirmation_action' , $_REQUEST['action']);
				$smarty->assign('mid', 'checkin_add_comment.tpl');
				$smarty->display('ranchbe.tpl');
				die;
			}
			break;

			//---------------------------------------------------------------------
		case 'LockDoc':   //Lock definitivly access to document
			if(!$check_flood) break;
			if (empty($_REQUEST['checked'])){
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select ar least one item');
			}else{
				check_ticket('container_document_manage' , $area_id);
				require_once('./class/common/document.php');
				$odocument = new document($space);
				foreach($_REQUEST['checked'] as $document_id){
					$odocument->init($document_id);
					//Check if access is free
					$access = $odocument->CheckAccess();
					if($access > 0){
						$Manager->error_stack->push(ERROR, 'Fatal', array(), tra("You cant lock this document"));
						break;
					}
					$odocument->LockDocument(11);
					//Lock access to files too
					$docfiles = $odocument->GetDocfiles();
					if(is_array($docfiles))
					foreach($docfiles as $docfile){
						$docfile->LockFile(11);
					}
					$odocument->history['action_name'] = 'Lock';
					$odocument->WriteHistory();
				}
			}
			break;

			//---------------------------------------------------------------------
		case 'UnMarkToSuppressDoc':   //Unmark a document
		case 'UnLockDoc':   //Unlock a document
			if(!$check_flood) break;
			if (empty($_REQUEST['checked'])) {
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select ar least one item');
			}else{
				check_ticket( 'container_document_unlock' , $area_id);
				require_once('./class/common/document.php');
				$odocument = new document($space);
				foreach ($_REQUEST["checked"] as $document_id){
					//Check if access is free
					$odocument->init($document_id);
					$access = $odocument->CheckAccess();
					if( $access < 5 || $access > 14 ){
						$Manager->error_stack->push(ERROR, 'Fatal', array(), tra("You cant unlock this document"));
						break;
					}
					$odocument->LockDocument(0);
					//UnLock access to files too
					$docfiles = $odocument->GetDocfiles();
					if( is_array($docfiles) )
					foreach($docfiles as $docfile){
						$docfile->LockFile(0);
					}
					//Write history
					if( $_REQUEST['action'] == 'UnMarkToSuppressDoc')
					$odocument->history['action_name'] = 'UnMarkToSuppress';
					else
					$odocument->history['action_name'] = 'Unlock';
					$odocument->WriteHistory();
				}
			}
			break;

			//---------------------------------------------------------------------
		case 'ResetDoc':   //Reset
			if(!$check_flood) break;
			if (empty($_REQUEST["checked"])) {
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select ar least one item' );
			}else{
				check_ticket( 'container_document_manage' , $area_id );
				require_once('./class/common/document.php');
				$odocument = new document($space);
				foreach ($_REQUEST["checked"] as $document_id) {
					$odocument->init($document_id);
					$odocument->CancelCheckOut();
				}
			}
			break;

			//---------------------------------------------------------------------
		case 'UpdateDoc':   //Update
			if(!$check_flood) break;
			if (empty($_REQUEST["checked"])) {
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select ar least one item');
			}else{
				check_ticket('container_document_manage' , $area_id);
				require_once('./class/common/document.php');
				$odocument = new document($space);
				foreach ($_REQUEST["checked"] as $document_id){
					$odocument->init($document_id);
					$odocument->CheckIn($update = true);
				}
			}
			break;

			//---------------------------------------------------------------------
			/*
			 case 'viewDocument':   //Read the file from browser
			 if (empty($_REQUEST["checked"])){
			 $Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select ar least one item' );
			 }else{
			 if($Manager->ViewDocument($_REQUEST["checked"][0]))
			 die;
			 }
			 break;
			 */

			//---------------------------------------------------------------------
		case 'putInWs':   //Put file in wildspace
			if (empty($_REQUEST['checked'])){
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select ar least one item');
			}else{
				require_once('./class/common/document.php');
				$odocument = new document($space);
				foreach($_REQUEST['checked'] as $document_id){
					$odocument->init($document_id);
					$odocument->PutInWildspace();
				}
			}
			break;

			//---------------------------------------------------------------------
		case 'freeze':   //Freeze document
			if (empty($_REQUEST['checked'])){
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select ar least one item');
			}else{
				require_once('./class/common/document.php');
				$odocument = new document($space);

				require_once './class/freeze.php';
				$freeze = new freeze($Manager);

				foreach($_REQUEST['checked'] as $document_id){
					$odocument->init($document_id);
					$freeze->freezeDocument($odocument);
				}
			}
			break;

			//---------------------------------------------------------------------
		case 'freezeAll':   //Freeze all documents of container
			require_once('./class/common/document.php');
			$odocument = new document($space);

			require_once './class/freeze.php';
			$freeze = new freeze($Manager);

			//GetDocument
			$list = $Manager->GetAllDocuments( array() , false);

			foreach($list as $docInfos){
				$odocument->init( $docInfos['document_id'] );
				$freeze->freezeDocument($odocument);
			}
			break;

			//---------------------------------------------------------------------
		case 'reset_doctype':   //Recalculate the doctype of the document
			if(empty($_REQUEST['checked'])){
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select ar least one item');
			}else{
				if(!$check_flood) break;
				check_ticket('container_document_doctype_reset' , $area_id);
				require_once('./class/common/document.php');
				$odocument = new document($space);
				foreach($_REQUEST['checked'] as $document_id){
					$odocument->init($document_id);
					if($docfiles =& $odocument->GetDocfiles()){ //Get all docfiles of the document
						$file_extension = $docfiles[0]->GetProperty('file_extension'); //Get the property of the main docfile
						$file_type = $docfiles[0]->GetProperty('file_type'); //Get the property of the main docfile
					}else{
						$file_extension = NULL;
						$file_type = 'nofile';
					}
					if($doctype = $odocument->SetDocType($file_extension, $file_type))
					$odocument->UpdateDocument(array('doctype_id'=>$doctype['doctype_id']));
				}
			}
			break;

			//---------------------------------------------------------------------
		case 'assocFile':   //Associate a file to the document

			if(isset($_REQUEST['cancel']))
			break;

			if(!isset($_REQUEST['file_name'])){
				if(!$check_flood) break;
				if(count($_REQUEST["checked"]) !== 1){
					$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one and only one item');
					break;
				}
				check_ticket('container_document_assoc' , $area_id);

				$odocument =& $Manager->initDoc($_REQUEST["checked"][0]);
				//Get infos about document
				$document_number = $odocument->GetNumber();
				$smarty->assign('document_number', $document_number);

				//Set params for filter list of files
				$params = array('offset'=>0 , 'maxRecords'=> 99999);

				//ASSOC_FILE_CHECK_NAME is set in ranchbe_setup.php
				//Limit list of files to files with the document name in the name
				if (ASSOC_FILE_CHECK_NAME == true){
					$params['find']['file_name'] = $document_number;
				}

				$wildspace = new wildspace();
				$list = $wildspace->GetDatas($params , false);
				if(is_array($list))
				foreach($list as $infos){
					$SelectSet[$infos['file_name']] = $infos['file_name'];
				}
				$smarty->assign('SelectSet', $SelectSet);
				$smarty->assign('document_id', $_REQUEST["checked"][0]);

				// Display the template
				$smarty->display('selectFile.tpl');
				die;
			} //End of if file_name

			if(isset($_REQUEST['document_id'])){
				$odocument =& $Manager->initDoc($_REQUEST['document_id']);
				$wildspace = new wildspace();
				$file = $wildspace->GetPath().'/'.$_REQUEST['file_name'];
				if(isset($_REQUEST['file_name'])&& is_file($file)){
					if(!$check_flood) break;
					$odocument->AssociateFile($file); //Assoc file "file_name" to document "document_id"
				}
			}
			break;

			//---------------------------------------------------------------------
		case 'AddDocLink':   //Link a document to the document
			break;

			//---------------------------------------------------------------------
		case 'ChangeState':   //Change state
		case 'multi_ChangeState':   //Change state
			if(isset($_REQUEST['cancel']) ) {
				unset($_REQUEST); //to dont interfere filters
				break;
			}

			//FIXME : check flood
			/*if( isset($_REQUEST['ticket']) ){
			 var_dump($_REQUEST['ticket']);
			 if(!$check_flood) break;
			 }*/

			//to retrieve previous selection -- nice.
			if (empty($_REQUEST['checked']) && !empty($_SESSION['changeStateCaddie']['documents']) ){
				$_REQUEST['checked'] = $_SESSION['changeStateCaddie']['documents'];
			}

			if (empty($_REQUEST['checked']) && empty($_SESSION['changeStateCaddie']['documents']) ){
				$msg = tra('You must select at least one item');
				$Manager->error_stack->push(ERROR, 'Fatal', array(), $msg);
				unset($_REQUEST); //to dont interfere with filters
				break;
			}

			check_ticket('container_document_change_state' , $area_id);

			require_once 'conf/galaxia_setup.php'; //set galaxia database, global var and functions
			//include_once 'lib/Galaxia/ProcessMonitor.php'; //set $processMonitor object
			//include_once 'lib/Galaxia/API.php'; // set $process, $instance, $baseActivity

			if(isset($_REQUEST['step'])){
				$step = $_REQUEST['step'];
			}else{
				$step = $_SESSION['changeStateCaddie']['step'];
			}

			//Step1: filter selection, recap selection, display selection
			if($step === 'step1'){
				$logger->log( 'documentManage.php::ChangeState : step :'.$step);
				if(isset($_SESSION['changeStateCaddie'])){
					unset($_SESSION['changeStateCaddie']['documents']); //initialise session var Dont use $_SESSION['changeStateCaddie']=array() > strange effects...
					unset($_SESSION['changeStateCaddie']['step']); //initialise session var Dont use $_SESSION['changeStateCaddie']=array() > strange effects...
					unset($_SESSION['changeStateCaddie']['current']['activityId']); //initialise session var Dont use $_SESSION['changeStateCaddie']=array() > strange effects...
					unset($_SESSION['changeStateCaddie']['current']['activities']); //initialise session var Dont use $_SESSION['changeStateCaddie']=array() > strange effects...
					unset($_SESSION['changeStateCaddie']['current']); //initialise session var Dont use $_SESSION['changeStateCaddie']=array() > strange effects...
					unset($_SESSION['changeStateCaddie']); //initialise session var Dont use $_SESSION['changeStateCaddie']=array() > strange effects...
				}
				//Create form and html header
				require_once ('HTML/QuickForm.php'); //Librairy to easily create forms
				require_once ('HTML/QuickForm/Renderer/ArraySmarty.php'); //Lib to use Smarty with QuickForm
				$form = new HTML_QuickForm('form', 'post');
				$form->addElement('header', 'infos', 'Multi change state of document');
				//Add hidden fields
				$form->addElement('hidden', 'ticket', $ticket);
				$form->addElement('hidden', 'page_id', 'multi_ChangeState');
				$form->addElement('hidden', 'action', 'multi_ChangeState');
				$form->addElement('hidden', 'step', 'step2');
				//Add submit button
				$form->addElement('reset', 'reset', 'reset');
				//$url = $_SERVER['REQUEST_URI'];
				$url = 'DocManage.php?page_id=DocManage_changestate&space='.$Manager->SPACE_NAME.'&action=multi_ChangeState';
				$p = array('onclick' => "document.form.action='".$url."';"); //definie un url par defaut. Cela permet de faire executer les formulaire de tache par cet url
				$form->addElement('submit', 'validate', 'next', $p);
				//return button
				$url = 'DocManage.php';
				$p = array('onclick' => "document.load='".$url."';"); //definie un url par defaut. Cela permet de faire executer les formulaire de tache par cet url
				$form->addElement('submit', 'cancel', 'cancel', $p);

				//Set the renderer for display QuickForm form in a smarty template
				require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm
				$renderer =& new HTML_QuickForm_Renderer_ArraySmarty($smarty, true);
				$form->accept($renderer);
				// assign array with form data
				$smarty->assign('form', $renderer->toArray());
				$smarty->assign('displayHeader' , 1); // true for display header
				$smarty->display('ChangeStateMulti.tpl');
				$smarty->assign('displayHeader' , 0);

				//Create element a recap line in the selecte manager for each document
				$smarty->assign('displayBody' , 1);
				foreach ($_REQUEST['checked'] as $document_id){
					$odocument =& $Manager->initDoc($document_id);
					require_once('class/workflow/docflow.php');
					$workflow = new docflow($odocument);
					//$workflow->initInstance(); //init the instance from the document properties
					//$workflow->initProcess(); //init the process
					//Display a recap array to input metadatas
					if(SetChangeStateForm($workflow)){ //generate html code for one line
						$_SESSION['changeStateCaddie']['documents'][] = $document_id;
					}

					// Display the template
					/*
					 if( is_array($_SESSION['changeStateCaddie']['errors'][$document_id]) ){
					 $smarty->assign('errors', $_SESSION['changeStateCaddie']['errors'][$document_id] ); //get errors from previous execution, display it
					 }else{
					 $smarty->assign('errors', __checkErrors() ); //get errors from error_stack and assign to template
					 }
					 */

					$smarty->assign('errors', __checkErrors() ); //get errors from error_stack and assign to template
					$smarty->display('ChangeStateMulti.tpl');
				}

				$smarty->assign('displayBody' , 0);
				//Generate select activity
				if(is_array($_SESSION['changeStateCaddie']['current']['activities'])){
					foreach($_SESSION['changeStateCaddie']['current']['activities'] as $activity){
						if(($activity['type'] == 'standalone')
						|| ($activity['type'] == 'start' && !$_SESSION['changeStateCaddie']['current']['is_running'])
						|| ($activity['actstatus'] == 'running')
						){
							$acts[$activity['activityId']] = $activity['name'];
						}
					}
					$select =& $form->addElement('select', 'activityId' , tra('activity'), $acts);
				}
				//Get paths for graph
				$smarty->assign( 'graph' , $_SESSION['changeStateCaddie']['current']['pgraph'] );
				$form->accept($renderer); //Render for smarty
				$smarty->assign('form', $renderer->toArray());
				//close html
				$smarty->assign( 'displayFooter' , 1);
				$smarty->display('ChangeStateMulti.tpl');
				$_SESSION['changeStateCaddie']['step'] = 'step2';
			}

			//Step2: display activity template if necessary
			//Call the view script
			if($step === 'step2'){
				$logger->log( 'documentManage.php::ChangeState : step :'.$step);
				$_SESSION['changeStateCaddie']['activity_Id'] = $_REQUEST['activityId'];
				require_once('class/workflow/docflow.php');
				//require_once('class/workflow/workflow.php');
				$workflow = new docflow();
				$workflow->initActivity($_REQUEST['activityId']);

				if($workflow->activity->isInteractive()){
					$workflow->activity->display($workflow, array($_SESSION['changeStateCaddie']['documents'], $Manager));
				}else{
					$step = 'step3';
				}

				/*
				 $workflow->getActivityTemplate($_REQUEST['activityId']);
				 if( $workflow->template == false ){ //its not interactive activity
				 $step = 'step3';
				 }else{
				 $workflow->display();
				 //$smarty->display($workflow->template);
				 }
				 */
				$_SESSION['changeStateCaddie']['step'] = 'step3';
			}

			//Step3: run activity on each selected document
			if($step === 'step3'){
				$logger->log( 'documentManage.php::ChangeState : step :'.$step );
				foreach ($_SESSION['changeStateCaddie']['documents'] as $document_id) {
					$odocument =& $Manager->initDoc($document_id);
					require_once('class/workflow/docflow.php');
					$docflow = new docflow($odocument);
					$docflow->initInstance(); //init the instance from the document properties
					$docflow->initProcess(); //init the process from the document properties
					$instance =& $docflow->execute($_SESSION['changeStateCaddie']['activity_Id']);

					//Record errors in session to display it on next request
					//$_SESSION['changeStateCaddie']['errors'][$document_id] = __checkErrors();

					if( $instance === false ){ //error in execute activity instance
						$e = array('element'=>$odocument->GetDocProperty('document_number'));
						$msg = 'change state failed for document %element%';
						$odocument->error_stack->push(ERROR, 'Fatal', $e, $msg);
					}else{
						$instances_id[] = $instance->getInstanceId();
						$instances_name[] = $instance->getName();
					}
				}
				$activity =& $docflow->getActivity();
				if($activity->isComment == 'y'){
					inputComment($_SESSION['changeStateCaddie']['activity_Id']); //display comment page
				}else{
					$step = 'step5';
				}
				//Record next step in session
				$_SESSION['changeStateCaddie']['instances'] = $instances_id;
				$_SESSION['changeStateCaddie']['step'] = 'step5';
			}

			//Step5: validate comment and update activity
			/* comments are same for all activity of the current selection */
			if($step === 'step5'){
				$logger->log( 'documentManage.php::ChangeState : step :'.$step);
				$title = $_REQUEST['__title'];
				$comment = $_REQUEST['__comment'];

				require_once('class/workflow/workflow.php');
				if( is_array($_SESSION['changeStateCaddie']['instances']) )
				foreach($_SESSION['changeStateCaddie']['instances'] as $instance_id){ //loop instances from the previous execution
					if($instance_id != 0){
						//validateActivityComment($instance_id,$title,$comment);
						$workflow = new workflow();
						$workflow->initInstance($instance_id); //init the instance from the document properties
						$workflow->initProcess(); //init the process from the instance
					}
				}

				/*
				 //Step6: return to document manager
				 $url = 'DocManage.php';
				 $urlp = array(
				 'space' => $Manager->SPACE_NAME ,
				 'action' => 'multi_ChangeState',
				 'page_id' => 'DocManage_changestate',
				 'step' => 'step1',
				 );
				 $i = 0;
				 foreach($_SESSION['changeStateCaddie']['documents'] as $document_id){
				 $urlp['checked['.$i.']'] = $document_id;
				 $i++;
				 }
				 if(isset($_SESSION['changeStateCaddie'])){
				 //unset($_SESSION['changeStateCaddie']); //initialise session var Dont use $_SESSION['changeStateCaddie']=array() > strange effects...
				 sleep(2); //because anti-flood
				 $param = 'space='.$Manager->SPACE_NAME;
				 foreach($urlp as $key=>$val){
				 $param .= '&'.$key.'='.$val;
				 }
				 //header('Location: '.$url.'?'.$param);
				 }
				 */

				unset($_REQUEST); //to dont interfere filters
				break;
			}

			$space->error_stack->checkErrors();
			die;
			break;

			//---------------------------------------------------------------------
			//this code is no more in use. is replaced by multi_changeState case
		case 'ChangeState_____no_more_used':   //Change state
			if (empty($_REQUEST["checked"]) || isset($_REQUEST["checked"][1])){
				$msg = tra('You must select at least one and only item');
				$Manager->error_stack->push(ERROR, 'Fatal', array(), $msg);
				$Manager->error_stack->checkErrors(array('close_button'=>true));
				die;
			}

			check_ticket('container_document_change_state' , $area_id);
			require_once 'conf/galaxia_setup.php';
			include_once 'lib/Galaxia/ProcessMonitor.php';
			include_once 'lib/Galaxia/ProcessManager.php';
			include_once 'lib/Galaxia/API.php';

			$document_id = $_REQUEST["checked"][0];
			$odocument = $Manager->initDoc($document_id);

			//Check if this item has a running process instance and get document number
			$Documentinfos = $odocument->GetDocumentInfos(NULL , array('instance_id' , 'document_number' , 'doctype_id' , 'document_indice_id' , 'designation'));
			$iid = $odocument->GetDocProperty('instance_id');

			$smarty->assign( 'iid' , $iid );
			$smarty->assign( 'document_id' , $document_id );
			$smarty->assign( 'document_number' , $odocument->GetDocProperty('document_number'));
			$smarty->assign( 'document_indice' , $odocument->GetDocProperty('document_indice_id'));
			$smarty->assign( 'document_designation' , $odocument->GetDocProperty('designation'));

			if (!empty($iid)){
				// Get the instance and set instance information
				$ins_info = $instanceManager->get_instance($iid);
				$smarty->assign_by_ref('ins_info', $ins_info);

				// Get the process from the instance and set information
				$proc_info = $processManager->get_process($ins_info['pId']);
				$smarty->assign_by_ref('proc_info', $proc_info);

				// Users
				$users = $LUA->getUsers(array('container' => 'auth'));
				if ($users === false){
					$logger->log( 'List_user: error on line: '.__LINE__ );
					$logger->log( print_r($LUA->getErrors()) );
				}
				$smarty->assign_by_ref('users', $users['data']);

				// Get list of activities from process
				//$acts = $activityManager->list_activities($ins_info['pId'] , 0, -1, 'flowNum_asc', '', '');
				//$smarty->assign_by_ref('acts', $acts['data']);

				//Get list of instances activities
				$acts = $instanceManager->get_instance_activities($iid);
				$smarty->assign_by_ref('acts', $acts);

				//Get the standalone activity from process
				//list_activities($pId, $offset, $maxRecords, $sort_mode, $find, $where = '')
				$standalone = $activityManager->list_activities($ins_info['pId'] , 0, -1, 'flowNum_asc', '', "type = 'standalone'");
				if(is_array($standalone['data']))
				$acts = array_merge($acts , $standalone['data']);

				//Get properties of instances
				//$props = $instanceManager->get_instance_properties($_REQUEST['iid']);

			}else{  // If no instance link get the process and display activities

				//Get the project id from the session record information
				$project_id = $_SESSION['selectedProject'];

				//Check if there is a process link to doctype,container,project
				$default_process_id = $odocument->GetProcessId();
				if(!$default_process_id ){
					$msg = tra('no default process is find');
					$Manager->error_stack->push(ERROR, 'Info', array(), $msg);
					$Manager->error_stack->checkErrors(array('close_button'=>true));
					die;
				}

				//Get infos about the selected process
				$proc_info = $processManager->get_process( $default_process_id );
				$smarty->assign_by_ref( 'proc_info', $proc_info );

				//Get the activities of the process
				$activities = $activityManager->list_activities($default_process_id , 0, -1, 'flowNum_asc', '', '');
				$smarty->assign('acts', $activities['data']);

			} //End of else

			//Get paths for graph
			$graph = 'lib/Galaxia/processes/' . $proc_info['normalized_name'] . "/graph/" . $proc_info['normalized_name'] . ".png";
			$smarty->assign( 'graph' , $graph );

			$smarty->assign('start_page', urlencode($_SERVER['REQUEST_URI'])); //Store URL to return to this page from next step pages
			$smarty->assign('popup', 1);
			$smarty->display('header.tpl');
			$smarty->display('documentActivities.tpl');

			die;
			break;

			//---------------------------------------------------------------------
		case 'RunActivity_______no_more_use':   //Run the activity
			//if(!$check_flood) break;
			if (empty($_REQUEST["checked"])) {
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item');
				$Manager->error_stack->checkErrors(array('close_button'=>true));
				return false;
			}
			check_ticket('container_document_change_state' , $area_id);

			require_once ('conf/galaxia_setup.php');
			//include_once ('lib/Galaxia/API.php');
			$activity = $baseActivity->getActivity($_REQUEST['activityId']);

			$document_id = $_REQUEST['checked'][0]; //Assign the document_id
			$smarty->assign('document_id', $document_id);
			$odocument =& $Manager->InitDoc($document_id);

			//Get infos about document
			$Documentinfos = $odocument->GetDocumentInfos( NULL , array('instance_id' , 'document_number' , 'document_indice_id' , 'doctype_id'));
			$iid = $odocument->GetDocProperty('instance_id');

			$accessCode = $odocument->CheckAccess();

			//Check if this item has a linked instance and get the iid
			if(empty($iid)){ //If no instance linked to document create a new instance
				//Check if this item has access free only if a start activity is required
				if(!($accessCode == 0 || ($accessCode > 1 && $accessCode <= 5 ))){
					$msg = tra('This item is not free');
					$Manager->error_stack->push(ERROR, 'Info', array(), $msg);
					$Manager->error_stack->checkErrors(array('close_button'=>true));
					die;
				}//Else no access is required for current instance
			}

			// Only run a start activity if none instance is linked to document
			// and if access is = 0 to prevent reassign process on document wich is terminate
			if (!empty ($iid) && $activity->getType() == 'start'){
				$msg = tra('There is already an activity in progress on this item or this document is terminated');
				$Manager->error_stack->push(ERROR, 'Info', array(), $msg);
				$Manager->error_stack->checkErrors(array('close_button'=>true));
				die;
			}

			//Set the name of the instance
			$_REQUEST['name'] = $odocument->GetDocProperty('document_number').'_'.$odocument->GetDocProperty('document_indice_id');
			//$_REQUEST['name'] = "$Documentinfos[document_number]" .'_'. "$Documentinfos[document_indice_id]";

			//Call galaxia scripts for run activity
			include 'galaxia_run_activity.php'; //Include the run scripts of tiki-wiki (with modifications)...

			//if ( empty ($iid) && $activity->getType() == 'start') { // If its empty, then no linked instance, so...
			if (empty($iid)) { // If its empty, then link the current instance to document
				$iid = $instance->instanceId;
				$odocument->LinkInstanceToDocument($iid ); //...link the instance
			}

			//Update the RanchBE object manager for reuse data in history writing
			$Manager->aid = $activity->activityId;

			//Write history
			if (isset($odocument->history)){
				$odocument->history['instance_id'] = $iid;
				$odocument->history['activity_id'] = $Manager->aid;
				$odocument->history['action_name'] = 'ChangeState';
				$odocument->WriteHistory();
			}

			$smarty->assign('start_page', $_REQUEST['start_page']); //Store URL to return to this page from next step pages

			die;
			break;

			//---------------------------------------------------------------------
		case 'ResetProcess':   //Unlink the process from the document
			//if(!$check_flood) die;
			check_ticket( 'container_document_instance' , 1 );

			if(empty($_REQUEST['document_id'])){
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item');
			}
			$odocument =& $Manager->initDoc($_REQUEST['document_id']);
			require_once('class/workflow/docflow.php');
			$docflow = new docflow($odocument);
			if( $docflow->resetProcess() )
			$Manager->error_stack->push(ERROR, 'Info', array('element'=>$odocument->GetNumber() ), 'process of document %element% is reset');

			break;

			//---------------------------------------------------------------------
		case 'MoveDocument':   //Change stateMove
			//if(!$check_flood) die;
			if (empty($_REQUEST["checked"])) {
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item' );
				$Manager->error_stack->checkErrors(array('close_button'=>true));
				die;
			}

			check_ticket( 'container_document_move' , $area_id  );

			require_once('./class/common/document.php');
			$odocument = new document($space);

			//Get list of containers
			$p=array(
          'sort_field' => $Manager->SPACE_NAME . '_number',
          'sort_order' => 'ASC',
          'select' => array($Manager->SPACE_NAME . '_number', $Manager->SPACE_NAME . '_id')
			);
			$target_container_list = $Manager->GetAll($p);

			require_once "GUI/Ranchbe_HTML_QuickForm.php"; //Librairy to easily create forms
			$form =& new Ranchbe_HTML_QuickForm('frmTest', 'POST');

			foreach ($_REQUEST["checked"] as $document_id) {
				$form->addElement('hidden', 'checked[]', $document_id);
				$form->addElement('hidden', 'page_id', 'DocManage_movedoc');
				$document_number[$document_id] = $odocument->GetNumber($document_id);
				$form->addElement('hidden', 'document_number[]', $document_number[$document_id]);
			}

			$form->addElement('header', null, '<i>'.tra('Move document').' :<br /></i>'.implode(',<br /> ', $document_number).'<i><br /> '.tra('from').' :</i><br />'.$_REQUEST['SelectedContainerNum'].'<br /><i> '.tra('to').' :</i>' );

			foreach ($target_container_list as $container){
				$selectSet[$container[$Manager->SPACE_NAME . '_id']] = $container[$Manager->SPACE_NAME . '_number'];
			}
			$select =& $form->addElement('select', 'target_container_id', tra('Container'), $selectSet);
			$select->setSize(5); $select->setMultiple(false);
			$form->addRule('target_container_id', tra('Required'), 'required', null, 'client');

			$form->addElement('hidden', 'action', 'MoveDocument');

			if (isset($_REQUEST['validate'])){ //Validate the form
				if ($form->validate()){ //Validate the form
					$form->freeze(); //and freeze it
					foreach ($_REQUEST["checked"] as $document_id) {
						//TODO: Check user right on target container
						/*$tproject_id = $Manager->GetFatherProject($_REQUEST['target_container_id']);
						 require_once('./class/project.php');
						 $oProject = new project;
						 $tarea_id = $oProject->GetProjectArea($tproject_id);
						 check_ticket( 'container_document_manage' , $tarea_id );*/

						//Update the document informations and move the associated files
						$odocument->init($document_id);
						$odocument->MoveDocument($_REQUEST['target_container_id']);
					}
				}
			}else{
				$form->addElement('submit', 'validate', 'GO');
				$form->applyFilter('__ALL__', 'trim');
			}

			$form->display();
			$Manager->error_stack->checkErrors(array('close_button'=>true));
			die;
			break; //End move

		//---------------------------------------------------------------------
		case 'CopyDocument':   //Copy
			//if(!$check_flood) break;
			if (count($_REQUEST["checked"]) != 1) {
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least and only one item');
				$Manager->error_stack->checkErrors(array('close_button'=>true));
				die;
			}

			$document_id=$_REQUEST["checked"][0];

			check_ticket( 'container_document_copy'  , $area_id );

			//Get list of containers
			$p=array(
		          'sort_field' => $Manager->SPACE_NAME . '_number',
		          'sort_order' => 'ASC',
		          'select' => array($Manager->SPACE_NAME . '_number', $Manager->SPACE_NAME . '_id')
			);
			$target_container_list = $Manager->GetAll($p);

			require_once "GUI/Ranchbe_HTML_QuickForm.php"; //Librairy to easily create forms
			$form =& new Ranchbe_HTML_QuickForm('frmTest', 'POST');

			$form->setDefaults(array(
							      'target_container_id'      => $Manager->GetId(),
								));

			$odocument =& $Manager->initDoc($document_id);
			$document_number = $odocument->GetNumber();
			$form->addElement('hidden', 'checked[]', $document_id);
			$form->addElement('header', null, tra('Copy document').' '.$document_number.'<br />');

			$form->addElement('text', 'newName', tra('New name'));
			$form->addRule('newName', tra('Required'), 'required', null, 'client');

			foreach ($target_container_list as $container){
				$selectSet[$container[$Manager->SPACE_NAME . '_id']] = $container[$Manager->SPACE_NAME . '_number'];
			}
			$select =& $form->addElement('select', 'target_container_id', tra('Target container'), $selectSet);
			$select->setSize(1);
			$select->setMultiple(false);
			$form->addRule('target_container_id', tra('Required'), 'required', null, 'client');

			//Get list of indice
			$indice_list = $odocument->GetIndices();
			$select =& $form->addElement('select', 'indice_id', tra('Indice').': ', $indice_list);
			$select->setSize(1);
			$select->setMultiple(false);
			$form->addRule('indice_id', tra('Required'), 'required', null, 'server');

			$form->addElement('hidden', 'action', 'CopyDocument');
			$form->addElement('hidden', 'page_id', 'DocManage_copydoc');

			if ($form->validate()){ //Validate the form...
				$form->freeze(); //...and freeze it
				//TODO: Check user right on target container
				/*$tproject_id = $Manager->GetFatherProject($_REQUEST['target_container_id']);
				 require_once('./class/project.php');
				 $ProjectManager = new project($Manager->space, $tproject_id);
				 $tarea_id = $ProjectManager->GetProjectArea($tproject_id);
				 check_ticket('container_document_manage' , $tarea_id);*/

				$odocument->CopyDocument($_REQUEST['target_container_id'] , $_REQUEST['newName'] , $_REQUEST['indice_id']);

			}else{
				$form->addElement('submit', null, 'GO');
				$form->applyFilter('__ALL__', 'trim');
			}

			$form->display();
			$Manager->error_stack->checkErrors(array('close_button'=>true));

			die;
			break; //End copy

		//---------------------------------------------------------------------
		case 'Archive':   //Archive
			if(!$check_flood) break;
			if (empty($_REQUEST['checked'])){
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item' );
				break;
			}
			
			check_ticket( 'container_document_suppress' , $area_id );
			
			require_once('class/Document/Archiver.php');
			
			if($_REQUEST['validate_archive']){
				foreach($_REQUEST['checked'] as $document_id){
					$Document =& $Manager->initDoc($document_id);
					
					if( $Document->GetDocProperty('document_access_code') < 100){
						$Archiver = new Document_Archiver( $Document );
						$Archiver->archive();
					}
				}
				
			}else{ //Display validation
				$Document =& $Manager->initDoc(0);
				foreach ($_REQUEST['checked'] as $document_id){
					$list[] = $Document->GetDocumentInfos($document_id);
				}
				$smarty->assign_by_ref('list' , $list);
				$smarty->assign('documentManage' , 'active');
				$smarty->assign('confirmation_text' , 'Are you sure that you want archive this documents');
				$smarty->assign('confirmation_return' , 'validate_archive');
				$smarty->assign('confirmation_formaction' , 'DocManage.php?docfileManage=0&space='.$Document->space->SPACE_NAME);
				$smarty->assign('confirmation_action' , $_REQUEST['action']);
				$smarty->assign('mid', 'confirm_document_action.tpl');
				$smarty->display('ranchbe.tpl');
				die;
			}
			break; //Archive
			
		//---------------------------------------------------------------------
		case 'multi_ChangeIndice':   //Upgrade indice of document
			check_ticket( 'container_document_change_indice'  , $area_id );
			if(!$check_flood) break;
			if (empty($_REQUEST['checked'])){
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item');
				break;
			}
			require_once('./class/common/document.php');
			$odocument = new document($space);
			foreach($_REQUEST['checked'] as $document_id){
				$odocument->init($document_id);
				$access = $odocument->CheckAccess();
				if($access == 0){
					check_ticket('container_document_manage' , $area_id);
					$odocument->LockDocument(11);
					//Lock access to files too
					$docfiles = $odocument->GetDocfiles();
					if(is_array($docfiles))
					foreach($docfiles as $docfile){
						$docfile->LockFile(11);
					}
					$odocument->history['action_name'] = 'Lock';
					$odocument->WriteHistory();
					$odocument->error_stack->push(ERROR, 'Info', array('element'=>$odocument->GetNumber() ), tra("the previous document %element% is locked"));
				}
				$odocument->UpgradeIndice();
			}
			unset($_REQUEST); //to dont interfere filters
			break; //End

			//TODO : add automatic locking for ChangeIndice
			//TODO : merge actions ChangeIndice and multi_ChangeIndice
			//---------------------------------------------------------------------
		case 'ChangeIndice':   //Upgrade indice of document
			check_ticket( 'container_document_change_indice'  , $area_id );
			if (empty($_REQUEST["checked"])){
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item');
				$Manager->error_stack->checkErrors(array('close_button'=>true));
				die;
			}
			
			require_once "HTML/QuickForm.php"; //Librairy to easily create forms
			$document_id = $_REQUEST["checked"][0];
			
			//Construct the form with QuickForm lib
			$form = new HTML_QuickForm('form', 'post');
			
			//Add hidden fields
			$form->addElement('hidden', 'checked[]', $document_id);
			
			//Add form elements
			$form->addElement('header', 'infos', "Change indice");
			
			//Get infos about document
			$odocument =& $Manager->initDoc($document_id);
			$Documentinfos = $odocument->GetDocumentInfos();
			$smarty->assign('document_number' , $Documentinfos['document_number']);
			
			//Get list of indices
			$indices = $odocument->GetIndices($Documentinfos['document_indice_id']);
			
			$select =& $form->addElement('select', 'indice', 'Indice' , $indices);
			$select->setSize(1);
			$select->setMultiple(false);
			
			//Add submit button
			$form->addElement('reset', 'reset', 'reset');
			$form->addElement('submit', 'action', 'ChangeIndice');
			
			// applies new filters to the element values
			$form->applyFilter('__ALL__', 'trim');
			$form->addRule('indice', 'indice is required', 'required');
			
			// Try to validate the form
			if ( $form->validate()) {
				$form->freeze(); //and freeze it
				// Form is validated, then processes the create request
				$form->process('ChangeIndice', true);
			}
			
			//Set the renderer for display QuickForm form in a smarty template
			include 'QuickFormRendererSet.php';
			
			$space->error_stack->checkErrors();
			$smarty->display('header.tpl');
			$smarty->display('indice.tpl');
			
			die;
			break; //End modify

			//---------------------------------------------------------------------

	} //End of switch between action
} //End of not empty action condition

require_once('./class/common/metadata.php');
$metadata = new docmetadata($Manager->space);
$optionalFields = $metadata->GetMetadataLinked( NULL, $Manager->GetId() );
if(is_array($optionalFields)){
	$smarty->assign('optionalFields',$optionalFields);
	foreach($optionalFields as $val)
	$all_field[0][$val['field_name']] = $val['field_description'];
	$smarty->assign('all_field', $all_field[0]);
}

if ($_REQUEST['action'] == 'downloadListing' ){ //Download a listing of the container
	//Get list of scripts
	require_once "GUI/Ranchbe_HTML_QuickForm.php"; //Librairy to easily create forms
	$form =& new Ranchbe_HTML_QuickForm('frmTest', 'POST');
	//require_once "HTML/QuickForm.php"; //Librairy to easily create forms
	//$form = new HTML_QuickForm('form', 'post');
	$form->setDefaults(array(
    'result_format'      => 'xls',
	));
	//Select result format (xls or csv)
	$form->addElement('radio', 'result_format', 'csv', '', 'csv');
	$form->addElement('radio', 'result_format', 'excel', '', 'xls');
	//Select xls render
	$path_queries_scripts = 'inc/excel_render/documentManager/';
	$scriptfile = glob($path_queries_scripts . '*.php');
	$scriptfile=array_combine($scriptfile,$scriptfile);
	$select =& $form->addElement('select', 'xls_render', tra('Render'), $scriptfile);
	$select->setSize(1);
	$select->setMultiple(false);
	//Submit
	$form->addElement('submit', 'action', 'downloadListing');
	$form->addElement('submit', 'cancel', 'Cancel');
	// Tries to validate the form
	if ($form->validate()) {
		if($_REQUEST['result_format'] === 'csv'){
			//get all documents
			$list = $Manager->GetAllDocuments($params, true);
			$smarty->assign_by_ref('list', $list);
			header("Content-type: application/csv ");
			header("Content-Disposition: attachment; filename=".$Manager->GetName().'_content.csv');
			$smarty->display("./csv_templates/container_listing_download.tpl");
			die;
		}else if ($_REQUEST['result_format'] === 'xls'){
			require_once($_REQUEST['xls_render']);
			excel_render($Manager);
			die;
		}
	}
	$form->display();
	die;
}


//http://www.oooforum.org/forum/viewtopic.phtml?t=68902
//http://www.indesko.com/technologies/uno/
//http://www.nexen.net/actualites/php/17301-puno_:_interface_openoffice_pour_php.php
//http://puno.sourceforge.net/example01.html
//http://download.openoffice.org/2.4.0/sdk.html
//http://puno.sourceforge.net/docs.html

/*
 /////////////////////////////////////////////////////////////////////////////////
 //DEFINE FUNCTION TO COMPATIBILITY BETWENN UNO(linux) AND COM(windows)
 require_once('lib/puno.inc');
 /////////////////////////////////////////////////////////////////////////////////
 $doc_type = 'swriter';
 $x_component_loader=get_remote_xcomponent("uno:socket,host=localhost,port=8100;urp;StarOffice.ServiceManager","com.sun.star.frame.Desktop");
 die;
 $load_url="private:factory/".$doc_type;
 $load_props=array();
 $x_component=$x_component_loader->loadComponentFromURL($load_url,"_blank",0,$load_props);
 die;

 $data=date("r");

 echo " <BR>Started at: $data<BR>";
 flush(NULL);

 $x_text_document=newDocComponent("swriter");
 $x_text=$x_text_document->getText();
 // simply set whole text as one string
 $x_text->setString("He lay flat on the brown, pine-needled flor of the forest, "
 ."his chin on his folden arms, and high overhead the wind blew in the tops "
 ."of the pine trees.");

 // create text cursor for selecting and formatting
 $x_text_cursor=$x_text->createTextCursor();

 // use cursor to select "He lay" and apply bold italic
 $x_text_cursor->gotoStart(false);

 $x_text_cursor->goRight(06,true);
 $x_text_cursor->setPropertyValue("CharWeight",FontWeight_BOLD);
 // add more text at the end of the text using insertString
 $x_text_cursor->gotoEnd(false);
 $x_text->insertString($x_text_cursor," The mountainside sloped gently where he lay; "
 ."but below it was steep and he could see the dark of the oiled road "
 ."winding through the pass. There was a stream alongside the road "
 ."and far down the pass he saw a mill beside the stream and the falling water "
 ."of the dam, white in the summer sunlight.",false);

 // after insertString the cursor is behind the inserted text, insert more text
 $x_text->insertString($x_text_cursor,"\n  \"Is that the mill?\" he asked." , false);

 $store_props=array();
 $store_props[0]=create_struct("com.sun.star.beans.PropertyValue");
 $store_props[0]->Name="FilterName";
 $store_props[0]->Value = "writer_pdf_Export";

 $x_text_document->storeToURl("file:///tmp/example01.pdf",$store_props);
 $data=date("r");
 $x_text_document->dispose();
 echo " <BR>Finished at: $data<BR>";

 function newDocComponent($doc_type)
 {

 $x_component_loader=get_remote_xcomponent("uno:socket,host=localhost,port=8100;urp;StarOffice.ServiceManager","com.sun.star.frame.Desktop");

 $load_url="private:factory/".$doc_type;
 $load_props=array();
 $x_component=$x_component_loader->loadComponentFromURL($load_url,"_blank",0,$load_props);


 return $x_component;
 }
 */

/*
 //$cmd = '"c:\Program Files\OpenOffice.org 2.4\program\soffice" -invisible "macro:///Standard.Module1.ConvertWordToPDF(C:\tmp\docconverter\test.doc)"';
 //$cmd = 'C:\tmp\docconverter\convert.bat)';
 $cmd = 'dir C:\tmp\docconverter\)';
 @system($cmd, $result);
 var_dump($result);
 die;
 echo "<br/><pre>$lastLine</pre>\n";
 die;
 function MakePropertyValue($name,$value,$osm){
 $oStruct = $osm->Bridge_GetStruct("com.sun.star.beans.PropertyValue");
 $oStruct->Name = $name;
 $oStruct->Value = $value;
 return $oStruct;
 }

 function word2pdf($doc_url, $output_url){
 //Invoke the OpenOffice.org service manager
 $osm = new COM("com.sun.star.ServiceManager") or die ("Please be sure that OpenOffice.org is installed.\n");
 return false;
 //Set the application to remain hidden to avoid flashing the document onscreen
 $args = array(MakePropertyValue("Hidden",true,$osm));
 var_dump($args);
 return false;
 //Launch the desktop
 $oDesktop = $osm->createInstance("com.sun.star.frame.Desktop");
 //Load the .doc file, and pass in the "Hidden" property from above
 $oWriterDoc = $oDesktop->loadComponentFromURL($doc_url,"_blank", 0, $args);
 //Set up the arguments for the PDF output
 $export_args = array(MakePropertyValue("FilterName","writer_pdf_Export",$osm));
 //Write out the PDF
 $oWriterDoc->storeToURL($output_url,$export_args);
 $oWriterDoc->close(true);
 }
 */


if ($_REQUEST['action'] == 'convert' ){ //Download a listing of the container
	require_once "HTML/QuickForm.php"; //Librairy to easily create forms
	//require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm
	$form = new HTML_QuickForm('form', 'post');
	$renderer =& $form->defaultRenderer();
	$form->setDefaults(array(
    'convert_format'      => 'pdf',
	));
	//Select output format
	$format = array('ps'=>'ps', 'pdf'=>'pdf');
	$select =& $form->addElement('select', 'convert_format', tra('output format'), $format);
	$select->setSize(1);
	$select->setMultiple(false);
	//Submit
	$form->addElement('submit', 'action', 'convert');
	$form->addElement('submit', 'cancel', 'Cancel');
	// Tries to validate the form
	if ($form->validate()) {
		if($_REQUEST['convert_format'] === 'pdf'){
			$output_dir = "c:/tmp/docconverter/";
			$doc_file = "c:/tmp/docconverter/test.doc";
			$pdf_file = "test1.pdf";
			$output_file = $output_dir . $pdf_file;
			$doc_file = "file:///" . $doc_file;
			$output_file = "file:///" . $output_file;
			$logger->log( 'documentManage.php::convert docfile: '.$doc_file.' output file: '.$output_file );
			//word2pdf($doc_file,$output_file);
		}else if ($_REQUEST['convert_format'] === 'ps'){
		}
	}
	$form->display();
	die;
}

//Select table where select field (field=>table)
$all_field['with']['category_number']['table'] = $Manager->SPACE_NAME.'_categories';
$all_field['with']['category_number']['col'] = 'category_id';
$all_field['with']['indice_value']['table'] = 'document_indice';
$all_field['with']['indice_value']['col'] = 'document_indice_id';
$all_field['with']['doctype_number']['table'] = 'doctypes';
$all_field['with']['doctype_number']['col'] = 'doctype_id';

//Include generic definition of the code for manage filters
$default_sort_field='document_number';    //Default value of the field to sort
$default_sort_order='ASC';    //Default value of the order ASC = ascendant, DESC = descandant
include('filterManager.php');

//Set the option for advanced search of the searchBar
$date_elements = array(
              	'check_out_date',
              	'update_date',
              	'open_date',
);
$smarty->assign_by_ref('date_elements', $date_elements);

//Set the option for advanced search of the searchBar
$user_elements = array(
                  	'check_out_by',
                  	'update_by',
                  	'open_by',
);
$smarty->assign_by_ref('user_elements', $user_elements);

//Get list of users for generate user select in advanced serachBar
global $LUA;
$LUparams = array ( 'container' => 'auth',
                  'orders' => array('handle' => 'ASC'),
                  'limit' => 9999,
                  'offset' => 0,
                  'fields' => array('handle','auth_user_id')
);
$smarty->assign_by_ref('user_list', $LUA->getUsers($LUparams));

//Get list of users for generate category select
/*
 require_once('./class/category.php');
 $ocategorie = new category($space);
 $categories = $ocategorie->GetCategories(array('sort_field'=>'category_number'));
 foreach ($categories as $value ) //Rewrite result array for quickform convenance
 {$selectedCat[$value['category_id']] = $value['category_number'];}
 $smarty->assign_by_ref('category_list', $selectedCat);
 */
$ocategory =& $Manager->initCategory();
//$categories = $ocategory->GetLinks(array('sort_field'=>'category_number'));
$categories = $ocategory->GetCategories(array('sort_field'=>'category_number'));

//Rewrite result array for quickform convenance
foreach ($categories as $value ){
	$selectedCat[$value['category_id']] = $value['category_number'];
}

$smarty->assign_by_ref('category_list', $selectedCat);

//get all documents
$list = $Manager->GetAllDocuments($params, $displayHistory);
$smarty->assign_by_ref('list', $list);

//Include generic definition of the code for manage the pagination. $list must set...
include('paginationManager.php');

$all_field[0] = array ('designation' => 'Designation',
//'document_number' => 'Number',
//'category_number' => 'Category',
//'document_access_code' => 'Access',
//'document_state' => 'State',
                      'indice_value' => 'Indice',
                      'document_version' => 'Version',
//'doctype_number' => 'Type',
);

$smarty->assign('icons_dir', DEFAULT_DOCTYPES_ICONS_DIR);
$smarty->assign('deposit_dir', $Manager->GetProperty('default_file_path'));
$smarty->assign('thumbs_dir', $Manager->DEFAULT_DEPOSIT_DIR.'/__attachments/_thumbs');
$smarty->assign('thumbs_extension', '.png');

$Manager->error_stack->checkErrors();

//Show or hide the docfile manager tab
if($_REQUEST['HideDocfileManage']){
	$_SESSION['DisplayDocfileTab'] = false; //reset session var to hide the file manager tab. See tabs.tpl and mySpaceTabs.tpl
}

require_once('./class/common/document.php');
$smarty->register_object( 'odocument' , new document($space)); //register object to use in smarty plugins

// Display the template
$_SESSION['myspace'] = array('activePage'=>$_SERVER['PHP_SELF'].'?docfileManage=0');  //to remember the activated tab
$smarty->assign('documentManage' , 'active');
$smarty->assign('mid', 'documentManageAlt1.tpl');
$smarty->display('ranchbe.tpl');
