<?php

switch ( $_REQUEST['action'] ) {

//---------------------------------------------------------------------
  case 'putInWs':   //Put file in wildspace
    if (empty($_REQUEST['checked'])){
      $Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select ar least one item');
    }else{
      require_once('./class/common/document.php');
      $odocument = new document($space);
      foreach($_REQUEST['checked'] as $document_id){
        $odocument->init($document_id);
        $odocument->PutInWildspace();
      }
    }
    break;
} //End of switch between action


//Select table where select field (field=>table)
$all_field['with']['category_number']['table'] = $Manager->SPACE_NAME.'_categories';
$all_field['with']['category_number']['col'] = 'category_id';
$all_field['with']['indice_value']['table'] = 'document_indice';
$all_field['with']['indice_value']['col'] = 'document_indice_id';
$all_field['with']['doctype_number']['table'] = 'doctypes';
$all_field['with']['doctype_number']['col'] = 'doctype_id';

//Include generic definition of the code for manage filters
$default_sort_field='document_number';    //Default value of the field to sort
$default_sort_order='ASC';    //Default value of the order ASC = ascendant, DESC = descandant
include('filterManager.php');

$sameurl_elements[] ='container_id'; //to re-send value in url
$sameurl_elements[] ='space'; //to re-send value in url

//Set the option for advanced search of the searchBar
$date_elements = array(
              	'check_out_date',
              	'update_date',
              	'open_date',
                );
$smarty->assign_by_ref('date_elements', $date_elements);

//Set the option for advanced search of the searchBar
$user_elements = array(
              	'check_out_by',
              	'update_by',
              	'open_by',
                );
$smarty->assign_by_ref('user_elements', $user_elements);

//Get list of users for generate user select in advanced serachBar
global $LUA;
$LUparams = array ( 'container' => 'auth',
                  'orders' => array('handle' => 'ASC'),
                  'limit' => 9999,
                  'offset' => 0,
                  'fields' => array('handle','auth_user_id')
                  );
$smarty->assign_by_ref('user_list', $LUA->getUsers($LUparams));

//Get list of users for generate category select
  require_once('./class/category.php');
  $ocategorie = new category($space);
  $categories = $ocategorie->GetCategories(array('sort_field'=>'category_number'));
    foreach ($categories as $value ) //Rewrite result array for quickform convenance
      {$selectedCat[$value['category_id']] = $value['category_number'];}
  $smarty->assign_by_ref('category_list', $selectedCat);

//get all documents
$list = $Manager->GetAllDocuments($params, $displayHistory);
$smarty->assign_by_ref('list', $list);

//Include generic definition of the code for manage the pagination. $list must set...
include('paginationManager.php');

$all_field[0] = array ('designation' => tra('designation'),
                      'indice_value' => tra('indice_value'),
                      'document_version' => tra('document_version'),
                      );



?>
