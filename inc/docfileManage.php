<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//this script may only be included - so its better to die if called directly.
if (strpos($_SERVER["SCRIPT_NAME"],basename(__FILE__)) !== false){
  header("location: index.php");
  exit;
}

if(!isset($Manager)) {print '$Manager is not set '; die;}

$area_id = $Manager->AREA_ID;

$_SESSION['DisplayDocfileTab'] = true; //to display the file manager tab in wildspace

//Manage actions on documents
if (!empty($_REQUEST["action"])) {
$smarty->assign('action', $_REQUEST["action"]);

switch ( $_REQUEST['action'] ) {
//---------------------------------------------------------------------

  case ('viewFile'):  //View file
    if(empty($_REQUEST['checked'][0])) return false;
    require_once('./class/common/attachment.php');
    $recordfile =& $Manager->initDocfile($_REQUEST['checked'][0]); //Get the recordfile from the container object
    $fsdata =& $recordfile->initFsdata(); //Get the fsdata from the record file
    $viewer = new viewer(); //Create a new viewer
    $viewer->initFsdata($fsdata); //link the fsdata to the viewer and set property of the viewer
    $viewer->pushfile(); //Get the viewable file
  break;

} //End of switch between action
} //End of not empty action condition

//Include generic definition of the code for manage filters
$default_sort_field='file_name';    //Default value of the field to sort
$default_sort_order='ASC';    //Default value of the order ASC = ascendant, DESC = descandant
include('filterManager_simple.php');

//get all files
require_once './class/common/docfile.php';
$space_name =& $space->SPACE_NAME;
$docfile = new docfile($space);
$params['with'] = array(table=>$space_name.'_documents' , col=>'document_id');
$params['select']=array($space_name.'_documents.document_number',
                        $space_name.'_documents.document_state',
                        $space_name.'_documents.document_version',
                        //'MAX('.$space_name.'_documents.document_indice_id) AS document_indice_id',
                        $space_name.'_documents.document_indice_id',
                        $space_name.'_doc_files.*');
$params['exact_find'][$Manager->SPACE_NAME . '_id'] = $Manager->GetId();
//$params['extra'] = 'GROUP BY '.$space_name.'_documents.document_number';
$list = $docfile->GetFiles($params);
//var_dump($list);
$smarty->assign_by_ref('list', $list);

//Include generic definition of the code for manage the pagination. $list must set...
include('paginationManager.php');

$all_field = array ('file_name' => 'Name',
                    'file_extension' => 'Extensions',
                    'import_order' => 'Import order',
                    'file_path' => 'Path',
                    'file_open_by' => 'Created by' ,
                    'file_open_date' => 'Created date',
                    'file_update_by' => 'Last update by',
                    'file_update_date' => 'Last update date',
                    'file_access_code' => 'Access',
                    'file_state' => 'State',
                    'file_version' => 'Version',
                    'file_type' => 'Type',
                    'file_size' => 'Size',
                    );
$smarty->assign('all_field', $all_field);

//Assign additional var to add to URL when redisplay the same url
$sameurl_elements[]='action';
$sameurl_elements[]=$Manager->SPACE_NAME . '_id';
$sameurl_elements[]='docfileManage';

//Assign name to particular fields
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number' );
$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description' );
$smarty->assign('CONTAINER_STATE' , $Manager->SPACE_NAME . '_state' );
$smarty->assign('CONTAINER_ID' , $Manager->SPACE_NAME . '_id' );
$smarty->assign('file_icons_dir', DEFAULT_FILE_ICONS_DIR);

$space->error_stack->checkErrors();

// Display the template
$_SESSION['myspace'] = array('activePage'=>$_SERVER['PHP_SELF'].'?docfileManage=1'); //to remember the activated tab
$smarty->assign('docfileManage' , 'active');
$smarty->assign('mid', 'docfileManage.tpl');
$smarty->display('ranchbe.tpl');
?>
