<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//this script may only be included - so its better to die if called directly.
if (strpos($_SERVER["SCRIPT_NAME"],basename(__FILE__)) !== false) {
  header("location: index.php");
  exit;
}
if(!isset($Manager)){print '$Manager is not set '; die;}

$area_id = $Manager->AREA_ID;

if( !empty( $_REQUEST['document_id'] ) ){ //If a document is selected, display only history of this doc...
  $document_id = $_REQUEST['document_id'];
  $odocument = $Manager->initDoc( $_REQUEST['document_id'] );
}else
  $odocument = $Manager->initDoc();

$ohistory = new history($odocument);

require_once "HTML/QuickForm.php"; //Librairy to easily create forms
require_once './lib/Date/date.php';

//$smarty->assign('container_map_id', $Manager->FIELDS_MAP_ID);

//Manage actions on history

if (!isset($_REQUEST['action'])) $_REQUEST['action'] = '';

switch ( $_REQUEST['action'] ) {

//---------------------------------------------------------------------
  case 'suppress':   //Process suppress request
  if(!empty($_REQUEST["histo_order"])){
    check_ticket('container_document_history' , $area_id);
    foreach ( $_REQUEST['histo_order'] as $id)
      $ohistory->RemoveHistorySingle($id);
  }else return false;
  $_REQUEST['action'] = '';
  break;

//---------------------------------------------------------------------
  case 'getComment':   //Get the comment
  if (! empty($_REQUEST["activity_id"]) &&  ! empty($_REQUEST["instance_id"]) ) {
    include_once ('lib/Galaxia/API.php');

    $instance->instanceId = $_REQUEST["instance_id"];
    $comments = $instance->get_instance_comments($_REQUEST["activity_id"]);
    $smarty->assign('comments',$comments);

    $_REQUEST['__user']     = $comments[0]['user'];
    $_REQUEST['__title']    = $comments[0]['title'];
    $_REQUEST['__comment']  = $comments[0]['comment'];
    $_REQUEST['__timestamp'] = $comments[0]['timestamp'];
    
    include 'galaxia_view_comment.php';
    die;

  }else return false;
  break;

//---------------------------------------------------------------------

  default:
    //echo 'no action';
  break;

} //End of switch

//Select table where select field (field=>table)
$all_field['with']['category_number']['table'] = $Manager->SPACE_NAME.'_categories';
$all_field['with']['category_number']['col'] = 'categorie_id';
$all_field['with']['indice_value']['table'] = 'document_indice';
$all_field['with']['indice_value']['col'] = 'document_indice_id';
$all_field['with']['doctype_number']['table'] = 'doctypes';
$all_field['with']['doctype_number']['col'] = 'doctype_id';

//Include generic definition of the code for manage filters
//$default_sort_field = ''; //Default value of the field to sort
//$default_sort_order='ASC'; //Default value of the order ASC = ascendant, DESC = descandant
include('filterManager_simple.php');

$sameurl_elements[] = 'document_id';

if(!empty($document_id)){ //If a document is selected, display only history of this doc...
  $list = $ohistory->GetHistory($document_id , $params);
  $smarty->assign('document_id', $document_id );
}else //Display history of all doc
  $list = $ohistory->GetAllHistory($params);

$smarty->assign_by_ref( 'list', $list );

//Include generic definition of the code for manage the pagination. $list must set...
include('paginationManager.php');

//Define select option for "find"
$all_field = array ('action_name' => 'action_name',
                    'document_number' => 'Number',
                    'designation' => 'Designation',
                    'document_access_code' => 'Access',
                    'document_state' => 'State',
                    'indice_value' => 'Indice',
                    'document_version' => 'Version',
                    'doctype_number' => 'Type',
                    );
$smarty->assign('all_field', $all_field);

//Assign name to particular fields
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number' );
$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description');
$smarty->assign('CONTAINER_STATE' , $Manager->SPACE_NAME . '_state' );
$smarty->assign('CONTAINER_ID' , $Manager->SPACE_NAME . '_id' );

$Manager->error_stack->checkErrors();

// Active the tab
// Display the template
$smarty->display("documentHistoryAlt1.tpl");

?>
