<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Olivier CYSSAU                                          |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  RanchBE is distributed in the hope that it will be useful,                |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//this script may only be included - so its better to die if called directly.
if (strpos($_SERVER["SCRIPT_NAME"],basename(__FILE__)) !== false) {
	header("location: index.php");
	exit;
}

if ( ! isset($Manager) ) {print '$Manager is not set '; die;}

//Assign name to particular fields
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number');
$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description');
$smarty->assign('CONTAINER_STATE' , $Manager->SPACE_NAME . '_state');
$smarty->assign('CONTAINER_ID' , $Manager->SPACE_NAME . '_id');

$area_id = $Manager->AREA_ID;
//var_dump($area_id);

check_ticket( 'container_get' , $area_id );

$check_flood = check_flood();

switch($_REQUEST['action']){

	case 'suppress':
		if(!$check_flood) break;
		if (empty($_REQUEST['checked'])){
			$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item' );
			break;
		}
		
		check_ticket('container_suppress' , $area_id);
		
		foreach ($_REQUEST['checked'] as $del_id){
			$Manager->init($del_id);
			$Manager->Suppress();
		}
		break;
		
	case 'rename':
		//if(!$check_flood) break;
		if (empty($_REQUEST['checked'])){
			$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item' );
			break;
		}
		check_ticket( 'container_document_suppress' , $area_id );
		
		$container_id = $_REQUEST['checked'][0];
		$target_name = $_REQUEST['newName'];
		$Manager->init($container_id);
		
		require_once "GUI/Ranchbe_HTML_QuickForm.php"; //Librairy to easily create forms
		$form =& new Ranchbe_HTML_QuickForm('frmTest', 'POST');
		$form->addElement('hidden', 'checked[]', $container_id);
		$form->addElement('hidden', 'action', 'rename');
		$form->addElement('header', null, tra('Rename container ').' '.$Manager->getProperty('container_number'));
		$form->addElement('text', 'newName', tra('New name'));
		$form->addRule('newName', tra('Required'), 'required', null, 'client');
	    $mask = constant('DEFAULT_'. strtoupper($Manager->SPACE_NAME) . '_MASK');
	    if($mask){
		    $form->addRule('newName', 'This number is not valid', 'regex', "/$mask/" , 'server');
	    }
		
		if ($form->validate()){ //Validate the form...
			$form->freeze(); //...and freeze it
			$target_name = str_replace('/', '_', $target_name);
			$target_name = str_replace('\\', '_', $target_name);
			$target_name = str_replace('.', '_', $target_name);
			$toPath = $Manager->DEFAULT_DEPOSIT_DIR .'/'. $target_name;
			$ok = $Manager->move($toPath);
			if( $ok ){
				if( strstr($Manager->GetProperty('default_file_path'),  '__imported' ) ){
					$toPath = $toPath . '/__imported';
				}
				$data = array($Manager->GetFieldName('number')=>$target_name, 'default_file_path'=>$toPath);
				$Manager->BasicUpdate( $data, $Manager->GetId() );
			}
			else{
				$Manager->error_stack->push(ERROR, 'Fatal', array(), 'Error during move' );
			}
		}else{
			$form->addElement('submit', null, 'GO');
			$form->applyFilter('__ALL__', 'trim');
		}
		
		$form->display();
		$Manager->error_stack->checkErrors(array('close_button'=>true));
		die;
		break;
		
	case 'getStats':
		include './inc/containerStats.php';
		die;
		break;
		
	case 'Archive':
		if(!$check_flood) break;
		if (empty($_REQUEST['checked'])){
			$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item' );
			break;
		}
		
		check_ticket( 'container_document_suppress' , $area_id );
		
		require_once('class/Document/Archiver.php');
		require_once('class/Container/Archiver.php');
		
		if($_REQUEST['validate_archive']){
			foreach($_REQUEST['checked'] as $container_id){
				$Manager->init($container_id);
				$Archiver = new Container_Archiver( $Manager );
				$Archiver->archive();
			}
		}else{ //Display validation
			foreach ($_REQUEST['checked'] as $container_id){
				$list[] = $Manager->GetInfos($container_id);
			}
			$smarty->assign_by_ref('list' , $list);
			$smarty->assign('documentManage' , 'active');
			$smarty->assign('confirmation_text' , 'Are you sure that you want archive this containers');
			$smarty->assign('confirmation_return' , 'validate_archive');
			$smarty->assign('confirmation_formaction' , $Manager->SPACE_NAME . 's.php');
			$smarty->assign('confirmation_action' , $_REQUEST['action']);
			$smarty->assign('mid', 'confirm_container_action.tpl');
			$smarty->display('ranchbe.tpl');
			die;
		}
		break;
		
}

//Select table where select field (field=>table)
$all_field['with']['project_number']['table'] = 'projects';
$all_field['with']['project_number']['col'] = 'project_id';

//Include generic definition of the code for manage filters
$default_sort_field = $Manager->SPACE_NAME . '_number'; //Default value of the field to sort
$default_sort_order='ASC'; //Default value of the order ASC = ascendant, DESC = descandant

//include('filterManager_simple.php');
include('filterManager.php');

//Set the option for advanced search of the searchBar
$date_elements = array(
              	'open_date',
              	'close_date',
              	'forseen_close_date',
);
$smarty->assign_by_ref('date_elements', $date_elements);

//Set the option for advanced search of the searchBar
$user_elements = array(
              	'open_by',
              	'close_by',
);
$smarty->assign_by_ref('user_elements', $user_elements);

//Get list of users for generate user select in advanced searchBar
global $LUA;
$LUparams = array ( 'container' => 'auth',
                    'orders' => array('handle' => 'ASC'),
                    'limit' => 9999,
                    'offset' => 0,
                    'fields' => array('handle','auth_user_id')
);
$smarty->assign_by_ref('user_list', $LUA->getUsers($LUparams));

//$params['select'] = array('container_id','container_number','container_state','container_description','container_indice_id','file_only','project_id','default_process_id','default_file_path','open_date','open_by','forseen_close_date','close_date','close_by','object_class','alias_id'); //Set select is required when union is set
$params['select'] = array(
$Manager->GetFieldName('id'),
$Manager->GetFieldName('number'),
$Manager->GetFieldName('state'),
$Manager->GetFieldName('description'),
$Manager->GetFieldName('indice'),
          'file_only',
          'project_id',
          'default_process_id',
          'default_file_path',
          'open_date','open_by',
          'forseen_close_date',
          'close_date',
          'close_by',
          'object_class',
          'alias_id'); //Set select is required when union is set


$all_field[0] = $Manager->FIELDS_MAPPING;
$optionalFields = $Manager->GetMetadata();
$smarty->assign('optionalFields',$optionalFields);
foreach($optionalFields as $val){
	$all_field[0][$val['field_name']] = $val['field_description'];
	$params['select'][] = $val['field_name'];
}
//Define select option for "find"
$smarty->assign('all_field', $all_field[0]);

require_once './class/common/containerAlias.php';
$Manager->SetUnion( new containerAlias($space) , $params['select'] ); //To get alias too

$params['numrows'] = 100;

$list_t = $Manager->GetAll($params);
if(is_array($list_t))
foreach($list_t as $element ){
	$Manager->init( $element[$Manager->GetFieldName('id')] );
	if( check_ticket( 'container_document_get' , $Manager->AREA_ID , false) ){
		$list[] = $element;
	}
}
$smarty->assign_by_ref('list', $list);
unset($list_t);

//Include generic definition of the code for manage the pagination. $list must set...
include('paginationManager.php');

$smarty->assign('PageTitle' , $Manager->SPACE_NAME . ' manager');

// Active the tab
$smarty->assign("$Manager->SPACE_NAME".'Tab' , 'active');

$Manager->error_stack->checkErrors();

// Display the template
$smarty->assign('randWindowName', 'container_'.uniqid());
$smarty->assign('mid', 'containers.tpl');
$smarty->display("ranchbe.tpl");
?>
