<?php

function stats_query_rejects_by_errorcode_by_users(){

  global $dbranchbe;
  global $_REQUEST;
  global $Manager;
  $space =& $Manager->space;

// ---------------------- DISPLAY ERRORS BY TYPE -------------------------------

//Query all reject from history table and galaxia_instance table
  $query = "SELECT document_number,action_by,started,owner,status,properties FROM $space->DOC_HISTORY_TABLE as histo
            JOIN galaxia_instances as instance
            ON histo.instance_id = instance.instanceId
            WHERE action_name = 'ChangeState'
            AND document_state = 'rejete'
            ";
  if(!empty($_REQUEST['container_id']))
    $query = $query.' AND '.$Manager->space->CONT_FIELDS_MAP_ID.' = '.$_REQUEST['container_id'];

//Execute query and get the recordset in Rset var
  if($Rset = $dbranchbe->Execute($query)){
    if($Rset->RecordCount() === 0) return false;
    $cumul_errors_by_type = array();
    while ($row = $Rset->FetchRow()) {
      $props = unserialize($row['properties']); //unserialize the properties wich are store in BLOB type field indatabase
      if(is_null($props['error_code']))
        $props['error_code'] = array('unknow');
      $users[] = $row['owner']; // Users wich have submit a workflow
      if(is_array($props['error_code'])){
        foreach($props['error_code'] as $error_code){ //pour chaque code erreur on cree une entree de tableau avec en valeur l'utilisateur qui a soumis le document a la verif.
          $cumul_errors_by_type[$error_code][] = $row['owner'];
        }
      }
    }
  }else{print 'error in query'; return false;}

  //var_dump($cumul_errors_by_type);

  $text =  "<h1>Erreurs par type et par utilisateur</h2><br />";
  $graphs[] = array('text' => $text);

  foreach($cumul_errors_by_type as $error_type=>$count_by_user){
    $Count_error[$error_type] = array_count_values($cumul_errors_by_type[$error_type]);
    $text = 'Type : "'. $error_type . '" :' . '<br />';
    $graphs[] = array('text' => $text);
    $values = array();$legend = array(); //init graph var
    foreach($Count_error[$error_type] as $user=>$val){
      //Set the text to display
      $text = $val . ' erreur type "' . $error_type . '" pour l\'utilisateur '.$user. '<br />';
      $graphs[] = array('text' => $text);
      //Display graph
      $values[] = $val;
      $legend[] = $user;
    }
    $title  = urlencode ($error_type);
    $values = urlencode(serialize($values));
    $legend = urlencode(serialize($legend));
    $graphs[] = array(
              'image' => "<img src='./inc/graphs/graphBar.php?title=$title&values=$values&legend=$legend' alt='no graph'/>",
              'text' => '');
    $text = '';
  }

  return $graphs;

}

?>
