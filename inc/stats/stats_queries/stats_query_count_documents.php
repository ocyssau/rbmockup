<?php

//Count files
/*
$query = "SELECT file_id FROM $Manager->DOC_FILE_TABLE";
$Count_Doc_File = count_query($query);
*/

function stats_query_count_documents(){

  global $dbranchbe;
  global $_REQUEST;
  global $Manager;

  //Count documents
  $query = 'SELECT document_id , doctype_id , document_indice_id, document_state, category_id 
            FROM '.$Manager->space->DOC_TABLE;
  if(!empty($_REQUEST['container_id']))
    $query = $query.' WHERE '.$Manager->space->CONT_FIELDS_MAP_ID.' = '.$_REQUEST['container_id'];
  
  $doctype=array(); $indice=array(); $state=array(); $category=array();
  
  if(!$Rset = $dbranchbe->Execute($query)){
    print 'error on query: '.$dbranchbe->ErrorMsg().'<br />'.$query;
    return false;
  }else{
    $Count_Doc = $Rset->RecordCount();
    while ($row = $Rset->FetchRow()) {
      $doctype[] = $row['doctype_id'];
      $indice[]  = $row['document_indice_id'];
      $state[]  = $row['document_state'];
      if(!is_null($row['category_id']))
        $category[] = $row['category_id'];
    }
  }

  $text =  "<li>Total of document : $Count_Doc</li>";
  $graphs[] = array(
            'text' => $text);
  
  //Count documents by type
  $Count_Type = array_count_values($doctype);
  //Display graph for document by type
  $values = array();$legend = array();
  $text =  "<li>Count documents by type :</li><ul>";
  foreach($Count_Type as $key=>$value ){
    $values[] = $value;
    $legend[] = typeName($key);
    $text .= '<li>'. typeName($key) .' : '. $value .'</li>';}
  $title = urlencode ('Documents by type');
  $values = urlencode(serialize($values));
  $legend = urlencode(serialize($legend));
  $text .= "</ul>";
  $graphs[] = array(
            'image' => "<img src='./inc/graphs/graphBar.php?title=$title&values=$values&legend=$legend' alt='no graph'/>" ,
            'text' => $text);
  
  
  //Count documents by indice
  $Count_Indice = array_count_values($indice);
  //Display graph for document by indice
  $values = array();$legend = array();
  $text =  "<li>Count documents by indice :</li><ul>";
  foreach($Count_Indice as $key=>$value ){
    $values[] = $value;
    $legend[] = indiceName($key);
    $text .= '<li>'. indiceName($key) .' : '. $value .'</li>';}
  $title = urlencode ('Documents by indice');
  $values = urlencode(serialize($values));
  $legend = urlencode(serialize($legend));
  $text .= "</ul>";
  $graphs[] = array(
            'image' => "<img src='./inc/graphs/graphBar.php?title=$title&values=$values&legend=$legend' alt='no graph'/>" ,
            'text' => $text);
  
  //Count documents by state
  $Count_State = array_count_values($state);
  //Display grap for document by state
  $values = array();$legend = array();
  $text =  "<li>Count documents by state :</li><ul>";
  foreach($Count_State as $key=>$value ){
    $values[] = $value;
    $legend[] = $key;
    $text .= '<li>'. $key .' : '. $value .'</li>';}
  $title = urlencode ('Documents by state');
  $values = urlencode(serialize($values));
  $legend = urlencode(serialize($legend));
  $text .= "</ul>";
  $graphs[] = array(
            'image' => "<img src='./inc/graphs/graphPie.php?title=$title&values=$values&legend=$legend' alt='no graph'/>" ,
            'text' => $text);
  
  //Count documents by categories
  $Count_Category = array_count_values($category);
  //Display grap for document by state
  $values = array();$legend = array();
  $text =  "<li>Count documents by category :</li><ul>";
  foreach($Count_Category as $key=>$value ){
    $values[] = $value;
    $legend[] = categoryName($key);
    $text .= '<li>'. categoryName($key) .' : '. $value .'</li>';}
  $title = urlencode ('Documents by category');
  $values = urlencode(serialize($values));
  $legend = urlencode(serialize($legend));
  $text .= "</ul>";
  $graphs[] = array(
            'image' => "<img src='./inc/graphs/graphBar.php?title=$title&values=$values&legend=$legend' alt='no graph'/>" ,
            'text' => $text);
  
  return $graphs;

}

?>
