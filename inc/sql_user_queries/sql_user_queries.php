<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//USER_QUERIES_REPOSIT_DIR

//Setup
require_once './conf/ranchbe_setup.php';
require_once 'class/common/file.php';

$dbranchbe =& create_adodb(true);

if (!is_dir (USER_QUERIES_REPOSIT_DIR) ) { print 'Error : USER_QUERIES_REPOSIT_DIR  do not exits'; die;}

$check_flood = check_flood();

switch ($_REQUEST['action']){

// ---- Suppress file
case 'suppressQueryFile':
  if(!$check_flood) break;
  if( empty($_REQUEST['checked'][0]) ) break;
  foreach($_REQUEST['checked'] as $query_file_name){
    $ofile = new file( USER_QUERIES_REPOSIT_DIR.'/'.$query_file_name );
    $ofile->suppress();
  }
  break;

// ---- Rename file
case 'renameQueryFile':
case 'copyQueryFile':
  if(!$check_flood) break;
  if( empty($_REQUEST['checked']) ) break;
  if( empty($_REQUEST['newName']) ) break;
  if( $_REQUEST['newName'] == 'null' ) break;
  $ofile = new file( USER_QUERIES_REPOSIT_DIR.'/'.$_REQUEST['checked'] );
  if( $_REQUEST['action'] == 'renameQueryFile' )
    $ofile->rename(USER_QUERIES_REPOSIT_DIR.'/'.$_REQUEST['newName']);
  if( $_REQUEST['action'] == 'copyQueryFile' )
    $ofile->copy(USER_QUERIES_REPOSIT_DIR.'/'.$_REQUEST['newName'], 0666, false);
  break;

// ----- Upload a file
case 'upload':
  file::UploadFile($_FILES['uploadFile'] , $_REQUEST['overwrite'], USER_QUERIES_REPOSIT_DIR);
  break;

// ----- Download a file
case 'download':
  require_once('./class/common/attachment.php');
  $viewer = new viewer();
  $viewer->initRawFile( USER_QUERIES_REPOSIT_DIR.'/'.$_REQUEST['checked'][0] );
  $viewer->pushfile();
  break;

// ----- Download multiples files in a zip
case ('DownloadZip'):
  require_once('File/Archive.php');
  File_Archive::setOption('zipCompressionLevel', 9);
  if( is_array($_REQUEST['checked']) ){
    foreach($_REQUEST['checked'] as $file){
      $multiread[] = File_Archive::read(USER_QUERIES_REPOSIT_DIR.'/'.$file, $file);
    }
  }else{
    break;
  }
  $reader = File_Archive::readMulti($multiread);
  $writer = File_Archive::toArchive('wildspace.zip',File_Archive::toOutput());
  File_Archive::extract($reader, $writer);
  die;
  break;

case 'editQueryFile':
  if( empty($_REQUEST['checked']) ) break;
  $smarty->assign('checked', $_REQUEST['checked']);
  $smarty->assign( 'sql_query' , file_get_contents(USER_QUERIES_REPOSIT_DIR.'/'.$_REQUEST['checked']) );
  $skip_list = true; //to prevent unusefull request of query file in USER_QUERIES_REPOSIT_DIR
  break;

case 'saveQueryFile':
  if( empty($_REQUEST['checked']) ) break;
  if( isset($_REQUEST['saveQueryFile']) ){
    $handle = fopen( USER_QUERIES_REPOSIT_DIR.'/'.$_REQUEST['checked']  , w );
    if (!$handle ) break;
    fputs($handle, $_REQUEST['sql_query'] );
    fclose($handle);
    break;
  }else break;

case 'executeQueryFile':
case 'downloadListing':
  if( empty($_REQUEST['checked']) ) break;
  if( $_REQUEST['cancel'] ){
    unset($_REQUEST['action']);
    break;
  }

  //Select the render form
  require_once ('GUI/Ranchbe_HTML_QuickForm.php'); //Librairy to easily create forms
  $form =& new Ranchbe_HTML_QuickForm('frmTest', 'POST');
  $form->setDefaults(array(
    'result_format'      => 'xls',
  ));

  //Select xls render
  $sel =& $form->addElement('hierselect', 'result_format', tra('result_format') );
  $sel->setMainOptions( array('direct'=>tra('direct display'), 'xls'=>'excel') );
  $path_queries_scripts = 'inc/excel_render/userQueries/';
  $scriptfiles = glob($path_queries_scripts . '*.php');
  $t = array(); //Temp var
  foreach($scriptfiles as $scriptfile){
    $t[] = basename($scriptfile);
  }
  $scriptfiles = array_combine($t,$t);
  $renders['xls'] = $scriptfiles;
  $renders['direct'] = array();
  $sel->setSecOptions($renders);

  //Submit
  $form->addElement('submit', 'submit', tra('show'));
  $form->addElement('submit', 'cancel', tra('cancel'));
  $form->addElement('hidden', 'checked', $_REQUEST['checked']);
  $form->addElement('hidden', 'action', 'executeQueryFile');
  $form->addElement('hidden', 'request_page', 'SQL_request');
  // Tries to validate the form
  if ($form->validate()) {
    //execute query
    $query = file_get_contents(USER_QUERIES_REPOSIT_DIR.'/'.basename($_REQUEST['checked']));
    $dbranchbe->execute($query);
    if(!$rs = $dbranchbe->execute( $query )){
      $error_stack->push(ERROR_DB, 'Fatal', array('query'=>$query), $dbranchbe->ErrorMsg());
      break;
    }
    
    //Select render
    if($_REQUEST['result_format'][0] === 'csv'){
    	echo 'bad format';die;
    }else if ($_REQUEST['result_format'][0] === 'xls'){
      require_once($path_queries_scripts.'/'.$_REQUEST['result_format'][1]);
      require_once('GUI/excelRender.php');
      $render = new excelRender($rs, 'user_query');
      $render->render();
      die;
    }else if ($_REQUEST['result_format'][0] === 'direct'){
      require_once('lib/toRBhtml.inc.php');
      rs2html($rs,'border=2 cellpadding=3');
      die;
    }
    
  }

  $smarty->assign('action', $_REQUEST['action']);
  $smarty->assign( 'render_select_form', $form->toHtml() );
  //$form->display();
  //die;
  $skip_list = true; //to prevent unusefull request of query file in USER_QUERIES_REPOSIT_DIR

} //End of switch

//Get files
if(!$skip_list){
  require_once 'class/common/rdirectory.php';
  $list = rdirectory::GetDatas(USER_QUERIES_REPOSIT_DIR , $params);
  $smarty->assign_by_ref('list', $list);
}

// Display the template
$error_stack->checkErrors();
$smarty->assign('action', $_REQUEST['action']);
$smarty->assign('mid', 'sql_user_queries.tpl');
$smarty->display('ranchbe.tpl');

?>
