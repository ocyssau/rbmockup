<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//TODO: REVOIR FILTRE: Lors du reset, affiche historique de tous plutot que l'historique du conteneur selectionne

//this script may only be included - so its better to die if called directly.
if (strpos($_SERVER["SCRIPT_NAME"],basename(__FILE__)) !== false){
  header("location: index.php");
  exit;
}
if(!isset($Manager)) {print '$Manager is not set '; die;}

$area_id = $Manager->AREA_ID;

if(!empty($_REQUEST['container_id'])){ //If a container is selected, display only history of this container...
  $container_id = $_REQUEST['container_id'];
  $Manager->init($container_id);
}

$ohistory = new history($Manager);

//Setup
require_once "HTML/QuickForm.php"; //Librairy to easily create forms
require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm
require_once './lib/Date/date.php';

//Assign name to particular fields
$smarty->assign('number_map', $Manager->GetFieldName('number'));
$smarty->assign('id_map', $Manager->GetFieldName('id'));
$smarty->assign('description_map', $Manager->GetFieldName('description'));
$smarty->assign('state_map', $Manager->GetFieldName('state'));
$smarty->assign('indice_map', $Manager->GetFieldName('indice'));
$smarty->assign('father_map_id', $Manager->GetFieldName('father'));
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number' );
$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description' );
$smarty->assign('CONTAINER_STATE' , $Manager->SPACE_NAME . '_state' );
$smarty->assign('CONTAINER_ID' , $Manager->SPACE_NAME . '_id' );
$smarty->assign('page_title' , $Manager->SPACE_NAME.' history' );

//------------------------------------------------------------------------
//Manage actions on history
if(!isset($_REQUEST['action'])) $_REQUEST['action'] = '';
switch($_REQUEST['action']){
  case 'suppress':   //Process suppress request
    check_ticket('container_history' , $area_id);
    foreach($_REQUEST['histo_order'] as $id)
      $ohistory->RemoveHistorySingle($id);
  $_REQUEST['action'] = '';
  break;
} //End of switch
//------------------------------------------------------------------------

//Include generic definition of the code for manage filters
//include('filterManager.php');


//Select table where select field (field=>table)
//$all_field['with']['category_number']['table'] = $Manager->SPACE_NAME.'_categories';
//$all_field['with']['category_number']['col'] = 'categorie_id';
$all_field['with']['indice_value']['table'] = 'document_indice';
$all_field['with']['indice_value']['col'] = 'document_indice_id';
//$all_field['with']['doctype_number']['table'] = 'doctypes';
//$all_field['with']['doctype_number']['col'] = 'doctype_id';

//Include generic definition of the code for manage filters
//$default_sort_field = ''; //Default value of the field to sort
//$default_sort_order='ASC'; //Default value of the order ASC = ascendant, DESC = descandant
include('filterManager_simple.php');

$sameurl_elements[] = 'container_id';

if(!empty($container_id)){
  $list = $ohistory->GetHistory($container_id, $params);
  $smarty->assign('container_id', $container_id);
}else //Display history of all container
  $list = $ohistory->GetAllHistory($params);

$smarty->assign_by_ref( 'list', $list );

//Include generic definition of the code for manage the pagination. $list must set...
include('paginationManager.php');

//Define select option for "find"
$all_field = array ('action_name' => 'action_name',
                    'action_by' => 'action_by',
                    'action_date' => 'action_date',
                    $Manager->GetFieldName('number') => 'Number',
                    $Manager->GetFieldName('description') => 'Description',
                    'open_by' => 'Created by' ,
                    'open_date' => 'Created date',
                    'close_by' => 'Close by' ,
                    'close_date' => 'Close date',
                    $Manager->GetFieldName('state') => 'State',
                    $Manager->GetFieldName('indice') => 'Indice',
                    );
$smarty->assign('all_field', $all_field);

$Manager->error_stack->checkErrors();

// Active the tab
// Display the template
$smarty->display("containerHistory.tpl");

?>
