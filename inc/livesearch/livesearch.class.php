<?php
class livesearch {
	var $livesearch = array();
  
  function __construct( &$dbranchbe ){
    $this->db =& $dbranchbe;
  }
  
	function qflssearch($input, $query) {
    if(strlen($input) < 5 ) return;
		$ret = array();
    $query = str_replace('%search_term%', $input, $query);
    if( !is_object($this->db) ) return array('error:$dbranchbe is not an object');
    $this->db->cacheSecs = 60; // cache n seconds
    $rs = $this->db->CacheSelectLimit( $query, 20 );
    if( !$rs ) return array('...');
    while( $row = $rs->fetchRow() ){
      $ret[ $row['document_number'] ] = utf8_encode( $row['document_number'] );
    }
    return $ret;
	}//End of method
} //End of class

?>
