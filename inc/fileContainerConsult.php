<?php

switch ( $_REQUEST['action'] ) {

  case 'putInWs':   //Put file in wildspace
    if (empty($_REQUEST['checked'])){
      $Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select ar least one item');
    }else{
      $recordfile =& $Manager->initRecordfile();
      foreach($_REQUEST['checked'] as $file_id){
        $recordfile->init($file_id);
        $recordfile->PutFileInWildspace();
      }
    }
    break;

//---------------------------------------------------------------------

  case 'viewFile':  //View file
    if(empty($_REQUEST['checked'][0])) return false;
    require_once('./class/common/attachment.php');
    $recordfile =& $Manager->initRecordfile($_REQUEST['checked'][0]); //Get the recordfile from the container object
    $fsdata =& $recordfile->initFsdata(); //Get the fsdata from the record file
    $viewer = new viewer(); //Create a new viewer
    $viewer->initFsdata($fsdata); //link the fsdata to the viewer and set property of the viewer
    $viewer->pushfile(); //Get the viewable file
  break;

} //End of switch between action

//Include generic definition of the code for manage filters
include('filterManager_simple.php');

//get all files
$recordfile =& $Manager->initRecordfile();
$list = $recordfile->GetAllFilesInFather($container_id , $params);
$smarty->assign_by_ref('list', $list);

//Include generic definition of the code for manage the pagination. $list must set...
include('paginationManager.php');

$all_field = array ('file_name' => 'Name',
                    'file_extension' => 'Extensions',
                    'import_order' => 'Import order',
                    'file_path' => 'Path',
                    'file_open_by' => 'Created by' ,
                    'file_open_date' => 'Created date',
                    'file_update_by' => 'Last update by',
                    'file_update_date' => 'Last update date',
                    'file_access_code' => 'Access',
                    'file_state' => 'State',
                    'file_version' => 'Version',
                    'file_type' => 'Type',
                    'file_size' => 'Size',
                    'file_md5' => 'Md5',
                    );
$smarty->assign('all_field', $all_field);

//Assign additional var to add to URL when redisplay the same url
$sameurl_elements[] ='container_id'; //to re-send value in url
$sameurl_elements[] ='space'; //to re-send value in url

?>
