<?php
//Script to generate xml used by dthtmlxtree javascripts lib to select document in any ranchbe area

require_once('../conf/ranchbe_setup.php');

header("Content-type:text/xml");

if(isset($_GET["id"]))
	$id=$_GET["id"];
else
	$id=0;

/*Create an array from string content in id from dthtmlxtree js lib
* $id is a string form like this : key__value--otherkey__othervalue--...etc
*/
//$id = 'space__workitem';
$id_string = explode('--' , $id);
foreach($id_string as $keyvalue){
  $keyvalue_explode = explode('__' , $keyvalue);
  $return[$keyvalue_explode[0]] = $keyvalue_explode[1];
}
//var_dump($return);

//Get space list
if(empty($return['space'])){
  print("<tree id='".$id."'>");
  $spacelist = array('workitem','bookshop','cadlib','mockup');
  foreach($spacelist as $space){
    print("<item child='1' id='space__".$space."' text='". $space ."'></item>
    ");
  }
}

/*
//Get projects
if($space == 'space'){
  require_once'class/common/space.php';
  $ospace = new space('project');
  print("<tree id='".$id."'>");
  require_once 'class/project.php'; //Class to manage the projects
  $projectManager = new project($ospace,$id); //Create new manager
  $params['sort_field'] = 'project_number';
  $params['sort_order'] = 'ASC';
  $projectlist = $projectManager->GetAllProjects($params);//get list of projects
  foreach($projectlist as $project){
    print("<item child='1' id='project__".$project['project_id']."' text='Project ". $project['project_number'] ."'>
           </item>");
  }
}
*/

//Get container list
if(isset($return['space']) && !isset($return['container'])){
  print("<tree id='".$id."'>");
  require_once '../class/common/space.php';
  $ospace = new space($return['space']); //$return['space'] = workitem/cadlib/bookshop/mockup...
  require_once '../class/common/container.php';
  $ocontainer = new container($ospace); //Create new manager
  //$params['sort_field'] = $return['space'].'_number';
  //$params['sort_order'] = 'ASC';
  $containerlist = $ocontainer->GetAll($params);//get list of projects
  if(is_array($containerlist)){
    foreach($containerlist as $container){
      $container_id = $container[$return['space'].'_id'];
      $container_number = $container[$return['space'].'_number'];
      print("<item child='1' id='".$id."--container__".$container_id."' text='".$container_number."'></item>"
      );
    }
  }
}

/*//If get container
if(isset($return['container']) && !isset($return['container_id'])){
  print("<tree id='".$id."'>");
  require_once 'class/common/container.php';
  $ocontainer = container::_factory($return['space'], $object_id); //Create new manager
  $params['exact_find']['project_id'] = $object_id;
  $params['sort_field'] = 'workitem_number';
  $params['sort_order'] = 'ASC';
  $containerlist = $ocontainer->GetAll($params);
  foreach($containerlist as $container){
    print("<item child='1' id='container__".$container['workitem_id']."' text='". $container['workitem_number'] ."'>
           </item>");
  }
}
*/

//Get categories list
if(isset($return['container']) && !isset($return['category'])){
  print("<tree id='".$id."'>");
  require_once '../class/common/container.php';
  $container = container::_factory($return['space'], $return['container']); //Create new manager
  $params['sort_field'] = 'category_number';
  $params['sort_order'] = 'ASC';
  require_once '../class/category.php';
  $ocategory = new category($container->space);
  $categorieslist = $ocategory->GetCategories($params);
  if(is_array($categorieslist)){
    foreach($categorieslist as $category){
        print("<item child='1' id='".$id."--category__".$category['category_id']."' text='".$category['category_number']."'></item>
        ");
    }
  }
  $params['where'][] = 'category_id IS NULL';
  $params['sort_field'] = 'document_number';
  $params['sort_order'] = 'ASC';
  $documentlist = $container->GetAllDocuments($params);
  if(is_array($documentlist)){
    foreach($documentlist as $document){
      print("<item child='0' id='".$id."--document_class__".$document['document_number']."' text='".$document['document_number']."'></item>
      ");
    }
  }
}

//If get documents
if(isset($return['category']) && !isset($return['document'])){
  print("<tree id='".$id."'>");
  require_once '../class/common/container.php';
  $container = container::_factory($return['space'], $return['container']); //Create new manager
  if(!empty($return['category']))
    $params['exact_find']['category_id'] = $return['category'];
  $params['sort_field'] = 'document_number';
  $params['sort_order'] = 'ASC';
  $documentlist = $container->GetAllDocuments($params);
  foreach($documentlist as $document){
      print("<item child='0' id='".$id."--document_class__".$document['document_number']."' text='".$document['document_number']."'></item>
      ");
  }
}

print("</tree>");

?> 
