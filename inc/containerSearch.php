<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Ranchbe is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

if ( ! isset($Manager) ) {print '$Manager is not set '; die;}
require_once('./class/common/document.php');
$odocument=new document($Manager->space);

require_once "HTML/QuickForm.php"; //Librairy to easily create forms
require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm
require_once'./GUI/GUI.php';
require_once './lib/Date/date.php';
require_once './lib/search.inc.php';//Search engine

$area_id = $Manager->AREA_ID;

check_ticket('container_get' , $area_id);

function prepaSearch($values){
  global $Manager;
  $values['container_type']=array($Manager->SPACE_NAME); //*space where to search
  unset($values['submit']);
  return search($values); //Search engine
}

if ($_REQUEST["action"] == 'downloadFile'){ //Read the file from browser
  if (empty($_REQUEST["checked"])){
    $Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select ar least one item' );
  }else{
    require_once('./class/common/document.php');
    $odocument=new document($Manager->space, $_REQUEST["checked"][0]);
    if($odocument->ViewDocument()) die; //0.4.5
  }
}

//-----------------------------------------------------------------------------

//Construct the form with QuickForm lib
$form = new HTML_QuickForm('form', 'post');

//Set default values
$form->setDefaults(array(
  'numrows'      => '100',
));

//Document type
require_once('./class/doctype.php');
$doctypeManager = new doctype;
$params['select']=array('doctype_number' , 'doctype_id');
$params['sort_field']='doctype_number';
$documentTypes = $doctypeManager->GetAll($params);
foreach($documentTypes as $key)
  $doctype_rekey[$key['doctype_id']] = $key['doctype_number'];
$select =& $form->addElement('select', 'doctype_id', tra('Document type'), $doctype_rekey);
$select->setSize(10);
$select->setMultiple(true);

//Designation
$form->addElement('text', 'designation', tra('Designation'));

//Number
$form->addElement('text', 'document_number', tra('Number'));

//Create user
include_once './lib/userslib.php';
$user_list = $userlib->get_users();
foreach($user_list as $key)
  $user_rekey[$key['auth_user_id']] = $key['handle'];
$select =& $form->addElement('select', 'open_by', tra('open_by'), $user_rekey);
$select->setSize(10);
$select->setMultiple(true);

//Checkout user
$select =& $form->addElement('select', 'check_out_by', tra('check_out_by'), $user_rekey);
$select->setSize(10);
$select->setMultiple(true);

//Update user
$select =& $form->addElement('select', 'update_by', tra('update_by'), $user_rekey);
$select->setSize(10);
$select->setMultiple(true);

//State
$form->addElement('text', 'document_state', tra('State'), array('size' => 10, 'maxlength' => 32));

//Indice
$indice_list = $odocument->GetIndices();
$select =& $form->addElement('select', 'document_indice_id', tra('Indice'), $indice_list);
$select->setSize(10);
$select->setMultiple(true);

//Version
$form->addElement('text', 'document_version', tra('Version'), array('size' => 3, 'maxlength' => 3));
$form->addRule('document_version', 'Version must be numeric', 'numeric', null, 'client');

//Contruct min open_date select field
$date_params = array(
          'field_name' => 'open_date_min',
          'field_required' => false,
);
construct_select_date($date_params , $form);
//Contruct max open_date select field
$date_params = array(
          'field_name' => 'open_date_max',
          'field_required' => false,
);
construct_select_date($date_params , $form);

//Contruct min checkout_date select field
$date_params = array(
          'field_name' => 'checkout_date_min',
          'field_required' => false,
          'showsTime' => true,
);
construct_select_date($date_params , $form);
//Contruct max checkout_date select field
$date_params = array(
          'field_name' => 'checkout_date_max',
          'field_required' => false,
          'showsTime' => true,
);
construct_select_date($date_params , $form);

//Contruct min update_date select field
$date_params = array(
          'field_name' => 'update_date_min',
          'field_required' => false,
          'showsTime' => true,
);
construct_select_date($date_params , $form);
//Contruct max update_date select field
$date_params = array(
          'field_name' => 'update_date_max',
          'field_required' => false,
          'showsTime' => true,
);
construct_select_date($date_params , $form);

//Get fields for custom metadata

require_once ('./class/common/metadata.php');
$metadataManager = new docmetadata($Manager->space);
$optionalFields = $metadataManager->GetMetadata();
$smarty->assign( 'optionalFields' , $optionalFields);
//var_dump($optionalFields);
if(is_array($optionalFields))
  foreach($optionalFields as $field){
    $field['field_multiple'] = true;
    construct_element($field , $form);
    if ($_REQUEST['action'] == 'modifyDoc')
      $default[$field['field_name']] = $Infos[$field['field_name']];
  }

//Select fields for document information
$fields = array(
          'document_id'=>'document_id',	
          'document_number'=>'document_number',	
          'document_state'=>'document_state',	
          'document_access_code'=>'document_access_code',	
          'document_version'=>'document_version',
          'document_indice_id'=>'document_indice_id',	
          'instance_id'=>'instance_id',	
          'doctype_id'=>'doctype_id',	
          'default_process_id'=>'default_process_id',
          'category_id'=>'category_id',	
          'check_out_by'=>'check_out_by',	
          'check_out_date'=>'check_out_date',	
          'designation'=>'designation',	
          'from_document_id'=>'from_document_id',
          'update_date'=>'update_date',	
          'update_by'=>'update_by',	
          'open_date'=>'open_date',	
          'open_by'=>'open_by'
          );

foreach($optionalFields as $val)
  $fields[$val['field_name']] = $val['field_description'];

$select =& $form->addElement('select', 'selectd', tra('Field to display for documents'), $fields);
$select->setSize(10);
$select->setMultiple(true);

//numrows
$form->addElement('text', 'numrows', tra('Number of displayed rows'), array('size' => 4, 'maxlength' => 9));

//Reset and submit
$form->addElement('reset', 'reset', 'reset');
$form->addElement('submit', 'submit', 'Search');

// Try to validate the form
if ($form->validate()) {
  // Form is validated, then process the request
  if ($_REQUEST['submit'] == 'Search'){
    $list=$form->process('prepaSearch', true);
    $smarty->assign_by_ref('list', $list);
  }
} //End of validate form

//Set the renderer for display QuickForm form in a smarty template
include 'QuickFormRendererSet.php';

$smarty->assign('PageTitle' , 'Search in ' . $Manager->SPACE_NAME);

// Active the tab
$smarty->assign( "$Manager->SPACE_NAME".'Tab' , 'active');

//Assign name to particular fields
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number');
$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description');
if(isset($search_return))
  $smarty->assign('search_return' , $search_return);

$Manager->error_stack->checkErrors();

// Display the template
$smarty->assign('icons_dir', DEFAULT_DOCTYPES_ICONS_DIR);
$smarty->assign('mid', 'containerSearch.tpl');
$smarty->display("ranchbe.tpl");

?>
