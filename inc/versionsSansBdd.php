<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//this script may only be included - so its better to die if called directly.
if (strpos($_SERVER["SCRIPT_NAME"],basename(__FILE__)) !== false){
  header("location: index.php");
  exit;
}

if(!isset($space)) {print '$space is not set '; die;}

$area_id = $space->AREA_ID;

$check_flood = check_flood();

require_once './class/common/docfile.php';
$docfile = new docfile($space,$_REQUEST['file_id']);

$smarty->assign_by_ref('file_id', $_REQUEST['file_id']);
$smarty->assign('file_name', $docfile->GetProperty('file_name'));

//Manage actions wich require at last one selection
switch($_REQUEST['action']){
//---------------------------------------------------------------------
  case 'SuppressVer':   //Suppress
    if(!$check_flood) break;
    if(empty($_REQUEST['checked'])){
      $Manager->error_stack->push(ERROR, 'Warning', array(), 'You must select at least one item');
      break;
    }
    check_ticket('container_document_version' , $area_id);
    foreach($_REQUEST["checked"] as $versionId){
      $fsdataVersion = $docfile->initFsdataVersion($versionId);
      $fsdataVersion->putInTrash();
    }
    break;
//---------------------------------------------------------------------
  case 'putInWs':   //Put version in wildspace
    if (empty($_REQUEST['checked'])){
      $Manager->error_stack->push(ERROR, 'Warning', array(), 'You must select at least one item');
      break;
    }
    foreach ($_REQUEST['checked'] as $versionId){
      $fsdataVersion = $docfile->initFsdataVersion($versionId);
      $addPrefix = 'consult__';
      $addPrefix = $addPrefix.$fsdataVersion->GetProperty('file_version').'__';
      $fsdataVersion->putInWildspace($addPrefix, true);
    }
    break;
//---------------------------------------------------------------------
  case 'viewFile':   //send file to user
    if (empty($_REQUEST['checked'])){
      $Manager->error_stack->push(ERROR, 'Warning', array(), 'You must select at least one item');
      break;
    }
    $fsdataVersion = $docfile->initFsdataVersion($_REQUEST['checked'][0]);
    $fsdataVersion->DownloadFile();
    break;
}//End of actions on multiselection

//get all file versions
if($aversion = $docfile->GetVersions())
  foreach($aversion as $version){
    $list[]=$version->getInfos(true);
  }
$smarty->assign_by_ref('list', $list);

//Define select option for "find"
$smarty->assign('all_field', $docfile->FIELDS_MAPPING);

//Assign name to particular fields
$smarty->assign('CONTAINER_TYPE', $space->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $space->SPACE_NAME . '_number' );
$smarty->assign('CONTAINER_DESCRIPTION' , $space->SPACE_NAME . '_description' );
$smarty->assign('CONTAINER_STATE' , $space->SPACE_NAME . '_state' );
$smarty->assign('CONTAINER_ID' , $space->SPACE_NAME . '_id' );

$space->error_stack->checkErrors();

// Active the tab
$smarty->assign( $space->SPACE_NAME.'Tab' , 'active');

// Display the template
$smarty->assign('randWindowName', 'container_'.uniqid());
$smarty->assign('mid', 'versions.tpl');
$smarty->display("versions.tpl");
?>
