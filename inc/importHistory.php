<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//this script may only be included - so its better to die if called directly.
if (strpos($_SERVER["SCRIPT_NAME"],basename(__FILE__)) !== false) {
  header("location: index.php");
  exit;
}
if ( ! isset($Manager) ) {print '$Manager is not set '; die;}

require_once './class/common/import.php'; //Class to manage the importations
$Import = new import($Manager->SPACE_NAME , $Manager);

$area_id = $Manager->AREA_ID;

$smarty->assign( 'action' , $_REQUEST['action']);

//Manage actions wich require at last one selection
switch ( $_REQUEST['action'] ) {

//---------------------------------------------------------------------
  case 'suppressHistory':   //Suppress history import
    if ( empty($_REQUEST["checked"]) ) {print 'none selected package'; break; }
    check_ticket( 'container_import_history' , $area_id );

    foreach ($_REQUEST['checked'] as $import_order ){
      $Import->SuppressHistory($import_order);
    }
    break;

//---------------------------------------------------------------------

  case 'cleanHistory':   //Clean the history import
    check_ticket( 'container_import_history' , $area_id );
    $Import->CleanHistory();
    break;

//---------------------------------------------------------------------

  case 'getImportHistory':   //Get the history of importations
    break;

//---------------------------------------------------------------------

  case 'viewLogFile':   //View log file
  case 'viewErrorFile':   //View log file
  if($_REQUEST['action'] == 'viewLogFile')
    $logFile = $Import->GetLogFile($_REQUEST['import_order']);
  if($_REQUEST['action'] == 'viewErrorFile')
    $logFile = $Import->GetErrorFile($_REQUEST['import_order']);
  require_once('class/common/fsdata.php');
  $fsdata = new fsdata($logFile);
  if($fsdata->DownloadFile()) die;
  else{
    $Import->error_stack->push(ERROR, 'Warning', array('element'=>$logFile), 'The logfile %element% don\'t exist');
    break;
  }

} //End of switch

$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_ID', $Manager->SPACE_NAME . '_id');

$default_sort_field = 'import_order'; //Default value of the field to sort
$default_sort_order = 'DESC'; //Default value of the order ASC = ascendant, DESC = descandant
include('filterManager_simple.php');

$spaceName = $Manager->SPACE_NAME;

//get infos on all package
if(isset($_REQUEST['fcontainer'])){
	$params['exact_find'][$spaceName . '_id'] = $_REQUEST['fcontainer'];
	$smarty->assign('fcontainer', $_REQUEST['fcontainer']);
}

$list = $Import->GetImportHistory($params);
$smarty->assign_by_ref('list', $list);

//Get list of containers
//$Container = new container::_factory($Manager->SPACE_NAME);
$containers = $Manager->GetAll(array(
        'sort_field' => $spaceName . '_number',
        'sort_order' => 'ASC',
        'select' => array($spaceName . '_number', $spaceName . '_id')
));
foreach ($containers as $value ){
	$t[$value[$spaceName . '_id']] = $value[$spaceName . '_number'];
}

$all_field[0] = array ( 'package_file_name' => 'package_file_name',
                        'description' => 'description',
						'import_order'=>'import_order');
$smarty->assign('all_field', $all_field[0]);
$smarty->assign('containers', $t);

$Manager->error_stack->checkErrors();

// Display the template
$smarty->display('importHistory.tpl');
