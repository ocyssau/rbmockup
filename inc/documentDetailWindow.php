<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//this script may only be included - so its better to die if called directly.
if (strpos($_SERVER["SCRIPT_NAME"],basename(__FILE__)) !== false){
	header("location: index.php");
	exit;
}
if(!isset($space)) {print '$space is not set '; die;}
if(!isset($odocument)) {print '$odocument is not set '; die;}
$smarty->register_object( 'odocument' , $odocument); //register object to use in smarty plugins

$area_id = $space->AREA_ID;

$check_flood = check_flood();

//Setup
require_once "HTML/QuickForm.php"; //Librairy to easily create forms
require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm

//Init variables
$document_id = $_REQUEST['document_id'];
$odocument->init($document_id);
$smarty->assign('document_id', $document_id);
$Manager =& $odocument; //Assign $Manager var use in smarty modifier functions.

$documentInfos = $odocument->GetDocumentInfos();
$document_number = $documentInfos['document_number'];
$smarty->assign('document_name', $document_number);
$smarty->assign('docinfos', $documentInfos);

$document_instance = $documentInfos['process_instance_id'];

require_once('./class/common/metadata.php');
$metadata = new docmetadata($odocument->space);
$optionalFields = $metadata->GetMetadataLinked( NULL, $odocument->GetDocProperty('container_id') );
$smarty->assign( 'optionalFields' , $optionalFields);

$smarty->assign('mid', 'documentDetailWindow.tpl');
$smarty->assign('file_icons_dir', DEFAULT_FILE_ICONS_DIR);

function filterSDoc($var){ //No more in use????
	return strstr($var , 'document');
}

/*Create an array from string content in id from dthtmlxtree js lib
 * $id is a string form like this : key__value--otherkey__othervalue--...etc
 */
function DthtmlxtreeIdToArray($id){
	$id_string = explode('--' , $id);
	foreach($id_string as $keyvalue){
		$keyvalue_explode = explode('__' , $keyvalue);
		$return[$keyvalue_explode[0]] = $keyvalue_explode[1];
	}
	return $return;
}

$smarty->assign('content1display', 'block');
$smarty->assign('content2display', 'none');
$smarty->assign('content3display', 'none');
$smarty->assign('content4display', 'none');

//Manage actions on documents --------------------------------------------------

switch ($_REQUEST['action']){

	//View document
	case ('viewDoc'):
		if($odocument->ViewDocument()) die;
		break;

		//----------DOCFILE MANAGEMENT :
	case ('SuppressLink'):
		if(!$check_flood) break;
		if (!empty($_REQUEST["checked"])){
			check_ticket('container_document_link_file', $area_id);
			require_once('./class/common/docfile.php');
			foreach ($_REQUEST["checked"] as $file_id){
				$docfile = new docfile($space, $file_id);
				$docfile->RemoveFile(true,true);
			}
		}
		break;

		//Reset files
	case ('ResetFile'):
		if(!$check_flood) break;
		if(!empty($_REQUEST["checked"])){
			require_once('./class/common/docfile.php');
			foreach ($_REQUEST["checked"] as $file_id){
				$docfile = new docfile($space, $file_id);
				$docfile->ResetFile();
			}
		}
		break;

		//View document
		//View file
	case ('viewFile'):
	case ('viewDocument'):
		if(!empty($_REQUEST['file_id'])){
			require_once('./class/common/attachment.php');
			$viewer = new viewer($odocument);
			$docfile =& $odocument->initDocfile($_REQUEST['file_id']);
			$fsdata  =& $docfile->initFsdata();
			$viewer->initFsdata($fsdata);
			//$viewer->initFsdata($odocument->initDocfile($_REQUEST['file_id'])->initFsdata());
			$viewer->pushfile();
		}
		break;

		//View file
		/*
		 case ('viewFile'):
		 if (empty($_REQUEST['checked'][0])){
		 $space->error_stack->push(ERROR, 'Warning', array(), 'You must select at least one item');
		 break;
		 }
		 require_once('./class/common/attachment.php');
		 $viewer = new viewer();
		 $version->init($_REQUEST['checked'][0]);
		 if($fsdata =& $version->initFsdata())
		 if($viewer->initFsdata($fsdata))
		 $viewer->pushfile();
		 break;
		 */

		//Put file in wildspace
	case 'putInWs':
		if (empty($_REQUEST["checked"])) {
			$Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select ar least one item');
		}else{
			require_once('./class/common/docfile.php');
			foreach($_REQUEST["checked"] as $file_id){
				$docfile = new docfile($space, $file_id);
				$docfile->PutFileInWildspace();
			}
		}
		break;

		//----------DOCLINKS MANAGEMENT :
	case ('AddSon'):
		if(!$check_flood) break;
		if(!isset($_REQUEST['l_selected'])){
			$smarty->assign('actionName', 'AddSon');
			$smarty->display('selectDocument.tpl');
			die;
		}else{
			require_once('./class/common/doclink.php');
			$docrel = new doclink($odocument);
			foreach ($_REQUEST['l_selected'] as $dthtmlxtreeId) {
				$son_doc = DthtmlxtreeIdToArray($dthtmlxtreeId);
				$docrel->AddSon( $son_doc['document_id'] );
			}
		}
		$smarty->assign('content1display', 'none');
		$smarty->assign('content3display', 'block');
		break;

		//Remove a son from document
	case ('SuppressSon'):
		if(!$check_flood) break;
		if (!empty($_REQUEST["dr_link_id"])){
			require_once('./class/common/doclink.php');
			$docrel = new doclink($odocument);
			foreach ($_REQUEST["dr_link_id"] as $link_id){
				$docrel->SuppressSon($link_id);
			}
		}
		$smarty->assign('content1display', 'none');
		$smarty->assign('content3display', 'block');
		break;

		//View father doclink
	case ('ViewFatherDoc'):
		if (!empty($_REQUEST["document_id"])){
			require_once('./class/common/doclink.php');
			$docrel = new doclink($odocument);
			$father_documents = $docrel->GetFathers();
			$smarty->assign_by_ref('father_documents', $father_documents);
		}
		$smarty->assign('content1display', 'none');
		$smarty->assign('content3display', 'block');
		break;

		//----------DOC RELATION SHIP MANAGEMENT :
		/*
		 case ('AddSon'):
		 if(!$check_flood) break;
		 if(!isset($_REQUEST['l_selected'])){
		 $smarty->assign('actionName', 'AddSon');
		 $smarty->display('selectDocument.tpl');
		 die;
		 }else{
		 require_once('./class/productStructure/relationship.php');
		 $docrel = new relationship($odocument);
		 foreach ($_REQUEST['l_selected'] as $dthtmlxtreeId) {
		 $son_doc = DthtmlxtreeIdToArray($dthtmlxtreeId);
		 $docrel->AddSon($son_doc['document_class'],$son_doc['space']); //$son_doc[1] = value of document class
		 }
		 }
		 break;

		 //Remove a son from document
		 case ('SuppressSon'):
		 if(!$check_flood) break;
		 if (!empty($_REQUEST["rs_link_id"])){
		 require_once('./class/productStructure/relationship.php');
		 $docrel = new relationship($odocument);
		 foreach ($_REQUEST["rs_link_id"] as $link_id){
		 $docrel->SuppressSon($link_id);
		 }
		 }
		 break;

		 //View father doclink
		 case ('ViewFatherDoc'):
		 if (!empty($_REQUEST["document_id"])){
		 require_once('./class/productStructure/relationship.php');
		 $docrel = new relationship($odocument);
		 $father_documents = $docrel->GetFathers();
		 $smarty->assign_by_ref('father_documents', $father_documents);
		 }
		 break;
		 */

		//----------DOCUMENT ALTERNATIVES - NOT IN USE FOR THE MOMENT:
		/*
		 case ('AddAlternative'):
		 if(!$check_flood) break;
		 if(!isset($_REQUEST['l_selected'])){
		 $smarty->assign('actionName', 'AddAlternative');
		 $smarty->assign('alternative_group', $_REQUEST['alternative_group']);
		 $smarty->display('selectDocument.tpl');
		 die;
		 }else{
		 require_once('./class/productStructure/alternative.php');
		 $alternative = new alternative($odocument);
		 foreach ($_REQUEST['l_selected'] as $dthtmlxtreeId){
		 $alt_doc = DthtmlxtreeIdToArray($dthtmlxtreeId);
		 $alternative->AddAlter($alt_doc['document_class'],$alt_doc['space'],$_REQUEST['alternative_group']);
		 }
		 }
		 break;

		 //----------DOCUMENT ALTERNATIVES - NOT IN USE FOR THE MOMENT:
		 //Remove an alternative from document
		 case ('SuppressAlter'):
		 if(!$check_flood) break;
		 if (!empty($_REQUEST['alt_link_id'])){
		 require_once('./class/productStructure/alternative.php');
		 $alternative = new alternative($odocument);
		 foreach ($_REQUEST['alt_link_id'] as $link_id){
		 $alternative->SuppressAlter($link_id);
		 }
		 }
		 break;
		 */

} //End of Case

//==========================================================================
//Monitor associated files--------------------------------------------------
//==========================================================================
//get all file associated to the selected document
$list = $odocument->GetAssociatedFiles(NULL , true);
//$odocument->error_stack->checkErrors();
$smarty->assign_by_ref('list', $list);

//Assign additional var to add to URL when redisplay the same url
$sameurl_elements[]='documentDetail';

//Assign name to particular fields
$smarty->assign('CONTAINER_TYPE', $odocument->SPACE_NAME);

//require_once('./class/common/attachment.php');
//$viewer = new viewer();
//$viewer->init($odocument);

//Display image of document
//$smarty->assign('documentPicture', $viewer->displayPicture($document_id));
//Display visu of document
//$smarty->assign('documentVisu', $viewer->displayVisu($document_id));

// Display the detail of files
//Affichage en plusieurs fois pour regler les problemes de conflits de variables entre les differents "ateliers"
$smarty->assign('display', 'init');
$smarty->display("documentDetailWindow.tpl");

//==========================================================================
//Monitor Relationships--------------------------------------------------
//==========================================================================
//require_once('./class/productStructure/relationship.php');
//$docrel = new relationship($odocument);
require_once('./class/common/doclink.php');
$docrel = new doclink($odocument);
$docrel_list = $docrel->GetSons();
//$odocument->error_stack->checkErrors();
$smarty->assign_by_ref('doclinked_list', $docrel_list);

// Display the detail of files
//Affichage en plusieurs fois pour regler les problemes de conflits de variables entre les differents "ateliers"
$smarty->assign('display', 'doclink');
$smarty->display('documentDetailWindow.tpl');

//==========================================================================
//Monitor Alternatives--------------------------------------------------
//==========================================================================
/*
 require_once('./class/productStructure/alternative.php');
 $alternative = new alternative($odocument);
 $alternative_list = $alternative->GetAlter();
 $odocument->error_stack->checkErrors();
 $smarty->assign_by_ref('alternative_list', $alternative_list);

 // Display the detail of files
 //Affichage en plusieurs fois pour regler les problemes de conflits de variables entre les differents "ateliers"
 $smarty->assign('display', 'alternative');
 $smarty->display("documentDetailWindow.tpl");
 */
//==========================================================================
//Monitor Instances--------------------------------------------------
//==========================================================================

if ( isset($_REQUEST['displayWf'])){
	require_once ('./conf/galaxia_setup.php');
	include_once ('lib/Galaxia/ProcessMonitor.php');

	$_REQUEST['filter_process'] = '';
	$_REQUEST['filter_instanceName'] = "$documentInfos[document_number]" .'_'. "$documentInfos[document_indice_id]";
	$popup = '1';

	include 'galaxia_monitor_instances.php';
	$smarty->assign('display', 'instance');
	$smarty->display("documentDetailWindow.tpl");

	//==========================================================================
	//Admin Instances--------------------------------------------------
	//==========================================================================
	if ( isset($_REQUEST['iid'])){

		$smarty->assign_by_ref('iid', $iid);

		$popup = '1';

		include 'galaxia_admin_instance.php';
		$smarty->assign('display', 'detailinstance');
		$smarty->display("documentDetailWindow.tpl");

	}



	//Detail activity--------------------------------------------------
	if ( isset($_REQUEST['aid'])){

		$popup = '1';

		include 'galaxia_admin_instance_activity.php';

		$smarty->assign('display', 'detailActivity');
		$smarty->display("documentDetailWindow.tpl");
	}

}



$smarty->assign('display', 'bottom');
$smarty->display("documentDetailWindow.tpl");
$odocument->error_stack->checkErrors();

