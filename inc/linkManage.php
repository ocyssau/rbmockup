<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once('class/propsetContainerLink.php');

//this script may only be included - so its better to die if called directly.
if(strpos($_SERVER["SCRIPT_NAME"],basename(__FILE__)) !== false){
  header("location: index.php");
  exit;}

if(!isset($Manager)) {print '$Manager is not set '; die;}
if(!isset($metadata)) {print '$metadata is not set '; die;}

$area_id = $Manager->AREA_ID;
$params=array(); //init var

//Prevent reused old POST data
$check_flood = check_flood($_REQUEST['page_id']);

//Load the right definition file for current area
//load_rights_def( $area_id );

// Active the tab
$smarty->assign('documentManage', 'active'); //Assign variable to smarty

//Assign name to particular fields
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number');
$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description');
$smarty->assign('CONTAINER_STATE' , $Manager->SPACE_NAME . '_state');
$smarty->assign('CONTAINER_ID' , $Manager->SPACE_NAME . '_id');

$smarty->assign('container_id', $_REQUEST['container_id']);
$smarty->assign('container_number', $Manager->GetProperty('container_number'));

//Manage actions on documents
if(!empty($_REQUEST["action"])){

switch ($_REQUEST['action']){
//---------------------------------------------------------------------
  case 'linkDoctype':   //Link a doctype
    check_ticket( 'container_doctype' , $area_id );
    $smarty->assign('action', $_REQUEST['action']);
    if(!$_REQUEST['flag']){ // Select the process to link
      require_once './class/doctype.php'; //Class to manage the doctypes
      $doctypesManager = new doctype; //Create new manager
      //Get list of doctypes
      $smarty->assign('sort_order', $_REQUEST['sort_order']);
      $smarty->assign('sort_field', $_REQUEST['sort_field']);
      $sameurl_elements = array('sort_order','sort_field', 'container_id', 'action', 'ticket');
      $smarty->assign_by_ref('sameurl_elements', $sameurl_elements);
      $params['sort_field'] = 'doctype_number'; //Default sort field
      $params['sort_order'] = 'ASC'; //Default sort order
      if(!empty($_REQUEST['sort_field'])){
        $params['sort_field'] = $_REQUEST['sort_field'];
        $params['sort_order'] = $_REQUEST['sort_order'];}
      $list = $doctypesManager->GetAll($params);
      $smarty->assign_by_ref('list', $list);
      $smarty->display('header.tpl');
      $smarty->display('containerAddLinkDoctype.tpl');
      die;
    }else{ // Get the process from selection and link it to doctype
      if(!$check_flood) {$_REQUEST['action'] = 'getDoctypeLinks'; break;}
      $smarty->assign('doctype_id', $_REQUEST['checked']);
      foreach ( $_REQUEST['checked'] as $doctype_id){
        $Manager->LinkDoctype($doctype_id);
      }
    }
    $_REQUEST['action'] = 'getDoctypeLinks'; //to return to the list
    break;

//---------------------------------------------------------------------
  case 'unlinkDoctype':   //Suppress
    if(!$check_flood) {$_REQUEST['action'] = 'getDoctypeLinks'; break;}
    if(empty($_REQUEST['link_id'])){
      print 'You must select ar least one item'; 
      $_REQUEST['action'] = 'getDoctypeLinks'; //to return to the list
      break;}
    check_ticket('container_doctype' , $area_id);
    foreach($_REQUEST['link_id'] as $link_id)
      $Manager->SuppressDoctypeLink($link_id);
    $_REQUEST['action'] = 'getDoctypeLinks'; //to return to the list
    break;

//---------------------------------------------------------------------
  case 'linkProcess':   //Link a process
    if (empty($_REQUEST['link_id'])){
      print 'You must select ar least one item';
      $_REQUEST['action'] = 'getDoctypeLinks'; //to return to the list
      break;}
    check_ticket( 'container_doctype' , $area_id );
    
    $smarty->assign('action', $_REQUEST['action']);
    $smarty->assign('link_id', $_REQUEST['link_id'][0]);

    require_once ('galaxia_setup.php');
    include_once ('lib/Galaxia/ProcessMonitor.php');

    $_REQUEST['offset'] = 0; //To correct a bug
    if(!$_REQUEST['flag']){ // Select the process to link
      $popup = 1;
      include 'galaxia_monitor_processes.php';
      $smarty->assign('popup', 1);
      $smarty->display('header.tpl');
      $smarty->display('containerAddLinkProcess.tpl');
      die;
    }else{ // Get the process from selection and link it to doctype
      if(!$check_flood) {$_REQUEST['action'] = 'getDoctypeLinks'; break;}
      $smarty->assign('process_id', $_REQUEST['checked']);
      $Manager->LinkDoctypesProc( $_REQUEST['link_id'] , $_REQUEST['checked'][0]);
    }
    $_REQUEST['action'] = 'getDoctypeLinks'; //to return to the list
    break;

//---------------------------------------------------------------------
  case 'unlinkDoctypeProcess':   //Remove process linked to doctype
    if(!$check_flood) {$_REQUEST['action'] = 'getDoctypeLinks'; break;}
    if(empty($_REQUEST['link_id'])){
      print 'You must select ar least one item'; 
      $_REQUEST['action'] = 'getDoctypeLinks'; //to return to the list
      break;}
    check_ticket( 'container_doctype' , $area_id );
    foreach ($_REQUEST['link_id'] as $link_id){
      $Manager->SuppressDoctypeProcLink($link_id);
    }
    $_REQUEST['action'] = 'getDoctypeLinks'; //to return to the list
    break;

//---------------------------------------------------------------------
  case 'linkMetadata':   //Link a metadata
    check_ticket( 'container_doctype' , $area_id );
    $smarty->assign('action', $_REQUEST['action']);
    if(!$_REQUEST['flag']){ // Select the process to link
      //Get list of metadatas
      $list = $metadata->GetMetadata();
      $smarty->assign_by_ref('list', $list);
      $smarty->display('header.tpl');
      $smarty->display('containerAddLinkMetadata.tpl');
      die;
    }else{ // Get the process from selection and link it to doctype
      if(!$check_flood) {$_REQUEST['action'] = 'getMetadataLinks'; break;}
      $smarty->assign('field_name', $_REQUEST['checked']);
      foreach ( $_REQUEST['checked'] as $field_name){
        $metadata->LinkMetadata($field_name, $_REQUEST['container_id']);
      }
    $_REQUEST['action'] = 'getMetadataLinks';
    }
    break;

//---------------------------------------------------------------------
  case 'unlinkMetadata':   //Suppress link to metadata
    if(!$check_flood) {$_REQUEST['action'] = 'getMetadataLinks'; break;}
    if (empty($_REQUEST['link_id'])){
      print 'You must select ar least one item'; 
      $_REQUEST['action'] = 'getMetadataLinks';
      break;}
    check_ticket( 'container_doctype' , $area_id );
    foreach ($_REQUEST['link_id'] as $link_id){
      $metadata->SuppressMetadaLink($link_id);
    }
    $_REQUEST['action'] = 'getMetadataLinks';
    break;

//---------------------------------------------------------------------
  case 'linkPropset':   //Link a propset
    check_ticket( 'container_doctype' , $area_id );
    $smarty->assign('action', $_REQUEST['action']);
    $propsetlinks = new propsetContainerLink($Manager);
    if(!$_REQUEST['flag']){ // Select the propset to link
      require_once('class/propset.php');
      $propset = new propset($space);
      $list = $propset->GetPropset(); //Get list of propset
      $smarty->assign_by_ref('list', $list);
      $smarty->display('header.tpl');
      $smarty->display('containerAddLinkPropset.tpl');
      die;
    }else{
      if(!$check_flood) {$_REQUEST['action'] = 'getPropsetLinks'; break;}
      //$smarty->assign('field_name', $_REQUEST['checked']);
      if( is_array($_REQUEST['checked']) )
      foreach ( $_REQUEST['checked'] as $propset_id){
        $propsetlinks->LinkPropset($propset_id);
      }
    $_REQUEST['action'] = 'getPropsetLinks';
    }
    break;

//---------------------------------------------------------------------
  case 'unlinkPropset':   //Suppress link to propset
    if(!$check_flood) {$_REQUEST['action'] = 'getPropsetLinks'; break;}
    $propsetlinks = new propsetContainerLink($Manager);
    if (empty($_REQUEST['link_id'])){
      $propsetlinks->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item');
      $_REQUEST['action'] = 'getPropsetLinks';
      break;
    }
    check_ticket( 'container_doctype' , $area_id );
    if( is_array($_REQUEST['link_id']) )
    foreach ($_REQUEST['link_id'] as $link_id){
      $propsetlinks->SuppressPropsetLink($link_id);
    }
    $_REQUEST['action'] = 'getPropsetLinks';
    break;

//---------------------------------------------------------------------
  case 'linkCategory':   //Link a category
    //check_ticket( 'container_doctype' , $area_id );
    $smarty->assign('action', $_REQUEST['action']);
    if(!$_REQUEST['flag']){ // Select the category to link
      require_once('class/category.php');
      $category = new category($space);
      $list = $category->GetCategories(); //Get list of categories
      $smarty->assign_by_ref('list', $list);
      $smarty->display('header.tpl');
      $smarty->display('containerAddLinkCategory.tpl');
      die;
    }else{
      if(!$check_flood) {$_REQUEST['action'] = 'getCategoryLinks'; break;}
      require_once('class/categoryContainerLink.php');
      $categoryContainerLink = new categoryContainerLink($Manager);
      if( is_array($_REQUEST['checked']) )
      foreach ( $_REQUEST['checked'] as $category_id){
        $categoryContainerLink->AddLink( array('container_id'=>$Manager->GetId(), 'category_id'=>$category_id ) );
      }
    $_REQUEST['action'] = 'getCategoryLinks';
    }
    break;

//---------------------------------------------------------------------
  case 'unlinkCategory':   //Suppress link to category
    if(!$check_flood) {$_REQUEST['action'] = 'getPropsetLinks'; break;}
    require_once('class/categoryContainerLink.php');
    $categoryContainerLink = new categoryContainerLink($Manager);

    if (empty($_REQUEST['link_id'])){
      $categoryContainerLink->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one item');
      $_REQUEST['action'] = 'getCategoryLinks';
      break;
    }
    //check_ticket( 'container_doctype' , $area_id );
    if( is_array($_REQUEST['link_id']) )
    foreach ($_REQUEST['link_id'] as $link_id){
      $categoryContainerLink->SuppressLink($link_id);
    }
    $_REQUEST['action'] = 'getCategoryLinks';
    break;
  } //End of switch between action

} //End of not empty action condition

//Include generic definition of the code for manage filters
//include('filterManager_simple.php');
//Process the sort request
$smarty->assign('sort_order', $_REQUEST['sort_order']);
$smarty->assign('sort_field', $_REQUEST['sort_field']);
$smarty->assign_by_ref('sameurl_elements', $sameurl_elements);
$sameurl_elements = array('sort_order','sort_field', 'container_id', 'action', 'space');

$container_number = $Manager->GetName();
$smarty->assign('container_number', $container_number);

//get links
if($_REQUEST["action"] == 'getMetadataLinks'){
  if(!empty($_REQUEST['sort_field'])){
    $params['sort_field'] = 'm.'.$_REQUEST['sort_field'];
    $params['sort_order'] = $_REQUEST['sort_order'];}
  $list = $metadata->GetMetadataLinked($params, $Manager->container_id);
  $template = 'containerMetadataLinked.tpl';
}

if($_REQUEST["action"] == 'getDoctypeLinks'){
  if(!empty($_REQUEST['sort_field'])){
    $params['sort_field'] = $_REQUEST['sort_field'];
    $params['sort_order'] = $_REQUEST['sort_order'];
  }
  $list = $Manager->GetDoctypes(NULL , NULL, $params);
  $template = 'containerDoctypes.tpl';
}

if($_REQUEST["action"] == 'getPropsetLinks'){
  $propsetlinks = new propsetContainerLink($Manager);
  if(!empty($_REQUEST['sort_field'])){
    $params['sort_field'] = $_REQUEST['sort_field'];
    $params['sort_order'] = $_REQUEST['sort_order'];
  }
  $list = $propsetlinks->GetLinks($params);
  $template = 'containerPropsetLinked.tpl';
}

if($_REQUEST["action"] == 'getCategoryLinks'){
  require_once('class/categoryContainerLink.php');
  $categoryContainerLink = new categoryContainerLink($Manager);
  if(!empty($_REQUEST['sort_field'])){
    $params['sort_field'] = $_REQUEST['sort_field'];
    $params['sort_order'] = $_REQUEST['sort_order'];
  }
  $list = $categoryContainerLink->GetLinks($params);
  $template = 'containerCategoryLinked.tpl';
}

$smarty->assign_by_ref('list', $list);

//Include generic definition of the code for manage the pagination. $list must set...
//include('paginationManager.php');

$all_field = array ('' => 'Number',
                    '' => 'Designation',
                    );
$smarty->assign('all_field', $all_field);

$Manager->error_stack->checkErrors();

// Display the template
//$smarty->display("containerDoctypes.tpl");
$smarty->display($template);
?>
