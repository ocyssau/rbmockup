<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//Setup
require_once 'conf/ranchbe_setup.php';
require_once 'class/common/file.php';
require_once 'class/common/rdirectory.php';

class fileManager{

function __construct($directory){

  if( empty($directory) ) {
    print 'Fatal Error: var $directory is empty <br />';
    die;
  }
  
  if( !filesystem::limitDir($directory) ){
    print 'Fatal Error: access to directory '.$directory.' is not auhorized. See $AUTHORIZED_DIR variable configuration <br />';
    //die;
  }
  
  if (!is_dir ($directory) ) {
    print 'Error : USER_QUERIES_REPOSIT_DIR  do not exits';
    die;
  }
  
  $this->directory =$directory;
  $this->check_flood = check_flood();
  
  global $smarty;
  $this->smarty =& $smarty;

} //End of method

function selectAction(){
  switch ($_REQUEST['action']){
  // ---- Suppress file
  case 'suppressFile':
    if(!$this->check_flood) break;
    if( empty($_REQUEST['checked'][0]) ) break;
    foreach($_REQUEST['checked'] as $file_name){
      $ofile = new file( $this->directory.'/'.$file_name );
      $ofile->suppress();
    }
    break;
  
  // ---- Rename file
  case 'renameFile':
  case 'copyFile':
    if(!$this->check_flood) break;
    if( empty($_REQUEST['checked']) ) return false;
    if( empty($_REQUEST['newName']) ) return false;
    if( $_REQUEST['newName'] == 'null' ) return false;
    $ofile = new file( $this->directory.'/'.$_REQUEST['checked'] );
    if( $_REQUEST['action'] == 'renameQueryFile' )
      $ofile->rename($this->directory.'/'.$_REQUEST['newName']);
    if( $_REQUEST['action'] == 'copyQueryFile' )
      $ofile->copy($this->directory.'/'.$_REQUEST['newName'], 0666, false);
    break;
  
  // ----- Upload a file
  case 'upload':
    file::UploadFile($_FILES['uploadFile'] , $_REQUEST['overwrite'], $this->directory);
    break;
  
  // ----- Download a file
  case 'download':
    require_once('./class/common/attachment.php');
    $viewer = new viewer();
    $viewer->initRawFile( $this->directory.'/'.$_REQUEST['checked'][0] );
    $viewer->pushfile();
    break;
  
  // ----- Download multiples files in a zip
  case ('DownloadZip'):
    require_once('File/Archive.php');
    File_Archive::setOption('zipCompressionLevel', 9);
    if( is_array($_REQUEST['checked']) ){
      foreach($_REQUEST['checked'] as $file){
        $multiread[] = File_Archive::read($this->directory.'/'.$file, $file);
      }
    }else{
      return false;
    }
    $reader = File_Archive::readMulti($multiread);
    $writer = File_Archive::toArchive('files.zip',File_Archive::toOutput());
    File_Archive::extract($reader, $writer);
    die;
    break;
  } //End of switch
} //End of method

function getFiles(){
  $list = rdirectory::GetDatas($this->directory , $params);
  $this->smarty->assign_by_ref('list', $list);
} //End of method

function render(){
  $this->smarty->assign('action', $_REQUEST['action']);
  $this->smarty->display('fileManager.tpl');
} //End of method

} //End of class

$fileManager = new fileManager(USER_QUERIES_REPOSIT_DIR);
$fileManager->getFiles();
$fileManager->render();

?>
