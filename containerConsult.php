<?php
require_once './conf/ranchbe_setup.php';//ranchBE configuration
require_once './class/common/container.php'; //Class to manage the container

//var_dump($_SESSION['objectList']);

if ( isset($_REQUEST['space']) && isset($_REQUEST['container_id']) ){
  $space_name = $_REQUEST['space'];
  $container_id = $_REQUEST['container_id'];
}else
  if ( isset($_SESSION['myActiveConsult']['space']) && isset($_SESSION['myActiveConsult']['container_id']) ){
    $space_name = $_SESSION['myActiveConsult']['space'];
    $container_id = $_SESSION['myActiveConsult']['container_id'];
}

if( isset($space_name) && isset($container_id) ){
  $Manager =& container::_factory($space_name , $container_id ); //Create new manager
  $space =& $Manager->space;
  $area_id = $Manager->AREA_ID;

  $smarty->assign('space', $space_name );
  $smarty->assign('container_id', $container_id );

  check_ticket( 'container_document_get',$area_id );

  $smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
  $smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number');
  $smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description');
  $smarty->assign('CONTAINER_STATE' , $Manager->SPACE_NAME . '_state');
  $smarty->assign('CONTAINER_ID' , $Manager->SPACE_NAME . '_id');

  if( $Manager->GetProperty('file_only') == 1 ){
    include('inc/fileContainerConsult.php');
  }else{
    include('inc/docContainerConsult.php');
  }

  $smarty->assign('icons_dir', DEFAULT_DOCTYPES_ICONS_DIR);
  $smarty->assign('deposit_dir', $Manager->GetProperty('default_file_path'));
  $smarty->assign('thumbs_dir', $Manager->DEFAULT_DEPOSIT_DIR.'/__attachments/_thumbs');
  $smarty->assign('thumbs_extension', '.png');
  
  $Manager->error_stack->checkErrors();

}

// Display the template
$_SESSION['myspace'] = array('activePage'=>$_SERVER['PHP_SELF']);  //to remember the activated tab

if( !empty($_REQUEST['container_id']) && !empty($_REQUEST['space']) )
  $_SESSION['myActiveConsult'] = array('container_id'=>$_REQUEST['container_id'], 'space'=>$_REQUEST['space']);  //to remember the activated tab

$smarty->assign('containerConsultTab' , 'active');

if( isset($space_name) && isset($container_id) ) {
  if( $Manager->GetProperty('file_only') == 1 ){
    $smarty->assign('mid', 'fileContainerConsult.tpl');
  }else{
    $smarty->assign('mid', 'docContainerConsult.tpl');
  }
}

$smarty->display('ranchbe.tpl');

?>
