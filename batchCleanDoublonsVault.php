#!/usr/bin/php
<?php 
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_USER_WARNING);

/**
 * Search for each mockups double file in previous folders
 */
require_once './conf/cliboot.php';
require_once './class/mockup.php'; //Class to manage the bookshop

if(is_file('/var/log/rbmockup/abnormalDoublons.log')){
	unlink('/var/log/rbmockup/abnormalDoublons.log');
}

$Mockup = new mockup();
$mockups = $Mockup->GetAll();

$saved = 0;
$count = 0;
foreach($mockups as $mockEntry){
	$folder = $mockEntry['default_file_path'];
	$folder = str_replace('/__imported', '', $folder);
	
	$i = 1;
	$folder0 = $folder;
	$folder1 = $folder0 . '-' . $i;
	
	echo 'Run on ' . $folder1 . PHP_EOL;
	
	while( is_dir($folder1) ){
		echo 'Follow the directory ' . $folder1 . PHP_EOL;
		if( is_dir($folder1 . '/__imported/cgr') ){
			$f1 = $folder1 . '/__imported/cgr';
			$f0 = $folder0 . '/__imported/cgr';
		}
		else{
			$f1 = $folder1 . '/__imported';
			$f0 = $folder0 . '/__imported';
		}
		
		$ret = regr_clean_doublons($folder, $i);
		
		$saved = $saved + $ret['saved'];
		$count = $count + $ret['count'];
		
		$i++;
		$folder0 = $folder1;
		$folder1 = $folder . '-' . $i;
	}
}

$saved = $saved/1024; //Ko
$saved = $saved/1024; //Mo
echo "Save $saved Mo on filesystem in $count files" . PHP_EOL;

/**
 * Regresive function
 */
function regr_clean_doublons( $folder, $i){
	$saved = 0;
	$count = 0;
	$folder1 = $folder . '-' . $i;
	$i = $i-1;
	$folder0 = $folder . '-' . $i;
	while( is_dir($folder0) && $i >= 0 ){
		echo  "regr_clean_doublons on $folder0" . PHP_EOL;
		if( is_dir($folder1 . '/__imported/cgr') ){
			$f1 = $folder1 . '/__imported/cgr';
			$f0 = $folder0 . '/__imported/cgr';
		}
		else{
			$f1 = $folder1 . '/__imported';
			$f0 = $folder0 . '/__imported';
		}
		
		echo 'suppress in f1: ' . $f1 . PHP_EOL;
		echo 'find in f0: ' . $f0 . PHP_EOL;
		
		$ret = it_clean_doublons($f0, $f1);
		
		$size = $saved + $ret['saved'];
		$count = $count + $ret['count'];
		
		$i = $i-1;
		if($i == 0){
			$folder0 = $folder;
		}
		else{
			$folder0 = $folder . '-' . $i;
		}
	}
	return array('saved'=>$saved, 'count'=>$count);
}


/**
 * Suppress doublons from $folder1 find in $folder0
 * 
 * @param string $folder0
 * @param string $folder1
 * @return array('saved'=>byte saved, 'count'=>number of suppressed doublons)
 */
function it_clean_doublons($folder0, $folder1){
	echo "Try to suppress doublons from $folder1 find in $folder0" . PHP_EOL;
	$saved = 0;
	$count = 0;
	
	$it = new DirectoryIterator($folder1);
	foreach($it as $prevFile){
		$prevFilePath = $prevFile->getPathname() . PHP_EOL;
		$lastFilePath = str_replace($folder1, $folder0, $prevFilePath);
		$prevFileTs = $prevFile->getMTime();
		$toSuppress = false;
		
		//limite la boucle
		if ($it->key() > 100000){
			echo "Max loop is reach " . $it->key() . PHP_EOL;
			break;
		}
		
		//echo $prevFilePath .'-'. $it->key() . PHP_EOL;
		
		if( is_file($lastFilePath) && is_file($prevFilePath) ){
			//echo "Previous file " .$prevFilePath . PHP_EOL;
			//echo "Last file " .$lastFilePath . PHP_EOL;
			
			$lastFile = new SplFileInfo($lastFilePath);
			$lastFileTs = $lastFile->getMTime();
			if($lastFileTs >= $prevFileTs){
				$toSuppress = $prevFilePath;
			}
			else{
				$prevFileTs = date ("m d Y H:i:s", $prevFileTs);
				$lastFileTs = date ("m d Y H:i:s", $lastFileTs);
				$msg = "The previous file $prevFilePath is more recent ($prevFileTs) that the last file $lastFilePath ($lastFileTs)" . PHP_EOL;
				file_put_contents('/var/log/rbmockup/abnormalDoublons.log', $msg, FILE_APPEND);
				//echo $msg;
			}
			
			if($toSuppress){
				echo 'Suppress obsolete file ' . $toSuppress . PHP_EOL;
				$size = $prevFile->getSize();
				$ok = unlink($toSuppress);
				if(!$ok){
					echo 'Unable to suppress file ' . $toSuppress . PHP_EOL;
				}
				else{
					$saved = $saved + $size;
					$count++;
				}
			}
		}
	}
	return array('saved'=>$saved, 'count'=>$count);
}
