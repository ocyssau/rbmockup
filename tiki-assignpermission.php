<?php

// $Header: /cvsroot/ranchbe/ranchbe/tiki-assignpermission.php,v 1.4 2007/11/19 18:27:03 ranchbe Exp $

// Copyright (c) 2002-2005, Luis Argerich, Garland Foster, Eduardo Polidor, et. al.
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.

// This script is used to assign permissions to a particular group
// ASSIGN PERMISSIONS TO GROUPS

//Setup
require_once './conf/ranchbe_setup.php';

//Require function to administrate user and group permissions
require_once './lib/userslib.php';
$area_id = $userlib->AREA_ID;
$smarty->assign('area_id', $area_id);

//Check if a group id is send by user.
if (!isset($_REQUEST["group"])) {
	$smarty->assign('msg', tra("Unknown group"));
	$smarty->display("error.tpl");
	die;
}

//$group is the group id
$group = $_REQUEST["group"];

//Check if group exist
if (!$userlib->group_exists($group, $LUA)) {
	$smarty->assign('msg', tra("Group doesnt exist"));
	$smarty->display("error.tpl");
	die;
}

$smarty->assign_by_ref('group', $group);

if (isset($_REQUEST['allper'])) {
	check_ticket('group_assign_perm' , $area_id);
	if ($_REQUEST['oper'] == 'assign') {
		$userlib->assign_level_permissions($group, $_REQUEST['level']);
		//$logslib->add_log('perms',"assigned all perms level ".$_REQUEST['level']." to group $group");
	} else {
		$userlib->remove_level_permissions($group, $_REQUEST['level']);
		//$logslib->add_log('perms',"unassigned all perms level ".$_REQUEST['level']." from group $group");
	}
}

//Process request actions
if (isset($_REQUEST["action"])) {

  //Assign a permission to a group
	if ($_REQUEST["action"] == 'assign') {
  	check_ticket('group_assign_perm' , $area_id);
		$userlib->assign_permission_to_group($_REQUEST["perm"], $group);
		//$logslib->add_log('perms',"assigned perm ".$_REQUEST['perm']." to group $group_name");
	}

  //Remove a permission from a group
	if ($_REQUEST["action"] == 'remove') {
  	check_ticket('group_assign_perm' , $area_id);

		if ($feature_ticketlib2 != 'y' or (isset($_POST['daconfirm']) and isset($_SESSION["ticket_$area"]))) {
			$userlib->remove_permission_from_group($_REQUEST["permission"], $group);
			$logslib->add_log('perms',"unassigned perm ".$_REQUEST['permission']." from group $group");
		} else {
			key_get($area);
		}
	}
}

//Update the permissions
//$perm content id right to set actif
//$permName content all right id
if (isset($_REQUEST['update'])) {
	check_ticket('group_assign_perm' , $area_id);
	foreach (array_keys($_REQUEST['permName'])as $per) {

		if (isset($_REQUEST['perm'][$per])) {
			$userlib->assign_permission_to_group($per, $group, $LUA);
		} else {
			$userlib->remove_permission_from_group($per, $group, $LUA);
		}
		//$logslib->add_log('perms',"changed perms for group $group");
	}
}

//Process the Sort and Limition items to display
  // If offset is set use it if not then use offset =0
  // use the maxRecords php variable to set the limit
  // if sortMode is not set then use lastModif_desc

  //Process the sort request
    if (!isset($_REQUEST["sort_field"]) || empty($_REQUEST["sort_field"]) ) {
    	$sort_field = 'area_id';    //Default value of the field to sort
    	$sort_order = 'ASC'; //Default value of the order ASC = ascendant, DESC = descandant
    } else {
    	$sort_field = $_REQUEST["sort_field"]; //request sort mode value
    	$sort_order = $_REQUEST["sort_order"]; //request sort mode value
    }
    $smarty->assign_by_ref('sort_order', $sort_order);
    $smarty->assign_by_ref('sort_field', $sort_field);

  //Offset of rows to display for pagination
    if (!isset($_REQUEST["offset"])) {
    	$offset = 0; //Default value
    } else {
    	$offset = $_REQUEST["offset"]; //Offset request value
    }
    $smarty->assign_by_ref('offset', $offset);

  //Limitation of number rows to display
    $maxRecords = 9999;
    if (!isset($_REQUEST["numrows"]) || empty($_REQUEST["numrows"])) {
    	$numrows = $maxRecords; //Default value
    } else {
    	$numrows = $_REQUEST["numrows"]; // Max rows to display request value
    }
    $smarty->assign_by_ref('numrows', $numrows);

  //Name filter
    if (isset($_REQUEST["find"])) {
    	$find = $_REQUEST["find"];  //Request value
    } else {
    	$find = ''; //Default value
    }
    $smarty->assign('find', $find);

  //Types filter
    if (!isset($_REQUEST["type"])) {
    	$_REQUEST["type"] = '';
    }
    $smarty->assign('type', $_REQUEST["type"]);

//Get the permissions of the group
$group_perms = $userlib->get_group_permissions($group, $LUA);
$smarty->assign('group_perms', $group_perms);

//Get all permissions

$area_id = $_REQUEST['area_id'];
$smarty->assign('area_id', $area_id);

$list = $userlib->get_permissions($offset, $maxRecords, $sort_field, $sort_order, "$find", $_REQUEST["type"], $LUA , $area_id);
$smarty->assign_by_ref('perms', $list);
//var_dump($list);

//Get all groups of system
$groups = $userlib->get_groups('0', '9999', 'group_define_name', 'ASC', ' ', $LUA);
$smarty->assign('groups', $groups);

// Get infos about current selected group
$group_info = $userlib->get_group_info($group, $LUA);
$smarty->assign_by_ref('group_info', $group_info);

//Get all area
$areas = $userlib->get_area();
$smarty->assign_by_ref('areas', $areas);

//Include generic definition of the code for manage the pagination. $list must set...
include('paginationManager.php');

// disallow robots to index page:
$smarty->assign('metatag_robots', 'NOINDEX, NOFOLLOW');

// Active the tab
$smarty->assign('usersTab', 'active');
// Display the template
$smarty->assign('mid', 'tiki-assignpermission.tpl');
$smarty->display("ranchbe.tpl");

?>
