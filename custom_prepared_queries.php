<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Ranchbe is distributed in the hope that it will be useful,                |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Ranchbe; if not, write to the Free Software                    |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//Setup
require_once './conf/ranchbe_setup.php';

$userName = $usr->getProperty('handle');
$smarty->assign('userName', $userName);

$path_queries_scripts = './modules/custom_queries/';

//Construct the form with QuickForm lib
require_once "HTML/QuickForm.php"; //Librairy to easily create forms
require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm

$form = new HTML_QuickForm('form', 'post');

$files = glob($path_queries_scripts . "*.php");
foreach ($files as $filename) $list[basename($filename)] = basename(basename($filename), '.php');
$select =& $form->addElement('select', 'statistics_queries', tra('Queries'), $list );
$select->setSize(10);
$select->setMultiple(true);

//Add hidden fields
$form->addElement('hidden', 'container_id', $_REQUEST['container_id']);
$form->addElement('hidden', 'action', 'getStats');

//Reset and submit
$form->addElement('reset', 'reset', 'reset');
$form->addElement('submit', 'submit', 'Display');
if ($form->validate()) {
  $graphs = array();
  foreach ($form->getElementValue('statistics_queries') as $script){
    include $path_queries_scripts . '/' . $script;
    $graphs = array_merge(call_user_func(basename($script, '.php')) , $graphs);
  }

} //End of validate form
//Set the renderer for display QuickForm form in a smarty template
include 'QuickFormRendererSet.php';

//Assign name to particular fields
$smarty->assign('graphs', $graphs);
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number' );
$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description' );
$smarty->assign('CONTAINER_STATE' , $Manager->SPACE_NAME . '_state' );
$smarty->assign('CONTAINER_ID' , $Manager->SPACE_NAME . '_id' );

$smarty->assign('PageTitle' , 'Stats of ' . $Manager->SPACE_NAME );


// Display the template
$smarty->assign('accueilTab', 'active');
$smarty->assign('mid', 'custom_prepared_queries.tpl');
$smarty->display('ranchbe.tpl');

?>
