<?php
/**
 * Advanced usage of HTML_AJAX_Server
 * Allows for a single server to manage exporting a large number of classes without high overhead per call
 * Also gives a single place to handle setup tasks especially useful if session setup is required
 *
 * The server responds to ajax calls and also serves the js client libraries, so they can be used directly from the PEAR data dir
 * 304 not modified headers are used when server client libraries so they will be cached on the browser reducing overhead
 *
 * @category   HTML
 * @package    AJAX
 * @author     Joshua Eichorn <josh@bluga.net>
 * @copyright  2005 Joshua Eichorn
 * @license    http://www.opensource.org/licenses/lgpl-license.php  LGPL
 * @version    Release: 0.4.0
 * @link       http://pear.php.net/package/HTML_AJAX
 */
 // $Id: auto_server.php,v 1.3 2008/11/14 09:35:01 ranchbe Exp $

require_once('HTML/AJAX/Server.php');
require_once('conf/ranchbe_setup.php');

// extend HTML_AJAX_Server creating our own custom one with init{ClassName} methods for each class it supports calls on
class LiveServer extends HTML_AJAX_Server {
	var $initMethods = true;

	// init method for the livesearch class, includes needed files an registers it for ajax
	function initLivesearch() {
	  global $dbranchbe;
		include 'inc/livesearch/livesearch.class.php';
		$this->registerClass(new livesearch($dbranchbe));
	}

} //end of class

// create an instance of our test server
$server = new LiveServer();

// you can use HTML_AJAX_Server to deliver your own custom javascript libs, when used with comma seperated client lists you can
// use just one javascript include for all your library files
// example url: auto_server.php?client=auto_server.php?client=Util,Main,Request,HttpClient,Dispatcher,Behavior,customLib
$server->registerJSLibrary('QfLiveSearch','live.js', 'inc/livesearch/');

// handle requests as needed
$server->handleRequest();
?>
