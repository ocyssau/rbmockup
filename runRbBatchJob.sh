#!/bin/bash
cd /var/www/rbmockup

echo "==========================================================================" >> /var/log/rbmockup/runRbBatchJob.log
echo "BEGIN OF RUN BATCH IMPORT" >> /var/log/rbmockup/runRbBatchJob.log
date >> /var/log/rbmockup/runRbBatchJob.log
echo "==========================================================================" >> /var/log/rbmockup/runRbBatchJob.log
php /var/www/rbmockup/batchImport.php >> /var/log/rbmockup/runRbBatchJob.log

#echo "==========================================================================" >> /var/log/rbmockup/runRbBatchJob.log
#echo "START INIT USER, GROUPS AND MOD" >> /var/log/rbmockup/runRbBatchJob.log
#date >> /var/log/rbmockup/runRbBatchJob.log
#echo "==========================================================================" >> /var/log/rbmockup/runRbBatchJob.log
#chown -R caoadmin:caomeca /MAQUETTE/Ranchbe_mockup/*
#chmod -R 755 /MAQUETTE/Ranchbe_mockup/*

echo "==========================================================================" >> /var/log/rbmockup/runRbBatchJob.log
echo "BEGIN OF OBSOLETE FILES CLEANNING" >> /var/log/rbmockup/runRbBatchJob.log
date >> /var/log/rbmockup/runRbBatchJob.log
echo "==========================================================================" >> /var/log/rbmockup/runRbBatchJob.log
php /var/www/rbmockup/batchCleanDoublonsVault.php >> /var/log/rbmockup/runRbBatchJob.log

#echo "==========================================================================" >> /var/log/rbmockup/runRbBatchJob.log
#echo "CLEAN TRASH" >> /var/log/rbmockup/runRbBatchJob.log
#date >> /var/log/rbmockup/runRbBatchJob.log
#echo "==========================================================================" >> /var/log/rbmockup/runRbBatchJob.log
#find /RANCHBE_TRASH -mtime +3 -exec \rm {} \; >> /var/log/rbmockup/runRbBatchJob.log

#echo "==========================================================================" >> /var/log/rbmockup/runRbBatchJob.log
#echo "BACKUP OF DB" >> /var/log/rbmockup/runRbBatchJob.log
#date >> /var/log/rbmockup/runRbBatchJob.log
#echo "==========================================================================" >> /var/log/rbmockup/runRbBatchJob.log 
#tar zcvf /SHARES/SIER/SCRIPTS/data/backup/rbmockupdb_raw.tar.z /mydatabase/mysql/ranchbe >> /var/log/rbmockup/runRbBatchJob.log

#echo "==========================================================================" >> /var/log/rbmockup/runRbBatchJob.log
#echo "START RECHERCHE DES DOUBLONS mockupDoublefile.sh" >> /var/log/rbmockup/runRbBatchJob.log
#date >> /var/log/rbmockup/runRbBatchJob.log
#echo "==========================================================================" >> /var/log/rbmockup/runRbBatchJob.log
#/var/www/rbmockup/mockupDoublefile.sh >> /var/log/rbmockup/runRbBatchJob.log

echo "==========================================================================" >> /var/log/rbmockup/runRbBatchJob.log
echo "END" >> /var/log/rbmockup/runRbBatchJob.log
date >> /var/log/rbmockup/runRbBatchJob.log
echo "==========================================================================" >> /var/log/rbmockup/runRbBatchJob.log




