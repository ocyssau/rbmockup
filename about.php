<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>About RanchBE</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <link href="styles/jalist.css" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="img/ranchbe_logo_20.png" />
</head>
<body>

<?php
//Setup
require_once './conf/ranchbe_setup.php';
?>

<table border=0>
<tr>
  <td><img src="./img/ranchBe_logo_190.png"></td>
  <td><h1 class="pagetitle">About RanchBE</h1>
  <b>RanchBE&#169;</b> <?php print(RANCHBE_VER.' '.RANCHBE_BUILD) ?><a href="http://www.ranchbe.com">www.ranchbe.com</a>
  <p>
    <b>RanchBE&#169;</b> is free software, you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    any later version.
  </p>
  <p>
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY, without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  </p>
  <p>
	<a href="./licence.txt" title="licence">See the GNU General Public License for more details.</a>
  </p>  

</td></tr>
</table>
<div id="power" style="text-align: center">
	<a href="http://www.php.net/" title="PHP"><img style="border: 0; vertical-align: middle" alt="{tr}Powered by{/tr} PHP" src="img/php.png" /></a>
	<a href="http://smarty.php.net/" title="Smarty"><img style="border: 0; vertical-align: middle" alt="{tr}Powered by{/tr} Smarty" src="img/smarty.gif"  /></a>
	<a href="http://adodb.sourceforge.net/" title="ADOdb"><img style="border: 0; vertical-align: middle" alt="{tr}Powered by{/tr} ADOdb" src="img/adodb.png" /></a>
	<a href="http://www.w3.org/Style/CSS/" title="CSS"><img style="border: 0; vertical-align: middle" alt="{tr}Made with{/tr} CSS" src="img/css1.png" /></a>
  <br />
	<a href="http://tikiwiki.org/" title="TikiWiki">This software reused code from TikiWiki. Thanks to TikiWiki staff</a>
  <br />
</div>


<form name="close">
  <input type="button" onclick="window.close()" value="Close">
</form>

</body>
