<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//Setup
//set_include_path(get_include_path() . ";c:\php\pear");
require_once './conf/ranchbe_setup.php';
require_once "HTML/QuickForm.php"; //Librairy to easily create forms
require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm
require_once './class/partner.php'; //Class to manage the partners

$Manager = new partner(); //Create new manager

$area_id = $Manager->AREA_ID;

if ($_REQUEST['action'] == 'csvimport'){
  check_ticket ( 'partner_create' , $area_id );
  $Manager->ImportPartnerCsv($_FILES['csvlist']['tmp_name'] , $_REQUEST['overwrite'] );
  unset($_REQUEST['action']);
}

//Construct the form with QuickForm lib
$form = new HTML_QuickForm('form', 'post');

//Set defaults values of elements if modify request
  if ($_REQUEST['action'] == 'modify'){
      check_ticket ('partner_modify' , $area_id);

      //Get infos
      $Infos = $Manager->GetPartnerInfos($_REQUEST['partner_id']);

      $form->setDefaults(array(
        'partner_number' => $Infos['partner_number'],
        'partner_type' => $Infos['partner_type'],
        'first_name' => $Infos['first_name'],
        'last_name' => $Infos['last_name'],
        'adress' => $Infos['adress'],
        'city' => $Infos['city'],
        'zip_code' => $Infos['zip_code'],
        'phone' => $Infos['phone'],
        'cell_phone' => $Infos['cell_phone'],
        'mail' => $Infos['mail'],
        'web_site' => $Infos['web_site'],
        'activity' => $Infos['activity'],
        'company' => $Infos['company'],
        'fsCloseDate'    => array('d'=>$fsCloseDate[d], 'm'=>$fsCloseDate[M], 'Y'=>$fsCloseDate[Y]),
        'submit'  => 'modify',
      ));

      //Add hidden fields
      $form->addElement('hidden', 'partner_id', $_REQUEST['partner_id']);
      $form->addElement('hidden', 'action', 'modify');
      //Assign var 'action' to smarty to manage display
      $smarty->assign('action', 'modify');
  }

//Set defaults values of elements if create request
  if ($_REQUEST['action'] == 'create'){
      check_ticket ('partner_create' , $area_id);

      $form->setDefaults(array(
        'submit'  => 'create',
      ));

      //Add hidden fields
       $form->addElement('hidden', 'action', 'create');

      //Assign var 'action' to smarty to manage display
       $smarty->assign('action', 'create');
  }

//Add fields for input informations in all case
  $form->addElement('text', 'partner_number', tra('Number') ,array('readonly', 'disabled', 'value'=>$Infos['partner_number'], 'size'=>32));
  $form->addElement('text', 'first_name', tra('first_name'));
  $form->addElement('text', 'last_name', tra('last_name'));
  $form->addElement('text', 'adress', tra('adress'));
  $form->addElement('text', 'city', tra('city'));
  $form->addElement('text', 'zip_code', tra('zip_code'));
  $form->addElement('text', 'phone', tra('phone'));
  $form->addElement('text', 'cell_phone', tra('cell_phone'));
  $form->addElement('text', 'fax', tra('fax'));
  $form->addElement('text', 'mail', tra('mail'));
  $form->addElement('text', 'web_site', tra('web_site'));
  $form->addElement('text', 'activity', tra('activity'));
  $form->addElement('text', 'company', tra('company'));

  //Construct array for selection set
  $SelectSet[NULL] = ''; //Leave a blank option for default none selected
  $SelectSet['customer'] = tra('customer');
  $SelectSet['supplier'] = tra('supplier');
  $SelectSet['staff'] = tra('staff');
  $select =& $form->addElement('select', 'partner_type' , tra('partner_type'), $SelectSet );

  $form->addElement('reset', 'reset', 'reset');
  $form->addElement('submit', 'submit', 'Go');

  //$smarty->assign('number_help', DEFAULT_PARTNER_MASK_HELP);

//Add validation rules to check input data
  $form->addRule('partner_type', tra('Partner type is required'), 'required');
  $form->applyFilter('__ALL__', 'trim');
  $form->applyFilter('last_name', 'mb_strtoupper');
  $form->applyFilter('first_name', 'mb_strtolower');
  $form->applyFilter('first_name', 'ucfirst');
  $form->applyFilter('partner_number', 'mb_strtoupper');
  $form->applyFilter('partner_number', '$Manager::no_accent');

  //$mask = DEFAULT_PARTNER_MASK;
  //$form->addRule('partner_number', tra('This number is not valid'), 'regex', "/$mask/" , 'server');

// Try to validate the form
if ($form->validate()) {
  $form->freeze(); //and freeze it

  //Get the forms values
  /*
  $proj[partner_id]   = $form->getSubmitValue('partner_id');
  $proj[partner_number]   = $form->getSubmitValue('partner_number');
  $proj[first_name]   = $form->getSubmitValue('first_name');
  $proj[last_name]   = $form->getSubmitValue('last_name');
  $proj[adress]   = $form->getSubmitValue('adress');
  $proj[city]   = $form->getSubmitValue('city');
  $proj[postal_code]   = $form->getSubmitValue('postal_code');
  $proj[phone]   = $form->getSubmitValue('phone');
  $proj[mail]   = $form->getSubmitValue('mail');
  $proj[web_site]   = $form->getSubmitValue('web_site');
  $proj[activity]   = $form->getSubmitValue('activity');
  */

// Form is validated, then processes the modify request
  if ($_REQUEST[action] == 'modify'){
    check_ticket ('partner_modify' , $area_id);
    $form->process('modifyProcess', true); //
  }

// Form is validated, then processes the create request
  if ($_REQUEST[action] == 'create'){
    check_ticket ('partner_create' , $area_id);
    $form->process('createProcess', true); //
  }
} //End of validate form

function modifyProcess($values){
  global $Manager;
  global $smarty;
  $values['partner_number'] = $Manager->composeNumber($values['first_name'] , $values['last_name']);
  $Manager->PartnerUpdate($values, $_REQUEST['partner_id']);
  $smarty->assign('action', 'modify'); //Assign var 'action' to smarty to manage display
}

function createProcess($values){
  global $Manager;
  global $smarty;
  $partner_id = $Manager->PartnerCreate($values);
  $smarty->assign('action', 'create'); //Assign var 'action' to smarty to manage display
}

//Set the renderer for display QuickForm form in a smarty template
include 'QuickFormRendererSet.php';

$Manager->error_stack->checkErrors();

// Display the template
$smarty->assign('partnersTab', 'active');
$smarty->assign('mid', 'partnerManage.tpl');
$smarty->display('partnerManage.tpl');
//$smarty->display('ranchbe.tpl');

?>
