<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once './conf/ranchbe_setup.php';
require_once './class/common/container.php'; //Class to manage the bookshop
require_once './class/Document/Import/RbXmlTree.php'; //Class to manage the importations

if(!isset($_REQUEST['space']))
  $_REQUEST['space'] = $_SESSION['SelectedContainerType'];

$container_id = $_REQUEST['SelectedContainer'];
$space_name = $_REQUEST['space'];

$Manager = container::_factory($space_name, $container_id); //Create new manager
$space =& $Manager->space;

/*
if (isset( $_SESSION['area_id']) && is_numeric($_SESSION['area_id'])){
	$area_id             = $_SESSION['area_id'];
}
else{
	$area_id             = $Manager->AREA_ID;
}
if (isset( $_SESSION['SelectedProjectNum']) ){
	$selected_project    = $_SESSION['SelectedProjectNum'];	
}
if ( isset ( $_SESSION['SelectedProject']) ){
	$selected_project_id    = $_SESSION['SelectedProject'];
}
*/

//Assign name to particular fields
$smarty->assign( 'container_number' , $Manager->GetProperty('container_number'));
$smarty->assign( 'action' , $_REQUEST['action']);
$smarty->assign( 'container_id' , $container_id);
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number');
$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description');
$smarty->assign('CONTAINER_STATE' , $Manager->SPACE_NAME . '_state');
$smarty->assign('CONTAINER_ID' , $Manager->SPACE_NAME . '_id');

ini_set('display_errors' , 1);


//-----------------------------------------------------------------------------------
//Manage actions wich require at last one selection
switch($_REQUEST['action']){

	//---------------------------------------------------------------------
	case 'visualize':   // Step 2 select the cvs file and display it for validation and check it
		$smarty->assign( 'step' , 'visu');
		$smarty->assign( 'step_description' , 'Validate the xml file');

		if(!is_file($_FILES['rbxmltreefile']['tmp_name'])){
			$list = glob($Manager->WILDSPACE . '/*.CATProduct_xmlPBS.xml');
			//var_dump($list);die;
			$smarty->assign_by_ref( 'list' , $list );
			$smarty->display('rbxmlImportSelect.tpl');
			die;
		}

		if( !is_dir($Manager->WILDSPACE . '/.rbetmp/') ){
			if(!mkdir($Manager->WILDSPACE . '/.rbetmp/')){
				throw new Exception('cant create tmpdir');
			}
		}

		$xmlfile = $Manager->WILDSPACE . '/.rbetmp/' . basename($_FILES['rbxmltreefile']['tmp_name']);
		move_uploaded_file($_FILES['rbxmltreefile']['tmp_name'], $xmlfile);
		$XmlTree = new Document_Import_RbXmlTree($xmlfile, $space_name);
		$list = $XmlTree->toArray();
		
		$smarty->assign_by_ref( 'list' , $list );
		$smarty->assign( 'xmlfile' , $xmlfile );
		$smarty->assign( 'rootProductName' , $XmlTree->getRootNode()->getAttribute('Name') );
		
		//Construct a array with the header of each cols
		$smarty->assign( 'fields' , array_keys($list[0]) );
		$smarty->display('rbxmlImportVisualize.tpl');
		die;
		break;

	case 'batchrun':   // Step 4 Create the documents
		foreach($_REQUEST['files'] as $xmlfile){
			echo '<p>Treatment of ' . $xmlfile. '...</p>';
			$XmlTree = new Document_Import_RbXmlTree($xmlfile, $space_name);
			$list = $XmlTree->run();
		}
		var_dump($XmlTree->errors);
		echo '<p>End</p>';
		die;
		
		//---------------------------------------------------------------------
	case 'run':   // Step 4 Create the documents
		check_ticket( 'container_document_manage' , $area_id );

		set_time_limit(24*3600); //To increase default TimeOut.No effect if php is in safe_mode
		$start_time = time();

		$errors = array(); //init the errors var
		$feedbacks = array(); //init the feedbacks var
		
		$smarty->assign( 'step' , 'run');
		$smarty->assign( 'step_description' , 'Run the importation');
		
		$xmlfile = $_REQUEST['xmlfile'];
		$smarty->assign( 'xmlfile' , $xmlfile);
		
		try{
			$XmlTree = new Document_Import_RbXmlTree($xmlfile, $space_name);
			$list = $XmlTree->run();
			require_once './lib/Date/date.php';
			$end_time = time();
			$body.= '<b>To container : </b>'.$Manager->GetProperty('container_number').'<br/>';
			$body.= '<b>By : </b>'.$Manager->importUserName.'<br/>';
			$body.= '<b>start to : </b>'.formatDate($start_time).'<br/>';
			$body.= '<b>end to : </b>'.formatDate($end_time).'<br/>';
			$body.= '<b>duration : </b>'.($end_time - $start_time).' sec<br/>';
			foreach($feedbacks as $feedback){
				$body.= '<font color="green">'.$feedback.'</font><br />';
			}
			foreach($errors as $error){
				$body.= '<font color="red"><b>ERROR : </b>'.$error.'</font><br />';
			}
			
			// Write a out trace file
			date("y/m/d H:i:s").'- :<br/>';
			$handle = fopen( $Manager->WILDSPACE . '/.rbetmp/importRbXml.out' , 'a+' );
			fwrite($handle , '================================================='."\n");
			fwrite($handle , date("y/m/d H:i:s")."\n");
			fwrite($handle , strip_tags(str_replace('<br/>',"\n",$subject.'<br/>')));
			fwrite($handle , strip_tags(str_replace('<br/>',"\n",$body)));
			fclose($handle);
			
			$smarty->assign_by_ref( 'list' , $list );
			//$smarty->assign( 'fields' , array_keys($list[0]) );
			$smarty->assign( 'fields' , array_keys( current($list) ) );
			$smarty->assign( 'xmlfile' , $xmlfile );
			
			$smarty->assign( 'step' , 'END');
			$smarty->assign( 'step_description' , 'End of batch importation');
			$smarty->assign( 'body' , $body);
		}catch(Exception $e){
			$feedback = $e->getTraceAsString();
		}
		
		break;
		
} //End of switch

//Include generic definition of the code for manage filters
//include('filterManager.php');

$Manager->error_stack->checkErrors();

// Display the template
$_SESSION['myspace'] = array('activePage'=>$_SERVER['PHP_SELF']);
$smarty->assign('documentManage' , 'active');
$smarty->assign('mid', 'rbxmlImportSuccess.tpl');
$smarty->display('ranchbe.tpl');

