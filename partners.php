<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//Setup
//set_include_path(get_include_path() . ";c:\php\pear");
require_once './conf/ranchbe_setup.php';
require_once "HTML/QuickForm.php"; //Librairy to easily create forms
require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm
require_once './class/partner.php'; //Class to manage the partners

$Manager = new partner(); //Create new manager

$area_id = $Manager->AREA_ID;

//Suppress
if ($_REQUEST['action'] == 'suppress' && !empty($_REQUEST["checked"])) {
  check_ticket( 'partner_suppress' , $area_id);
  foreach ($_REQUEST["checked"] as $del_id) {
 	$Manager->PartnerSuppress($del_id);
  }
}

//Export
if ($_REQUEST['action'] == 'export') {
	header("Content-type: application/csv ");
  header("Content-Disposition: attachment; filename=export_partners.csv ");
  $list = $Manager->GetAllPartners();
  $smarty->assign('list', $list);
	$smarty->display("./partners_export.tpl");
	die;
}

//Include generic definition of the code for manage filters
$default_sort_field='partner_number';    //Default value of the field to sort
$default_sort_order='ASC';    //Default value of the order ASC = ascendant, DESC = descandant
include('filterManager_simple.php');

//get all partners
  $list = $Manager->GetAllPartners($params);
  $smarty->assign_by_ref('list', $list);

//Include generic definition of the code for manage the pagination. $list must be set...
include('paginationManager.php');

//Define select option for "find"
$all_field = array ('partner_number' => 'Number',
                    'first_name' => 'first_name',
                    'last_name' => 'last_name',
                    'partner_type' => 'partner_type',
                    'adress' => 'adress',
                    'city' => 'city' ,
                    'zip_code' => 'zip_code',
                    'phone' => 'phone',
                    'cell_phone' => 'cell_phone',
                    'mail' => 'mail',
                    'web_site' => 'web_site',
                    'activity' => 'activity',
                    'company' => 'company',
                    );

$smarty->assign('all_field', $all_field);

$Manager->error_stack->checkErrors();

// Active the tab
  $smarty->assign('partnersTab', 'active');
// Display the template
  $smarty->assign('mid', 'partners.tpl');
  $smarty->display("ranchbe.tpl");
?>
