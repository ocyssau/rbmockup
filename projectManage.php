<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//Todo :
//ajouter des regles de validation des numeros de projet
//Revoir dates

//set_include_path(get_include_path() . ";c:\php\pear");
require_once './conf/ranchbe_setup.php';
require_once "HTML/QuickForm.php"; //Librairy to easily create forms
require_once 'HTML/QuickForm/Renderer/ArraySmarty.php'; //Lib to use Smarty with QuickForm
require_once './class/project.php'; //Class to manage the projects
require_once './lib/Date/date.php';

//Load the right definition file
//load_rights_def();

$projectManager = project::_factory();
$area_id = $projectManager->AREA_ID;

//Construct the form with QuickForm lib
 $form = new HTML_QuickForm('form', 'post');

//Set defaults values of elements if modify request
  if ($_REQUEST['action'] == 'modify'){
      check_ticket( 'project_create' , $area_id);

      //Get the project infos
      $Infos = $projectManager->GetProjectInfos($_REQUEST['project_id']);

      $fsCloseDate = $Infos['forseen_close_date'];

      //Set defaults values
      $form->setDefaults( array (
        'project_id'  => $Infos['project_id'],
        'project_description'  => $Infos['project_description'],
        'project_number'  => $Infos['project_number'],
        'submit'  => 'modify',
      ));

      //Add hidden fields
      $form->addElement('hidden', 'project_id', 'project_id');
      $form->addElement('hidden', 'action', 'modify');
      //Assign var 'action' to smarty to manage display
      $smarty->assign('action', 'modify');
  }

//Set defaults values of elements if create request
  if ($_REQUEST['action'] == 'create'){
      check_ticket( 'project_create' , $area_id);

      $fsCloseDate = time() + ( 3600 * 24 * $DEFAULT_LIFE_TIME );
      $form->setDefaults(array(
        'project_description'  => 'New project',
        'submit'  => 'create',
      ));

      //Add hidden fields
      $form->addElement('hidden', 'action', 'create');
      //Assign var 'action' to smarty to manage display
      $smarty->assign('action', 'create');
  }

//Add fields for input informations in all case
  $form->addElement('text', 'project_number', tra('Number'));
  $form->addElement('textarea', 'project_description', tra('Description'), array('col'=>30));

  $smarty->assign('number_help', DEFAULT_PROJECT_MASK_HELP);

  require_once('./GUI/GUI.php');
  //Contruct date select field
  $date_params = array(
            'field_name' => 'forseen_close_date',
            'default_value' => $fsCloseDate,
            'field_required' => true,
  );
  construct_select_date($date_params , $form);


  $form->addElement('reset', 'reset', 'reset');
  $form->addElement('submit', 'submit', 'Go');

//Add validation rules to check input data
  $form->addRule('fsCloseDate', tra('fsCloseDate is required'), 'required');
  $form->addRule('fsCloseDate', tra('fsCloseDate must be a date'), 'date');
  $form->addRule('project_number', tra('Project number is required'), 'required');
  $mask = DEFAULT_PROJECT_MASK;
  $form->addRule('project_number', 'This number is not valid', 'regex', "/$mask/" , 'server');
  $form->addRule('project_description', 'Project description is required', 'required');

// Try to validate the form
if ($form->validate()) {
  $form->freeze(); //and freeze it

  $form->setConstants(array( //Re-set value for display correct date in freeze form
   'fsCloseDate' => $form->getSubmitValue('fsCloseDate'),
  ));

// Form is validated, then processes the modify request
  if ($_REQUEST['action'] == 'modify'){
    check_ticket( 'project_create' , $area_id);
    $form->process('modifyPROJ', true);
  }

// Form is validated, then processes the create request
  if ($_REQUEST['action'] == 'create'){
    check_ticket( 'project_create' , $area_id);
    $form->process('createPROJ', true);
  }
} //End of validate form

function modifyPROJ($values){

  global $projectManager;
  global $smarty;

  if(!is_numeric($values['forseen_close_date']))
    $values['forseen_close_date'] = getTSFromDate( $values['forseen_close_date'] );

  $projectManager->Update($values, $_REQUEST['project_id']);

  //Assign var 'action' to smarty to manage display
  $smarty->assign('action', 'modify');
}

function createPROJ($values){

  global $projectManager;
  global $smarty;

  if(!is_numeric($values['forseen_close_date']))
    $values['forseen_close_date'] = getTSFromDate( $values['forseen_close_date'] );

  $projectManager->Create($values);

  //Assign var 'action' to smarty to manage display
  $smarty->assign('action', 'create');
}

//Set the renderer for display QuickForm form in a smarty template
include 'QuickFormRendererSet.php';

$projectManager->error_stack->checkErrors();

// Display the template
$smarty->assign('projectsTab', 'active');
$smarty->assign('mid', 'projectManage.tpl');
$smarty->display('projectManage.tpl');
//$smarty->display('ranchbe.tpl');

?>
