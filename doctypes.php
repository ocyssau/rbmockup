<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//Setup
//set_include_path(get_include_path() . ";c:\php\pear");
require_once './conf/ranchbe_setup.php';

require_once './class/doctype.php'; //Class to manage the doctypes
$Manager = new doctype; //Create new manager

$area_id = 1;

//Manage actions
if (!empty($_REQUEST["action"]) && !empty($_REQUEST["checked"])) {
  //Suppress
  if ($_REQUEST['action'] == 'suppress' ) {
    check_ticket( 'doctype_suppress'  , $area_id );

    foreach ($_REQUEST["checked"] as $del_id) {
    	$Manager->Suppress($del_id);
    }
	}
}

//Include generic definition of the code for manage filters
$default_sort_field='doctype_number';    //Default value of the field to sort
$default_sort_order='ASC';    //Default value of the order ASC = ascendant, DESC = descandant
include('filterManager_simple.php');

//get all doctypes
  //$maxRecords
  $list = $Manager->GetAll($params);
  $smarty->assign_by_ref('list', $list);

//Include generic definition of the code for manage the pagination. $list must set...
include('paginationManager.php');

//Define select option for "find"
$all_field = array ('doctype_number' => 'Number',
                    'doctype_description' => 'Description',
                    'script_post_store' => 'script_post_store',
                    'script_pre_store' => 'script_pre_store',
                    'script_pre_update' => 'script_pre_update' ,
                    'script_post_update' => 'script_post_update',
                    'recognition_regexp' => 'recognition_regexp',
                    'file_extension' => 'file_extension',
                    'file_type' => 'file_type',
                    'can_be_composite' => 'can_be_composite',
                    );
$smarty->assign('all_field', $all_field);

$smarty->assign('icons_dir', DEFAULT_DOCTYPES_ICONS_DIR);
// Active the tab
$smarty->assign('doctypesTab', 'active');

$Manager->error_stack->checkErrors();

// Display the template
$smarty->assign('mid', 'doctypes.tpl');
$smarty->display("ranchbe.tpl");
?>
