<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//Setup
require_once './conf/ranchbe_setup.php';
require_once './class/common/container.php'; //Class to manage the container

//This form must be talk by a father to get the document name to store and the
//base_dir of the file. Get this values here
$Manager =& container::_factory($_SESSION['SelectedContainerType'] , $_SESSION['SelectedContainer']); //Create new manager
$space =& $Manager->space;

//include './inc/documentManage.php';

$Manager->RecordSelectedContainer(); //Record the selected container in the session

$area_id             = $Manager->AREA_ID;

if ( isset ( $_SESSION['SelectedProjectNum']) )
  $selected_project    = $_SESSION['SelectedProjectNum'];
if ( isset ( $_SESSION['SelectedProject']))
  $selected_project_id    = $_SESSION['SelectedProject'];

//Assign name to particular fields
$smarty->assign('CONTAINER_TYPE', $Manager->SPACE_NAME);
$smarty->assign('CONTAINER_NUMBER' , $Manager->SPACE_NAME . '_number');
$smarty->assign('CONTAINER_DESCRIPTION' , $Manager->SPACE_NAME . '_description');
$smarty->assign('CONTAINER_STATE' , $Manager->SPACE_NAME . '_state');
$smarty->assign('CONTAINER_ID' , $Manager->SPACE_NAME . '_id');

// Active the tab
$smarty->assign('documentManageScript', "$Manager->SPACE_NAME".'DocManage.php'); //Assign variable to smarty
$smarty->assign('documentManage', 'active'); //Assign variable to smarty

check_ticket('container_document_get',$area_id);

//-----------------------------------------------------------------
//Genere une ligne de formulaire dans le cas du store multiple
//Le principe est de renommer chaque champ du formulaire (y compris les champs issues des metadonnees crees par les utilisateurs)
//et d'y ajouter '[$i]' ou $i est un numero incrementale issue de la boucle de parcours de $_REQUEST['checked'].
//Le formulaire retourne alors des variables tableaux. La designation[1] correspondra au fichier[1] etc.
function SetCreateDocForm($document,$loop_id){
  global $Manager;
  global $smarty;
  global $container_id;
  
  $file = $document['file'];

  $fields = array('document_number', 'designation', 'category_id', 'document_indice_id', 'file_id', 'document_id', 'file');

  //Construct the form with QuickForm lib
  require_once('./GUI/GUI.php');
  $form = new HTML_QuickForm('form', 'post');

  $form->addElement('text', 'file['.$loop_id.']', 'File' ,array('readonly', 'value'=>$file, 'size'=>32));
  $mask = DEFAULT_DOCUMENT_MASK;
  $form->addRule('file['.$loop_id.']', 'This file name is not valid', 'regex', "/$mask/" , 'server');
  $form->addElement('text', 'document_number['.$loop_id.']', 'Document_number' , array('readonly', 'size'=>32));

  if(empty($document['designation'])) $document['designation'] = 'undefined';

  $form->setDefaults(array(
    'document_number['.$loop_id.']' => $document['document_number'],
    'designation['.$loop_id.']' => $document['designation'],
    'category_id['.$loop_id.']' => $document['category_id'],
  ));

  if( array_key_exists( 'upgrade_doc', $document['actions'] ) || array_key_exists( 'create_doc', $document['actions'] ) ){
    //Add select document indice
    $params = array(
              'field_multiple' => false,
              'field_name' => 'document_indice_id['.$loop_id.']',
              'field_description' => 'indice',
              'field_size' => '1',
              'field_required' => true,
              'default_value' => $document['document_indice_id'],
    );
    construct_select_document_indice($params , $form , $Manager);
  }

  $form->addElement('textarea', 'designation['.$loop_id.']' , 'Designation' ,array('rows'=>2,'cols'=>32));
  $form->addRule('designation['.$loop_id.']', 'Designation is required', 'required');
  $form->addRule('document_number['.$loop_id.']', 'This document name is not valid', 'regex', "/$mask/" , 'server');

  //Add select category
  $params = array(
            'field_multiple' => false,
            'field_name' => 'category_id['.$loop_id.']',
            'field_description' => 'category',
            'field_size' => '1',
            'return_name' => false,
            'default_value' => $document['category_id'],
  );
  construct_select_category($params , $form , $Manager);

  //Add select action to perform
  $params = array(
            'field_multiple' => false,
            'field_name' => 'docaction['.$loop_id.']',
            'field_description' => 'Action',
            'field_size' => '1',
            'return_name' => false,
            'default_value' => key($document['actions']), //Default value is the first key of the action array
  );
  construct_select($document['actions'] , $params , $form);

  //Get fields for custom metadata
  require_once('./class/common/metadata.php');
  $metadata = new docmetadata($Manager->space);
  $optionalFields = $metadata->GetMetadataLinked(NULL, $container_id);
  $smarty->assign( 'optionalFields' , $optionalFields);
  foreach($optionalFields as $field){
    $field['default_value'] = $document[$field['field_name']];
    $fields[] = $field['field_name'];
    $field['field_name'] = $field['field_name'] . '['.$loop_id.']';
    construct_element($field , $form); //function declared in GUI.php
  }
  $smarty->assign('fields', $fields);

  //Apply new filters to the element values
  $form->applyFilter('__ALL__', 'trim');

  //Set the renderer for display QuickForm form in a smarty template
  include 'QuickFormRendererSet.php';

} //End of function

//init wildspace
$wilspacePath = $space->getWildspace();

switch ( $_REQUEST['action'] ) {

//---------------------------------------------------------------------
  case ('Store'):   // Step 2 test and set actions
  case ('StoreMulti'):

    $smarty->assign( 'step' , '2');
    $smarty->assign( 'step_description' , 'Selection review');

    if ( count($_REQUEST['checked']) == 0 ){
      $Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select at least one file');
      $Manager->error_stack->checkErrors(array('close_button'=>true));
      break;
    }

    $tmp_list = array(); //init the var for foreach loop
    $clean_list = array(); //init the var for foreach loop
    $smarty->assign( 'displayHeader' , 1); // true for display header
    $i = 0;

    foreach($_REQUEST['checked'] as $file_name){
      $fileInfos = array(); //init var
      $docinfo = array(); //init var
      $i = $i + 1;

      //---------- Check if file exist in wildspace
      require_once './class/common/fsdata.php';
      $odata = fsdata::_dataFactory($wilspacePath.'/'.$file_name);
      if(!$odata){
        $document['errors'][] = $file_name.' is not in your Wildspace.';
        $document['actions']['ignore'] = 'Ignore';
        continue;
      }
      $file = $odata->getProperty('file');

      $odocument = $Manager->initDoc(); //init the document object from the current container
      $odocfile = $odocument->initDocfile(); //init the docfile object from current document

      if($file_id = $odocfile->GetFileId( $file_name ) ){ //- The file is recorded in database
        $odocfile->init($file_id);
        $fdoc_id = $odocfile->GetDocumentId(); //Get info about the father document
        $odocument->init($fdoc_id);
        $document = $odocument->GetDocumentInfos(); //Get the document of this file
        $document['errors'][] = 'file '.$file_name . ' exist in this database. ID=' . $file_id;
        $document['errors'][] = 'file '.$file_name . ' is linked to document '. $document['document_number'].' ID='.$fdoc_id;
        //Compare md5
        if( $odocfile->GetProperty('file_md5') == $odata->getProperty('file_md5') ) $equal_md5 = true;

        if($odocument->CheckAccess() != 0){
          $document['errors'][] = 'The document '.$document['document_number'] . ' is not free ';
        }else if( $odocfile->CheckFileAccess() != 0 ){
          $document['errors'][] = 'file '.$file_name . ' is not free ';
        }else{
          if( $equal_md5 ){
            $document['errors'][] = 'files '.$file_name.' from wildspace and from vault are strictly identicals';
            $document['actions']['update_doc'] = 'Update the document '.$document['document_number'];
          }else{
            $document['actions']['update_doc_file'] = 'Update the document '.$document['document_number'].' and files';
            $document['actions']['update_file'] = 'Update the file '.$file_name.' only';
            $document['actions']['upgrade_doc'] = 'Create a new indice of '.$document['document_number'];
          }
        }
        $document['file_id'] = $file_id;
        $document['file'] = $file_name;

      }else{//-- The file is not recorded in database
        //echo 'Generate the number from the file name<br>';
        $document['file'] = $file_name;
        $document['document_number'] = $odata->getProperty('doc_name'); //Generate the document number
        $odocument->SetDocProperty( 'document_number', $document['document_number'] );
        //Check double documents
        if(in_array($document['document_number'] , $tmp_list)){
          $document['errors'][] = $document['document_number'] . ' is not unique in this package';
          $document['actions']['add_file'] = 'Add the file to the document '.$document['document_number'];
        }else{
          $tmp_list[] = $document['document_number'];
          //Check if document exist in database
          if( $document_id = $odocument->DocumentExist( $document['document_number'] ) ){
            $document['errors'][] = 'The document '.$document['document_number'] . ' exist in this database. ID=' . $document_id;
            if($odocument->CheckAccess() !== 0){
              $document['errors'][] = 'The document '.$document['document_number'] . ' is not free ';
            }else{
              $document['actions']['add_file'] = 'Add the file to the document '.$document['document_number'];
              $document['actions']['upgrade_doc'] = 'Create a new indice of '.$document['document_number'];
            }
            $document['document_id'] = $document_id;
          }else{
            $document['actions']['create_doc'] = 'Create a new document';
          }
        }
      }

      //Check the doctype for the new document
      if(isset($document['actions']['create_doc'])){
        $fsdata = $odocfile->initFsdata($file);
        $document['doctype'] = $odocument->SetDocType($fsdata->getProperty('file_extension'), $fsdata->getProperty('file_type'));
        if(!$document['doctype']){
          $document['errors'][] = 'This document has a bad doctype';
          unset($document['actions']); //Reinit all actions
        }
      }

      //Add default actions
      $document['actions']['ignore'] = 'Ignore';
      SetCreateDocForm($document, $i);
      $smarty->assign( 'document' , $document);
      $smarty->assign( 'loop_id' , $i);
      $smarty->display('StoreMulti.tpl');
      //from now dont display header
      $smarty->assign('displayHeader' , 0);
      $document = array();

    }//End of foreach
    $smarty->assign( 'displayFooter' , 1);
    $smarty->display('StoreMulti.tpl');
  die;
  break;

//---------------------------------------------------------------------
  case ('ValidateStore'):   // Step 3 apply actions

  if ( count($_REQUEST['file']) == 0 ){
    $Manager->error_stack->push(ERROR, 'Fatal', array(), 'You must select one file');
    break;
  }

  check_ticket( 'container_document_manage' , $area_id );

  $smarty->assign( 'step' , 3);
  $smarty->assign( 'step_description' , 'Validate store');

  $errors = array(); //init the errors var
  $feedbacks = array(); //init the feedbacks var
  $ImportData = $_REQUEST;

  //Loop action and get datas
  for ($i = 0; $i <= $ImportData['loop']; $i++){
    $docaction = $ImportData['docaction'][$i];
    if( empty($docaction) ) continue;

    //Get properties of document from form
    foreach($ImportData['fields'] as $field){
      $doc_properties[$field] = $ImportData[$field][$i]; //document_number, designation, category_id, document_indice_id, document_id, file_id, file + meta-datas
    }
    $document_id = $ImportData['document_id'][$i];
    $document_indice_id = $ImportData['document_indice_id'][$i];
    $file_name = $ImportData['file'][$i]; //file name from form
    $file_id = $ImportData['file_id'][$i]; //file_id from form
    $file = $wilspacePath.'/'.$file_name; //file path from form
    unset($doc_properties['file']); // this property is normaly not a document property
    unset($doc_properties['file_id']); // this property is normaly not a document property
    unset($doc_properties['document_id']); // this property is normaly not a document property
    $odocument =& $Manager->initDoc($document_id);

    switch($docaction){
      case "ignore": //Ignore
      break;

      case "update_doc": //Update the document only
      case "update_doc_file": //Update the document and the file if specified
        unset($doc_properties['document_indice_id']); // this property is normaly not change by this way
        //Update the document record metadatas
        if( $odocument->UpdateDocInfos($doc_properties) )
          $feedbacks[] = 'the document ='.$doc_properties['document_number'].' has been updated.';
        //No break, continue on next... update file...

      case "update_doc":
      break;

      case "update_file": //Update the associated file only
        $feedbacks[] = 'update_file : '. $file_name;
        //If file is specified, update the file. The file must be stored in the wildspace
        if( !empty( $file_id ) ){
          if( $odocfile = $odocument->initDocfile($file_id) ){
            //---------- Check if file exist in wildspace
            require_once './class/common/fsdata.php';
            $odata = fsdata::_dataFactory($file);
            if($odata){ //-----update the file
              $odocument->CheckOut(false); //Checkout without files checkout
              if($odocfile->CheckOutFile(false)){
                //if($odocfile->CheckInFile(false , false , true)){
                if( $odocument->CheckIn(false, false) ){ //replace document and checkout files
                  $feedbacks[] = $file_name.' as been updated.';
                }else{
                  $errors[] = 'Can not checkin the file ='. $file_name;
                  //$odocfile->CheckInFile(false , true , true); //Try to reset the file
                }
              }else{
                $errors[] = 'Can not checkout the file ='. $file_name;
              }
            }else{
              $errors[] = 'The file '.$file_name.' is not in your Wildspace ';
            }
          }
        }
      break;

      case "add_file": //Add the file to the document
        unset($doc_properties['document_indice_id']); // this property is normaly not change by this way
      case "create_doc": //Create a new document
        require_once './class/common/fsdata.php';
        $odata = fsdata::_dataFactory($file);
        if($odata){
          if( $document_id = $odocument->DocumentExist($doc_properties['document_number']) ){ //Dont believe the id give by step 1. The document_id might have changed after an indice upgrade,
            $feedbacks[] = 'add_file : '. $file_name .' to '.$doc_properties['document_number'];
            $odocument->init($document_id);
            if( $odocument->AssociateFile($file , true) ){
              $feedbacks[] = 'the file '.$file_name.' has been associated to document '.$doc_properties['document_number'];
            }else{
              $errors[] = 'The file '.$file_name.' can not be associated to the document';
            }
          }else{ //Create the doc
            $feedbacks[] = 'create_doc : '.$doc_properties['document_number'];
            if(!isset($doc_properties['container_id'])){
              $doc_properties['container_id'] = $container_id;
            }
            $feedbacks[] = 'create_doc from: '.$element;
            $doc_properties['container_id'] = $Manager->GetId();
            $doc_properties['file'] = $file_name;
            if($odocument->Store($doc_properties))
              $feedbacks[] = 'The doc '.$element.' has been created';
            else{
              $errors[] = 'Can not create the doc '.$element;
              break;
            }
          } //End of create doc
        }else
          $errors[] = 'The file '.$file_name.' is not in your Wildspace';
      break;

      case "upgrade_doc": //Create a new indice and update the files if specifed
        if(check_ticket( 'container_document_change_indice' , $area_id , false) === false){
          $feedbacks[] = 'You have not permission to upgrade the document '.$data['document_number'];
          break;
        }
        $feedbacks[] = 'Try to upgrade document '.$doc_properties['document_number'];
        //Check access
        $accessCode = $odocument->CheckAccess();
        if($accessCode == 0){
          //Lock the current indice
          if( !$odocument->LockDocument(11) ){
            $errors[] = 'Can not lock the document '.$doc_properties['document_number'];
            break;
          }
          //Lock access to files too
          $docfiles = $odocument->GetDocfiles();
          if(is_array($docfiles))
            foreach($docfiles as $docfile){
              $docfile->LockFile(11);
            }
          $feedbacks[] = 'the document '.$doc_properties['document_number'].' has been locked.';
        }else
        if($accessCode == 1){
          $errors[] = 'This document is checkout';
          break;
        }

        //Create the new indice
        if($doc_properties['document_indice_id'] > $odocument->GetDocProperty('document_indice_id') ) 
          $new_indice_id = $doc_properties['document_indice_id'];
        else 
          $new_indice_id=NULL;
        if( $new_doc_id = $odocument->UpgradeIndice($new_indice_id, $Manager->GetId() ) ){
          $feedbacks[] = 'the document ID='.$new_doc_id.' has been created.';
          $onewDocument = new document($space, $new_doc_id);
          //Update the new indice with metadata to import
          if( !$onewDocument->UpdateDocInfos($doc_properties) ){
            $errors[] = 'Can not update document data='.$accessCode;
            break;
          }
          $feedbacks[] = 'the document ID='.$new_doc_id.' has been updated.';

          //----------------- Update the files
          //Get docfiles
          $odocfile =& $onewDocument->initDocfile();
          if($file_id = $odocfile->GetFileId( $file_name , $new_doc_id ) ){ //- Get the id of file from new document
            $odocfile->init($file_id);
            require_once './class/common/fsdata.php';
            $odata = fsdata::_dataFactory($file);
            if($odata){ //-----update the file
              $onewDocument->CheckOut(false); //Checkout document without files
              if($odocfile->CheckOutFile(false)){
                //if($odocfile->CheckInFile(false , false , true)){
                if( $onewDocument->CheckIn(false, false) ){ //replace document and checkout files
                  $feedbacks[] = $file_name.' as been updated.';
                }else{
                  $errors[] = 'Can not checkin the file ='. $file_name;
                  //$odocfile->CheckInFile(false , true , true); //Try to reset the file
                }
              }else{
                $errors[] = 'Can not checkout the file ='. $file_name;
              }
            }else{
              $errors[] = 'The file '.$file_name.' is not in your Wildspace ';
            }
          }else $errors[] = 'Can not create the new indice';
        }
      break;

    }
  } //End 'for' loop

  $smarty->assign( 'errors' , $errors);
  $smarty->assign( 'feedbacks' , $feedbacks);

  break;

} //End of switch

$Manager->error_stack->checkErrors();

// Display the template
$_SESSION['myspace'] = array('activePage'=>$_SERVER['PHP_SELF']);
$smarty->assign( 'container_number' , $Manager->GetName() );
$smarty->assign('documentManage' , 'active');
$smarty->assign('displayHeader' , 1);
$smarty->assign('displayFooter' , 1);

$smarty->display('StoreMulti.tpl');

?>
