<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Ranchbe is distributed in the hope that it will be useful,                |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Ranchbe; if not, write to the Free Software                    |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

//Setup
require_once './conf/ranchbe_setup.php';

//$_REQUEST['popup'] = 1;
//include './messu-mailbox.php';

// Get unread messages
include_once './class/messu/messulib.php';
$offset = 0;
$maxRecords = 5;
$sort_mode = 'date_desc';
$find = '';
$flag = 'isRead';
$flagval = 'n';
$orig_or_reply="r";

if (!isset($_REQUEST['replyto'])) $_REQUEST['replyto'] = '';

$items = $messulib->list_user_messages($user, $offset, $maxRecords, $sort_mode,
 	$find, $flag, $flagval, '', '', $_REQUEST['replyto'], $orig_or_reply);
$smarty->assign_by_ref('items', $items["data"]);

//Get list of received tasks
include_once './class/tasks/tasklib.php';
$sort_mode = 'priority_desc';
$tasklist = $tasklib->list_tasks(	$user,$offset ,$maxRecords, $find, $sort_mode, 
								$show_private=false, $show_submitted=false, $show_received=true, $show_shared=false,
								false, null);

$smarty->assign_by_ref('tasklist', $tasklist["data"]);

// Display the template
$smarty->assign('accueilTab', 'active');
$smarty->assign('mid', 'accueil.tpl');
$smarty->display('ranchbe.tpl');

?>
