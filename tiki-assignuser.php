<?php

// $header: /cvsroot/tikiwiki/tiki/tiki-assignuser.php,v 1.11.2.11 2006/11/02 14:35:43 sylvieg exp $

// copyright (c) 2002-2005, luis argerich, garland foster, eduardo polidor, et. al.
// all rights reserved. see copyright.txt for details and a complete list of authors.
// licensed under the gnu lesser general public license. see license.txt for details.

// this script is used to assign groups to a particular user
// assign user to groups
// Initialization
//Setup

require_once './conf/ranchbe_setup.php';
require_once './lib/userslib.php';

$area_id = 1;

$tikifeedback = array();

//check if a user id is send to program
if (!isset($_REQUEST["assign_user"])) {
	$smarty->assign('msg', tra("Unknown user"));
	$smarty->display("error.tpl");
	die;
}

//assign user to smarty
$assign_user_id = $_REQUEST["assign_user"];
$smarty->assign_by_ref('assign_user_id', $assign_user_id);

//process the actions
if (isset($_REQUEST["action"])) {
  //check if group is request	
	if (!isset($_REQUEST["group"])) {
		$smarty->assign('msg', tra("You have to indicate a group"));
		$smarty->display("error.tpl");
		die;
	}

	//check if group exist
	if ($_REQUEST["action"] == 'assign') {
    check_ticket('user_assign_group', $area_id);
		if (!$userlib->group_exists($_REQUEST["group"], $LUA)) {
			$smarty->assign('msg', tra("This group is invalid"));
			$smarty->display("error.tpl");
			die;
		}
		//assign user to a group with liveuser lib
			$userlib->assign_user_to_group($_REQUEST["assign_user"], $_REQUEST["group"], $LUA);
			//$logslib->add_log('perms',sprintf("Assigned %s in group %s",$_REQUEST["assign_user"], $_REQUEST["group"]));
    //remove a user from a group
	} elseif ($_REQUEST["action"] == 'removegroup') {
    check_ticket('user_assign_group', $area_id);
		//$area = 'deluserfromgroup';
		//key_check($area);
				$userlib->remove_user_from_group($_REQUEST["assign_user"], $_REQUEST["group"], $LUA);
				//$logslib->add_log('perms',sprintf("Removed %s from group %s",$_REQUEST["assign_user"], $_REQUEST["group"]));
	}
}

//set a default group to a user
if(isset($_REQUEST['set_default'])) {
  check_ticket('user_assign_group', $area_id);
	$userlib->set_default_group($_REQUEST['login'],$_REQUEST['defaultgroup'], $LUA);
}

//Include generic definition of the code for manage filters
$default_sort_field = 'group_define_name';    //Default value of the field to sort
$default_sort_order = 'ASC'; //Default value of the order ASC = ascendant, DESC = descandant
include('filterManager.php');


//check that user exist in liveuser table
//and get informations about the user to asign
if (! $assign_user_info = $userlib->get_userid_info($assign_user_id, $LUA)) {
	$smarty->assign('msg', tra("User doesnt exist"));
	$smarty->display("error.tpl");
	die;
}else{
  $assign_user_handle = $assign_user_info['handle'];
  $smarty->assign_by_ref('assign_user_handle', $assign_user_handle);
  $smarty->assign_by_ref('user_info', $assign_user_info);
//and get informations about the user groups
  $include_groups = $userlib->get_user_groups_inclusion($assign_user_id, $LUA);
  //var_dump ($include_groups);
  $smarty->assign_by_ref('include_groups', $include_groups);
}

// get users (list of users)
$list = $userlib->get_groups($offset, $numrows, $sort_field, $sort_order, $find, $LUA);
$smarty->assign_by_ref('groups', $list);

//Include generic definition of the code for manage the pagination. $list must set...
include('paginationManager.php');

// disallow robots to index page:
$smarty->assign('metatag_robots', 'NOINDEX, NOFOLLOW');

// Active the tab
$smarty->assign('usersTab', 'active');

// Display the template
$smarty->assign('mid', 'tiki-assignuser.tpl');
$smarty->display("ranchbe.tpl");
?>
