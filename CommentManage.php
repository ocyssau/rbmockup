<?php
// +----------------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                           |
// +----------------------------------------------------------------------------+
// | This file is part of Ranchbe.                                              |
// |                                                                            |
// |  Ranchbe is free software; you can redistribute it and/or modify           |
// |  it under the terms of the GNU General Public License as published by      |
// |  the Free Software Foundation; either version 2 of the License, or         |
// |  (at your option) any later version.                                       |
// |                                                                            |
// |  Foobar is distributed in the hope that it will be useful,                 |
// |  but WITHOUT ANY WARRANTY; without even the implied warranty of            |
// |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             |
// |  GNU General Public License for more details.                              |
// |                                                                            |
// |  You should have received a copy of the GNU General Public License         |
// |  along with Foobar; if not, write to the Free Software                     |
// |  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA |
// +----------------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                                   |
// +----------------------------------------------------------------------------+

require_once './conf/ranchbe_setup.php';//ranchBE configuration
require_once './class/doccomment.php';
require_once './class/common/space.php';
$space = new space($_REQUEST['space']);
$doccomment = new doccomment($space);

switch ( $_REQUEST['action'] ) {
  case ('addComment'):
    $doccomment->add($_REQUEST['document_id'], $_REQUEST['newComment']);
    break;

  case ('delComment'):
    $doccomment->Suppress( $_REQUEST['comment_id'] );
    break;

} //end of switch
header("location: DocManage.php?space=".$_REQUEST['space']);

?>
