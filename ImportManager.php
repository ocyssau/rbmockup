<?php
// +----------------------------------------------------------------------+
// | Copyright (c) 2007 Ranchbe group                                     |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.gnu.org/licenses/gpl.html                                 |
// | and to licence.txt to root directory of ranchbe.                     |
// | The librairies content in lib directory can be subjects to an other  |
// | licence.                                                             |
// | If you did not receive a copy of the GPL license and are unable to   |
// | obtain it through the world-wide-web, please dont use this software. |
// +----------------------------------------------------------------------+
// | Author: Olivier Cyssau <ocyssau@free.fr>                             |
// +----------------------------------------------------------------------+

require_once './conf/ranchbe_setup.php';
require_once './class/common/container.php'; //Class to manage the bookshop

if(!isset($_REQUEST['space'])){
	$_REQUEST['space'] = $_SESSION['SelectedContainerType'];
}

if(!isset($_REQUEST['SelectedContainer'])){
	$_REQUEST['space'] = $_SESSION['SelectedContainer'];
}


if($_REQUEST['SelectedContainer']){
	$Manager = container::_factory($_REQUEST['space'], $_REQUEST['SelectedContainer']); //Create new manager
	$space =& $Manager->space;
	
	if($Manager->GetProperty('file_only') == 1){
		include './inc/fileImport.php';
	}
	else{
		include './inc/documentImport.php';
	}
}
else{
	require_once('./class/mockup.php');
	$Manager = new mockup(); //Create new manager
	$space =& $Manager->space;
	include './inc/fileImport.php';
}

